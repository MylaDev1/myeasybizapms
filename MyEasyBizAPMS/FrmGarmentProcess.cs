﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MyEasyBizAPMS
{
    public partial class FrmGarmentProcess : Form
    {
        public FrmGarmentProcess()
        {
            InitializeComponent();
            this.tabControlAdv1.TabStyle = typeof(Syncfusion.Windows.Forms.Tools.OneNoteStyleRenderer);
            this.SfdDataGridGarments.Style.HeaderStyle.Font.FontStyle = FontStyle.Bold;
            this.SfdDataGridGarments.Style.HeaderStyle.BackColor = Color.DeepSkyBlue;
        }
        SqlConnection connection = new SqlConnection(GeneralParameters.ConnectionString);
        SQLDBHelper db = new SQLDBHelper();
        BindingSource bsDocNo = new BindingSource();
        BindingSource bsGarment = new BindingSource();
        DataTable dtOrderQty = new DataTable();

        int Fillid = 0;
        private int SelectId = 0;
        int loadid = 0; int Editid = 0;

        private void FrmGarmentProcess_Load(object sender, EventArgs e)
        {
            grFront.Visible = true;
            Grback.Visible = false;
            LoadDataGridTrims();
            LoadComponentGrid();
            LoadDataGridBitProcess();
            LoadDataGridTrimsGarment();
            Uom();
        }

        protected void LoadDataGridTrims()
        {
            DataGridGarment.DataSource = null;
            DataGridGarment.AutoGenerateColumns = false;
            DataGridGarment.ColumnCount = 9;

            DataGridGarment.Columns[0].Name = "Component";
            DataGridGarment.Columns[0].HeaderText = "Component";
            DataGridGarment.Columns[0].Width = 250;

            DataGridGarment.Columns[1].Name = "ProcessName";
            DataGridGarment.Columns[1].HeaderText = "Process";
            DataGridGarment.Columns[1].Width = 200;

            DataGridGarment.Columns[2].Name = "Colour";
            DataGridGarment.Columns[2].HeaderText = "Colour";

            DataGridGarment.Columns[3].Name = "Qty";
            DataGridGarment.Columns[3].HeaderText = "Qty";

            DataGridGarment.Columns[4].Name = "Rate";
            DataGridGarment.Columns[4].HeaderText = "Rate";
            DataGridGarment.Columns[4].Visible = false;

            DataGridGarment.Columns[5].Name = "ProcessLoss";
            DataGridGarment.Columns[5].HeaderText = "ProcessLoss";
            DataGridGarment.Columns[5].Visible = false;

            DataGridGarment.Columns[6].Name = "ComponentUid";
            DataGridGarment.Columns[6].HeaderText = "ComponentUid";
            DataGridGarment.Columns[6].Visible = false;

            DataGridGarment.Columns[7].Name = "ProcessId";
            DataGridGarment.Columns[7].HeaderText = "ProcessId";
            DataGridGarment.Columns[7].Visible = false;

            DataGridGarment.Columns[8].Name = "Uid";
            DataGridGarment.Columns[8].HeaderText = "Uid";
            DataGridGarment.Columns[8].Visible = false;
        }
        protected void LoadDataGridTrimsGarment()
        {
            DataGridGar.DataSource = null;
            DataGridGar.AutoGenerateColumns = false;
            DataGridGar.ColumnCount = 9;

            DataGridGar.Columns[0].Name = "Garment";
            DataGridGar.Columns[0].HeaderText = "Garment";
            DataGridGar.Columns[0].Width = 250;

            DataGridGar.Columns[1].Name = "ProcessName";
            DataGridGar.Columns[1].HeaderText = "Process";
            DataGridGar.Columns[1].Width = 200;

            DataGridGar.Columns[2].Name = "Colour";
            DataGridGar.Columns[2].HeaderText = "Colour";

            DataGridGar.Columns[3].Name = "Qty";
            DataGridGar.Columns[3].HeaderText = "Qty";

            DataGridGar.Columns[4].Name = "Rate";
            DataGridGar.Columns[4].HeaderText = "Rate";
            DataGridGar.Columns[4].Visible = false;

            DataGridGar.Columns[5].Name = "ProcessLoss";
            DataGridGar.Columns[5].HeaderText = "ProcessLoss";
            DataGridGar.Columns[5].Visible = false;

            DataGridGar.Columns[6].Name = "garmentUid";
            DataGridGar.Columns[6].HeaderText = "garmentUid";
            DataGridGar.Columns[6].Visible = false;

            DataGridGar.Columns[7].Name = "ProcessId";
            DataGridGar.Columns[7].HeaderText = "ProcessId";
            DataGridGar.Columns[7].Visible = false;

            DataGridGar.Columns[8].Name = "Uid";
            DataGridGar.Columns[8].HeaderText = "Uid";
            DataGridGar.Columns[8].Visible = false;
        }
        protected void LoadDataGridBitProcess()
        {
            DataGridBitProcess.DataSource = null;
            DataGridBitProcess.AutoGenerateColumns = false;
            DataGridBitProcess.ColumnCount = 8;

            DataGridBitProcess.Columns[0].Name = "Description";
            DataGridBitProcess.Columns[0].HeaderText = "Description";
            DataGridBitProcess.Columns[0].Width = 250;

            DataGridBitProcess.Columns[1].Name = "ProcessName";
            DataGridBitProcess.Columns[1].HeaderText = "Process";
            DataGridBitProcess.Columns[1].Width = 200;

            DataGridBitProcess.Columns[2].Name = "Colour";
            DataGridBitProcess.Columns[2].HeaderText = "Colour";

            DataGridBitProcess.Columns[3].Name = "Qty";
            DataGridBitProcess.Columns[3].HeaderText = "Qty";

            DataGridBitProcess.Columns[4].Name = "Uom";
            DataGridBitProcess.Columns[4].HeaderText = "Uom";
            DataGridBitProcess.Columns[4].Visible = true;

            DataGridBitProcess.Columns[5].Name = "ProcessId";
            DataGridBitProcess.Columns[5].HeaderText = "ProcessId";
            DataGridBitProcess.Columns[5].Visible = false;

            DataGridBitProcess.Columns[6].Name = "UomId";
            DataGridBitProcess.Columns[6].HeaderText = "UomId";
            DataGridBitProcess.Columns[6].Visible = false;

            DataGridBitProcess.Columns[7].Name = "Uid";
            DataGridBitProcess.Columns[7].HeaderText = "Uid";
            DataGridBitProcess.Columns[7].Visible = false;
        }

        public void Uom()
        {
            string qur = "select Generalname as Uom,Guid from generalm where typemuid = 7";
            SqlCommand cmd = new SqlCommand(qur, connection);
            SqlDataAdapter apt = new SqlDataAdapter(cmd);
            DataTable tab = new DataTable();
            apt.Fill(tab);
            CmbUom.DataSource = null;
            CmbUom.DisplayMember = "Uom";
            CmbUom.ValueMember = "Guid";
            CmbUom.DataSource = tab;
        }

        private void TxtOrderNo_Click(object sender, EventArgs e)
        {
            try
            {
                string Query = "Select Uid,DocNo,DocDate,StyleDesc from OrderM Where Uid Not in (Select OrderMuid from OrderMBudjetGarmentProcess) and DocSts = 'Entry Completed' order by Uid desc";
                DataTable dt = db.GetDataWithoutParam(CommandType.Text, Query, connection);
                bsDocNo.DataSource = dt;
                FillGrid(dt, 1);
                Point loc = Genclass.FindLocation(txtOrderNo);
                grSearch.Location = new Point(loc.X - 10, loc.Y + 20);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void FillGrid(DataTable dt, int FillId)
        {
            try
            {
                DataGridCommon.DataSource = null;
                DataGridCommon.AutoGenerateColumns = false;
                if (FillId == 1)
                {
                    Fillid = 1;
                    DataGridCommon.ColumnCount = 3;
                    DataGridCommon.Columns[0].Name = "Uid";
                    DataGridCommon.Columns[0].HeaderText = "Uid";
                    DataGridCommon.Columns[0].DataPropertyName = "Uid";
                    DataGridCommon.Columns[0].Visible = false;
                    DataGridCommon.Columns[0].Width = 0;
                    DataGridCommon.Columns[1].Name = "DocNo";
                    DataGridCommon.Columns[1].HeaderText = "DocNo";
                    DataGridCommon.Columns[1].DataPropertyName = "DocNo";
                    DataGridCommon.Columns[1].Width = 100;
                    DataGridCommon.Columns[2].Name = "StyleDesc";
                    DataGridCommon.Columns[2].HeaderText = "StyleDesc";
                    DataGridCommon.Columns[2].DataPropertyName = "StyleDesc";
                    DataGridCommon.Columns[2].Width = 210;
                    DataGridCommon.DataSource = bsDocNo;
                }
                if (FillId == 2)
                {
                    Fillid = 2;
                    DataGridCommon.ColumnCount = 3;
                    DataGridCommon.Columns[0].Name = "Uid";
                    DataGridCommon.Columns[0].HeaderText = "Uid";
                    DataGridCommon.Columns[0].DataPropertyName = "GUid";
                    DataGridCommon.Columns[0].Visible = false;
                    DataGridCommon.Columns[0].Width = 0;
                    DataGridCommon.Columns[1].Name = "ProcessName";
                    DataGridCommon.Columns[1].HeaderText = "Process Name";
                    DataGridCommon.Columns[1].DataPropertyName = "GeneralName";
                    DataGridCommon.Columns[1].Width = 200;
                    DataGridCommon.Columns[2].Name = "ShortName";
                    DataGridCommon.Columns[2].HeaderText = "ShortName";
                    DataGridCommon.Columns[2].DataPropertyName = "ShortName";
                    DataGridCommon.DataSource = bsGarment;
                }
                if (FillId == 3)
                {
                    Fillid = 3;
                    DataGridCommon.ColumnCount = 3;
                    DataGridCommon.Columns[0].Name = "Uid";
                    DataGridCommon.Columns[0].HeaderText = "Uid";
                    DataGridCommon.Columns[0].DataPropertyName = "GUid";
                    DataGridCommon.Columns[0].Visible = false;
                    DataGridCommon.Columns[0].Width = 0;
                    DataGridCommon.Columns[1].Name = "ProcessName";
                    DataGridCommon.Columns[1].HeaderText = "Process Name";
                    DataGridCommon.Columns[1].DataPropertyName = "GeneralName";
                    DataGridCommon.Columns[1].Width = 200;
                    DataGridCommon.Columns[2].Name = "ShortName";
                    DataGridCommon.Columns[2].HeaderText = "ShortName";
                    DataGridCommon.Columns[2].DataPropertyName = "ShortName";
                    DataGridCommon.DataSource = bsGarment;
                }
                grSearch.Visible = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void TxtOrderNo_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bsDocNo.Filter = string.Format("DocNo LIKE '%{0}%'", txtOrderNo.Text);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void BtnSelect_Click(object sender, EventArgs e)
        {
            try
            {
                SelectId = 1;
                int Index = DataGridCommon.SelectedCells[0].RowIndex;
                if (Fillid == 1)
                {
                    SelectId = 1;
                    txtOrderNo.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtOrderNo.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    DataTable dtq = GetorderStyle(Convert.ToDecimal(txtOrderNo.Tag));
                    CmbStyle.DisplayMember = "StyleName";
                    CmbStyle.ValueMember = "Uid";
                    CmbStyle.DataSource = dtq;
                    SqlParameter[] sqlParameters12 = { new SqlParameter("@OrderMuid", txtOrderNo.Tag) };
                    DataSet dataSetQty = db.GetDataWithParamMultiple(CommandType.StoredProcedure, "Proc_GetOrderTotalQty", sqlParameters12, connection);
                    dtOrderQty = dataSetQty.Tables[0];
                    SelectId = 0;
                    SqlParameter[] sqlParameters = { new SqlParameter("@OrderMuid", txtOrderNo.Tag) };
                    DataTable dataTable = db.GetDataWithParam(CommandType.StoredProcedure, "SP_GetComponentSummary", sqlParameters, connection);
                    CmbComponent.DataSource = null;
                    CmbComponent.DisplayMember = "Component";
                    CmbComponent.ValueMember = "ComponentUid";
                    CmbComponent.DataSource = dataTable;
                }
                else if (Fillid == 2)
                {
                    txtProcess.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtProcess.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                }
                else if (Fillid == 3)
                {
                    txtBitProcess.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtBitProcess.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                }
                grSearch.Visible = false;
                SelectId = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }


        protected DataTable GetorderStyle(decimal orderId)
        {
            DataTable dataTable = new DataTable();
            try
            {
                SqlParameter[] parameters = { new SqlParameter("@OrderMUid", orderId) };
                dataTable = db.GetDataWithParam(CommandType.StoredProcedure, "SP_GetOrderMStyles", parameters, connection);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            return dataTable;
        }

        private void SplitAdd_Click(object sender, EventArgs e)
        {
            try
            {
                grFront.Visible = false;
                Grback.Visible = true;
                CmbStyle.DataSource = null;
                txtDocNo.Text = string.Empty;
                txtOrderNo.Text = string.Empty;
                DataGridGarment.Rows.Clear();
                txtDocNo.Text = GeneralParameters.GetDocNo(5, connection);
                txtDocNo.Tag = "0";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnHide_Click(object sender, EventArgs e)
        {
            grSearch.Visible = false;
        }

        private void txtProcess_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable data = GetGeneralData(33, 1);
                bsGarment.DataSource = data;
                FillGrid(data, 2);
                Point loc = Genclass.FindLocation(txtProcess);
                grSearch.Location = new Point(loc.X - 10, loc.Y + 20);
                grSearch.Visible = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected DataTable GetGeneralData(int TypemUid, int Active)
        {
            DataTable data = new DataTable();
            try
            {
                SqlParameter[] sqlParameters = { new SqlParameter("@TypeMUid", TypemUid), new SqlParameter("@Active", Active) };
                data = db.GetDataWithParam(CommandType.StoredProcedure, "SP_GetGeneralM", sqlParameters, connection);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            return data;
        }

        private void TxtProcess_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bsGarment.Filter = string.Format("GeneralName LIKE '%{0}%'", txtProcess.Text);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void BtnOk_Click(object sender, EventArgs e)
        {
            try
            {
                if (CmbComponent.SelectedIndex != -1 && txtProcess.Text != string.Empty && txtQty.Text != string.Empty)
                {
                    if (txtProcessLoss.Text == string.Empty)
                    {
                        txtProcessLoss.Text = "0";
                    }
                    if (txtRate.Text == string.Empty)
                    {
                        txtRate.Text = "0";
                    }
                    int uid = 0;
                    if (Editid == 0)
                    {
                        uid = 0;
                    }
                    else
                    {
                        uid = Editid;
                    }
                    int Index = DataGridGarment.Rows.Add();
                    DataGridViewRow dataGridViewRow = DataGridGarment.Rows[Index];
                    dataGridViewRow.Cells[0].Value = CmbComponent.Text;
                    dataGridViewRow.Cells[1].Value = txtProcess.Text;
                    dataGridViewRow.Cells[2].Value = txtColour.Text;
                    dataGridViewRow.Cells[3].Value = txtQty.Text;
                    dataGridViewRow.Cells[4].Value = "0";
                    dataGridViewRow.Cells[5].Value = "0";
                    dataGridViewRow.Cells[6].Value = CmbComponent.SelectedValue;
                    dataGridViewRow.Cells[7].Value = txtProcess.Tag;
                    dataGridViewRow.Cells[8].Value = uid;

                    CmbComponent.SelectedIndex = -1;
                    txtProcess.Text = string.Empty;
                    txtProcess.Tag = string.Empty;
                    txtColour.Text = string.Empty;
                    txtQty.Text = string.Empty;
                    txtRate.Text = string.Empty;
                    txtProcessLoss.Text = string.Empty;
                    Editid = 0;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void SplitSave_Click(object sender, EventArgs e)
        {
            try
            {
                for (int i = 0; i < DataGridGarment.Rows.Count; i++)
                {
                    SqlParameter[] sqlParameters = {
                        new SqlParameter("@Uid",DataGridGarment.Rows[i].Cells[8].Value.ToString()),
                        new SqlParameter("@OrderMuid",txtOrderNo.Tag),
                        new SqlParameter("@OrderMStyleUid",CmbStyle.SelectedValue),
                        new SqlParameter("@ComponentUid",DataGridGarment.Rows[i].Cells[6].Value.ToString()),
                        new SqlParameter("@GarmentProcessUid",DataGridGarment.Rows[i].Cells[7].Value.ToString()),
                        new SqlParameter("@Colour",DataGridGarment.Rows[i].Cells[2].Value.ToString()),
                        new SqlParameter("@ProcessLoss",DataGridGarment.Rows[i].Cells[5].Value.ToString()),
                        new SqlParameter("@Qty",DataGridGarment.Rows[i].Cells[3].Value.ToString()),
                        new SqlParameter("@Rate",DataGridGarment.Rows[i].Cells[4].Value.ToString()),
                        new SqlParameter("@CreaetDate",DateTime.Now),
                        new SqlParameter("@UserId",GeneralParameters.UserdId),
                        new SqlParameter("@DocNo",txtDocNo.Text),
                        new SqlParameter("@BitDesc",DBNull.Value),
                        new SqlParameter("@UomId","13"),
                        new SqlParameter("@GPType","Component")
                    };
                    db.ExecuteNonQuery(CommandType.StoredProcedure, "Proc_OrderMGarmentProcess", sqlParameters, connection);
                }
                for (int i = 0; i < DataGridGar.Rows.Count; i++)
                {
                    SqlParameter[] sqlParameters = {
                        new SqlParameter("@Uid",DataGridGar.Rows[i].Cells[8].Value.ToString()),
                        new SqlParameter("@OrderMuid",txtOrderNo.Tag),
                        new SqlParameter("@OrderMStyleUid",CmbStyle.SelectedValue),
                        new SqlParameter("@ComponentUid",DataGridGar.Rows[i].Cells[6].Value.ToString()),
                        new SqlParameter("@GarmentProcessUid",DataGridGar.Rows[i].Cells[7].Value.ToString()),
                        new SqlParameter("@Colour",DataGridGar.Rows[i].Cells[2].Value.ToString()),
                        new SqlParameter("@ProcessLoss",DataGridGar.Rows[i].Cells[5].Value.ToString()),
                        new SqlParameter("@Qty",DataGridGar.Rows[i].Cells[3].Value.ToString()),
                        new SqlParameter("@Rate",DataGridGar.Rows[i].Cells[4].Value.ToString()),
                        new SqlParameter("@CreaetDate",DateTime.Now),
                        new SqlParameter("@UserId",GeneralParameters.UserdId),
                        new SqlParameter("@DocNo",txtDocNo.Text),
                        new SqlParameter("@BitDesc",DBNull.Value),
                        new SqlParameter("@UomId","13"),
                        new SqlParameter("@GPType","Garment")
                    };
                    db.ExecuteNonQuery(CommandType.StoredProcedure, "Proc_OrderMGarmentProcess", sqlParameters, connection);
                }
                for (int i = 0; i < DataGridBitProcess.Rows.Count; i++)
                {
                    SqlParameter[] sqlParametersd = {
                        new SqlParameter("@Uid",DataGridBitProcess.Rows[i].Cells[7].Value.ToString()),
                        new SqlParameter("@OrderMuid",txtOrderNo.Tag),
                        new SqlParameter("@OrderMStyleUid",CmbStyle.SelectedValue),
                        new SqlParameter("@ComponentUid","0"),
                        new SqlParameter("@GarmentProcessUid",DataGridBitProcess.Rows[i].Cells[5].Value.ToString()),
                        new SqlParameter("@Colour",DataGridBitProcess.Rows[i].Cells[2].Value.ToString()),
                        new SqlParameter("@ProcessLoss","0"),
                        new SqlParameter("@Qty",DataGridBitProcess.Rows[i].Cells[3].Value.ToString()),
                        new SqlParameter("@Rate","0"),
                        new SqlParameter("@CreaetDate",DateTime.Now),
                        new SqlParameter("@UserId",GeneralParameters.UserdId),
                        new SqlParameter("@DocNo",txtDocNo.Text),
                        new SqlParameter("@BitDesc",DataGridBitProcess.Rows[i].Cells[0].Value.ToString()),
                        new SqlParameter("@UomId",DataGridBitProcess.Rows[i].Cells[6].Value.ToString()),
                        new SqlParameter("@GPType","BitProcess")
                    };
                    db.ExecuteNonQuery(CommandType.StoredProcedure, "Proc_OrderMGarmentProcess", sqlParametersd, connection);
                }


                if (txtDocNo.Tag.ToString() == "0")
                {
                    string Query = "Update DocTypeM Set Lastno = Lastno+1 Where DocTypeId = 6";
                    db.ExecuteNonQuery(CommandType.Text, Query, connection);
                }
                MessageBox.Show("Record has been saved successfully", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                CmbStyle.DataSource = null;
                txtDocNo.Text = string.Empty;
                txtOrderNo.Text = string.Empty;
                DataGridGarment.Rows.Clear();
                DataGridBitProcess.Rows.Clear();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected void LoadComponentGrid()
        {
            try
            {
                DataTable dataTable = db.GetDataWithoutParam(CommandType.StoredProcedure, "Proc_GetOrderMGarmentProcess", connection);
                SfdDataGridGarments.DataSource = null;
                SfdDataGridGarments.DataSource = dataTable;
                SfdDataGridGarments.Columns[0].Width = 100;
                SfdDataGridGarments.Columns[2].Width = 180;
                SfdDataGridGarments.Columns[6].Visible = false;
                SfdDataGridGarments.Columns[4].Visible = false;
                SfdDataGridGarments.Columns[5].Width = 380;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void SplitAdd_DropDowItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            try
            {
                if (e.ClickedItem.Text == "Close")
                {
                    this.Close();
                }
                else
                {
                    if (SfdDataGridGarments.SelectedIndex == -1)
                    {
                        MessageBox.Show("Select a row", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                    var selectedItem = SfdDataGridGarments.SelectedItems[0];
                    var dataRow = (selectedItem as DataRowView).Row;
                    decimal CUid = Convert.ToDecimal(dataRow["OrderMuid"].ToString());
                    SqlParameter[] sqlParameters = { new SqlParameter("@OrdermUid", CUid) };
                    DataTable dataTableM = db.GetDataWithParam(CommandType.StoredProcedure, "Proc_OrderMGarmentProcessEdit", sqlParameters, connection);
                    DataTable dtq = GetorderStyle(CUid);
                    CmbStyle.DisplayMember = "StyleName";
                    CmbStyle.ValueMember = "Uid";
                    CmbStyle.DataSource = dtq;
                    SqlParameter[] sqlParameters12 = { new SqlParameter("@OrderMuid", CUid) };
                    DataSet dataSetQty = db.GetDataWithParamMultiple(CommandType.StoredProcedure, "Proc_GetOrderTotalQty", sqlParameters12, connection);
                    dtOrderQty = dataSetQty.Tables[0];
                    SqlParameter[] sqlParameters11 = { new SqlParameter("@OrderMuid", CUid) };
                    DataTable dataTable1 = db.GetDataWithParam(CommandType.StoredProcedure, "SP_GetComponentSummary", sqlParameters11, connection);
                    CmbComponent.DataSource = null;
                    CmbComponent.DisplayMember = "Component";
                    CmbComponent.ValueMember = "ComponentUid";
                    CmbComponent.DataSource = dataTable1;

                    DataTable dataTable = new DataTable();
                    DataTable dataTableProcess = new DataTable();
                    DataTable dataTablegarmentss = new DataTable();
                    DataRow[] dataRows = dataTableM.Select("GPType='Component'");
                    if(dataRows.Length > 0)
                    {
                        dataTable = dataTableM.Select("GPType='Component'").CopyToDataTable();
                    }
                    else
                    {
                        dataTable = dataTableM.Clone();
                    }
                    DataRow[] dataRowsProcess = dataTableM.Select("GPType='BitProcess'");
                    if (dataRowsProcess.Length > 0)
                    {
                        dataTableProcess = dataTableM.Select("GPType='BitProcess'").CopyToDataTable();
                    }
                    else
                    {
                        dataTableProcess = dataTableM.Clone();
                    }
                    DataRow[] daterowgarment = dataTableM.Select("GPType='Garment'");
                    if (daterowgarment.Length > 0)
                    {
                        dataTablegarmentss = dataTableM.Select("GPType='Garment'").CopyToDataTable();
                    }
                    else
                    {
                        dataTablegarmentss = dataTableM.Clone();
                    }
                    DataGridGar.Rows.Clear();
                    for (int i = 0; i < dataTablegarmentss.Rows.Count; i++)
                    {
                        int Index = DataGridGar.Rows.Add();
                        DataGridViewRow dataGridViewRow = DataGridGar.Rows[Index];
                        dataGridViewRow.Cells[0].Value = dataTablegarmentss.Rows[i]["Component"].ToString();
                        dataGridViewRow.Cells[1].Value = dataTablegarmentss.Rows[i]["process"].ToString();
                        dataGridViewRow.Cells[2].Value = dataTablegarmentss.Rows[i]["Colour"].ToString();
                        dataGridViewRow.Cells[3].Value = dataTablegarmentss.Rows[i]["Qty"].ToString();
                        dataGridViewRow.Cells[4].Value = dataTablegarmentss.Rows[i]["Rate"].ToString();
                        dataGridViewRow.Cells[5].Value = dataTablegarmentss.Rows[i]["ProcessLoss"].ToString();
                        dataGridViewRow.Cells[6].Value = dataTablegarmentss.Rows[i]["ComponentUid"].ToString();
                        dataGridViewRow.Cells[7].Value = dataTablegarmentss.Rows[i]["GarmentProcessUid"].ToString();
                        dataGridViewRow.Cells[8].Value = dataTablegarmentss.Rows[i]["Uid"].ToString();
                    }

                    DataGridGarment.Rows.Clear();
                    for (int i = 0; i < dataTable.Rows.Count; i++)
                    {
                        int Index = DataGridGarment.Rows.Add();
                        DataGridViewRow dataGridViewRow = DataGridGarment.Rows[Index];
                        dataGridViewRow.Cells[0].Value = dataTable.Rows[i]["Component"].ToString();
                        dataGridViewRow.Cells[1].Value = dataTable.Rows[i]["process"].ToString();
                        dataGridViewRow.Cells[2].Value = dataTable.Rows[i]["Colour"].ToString();
                        dataGridViewRow.Cells[3].Value = dataTable.Rows[i]["Qty"].ToString();
                        dataGridViewRow.Cells[4].Value = dataTable.Rows[i]["Rate"].ToString();
                        dataGridViewRow.Cells[5].Value = dataTable.Rows[i]["ProcessLoss"].ToString();
                        dataGridViewRow.Cells[6].Value = dataTable.Rows[i]["ComponentUid"].ToString();
                        dataGridViewRow.Cells[7].Value = dataTable.Rows[i]["GarmentProcessUid"].ToString();
                        dataGridViewRow.Cells[8].Value = dataTable.Rows[i]["Uid"].ToString();
                    }

                    DataGridBitProcess.Rows.Clear();
                    for (int i = 0; i < dataTableProcess.Rows.Count; i++)
                    {
                        int Index = DataGridBitProcess.Rows.Add();
                        DataGridViewRow dataGridViewRow = DataGridBitProcess.Rows[Index];
                        dataGridViewRow.Cells[0].Value = dataTableProcess.Rows[i]["BitDesc"].ToString();
                        dataGridViewRow.Cells[1].Value = dataTableProcess.Rows[i]["process"].ToString();
                        dataGridViewRow.Cells[2].Value = dataTableProcess.Rows[i]["Colour"].ToString();
                        dataGridViewRow.Cells[3].Value = dataTableProcess.Rows[i]["Qty"].ToString();
                        dataGridViewRow.Cells[4].Value = dataTableProcess.Rows[i]["Uom"].ToString();
                        dataGridViewRow.Cells[5].Value = dataTableProcess.Rows[i]["GarmentProcessUid"].ToString();
                        dataGridViewRow.Cells[6].Value = dataTableProcess.Rows[i]["UomId"].ToString();
                        dataGridViewRow.Cells[7].Value = dataTableProcess.Rows[i]["Uid"].ToString();
                    }
                    txtDocNo.Text = dataTableM.Rows[0]["DocNo"].ToString();
                    txtDocNo.Tag = dataTableM.Rows[0]["Uid"].ToString();
                    txtOrderNo.Text = dataRow["OrderNo"].ToString();
                    txtOrderNo.Tag = CUid;
                    dtpOrderDate.Text = dataRow["OrderDate"].ToString();
                    grFront.Visible = false;
                    Grback.Visible = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void SplitSave_DropDowItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            try
            {
                if (e.ClickedItem.Text == "Back")
                {
                    grFront.Visible = true;
                    Grback.Visible = false;
                    LoadComponentGrid();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void CmbComponent_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtQty.Text = dtOrderQty.Rows[0]["OrderPlanQty"].ToString();
        }

        private void DataGridGarment_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                int Index = DataGridGarment.SelectedCells[0].RowIndex;
                CmbComponent.Text = DataGridGarment.Rows[Index].Cells[0].Value.ToString();
                txtProcess.Text = DataGridGarment.Rows[Index].Cells[1].Value.ToString();
                txtColour.Text = DataGridGarment.Rows[Index].Cells[2].Value.ToString();
                txtQty.Text = DataGridGarment.Rows[Index].Cells[3].Value.ToString();
                txtRate.Text = DataGridGarment.Rows[Index].Cells[4].Value.ToString();
                txtProcessLoss.Text = DataGridGarment.Rows[Index].Cells[5].Value.ToString();
                txtProcess.Tag = DataGridGarment.Rows[Index].Cells[7].Value.ToString();
                Editid = Convert.ToInt32(DataGridGarment.Rows[Index].Cells[8].Value.ToString());
                DataGridGarment.Rows.RemoveAt(Index);
                DataGridGarment.ClearSelection();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void BtnBitProcessOk_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtBitDescription.Text != string.Empty && txtBitProcess.Text != string.Empty && txtBitQty.Text != string.Empty && CmbUom.Text != string.Empty)
                {
                    int uid = 0;
                    if (Editid == 0)
                    {
                        uid = 0;
                    }
                    else
                    {
                        uid = Editid;
                    }
                    int Index = DataGridBitProcess.Rows.Add();
                    DataGridViewRow dataGridViewRow = DataGridBitProcess.Rows[Index];
                    dataGridViewRow.Cells[0].Value = txtBitDescription.Text;
                    dataGridViewRow.Cells[1].Value = txtBitProcess.Text;
                    dataGridViewRow.Cells[2].Value = txtBitColour.Text;
                    dataGridViewRow.Cells[3].Value = txtBitQty.Text;
                    dataGridViewRow.Cells[4].Value = CmbUom.Text;
                    dataGridViewRow.Cells[5].Value = txtBitProcess.Tag;
                    dataGridViewRow.Cells[6].Value = CmbUom.SelectedValue;
                    dataGridViewRow.Cells[7].Value = uid;

                    CmbUom.SelectedIndex = -1;
                    SelectId = 1;
                    txtBitProcess.Text = string.Empty;
                    txtBitProcess.Tag = string.Empty;
                    txtBitColour.Text = string.Empty;
                    txtBitQty.Text = string.Empty;
                    txtBitDescription.Text = string.Empty;
                    SelectId = 0;
                    Editid = 0;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void TxtBitProcess_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable data = GetGeneralData(33, 1);
                bsGarment.DataSource = data;
                FillGrid(data, 3);
                Point loc = Genclass.FindLocation(txtProcess);
                grSearch.Location = new Point(loc.X - 10, loc.Y + 20);
                grSearch.Visible = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void TxtBitProcess_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bsGarment.Filter = string.Format("GeneralName LIKE '%{0}%'", txtProcess.Text);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void DataGridCommon_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            BtnSelect_Click(sender, e);
        }

        private void DataGridBitProcess_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                int Index = DataGridBitProcess.SelectedCells[0].RowIndex;
                txtBitDescription.Text = DataGridBitProcess.Rows[Index].Cells[0].Value.ToString();
                txtBitProcess.Text = DataGridBitProcess.Rows[Index].Cells[1].Value.ToString();
                txtBitColour.Text = DataGridBitProcess.Rows[Index].Cells[2].Value.ToString();
                txtBitQty.Text = DataGridBitProcess.Rows[Index].Cells[3].Value.ToString();
                CmbUom.Text = DataGridBitProcess.Rows[Index].Cells[4].Value.ToString();
                txtBitProcess.Tag = DataGridBitProcess.Rows[Index].Cells[5].Value.ToString();
                Editid = Convert.ToInt32(DataGridBitProcess.Rows[Index].Cells[7].Value.ToString());
                DataGridBitProcess.Rows.RemoveAt(Index);
                DataGridBitProcess.ClearSelection();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void DataGridGarment_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if(e.KeyCode == Keys.Delete)
                {
                    DialogResult dialogResult = MessageBox.Show("Do you want to delete this row ?", "Information", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                    if(dialogResult == DialogResult.Yes)
                    {
                        int Index = DataGridGarment.SelectedCells[0].RowIndex;
                        int Uid = Convert.ToInt32(DataGridGarment.Rows[Index].Cells[8].Value.ToString());
                        if (Uid == 0)
                        {
                            DataGridGarment.Rows.RemoveAt(Index);
                            DataGridGarment.ClearSelection();
                        }
                        else
                        {
                            string Query = "Delete from OrderMgarmentProcess Where Uid =" + Uid + " and OrderMuid = " + txtOrderNo.Tag + "";
                            db.ExecuteNonQuery(CommandType.Text, Query, connection);
                            DataGridGarment.Rows.RemoveAt(Index);
                            DataGridGarment.ClearSelection();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                if (cbogarment.SelectedIndex != -1 && GarProcess.Text != string.Empty && garqty.Text != string.Empty)
                {
                    if (gatloss.Text == string.Empty)
                    {
                        gatloss.Text = "0";
                    }
                    if (garrate.Text == string.Empty)
                    {
                        garrate.Text = "0";
                    }
                    int uid = 0;
                    if (Editid == 0)
                    {
                        uid = 0;
                    }
                    else
                    {
                        uid = Editid;
                    }
                    int Index = DataGridGar.Rows.Add();
                    DataGridViewRow dataGridViewRow = DataGridGar.Rows[Index];
                    dataGridViewRow.Cells[0].Value = cbogarment.Text;
                    dataGridViewRow.Cells[1].Value = GarProcess.Text;
                    dataGridViewRow.Cells[2].Value = Garcolour.Text;
                    dataGridViewRow.Cells[3].Value = garqty.Text;
                    dataGridViewRow.Cells[4].Value = "0";
                    dataGridViewRow.Cells[5].Value = "0";
                    dataGridViewRow.Cells[6].Value = cbogarment.SelectedValue;
                    dataGridViewRow.Cells[7].Value = GarProcess.Tag;
                    dataGridViewRow.Cells[8].Value = uid;

                    cbogarment.SelectedIndex = -1;
                    GarProcess.Text = string.Empty;
                    GarProcess.Tag = string.Empty;
                    Garcolour.Text = string.Empty;
                    garqty.Text = string.Empty;
                    garrate.Text = string.Empty;
                    gatloss.Text = string.Empty;
                    Editid = 0;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
    }
}
