﻿namespace MyEasyBizAPMS
{
    partial class FrmProcess
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Syncfusion.Windows.Forms.Tools.SplitButtonRenderer splitButtonRenderer1 = new Syncfusion.Windows.Forms.Tools.SplitButtonRenderer();
            Syncfusion.Windows.Forms.Tools.SplitButtonRenderer splitButtonRenderer2 = new Syncfusion.Windows.Forms.Tools.SplitButtonRenderer();
            this.SplitSave = new Syncfusion.Windows.Forms.Tools.SplitButton();
            this.toolstripBack = new Syncfusion.Windows.Forms.Tools.toolstripitem();
            this.SplitAdd = new Syncfusion.Windows.Forms.Tools.SplitButton();
            this.toolstripEdit = new Syncfusion.Windows.Forms.Tools.toolstripitem();
            this.toolDelete = new Syncfusion.Windows.Forms.Tools.toolstripitem();
            this.toolstripitem1 = new Syncfusion.Windows.Forms.Tools.toolstripitem();
            this.grBack = new System.Windows.Forms.GroupBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.CmbProcessGroup = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.chckTrims = new System.Windows.Forms.CheckBox();
            this.chckGarments = new System.Windows.Forms.CheckBox();
            this.chckComponents = new System.Windows.Forms.CheckBox();
            this.chckFabric = new System.Windows.Forms.CheckBox();
            this.chckYarn = new System.Windows.Forms.CheckBox();
            this.chckActive = new System.Windows.Forms.CheckBox();
            this.CmbTax = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtShortName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtProcessName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.grFront = new System.Windows.Forms.GroupBox();
            this.SfDataGridProcess = new Syncfusion.WinForms.DataGrid.SfDataGrid();
            this.grBack.SuspendLayout();
            this.grFront.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SfDataGridProcess)).BeginInit();
            this.SuspendLayout();
            // 
            // SplitSave
            // 
            this.SplitSave.BackColor = System.Drawing.SystemColors.Control;
            this.SplitSave.BeforeTouchSize = new System.Drawing.Size(75, 32);
            this.SplitSave.DropDownItems.Add(this.toolstripBack);
            this.SplitSave.DropDownPosition = Syncfusion.Windows.Forms.Tools.Position.Bottom;
            this.SplitSave.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SplitSave.ForeColor = System.Drawing.Color.Black;
            this.SplitSave.Location = new System.Drawing.Point(631, 368);
            this.SplitSave.MinimumSize = new System.Drawing.Size(75, 23);
            this.SplitSave.Name = "SplitSave";
            splitButtonRenderer1.SplitButton = this.SplitSave;
            this.SplitSave.Renderer = splitButtonRenderer1;
            this.SplitSave.ShowDropDownOnButtonClick = false;
            this.SplitSave.Size = new System.Drawing.Size(75, 32);
            this.SplitSave.TabIndex = 12;
            this.SplitSave.Text = "Save";
            this.SplitSave.DropDowItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.SplitSave_DropDowItemClicked);
            this.SplitSave.Click += new System.EventHandler(this.SplitSave_Click);
            // 
            // toolstripBack
            // 
            this.toolstripBack.Name = "toolstripBack";
            this.toolstripBack.Size = new System.Drawing.Size(23, 23);
            this.toolstripBack.Text = "Back";
            // 
            // SplitAdd
            // 
            this.SplitAdd.BackColor = System.Drawing.SystemColors.Control;
            this.SplitAdd.BeforeTouchSize = new System.Drawing.Size(75, 30);
            this.SplitAdd.DropDownItems.Add(this.toolstripEdit);
            this.SplitAdd.DropDownItems.Add(this.toolDelete);
            this.SplitAdd.DropDownItems.Add(this.toolstripitem1);
            this.SplitAdd.DropDownPosition = Syncfusion.Windows.Forms.Tools.Position.Bottom;
            this.SplitAdd.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SplitAdd.ForeColor = System.Drawing.Color.Black;
            this.SplitAdd.Location = new System.Drawing.Point(649, 370);
            this.SplitAdd.MinimumSize = new System.Drawing.Size(75, 23);
            this.SplitAdd.Name = "SplitAdd";
            splitButtonRenderer2.SplitButton = this.SplitAdd;
            this.SplitAdd.Renderer = splitButtonRenderer2;
            this.SplitAdd.ShowDropDownOnButtonClick = false;
            this.SplitAdd.Size = new System.Drawing.Size(75, 30);
            this.SplitAdd.TabIndex = 1;
            this.SplitAdd.Text = "Add";
            this.SplitAdd.DropDowItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.SplitAdd_DropDowItemClicked);
            this.SplitAdd.Click += new System.EventHandler(this.SplitAdd_Click);
            // 
            // toolstripEdit
            // 
            this.toolstripEdit.Name = "toolstripEdit";
            this.toolstripEdit.Size = new System.Drawing.Size(23, 23);
            this.toolstripEdit.Text = "Edit";
            // 
            // toolDelete
            // 
            this.toolDelete.Name = "toolDelete";
            this.toolDelete.Size = new System.Drawing.Size(23, 23);
            this.toolDelete.Text = "Delete";
            // 
            // toolstripitem1
            // 
            this.toolstripitem1.Name = "toolstripitem1";
            this.toolstripitem1.Size = new System.Drawing.Size(23, 23);
            this.toolstripitem1.Text = "Close";
            // 
            // grBack
            // 
            this.grBack.Controls.Add(this.label8);
            this.grBack.Controls.Add(this.label7);
            this.grBack.Controls.Add(this.label6);
            this.grBack.Controls.Add(this.label5);
            this.grBack.Controls.Add(this.CmbProcessGroup);
            this.grBack.Controls.Add(this.label4);
            this.grBack.Controls.Add(this.chckTrims);
            this.grBack.Controls.Add(this.SplitSave);
            this.grBack.Controls.Add(this.chckGarments);
            this.grBack.Controls.Add(this.chckComponents);
            this.grBack.Controls.Add(this.chckFabric);
            this.grBack.Controls.Add(this.chckYarn);
            this.grBack.Controls.Add(this.chckActive);
            this.grBack.Controls.Add(this.CmbTax);
            this.grBack.Controls.Add(this.label3);
            this.grBack.Controls.Add(this.txtShortName);
            this.grBack.Controls.Add(this.label2);
            this.grBack.Controls.Add(this.txtProcessName);
            this.grBack.Controls.Add(this.label1);
            this.grBack.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grBack.Location = new System.Drawing.Point(8, 3);
            this.grBack.Name = "grBack";
            this.grBack.Size = new System.Drawing.Size(724, 406);
            this.grBack.TabIndex = 0;
            this.grBack.TabStop = false;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.ForeColor = System.Drawing.Color.Red;
            this.label8.Location = new System.Drawing.Point(474, 98);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(13, 15);
            this.label8.TabIndex = 18;
            this.label8.Text = "*";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.ForeColor = System.Drawing.Color.Red;
            this.label7.Location = new System.Drawing.Point(407, 62);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(13, 15);
            this.label7.TabIndex = 17;
            this.label7.Text = "*";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.ForeColor = System.Drawing.Color.Red;
            this.label6.Location = new System.Drawing.Point(681, 25);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(13, 15);
            this.label6.TabIndex = 16;
            this.label6.Text = "*";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.Color.Red;
            this.label5.Location = new System.Drawing.Point(408, 346);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(13, 15);
            this.label5.TabIndex = 15;
            this.label5.Text = "*";
            // 
            // CmbProcessGroup
            // 
            this.CmbProcessGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CmbProcessGroup.FormattingEnabled = true;
            this.CmbProcessGroup.Location = new System.Drawing.Point(138, 341);
            this.CmbProcessGroup.Name = "CmbProcessGroup";
            this.CmbProcessGroup.Size = new System.Drawing.Size(266, 23);
            this.CmbProcessGroup.TabIndex = 13;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(45, 345);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(87, 15);
            this.label4.TabIndex = 14;
            this.label4.Text = "Process Group";
            // 
            // chckTrims
            // 
            this.chckTrims.AutoSize = true;
            this.chckTrims.Location = new System.Drawing.Point(138, 303);
            this.chckTrims.Name = "chckTrims";
            this.chckTrims.Size = new System.Drawing.Size(56, 19);
            this.chckTrims.TabIndex = 8;
            this.chckTrims.Text = "Trims";
            this.chckTrims.UseVisualStyleBackColor = true;
            // 
            // chckGarments
            // 
            this.chckGarments.AutoSize = true;
            this.chckGarments.Location = new System.Drawing.Point(138, 261);
            this.chckGarments.Name = "chckGarments";
            this.chckGarments.Size = new System.Drawing.Size(79, 19);
            this.chckGarments.TabIndex = 7;
            this.chckGarments.Text = "Garments";
            this.chckGarments.UseVisualStyleBackColor = true;
            // 
            // chckComponents
            // 
            this.chckComponents.AutoSize = true;
            this.chckComponents.Location = new System.Drawing.Point(138, 221);
            this.chckComponents.Name = "chckComponents";
            this.chckComponents.Size = new System.Drawing.Size(94, 19);
            this.chckComponents.TabIndex = 6;
            this.chckComponents.Text = "Components";
            this.chckComponents.UseVisualStyleBackColor = true;
            // 
            // chckFabric
            // 
            this.chckFabric.AutoSize = true;
            this.chckFabric.Location = new System.Drawing.Point(138, 182);
            this.chckFabric.Name = "chckFabric";
            this.chckFabric.Size = new System.Drawing.Size(61, 19);
            this.chckFabric.TabIndex = 5;
            this.chckFabric.Text = "Fabric";
            this.chckFabric.UseVisualStyleBackColor = true;
            // 
            // chckYarn
            // 
            this.chckYarn.AutoSize = true;
            this.chckYarn.Location = new System.Drawing.Point(138, 141);
            this.chckYarn.Name = "chckYarn";
            this.chckYarn.Size = new System.Drawing.Size(50, 19);
            this.chckYarn.TabIndex = 4;
            this.chckYarn.Text = "Yarn";
            this.chckYarn.UseVisualStyleBackColor = true;
            // 
            // chckActive
            // 
            this.chckActive.AutoSize = true;
            this.chckActive.Location = new System.Drawing.Point(410, 97);
            this.chckActive.Name = "chckActive";
            this.chckActive.Size = new System.Drawing.Size(58, 19);
            this.chckActive.TabIndex = 3;
            this.chckActive.Text = "Active";
            this.chckActive.UseVisualStyleBackColor = true;
            // 
            // CmbTax
            // 
            this.CmbTax.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CmbTax.FormattingEnabled = true;
            this.CmbTax.Location = new System.Drawing.Point(138, 95);
            this.CmbTax.Name = "CmbTax";
            this.CmbTax.Size = new System.Drawing.Size(266, 23);
            this.CmbTax.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(107, 101);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(25, 15);
            this.label3.TabIndex = 4;
            this.label3.Text = "Tax";
            // 
            // txtShortName
            // 
            this.txtShortName.Location = new System.Drawing.Point(138, 58);
            this.txtShortName.Name = "txtShortName";
            this.txtShortName.Size = new System.Drawing.Size(266, 23);
            this.txtShortName.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(62, 62);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(70, 15);
            this.label2.TabIndex = 2;
            this.label2.Text = "Short Name";
            // 
            // txtProcessName
            // 
            this.txtProcessName.Location = new System.Drawing.Point(138, 23);
            this.txtProcessName.Name = "txtProcessName";
            this.txtProcessName.Size = new System.Drawing.Size(537, 23);
            this.txtProcessName.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(48, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(84, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "Process Name";
            // 
            // grFront
            // 
            this.grFront.Controls.Add(this.SfDataGridProcess);
            this.grFront.Controls.Add(this.SplitAdd);
            this.grFront.Location = new System.Drawing.Point(8, 3);
            this.grFront.Name = "grFront";
            this.grFront.Size = new System.Drawing.Size(730, 406);
            this.grFront.TabIndex = 13;
            this.grFront.TabStop = false;
            // 
            // SfDataGridProcess
            // 
            this.SfDataGridProcess.AccessibleName = "Table";
            this.SfDataGridProcess.Location = new System.Drawing.Point(5, 14);
            this.SfDataGridProcess.Name = "SfDataGridProcess";
            this.SfDataGridProcess.Size = new System.Drawing.Size(719, 350);
            this.SfDataGridProcess.TabIndex = 0;
            this.SfDataGridProcess.Text = "sfDataGrid1";
            // 
            // FrmProcess
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(739, 415);
            this.Controls.Add(this.grFront);
            this.Controls.Add(this.grBack);
            this.Name = "FrmProcess";
            this.Text = "Process";
            this.Load += new System.EventHandler(this.FrmProcess_Load);
            this.grBack.ResumeLayout(false);
            this.grBack.PerformLayout();
            this.grFront.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.SfDataGridProcess)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grBack;
        private System.Windows.Forms.TextBox txtProcessName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtShortName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox CmbTax;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckBox chckTrims;
        private System.Windows.Forms.CheckBox chckGarments;
        private System.Windows.Forms.CheckBox chckComponents;
        private System.Windows.Forms.CheckBox chckFabric;
        private System.Windows.Forms.CheckBox chckYarn;
        private System.Windows.Forms.CheckBox chckActive;
        private Syncfusion.Windows.Forms.Tools.SplitButton SplitSave;
        private Syncfusion.Windows.Forms.Tools.toolstripitem toolstripBack;
        private System.Windows.Forms.GroupBox grFront;
        private Syncfusion.WinForms.DataGrid.SfDataGrid SfDataGridProcess;
        private Syncfusion.Windows.Forms.Tools.SplitButton SplitAdd;
        private Syncfusion.Windows.Forms.Tools.toolstripitem toolstripEdit;
        private Syncfusion.Windows.Forms.Tools.toolstripitem toolstripitem1;
        private Syncfusion.Windows.Forms.Tools.toolstripitem toolDelete;
        private System.Windows.Forms.ComboBox CmbProcessGroup;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
    }
}