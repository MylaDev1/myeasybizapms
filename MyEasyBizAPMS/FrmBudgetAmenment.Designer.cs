﻿namespace MyEasyBizAPMS
{
    partial class FrmBudgetAmenment
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Syncfusion.Windows.Forms.Tools.SplitButtonRenderer splitButtonRenderer1 = new Syncfusion.Windows.Forms.Tools.SplitButtonRenderer();
            Syncfusion.Windows.Forms.Tools.SplitButtonRenderer splitButtonRenderer2 = new Syncfusion.Windows.Forms.Tools.SplitButtonRenderer();
            this.SplitSave = new Syncfusion.Windows.Forms.Tools.SplitButton();
            this.toolstripBack = new Syncfusion.Windows.Forms.Tools.toolstripitem();
            this.SplitAdd = new Syncfusion.Windows.Forms.Tools.SplitButton();
            this.toolstripEdit = new Syncfusion.Windows.Forms.Tools.toolstripitem();
            this.toolstripClose = new Syncfusion.Windows.Forms.Tools.toolstripitem();
            this.grBack = new System.Windows.Forms.GroupBox();
            this.grSearch = new System.Windows.Forms.GroupBox();
            this.btnSelect = new System.Windows.Forms.Button();
            this.DataGridCommon = new System.Windows.Forms.DataGridView();
            this.btnHide = new System.Windows.Forms.Button();
            this.tabControlAmentment = new Syncfusion.Windows.Forms.Tools.TabControlAdv();
            this.tabPageYarn = new Syncfusion.Windows.Forms.Tools.TabPageAdv();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.DataGridYan = new System.Windows.Forms.DataGridView();
            this.tabPageFabric = new Syncfusion.Windows.Forms.Tools.TabPageAdv();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.DataGridFabric = new System.Windows.Forms.DataGridView();
            this.tabPageTrimsPurchase = new Syncfusion.Windows.Forms.Tools.TabPageAdv();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.btnTrimsPurchase = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblItemName = new System.Windows.Forms.Label();
            this.lblCtgy = new System.Windows.Forms.Label();
            this.txtQty = new System.Windows.Forms.TextBox();
            this.txtRate = new System.Windows.Forms.TextBox();
            this.txtItemName = new System.Windows.Forms.TextBox();
            this.txtCtgy = new System.Windows.Forms.TextBox();
            this.DataGridTrims = new System.Windows.Forms.DataGridView();
            this.tabPageTrimsProcess = new Syncfusion.Windows.Forms.Tools.TabPageAdv();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.DataGridTrimsProcess = new System.Windows.Forms.DataGridView();
            this.tabPageGatmentProcess = new Syncfusion.Windows.Forms.Tools.TabPageAdv();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.DataGridGarmentProcess = new System.Windows.Forms.DataGridView();
            this.tabPageCMTProcess = new Syncfusion.Windows.Forms.Tools.TabPageAdv();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.DatGridCMTProcess = new System.Windows.Forms.DataGridView();
            this.tabPageOtehrExp = new Syncfusion.Windows.Forms.Tools.TabPageAdv();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.DatGridOtherExp = new System.Windows.Forms.DataGridView();
            this.ChckBudget = new System.Windows.Forms.CheckBox();
            this.CheckProcess = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.CmbType = new System.Windows.Forms.ComboBox();
            this.CmbProcess = new System.Windows.Forms.ComboBox();
            this.txtOrderNo = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.grFront = new System.Windows.Forms.GroupBox();
            this.SfDataGridBudget = new Syncfusion.WinForms.DataGrid.SfDataGrid();
            this.grBack.SuspendLayout();
            this.grSearch.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridCommon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabControlAmentment)).BeginInit();
            this.tabControlAmentment.SuspendLayout();
            this.tabPageYarn.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridYan)).BeginInit();
            this.tabPageFabric.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridFabric)).BeginInit();
            this.tabPageTrimsPurchase.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridTrims)).BeginInit();
            this.tabPageTrimsProcess.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridTrimsProcess)).BeginInit();
            this.tabPageGatmentProcess.SuspendLayout();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridGarmentProcess)).BeginInit();
            this.tabPageCMTProcess.SuspendLayout();
            this.groupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DatGridCMTProcess)).BeginInit();
            this.tabPageOtehrExp.SuspendLayout();
            this.groupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DatGridOtherExp)).BeginInit();
            this.grFront.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SfDataGridBudget)).BeginInit();
            this.SuspendLayout();
            // 
            // SplitSave
            // 
            this.SplitSave.BackColor = System.Drawing.SystemColors.Control;
            this.SplitSave.BeforeTouchSize = new System.Drawing.Size(83, 32);
            this.SplitSave.DropDownItems.Add(this.toolstripBack);
            this.SplitSave.DropDownPosition = Syncfusion.Windows.Forms.Tools.Position.Bottom;
            this.SplitSave.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SplitSave.ForeColor = System.Drawing.Color.Black;
            this.SplitSave.Location = new System.Drawing.Point(930, 537);
            this.SplitSave.MinimumSize = new System.Drawing.Size(75, 23);
            this.SplitSave.Name = "SplitSave";
            splitButtonRenderer1.SplitButton = this.SplitSave;
            this.SplitSave.Renderer = splitButtonRenderer1;
            this.SplitSave.ShowDropDownOnButtonClick = false;
            this.SplitSave.Size = new System.Drawing.Size(83, 32);
            this.SplitSave.TabIndex = 38;
            this.SplitSave.Text = "Save";
            this.SplitSave.DropDowItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.SplitSave_DropDowItemClicked);
            // 
            // toolstripBack
            // 
            this.toolstripBack.Name = "toolstripBack";
            this.toolstripBack.Size = new System.Drawing.Size(23, 23);
            this.toolstripBack.Text = "Back";
            // 
            // SplitAdd
            // 
            this.SplitAdd.BackColor = System.Drawing.SystemColors.Control;
            this.SplitAdd.BeforeTouchSize = new System.Drawing.Size(79, 32);
            this.SplitAdd.DropDownItems.Add(this.toolstripEdit);
            this.SplitAdd.DropDownItems.Add(this.toolstripClose);
            this.SplitAdd.DropDownPosition = Syncfusion.Windows.Forms.Tools.Position.Bottom;
            this.SplitAdd.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SplitAdd.ForeColor = System.Drawing.Color.Black;
            this.SplitAdd.Location = new System.Drawing.Point(849, 536);
            this.SplitAdd.MinimumSize = new System.Drawing.Size(75, 23);
            this.SplitAdd.Name = "SplitAdd";
            splitButtonRenderer2.SplitButton = this.SplitAdd;
            this.SplitAdd.Renderer = splitButtonRenderer2;
            this.SplitAdd.ShowDropDownOnButtonClick = false;
            this.SplitAdd.Size = new System.Drawing.Size(79, 32);
            this.SplitAdd.TabIndex = 1;
            this.SplitAdd.Text = "Add";
            this.SplitAdd.DropDowItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.SplitAdd_DropDowItemClicked);
            this.SplitAdd.Click += new System.EventHandler(this.SplitAdd_Click);
            // 
            // toolstripEdit
            // 
            this.toolstripEdit.Name = "toolstripEdit";
            this.toolstripEdit.Size = new System.Drawing.Size(23, 23);
            this.toolstripEdit.Text = "Edit";
            // 
            // toolstripClose
            // 
            this.toolstripClose.Name = "toolstripClose";
            this.toolstripClose.Size = new System.Drawing.Size(23, 23);
            this.toolstripClose.Text = "Close";
            // 
            // grBack
            // 
            this.grBack.Controls.Add(this.grSearch);
            this.grBack.Controls.Add(this.tabControlAmentment);
            this.grBack.Controls.Add(this.ChckBudget);
            this.grBack.Controls.Add(this.CheckProcess);
            this.grBack.Controls.Add(this.label2);
            this.grBack.Controls.Add(this.label1);
            this.grBack.Controls.Add(this.SplitSave);
            this.grBack.Controls.Add(this.CmbType);
            this.grBack.Controls.Add(this.CmbProcess);
            this.grBack.Controls.Add(this.txtOrderNo);
            this.grBack.Controls.Add(this.label4);
            this.grBack.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grBack.Location = new System.Drawing.Point(11, 3);
            this.grBack.Name = "grBack";
            this.grBack.Size = new System.Drawing.Size(1028, 574);
            this.grBack.TabIndex = 0;
            this.grBack.TabStop = false;
            // 
            // grSearch
            // 
            this.grSearch.Controls.Add(this.btnSelect);
            this.grSearch.Controls.Add(this.DataGridCommon);
            this.grSearch.Controls.Add(this.btnHide);
            this.grSearch.Location = new System.Drawing.Point(790, 22);
            this.grSearch.Name = "grSearch";
            this.grSearch.Size = new System.Drawing.Size(346, 229);
            this.grSearch.TabIndex = 34;
            this.grSearch.TabStop = false;
            this.grSearch.Visible = false;
            // 
            // btnSelect
            // 
            this.btnSelect.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnSelect.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSelect.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnSelect.Location = new System.Drawing.Point(281, 197);
            this.btnSelect.Name = "btnSelect";
            this.btnSelect.Size = new System.Drawing.Size(61, 28);
            this.btnSelect.TabIndex = 396;
            this.btnSelect.Text = "Select";
            this.btnSelect.UseVisualStyleBackColor = false;
            this.btnSelect.Click += new System.EventHandler(this.BtnSelect_Click);
            // 
            // DataGridCommon
            // 
            this.DataGridCommon.AllowUserToAddRows = false;
            this.DataGridCommon.BackgroundColor = System.Drawing.Color.White;
            this.DataGridCommon.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridCommon.Location = new System.Drawing.Point(4, 12);
            this.DataGridCommon.MultiSelect = false;
            this.DataGridCommon.Name = "DataGridCommon";
            this.DataGridCommon.ReadOnly = true;
            this.DataGridCommon.RowHeadersVisible = false;
            this.DataGridCommon.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridCommon.Size = new System.Drawing.Size(336, 183);
            this.DataGridCommon.TabIndex = 0;
            this.DataGridCommon.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.DataGridCommon_CellMouseDoubleClick);
            // 
            // btnHide
            // 
            this.btnHide.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnHide.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHide.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnHide.Location = new System.Drawing.Point(6, 197);
            this.btnHide.Name = "btnHide";
            this.btnHide.Size = new System.Drawing.Size(65, 28);
            this.btnHide.TabIndex = 395;
            this.btnHide.Text = "Close";
            this.btnHide.UseVisualStyleBackColor = false;
            this.btnHide.Click += new System.EventHandler(this.BtnHide_Click);
            // 
            // tabControlAmentment
            // 
            this.tabControlAmentment.ActiveTabForeColor = System.Drawing.Color.Empty;
            this.tabControlAmentment.BeforeTouchSize = new System.Drawing.Size(1005, 485);
            this.tabControlAmentment.CloseButtonForeColor = System.Drawing.Color.Empty;
            this.tabControlAmentment.CloseButtonHoverForeColor = System.Drawing.Color.Empty;
            this.tabControlAmentment.CloseButtonPressedForeColor = System.Drawing.Color.Empty;
            this.tabControlAmentment.Controls.Add(this.tabPageYarn);
            this.tabControlAmentment.Controls.Add(this.tabPageFabric);
            this.tabControlAmentment.Controls.Add(this.tabPageTrimsPurchase);
            this.tabControlAmentment.Controls.Add(this.tabPageTrimsProcess);
            this.tabControlAmentment.Controls.Add(this.tabPageGatmentProcess);
            this.tabControlAmentment.Controls.Add(this.tabPageCMTProcess);
            this.tabControlAmentment.Controls.Add(this.tabPageOtehrExp);
            this.tabControlAmentment.InActiveTabForeColor = System.Drawing.Color.Empty;
            this.tabControlAmentment.Location = new System.Drawing.Point(17, 45);
            this.tabControlAmentment.Name = "tabControlAmentment";
            this.tabControlAmentment.SeparatorColor = System.Drawing.SystemColors.ControlDark;
            this.tabControlAmentment.ShowSeparator = false;
            this.tabControlAmentment.Size = new System.Drawing.Size(1005, 485);
            this.tabControlAmentment.TabIndex = 39;
            // 
            // tabPageYarn
            // 
            this.tabPageYarn.Controls.Add(this.groupBox1);
            this.tabPageYarn.Image = null;
            this.tabPageYarn.ImageSize = new System.Drawing.Size(16, 16);
            this.tabPageYarn.Location = new System.Drawing.Point(1, 27);
            this.tabPageYarn.Name = "tabPageYarn";
            this.tabPageYarn.ShowCloseButton = true;
            this.tabPageYarn.Size = new System.Drawing.Size(1002, 456);
            this.tabPageYarn.TabIndex = 1;
            this.tabPageYarn.Text = "Yarn";
            this.tabPageYarn.ThemesEnabled = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.DataGridYan);
            this.groupBox1.Location = new System.Drawing.Point(10, 5);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(985, 439);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // DataGridYan
            // 
            this.DataGridYan.AllowUserToAddRows = false;
            this.DataGridYan.BackgroundColor = System.Drawing.Color.White;
            this.DataGridYan.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridYan.Location = new System.Drawing.Point(6, 62);
            this.DataGridYan.Name = "DataGridYan";
            this.DataGridYan.RowHeadersVisible = false;
            this.DataGridYan.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridYan.Size = new System.Drawing.Size(973, 368);
            this.DataGridYan.TabIndex = 0;
            // 
            // tabPageFabric
            // 
            this.tabPageFabric.Controls.Add(this.groupBox2);
            this.tabPageFabric.Image = null;
            this.tabPageFabric.ImageSize = new System.Drawing.Size(16, 16);
            this.tabPageFabric.Location = new System.Drawing.Point(1, 27);
            this.tabPageFabric.Name = "tabPageFabric";
            this.tabPageFabric.ShowCloseButton = true;
            this.tabPageFabric.Size = new System.Drawing.Size(1002, 456);
            this.tabPageFabric.TabIndex = 2;
            this.tabPageFabric.Text = "Fabric";
            this.tabPageFabric.ThemesEnabled = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.DataGridFabric);
            this.groupBox2.Location = new System.Drawing.Point(9, 9);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(985, 439);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            // 
            // DataGridFabric
            // 
            this.DataGridFabric.AllowUserToAddRows = false;
            this.DataGridFabric.BackgroundColor = System.Drawing.Color.White;
            this.DataGridFabric.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridFabric.Location = new System.Drawing.Point(6, 62);
            this.DataGridFabric.Name = "DataGridFabric";
            this.DataGridFabric.RowHeadersVisible = false;
            this.DataGridFabric.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridFabric.Size = new System.Drawing.Size(973, 368);
            this.DataGridFabric.TabIndex = 0;
            // 
            // tabPageTrimsPurchase
            // 
            this.tabPageTrimsPurchase.Controls.Add(this.groupBox3);
            this.tabPageTrimsPurchase.Image = null;
            this.tabPageTrimsPurchase.ImageSize = new System.Drawing.Size(16, 16);
            this.tabPageTrimsPurchase.Location = new System.Drawing.Point(1, 27);
            this.tabPageTrimsPurchase.Name = "tabPageTrimsPurchase";
            this.tabPageTrimsPurchase.ShowCloseButton = true;
            this.tabPageTrimsPurchase.Size = new System.Drawing.Size(1002, 456);
            this.tabPageTrimsPurchase.TabIndex = 3;
            this.tabPageTrimsPurchase.Text = "Trims Purchase";
            this.tabPageTrimsPurchase.ThemesEnabled = false;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.btnTrimsPurchase);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Controls.Add(this.lblItemName);
            this.groupBox3.Controls.Add(this.lblCtgy);
            this.groupBox3.Controls.Add(this.txtQty);
            this.groupBox3.Controls.Add(this.txtRate);
            this.groupBox3.Controls.Add(this.txtItemName);
            this.groupBox3.Controls.Add(this.txtCtgy);
            this.groupBox3.Controls.Add(this.DataGridTrims);
            this.groupBox3.Location = new System.Drawing.Point(9, 9);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(985, 439);
            this.groupBox3.TabIndex = 1;
            this.groupBox3.TabStop = false;
            // 
            // btnTrimsPurchase
            // 
            this.btnTrimsPurchase.Location = new System.Drawing.Point(713, 44);
            this.btnTrimsPurchase.Name = "btnTrimsPurchase";
            this.btnTrimsPurchase.Size = new System.Drawing.Size(33, 25);
            this.btnTrimsPurchase.TabIndex = 31;
            this.btnTrimsPurchase.Text = "Ok";
            this.btnTrimsPurchase.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(624, 19);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(27, 15);
            this.label5.TabIndex = 30;
            this.label5.Text = "Qty";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(533, 19);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(32, 15);
            this.label3.TabIndex = 29;
            this.label3.Text = "Rate";
            // 
            // lblItemName
            // 
            this.lblItemName.AutoSize = true;
            this.lblItemName.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblItemName.Location = new System.Drawing.Point(230, 19);
            this.lblItemName.Name = "lblItemName";
            this.lblItemName.Size = new System.Drawing.Size(69, 15);
            this.lblItemName.TabIndex = 28;
            this.lblItemName.Text = "Item Name";
            // 
            // lblCtgy
            // 
            this.lblCtgy.AutoSize = true;
            this.lblCtgy.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCtgy.Location = new System.Drawing.Point(12, 19);
            this.lblCtgy.Name = "lblCtgy";
            this.lblCtgy.Size = new System.Drawing.Size(56, 15);
            this.lblCtgy.TabIndex = 27;
            this.lblCtgy.Text = "Category";
            // 
            // txtQty
            // 
            this.txtQty.Location = new System.Drawing.Point(624, 45);
            this.txtQty.Name = "txtQty";
            this.txtQty.Size = new System.Drawing.Size(89, 23);
            this.txtQty.TabIndex = 26;
            // 
            // txtRate
            // 
            this.txtRate.Location = new System.Drawing.Point(533, 45);
            this.txtRate.Name = "txtRate";
            this.txtRate.Size = new System.Drawing.Size(89, 23);
            this.txtRate.TabIndex = 25;
            // 
            // txtItemName
            // 
            this.txtItemName.BackColor = System.Drawing.Color.White;
            this.txtItemName.Location = new System.Drawing.Point(230, 45);
            this.txtItemName.Name = "txtItemName";
            this.txtItemName.ReadOnly = true;
            this.txtItemName.Size = new System.Drawing.Size(301, 23);
            this.txtItemName.TabIndex = 16;
            this.txtItemName.Click += new System.EventHandler(this.TxtItemName_Click);
            this.txtItemName.TextChanged += new System.EventHandler(this.txtItemName_TextChanged);
            // 
            // txtCtgy
            // 
            this.txtCtgy.Location = new System.Drawing.Point(12, 45);
            this.txtCtgy.Name = "txtCtgy";
            this.txtCtgy.Size = new System.Drawing.Size(217, 23);
            this.txtCtgy.TabIndex = 15;
            this.txtCtgy.Click += new System.EventHandler(this.TxtCtgy_Click);
            this.txtCtgy.TextChanged += new System.EventHandler(this.TxtCtgy_TextChanged);
            // 
            // DataGridTrims
            // 
            this.DataGridTrims.AllowUserToAddRows = false;
            this.DataGridTrims.BackgroundColor = System.Drawing.Color.White;
            this.DataGridTrims.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridTrims.Location = new System.Drawing.Point(6, 112);
            this.DataGridTrims.Name = "DataGridTrims";
            this.DataGridTrims.RowHeadersVisible = false;
            this.DataGridTrims.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridTrims.Size = new System.Drawing.Size(973, 318);
            this.DataGridTrims.TabIndex = 0;
            this.DataGridTrims.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.DataGridTrims_CellMouseDoubleClick);
            // 
            // tabPageTrimsProcess
            // 
            this.tabPageTrimsProcess.Controls.Add(this.groupBox4);
            this.tabPageTrimsProcess.Image = null;
            this.tabPageTrimsProcess.ImageSize = new System.Drawing.Size(16, 16);
            this.tabPageTrimsProcess.Location = new System.Drawing.Point(1, 27);
            this.tabPageTrimsProcess.Name = "tabPageTrimsProcess";
            this.tabPageTrimsProcess.ShowCloseButton = true;
            this.tabPageTrimsProcess.Size = new System.Drawing.Size(1002, 456);
            this.tabPageTrimsProcess.TabIndex = 4;
            this.tabPageTrimsProcess.Text = "Trims Process";
            this.tabPageTrimsProcess.ThemesEnabled = false;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.DataGridTrimsProcess);
            this.groupBox4.Location = new System.Drawing.Point(9, 9);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(985, 439);
            this.groupBox4.TabIndex = 1;
            this.groupBox4.TabStop = false;
            // 
            // DataGridTrimsProcess
            // 
            this.DataGridTrimsProcess.AllowUserToAddRows = false;
            this.DataGridTrimsProcess.BackgroundColor = System.Drawing.Color.White;
            this.DataGridTrimsProcess.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridTrimsProcess.Location = new System.Drawing.Point(6, 62);
            this.DataGridTrimsProcess.Name = "DataGridTrimsProcess";
            this.DataGridTrimsProcess.RowHeadersVisible = false;
            this.DataGridTrimsProcess.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridTrimsProcess.Size = new System.Drawing.Size(973, 368);
            this.DataGridTrimsProcess.TabIndex = 0;
            // 
            // tabPageGatmentProcess
            // 
            this.tabPageGatmentProcess.Controls.Add(this.groupBox5);
            this.tabPageGatmentProcess.Image = null;
            this.tabPageGatmentProcess.ImageSize = new System.Drawing.Size(16, 16);
            this.tabPageGatmentProcess.Location = new System.Drawing.Point(1, 27);
            this.tabPageGatmentProcess.Name = "tabPageGatmentProcess";
            this.tabPageGatmentProcess.ShowCloseButton = true;
            this.tabPageGatmentProcess.Size = new System.Drawing.Size(1002, 456);
            this.tabPageGatmentProcess.TabIndex = 5;
            this.tabPageGatmentProcess.Text = "Garment Process";
            this.tabPageGatmentProcess.ThemesEnabled = false;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.DataGridGarmentProcess);
            this.groupBox5.Location = new System.Drawing.Point(9, 9);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(985, 439);
            this.groupBox5.TabIndex = 1;
            this.groupBox5.TabStop = false;
            // 
            // DataGridGarmentProcess
            // 
            this.DataGridGarmentProcess.AllowUserToAddRows = false;
            this.DataGridGarmentProcess.BackgroundColor = System.Drawing.Color.White;
            this.DataGridGarmentProcess.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridGarmentProcess.Location = new System.Drawing.Point(6, 62);
            this.DataGridGarmentProcess.Name = "DataGridGarmentProcess";
            this.DataGridGarmentProcess.RowHeadersVisible = false;
            this.DataGridGarmentProcess.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridGarmentProcess.Size = new System.Drawing.Size(973, 368);
            this.DataGridGarmentProcess.TabIndex = 0;
            // 
            // tabPageCMTProcess
            // 
            this.tabPageCMTProcess.Controls.Add(this.groupBox6);
            this.tabPageCMTProcess.Image = null;
            this.tabPageCMTProcess.ImageSize = new System.Drawing.Size(16, 16);
            this.tabPageCMTProcess.Location = new System.Drawing.Point(1, 27);
            this.tabPageCMTProcess.Name = "tabPageCMTProcess";
            this.tabPageCMTProcess.ShowCloseButton = true;
            this.tabPageCMTProcess.Size = new System.Drawing.Size(1002, 456);
            this.tabPageCMTProcess.TabIndex = 6;
            this.tabPageCMTProcess.Text = "CMT Process";
            this.tabPageCMTProcess.ThemesEnabled = false;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.DatGridCMTProcess);
            this.groupBox6.Location = new System.Drawing.Point(9, 9);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(985, 439);
            this.groupBox6.TabIndex = 1;
            this.groupBox6.TabStop = false;
            // 
            // DatGridCMTProcess
            // 
            this.DatGridCMTProcess.AllowUserToAddRows = false;
            this.DatGridCMTProcess.BackgroundColor = System.Drawing.Color.White;
            this.DatGridCMTProcess.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DatGridCMTProcess.Location = new System.Drawing.Point(6, 62);
            this.DatGridCMTProcess.Name = "DatGridCMTProcess";
            this.DatGridCMTProcess.RowHeadersVisible = false;
            this.DatGridCMTProcess.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DatGridCMTProcess.Size = new System.Drawing.Size(973, 368);
            this.DatGridCMTProcess.TabIndex = 0;
            // 
            // tabPageOtehrExp
            // 
            this.tabPageOtehrExp.Controls.Add(this.groupBox7);
            this.tabPageOtehrExp.Image = null;
            this.tabPageOtehrExp.ImageSize = new System.Drawing.Size(16, 16);
            this.tabPageOtehrExp.Location = new System.Drawing.Point(1, 27);
            this.tabPageOtehrExp.Name = "tabPageOtehrExp";
            this.tabPageOtehrExp.ShowCloseButton = true;
            this.tabPageOtehrExp.Size = new System.Drawing.Size(1002, 456);
            this.tabPageOtehrExp.TabIndex = 7;
            this.tabPageOtehrExp.Text = "Other Expenses";
            this.tabPageOtehrExp.ThemesEnabled = false;
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.DatGridOtherExp);
            this.groupBox7.Location = new System.Drawing.Point(9, 9);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(985, 439);
            this.groupBox7.TabIndex = 1;
            this.groupBox7.TabStop = false;
            // 
            // DatGridOtherExp
            // 
            this.DatGridOtherExp.AllowUserToAddRows = false;
            this.DatGridOtherExp.BackgroundColor = System.Drawing.Color.White;
            this.DatGridOtherExp.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DatGridOtherExp.Location = new System.Drawing.Point(6, 62);
            this.DatGridOtherExp.Name = "DatGridOtherExp";
            this.DatGridOtherExp.RowHeadersVisible = false;
            this.DatGridOtherExp.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DatGridOtherExp.Size = new System.Drawing.Size(973, 368);
            this.DatGridOtherExp.TabIndex = 0;
            // 
            // ChckBudget
            // 
            this.ChckBudget.AutoSize = true;
            this.ChckBudget.Location = new System.Drawing.Point(821, 18);
            this.ChckBudget.Name = "ChckBudget";
            this.ChckBudget.Size = new System.Drawing.Size(63, 19);
            this.ChckBudget.TabIndex = 43;
            this.ChckBudget.Text = "Budget";
            this.ChckBudget.UseVisualStyleBackColor = true;
            // 
            // CheckProcess
            // 
            this.CheckProcess.AutoSize = true;
            this.CheckProcess.Location = new System.Drawing.Point(746, 19);
            this.CheckProcess.Name = "CheckProcess";
            this.CheckProcess.Size = new System.Drawing.Size(69, 19);
            this.CheckProcess.TabIndex = 42;
            this.CheckProcess.Text = "Process";
            this.CheckProcess.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(471, 20);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 15);
            this.label2.TabIndex = 41;
            this.label2.Text = "Type";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(186, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(48, 15);
            this.label1.TabIndex = 40;
            this.label1.Text = "Process";
            // 
            // CmbType
            // 
            this.CmbType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CmbType.FormattingEnabled = true;
            this.CmbType.Items.AddRange(new object[] {
            "New Item",
            "Modify Item",
            "Alternate Item"});
            this.CmbType.Location = new System.Drawing.Point(510, 16);
            this.CmbType.Name = "CmbType";
            this.CmbType.Size = new System.Drawing.Size(226, 23);
            this.CmbType.TabIndex = 36;
            this.CmbType.SelectedIndexChanged += new System.EventHandler(this.CmbType_SelectedIndexChanged);
            // 
            // CmbProcess
            // 
            this.CmbProcess.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CmbProcess.FormattingEnabled = true;
            this.CmbProcess.Items.AddRange(new object[] {
            "Yarn",
            "Fabric",
            "Trims Purchase",
            "Trims Process",
            "Garment Process",
            "CMT Process",
            "Other Expenses"});
            this.CmbProcess.Location = new System.Drawing.Point(240, 16);
            this.CmbProcess.Name = "CmbProcess";
            this.CmbProcess.Size = new System.Drawing.Size(226, 23);
            this.CmbProcess.TabIndex = 35;
            // 
            // txtOrderNo
            // 
            this.txtOrderNo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.txtOrderNo.Location = new System.Drawing.Point(70, 16);
            this.txtOrderNo.Name = "txtOrderNo";
            this.txtOrderNo.Size = new System.Drawing.Size(108, 23);
            this.txtOrderNo.TabIndex = 8;
            this.txtOrderNo.Click += new System.EventHandler(this.TxtOrderNo_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(11, 20);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(59, 15);
            this.label4.TabIndex = 7;
            this.label4.Text = "Order No";
            // 
            // grFront
            // 
            this.grFront.Controls.Add(this.SplitAdd);
            this.grFront.Controls.Add(this.SfDataGridBudget);
            this.grFront.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grFront.Location = new System.Drawing.Point(11, 3);
            this.grFront.Name = "grFront";
            this.grFront.Size = new System.Drawing.Size(1028, 574);
            this.grFront.TabIndex = 42;
            this.grFront.TabStop = false;
            // 
            // SfDataGridBudget
            // 
            this.SfDataGridBudget.AccessibleName = "Table";
            this.SfDataGridBudget.Location = new System.Drawing.Point(6, 16);
            this.SfDataGridBudget.Name = "SfDataGridBudget";
            this.SfDataGridBudget.Size = new System.Drawing.Size(1016, 516);
            this.SfDataGridBudget.TabIndex = 0;
            this.SfDataGridBudget.Text = "sfDataGrid1";
            // 
            // FrmBudgetAmenment
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1104, 583);
            this.Controls.Add(this.grFront);
            this.Controls.Add(this.grBack);
            this.Name = "FrmBudgetAmenment";
            this.Text = "FrmBudgetAmenment";
            this.Load += new System.EventHandler(this.FrmBudgetAmenment_Load);
            this.grBack.ResumeLayout(false);
            this.grBack.PerformLayout();
            this.grSearch.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DataGridCommon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabControlAmentment)).EndInit();
            this.tabControlAmentment.ResumeLayout(false);
            this.tabPageYarn.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DataGridYan)).EndInit();
            this.tabPageFabric.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DataGridFabric)).EndInit();
            this.tabPageTrimsPurchase.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridTrims)).EndInit();
            this.tabPageTrimsProcess.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DataGridTrimsProcess)).EndInit();
            this.tabPageGatmentProcess.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DataGridGarmentProcess)).EndInit();
            this.tabPageCMTProcess.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DatGridCMTProcess)).EndInit();
            this.tabPageOtehrExp.ResumeLayout(false);
            this.groupBox7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DatGridOtherExp)).EndInit();
            this.grFront.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.SfDataGridBudget)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grBack;
        private System.Windows.Forms.TextBox txtOrderNo;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox CmbType;
        private System.Windows.Forms.ComboBox CmbProcess;
        private System.Windows.Forms.GroupBox grSearch;
        private System.Windows.Forms.Button btnSelect;
        private System.Windows.Forms.DataGridView DataGridCommon;
        private System.Windows.Forms.Button btnHide;
        private Syncfusion.Windows.Forms.Tools.SplitButton SplitSave;
        private Syncfusion.Windows.Forms.Tools.toolstripitem toolstripBack;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private Syncfusion.Windows.Forms.Tools.TabControlAdv tabControlAmentment;
        private Syncfusion.Windows.Forms.Tools.TabPageAdv tabPageYarn;
        private Syncfusion.Windows.Forms.Tools.TabPageAdv tabPageFabric;
        private Syncfusion.Windows.Forms.Tools.TabPageAdv tabPageTrimsPurchase;
        private Syncfusion.Windows.Forms.Tools.TabPageAdv tabPageTrimsProcess;
        private Syncfusion.Windows.Forms.Tools.TabPageAdv tabPageGatmentProcess;
        private Syncfusion.Windows.Forms.Tools.TabPageAdv tabPageCMTProcess;
        private Syncfusion.Windows.Forms.Tools.TabPageAdv tabPageOtehrExp;
        private System.Windows.Forms.GroupBox grFront;
        private Syncfusion.WinForms.DataGrid.SfDataGrid SfDataGridBudget;
        private Syncfusion.Windows.Forms.Tools.SplitButton SplitAdd;
        private Syncfusion.Windows.Forms.Tools.toolstripitem toolstripEdit;
        private Syncfusion.Windows.Forms.Tools.toolstripitem toolstripClose;
        private System.Windows.Forms.CheckBox ChckBudget;
        private System.Windows.Forms.CheckBox CheckProcess;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView DataGridYan;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DataGridView DataGridFabric;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.DataGridView DataGridTrims;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.DataGridView DataGridTrimsProcess;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.DataGridView DataGridGarmentProcess;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.DataGridView DatGridCMTProcess;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.DataGridView DatGridOtherExp;
        private System.Windows.Forms.TextBox txtItemName;
        private System.Windows.Forms.TextBox txtCtgy;
        private System.Windows.Forms.TextBox txtQty;
        private System.Windows.Forms.TextBox txtRate;
        private System.Windows.Forms.Label lblCtgy;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblItemName;
        private System.Windows.Forms.Button btnTrimsPurchase;
    }
}