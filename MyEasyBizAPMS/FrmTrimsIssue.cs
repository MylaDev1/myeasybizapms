﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Drawing.Imaging;
using System.Configuration;
using System.Data.SqlClient;
using System.Diagnostics;
using excel = Microsoft.Office.Interop.Excel;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;

namespace MyEasyBizAPMS
{
    public partial class FrmTrimsIssue : Form
    {
        private CrystalDecisions.Windows.Forms.CrystalReportViewer Cryview;

        ReportDocument doc = new ReportDocument();
        public FrmTrimsIssue()
        {
            this.BackColor = Color.White; ;
            InitializeComponent();
        }
        string uid = "";
        int mode = 0;
        string tpuid = "";
        SqlConnection conn = new SqlConnection(GeneralParameters.ConnectionString);
        SqlCommand qur = new SqlCommand();
        private DataRow doc1;
        DataTable Docno = new DataTable();
        BindingSource bs = new BindingSource();

        DataTable Docno1 = new DataTable();
        BindingSource bsc = new BindingSource();
        BindingSource bsp = new BindingSource();
        BindingSource bsserial = new BindingSource();
        BindingSource IN = new BindingSource();
        BindingSource OUT = new BindingSource();
        BindingSource bsFabric = new BindingSource();
        SQLDBHelper db = new SQLDBHelper();
   
        int Fillid;
        BindingSource bsParty = new BindingSource();
        string str1key;
        public int SelectId = 0;
        private void txtdcno_TextChanged(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            mode = 1;
            Genpan.Visible = false;

            panadd.Visible = false;
            Genclass.h = 0;
            Genclass.ClearTextBox(this, Editpnl);
            Genclass.STR = "";
            Genclass.Gendocno();
            txtgrn.Text = Genclass.ST;
            Editpnl.Visible = true;
            lkppnl.Visible = false;
            Genclass.sum1 = 0;
            Genclass.sum2 = 0;
            HFIT.Refresh();
            HFIT.DataSource = null;
            HFIT.Rows.Clear();
            txtgrn.Tag = 0;

            HFGP1.Refresh();
            HFGP1.DataSource = null;
            HFGP1.Rows.Clear();

            conn.Close();
            conn.Open();
            Docno.Clear();

            button13.Visible = false;
            if (Docno.Columns.Count == 0)
            {
                Docno.Columns.Add("uid");
                Docno.Columns.Add("Docno");
            }
            conn.Close();
            conn.Open();


        }

        private void butedit_Click(object sender, EventArgs e)
        {



            mode = 2;
            panadd.Visible = false;
            Genpan.Visible = false;
            Editpnl.Visible = true;
            lkppnl.Visible = false;
     
            type1();
            int i = HFGP.SelectedCells[0].RowIndex;
            uid = HFGP.Rows[i].Cells[0].Value.ToString();
            txtgrnid.Text = HFGP.Rows[i].Cells[0].Value.ToString();
            txtgrn.Tag = Convert.ToInt32(HFGP.Rows[i].Cells[0].Value.ToString());
            txtgrn.Text = HFGP.Rows[i].Cells[1].Value.ToString();
            dtpgrndt.Text = HFGP.Rows[i].Cells[2].Value.ToString();
            txtcolor.Text = HFGP.Rows[i].Cells[3].Value.ToString();
            txtcolor.SelectedValue = HFGP.Rows[i].Cells[4].Value.ToString();
            txtnarration.Text = HFGP.Rows[i].Cells[5].Value.ToString();


            //Chkedtact.Checked = true;
            HFIT.Refresh();
            HFIT.DataSource = null;
            HFIT.Rows.Clear();

 

            Titlep();
            load();

        }
        private void load()
        {
            Genclass.strsql = "SP_GETTRIMSISSUEEDIT " + txtgrnid.Text + " ";
            Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
            SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
            DataTable tap1 = new DataTable();
            aptr1.Fill(tap1);

            Genclass.sum1 = 0;


            for (int k = 0; k < tap1.Rows.Count; k++)
            {
            
                var index = HFIT.Rows.Add();
       
                HFIT.Rows[index].Cells[0].Value = tap1.Rows[k]["HEADID"].ToString();

                HFIT.Rows[index].Cells[1].Value = tap1.Rows[k]["SOCNO"].ToString();


                HFIT.Rows[index].Cells[2].Value = tap1.Rows[k]["ITEMNAME"].ToString();

                HFIT.Rows[index].Cells[3].Value = tap1.Rows[k]["ITEMID"].ToString();

                HFIT.Rows[index].Cells[4].Value = tap1.Rows[k]["UOM"].ToString();


                HFIT.Rows[index].Cells[5].Value = tap1.Rows[k]["QTY"].ToString();
                HFIT.Rows[index].Cells[6].Value = tap1.Rows[k]["UID"].ToString();
              
            }
        }

        private void FrmTrimsIssue_Load(object sender, EventArgs e)
        {
            qur.Connection = conn;
            //Left = (MdiParent.ClientRectangle.Width - Width) / 3;
            //Top = (MdiParent.ClientRectangle.Height - Height) / 3;
            Genclass.buttonstyleform(this);
            Genclass.buttonstylepanel(panadd);
            panadd.Visible = true;
            Genpan.Visible = true;
            Editpnl.Visible = false;

            this.HFGP.DefaultCellStyle.Font = new Font("calibri", 10);
            this.HFGP.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 10, FontStyle.Bold);
            this.HFIT.DefaultCellStyle.Font = new Font("calibri", 10);
            this.HFIT.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 10, FontStyle.Bold);

            this.HFGP1.DefaultCellStyle.Font = new Font("calibri", 10);
            this.HFGP1.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 10, FontStyle.Bold);

            this.HFGP2.DefaultCellStyle.Font = new Font("calibri", 10);
            this.HFGP2.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 10, FontStyle.Bold);

            HFIT.RowHeadersVisible = false;
            HFGP.RowHeadersVisible = false;
            HFGP2.RowHeadersVisible = false;
            HFGP1.RowHeadersVisible = false;




            HFGP2.EnableHeadersVisualStyles = false;
            HFGP2.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.GradientActiveCaption;

            HFIT.EnableHeadersVisualStyles = false;
            HFIT.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.GradientActiveCaption;
            HFGP1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            HFGP1.EnableHeadersVisualStyles = false;
            HFGP1.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.GradientActiveCaption;

            HFGP.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            HFGP.EnableHeadersVisualStyles = false;
            HFGP.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.GradientActiveCaption;

            Genclass.Dtype = 1440;

            dtpfnt.Format = DateTimePickerFormat.Custom;
            dtpfnt.CustomFormat = "dd/MM/yyyy";
            dtpgrndt.Format = DateTimePickerFormat.Custom;
            dtpgrndt.CustomFormat = "dd/MM/yyyy";
            //////dtpsupp.Format = DateTimePickerFormat.Custom;
            //////dtpsupp.CustomFormat = "dd/MM/yyyy";
            chkact.Checked = true;


            LoadGetJobCard(1);
            Titlep();
            type1();
        }
        public void type1()
        {
                    string Query = "Select guid,generalname from generalm  where typemuid=20";
                    DataTable data = db.GetDataWithoutParam(CommandType.Text, Query, conn);
                    DataRow dataRow = data.NewRow();
                    //dataRow["Colour"] = "Blank";
                    //data.Rows.Add(dataRow);
                    txtcolor.DisplayMember = "generalname";
                    txtcolor.ValueMember = "guid";
                    txtcolor.DataSource = data;
                
            

        }
        private void Titlep()
        {
            HFIT.AutoGenerateColumns = false;
            HFIT.Refresh();
            HFIT.DataSource = null;
            HFIT.Rows.Clear();
            HFIT.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            HFIT.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            HFIT.ColumnCount = 7;
            HFIT.Columns[0].Name = "headid";
            HFIT.Columns[1].Name = "Socno";
            HFIT.Columns[2].Name = "Item";
            HFIT.Columns[3].Name = "itemid";
            HFIT.Columns[4].Name = "uom";
            HFIT.Columns[5].Name = "Qty";
            HFIT.Columns[6].Name = "uid";
           
            //HFIT.EditMode = DataGridViewEditMode.EditOnKeystroke;




            HFIT.Columns[2].Width = 400;

            HFIT.Columns[4].Width = 120;
            HFIT.Columns[5].Width = 100;
            HFIT.Columns[6].Visible = false;
 
            HFIT.Columns[1].Width = 120;
            HFIT.Columns[0].Visible = false;
            HFIT.Columns[3].Visible = false;
          
        }
        protected DataTable LoadGetJobCard(int tag)
        {

            int SP;
            chkact.Checked = true;
            if (chkact.Checked == true)
            {
                SP = 1;

            }
            else
            {
                SP = 0;

            }


            DataTable dt = new DataTable();
            try
            {

                SqlParameter[] para = {
                    new SqlParameter("@active",SP),



                };

                dt = db.GetData(CommandType.StoredProcedure, "SP_GETTRIMSIISUE", para);


                LoadDataTable(dt);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        protected void LoadDataTable(DataTable dt)
        {
            try
            {
                HFGP.DataSource = null;
                HFGP.AutoGenerateColumns = false;
                HFGP.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
                HFGP.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
                HFGP.ColumnCount = 6;
                HFGP.Columns[0].Name = "Uid";
                HFGP.Columns[0].HeaderText = "Uid";
                HFGP.Columns[0].DataPropertyName = "Uid";

                HFGP.Columns[1].Name = "Docno";
                HFGP.Columns[1].HeaderText = "Docno";
                HFGP.Columns[1].DataPropertyName = "Docno";

                HFGP.Columns[2].Name = "Docdate";
                HFGP.Columns[2].HeaderText = "Docdate";
                HFGP.Columns[2].DataPropertyName = "Docdate";

                HFGP.Columns[3].Name = "Department";
                HFGP.Columns[3].HeaderText = "Department";
                HFGP.Columns[3].DataPropertyName = "Department";

                HFGP.Columns[4].Name = "DEPARTMENTID";
                HFGP.Columns[4].HeaderText = "DEPARTMENTID";
                HFGP.Columns[4].DataPropertyName = "DEPARTMENTID";


                HFGP.Columns[5].Name = "narration";
                HFGP.Columns[5].HeaderText = "narration";
                HFGP.Columns[5].DataPropertyName = "narration";




                bs.DataSource = dt;

                HFGP.DataSource = bs;
                HFGP.Columns[5].Visible = false;

                HFGP.Columns[0].Visible = false;

       
                HFGP.Columns[1].Width = 100;
                HFGP.Columns[2].Width = 100;
                HFGP.Columns[3].Width = 500;
                HFGP.Columns[4].Visible = false;





            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void txtwo_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bsFabric.Filter = string.Format("Socno LIKE '%{0}%' ", txtwo.Text);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtwo_Click(object sender, EventArgs e)
        {
            Genclass.type = 1;

            DataTable dt = getParty();
            bsFabric.DataSource = dt;
            FillGrid1(dt, 1);
            Point loc = FindLocation(txtwo);
            grNewSearch.Location = new Point(loc.X, loc.Y + 20);
            grNewSearch.Visible = true;
        }
        private Point FindLocation(Control ctrl)
        {
            if (ctrl.Parent is Form)
                return ctrl.Location;
            else
            {
                Point p = FindLocation(ctrl.Parent);
                p.X += ctrl.Location.X;
                p.Y += ctrl.Location.Y;
                return p;
            }
        }

        protected void FillGrid1(DataTable dt, int Fillid)
        {
            try
            {
                DataGridCommonNew.DataSource = null;
                DataGridCommonNew.AutoGenerateColumns = false;
                DataGridCommonNew.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
                DataGridCommonNew.DefaultCellStyle.WrapMode = DataGridViewTriState.True;

                DataGridCommonNew.ColumnCount = 3;
                DataGridCommonNew.Columns[0].Name = "Uid";
                DataGridCommonNew.Columns[0].HeaderText = "Uid";
                DataGridCommonNew.Columns[0].DataPropertyName = "Uid";
                DataGridCommonNew.Columns[1].Name = "Socno";
                DataGridCommonNew.Columns[1].HeaderText = "Socno";
                DataGridCommonNew.Columns[1].DataPropertyName = "Socno";
                DataGridCommonNew.Columns[1].Width = 100;
                DataGridCommonNew.Columns[2].Name = "WORKORDER";
                DataGridCommonNew.Columns[2].HeaderText = "WORKORDER";
                DataGridCommonNew.Columns[2].DataPropertyName = "WORKORDER";
                DataGridCommonNew.Columns[2].Visible = false;
                DataGridCommonNew.DataSource = bsFabric;
                DataGridCommonNew.Columns[0].Visible = false;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        protected void FillGrid3(DataTable dt, int Fillid)
        {
            try
            {
                DataGridCommonNew.DataSource = null;
                DataGridCommonNew.AutoGenerateColumns = false;
                DataGridCommonNew.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
                DataGridCommonNew.DefaultCellStyle.WrapMode = DataGridViewTriState.True;

                DataGridCommonNew.ColumnCount = 2;
                DataGridCommonNew.Columns[0].Name = "Uid";
                DataGridCommonNew.Columns[0].HeaderText = "Uid";
                DataGridCommonNew.Columns[0].DataPropertyName = "Uid";
                DataGridCommonNew.Columns[1].Name = "Name";
                DataGridCommonNew.Columns[1].HeaderText = "Name";
                DataGridCommonNew.Columns[1].DataPropertyName = "Name";
                DataGridCommonNew.Columns[1].Width = 200;
               
                DataGridCommonNew.DataSource = bs;
                DataGridCommonNew.Columns[0].Visible = false;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        protected DataTable getParty()
        {
            DataTable dt = new DataTable();
            try
            {


                if (Genclass.type == 1)
                {
                    dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "SP_GETSOCNOFRONGRNSTOCK", conn);
                    bsFabric.DataSource = dt;
                }
                else if (Genclass.type == 2)
                {

                    SqlParameter[] para = { new SqlParameter("@socno", Convert.ToString(txtwo.Text)) };

             
                    dt = db.getDataWithParameterCmdType(CommandType.StoredProcedure, "SP_GETITEMFROMGENSTOCK", para);
                    bsc.DataSource = dt;

                }
                else if (Genclass.type == 3)
                {
                    dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "SP_GETPARTYSUPPLIER1", conn);
                    bs.DataSource = dt;

                }
                else if (Genclass.type == 4)
                {


                    SqlParameter[] para = { new SqlParameter("@socno", Convert.ToString(txtwo.Text)) };
                    
             
                    
                    dt = db.getDataWithParameterCmdType(CommandType.StoredProcedure, "SP_GETRATEAPPREFNOprocess", para);
                    bsp.DataSource = dt;

                }


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            return dt;
        }

        private void btnSelect_Click(object sender, EventArgs e)
        {
            try
            {
                SelectId = 1;
                if (Genclass.type == 1)
                {
                    int Index = DataGridCommonNew.SelectedCells[0].RowIndex;
                    txtwo.Text = DataGridCommonNew.Rows[Index].Cells[1].Value.ToString();
                    txtwo.Tag = DataGridCommonNew.Rows[Index].Cells[0].Value.ToString();
                    //textBox2.Text = DataGridCommonNew.Rows[Index].Cells[2].Value.ToString();
                  
                }
                else if (Genclass.type == 3)
                {
                    int Index = DataGridCommonNew.SelectedCells[0].RowIndex;
                    textBox3.Text = DataGridCommonNew.Rows[Index].Cells[1].Value.ToString();
                    textBox3.Tag = DataGridCommonNew.Rows[Index].Cells[0].Value.ToString();

                }

                SelectId = 0;
                grNewSearch.Visible = false;
                SelectId = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }

        private void DataGridCommonNew_KeyDown(object sender, KeyEventArgs e)
        {

        }

        private void DataGridCommonNew_Click(object sender, EventArgs e)
        {
            SelectId = 1;
            if (Genclass.type == 1)
            {
                int Index = DataGridCommonNew.SelectedCells[0].RowIndex;
                txtwo.Text = DataGridCommonNew.Rows[Index].Cells[1].Value.ToString();
                txtwo.Tag = DataGridCommonNew.Rows[Index].Cells[0].Value.ToString();
                //textBox2.Text = DataGridCommonNew.Rows[Index].Cells[2].Value.ToString();
             
            }
            else if (Genclass.type == 3)
            {
                int Index = DataGridCommonNew.SelectedCells[0].RowIndex;
                textBox3.Text = DataGridCommonNew.Rows[Index].Cells[1].Value.ToString();
                textBox3.Tag = DataGridCommonNew.Rows[Index].Cells[0].Value.ToString();

            }

            SelectId = 0;
            grNewSearch.Visible = false;
            SelectId = 0;

            SelectId = 0;
            grNewSearch.Visible = false;
            SelectId = 0;
        }

        private void txtdcqty_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bsc.Filter = string.Format("Itemname LIKE '%{0}%' ", txtdcqty.Text);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtdcqty_Click(object sender, EventArgs e)
        {
            Genclass.type = 2;

            DataTable dt = getParty();
            bsc.DataSource = dt;
            FillGrid2(dt, 1);
            Point loc = FindLocation(txtdcqty);
            lkppnl.Location = new Point(loc.X, loc.Y + 20);
            lkppnl.Visible = true;
        }
        protected void FillGrid2(DataTable dt, int FillId)
        {
            try
            {
                HFGP2.DataSource = null;
                HFGP2.AutoGenerateColumns = false;


                HFGP2.ColumnCount = 4;
                HFGP2.Columns[0].Name = "ITEMID";
                HFGP2.Columns[0].HeaderText = "ITEMID";
                HFGP2.Columns[0].DataPropertyName = "ITEMID";

                HFGP2.Columns[1].Name = "Itemname";
                HFGP2.Columns[1].HeaderText = "ItemName";
                HFGP2.Columns[1].DataPropertyName = "Itemname";
                HFGP2.Columns[1].Width = 300;


                HFGP2.Columns[2].Name = "UOM";
                HFGP2.Columns[2].HeaderText = "UOM";
                HFGP2.Columns[2].DataPropertyName = "UOM";
                HFGP2.Columns[2].Width = 100;


                HFGP2.Columns[3].Name = "Qty";
                HFGP2.Columns[3].HeaderText = "Qty";
                HFGP2.Columns[3].DataPropertyName = "Qty";
                HFGP2.Columns[3].Width = 100;



                HFGP2.DataSource = bsc;
                HFGP2.Columns[0].Visible = false;
            

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        protected void FillGrid4(DataTable dt, int FillId)
        {
            try
            {
                HFGP2.DataSource = null;
                HFGP2.AutoGenerateColumns = false;


                HFGP2.ColumnCount = 8;


                HFGP2.Columns[0].Name = "ITEMID";
                HFGP2.Columns[0].HeaderText = "ITEMID";
                HFGP2.Columns[0].DataPropertyName = "ITEMID";
                HFGP2.Columns[1].Name = "PROCESS";
                HFGP2.Columns[1].HeaderText = "PROCESS";
                HFGP2.Columns[1].DataPropertyName = "PROCESS";
                HFGP2.Columns[1].Width = 100;
           

                HFGP2.Columns[2].Name = "Itemname";
                HFGP2.Columns[2].HeaderText = "ItemName";
                HFGP2.Columns[2].DataPropertyName = "Itemname";
                HFGP2.Columns[2].Width = 300;


                HFGP2.Columns[3].Name = "COLOUR";
                HFGP2.Columns[3].HeaderText = "COLOUR";
                HFGP2.Columns[3].DataPropertyName = "COLOUR";
                HFGP2.Columns[3].Width = 100;


                HFGP2.Columns[4].Name = "UID";
                HFGP2.Columns[4].HeaderText = "UID";
                HFGP2.Columns[4].DataPropertyName = "UID";

                HFGP2.Columns[5].Name = "qty";
                HFGP2.Columns[5].HeaderText = "qty";
                HFGP2.Columns[5].DataPropertyName = "qty";
                HFGP2.Columns[5].Width = 100;

                HFGP2.Columns[6].Name = "UOM";
                HFGP2.Columns[6].HeaderText = "UOM";
                HFGP2.Columns[6].DataPropertyName = "UOM";

                HFGP2.Columns[7].Name = "seqno";
                HFGP2.Columns[7].HeaderText = "seqno";
                HFGP2.Columns[7].DataPropertyName = "seqno";

                HFGP2.DataSource = bsp;
                HFGP2.Columns[0].Visible = false;
                HFGP2.Columns[4].Visible = false;
                HFGP2.Columns[6].Visible = false;
                HFGP2.Columns[7].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        private void txtdcqty_KeyDown(object sender, KeyEventArgs e)
        {
           
        }

        private void button10_Click(object sender, EventArgs e)
        {
            try
            {
                SelectId = 1;

                if (Genclass.type == 2)
                {
                    int Index = HFGP2.SelectedCells[0].RowIndex;

                    txtdcqty.Text = HFGP2.Rows[Index].Cells[1].Value.ToString();
                    txtdcqty.Tag = HFGP2.Rows[Index].Cells[0].Value.ToString();
                    txtrate.Text = HFGP2.Rows[Index].Cells[2].Value.ToString();
                    txtqty.Text = HFGP2.Rows[Index].Cells[3].Value.ToString();

                }
              
               


                lkppnl.Visible = false;
                SelectId = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void HFGP2_Click(object sender, EventArgs e)
        {
            SelectId = 1;
            if (Genclass.type == 2)
            {
                int Index = HFGP2.SelectedCells[0].RowIndex;

                txtdcqty.Text = HFGP2.Rows[Index].Cells[1].Value.ToString();
                txtdcqty.Tag = HFGP2.Rows[Index].Cells[0].Value.ToString();
                txtrate.Text = HFGP2.Rows[Index].Cells[2].Value.ToString();
                txtqty.Text = HFGP2.Rows[Index].Cells[3].Value.ToString();


            }




            lkppnl.Visible = false;
            SelectId = 0;
        }

        private void button11_Click(object sender, EventArgs e)
        {
            lkppnl.Visible = false;
        }

        private void button9_Click(object sender, EventArgs e)
        {
            grNewSearch.Visible = false;
        }

        private void btnsave_Click(object sender, EventArgs e)
        {
            conn.Close();
            conn.Open();
            Genclass.Dtype = 1440;

            if (HFIT.RowCount <= 1)
            {
                MessageBox.Show("Enter the details");
                return;

            }




            SqlParameter[] para ={
                

                    new SqlParameter("@uid",txtgrn.Tag),
                    new SqlParameter("@DOCNO",txtgrn.Text),
                    new SqlParameter("@DOCDATE",Convert.ToDateTime(dtpgrndt.Text)),
                    new SqlParameter("@DEPARTMENTID",txtcolor.SelectedValue),
                    new SqlParameter("@doctypeid",Genclass.Dtype),
                    new SqlParameter("@narration",txtnarration.Text),

            };
            DataTable dataTable = db.GetDataWithParam(CommandType.StoredProcedure, "SP_TRIMSISSUEM", para, conn);
            int ResponseCode = Convert.ToInt32(dataTable.Rows[0]["ResponseCode"].ToString());
            int ReturnId = Convert.ToInt32(dataTable.Rows[0]["ReturnId"].ToString());


            for (int i = 0; i < HFIT.RowCount - 1; i++)
            {

                conn.Close();
                conn.Open();

                SqlParameter[] para1 ={


            new SqlParameter("@uid", HFIT.Rows[i].Cells[6].Value),
                   new SqlParameter("@HEADID",ReturnId),
                    new SqlParameter("@SOCNO",HFIT.Rows[i].Cells[1].Value),
                    new SqlParameter("@ITEMNAME", HFIT.Rows[i].Cells[2].Value),
            
                    new SqlParameter("@UOM", HFIT.Rows[i].Cells[4].Value),

                          new SqlParameter("@QTY", HFIT.Rows[i].Cells[5].Value),
                       
                    new SqlParameter("@itemid", HFIT.Rows[i].Cells[3].Value),


       

                };

                db.ExecuteNonQuery(CommandType.StoredProcedure, "sp_TRIMSISSUED", para1, conn);
            }


            

                if (mode == 1)
            {
                qur.CommandText = "update doctypem set lastno= lastno + 1 where doctypeid=" + Genclass.Dtype + "  and finyear='19-20'";
                qur.ExecuteNonQuery();


            }
            MessageBox.Show("Records Saved");
            Editpnl.Visible = false;
            Genpan.Visible = true;
            panadd.Visible = true;
            LoadGetJobCard(1);
        }

        private void btnaddrcan_Click(object sender, EventArgs e)
        {
            Editpnl.Visible = false;
            Genpan.Visible = true;
            panadd.Visible = true;
            LoadGetJobCard(1);
        }

        private void button1_Click(object sender, EventArgs e)
        {

           
            var index = HFIT.Rows.Add();

            HFIT.Rows[index].Cells[0].Value ="0";
            HFIT.Rows[index].Cells[1].Value = txtwo.Text;
            HFIT.Rows[index].Cells[2].Value = txtdcqty.Text;
            HFIT.Rows[index].Cells[3].Value = txtdcqty.Tag;
            HFIT.Rows[index].Cells[4].Value = txtrate.Text;
            HFIT.Rows[index].Cells[5].Value = txtqty.Text;
            HFIT.Rows[index].Cells[6].Value = 0;


            txtwo.Text = string.Empty;
            txtdcqty.Text = string.Empty;
            txtqty.Text = string.Empty;
            txtrate.Text = string.Empty;


        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    if (textBox3.Text != "")
                    {
                        bs.Filter = string.Format("Name LIKE '%{0}%' ", textBox3.Text);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void textBox3_Click(object sender, EventArgs e)
        {
            Genclass.type = 3;

            DataTable dt = getParty();
            bs.DataSource = dt;
            FillGrid3(dt, 1);
            Point loc = FindLocation(textBox3);
            grNewSearch.Location = new Point(loc.X, loc.Y + 20);
            grNewSearch.Visible = true;
        }

        private void DataGridCommonNew_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void txtprocess_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bsp.Filter = string.Format("process LIKE '%{0}%' ", txtprocess.Text);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void cbopono_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbopono.Text != "")
            {
                Genclass.type = 2;
                DataTable dt = getParty();
                bsc.DataSource = dt;
                FillGrid2(dt, 1);
                Point loc = FindLocation(txtprocess);
                lkppnl.Location = new Point(loc.X, loc.Y + 20);
                lkppnl.Visible = true;
            }
        }

        private void txtprocess_Click(object sender, EventArgs e)
        {
            Genclass.type = 4;

            DataTable dt = getParty();
            bsp.DataSource = dt;
            FillGrid4(dt, 1);
            Point loc = FindLocation(txtprocess);
            lkppnl.Location = new Point(loc.X, loc.Y + 20);
            lkppnl.Visible = true;
        }

        private void button22_Click(object sender, EventArgs e)
        {

        }

        private void buttnext1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void label9_Click(object sender, EventArgs e)
        {

        }

        private void HFGP2_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void txtqty_TextChanged(object sender, EventArgs e)
        {

        }

        private void button12_Click(object sender, EventArgs e)
        {
             string message = "Are you sure to cancel this PO ?";
            string caption = "Dilama";
            MessageBoxButtons buttons = MessageBoxButtons.YesNo;
            DialogResult result;

            result = MessageBox.Show(message, caption, buttons);

            if (result == System.Windows.Forms.DialogResult.Yes)
            {

                int i = HFGP.SelectedCells[0].RowIndex;
                uid = HFGP.Rows[i].Cells[0].Value.ToString();

                conn.Close();
                conn.Open();

                qur.CommandText = "delete  from  TRIMSISSUEd  where headid=" + HFGP.Rows[i].Cells[0].Value.ToString() + "";
                qur.ExecuteNonQuery();
                qur.CommandText = "delete  from  TRIMSISSUEM  where uid=" + HFGP.Rows[i].Cells[0].Value.ToString() + "";

                qur.ExecuteNonQuery();

                MessageBox.Show("Deleted");

            }
            LoadGetJobCard(1);
        }

        private void HFIT_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (HFIT.CurrentRow.Cells[0].Value != null && HFIT.CurrentCell.ColumnIndex == 4)
            {
                if (HFIT.CurrentCell.ColumnIndex == 4)

                {
                    DataGridViewCell cell = HFIT.CurrentRow.Cells[4];
                    HFIT.CurrentCell = cell;
                    HFIT.BeginEdit(true);
                }

            }
        }

        private void HFIT_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}

