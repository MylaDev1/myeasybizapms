﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Drawing.Imaging;
using System.Configuration;
using System.Data.SqlClient;
using System.Diagnostics;
using excel = Microsoft.Office.Interop.Excel;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
namespace MyEasyBizAPMS
{
    public partial class FrmPrintingIssue : Form
    {
        private CrystalDecisions.Windows.Forms.CrystalReportViewer Cryview;

        ReportDocument doc = new ReportDocument();
        public FrmPrintingIssue()
        {
            this.BackColor = Color.White; ;
            InitializeComponent();
        }

        string uid = "";
        int mode = 0;
        string tpuid = "";

        int type = 0;
        DateTime str9;
        double sum1;
        double sum2;
        int SP;
        int Dtype = 0;
        int h = 0;
        int i = 0;
        int kk = 0;
        SqlConnection conn = new SqlConnection(GeneralParameters.ConnectionString);
        SqlCommand qur = new SqlCommand();
        private DataRow doc1;
        DataTable Docno = new DataTable();
        BindingSource bs = new BindingSource();
             DataTable Docno1 = new DataTable();
        BindingSource bsc = new BindingSource();
        BindingSource bsp = new BindingSource();
        BindingSource bsserial = new BindingSource();
        BindingSource bscombo = new BindingSource();
        BindingSource IN = new BindingSource();
        BindingSource OUT = new BindingSource();
        BindingSource bsFabric = new BindingSource();

        SQLDBHelper db = new SQLDBHelper();
        int Fillid;
        BindingSource bsParty = new BindingSource();
        string str1key;
        public int SelectId = 0;
        private void FrmPrintingIssue_Load(object sender, EventArgs e)
        {
            qur.Connection = conn;
      
            Genclass.buttonstyleform(this);
            Genclass.buttonstylepanel(panadd);
            panadd.Visible = true;
            Genpan.Visible = true;
            Editpnl.Visible = false;

            this.HFGP.DefaultCellStyle.Font = new Font("calibri", 10);
            this.HFGP.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 10, FontStyle.Bold);
            this.HFIT.DefaultCellStyle.Font = new Font("calibri", 10);
            this.HFIT.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 10, FontStyle.Bold);

            this.HFGP1.DefaultCellStyle.Font = new Font("calibri", 10);
            this.HFGP1.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 10, FontStyle.Bold);

            this.HFGP2.DefaultCellStyle.Font = new Font("calibri", 10);
            this.HFGP2.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 10, FontStyle.Bold);

            HFIT.RowHeadersVisible = false;
            HFGP.RowHeadersVisible = false;
            HFGP2.RowHeadersVisible = false;
            HFGP1.RowHeadersVisible = false;




            HFGP2.EnableHeadersVisualStyles = false;
            HFGP2.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.GradientActiveCaption;

            HFIT.EnableHeadersVisualStyles = false;
            HFIT.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.GradientActiveCaption;
            HFGP1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            HFGP1.EnableHeadersVisualStyles = false;
            HFGP1.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.GradientActiveCaption;

            HFGP.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            HFGP.EnableHeadersVisualStyles = false;
            HFGP.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.GradientActiveCaption;


           
                Genclass.Dtype = 1420;


            dtpfnt.Format = DateTimePickerFormat.Custom;
            dtpfnt.CustomFormat = "dd/MM/yyyy";
            dtpgrndt.Format = DateTimePickerFormat.Custom;
            dtpgrndt.CustomFormat = "dd/MM/yyyy";
            dtpsupp.Format = DateTimePickerFormat.Custom;
            dtpsupp.CustomFormat = "dd/MM/yyyy";
            chkact.Checked = true;

            LoadGetJobCard(1);
            Titlep();
      
        }
        private void loaduom()
        {

            string qur1 = " select distinct uid from orderm where docno='"+ txtqty.Text+"'";
            SqlCommand cmd = new SqlCommand(qur1, conn);
            SqlDataAdapter apt = new SqlDataAdapter(cmd);
            DataTable tab = new DataTable();
            apt.Fill(tab);
     if(tab.Rows.Count>0)
            {
                txtqty.Tag = tab.Rows[0]["uid"].ToString();

            }

        }
        private void loadproductionline()
        {
            conn.Open();
            string qur = "select Guid as uid,generalname as Stage from generalm where typemuid=41 ";
            SqlCommand cmd = new SqlCommand(qur, conn);
            SqlDataAdapter apt = new SqlDataAdapter(cmd);
            DataTable tab = new DataTable();
            apt.Fill(tab);
            cboproline.DataSource = null;
            cboproline.DataSource = tab;
            cboproline.DisplayMember = "Stage";
            cboproline.ValueMember = "uid";
            cboproline.SelectedIndex = -1;
         
        }

        private void loadstage()
        {
            conn.Open();
            string qur = "select Guid as uid,generalname as Stage from generalm where  typemuid=40 and shortname is not null ";
            SqlCommand cmd = new SqlCommand(qur, conn);
            SqlDataAdapter apt = new SqlDataAdapter(cmd);
            DataTable tab = new DataTable();
            apt.Fill(tab);
            cbostage.DataSource = null;
            cbostage.DataSource = tab;
            cbostage.DisplayMember = "Stage";
            cbostage.ValueMember = "uid";
            cbostage.SelectedIndex = -1;
            conn.Close();
        }
        private void loadcon()
        {
          
            string qur = "select Guid as uid,generalname as Stage from generalm where typemuid=37 ";
            SqlCommand cmd = new SqlCommand(qur, conn);
            SqlDataAdapter apt = new SqlDataAdapter(cmd);
            DataTable tab = new DataTable();
            apt.Fill(tab);
            cbocon.DataSource = null;
            cbocon.DataSource = tab;
            cbocon.DisplayMember = "Stage";
            cbocon.ValueMember = "uid";
            cbocon.SelectedIndex = -1;
           
        }
        protected DataTable LoadGetJobCard(int tag)
        {

    
            chkact.Checked = true;
            if (chkact.Checked == true)
            {
                SP = 1;

            }
            else
            {
                SP = 0;

            }


            DataTable dt = new DataTable();
            try
            {

                SqlParameter[] para = {
                    //new SqlParameter("@active",SP),
                         new SqlParameter("@doctypeid",Genclass.Dtype),


                };

                dt = db.GetData(CommandType.StoredProcedure, "SP_GETPRINTINGISSUE", para);


                LoadDataTable(dt);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        protected void LoadDataTable(DataTable dt)
        {
            try
            {
                HFGP.DataSource = null;
                HFGP.AutoGenerateColumns = false;
                HFGP.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
                HFGP.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
                HFGP.ColumnCount = 8;
                HFGP.Columns[0].Name = "Uid";
                HFGP.Columns[0].HeaderText = "Uid";
                HFGP.Columns[0].DataPropertyName = "Uid";

                HFGP.Columns[1].Name = "Docno";
                HFGP.Columns[1].HeaderText = "Docno";
                HFGP.Columns[1].DataPropertyName = "Docno";

                HFGP.Columns[2].Name = "Docdate";
                HFGP.Columns[2].HeaderText = "Docdate";
                HFGP.Columns[2].DataPropertyName = "Docdate";

                HFGP.Columns[3].Name = "ProOrderNo";
                HFGP.Columns[3].HeaderText = "ProOrderNo";
                HFGP.Columns[3].DataPropertyName = "ProOrderNo";

                HFGP.Columns[4].Name = "SocNo";
                HFGP.Columns[4].HeaderText = "SocNo";
                HFGP.Columns[4].DataPropertyName = "SocNo";

                HFGP.Columns[5].Name = "Supplier";
                HFGP.Columns[5].HeaderText = "Supplier";
                HFGP.Columns[5].DataPropertyName = "Supplier";

                HFGP.Columns[6].Name = "suppid";
                HFGP.Columns[6].HeaderText = "suppid";
                HFGP.Columns[6].DataPropertyName = "suppid";

                HFGP.Columns[7].Name = "PrintingType";
                HFGP.Columns[7].HeaderText = "PrintingType";
                HFGP.Columns[7].DataPropertyName = "PrintingType";





                bs.DataSource = dt;

                HFGP.DataSource = bs;


                HFGP.Columns[0].Visible = false;
           
          
          
                HFGP.Columns[1].Width = 90;
                HFGP.Columns[2].Width = 90;
                HFGP.Columns[3].Visible = false;

                HFGP.Columns[4].Visible = false;
                HFGP.Columns[5].Width = 750;


                HFGP.Columns[6].Visible = false;
                HFGP.Columns[7].Visible = false;
 
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        private void button3_Click(object sender, EventArgs e)
        {
            mode = 1;
            Genpan.Visible = false;
            txtuomm.Text = string.Empty;
            txtuom.Text = string.Empty;
            panadd.Visible = false;
            h = 0;
            Genclass.ClearTextBox(this, Editpnl);
            Genclass.STR = "";
            Genclass.Gendocno();
            txtgrn.Text = Genclass.ST;
            Editpnl.Visible = true;
            lkppnl.Visible = false;
            sum1 = 0;
            sum2 = 0;
            txtgrn.Tag = 0;
            HFIT.Refresh();
            HFIT.DataSource = null;
            HFIT.Rows.Clear();

            HFGP1.Refresh();
            HFGP1.DataSource = null;
            HFGP1.Rows.Clear();

            conn.Close();
            conn.Open();
            Docno.Clear();

            button13.Visible = false;
            if (Docno.Columns.Count == 0)
            {
                Docno.Columns.Add("uid");
                Docno.Columns.Add("Docno");
            }
            conn.Close();
            conn.Open();
            Chkedtact.Checked = true;

            txtuomm.Text = "PCS";

            txtuom.Text = string.Empty;
            HFIT.Refresh();
            HFIT.DataSource = null;
            HFIT.Rows.Clear();
            HFGP1.Refresh();
            HFGP1.DataSource = null;
            HFGP1.Rows.Clear();

            Titlep();


            dtpgrndt.Focus();
        }
        private void Titlep()
        {
            HFIT.AutoGenerateColumns = false;
            HFIT.Refresh();
            HFIT.DataSource = null;
            HFIT.Rows.Clear();
            HFIT.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            HFIT.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            HFIT.ColumnCount = 12;
            HFIT.Columns[0].Name = "SocNo";
            HFIT.Columns[1].Name = "Operations";
            HFIT.Columns[2].Name = "Itemname";
            HFIT.Columns[3].Name = "Rate";
            HFIT.Columns[4].Name = "Size";
            HFIT.Columns[5].Name = "Uom";
            HFIT.Columns[6].Name = "Qty";
            HFIT.Columns[7].Name = "Bundles";
            HFIT.Columns[8].Name = "Refuid";
            HFIT.Columns[9].Name = "weight";
            HFIT.Columns[10].Name = "DcNo";
            HFIT.Columns[11].Name = "uid";


            //HFIT.EditMode = DataGridViewEditMode.EditOnKeystroke;

            HFIT.Columns[0].Width = 100;
            HFIT.Columns[1].Width = 120;
            HFIT.Columns[2].Width = 200;
            //HFIT.Columns[3].Width = 80;
            //HFIT.Columns[4].Width = 80;
            HFIT.Columns[3].Width = 100;
            HFIT.Columns[4].Width = 70;
            HFIT.Columns[5].Width = 80;
            HFIT.Columns[6].Width = 60;
            HFIT.Columns[7].Width = 60;
            HFIT.Columns[8].Visible = false;
            HFIT.Columns[9].Width = 60;
            HFIT.Columns[10].Visible = false;
            HFIT.Columns[11].Visible = false;

        }
        private void txtname_Click(object sender, EventArgs e)
        {
            type = 1;
            DataTable dt = getParty();
            bsp.DataSource = dt;
            FillGrid2(dt, 1);
            Point loc = FindLocation(txtname);
            grSearch.Location = new Point(loc.X, loc.Y + 20);
            grSearch.Visible = true;
            grSearch.Text = "Name Search";
        }
        protected void FillGrid2(DataTable dt, int FillId)
        {
            try
            {
                DataGridCommon.DataSource = null;
                DataGridCommon.AutoGenerateColumns = false;


                DataGridCommon.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
                DataGridCommon.DefaultCellStyle.WrapMode = DataGridViewTriState.True;


                DataGridCommon.ColumnCount = 3;
                DataGridCommon.Columns[0].Name = "uid";
                DataGridCommon.Columns[0].HeaderText = "uid";
                DataGridCommon.Columns[0].DataPropertyName = "uid";
                DataGridCommon.Columns[1].Name = "Socno";
                DataGridCommon.Columns[1].HeaderText = "Socno";
                DataGridCommon.Columns[1].DataPropertyName = "Socno";
                DataGridCommon.Columns[1].Width = 100;
                DataGridCommon.Columns[2].Name = "Docno";
                DataGridCommon.Columns[2].HeaderText = "Docno";
                DataGridCommon.Columns[2].DataPropertyName = "Docno";
                DataGridCommon.Columns[2].Width = 100;
  

                DataGridCommon.DataSource = bsFabric;
                DataGridCommon.Columns[0].Visible = false;
             


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        private void txtname_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.F10)
                {
                    grSearch.Visible = false;
                }
                else if (e.KeyCode == Keys.F2)
                {
                    SelectId = 1;
                    int Index = DataGridCommon.SelectedCells[0].RowIndex;

                    txtname.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtpuid.Text = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    //txtmillid.Text = DataGridCommon.Rows[Index].Cells[4].Value.ToString();
                    txtmode.Focus();
                    grSearch.Visible = false;
                    SelectId = 0;
                }
                else if (e.KeyCode == Keys.Escape)
                {
                    grSearch.Visible = false;
                }
                else if (e.KeyValue == 40)
                {
                    DataGridCommon.Select();
                }
                else if (e.KeyCode == Keys.Enter)
                {
                    txtname_Click(sender, e);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtname_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bsp.Filter = string.Format("Name LIKE '%{0}%' ", txtname.Text);



                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtdcqty_Click(object sender, EventArgs e)
        {
           
            
        
            Point loc = FindLocation(txtdcqty);
            grSearch.Location = new Point(loc.X, loc.Y + 20);
            grSearch.Visible = true;
            if (cbostage.Text == "Garment")
            {
                type = 3;
                comboname();
            }
            else if (cbostage.Text == "Component")

            {
                type = 20;
                comboname1();
            }


        }
        private void comboname()

        {
            

                string qur1 = "select b.uid,b.Itemname,b.Type,b.size,b.Uom,b.accqty-isnull(sum(c.QTY),0) as QTy,b.Bundles  from PROORDERENTRYM A inner JOIN PROCDUCTIONENTRYD B ON A.uid=B.headid   left join PRINTINGISSUED  c on b.uid=c.refuid LEFT JOIN PRINTINGISSUEM D ON C.HEADID=D.UID AND D.PRINTINGTYPE='Garment'  left join JOBWORKISSUED  e on b.uid=e.refuid LEFT JOIN JOBWORKISSUEm f ON e.HEADID=f.UID AND D.PRINTINGTYPE='Garment'  where b.operations='" + Operations.Text + "' and a.socno='" + txtqty.Text + "' GROUP BY  b.uid,b.Itemname,b.accqty ,b.Type,b.size,b.Uom,b.Bundles HAVING b.accqty-isnull(sum(c.QTY),0)>0  ";
                SqlCommand cmd1 = new SqlCommand(qur1, conn);
                SqlDataAdapter apt1 = new SqlDataAdapter(cmd1);
      
          
       
            DataTable tab1 = new DataTable();
            apt1.Fill(tab1);
            bscombo.DataSource = tab1;
            //Point loc = FindLocation(txtdcqty);
            //grSearch.Location = new Point(loc.X, loc.Y + 20);
            grSearch.Visible = true;
            DataGridCommon.AutoGenerateColumns = false;
            DataGridCommon.Refresh();
            DataGridCommon.DataSource = null;
            DataGridCommon.Rows.Clear();
            DataGridCommon.ColumnCount = tab1.Columns.Count;
            i = 0;
            foreach (DataColumn column in tab1.Columns)
            {
                DataGridCommon.Columns[i].Name = column.ColumnName;
                DataGridCommon.Columns[i].HeaderText = column.ColumnName;
                DataGridCommon.Columns[i].DataPropertyName = column.ColumnName;
                i = i + 1;
            }

            DataGridCommon.DataSource = tab1;

            DataGridCommon.Columns[1].Width = 300;
            DataGridCommon.Columns[0].Visible = false;
            DataGridCommon.Columns[2].Visible = false;
            DataGridCommon.Columns[3].Visible = false;
            DataGridCommon.Columns[4].Visible = false;
            DataGridCommon.Columns[5].Visible = false;
            DataGridCommon.Columns[6].Visible = false;

        }
        private void comboname1()

        {
           

                string qur1 = "select distinct 0 uid,b.componentname as Item  from cuttingEntryM A inner JOIN cuttingComboComponentList B ON A.uid=B.headid   left join PRINTINGISSUED  c on b.uid=c.refuid  LEFT JOIN PRINTINGISSUEM D ON C.HEADID=D.UID AND D.PRINTINGTYPE='Component'  left join JOBWORKISSUED  e on b.uid=e.refuid LEFT JOIN JOBWORKISSUEm f ON e.HEADID=f.UID AND D.PRINTINGTYPE='Component' where a.socno='" + txtqty.Text + "'  and a.docno like '%CUT%' GROUP BY   b.componentname,b.weight ,b.size,b.Uom HAVING b.weight-isnull(sum(c.QTY),0)-isnull(sum(e.QTY),0)>0  ";
                SqlCommand cmd1 = new SqlCommand(qur1, conn);
                SqlDataAdapter apt1 = new SqlDataAdapter(cmd1);
            


            DataTable tab1 = new DataTable();
            apt1.Fill(tab1);
            bscombo.DataSource = tab1;
            //Point loc = FindLocation(txtdcqty);
            //grSearch.Location = new Point(loc.X, loc.Y + 20);
            grSearch.Visible = true;
            DataGridCommon.AutoGenerateColumns = false;
            DataGridCommon.Refresh();
            DataGridCommon.DataSource = null;
            DataGridCommon.Rows.Clear();
            DataGridCommon.ColumnCount = tab1.Columns.Count;
            i = 0;
            foreach (DataColumn column in tab1.Columns)
            {
                DataGridCommon.Columns[i].Name = column.ColumnName;
                DataGridCommon.Columns[i].HeaderText = column.ColumnName;
                DataGridCommon.Columns[i].DataPropertyName = column.ColumnName;
                i = i + 1;
            }

            DataGridCommon.DataSource = tab1;

            DataGridCommon.Columns[1].Width = 300;
            DataGridCommon.Columns[0].Visible = false;
            //DataGridCommon.Columns[2].Visible = false;
            //DataGridCommon.Columns[3].Visible = false;
            //DataGridCommon.Columns[4].Visible = false;
            //DataGridCommon.Columns[5].Visible = false;
            //DataGridCommon.Columns[6].Visible = false;

        }
        private void componentname()

        {
            string qur1 = "sp_getitemproentry '"+ txtqty.Text + "','"+ cbostage.Text+ "' ";
            SqlCommand cmd1 = new SqlCommand(qur1, conn);
            SqlDataAdapter apt1 = new SqlDataAdapter(cmd1);
            DataTable tab1 = new DataTable();
            apt1.Fill(tab1);
            bsParty.DataSource = tab1;
            grSearch.Visible = true;
            DataGridCommon.AutoGenerateColumns = false;
            DataGridCommon.Refresh();
            DataGridCommon.DataSource = null;
            DataGridCommon.Rows.Clear();
            DataGridCommon.ColumnCount = tab1.Columns.Count;
            i = 0;
            foreach (DataColumn column in tab1.Columns)
            {
                DataGridCommon.Columns[i].Name = column.ColumnName;
                DataGridCommon.Columns[i].HeaderText = column.ColumnName;
                DataGridCommon.Columns[i].DataPropertyName = column.ColumnName;
                i = i + 1;
            }

            DataGridCommon.DataSource = tab1;

            DataGridCommon.Columns[1].Width = 300;

            DataGridCommon.Columns[0].Visible = false;
            //DataGridCommon.Columns[2].Visible = false;
            //DataGridCommon.Columns[3].Visible = false;
            //DataGridCommon.Columns[4].Visible = false;

        }
        private void size1()

        {

            string qur1 = "select b.uid as sizeuid,b.size sizename,b.Uom,b.weight-isnull(sum(c.QTY),0)-isnull(sum(e.QTY),0) as QTy,0 as Bundles  from cuttingEntryM A  inner JOIN cuttingComboComponentList B ON A.uid=B.headid    left join PRINTINGISSUED  c on b.uid=c.refuid  LEFT JOIN PRINTINGISSUEM D ON C.HEADID=D.UID AND D.PRINTINGTYPE='Component'  left join JOBWORKISSUED  e on b.uid=e.refuid LEFT JOIN JOBWORKISSUEm f ON e.HEADID=f.UID AND D.PRINTINGTYPE='Component' where  a.socno='" + txtqty.Text + "'  and b.componentname='"+ txtdcqty.Text   + "'  and a.docno='"+ txtrewqty.Text + "' and   a.docno like '%CUT%'  GROUP BY  b.uid,b.weight ,b.size,b.Uom HAVING b.weight-isnull(sum(c.QTY),0)-isnull(sum(e.QTY),0)>0  	";
            SqlCommand cmd1 = new SqlCommand(qur1, conn);
            SqlDataAdapter apt1 = new SqlDataAdapter(cmd1);
            DataTable tab1 = new DataTable();
            apt1.Fill(tab1);



            bsserial.DataSource = tab1;
            Point loc = FindLocation(txtaddnotes);
            grSearch.Location = new Point(loc.X, loc.Y + 20);
            grSearch.Visible = true;
            DataGridCommon.AutoGenerateColumns = false;
            DataGridCommon.Refresh();
            DataGridCommon.DataSource = null;
            DataGridCommon.Rows.Clear();
            DataGridCommon.ColumnCount = tab1.Columns.Count;
            i = 0;
            foreach (DataColumn column in tab1.Columns)
            {
                DataGridCommon.Columns[i].Name = column.ColumnName;
                DataGridCommon.Columns[i].HeaderText = column.ColumnName;
                DataGridCommon.Columns[i].DataPropertyName = column.ColumnName;
                i = i + 1;
            }

            DataGridCommon.DataSource = tab1;

            DataGridCommon.Columns[1].Width = 300;

            DataGridCommon.Columns[0].Visible = false;
            DataGridCommon.Columns[2].Visible = false;
            DataGridCommon.Columns[3].Visible = false;
            DataGridCommon.Columns[4].Visible = false;

        }
        private void size()

        {
        
            string qur1 = "select distinct a.sizeuid,d.sizename as sizename  from ordermfabric  a  inner join  stylecombo b on a.ordermuid=b.ordermuid and a.combouid=b.uid   inner join generalm c on a.componentuid=c.guid  inner join  StyleSize d on a.sizeuid=d.uid  inner join orderM e on a.OrderMuid = e.Uid 	  where e.docno='" + txtqty.Text + "'";
            SqlCommand cmd1 = new SqlCommand(qur1, conn);
            SqlDataAdapter apt1 = new SqlDataAdapter(cmd1);
            DataTable tab1 = new DataTable();
            apt1.Fill(tab1);



            bsserial.DataSource = tab1;
            Point loc = FindLocation(txtaddnotes);
            grSearch.Location = new Point(loc.X, loc.Y + 20);
            grSearch.Visible = true;
            DataGridCommon.AutoGenerateColumns = false;
            DataGridCommon.Refresh();
            DataGridCommon.DataSource = null;
            DataGridCommon.Rows.Clear();
            DataGridCommon.ColumnCount = tab1.Columns.Count;
            i = 0;
            foreach (DataColumn column in tab1.Columns)
            {
                DataGridCommon.Columns[i].Name = column.ColumnName;
                DataGridCommon.Columns[i].HeaderText = column.ColumnName;
                DataGridCommon.Columns[i].DataPropertyName = column.ColumnName;
                i = i + 1;
            }

            DataGridCommon.DataSource = tab1;

            DataGridCommon.Columns[1].Width = 300;

            DataGridCommon.Columns[0].Visible = false;


        }
        protected void FillGrid(DataTable dt, int FillId)
        {
            try
            {
                DataGridCommon.DataSource = null;
                DataGridCommon.AutoGenerateColumns = false;


                DataGridCommon.ColumnCount = 4;
                DataGridCommon.Columns[0].Name = "uid";
                DataGridCommon.Columns[0].HeaderText = "uid";
                DataGridCommon.Columns[0].DataPropertyName = "uid";
                DataGridCommon.Columns[1].Name = "Itemname";
                DataGridCommon.Columns[1].HeaderText = "Itemname";
                DataGridCommon.Columns[1].DataPropertyName = "Itemname";
                DataGridCommon.Columns[1].Width = 300;
                DataGridCommon.Columns[2].Name = "UOMID";
                DataGridCommon.Columns[2].HeaderText = "UOMID";
                DataGridCommon.Columns[2].DataPropertyName = "UOMID";

                DataGridCommon.Columns[3].Name = "GENERALNAME";
                DataGridCommon.Columns[3].HeaderText = "GENERALNAME";
                DataGridCommon.Columns[3].DataPropertyName = "GENERALNAME";



                DataGridCommon.DataSource = bsParty;
                DataGridCommon.Columns[0].Visible = false;
                DataGridCommon.Columns[2].Visible = false;
                DataGridCommon.Columns[3].Visible = false;



            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private Point FindLocation(Control ctrl)
        {
            if (ctrl.Parent is Form)
                return ctrl.Location;
            else
            {
                Point p = FindLocation(ctrl.Parent);
                p.X += ctrl.Location.X;
                p.Y += ctrl.Location.Y;
                return p;
            }
        }
        protected DataTable getParty()
        {
            DataTable dt = new DataTable();
            try
            {
                if (type == 1 && cbostage.Text=="Garment")
                {
                    
                    dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "sp_getCuttingIssueSocno", conn);
                    bsFabric.DataSource = dt;
                }
                if (type == 1 && cbostage.Text == "Component")
                {

                    dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "sp_getCuttingEntryPrintIssueSocno", conn);
                    bsFabric.DataSource = dt;
                }
                if (type == 10)
                {
                    dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "SP_GETPARTYSUPPLIER1", conn);
                    bsp.DataSource = dt;
                }
                else if (type == 1 && Text == "Fabric Without Process")
                {
                    dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "SP_FABWOPROCESSSUPP", conn);
                    bsp.DataSource = dt;
                }
                else if (type == 2 && Text == "Knitting Without Process")
                {
                    //dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "sp_getwo", conn);
                    //bsc1.DataSource = dt;
                }
                else if (type == 2 && Text == "Fabric Without Process")
                {
                    //dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "sp_getwo", conn);
                    //bsc1.DataSource = dt;
                }



            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            return dt;
        }
        private void txtdcqty_KeyDown(object sender, KeyEventArgs e)
        {

            try
            {
                if (e.KeyCode == Keys.F10)
                {
                    grSearch.Visible = false;
                }
                else if (e.KeyCode == Keys.F2)
                {
                    SelectId = 1;
                    int Index = DataGridCommon.SelectedCells[0].RowIndex;
                    txtdcqty.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtdcqty.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    textBox5.Focus();
                    grSearch.Visible = false;
                    SelectId = 0;
                }
                else if (e.KeyCode == Keys.Escape)
                {
                    grSearch.Visible = false;
                }
                else if (e.KeyValue == 40)
                {
                    DataGridCommon.Select();
                }
                else if (e.KeyCode == Keys.Enter)
                {
                    txtdcqty_Click(sender, e);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtdcqty_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bscombo.Filter = string.Format("item LIKE '%{0}%' ", txtdcqty.Text);



                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtitem_Click(object sender, EventArgs e)        {
            type = 2;
            DataTable dt = getParty();
            //bsc1.DataSource = dt;
            FillGrid3(dt, 1);
            Point loc = FindLocation(txtitem);
            grSearch.Location = new Point(loc.X, loc.Y + 20);
            grSearch.Visible = true;
            grSearch.Text = "Name Search";
        }

        private void txtitem_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.F10)
                {
                    grSearch.Visible = false;
                }
                else if (e.KeyCode == Keys.F2)
                {
                    SelectId = 1;
                    int Index = DataGridCommon.SelectedCells[0].RowIndex;

                    txtitem.Text = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    txtitem.Tag = DataGridCommon.Rows[Index].Cells[1].Value.ToString();

                    txtmode.Focus();
                    lkppnl.Visible = false;
                    SelectId = 0;
                }
                else if (e.KeyCode == Keys.Escape)
                {
                    grSearch.Visible = false;
                }
                else if (e.KeyValue == 40)
                {
                    DataGridCommon.Select();
                }
                else if (e.KeyCode == Keys.Enter)
                {
                    txtitem_Click(sender, e);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
        protected void FillGrid3(DataTable dt, int FillId)
        {
            try
            {
                DataGridCommon.DataSource = null;
                DataGridCommon.AutoGenerateColumns = false;
                DataGridCommon.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
                DataGridCommon.DefaultCellStyle.WrapMode = DataGridViewTriState.True;

                DataGridCommon.ColumnCount = 2;
                DataGridCommon.Columns[0].Name = "Uid";
                DataGridCommon.Columns[0].HeaderText = "Uid";
                DataGridCommon.Columns[0].DataPropertyName = "Uid";

                DataGridCommon.Columns[1].Name = "Name";
                DataGridCommon.Columns[1].HeaderText = "Name";
                DataGridCommon.Columns[1].DataPropertyName = "Name";
                DataGridCommon.Columns[1].Width = 300;





                DataGridCommon.DataSource = bsp;
                DataGridCommon.Columns[0].Visible = false;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        private void txtitem_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bsp.Filter = string.Format("docno LIKE '%{0}%' ", txtvehicle.Text);



                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void button18_Click(object sender, EventArgs e)
        {
            try
            {
                SelectId = 1;
                int Index = DataGridCommon.SelectedCells[0].RowIndex;
                if (type == 1)
                {
                    txtqty.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtqty.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    txtrewqty.Text = DataGridCommon.Rows[Index].Cells[2].Value.ToString();
                    txtitem.Focus();
                    cbostage_SelectedIndexChanged(sender, e);
                }
                else if (type == 3)
                {
                    txtdcqty.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtdcqty.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    cbooperations.Text = DataGridCommon.Rows[Index].Cells[2].Value.ToString();
                    txtaddnotes.Text = DataGridCommon.Rows[Index].Cells[3].Value.ToString();
                    txtuomm.Text = DataGridCommon.Rows[Index].Cells[4].Value.ToString();
                    txtuom.Text = DataGridCommon.Rows[Index].Cells[5].Value.ToString();
                    txtbun.Text = DataGridCommon.Rows[Index].Cells[6].Value.ToString();
                    
                }
                else if (type == 20)
                {
                    txtdcqty.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                
                 
                }
                else if (type == 21)
                {
                    txtaddnotes.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtdcqty.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    txtuomm.Text = DataGridCommon.Rows[Index].Cells[2].Value.ToString();
                    txtuom.Text = DataGridCommon.Rows[Index].Cells[3].Value.ToString();
                    txtbun.Text = DataGridCommon.Rows[Index].Cells[4].Value.ToString();
             

                }
                else if (type == 10)
                {
                    txtsocno.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtsocno.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    cbostage.Focus();
                }
                else if (type == 5)
                {
                    txtaddnotes.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtaddnotes.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    txtuom.Focus();
                }

                else if (type == 4)
                {
                    textBox5.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    textBox5.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtaddnotes.Text = DataGridCommon.Rows[Index].Cells[2].Value.ToString();
                    txtprice.Text = DataGridCommon.Rows[Index].Cells[3].Value.ToString();
                    txtuom.Text = DataGridCommon.Rows[Index].Cells[4].Value.ToString();
                   
                   
                }
             
                grSearch.Visible = false;
                SelectId = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
        private void loadallitems()

        {

            if (txtname.Text == string.Empty)
            {
                MessageBox.Show("Select Supplier", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtname.Focus();
                return;
            }
            lkppnl.Visible = true;
            txtscr11.Text = "";
            txtscr11.Focus();


            if (Text == "Knitting Without Process")
            {
                Genclass.strsql = "SP_GETKNITRECWOITEMS  " + txtpuid.Text + ",'" + txtscono.Text + "' ,'" + txtwok.Text + "','" + txtwo.Text + "' ";
                Genclass.FSSQLSortStr = "Itemdes";
            }
            else

            {

                Genclass.strsql = "SP_GETFABRECWOITEMS  " + txtpuid.Text + ",'" + txtscono.Text + "' ,'" + txtwok.Text + "','" + txtwo.Text + "' ";
                Genclass.FSSQLSortStr = "Itemdes";
            }


            Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
            SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
            DataTable tap = new DataTable();
            aptr.Fill(tap);

            bsc.DataSource = tap;

            HFGP2.AutoGenerateColumns = false;
            HFGP2.Refresh();
            HFGP2.DataSource = null;
            HFGP2.Rows.Clear();
            HFGP2.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            HFGP2.DefaultCellStyle.WrapMode = DataGridViewTriState.True;


            HFGP2.ColumnCount = tap.Columns.Count;
            i = 0;

            foreach (DataColumn column in tap.Columns)
            {
                HFGP2.Columns[i].Name = column.ColumnName;
                HFGP2.Columns[i].HeaderText = column.ColumnName;
                HFGP2.Columns[i].DataPropertyName = column.ColumnName;
                i = i + 1;
            }
            HFGP2.DataSource = tap;

            HFGP2.Columns[0].Visible = false;
            HFGP2.Columns[2].Width = 100;

            HFGP2.Columns[3].Width = 100;
            HFGP2.Columns[4].Visible = false;
            HFGP2.Columns[5].Visible = false;
            HFGP2.Columns[6].Visible = false;
            HFGP2.Columns[1].Width = 250;
            HFGP2.Columns[7].Visible = false;
            HFGP2.Columns[8].Width = 100;
            HFGP2.Columns[9].Width = 200;
            HFGP2.Columns[10].Visible = false;
            conn.Close();
        }
        private void btnHide_Click(object sender, EventArgs e)
        {
            grSearch.Visible = false;
        }

        private void DataGridCommon_DoubleClick(object sender, EventArgs e)
        {
            SelectId = 1;
            int Index = DataGridCommon.SelectedCells[0].RowIndex;
            if (type == 1)
            {
                txtqty.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                txtqty.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                txtrewqty.Text = DataGridCommon.Rows[Index].Cells[2].Value.ToString();
                txtitem.Focus();
                cbostage_SelectedIndexChanged(sender, e);
            }
            else if (type == 20)
            {
                txtdcqty.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                txtdcqty.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();

            }
            else if (type == 21)
            {
                txtaddnotes.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                txtdcqty.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                txtuomm.Text = DataGridCommon.Rows[Index].Cells[2].Value.ToString();
                txtuom.Text = DataGridCommon.Rows[Index].Cells[3].Value.ToString();
                txtbun.Text = DataGridCommon.Rows[Index].Cells[4].Value.ToString();


            }
            else if (type == 10)
            {
                txtsocno.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                txtsocno.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                cbostage.Focus();
            }
            else if (type == 3)
            {
                txtdcqty.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                txtdcqty.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                cbooperations.Text = DataGridCommon.Rows[Index].Cells[2].Value.ToString();
                txtaddnotes.Text = DataGridCommon.Rows[Index].Cells[3].Value.ToString();
                txtuomm.Text = DataGridCommon.Rows[Index].Cells[4].Value.ToString();
                txtuom.Text = DataGridCommon.Rows[Index].Cells[5].Value.ToString();
                txtbun.Text = DataGridCommon.Rows[Index].Cells[6].Value.ToString();

            }
            else if (type == 5)
            {
                txtaddnotes.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                txtaddnotes.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                txtuom.Focus();
            }
            else if (type == 4)
            {
                textBox5.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                textBox5.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                txtaddnotes.Text = DataGridCommon.Rows[Index].Cells[2].Value.ToString();
                txtprice.Text = DataGridCommon.Rows[Index].Cells[3].Value.ToString();
                txtuom.Text = DataGridCommon.Rows[Index].Cells[4].Value.ToString();


            }
            grSearch.Visible = false;
            SelectId = 0;
        }

        private void DataGridCommon_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                int Index = DataGridCommon.SelectedCells[0].RowIndex;
                if (type == 1)
                {
                    txtqty.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtqty.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    txtrewqty.Text = DataGridCommon.Rows[Index].Cells[2].Value.ToString();
                    txtitem.Focus();
                    cbostage_SelectedIndexChanged(sender, e);
                }
                else if (type == 20)
                {
                    txtdcqty.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtdcqty.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();

                }
                else if (type == 21)
                {
                    txtaddnotes.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtdcqty.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    txtuomm.Text = DataGridCommon.Rows[Index].Cells[2].Value.ToString();
                    txtuom.Text = DataGridCommon.Rows[Index].Cells[3].Value.ToString();
                    txtbun.Text = DataGridCommon.Rows[Index].Cells[4].Value.ToString();


                }
                else if (type == 10)
                {
                    txtsocno.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtsocno.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    cbostage.Focus();
                }
                else if (type == 3)
                {
                    txtdcqty.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtdcqty.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    cbooperations.Text = DataGridCommon.Rows[Index].Cells[2].Value.ToString();
                    txtaddnotes.Text = DataGridCommon.Rows[Index].Cells[3].Value.ToString();
                    txtuomm.Text = DataGridCommon.Rows[Index].Cells[4].Value.ToString();
                    txtuom.Text = DataGridCommon.Rows[Index].Cells[5].Value.ToString();
                    txtbun.Text = DataGridCommon.Rows[Index].Cells[6].Value.ToString();

                }
                else if (type == 5)
                {
                    txtaddnotes.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtaddnotes.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    txtuom.Focus();
                }
                else if (type == 4)
                {
                    textBox5.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    textBox5.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtaddnotes.Text = DataGridCommon.Rows[Index].Cells[2].Value.ToString();
                    txtprice.Text = DataGridCommon.Rows[Index].Cells[3].Value.ToString();
                    txtuom.Text = DataGridCommon.Rows[Index].Cells[4].Value.ToString();


                }
                grSearch.Visible = false;
                SelectId = 0;
            }
        }

        private void button10_Click(object sender, EventArgs e)
        {

            int Index = HFGP2.SelectedCells[0].RowIndex;
            txtdcqty.Text = HFGP2.Rows[Index].Cells[1].Value.ToString();
            txtdcqty.Tag = HFGP2.Rows[Index].Cells[0].Value.ToString();
            txtuom.Text = HFGP2.Rows[Index].Cells[3].Value.ToString();
            txtoutputid.Text = HFGP2.Rows[Index].Cells[3].Value.ToString();
            txtitemid.Text = HFGP2.Rows[Index].Cells[4].Value.ToString();
            txtprice.Text = HFGP2.Rows[Index].Cells[5].Value.ToString();
            txtprice.Tag = HFGP2.Rows[Index].Cells[6].Value.ToString();
            txtisstot.Text = HFGP2.Rows[Index].Cells[7].Value.ToString();
            txtisstot.Tag = HFGP2.Rows[Index].Cells[10].Value.ToString();
            lkppnl.Visible = false;
            txtaddnotes.Focus();
        }

        private void HFGP2_DoubleClick(object sender, EventArgs e)
        {
            int Index = HFGP2.SelectedCells[0].RowIndex;

            txtdcqty.Text = HFGP2.Rows[Index].Cells[1].Value.ToString();
            txtdcqty.Tag = HFGP2.Rows[Index].Cells[0].Value.ToString();
            txtuom.Text = HFGP2.Rows[Index].Cells[3].Value.ToString();
            txtoutputid.Text = HFGP2.Rows[Index].Cells[3].Value.ToString();
            txtitemid.Text = HFGP2.Rows[Index].Cells[4].Value.ToString();
            txtprice.Text = HFGP2.Rows[Index].Cells[5].Value.ToString();
            txtprice.Tag = HFGP2.Rows[Index].Cells[6].Value.ToString();
            txtisstot.Text = HFGP2.Rows[Index].Cells[7].Value.ToString();
            txtisstot.Tag = HFGP2.Rows[Index].Cells[10].Value.ToString();
            lkppnl.Visible = false;
            txtaddnotes.Focus();
        }

        private void HFGP2_KeyDown(object sender, KeyEventArgs e)
        {

            if (e.KeyCode == Keys.Enter)
            {
                int Index = HFGP2.SelectedCells[0].RowIndex;
                txtdcqty.Text = HFGP2.Rows[Index].Cells[1].Value.ToString();
                txtdcqty.Tag = HFGP2.Rows[Index].Cells[0].Value.ToString();
                txtuom.Text = HFGP2.Rows[Index].Cells[3].Value.ToString();
                txtoutputid.Text = HFGP2.Rows[Index].Cells[3].Value.ToString();
                txtitemid.Text = HFGP2.Rows[Index].Cells[4].Value.ToString();
                txtprice.Text = HFGP2.Rows[Index].Cells[5].Value.ToString();
                txtprice.Tag = HFGP2.Rows[Index].Cells[6].Value.ToString();
                txtisstot.Text = HFGP2.Rows[Index].Cells[7].Value.ToString();
                txtisstot.Tag = HFGP2.Rows[Index].Cells[10].Value.ToString();
                lkppnl.Visible = false;
                txtaddnotes.Focus();
            }
        }

        private void buttnfinbk_Click(object sender, EventArgs e)
        {
            Editpnl.Visible = false;
            Genpan.Visible = true;
            panadd.Visible = true;
            LoadGetJobCard(1);
        }

        private void btnadd_Click(object sender, EventArgs e)
        {
            if (txtdcqty.Text == "")
            {
                MessageBox.Show("Enter the Item");
                txtdcqty.Focus();
                return;
            }
            if (txtaddnotes.Text == "")
            {
                MessageBox.Show("Enter the Size");
                txtaddnotes.Focus();
                return;
            }
            
            if (txtbun.Text == "")
            {
                MessageBox.Show("Enter the bundles");
                txtbun.Focus();
                return;
            }
            if (txtuom.Text == "")
            {
                MessageBox.Show("Enter the Qty");
                txtuom.Focus();
                return;
            }
            //if (txtaccqty.Text == "")
            //{
            //    MessageBox.Show("Enter the txtaccqty");
            //    txtaccqty.Focus();
            //    return;
            //}
            //for (int p=0;p< HFIT.RowCount - 1; p++)
            //{
            //    if(cbocon.Text== HFIT.Rows[p].Cells[4].Value.ToString())
            //    {
            //        MessageBox.Show("Already contractor name added");
            //        return;
            //    }
            //    if (txtaddnotes.Text == HFIT.Rows[p].Cells[5].Value.ToString())
            //    {
            //        MessageBox.Show("Already  size added");
            //        return;
            //    }

            //}

            //double pp = Convert.ToDouble(txtuom.Text);
            //double aa = Convert.ToDouble(txtaccqty.Text);
            //double bb = Convert.ToDouble(txtrejqty.Text);
            //double cc = Convert.ToDouble(txtrewqty.Text);
            //double mm = Convert.ToDouble(txtwok.Text);
            //if (pp>mm)

            //{

            //    MessageBox.Show("Qty Exceed");
            //    return;
            //}
            //double dd = pp - (aa + bb + cc);

            //if (dd!=0)

            //{
            //    MessageBox.Show("Qty mismatch");
            //    return;

            //}

            kk = HFIT.Rows.Count - 1;
            h = kk;
                    h = h + 1;
                var index = HFIT.Rows.Add();
                HFIT.Rows[index].Cells[0].Value = txtqty.Text;
                HFIT.Rows[index].Cells[1].Value = Operations.Text;
                HFIT.Rows[index].Cells[2].Value = txtdcqty.Text;
                HFIT.Rows[index].Cells[3].Value = txtrate.Text;
                HFIT.Rows[index].Cells[4].Value = txtaddnotes.Text;
                HFIT.Rows[index].Cells[5].Value = txtuomm.Text;
                HFIT.Rows[index].Cells[6].Value = txtuom.Text;
                HFIT.Rows[index].Cells[7].Value = txtbun.Text;
                HFIT.Rows[index].Cells[8].Value = txtdcqty.Tag;
                HFIT.Rows[index].Cells[9].Value = txtwt.Text;
                HFIT.Rows[index].Cells[10].Value = txtrewqty.Text;
                HFIT.Rows[index].Cells[11].Value = "0";




             txtrate.Text = "";
            txtwt.Text = "";
            txtrewqty.Text = "";
            txtaddnotes.Text = "";
            txtbags.Text = "";
            txtuom.Text = "";
            txtuomm.Text = "";
        
           txtbun.Text = "";
            txtqty.Text = "";
            txtdcqty.Text = "";

            txtdcqty.Focus();
        }

        private void btnsave_Click(object sender, EventArgs e)
        {
            conn.Close();
            conn.Open();
            

    
            if (Chkedtact.Checked == true)
            {

                SP = 1;
            }
            else
            {
                SP = 0;

            }
            SqlParameter[] para ={



                    new SqlParameter("@uid",txtgrn.Tag),
                    new SqlParameter("@DOCNO",txtgrn.Text),
                    new SqlParameter("@DOCDATE",Convert.ToDateTime(dtpgrndt.Text)),
                    new SqlParameter("@SOCNO","0"),
                    new SqlParameter("@SUPPID",txtsocno.Tag),
                    new SqlParameter("@PRINTINGTYPE",cbostage.Text),
                    new SqlParameter("@doctypeid",Genclass.Dtype),
                    new SqlParameter("@PROORDERNO","0"),




            };
            DataTable dataTable = db.GetDataWithParam(CommandType.StoredProcedure, "SP_PRINTINGISSUEM", para, conn);
            int ResponseCode = Convert.ToInt32(dataTable.Rows[0]["ResponseCode"].ToString());
            int ReturnId = Convert.ToInt32(dataTable.Rows[0]["ReturnId"].ToString());
            //db.ExecuteNonQuery(CommandType.StoredProcedure, "sp_cuttingEntryM", para, conn);
            //txtgrnid.Text= Convert.ToInt32(dataTable.Rows[0]["ReturnId"].ToString());


          

            for (int i = 0; i < HFIT.RowCount - 1; i++)
            {

                conn.Close();
                conn.Open();

                SqlParameter[] para1 ={


            new SqlParameter("@uid", HFIT.Rows[i].Cells[11].Value),
                   new SqlParameter("@HEADID",ReturnId),
                    new SqlParameter("@PROCESS",HFIT.Rows[i].Cells[1].Value),
                    new SqlParameter("@item", HFIT.Rows[i].Cells[2].Value),
                    new SqlParameter("@TYPE", HFIT.Rows[i].Cells[3].Value),
            
                          new SqlParameter("@SIZE", HFIT.Rows[i].Cells[4].Value),
                              new SqlParameter("@UOM", HFIT.Rows[i].Cells[5].Value),
                              new SqlParameter("@QTY", HFIT.Rows[i].Cells[6].Value),

                                 new SqlParameter("@bundels", HFIT.Rows[i].Cells[7].Value),
                    new SqlParameter("@refuid", HFIT.Rows[i].Cells[8].Value),
                             new SqlParameter("@weight", HFIT.Rows[i].Cells[9].Value),

                                 new SqlParameter("@socno", HFIT.Rows[i].Cells[0].Value),
                    new SqlParameter("@dcno", HFIT.Rows[i].Cells[10].Value),




                };

                db.ExecuteNonQuery(CommandType.StoredProcedure, "sp_PRINTINGISSUED", para1, conn);
            }



            conn.Close();
            conn.Open();
            if (mode == 1)
            {
                qur.CommandText = "update doctypem set lastno=lastno+1 where doctypeid=" + Genclass.Dtype + " and finyear='19-20'";
                qur.ExecuteNonQuery();

            }
            //conn.Close();
            //conn.Open();
            //if (ReturnId != 0)
            //{
            //    if (mode == 1)
            //    {
            //        qur.CommandText = "exec SP_stocklegCUttingReceipt " + ReturnId + "," + Genclass.Dtype + "   ,1";
            //        qur.ExecuteNonQuery();
            //        qur.CommandText = "exec SP_PRODUCTIONISSUE_WIP_STOCK_LEDGER " + ReturnId + "  ,1," + Genclass.Dtype + " ";
            //        qur.ExecuteNonQuery();

            //    }
            //    else
            //    {
            //        qur.CommandText = "exec SP_stocklegCUttingReceipt " + ReturnId + "," + Genclass.Dtype + "   ,2";
            //        qur.ExecuteNonQuery();
            //        qur.CommandText = "exec SP_PRODUCTIONISSUE_WIP_STOCK_LEDGER " + ReturnId + ",2," + Genclass.Dtype + " ";
            //        qur.ExecuteNonQuery();

            //    }

            //}
            MessageBox.Show("Save Record");
            Editpnl.Visible = false;

            Genpan.Visible = true;
            panadd.Visible = true;
            LoadGetJobCard(1);



        }

        private void butedit_Click(object sender, EventArgs e)
        {
            mode = 2;
            Chkedtact.Checked = true;
            mode = 2;
            panadd.Visible = false;
            Genpan.Visible = false;
            Editpnl.Visible = true;
            lkppnl.Visible = false;
           
            conn.Close();
            conn.Open();
            txtuomm.Text = string.Empty;
            txtuom.Text = string.Empty;
            txtuomm.Text = "PCS";


            //cbosGReturnItem.Items.Clear();
            int i = HFGP.SelectedCells[0].RowIndex;
            uid = HFGP.Rows[i].Cells[0].Value.ToString();
            txtgrnid.Text = HFGP.Rows[i].Cells[0].Value.ToString();
            txtgrn.Tag = Convert.ToInt32(HFGP.Rows[i].Cells[0].Value.ToString());
            txtgrn.Text = HFGP.Rows[i].Cells[1].Value.ToString();
            dtpgrndt.Text = HFGP.Rows[i].Cells[2].Value.ToString();
            //txtrewqty.Text = HFGP.Rows[i].Cells[3].Value.ToString();
            //txtqty.Text = HFGP.Rows[i].Cells[4].Value.ToString();
            txtsocno.Text = HFGP.Rows[i].Cells[5].Value.ToString();
            txtsocno.Tag = HFGP.Rows[i].Cells[6].Value.ToString();
            cbostage.Text = HFGP.Rows[i].Cells[7].Value.ToString();
            loaduom();
            Chkedtact.Checked = true;
            HFIT.Refresh();
            HFIT.DataSource = null;
            HFIT.Rows.Clear();

            HFGP2.Refresh();
            HFGP2.DataSource = null;
            HFGP2.Rows.Clear();
            HFGP1.Refresh();
            HFGP1.DataSource = null;
            HFGP1.Rows.Clear();

            Titlep();
            load();

        }
        private void load()
        {
            Genclass.strsql = "SP_GETPRINTINGISSUEEDIT " + txtgrnid.Text + " ";
            Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
            SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
            DataTable tap1 = new DataTable();
            aptr1.Fill(tap1);

            Genclass.sum1 = 0;


            for (int k = 0; k < tap1.Rows.Count; k++)
            {
                var index = HFIT.Rows.Add();
      
                HFIT.Rows[index].Cells[0].Value = tap1.Rows[k]["socno"].ToString();
                HFIT.Rows[index].Cells[1].Value = tap1.Rows[k]["process"].ToString();

                HFIT.Rows[index].Cells[2].Value = tap1.Rows[k]["item"].ToString();


                HFIT.Rows[index].Cells[3].Value = tap1.Rows[k]["type"].ToString();

                HFIT.Rows[index].Cells[4].Value = tap1.Rows[k]["size"].ToString();

                HFIT.Rows[index].Cells[5].Value = tap1.Rows[k]["uom"].ToString();


                HFIT.Rows[index].Cells[6].Value = tap1.Rows[k]["qty"].ToString();
                HFIT.Rows[index].Cells[7].Value = tap1.Rows[k]["bundels"].ToString();
                HFIT.Rows[index].Cells[8].Value = tap1.Rows[k]["refuid"].ToString();

                HFIT.Rows[index].Cells[9].Value = tap1.Rows[k]["weight"].ToString();
                HFIT.Rows[index].Cells[10].Value = tap1.Rows[k]["dcno"].ToString();
                HFIT.Rows[index].Cells[11].Value = tap1.Rows[k]["uid"].ToString();
            }
        }

        private void dtpgrndt_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                e.Handled = true;
                SendKeys.Send("{TAB}");
            }
        }

        private void txtqty_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                e.Handled = true;
                SendKeys.Send("{TAB}");
            }
        }

        private void txtmode_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                e.Handled = true;
                SendKeys.Send("{TAB}");
            }
        }

        private void txtvehicle_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                e.Handled = true;
                SendKeys.Send("{TAB}");
            }
        }

        private void txtdcqty_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                e.Handled = true;
                SendKeys.Send("{TAB}");
            }
        }

        private void txtaddnotes_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                e.Handled = true;
                SendKeys.Send("{TAB}");
            }
        }

        private void txtisstot_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                e.Handled = true;
                SendKeys.Send("{TAB}");
            }
        }

        private void txtbags_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                e.Handled = true;
                SendKeys.Send("{TAB}");
            }
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                e.Handled = true;
                SendKeys.Send("{TAB}");
            }
        }

        private void txtprice_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                e.Handled = true;
                SendKeys.Send("{TAB}");
            }

        }

        private void txtuom_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void button11_Click(object sender, EventArgs e)
        {
            lkppnl.Visible = false;
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void buttnext1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void DataGridCommon_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dtpsupp_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                e.Handled = true;
                SendKeys.Send("{TAB}");
            }
        }

        private void txtscr11_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.F10)
                {
                    lkppnl.Visible = false;
                }
                else if (e.KeyCode == Keys.F2)
                {
                    SelectId = 1;


                    int Index = HFGP2.SelectedCells[0].RowIndex;
                    txtdcqty.Text = HFGP2.Rows[Index].Cells[1].Value.ToString();
                    txtdcqty.Tag = HFGP2.Rows[Index].Cells[0].Value.ToString();
                    txtuom.Text = HFGP2.Rows[Index].Cells[3].Value.ToString();
                    txtitemid.Text = HFGP2.Rows[Index].Cells[4].Value.ToString();
                    txtprice.Text = HFGP2.Rows[Index].Cells[5].Value.ToString();
                    txtprice.Tag = HFGP2.Rows[Index].Cells[6].Value.ToString();
                    txtisstot.Text = HFGP2.Rows[Index].Cells[7].Value.ToString();
                    lkppnl.Visible = false;
                    txtaddnotes.Focus();

                    SelectId = 0;
                }
                else if (e.KeyCode == Keys.Escape)
                {
                    lkppnl.Visible = false;
                }
                else if (e.KeyValue == 40)
                {
                    HFGP2.Select();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtscr11_TextChanged(object sender, EventArgs e)
        {
            bsc.Filter = string.Format("itemdes LIKE '%{0}%' ", txtscr11.Text);
        }

        private void txtuom_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtuom_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)

            {
                btnadd_Click(sender, e);

            }
        }

        private void button13_Click(object sender, EventArgs e)
        {

        }

        private void txtdcno_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtname.Text == string.Empty)
                {
                    MessageBox.Show("Select Supplier Name", "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtname.Focus();
                    return;
                }
                if (txtmode.Text == string.Empty)
                {
                    MessageBox.Show("Enter Supplier DC Number and Date", "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtmode.Focus();
                    return;
                }
                txtscr11.Text = "";
                type = 10;
                txtscr11.Focus();
                Genclass.strsql = "SP_GETKNITRECDCNO   " + txtpuid.Text + "";
                Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
                DataTable tap = new DataTable();
                aptr.Fill(tap);
                bsc.DataSource = tap;
                Point loc = FindLocation(txtdcno);
                grSearch.Location = new Point(loc.X, loc.Y + 20);
                grSearch.Visible = true;
                DataGridCommon.AutoGenerateColumns = false;
                DataGridCommon.Refresh();
                DataGridCommon.DataSource = null;
                DataGridCommon.Rows.Clear();
                DataGridCommon.ColumnCount = tap.Columns.Count;
                i = 0;
                foreach (DataColumn column in tap.Columns)
                {
                    DataGridCommon.Columns[i].Name = column.ColumnName;
                    DataGridCommon.Columns[i].HeaderText = column.ColumnName;
                    DataGridCommon.Columns[i].DataPropertyName = column.ColumnName;
                    i = i + 1;
                }
                DataGridCommon.DataSource = tap;
                DataGridCommon.Columns[0].Width = 100;
                DataGridCommon.Columns[1].Width = 100;
                DataGridCommon.Columns[2].Width = 100;
                DataGridCommon.Columns[3].Width = 100;
                DataGridCommon.Columns[4].Visible = false;
                DataGridCommon.Columns[5].Visible = false;
                conn.Close();
            }
            catch (Exception)
            {

                throw;
            }
        }

        private void butcan_Click(object sender, EventArgs e)
        {
            conn.Close();
            conn.Open();




            LoadGetJobCard(1);
        }

        private void txtdcno_TextChanged(object sender, EventArgs e)
        {

        }

        private void Editpnl_Paint(object sender, PaintEventArgs e)
        {

        }

        private void chkact_CheckedChanged(object sender, EventArgs e)
        {
            LoadGetJobCard(1);
        }

        private void txtscr1_TextChanged(object sender, EventArgs e)
        {
            bs.Filter = string.Format("DOcno Like '%{0}%'  or  Docdate Like '%{1}%' or  name Like '%{1}%' or  Reference Like '%{1}%' or  WorkOrderNo Like '%{1}%'  or  ProcessName Like '%{1}%'  or  vehicleno Like '%{1}%' ", txtscr1.Text, txtscr1.Text, txtscr1.Text, txtscr1.Text, txtscr1.Text, txtscr1.Text, txtscr1.Text);

        }

        private void Genpan_Paint(object sender, PaintEventArgs e)
        {

        }

        private void HFGP2_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtscono_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtname.Text == string.Empty)
                {
                    MessageBox.Show("Select Supplier Name", "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtname.Focus();
                    return;
                }
                if (txtmode.Text == string.Empty)
                {
                    MessageBox.Show("Enter Supplier DC Number and Date", "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtmode.Focus();
                    return;
                }
                txtscr11.Text = "";
                type = 11;
                txtscr11.Focus();
                if (Text == "Knitting Without Process")
                {
                    Genclass.strsql = "SP_GETKNITWODCNO   " + txtpuid.Text + "";
                    Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                }
                else

                {
                    Genclass.strsql = "SP_GETFABTWODCNO   " + txtpuid.Text + "";
                    Genclass.cmd = new SqlCommand(Genclass.strsql, conn);

                }
                SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
                DataTable tap = new DataTable();
                aptr.Fill(tap);
                bsc.DataSource = tap;
                Point loc = FindLocation(txtscono);
                grSearch.Location = new Point(loc.X, loc.Y + 20);
                grSearch.Visible = true;
                DataGridCommon.AutoGenerateColumns = false;
                DataGridCommon.Refresh();
                DataGridCommon.DataSource = null;
                DataGridCommon.Rows.Clear();
                DataGridCommon.ColumnCount = tap.Columns.Count;
                i = 0;
                foreach (DataColumn column in tap.Columns)
                {
                    DataGridCommon.Columns[i].Name = column.ColumnName;
                    DataGridCommon.Columns[i].HeaderText = column.ColumnName;
                    DataGridCommon.Columns[i].DataPropertyName = column.ColumnName;
                    i = i + 1;
                }
                DataGridCommon.DataSource = tap;
                DataGridCommon.Columns[0].Visible = true;
                DataGridCommon.Columns[0].Width = 100;
                DataGridCommon.Columns[1].Width = 100;
                DataGridCommon.Columns[2].Width = 100;
                DataGridCommon.Columns[3].Visible = false;
                DataGridCommon.Columns[4].Visible = false;
                DataGridCommon.Columns[5].Visible = false;
                DataGridCommon.Columns[6].Width = 100;
                conn.Close();
            }
            catch (Exception)
            {

                throw;
            }
        }

        private void txtscono_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    if (txtscono.Text != "")
                    {
                        bsc.Filter = string.Format("socno LIKE '%{0}%' ", txtscono.Text);

                    }

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtqty_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bsFabric.Filter = string.Format("docno LIKE '%{0}%' ", txtqty.Text);



                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtqty_Click(object sender, EventArgs e)
        {
            type = 1;
            DataTable dt = getParty();
            bsFabric.DataSource = dt;
            FillGrid2(dt, 1);
            Point loc = FindLocation(txtqty);
            grSearch.Location = new Point(loc.X, loc.Y + 20);
            grSearch.Visible = true;
        }

        private void textBox5_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bsParty.Filter = string.Format("itemname LIKE '%{0}%' ", textBox5.Text);



                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void textBox5_Click(object sender, EventArgs e)
        {

            type = 4;
            Point loc = FindLocation(textBox5);
            grSearch.Location = new Point(loc.X, loc.Y + 20);
            grSearch.Visible = true;
                       componentname();
        }

        private void cbostage_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (cbostage.Text == "Garment")
            {
                string qur = "select distinct  operations ,0 as uid from  PROORDERENTRYM a  inner join PROCDUCTIONENTRYD b on a.uid=b.headid where a.socno='"+ txtqty.Text +"' ";
                SqlCommand cmd = new SqlCommand(qur, conn);
                SqlDataAdapter apt = new SqlDataAdapter(cmd);
                DataTable tab = new DataTable();
                apt.Fill(tab);
                Operations.DataSource = null;
                Operations.DataSource = tab;
                Operations.DisplayMember = "operations";
                Operations.ValueMember = "uid";
                Operations.SelectedIndex = -1;
              
            }
            else if (cbostage.Text == "Component")
            {
                string qur = "select Generalname as operations,guid uid from generalm where   Typemuid in (40) and userid=1 and active=1  order by guid  ";
                SqlCommand cmd = new SqlCommand(qur, conn);
                SqlDataAdapter apt = new SqlDataAdapter(cmd);
                DataTable tab = new DataTable();
                apt.Fill(tab);
                Operations.DataSource = null;
                Operations.DataSource = tab;
                Operations.DisplayMember = "operations";
                Operations.ValueMember = "uid";
                Operations.SelectedIndex = -1;

            }
        }

        private void txtdcqty_TextChanged_1(object sender, EventArgs e)
        {
            txtdcqty_TextChanged(sender, e);
        }

        private void txtdcqty_Click_1(object sender, EventArgs e)
        {
            txtdcqty_Click(sender, e);
        }

        private void txtaddnotes_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bsserial.Filter = string.Format("sizename LIKE '%{0}%' ", txtaddnotes.Text);



                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtaddnotes_Click(object sender, EventArgs e)
        {
      
            if (cbostage.Text == "Garment")
            {
                type = 5;
                size();
            }
            else if (cbostage.Text == "Component")
            {
                type = 21;
                size1();
            }

        }

        private void cbotype_SelectedIndexChanged(object sender, EventArgs e)
        {
            
            string qur = "select distinct  guid as uid,generalname as operations from generalm  where typemuid=37";
            SqlCommand cmd = new SqlCommand(qur, conn);
            SqlDataAdapter apt = new SqlDataAdapter(cmd);
            DataTable tab = new DataTable();
            apt.Fill(tab);
            cbocon.DataSource = null;
            cbocon.DataSource = tab;
            cbocon.DisplayMember = "operations";
            cbocon.ValueMember = "uid";
            cbocon.SelectedIndex = -1;
           
        }

        private void cbooperations_SelectedIndexChanged(object sender, EventArgs e)
        {



            if (cbooperations.Text != "" && cbooperations.ValueMember != "" && cbooperations.DisplayMember != "")
            {
                string qur = "select distinct b.uom,b.qty from PROORDERENTRYM a inner join PROORDERENTRYDPROCESS b on a.uid=b.headid where docno='" + txtsocno.Text + "' and  socno='" + txtqty.Text + "' and b.operations='" + cbooperations.Text + "'";
                SqlCommand cmd = new SqlCommand(qur, conn);
                SqlDataAdapter apt = new SqlDataAdapter(cmd);
                DataTable tab = new DataTable();
                apt.Fill(tab);
                if (tab.Rows.Count > 0)
                {

                    txtuomm.Text = tab.Rows[0]["uom"].ToString();
                    txtuom.Text = tab.Rows[0]["qty"].ToString();
                    txtwok.Text = tab.Rows[0]["qty"].ToString();
                }
            }
        }

        private void HFIT_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {




                int index = HFIT.SelectedCells[0].RowIndex;

                if (mode == 1)
                {
                    HFIT.Rows.RemoveAt(index);
                    HFIT.ClearSelection();
                }
                else if (mode == 2)
                {


                    int i = HFIT.SelectedCells[0].RowIndex;
                    uid = HFIT.Rows[i].Cells[0].Value.ToString();

                    qur.CommandText = "delete from PROCDUCTIONENTRYd where uid=" + HFIT.Rows[i].Cells[0].Value.ToString() + "";
                    qur.ExecuteNonQuery();
                    HFIT.Rows.RemoveAt(index);
                    HFIT.ClearSelection();
                }
            }
        }

        private void HFIT_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void cbocon_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void txtsocno_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bsp.Filter = string.Format("Name LIKE '%{0}%' ", txtsocno.Text);



                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtsocno_Click(object sender, EventArgs e)
        {
            type = 10;
          
            DataTable dt = getParty();
            bsp.DataSource = dt;
            FillGrid3(dt, 1);
            Point loc = FindLocation(txtsocno);
            grSearch.Location = new Point(loc.X, loc.Y + 20);
            grSearch.Visible = true;
        }
    }
}
