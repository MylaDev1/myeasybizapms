﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MyEasyBizAPMS
{
    public partial class FrmItemAttribute : Form
    {
        public FrmItemAttribute()
        {
            InitializeComponent();
            this.sfDataGrid1.Style.HeaderStyle.Font.FontStyle = FontStyle.Bold;
            this.sfDataGrid1.Style.HeaderStyle.BackColor = Color.DeepSkyBlue;
        }
        SqlConnection connection = new SqlConnection(GeneralParameters.ConnectionString);
        SQLDBHelper db = new SQLDBHelper();
        decimal F1 = 0;
        decimal F3 = 0;
        decimal F2 = 0;
        int Active = 0;

        private void FrmItemAttribute_Load(object sender, EventArgs e)
        {
            LoadItemCaegory();
            LoadItemCategoryGrid();
            grFront.Visible = true;
            grBack.Visible = false;
        }

        protected void LoadItemCaegory()
        {
            try
            {
                SqlParameter[] sqlParameters = { new SqlParameter("@TypeMUid", 13), new SqlParameter("@Active", 1) };
                DataTable dt = db.GetDataWithParam(CommandType.StoredProcedure, "SP_GetGeneralM", sqlParameters, connection);
                Cmbcategory.DataSource = null;
                Cmbcategory.DisplayMember = "GeneralName";
                Cmbcategory.ValueMember = "Guid";
                Cmbcategory.DataSource = dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void LoadItemCategoryGrid()
        {
            try
            {
                SqlParameter[] sqlParameters = { new SqlParameter("@TypeMUid", 13), new SqlParameter("@Active", 1) };
                DataTable dt = db.GetDataWithParam(CommandType.StoredProcedure, "SP_GetGeneralM", sqlParameters, connection);
                DataGridItemCategory.DataSource = null;
                DataGridItemCategory.AutoGenerateColumns = false;
                DataGridItemCategory.ColumnCount = 2;


                DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn = new DataGridViewCheckBoxColumn
                {
                    HeaderText = "Chck",
                    Name = "Chck",
                    Width = 50
                };
                DataGridItemCategory.Columns.Insert(0, dataGridViewCheckBoxColumn);

                DataGridItemCategory.Columns[1].Name = "Category";
                DataGridItemCategory.Columns[1].HeaderText = "Category";
                DataGridItemCategory.Columns[1].DataPropertyName = "GeneralName";
                DataGridItemCategory.Columns[1].Width = 200;

                DataGridItemCategory.Columns[2].Name = "Guid";
                DataGridItemCategory.Columns[2].HeaderText = "Guid";
                DataGridItemCategory.Columns[2].DataPropertyName = "Guid";
                DataGridItemCategory.Columns[2].Visible = false;
                DataGridItemCategory.DataSource = dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void SplitAdd_Click(object sender, EventArgs e)
        {
            try
            {
                txtName.Tag = "0";
                grFront.Visible = false;
                grBack.Visible = true;
                txtName.Text = string.Empty;
                txtShortName.Text = string.Empty;
                ChckActive.Checked = true;
                foreach (DataGridViewRow row in DataGridItemCategory.Rows)
                {
                    DataGridItemCategory[0, row.Index].Value = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void SpitButtonSave_Click(object sender, EventArgs e)
        {
            try
            {
                int TypeMuid = 28;
                if (ChckActive.Checked == true)
                {
                    Active = 1;
                }
                else
                {
                    Active = 0;
                }
                if (txtName.Text != string.Empty && txtShortName.Text != string.Empty)
                {
                    foreach (DataGridViewRow row2 in DataGridItemCategory.Rows)
                    {
                        DataGridViewCheckBoxCell cellComponent = row2.Cells[0] as DataGridViewCheckBoxCell;
                        if (cellComponent.Value != null)
                        {
                            if (cellComponent.Value.ToString() == "True")
                            {
                                F1 = 0;
                                F3 = Convert.ToDecimal(row2.Cells[2].Value.ToString());
                                F2 = 0;
                                SqlParameter[] sqlParameters = {
                                    new SqlParameter("@TypeMUid",TypeMuid),
                                    new SqlParameter("@GeneralName",txtName.Text),
                                    new SqlParameter("@ShortName",txtShortName.Text),
                                    new SqlParameter("@F1",F1),
                                    new SqlParameter("@F2",F2),
                                    new SqlParameter("@F3",F3),
                                    new SqlParameter("@CreateDate",DateTime.Now),
                                    new SqlParameter("@UserId",GeneralParameters.UserdId),
                                    new SqlParameter("@GUid",txtName.Tag),
                                    new SqlParameter("@Active",Active)
                                };
                                db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_GeneralM", sqlParameters, connection);
                            }
                        }
                    }
                    MessageBox.Show("Record Saved Successfully", "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    ClearControl();
                    GetGeneralData(28, 1);
                    grFront.Visible = true;
                    grBack.Visible = false;
                }
                else
                {
                    MessageBox.Show("Enter name", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtName.Focus();
                    return;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void ClearControl()
        {
            txtName.Text = string.Empty;
            txtShortName.Text = string.Empty;
            ChckActive.Checked = false;
        }

        protected void GetGeneralData(int TypemUid, int Active)
        {
            try
            {
                SqlParameter[] sqlParameters = { new SqlParameter("@TypeMUid", TypemUid), new SqlParameter("@Active", Active) };
                DataTable dt = db.GetDataWithParam(CommandType.StoredProcedure, "SP_GetGeneralMItemAttribute", sqlParameters, connection);
                DataRow[] dataRows = dt.Select("F3 =" + Cmbcategory.SelectedValue + "");
                DataTable data = new DataTable();
                if (dataRows.Length > 0)
                {
                    data = dt.Select("F3 =" + Cmbcategory.SelectedValue + "").CopyToDataTable();
                }
                else
                {
                    data = dt.Clone();
                }
                sfDataGrid1.DataSource = data;
                sfDataGrid1.Columns[0].Visible = false;
                sfDataGrid1.Columns[1].Visible = false;
                sfDataGrid1.Columns[4].Visible = false;
                sfDataGrid1.Columns[5].Visible = false;
                sfDataGrid1.Columns[6].Visible = false;
                sfDataGrid1.Columns[7].Visible = false;
                sfDataGrid1.Columns[8].Visible = false;
                sfDataGrid1.Columns[9].Visible = false;
                sfDataGrid1.Columns[10].Width = 150;
                sfDataGrid1.Columns[2].Width = 270;
                sfDataGrid1.Columns[3].Width = 200;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void Cmbcategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                GetGeneralData(28, 1);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void SplitAdd_DropDowItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            try
            {
                if (e.ClickedItem.Text == "Edit")
                {
                    try
                    {
                        ClearControl();
                        var selectedItem = sfDataGrid1.SelectedItems[0];
                        var dataRow = (selectedItem as DataRowView).Row;
                        decimal CUid = Convert.ToDecimal(dataRow["GUID"].ToString());
                        txtName.Text = dataRow["Name"].ToString();
                        txtShortName.Text = dataRow["ShortName"].ToString();
                        F1 = Convert.ToDecimal(dataRow["F1"].ToString());
                        string active = dataRow["Active"].ToString();
                        if (active == "True")
                        {
                            Active = 1;
                            ChckActive.Checked = true;
                        }
                        else
                        {
                            Active = 0;
                            ChckActive.Checked = false;
                        }
                        txtName.Tag = dataRow["GUID"].ToString();
                        string cat = dataRow["ItemCatgrory"].ToString();
                        foreach (DataGridViewRow row in DataGridItemCategory.Rows)
                        {
                            object val2 = row.Cells[1].Value;
                            if (val2 != null && val2.ToString() == cat)
                            {
                                DataGridItemCategory[0, row.Index].Value = true;
                            }
                        }
                        grFront.Visible = false;
                        grBack.Visible = true;
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }

                }
                else
                {
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void SpitButtonSave_DropDowItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            try
            {
                if (e.ClickedItem.Text == "Back")
                {
                    grFront.Visible = true;
                    grBack.Visible = false;
                    GetGeneralData(28, 1);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void ChckSelectAll_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (ChckSelectAll.Checked == true)
                {
                    for (int i = 0; i < DataGridItemCategory.RowCount; i++)
                    {
                        DataGridItemCategory[0, i].Value = true;
                    }
                }
                else
                {
                    for (int i = 0; i < DataGridItemCategory.RowCount; i++)
                    {
                        DataGridItemCategory[0, i].Value = false;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }
    }
}

