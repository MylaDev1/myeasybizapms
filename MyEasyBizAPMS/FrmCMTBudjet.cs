﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Drawing.Imaging;
using System.Configuration;
using System.Data.SqlClient;
using System.Diagnostics;
using excel = Microsoft.Office.Interop.Excel;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
namespace MyEasyBizAPMS
{
    public partial class FrmCMTBudjet : Form
    {
        private CrystalDecisions.Windows.Forms.CrystalReportViewer Cryview;

        ReportDocument doc = new ReportDocument();
        public FrmCMTBudjet()
        {
            this.BackColor = Color.White; ;
            InitializeComponent();
        }
        DataGridViewComboBoxCell cbxComboRow6;
        ComboBox cbxRow5;
        string uid = "";
        int mode = 0;
        string tpuid = "";

        int type = 0;
        DateTime str9;
        double sum1;
        double sum2;
        int SP;
        int Dtype = 0;
        int h = 0;
        int i = 0;
        int kk = 0;
        SqlConnection conn = new SqlConnection(GeneralParameters.ConnectionString);
        SqlCommand qur = new SqlCommand();
        private DataRow doc1;
        DataTable Docno = new DataTable();
        BindingSource bs = new BindingSource();
             DataTable Docno1 = new DataTable();
        BindingSource bsc = new BindingSource();
        BindingSource bsp = new BindingSource();
        BindingSource bsserial = new BindingSource();
        BindingSource IN = new BindingSource();
        BindingSource OUT = new BindingSource();
        BindingSource bsFabric = new BindingSource();

        SQLDBHelper db = new SQLDBHelper();
        int Fillid;
        BindingSource bsParty = new BindingSource();
        string str1key;
        public int SelectId = 0;
        private void FrmCMTBudjet_Load(object sender, EventArgs e)
        {
            qur.Connection = conn;
            lkppnl.Visible = false;
            Genclass.buttonstyleform(this);
            Genclass.buttonstylepanel(panadd);
            panadd.Visible = false;
            Genpan.Visible = false;
            Editpnl.Visible = true;

            this.HFGP.DefaultCellStyle.Font = new Font("calibri", 10);
            this.HFGP.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 10, FontStyle.Bold);
            this.HFIT.DefaultCellStyle.Font = new Font("calibri", 10);
            this.HFIT.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 10, FontStyle.Bold);

            this.HFGP1.DefaultCellStyle.Font = new Font("calibri", 10);
            this.HFGP1.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 10, FontStyle.Bold);

            this.HFGP2.DefaultCellStyle.Font = new Font("calibri", 10);
            this.HFGP2.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 10, FontStyle.Bold);

            HFIT.RowHeadersVisible = false;
            HFGP.RowHeadersVisible = false;
            HFGP2.RowHeadersVisible = false;
            HFGP1.RowHeadersVisible = false;




            HFGP2.EnableHeadersVisualStyles = false;
            HFGP2.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.GradientActiveCaption;

            HFIT.EnableHeadersVisualStyles = false;
            HFIT.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.GradientActiveCaption;
            HFGP1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            HFGP1.EnableHeadersVisualStyles = false;
            HFGP1.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.GradientActiveCaption;

            HFGP.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            HFGP.EnableHeadersVisualStyles = false;
            HFGP.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.GradientActiveCaption;


           
                Genclass.Dtype = 1380;


            dtpfnt.Format = DateTimePickerFormat.Custom;
            dtpfnt.CustomFormat = "dd/MM/yyyy";
            dtpgrndt.Format = DateTimePickerFormat.Custom;
            dtpgrndt.CustomFormat = "dd/MM/yyyy";
            dtpsupp.Format = DateTimePickerFormat.Custom;
            dtpsupp.CustomFormat = "dd/MM/yyyy";
            chkact.Checked = true;


            LoadGetJobCard(1);
            Titlep();
        }
        protected DataTable LoadGetJobCard(int tag)
        {

    
            chkact.Checked = true;
            if (chkact.Checked == true)
            {
                SP = 1;

            }
            else
            {
                SP = 0;

            }


            DataTable dt = new DataTable();
            try
            {

                SqlParameter[] para = {
                    //new SqlParameter("@active",SP),
                         new SqlParameter("@doctypeid",Genclass.Dtype),


                };

                dt = db.GetData(CommandType.StoredProcedure, "SP_CMTOPERATIONload", para);


                LoadDataTable(dt);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        protected void LoadDataTable(DataTable dt)
        {
            try
            {
                HFGP.DataSource = null;
                HFGP.AutoGenerateColumns = false;
                HFGP.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
                HFGP.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
                HFGP.ColumnCount = 5;
                HFGP.Columns[0].Name = "Uid";
                HFGP.Columns[0].HeaderText = "Uid";
                HFGP.Columns[0].DataPropertyName = "Uid";

                HFGP.Columns[1].Name = "Docno";
                HFGP.Columns[1].HeaderText = "Docno";
                HFGP.Columns[1].DataPropertyName = "Docno";

                HFGP.Columns[2].Name = "Docdate";
                HFGP.Columns[2].HeaderText = "Docdate";
                HFGP.Columns[2].DataPropertyName = "Docdate";

                HFGP.Columns[3].Name = "SocNo";
                HFGP.Columns[3].HeaderText = "SocNo";
                HFGP.Columns[3].DataPropertyName = "SocNo";

                HFGP.Columns[4].Name = "Style";
                HFGP.Columns[4].HeaderText = "Style";
                HFGP.Columns[4].DataPropertyName = "Style";


               




                bs.DataSource = dt;

                HFGP.DataSource = bs;


                HFGP.Columns[0].Visible = false;
           
          
                HFGP.Columns[1].Width = 90;
                HFGP.Columns[2].Width = 90;
                HFGP.Columns[3].Width = 250;
    
                HFGP.Columns[4].Width = 250;
             
            
              

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        private void button3_Click(object sender, EventArgs e)
        {
            mode = 1;
            Genpan.Visible = false;

            panadd.Visible = false;
            h = 0;
            Genclass.ClearTextBox(this, Editpnl);
            Genclass.STR = "";
            Genclass.Gendocno();
            txtgrn.Text = Genclass.ST;
            Editpnl.Visible = true;
            lkppnl.Visible = false;
            sum1 = 0;
            sum2 = 0;
            txtgrn.Tag = 0;
            HFIT.Refresh();
            HFIT.DataSource = null;
            HFIT.Rows.Clear();

            HFGP1.Refresh();
            HFGP1.DataSource = null;
            HFGP1.Rows.Clear();

            conn.Close();
            conn.Open();
            Docno.Clear();

            button13.Visible = false;
            if (Docno.Columns.Count == 0)
            {
                Docno.Columns.Add("uid");
                Docno.Columns.Add("Docno");
            }
            conn.Close();
            conn.Open();
            Chkedtact.Checked = true;




            HFIT.Refresh();
            HFIT.DataSource = null;
            HFIT.Rows.Clear();
            HFGP1.Refresh();
            HFGP1.DataSource = null;
            HFGP1.Rows.Clear();

            Titlep();


            dtpgrndt.Focus();
        }
        private void Titlep()
        {
            HFIT.AutoGenerateColumns = false;
            HFIT.Refresh();
            HFIT.DataSource = null;
            HFIT.Rows.Clear();
            HFIT.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            HFIT.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            HFIT.ColumnCount = 4;
            HFIT.Columns[0].Name = "uid";
            HFIT.Columns[1].Name = "SocNo";
            HFIT.Columns[2].Name = "Operation";
      
            HFIT.Columns[3].Name = "BudjetRate";
                 

            HFIT.Columns[0].Visible = false;

            HFIT.Columns[1].Visible = false;
            HFIT.Columns[2].Width = 400;
          
            HFIT.Columns[3].Width = 150;
       
            
         
        }
        private void txtname_Click(object sender, EventArgs e)
        {
            type = 1;
            DataTable dt = getParty();
            bsp.DataSource = dt;
            FillGrid2(dt, 1);
            Point loc = FindLocation(txtname);
            grSearch.Location = new Point(loc.X, loc.Y + 20);
            grSearch.Visible = true;
            grSearch.Text = "Name Search";
        }
        protected void FillGrid2(DataTable dt, int FillId)
        {
            try
            {
                DataGridCommon.DataSource = null;
                DataGridCommon.AutoGenerateColumns = false;


                DataGridCommon.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
                DataGridCommon.DefaultCellStyle.WrapMode = DataGridViewTriState.True;


                DataGridCommon.ColumnCount = 3;
                DataGridCommon.Columns[0].Name = "uid";
                DataGridCommon.Columns[0].HeaderText = "uid";
                DataGridCommon.Columns[0].DataPropertyName = "uid";
                DataGridCommon.Columns[1].Name = "OrderNO";
                DataGridCommon.Columns[1].HeaderText = "Socno";
                DataGridCommon.Columns[1].DataPropertyName = "OrderNO";
                DataGridCommon.Columns[1].Width = 100;
                DataGridCommon.Columns[2].Name = "StyleName";
                DataGridCommon.Columns[2].HeaderText = "StyleName";
                DataGridCommon.Columns[2].DataPropertyName = "StyleName";
                DataGridCommon.Columns[2].Width = 100;
  

                DataGridCommon.DataSource = bsFabric;
                DataGridCommon.Columns[0].Visible = false;
             


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        private void txtname_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.F10)
                {
                    grSearch.Visible = false;
                }
                else if (e.KeyCode == Keys.F2)
                {
                    SelectId = 1;
                    int Index = DataGridCommon.SelectedCells[0].RowIndex;

                    txtname.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtpuid.Text = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    //txtmillid.Text = DataGridCommon.Rows[Index].Cells[4].Value.ToString();
                    txtmode.Focus();
                    grSearch.Visible = false;
                    SelectId = 0;
                }
                else if (e.KeyCode == Keys.Escape)
                {
                    grSearch.Visible = false;
                }
                else if (e.KeyValue == 40)
                {
                    DataGridCommon.Select();
                }
                else if (e.KeyCode == Keys.Enter)
                {
                    txtname_Click(sender, e);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtname_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bsp.Filter = string.Format("Name LIKE '%{0}%' ", txtname.Text);



                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtdcqty_Click(object sender, EventArgs e)
        {
           
            
            type = 3;
            Point loc = FindLocation(txtdcqty);
            grSearch.Location = new Point(loc.X, loc.Y + 20);
            grSearch.Visible = true;
            comboname();


        }
        private void comboname()

        {

            string qur1 = "select DETTABLE_UID as uid,A.COMBONAME from WIP_STOCK_LEDGER A LEFT JOIN CUTTINGRECEIPTD B ON A.DETTABLE_UID=B.UID GROUP BY  DETTABLE_UID ,A.COMBONAME,A.qty  HAVING A.qty-isnull(sum(b.QTY),0)>0  ";
            SqlCommand cmd1 = new SqlCommand(qur1, conn);
            SqlDataAdapter apt1 = new SqlDataAdapter(cmd1);
            DataTable tab1 = new DataTable();
            apt1.Fill(tab1);
            bsp.DataSource = tab1;
            //Point loc = FindLocation(txtdcqty);
            //grSearch.Location = new Point(loc.X, loc.Y + 20);
            grSearch.Visible = true;
            DataGridCommon.AutoGenerateColumns = false;
            DataGridCommon.Refresh();
            DataGridCommon.DataSource = null;
            DataGridCommon.Rows.Clear();
            DataGridCommon.ColumnCount = tab1.Columns.Count;
            i = 0;
            foreach (DataColumn column in tab1.Columns)
            {
                DataGridCommon.Columns[i].Name = column.ColumnName;
                DataGridCommon.Columns[i].HeaderText = column.ColumnName;
                DataGridCommon.Columns[i].DataPropertyName = column.ColumnName;
                i = i + 1;
            }

            DataGridCommon.DataSource = tab1;

            DataGridCommon.Columns[1].Width = 300;

            DataGridCommon.Columns[0].Visible = false;



        }
        private void componentname()

        {

            string qur1 = "select DETTABLE_UID as uid,a.COMPONENT +' / '+ a.size as ItemName,a.Size,a.uom,a.Weight from WIP_STOCK_LEDGER A LEFT JOIN CUTTINGRECEIPTD B ON A.DETTABLE_UID=B.UID   where dettable_uid=" + txtdcqty.Tag + " GROUP BY  DETTABLE_UID,a.COMPONENT ,a.Size,a.uom,a.Weight,A.qty  HAVING A.qty-isnull(sum(b.QTY),0)>0   ";
            SqlCommand cmd1 = new SqlCommand(qur1, conn);
            SqlDataAdapter apt1 = new SqlDataAdapter(cmd1);
            DataTable tab1 = new DataTable();
            apt1.Fill(tab1);
            bsParty.DataSource = tab1;
            //Point loc = FindLocation(txtdcqty);
            //grSearch.Location = new Point(loc.X, loc.Y + 20);
            grSearch.Visible = true;
            DataGridCommon.AutoGenerateColumns = false;
            DataGridCommon.Refresh();
            DataGridCommon.DataSource = null;
            DataGridCommon.Rows.Clear();
            DataGridCommon.ColumnCount = tab1.Columns.Count;
            i = 0;
            foreach (DataColumn column in tab1.Columns)
            {
                DataGridCommon.Columns[i].Name = column.ColumnName;
                DataGridCommon.Columns[i].HeaderText = column.ColumnName;
                DataGridCommon.Columns[i].DataPropertyName = column.ColumnName;
                i = i + 1;
            }

            DataGridCommon.DataSource = tab1;

            DataGridCommon.Columns[1].Width = 300;

            DataGridCommon.Columns[0].Visible = false;
            DataGridCommon.Columns[2].Visible = false;
            DataGridCommon.Columns[3].Visible = false;
            DataGridCommon.Columns[4].Visible = false;

        }
        protected void FillGrid(DataTable dt, int FillId)
        {
            try
            {
                DataGridCommon.DataSource = null;
                DataGridCommon.AutoGenerateColumns = false;


                DataGridCommon.ColumnCount = 4;
                DataGridCommon.Columns[0].Name = "uid";
                DataGridCommon.Columns[0].HeaderText = "uid";
                DataGridCommon.Columns[0].DataPropertyName = "uid";
                DataGridCommon.Columns[1].Name = "Itemname";
                DataGridCommon.Columns[1].HeaderText = "Itemname";
                DataGridCommon.Columns[1].DataPropertyName = "Itemname";
                DataGridCommon.Columns[1].Width = 300;
                DataGridCommon.Columns[2].Name = "UOMID";
                DataGridCommon.Columns[2].HeaderText = "UOMID";
                DataGridCommon.Columns[2].DataPropertyName = "UOMID";

                DataGridCommon.Columns[3].Name = "GENERALNAME";
                DataGridCommon.Columns[3].HeaderText = "GENERALNAME";
                DataGridCommon.Columns[3].DataPropertyName = "GENERALNAME";



                DataGridCommon.DataSource = bsParty;
                DataGridCommon.Columns[0].Visible = false;
                DataGridCommon.Columns[2].Visible = false;
                DataGridCommon.Columns[3].Visible = false;



            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private Point FindLocation(Control ctrl)
        {
            if (ctrl.Parent is Form)
                return ctrl.Location;
            else
            {
                Point p = FindLocation(ctrl.Parent);
                p.X += ctrl.Location.X;
                p.Y += ctrl.Location.Y;
                return p;
            }
        }
        protected DataTable getParty()
        {
            DataTable dt = new DataTable();
            try
            {
                if (type == 1)
                {
                    dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "sp_getorderno", conn);
                    bsFabric.DataSource = dt;
                }
                else if (type == 1 && Text == "Fabric Without Process")
                {
                    dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "SP_FABWOPROCESSSUPP", conn);
                    bsp.DataSource = dt;
                }
                else if (type == 2 && Text == "Knitting Without Process")
                {
                    //dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "sp_getwo", conn);
                    //bsc1.DataSource = dt;
                }
                else if (type == 2 && Text == "Fabric Without Process")
                {
                    //dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "sp_getwo", conn);
                    //bsc1.DataSource = dt;
                }



            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            return dt;
        }
        private void txtdcqty_KeyDown(object sender, KeyEventArgs e)
        {

            try
            {
                if (e.KeyCode == Keys.F10)
                {
                    grSearch.Visible = false;
                }
                else if (e.KeyCode == Keys.F2)
                {
                    SelectId = 1;
                    int Index = DataGridCommon.SelectedCells[0].RowIndex;
                    txtdcqty.Text = HFGP2.Rows[Index].Cells[1].Value.ToString();
                    txtdcqty.Tag = HFGP2.Rows[Index].Cells[0].Value.ToString();
                    txtuom.Text = HFGP2.Rows[Index].Cells[2].Value.ToString();
                    txtitemid.Text = HFGP2.Rows[Index].Cells[3].Value.ToString();
                    txtprice.Text = HFGP2.Rows[Index].Cells[4].Value.ToString();
                    txtprice.Tag = HFGP2.Rows[Index].Cells[5].Value.ToString();
                    txtisstot.Text = HFGP2.Rows[Index].Cells[6].Value.ToString();

                    txtitem.Focus();
                    grSearch.Visible = false;
                    SelectId = 0;
                }
                else if (e.KeyCode == Keys.Escape)
                {
                    grSearch.Visible = false;
                }
                else if (e.KeyValue == 40)
                {
                    DataGridCommon.Select();
                }
                else if (e.KeyCode == Keys.Enter)
                {
                    txtdcqty_Click(sender, e);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtdcqty_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bsp.Filter = string.Format("comboname LIKE '%{0}%' ", txtdcqty.Text);



                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtitem_Click(object sender, EventArgs e)        {
            type = 2;
            DataTable dt = getParty();
            //bsc1.DataSource = dt;
            FillGrid3(dt, 1);
            Point loc = FindLocation(txtitem);
            grSearch.Location = new Point(loc.X, loc.Y + 20);
            grSearch.Visible = true;
            grSearch.Text = "Name Search";
        }

        private void txtitem_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.F10)
                {
                    grSearch.Visible = false;
                }
                else if (e.KeyCode == Keys.F2)
                {
                    SelectId = 1;
                    int Index = DataGridCommon.SelectedCells[0].RowIndex;

                    txtitem.Text = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    txtitem.Tag = DataGridCommon.Rows[Index].Cells[1].Value.ToString();

                    txtmode.Focus();
                    lkppnl.Visible = false;
                    SelectId = 0;
                }
                else if (e.KeyCode == Keys.Escape)
                {
                    grSearch.Visible = false;
                }
                else if (e.KeyValue == 40)
                {
                    DataGridCommon.Select();
                }
                else if (e.KeyCode == Keys.Enter)
                {
                    txtitem_Click(sender, e);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
        protected void FillGrid3(DataTable dt, int FillId)
        {
            try
            {
                DataGridCommon.DataSource = null;
                DataGridCommon.AutoGenerateColumns = false;


                DataGridCommon.ColumnCount = 4;
                DataGridCommon.Columns[0].Name = "Uid";
                DataGridCommon.Columns[0].HeaderText = "Uid";
                DataGridCommon.Columns[0].DataPropertyName = "Uid";

                DataGridCommon.Columns[1].Name = "docno";
                DataGridCommon.Columns[1].HeaderText = "Docno";
                DataGridCommon.Columns[1].DataPropertyName = "docno";
                DataGridCommon.Columns[1].Width = 150;


                DataGridCommon.Columns[2].Name = "processname";
                DataGridCommon.Columns[2].HeaderText = "processname";
                DataGridCommon.Columns[2].DataPropertyName = "processname";




                DataGridCommon.Columns[3].Name = "processid";
                DataGridCommon.Columns[3].HeaderText = "processid";
                DataGridCommon.Columns[3].DataPropertyName = "processid";






                //DataGridCommon.DataSource = bsc1;
                DataGridCommon.Columns[0].Visible = false;
                DataGridCommon.Columns[2].Visible = false;
                DataGridCommon.Columns[3].Visible = false;



            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        private void txtitem_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bsp.Filter = string.Format("docno LIKE '%{0}%' ", txtvehicle.Text);



                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void button18_Click(object sender, EventArgs e)
        {
            try
            {
                SelectId = 1;
                int Index = DataGridCommon.SelectedCells[0].RowIndex;
                if (type == 1)
                {
                    txtqty.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtqty.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    txtsocno.Text = DataGridCommon.Rows[Index].Cells[2].Value.ToString();
        
                    loadprocess();
                }
               
                grSearch.Visible = false;
                SelectId = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
        private void loadprocess()
        {
            if (txtqty.Text != "")
            {
                HFIT.AutoGenerateColumns = false;
                HFIT.Refresh();
                HFIT.DataSource = null;
                HFIT.Rows.Clear();

                HFIT.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
                HFIT.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
                string qur = " exec sp_CMTRptSOcNODetails '" + txtqty.Text + "'";
                SqlCommand cmd = new SqlCommand(qur, conn);
                SqlDataAdapter apt1 = new SqlDataAdapter(cmd);
                DataTable tab1 = new DataTable();
                apt1.Fill(tab1);

          

                if (tab1.Rows.Count > 0)

                {
                    for (int k = 0; k < tab1.Rows.Count; k++)
                    {


                        var index = HFIT.Rows.Add();
                        HFIT.Rows[index].Cells[0].Value = tab1.Rows[k]["uid"].ToString();
                        HFIT.Rows[index].Cells[1].Value = tab1.Rows[k]["SocNo"].ToString();
                        HFIT.Rows[index].Cells[2].Value = tab1.Rows[k]["process"].ToString();
                        HFIT.Rows[index].Cells[3].Value = tab1.Rows[k]["budjetRate"].ToString();
                        //HFIT.Rows[index].Cells[4].Value = tab1.Rows[k]["Actualrate"].ToString();
                        //HFIT.Rows[index].Cells[5].Value = tab1.Rows[k]["Status"].ToString();


                    }

                    

                }




            }

        }

        private void loadallitems()

        {

            if (txtname.Text == string.Empty)
            {
                MessageBox.Show("Select Supplier", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtname.Focus();
                return;
            }
            lkppnl.Visible = true;
            txtscr11.Text = "";
            txtscr11.Focus();


            if (Text == "Knitting Without Process")
            {
                Genclass.strsql = "SP_GETKNITRECWOITEMS  " + txtpuid.Text + ",'" + txtscono.Text + "' ,'" + txtwok.Text + "','" + txtwo.Text + "' ";
                Genclass.FSSQLSortStr = "Itemdes";
            }
            else

            {

                Genclass.strsql = "SP_GETFABRECWOITEMS  " + txtpuid.Text + ",'" + txtscono.Text + "' ,'" + txtwok.Text + "','" + txtwo.Text + "' ";
                Genclass.FSSQLSortStr = "Itemdes";
            }


            Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
            SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
            DataTable tap = new DataTable();
            aptr.Fill(tap);

            bsc.DataSource = tap;

            HFGP2.AutoGenerateColumns = false;
            HFGP2.Refresh();
            HFGP2.DataSource = null;
            HFGP2.Rows.Clear();
            HFGP2.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            HFGP2.DefaultCellStyle.WrapMode = DataGridViewTriState.True;


            HFGP2.ColumnCount = tap.Columns.Count;
            i = 0;

            foreach (DataColumn column in tap.Columns)
            {
                HFGP2.Columns[i].Name = column.ColumnName;
                HFGP2.Columns[i].HeaderText = column.ColumnName;
                HFGP2.Columns[i].DataPropertyName = column.ColumnName;
                i = i + 1;
            }
            HFGP2.DataSource = tap;

            HFGP2.Columns[0].Visible = false;
            HFGP2.Columns[2].Width = 100;

            HFGP2.Columns[3].Width = 100;
            HFGP2.Columns[4].Visible = false;
            HFGP2.Columns[5].Visible = false;
            HFGP2.Columns[6].Visible = false;
            HFGP2.Columns[1].Width = 250;
            HFGP2.Columns[7].Visible = false;
            HFGP2.Columns[8].Width = 100;
            HFGP2.Columns[9].Width = 200;
            HFGP2.Columns[10].Visible = false;
            conn.Close();
        }
        private void btnHide_Click(object sender, EventArgs e)
        {
            grSearch.Visible = false;
        }

        private void DataGridCommon_DoubleClick(object sender, EventArgs e)
        {
            SelectId = 1;
            int Index = DataGridCommon.SelectedCells[0].RowIndex;
            if (type == 1)
            {
                txtqty.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                txtqty.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                txtsocno.Text = DataGridCommon.Rows[Index].Cells[2].Value.ToString();
                loadprocess();
            }
            else if (type == 3)
            {
                txtdcqty.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                txtdcqty.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                textBox5.Focus();
            }
            else if (type == 4)
            {
                textBox5.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                textBox5.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                txtaddnotes.Text = DataGridCommon.Rows[Index].Cells[2].Value.ToString();
                txtprice.Text = DataGridCommon.Rows[Index].Cells[3].Value.ToString();
                txtuom.Text = DataGridCommon.Rows[Index].Cells[4].Value.ToString();


            }
            grSearch.Visible = false;
            SelectId = 0;
        }

        private void DataGridCommon_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                int Index = DataGridCommon.SelectedCells[0].RowIndex;
                if (type == 1)
                {
                    txtqty.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtqty.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    txtsocno.Text = DataGridCommon.Rows[Index].Cells[2].Value.ToString();
                    loadprocess();
                }
                else if (type == 3)
                {
                    txtdcqty.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtdcqty.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    textBox5.Focus();
                }
                else if (type == 4)
                {
                    textBox5.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    textBox5.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtaddnotes.Text = DataGridCommon.Rows[Index].Cells[2].Value.ToString();
                    txtprice.Text = DataGridCommon.Rows[Index].Cells[3].Value.ToString();
                    txtuom.Text = DataGridCommon.Rows[Index].Cells[4].Value.ToString();


                }
                grSearch.Visible = false;
                SelectId = 0;
            }
        }

        private void button10_Click(object sender, EventArgs e)
        {

            int Index = HFGP2.SelectedCells[0].RowIndex;
            txtdcqty.Text = HFGP2.Rows[Index].Cells[1].Value.ToString();
            txtdcqty.Tag = HFGP2.Rows[Index].Cells[0].Value.ToString();
            txtuom.Text = HFGP2.Rows[Index].Cells[3].Value.ToString();
            txtoutputid.Text = HFGP2.Rows[Index].Cells[3].Value.ToString();
            txtitemid.Text = HFGP2.Rows[Index].Cells[4].Value.ToString();
            txtprice.Text = HFGP2.Rows[Index].Cells[5].Value.ToString();
            txtprice.Tag = HFGP2.Rows[Index].Cells[6].Value.ToString();
            txtisstot.Text = HFGP2.Rows[Index].Cells[7].Value.ToString();
            txtisstot.Tag = HFGP2.Rows[Index].Cells[10].Value.ToString();
            lkppnl.Visible = false;
            txtaddnotes.Focus();
        }

        private void HFGP2_DoubleClick(object sender, EventArgs e)
        {
            int Index = HFGP2.SelectedCells[0].RowIndex;

            txtdcqty.Text = HFGP2.Rows[Index].Cells[1].Value.ToString();
            txtdcqty.Tag = HFGP2.Rows[Index].Cells[0].Value.ToString();
            txtuom.Text = HFGP2.Rows[Index].Cells[3].Value.ToString();
            txtoutputid.Text = HFGP2.Rows[Index].Cells[3].Value.ToString();
            txtitemid.Text = HFGP2.Rows[Index].Cells[4].Value.ToString();
            txtprice.Text = HFGP2.Rows[Index].Cells[5].Value.ToString();
            txtprice.Tag = HFGP2.Rows[Index].Cells[6].Value.ToString();
            txtisstot.Text = HFGP2.Rows[Index].Cells[7].Value.ToString();
            txtisstot.Tag = HFGP2.Rows[Index].Cells[10].Value.ToString();
            lkppnl.Visible = false;
            txtaddnotes.Focus();
        }

        private void HFGP2_KeyDown(object sender, KeyEventArgs e)
        {

            if (e.KeyCode == Keys.Enter)
            {
                int Index = HFGP2.SelectedCells[0].RowIndex;
                txtdcqty.Text = HFGP2.Rows[Index].Cells[1].Value.ToString();
                txtdcqty.Tag = HFGP2.Rows[Index].Cells[0].Value.ToString();
                txtuom.Text = HFGP2.Rows[Index].Cells[3].Value.ToString();
                txtoutputid.Text = HFGP2.Rows[Index].Cells[3].Value.ToString();
                txtitemid.Text = HFGP2.Rows[Index].Cells[4].Value.ToString();
                txtprice.Text = HFGP2.Rows[Index].Cells[5].Value.ToString();
                txtprice.Tag = HFGP2.Rows[Index].Cells[6].Value.ToString();
                txtisstot.Text = HFGP2.Rows[Index].Cells[7].Value.ToString();
                txtisstot.Tag = HFGP2.Rows[Index].Cells[10].Value.ToString();
                lkppnl.Visible = false;
                txtaddnotes.Focus();
            }
        }

        private void buttnfinbk_Click(object sender, EventArgs e)
        {
            Editpnl.Visible = false;
            Genpan.Visible = true;
            panadd.Visible = true;
            LoadGetJobCard(1);
        }

        private void btnadd_Click(object sender, EventArgs e)
        {
            if (txtdcqty.Text == "")
            {
                MessageBox.Show("Enter the Item");
                txtdcqty.Focus();
                return;
            }
            if (txtaddnotes.Text == "")
            {
                MessageBox.Show("Enter the Batchno");
                txtaddnotes.Focus();
                return;
            }
            
            if (textBox1.Text == "")
            {
                MessageBox.Show("Enter the rolls");
                textBox1.Focus();
                return;
            }
            if (txtuom.Text == "")
            {
                MessageBox.Show("Enter the Qty");
                txtuom.Focus();
                return;
            }


            //if (tt==pp || tt>pp || ll == pp)
            //{


            //}
            //else if(ll < pp)

            //{
            //    MessageBox.Show("Qty Exceed 10%");
            //    return;

            //}


            kk = HFIT.Rows.Count - 1;
            h = kk;


          
                h = h + 1;
                var index = HFIT.Rows.Add();
                HFIT.Rows[index].Cells[0].Value = "0";
                HFIT.Rows[index].Cells[1].Value = txtdcqty.Text;

                HFIT.Rows[index].Cells[2].Value = textBox5.Text;
                HFIT.Rows[index].Cells[3].Value = txtaddnotes.Text;

                HFIT.Rows[index].Cells[4].Value = txtprice.Text;
                HFIT.Rows[index].Cells[5].Value = txtuom.Text;

                HFIT.Rows[index].Cells[6].Value = textBox1.Text;
                HFIT.Rows[index].Cells[7].Value = txtdcqty.Tag;

                HFIT.Rows[index].Cells[8].Value = textBox5.Tag;
            HFIT.Rows[index].Cells[9].Value = cbocuttype.Text;

            txtdcqty.Text = "";
            txtstyle.Text = "";

            txtaddnotes.Text = "";
            txtbags.Text = "";
            txtuom.Text = "";
            textBox5.Text = "";
            txtprice.Text = "";
            txtdcqty.Text = "";
            textBox1.Text = "";



            txtdcqty.Focus();
        }

        private void btnsave_Click(object sender, EventArgs e)
        {
            conn.Close();
            conn.Open();
            

    
            if (Chkedtact.Checked == true)
            {

                SP = 1;
            }
            else
            {
                SP = 0;

            }
            SqlParameter[] para ={

               

                    new SqlParameter("@uid",txtgrn.Tag),
                    new SqlParameter("@DOCNO",txtgrn.Text),
                    new SqlParameter("@DOCDATE",Convert.ToDateTime(dtpgrndt.Text)),
                    new SqlParameter("@SOCNO",txtqty.Text),
                    new SqlParameter("@STYLE",txtsocno.Text),
                   
                   



            };
            DataTable dataTable = db.GetDataWithParam(CommandType.StoredProcedure, "SP_CMTOPERATIONM", para, conn);
            int ResponseCode = Convert.ToInt32(dataTable.Rows[0]["ResponseCode"].ToString());
            int ReturnId = Convert.ToInt32(dataTable.Rows[0]["ReturnId"].ToString());
            //db.ExecuteNonQuery(CommandType.StoredProcedure, "sp_cuttingEntryM", para, conn);
            //txtgrnid.Text= Convert.ToInt32(dataTable.Rows[0]["ReturnId"].ToString());


          

            for (int i = 0; i < HFIT.RowCount - 1; i++)
            {

                conn.Close();
                conn.Open();

                SqlParameter[] para1 ={

                   
            new SqlParameter("@uid", HFIT.Rows[i].Cells[0].Value),
                   new SqlParameter("@HEADID",ReturnId),
                    new SqlParameter("@SOCNO",HFIT.Rows[i].Cells[1].Value),
                    new SqlParameter("@OPERAIONS", HFIT.Rows[i].Cells[2].Value),
                    new SqlParameter("@BUDJETRATE", HFIT.Rows[i].Cells[3].Value),
                    new SqlParameter("@ACTUALRATE", HFIT.Rows[i].Cells[4].Value),
                              new SqlParameter("@STATUS", HFIT.Rows[i].Cells[5].Value),
                   


                };

                db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_CMTOPERATIOND", para1, conn);
            }



            conn.Close();
            conn.Open();
            if (mode == 1)
            {
                qur.CommandText = "update doctypem set lastno=lastno+1 where doctypeid=" + Genclass.Dtype + " and finyear='19-20'";
                qur.ExecuteNonQuery();

            }
            //conn.Close();
            //conn.Open();
            //if (ReturnId != 0)
            //{
            //    if (mode == 1)
            //    {
            //        qur.CommandText = "exec SP_stocklegCUttingReceipt " + ReturnId + "," + Genclass.Dtype + "   ,1";
            //        qur.ExecuteNonQuery();
            //        qur.CommandText = "exec SP_PRODUCTIONISSUE_WIP_STOCK_LEDGER " + ReturnId + "  ,1," + Genclass.Dtype + " ";
            //        qur.ExecuteNonQuery();

            //    }
            //    else
            //    {
            //        qur.CommandText = "exec SP_stocklegCUttingReceipt " + ReturnId + "," + Genclass.Dtype + "   ,2";
            //        qur.ExecuteNonQuery();
            //        qur.CommandText = "exec SP_PRODUCTIONISSUE_WIP_STOCK_LEDGER " + ReturnId + ",2," + Genclass.Dtype + " ";
            //        qur.ExecuteNonQuery();

            //    }

            //}
            MessageBox.Show("Save Record");
            Editpnl.Visible = false;

            Genpan.Visible = true;
            panadd.Visible = true;
            LoadGetJobCard(1);



        }

        private void butedit_Click(object sender, EventArgs e)
        {
            mode = 2;
            Chkedtact.Checked = true;
            mode = 2;
            panadd.Visible = false;
            Genpan.Visible = false;
            Editpnl.Visible = true;
            lkppnl.Visible = false;
            conn.Close();
            conn.Open();
          

            //cbosGReturnItem.Items.Clear();
            int i = HFGP.SelectedCells[0].RowIndex;
            uid = HFGP.Rows[i].Cells[0].Value.ToString();
            txtgrnid.Text = HFGP.Rows[i].Cells[0].Value.ToString();
            txtgrn.Tag = Convert.ToInt32(HFGP.Rows[i].Cells[0].Value.ToString());
            txtgrn.Text = HFGP.Rows[i].Cells[1].Value.ToString();
            dtpgrndt.Text = HFGP.Rows[i].Cells[2].Value.ToString();

            txtqty.Text = HFGP.Rows[i].Cells[3].Value.ToString();
            txtsocno.Text = HFGP.Rows[i].Cells[4].Value.ToString();

         
            Chkedtact.Checked = true;
            HFIT.Refresh();
            HFIT.DataSource = null;
            HFIT.Rows.Clear();

            HFGP2.Refresh();
            HFGP2.DataSource = null;
            HFGP2.Rows.Clear();
            HFGP1.Refresh();
            HFGP1.DataSource = null;
            HFGP1.Rows.Clear();

            Titlep();
            load();

        }
        private void load()
        {
            Genclass.strsql = "CMTOPERATIONEDIT " + txtgrnid.Text + " ";
            Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
            SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
            DataTable tap1 = new DataTable();
            aptr1.Fill(tap1);

            Genclass.sum1 = 0;


            for (int k = 0; k < tap1.Rows.Count; k++)
            {
                var index = HFIT.Rows.Add();
               
                HFIT.Rows[index].Cells[0].Value = tap1.Rows[k]["uid"].ToString();

                HFIT.Rows[index].Cells[1].Value = tap1.Rows[k]["SOCNO"].ToString();


                HFIT.Rows[index].Cells[2].Value = tap1.Rows[k]["OPERAIONS"].ToString();

                HFIT.Rows[index].Cells[3].Value = tap1.Rows[k]["BUDJETRATE"].ToString();

                HFIT.Rows[index].Cells[4].Value = tap1.Rows[k]["ACTUALRATE"].ToString();


                HFIT.Rows[index].Cells[5].Value = tap1.Rows[k]["STATUS"].ToString();
             
              

            }
        }

        private void dtpgrndt_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                e.Handled = true;
                SendKeys.Send("{TAB}");
            }
        }

        private void txtqty_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                e.Handled = true;
                SendKeys.Send("{TAB}");
            }
        }

        private void txtmode_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                e.Handled = true;
                SendKeys.Send("{TAB}");
            }
        }

        private void txtvehicle_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                e.Handled = true;
                SendKeys.Send("{TAB}");
            }
        }

        private void txtdcqty_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                e.Handled = true;
                SendKeys.Send("{TAB}");
            }
        }

        private void txtaddnotes_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                e.Handled = true;
                SendKeys.Send("{TAB}");
            }
        }

        private void txtisstot_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                e.Handled = true;
                SendKeys.Send("{TAB}");
            }
        }

        private void txtbags_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                e.Handled = true;
                SendKeys.Send("{TAB}");
            }
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                e.Handled = true;
                SendKeys.Send("{TAB}");
            }
        }

        private void txtprice_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                e.Handled = true;
                SendKeys.Send("{TAB}");
            }

        }

        private void txtuom_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void button11_Click(object sender, EventArgs e)
        {
            lkppnl.Visible = false;
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void buttnext1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void DataGridCommon_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dtpsupp_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                e.Handled = true;
                SendKeys.Send("{TAB}");
            }
        }

        private void txtscr11_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.F10)
                {
                    lkppnl.Visible = false;
                }
                else if (e.KeyCode == Keys.F2)
                {
                    SelectId = 1;


                    int Index = HFGP2.SelectedCells[0].RowIndex;
                    txtdcqty.Text = HFGP2.Rows[Index].Cells[1].Value.ToString();
                    txtdcqty.Tag = HFGP2.Rows[Index].Cells[0].Value.ToString();
                    txtuom.Text = HFGP2.Rows[Index].Cells[3].Value.ToString();
                    txtitemid.Text = HFGP2.Rows[Index].Cells[4].Value.ToString();
                    txtprice.Text = HFGP2.Rows[Index].Cells[5].Value.ToString();
                    txtprice.Tag = HFGP2.Rows[Index].Cells[6].Value.ToString();
                    txtisstot.Text = HFGP2.Rows[Index].Cells[7].Value.ToString();
                    lkppnl.Visible = false;
                    txtaddnotes.Focus();

                    SelectId = 0;
                }
                else if (e.KeyCode == Keys.Escape)
                {
                    lkppnl.Visible = false;
                }
                else if (e.KeyValue == 40)
                {
                    HFGP2.Select();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtscr11_TextChanged(object sender, EventArgs e)
        {
            bsc.Filter = string.Format("itemdes LIKE '%{0}%' ", txtscr11.Text);
        }

        private void txtuom_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtuom_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)

            {
                btnadd_Click(sender, e);

            }
        }

        private void button13_Click(object sender, EventArgs e)
        {

        }

        private void txtdcno_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtname.Text == string.Empty)
                {
                    MessageBox.Show("Select Supplier Name", "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtname.Focus();
                    return;
                }
                if (txtmode.Text == string.Empty)
                {
                    MessageBox.Show("Enter Supplier DC Number and Date", "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtmode.Focus();
                    return;
                }
                txtscr11.Text = "";
                type = 10;
                txtscr11.Focus();
                Genclass.strsql = "SP_GETKNITRECDCNO   " + txtpuid.Text + "";
                Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
                DataTable tap = new DataTable();
                aptr.Fill(tap);
                bsc.DataSource = tap;
                Point loc = FindLocation(txtdcno);
                grSearch.Location = new Point(loc.X, loc.Y + 20);
                grSearch.Visible = true;
                DataGridCommon.AutoGenerateColumns = false;
                DataGridCommon.Refresh();
                DataGridCommon.DataSource = null;
                DataGridCommon.Rows.Clear();
                DataGridCommon.ColumnCount = tap.Columns.Count;
                i = 0;
                foreach (DataColumn column in tap.Columns)
                {
                    DataGridCommon.Columns[i].Name = column.ColumnName;
                    DataGridCommon.Columns[i].HeaderText = column.ColumnName;
                    DataGridCommon.Columns[i].DataPropertyName = column.ColumnName;
                    i = i + 1;
                }
                DataGridCommon.DataSource = tap;
                DataGridCommon.Columns[0].Width = 100;
                DataGridCommon.Columns[1].Width = 100;
                DataGridCommon.Columns[2].Width = 100;
                DataGridCommon.Columns[3].Width = 100;
                DataGridCommon.Columns[4].Visible = false;
                DataGridCommon.Columns[5].Visible = false;
                conn.Close();
            }
            catch (Exception)
            {

                throw;
            }
        }

        private void butcan_Click(object sender, EventArgs e)
        {
            conn.Close();
            conn.Open();



            int i = HFGP.SelectedCells[0].RowIndex;

            qur.CommandText = "delete  from  CMTOPERATIONd  where headid=" + HFGP.Rows[i].Cells[0].Value.ToString() + "";
            qur.ExecuteNonQuery();

            qur.CommandText = "delete  from  CMTOPERATIONm  where uid=" + HFGP.Rows[i].Cells[0].Value.ToString() + "";
            qur.ExecuteNonQuery();

        

            LoadGetJobCard(1);
        }

        private void txtdcno_TextChanged(object sender, EventArgs e)
        {

        }

        private void Editpnl_Paint(object sender, PaintEventArgs e)
        {

        }

        private void chkact_CheckedChanged(object sender, EventArgs e)
        {
            LoadGetJobCard(1);
        }

        private void txtscr1_TextChanged(object sender, EventArgs e)
        {
            bs.Filter = string.Format("DOcno Like '%{0}%'  or  Docdate Like '%{1}%' or  name Like '%{1}%' or  Reference Like '%{1}%' or  WorkOrderNo Like '%{1}%'  or  ProcessName Like '%{1}%'  or  vehicleno Like '%{1}%' ", txtscr1.Text, txtscr1.Text, txtscr1.Text, txtscr1.Text, txtscr1.Text, txtscr1.Text, txtscr1.Text);

        }

        private void Genpan_Paint(object sender, PaintEventArgs e)
        {

        }

        private void HFGP2_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtscono_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtname.Text == string.Empty)
                {
                    MessageBox.Show("Select Supplier Name", "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtname.Focus();
                    return;
                }
                if (txtmode.Text == string.Empty)
                {
                    MessageBox.Show("Enter Supplier DC Number and Date", "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtmode.Focus();
                    return;
                }
                txtscr11.Text = "";
                type = 11;
                txtscr11.Focus();
                if (Text == "Knitting Without Process")
                {
                    Genclass.strsql = "SP_GETKNITWODCNO   " + txtpuid.Text + "";
                    Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                }
                else

                {
                    Genclass.strsql = "SP_GETFABTWODCNO   " + txtpuid.Text + "";
                    Genclass.cmd = new SqlCommand(Genclass.strsql, conn);

                }
                SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
                DataTable tap = new DataTable();
                aptr.Fill(tap);
                bsc.DataSource = tap;
                Point loc = FindLocation(txtscono);
                grSearch.Location = new Point(loc.X, loc.Y + 20);
                grSearch.Visible = true;
                DataGridCommon.AutoGenerateColumns = false;
                DataGridCommon.Refresh();
                DataGridCommon.DataSource = null;
                DataGridCommon.Rows.Clear();
                DataGridCommon.ColumnCount = tap.Columns.Count;
                i = 0;
                foreach (DataColumn column in tap.Columns)
                {
                    DataGridCommon.Columns[i].Name = column.ColumnName;
                    DataGridCommon.Columns[i].HeaderText = column.ColumnName;
                    DataGridCommon.Columns[i].DataPropertyName = column.ColumnName;
                    i = i + 1;
                }
                DataGridCommon.DataSource = tap;
                DataGridCommon.Columns[0].Visible = true;
                DataGridCommon.Columns[0].Width = 100;
                DataGridCommon.Columns[1].Width = 100;
                DataGridCommon.Columns[2].Width = 100;
                DataGridCommon.Columns[3].Visible = false;
                DataGridCommon.Columns[4].Visible = false;
                DataGridCommon.Columns[5].Visible = false;
                DataGridCommon.Columns[6].Width = 100;
                conn.Close();
            }
            catch (Exception)
            {

                throw;
            }
        }

        private void txtscono_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    if (txtscono.Text != "")
                    {
                        bsc.Filter = string.Format("socno LIKE '%{0}%' ", txtscono.Text);

                    }

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtqty_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bsFabric.Filter = string.Format("docno LIKE '%{0}%' ", txtqty.Text);



                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtqty_Click(object sender, EventArgs e)
        {
            type = 1;
            DataTable dt = getParty();
            bsFabric.DataSource = dt;
            FillGrid2(dt, 1);
            Point loc = FindLocation(txtqty);
            grSearch.Location = new Point(loc.X, loc.Y + 20);
            grSearch.Visible = true;
        }

        private void textBox5_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bsParty.Filter = string.Format("itemname LIKE '%{0}%' ", textBox5.Text);



                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void textBox5_Click(object sender, EventArgs e)
        {

            type = 4;
            Point loc = FindLocation(textBox5);
            grSearch.Location = new Point(loc.X, loc.Y + 20);
            grSearch.Visible = true;
                       componentname();
        }
        private DataTable Ctype()
        {
            DataTable tab = new DataTable();
         
                if (HFIT.CurrentRow.Cells[1].Value != null)
                {
                    string qur = "select status,0 as lvid from statuss Order by status";
                    SqlCommand cmd = new SqlCommand(qur, conn);
                    SqlDataAdapter apt = new SqlDataAdapter(cmd);
                    apt.Fill(tab);
                }
           
            return tab;


        }
        private void cbxItemCode_SelectionChangeCommitted(object sender, EventArgs e)
        {


            //this.HFIT.CurrentRow.Cells[5].Value = this.cbxRow5.Text;

        }
        private void HFIT_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            
           
        }
       
        private void HFIT_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            //if (HFIT.CurrentCell.ColumnIndex == 5)
            //{

            //    if (HFIT.CurrentCell.ColumnIndex == HFIT.CurrentRow.Cells[5].ColumnIndex)
            //    {



            //        if (e.Control is ComboBox)
            //        {
            //            cbxRow5 = e.Control as ComboBox;
            //            cbxRow5.SelectionChangeCommitted -= new EventHandler(cbxItemCode_SelectionChangeCommitted);
            //            cbxRow5.SelectionChangeCommitted += new EventHandler(cbxItemCode_SelectionChangeCommitted);
            //        }
            //    }
            //}
        }

        private void HFIT_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void txtsocno_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
