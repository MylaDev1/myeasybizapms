﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Drawing.Imaging;
using System.Configuration;
using System.Data.SqlClient;
using System.Diagnostics;
using excel = Microsoft.Office.Interop.Excel;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;

namespace MyEasyBizAPMS
{
    public partial class FrmJOInEW : Form
    {
        private CrystalDecisions.Windows.Forms.CrystalReportViewer Cryview;

        ReportDocument doc = new ReportDocument();
        public FrmJOInEW()
        {
            this.BackColor = Color.White; ;
            InitializeComponent();
        }
        int proces;
        int processid;
        string uid = "";
        int mode = 0;
        int SP;
        double sum1;
        double sum2;
        int h = 0;
        double uu = 0;
        int kk = 0;
        int pp = 0;
        int qq = 0;
        SqlConnection conn = new SqlConnection(GeneralParameters.ConnectionString);
        SqlCommand qur = new SqlCommand();
        private DataRow doc1;
        DataTable Docno = new DataTable();
        BindingSource bs = new BindingSource();
        BindingSource bsc = new BindingSource();
        BindingSource bsp = new BindingSource();
        BindingSource bsserial = new BindingSource();
        BindingSource bsc1 = new BindingSource();
        BindingSource bsc2 = new BindingSource();

        SQLDBHelper db = new SQLDBHelper();
        int Fillid;
        BindingSource bsParty = new BindingSource();
        string str1key;
        public int SelectId = 0;
        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void label28_Click(object sender, EventArgs e)
        {

        }

        private void reqdt_ValueChanged(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {

        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void FrmJOInEW_Load(object sender, EventArgs e)
        {
            qur.Connection = conn;
            Genclass.buttonstylepanel(panadd);
            panadd.Visible = true;
            Genpan.Visible = true;
            Editpnl.Visible = false;
            panel4.Visible = false;
            tabC.Visible = false;
            HFG9.RowHeadersVisible = false;
            HFG9.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            HFG9.EnableHeadersVisualStyles = false;
            HFG9.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.GradientActiveCaption; HFIT.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            this.HFG9.DefaultCellStyle.Font = new Font("calibri", 10);
            this.HFG9.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 10, FontStyle.Bold);
            this.HFGP.DefaultCellStyle.Font = new Font("calibri", 10);
            this.HFGP.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 10, FontStyle.Bold);
            this.HFIT.DefaultCellStyle.Font = new Font("calibri", 10);
            this.HFIT.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 10, FontStyle.Bold);
            this.RQGR.DefaultCellStyle.Font = new Font("calibri", 10);
            this.RQGR.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 10, FontStyle.Bold);
            this.HFGP1.DefaultCellStyle.Font = new Font("calibri", 10);
            this.HFGP1.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 10, FontStyle.Bold);
                 HFIT.RowHeadersVisible = false;
            HFGP.RowHeadersVisible = false;
            RQGR.RowHeadersVisible = false;
            HFGP1.RowHeadersVisible = false;
             this.Dataserial.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 10, FontStyle.Bold);
            this.Dataserial.DefaultCellStyle.Font = new Font("calibri", 10);
            this.Dataserial.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 10, FontStyle.Bold);
                  
            HFIT.EnableHeadersVisualStyles = false;
            HFIT.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.GradientActiveCaption;
            HFGP1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            HFGP1.EnableHeadersVisualStyles = false;
            HFGP1.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.GradientActiveCaption;
            RQGR.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            RQGR.EnableHeadersVisualStyles = false;
            RQGR.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.GradientActiveCaption;
            HFGP.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            HFGP.EnableHeadersVisualStyles = false;
            HFGP.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.GradientActiveCaption;
            Dataserial.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            Dataserial.EnableHeadersVisualStyles = false;
            Dataserial.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.GradientActiveCaption;
            Dataserial.Focus();

            if (Text == "Fabric Process Deliveries")
            {


                Genclass.Dtype = 560;
                label33.Text = "No of Rolls";

            }
            else if (GeneralParametrs.MenyKey == 43)
            {

                Genclass.Dtype = 580;
                label33.Text = "No of bundles";
            }
            else if (GeneralParametrs.MenyKey == 45)
            {
                Genclass.Dtype = 600;
                label33.Text = "No of bundles";

            }
            else if (GeneralParametrs.MenyKey == 47)

            {

                Genclass.Dtype = 620;
                label33.Text = "No of bundles";

            }
            else if(Text == "Yarn Process Delivery")

            {

                Genclass.Dtype = 210;
                label33.Text = "No of Bags";

            }
            else if (GeneralParametrs.MenyKey == 60)

            {

                Genclass.Dtype = 950;
                label33.Text = "No of Rolls";

            }

            dtpfnt.Format = DateTimePickerFormat.Custom;
            dtpfnt.CustomFormat = "dd/MM/yyyy";
            dtpgrndt.Format = DateTimePickerFormat.Custom;
            dtpgrndt.CustomFormat = "dd/MM/yyyy";
            reqdt.Format = DateTimePickerFormat.Custom;
            reqdt.CustomFormat = "dd/MM/yyyy";
            chkact.Checked = true;
            tax();
            Titlep3();
            type1();
            LoadGetJobCard(1);
            Titlep();
            //tax1();
        }
        public void type1()
        {
            conn.Open();
            string qur = "select Generalname as type,guid   uid from generalm where   Typemuid in (40)  and active=1  and guid in(460,461) ";
            SqlCommand cmd = new SqlCommand(qur, conn);
            SqlDataAdapter apt = new SqlDataAdapter(cmd);
            DataTable tab = new DataTable();
            apt.Fill(tab);
            cbotype.DataSource = null;
            cbotype.DataSource = tab;
            cbotype.DisplayMember = "type";
            cbotype.ValueMember = "uid";
            cbotype.SelectedIndex = -1;
            conn.Close();
        }
        private void Titlep3()
        {
            RQGR.ColumnCount = 13;
            RQGR.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            RQGR.DefaultCellStyle.WrapMode = DataGridViewTriState.True;

            RQGR.Columns[0].Name = "SLNO";
            RQGR.Columns[1].Name = "ITEMID";
            RQGR.Columns[2].Name = "ITEMNAME";
            RQGR.Columns[3].Name = "Uom";
            RQGR.Columns[4].Name = "QTY";
            if (Genclass.Dtype == 560)
            {
                RQGR.Columns[5].Name = "NoofBags";
            }
            else if (Genclass.Dtype == 580)
            {
                RQGR.Columns[5].Name = "NoofBundles";
            }
            else if (Genclass.Dtype == 600)
            {
                RQGR.Columns[5].Name = "NoofBundles";
            }
            else if (Genclass.Dtype == 620)
            {
                RQGR.Columns[5].Name = "NoofBundles";
            }
            else if (Genclass.Dtype == 210)
            {
                RQGR.Columns[5].Name = "NoofBags";
            }
            RQGR.Columns[6].Name = "uid";
            RQGR.Columns[7].Name = "refid";
            RQGR.Columns[8].Name = "Socno";
            RQGR.Columns[9].Name = "workorder";
            RQGR.Columns[10].Name = "Style";
            RQGR.Columns[11].Name = "processid";
            RQGR.Columns[12].Name = "processdetuid";
            RQGR.Columns[0].Width = 50;
            RQGR.Columns[1].Visible = false;
            RQGR.Columns[12].Visible = false;
            RQGR.Columns[2].Width = 680;
            RQGR.Columns[3].Width = 60;
            RQGR.Columns[4].Width = 100;
            RQGR.Columns[5].Width = 100;
            RQGR.Columns[6].Visible = false;
            RQGR.Columns[7].Visible = false;
            RQGR.Columns[8].Visible = false;
            RQGR.Columns[9].Visible = false;
            RQGR.Columns[10].Width = 100;
            RQGR.Columns[11].Visible = false;
        }
        private void Titlep()
        {
            HFIT.AutoGenerateColumns = false;
            HFIT.Refresh();
            HFIT.DataSource = null;
            HFIT.Rows.Clear();
            HFIT.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            HFIT.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            HFIT.ColumnCount = 23;
            HFIT.Columns[0].Name = "SLNo";
            HFIT.Columns[1].Name = "Itemname";
            HFIT.Columns[2].Name = "Color";
            HFIT.Columns[3].Name = "Uom";
            HFIT.Columns[4].Name = "BillingUOm";
            HFIT.Columns[5].Name = "Type";
            HFIT.Columns[6].Name = "Qty";
            HFIT.Columns[7].Name = "Billing Qty";
            HFIT.Columns[8].Name = "Rate";
            HFIT.Columns[9].Name = "Uomid";
            HFIT.Columns[10].Name = "BillUomid";
            HFIT.Columns[11].Name = "Itemid";
            HFIT.Columns[12].Name = "uid";
            HFIT.Columns[13].Name = "itemdes";
            HFIT.Columns[14].Name = "useitemid";
            HFIT.Columns[15].Name = "refid";
            HFIT.Columns[16].Name = "dcotypeid";
            HFIT.Columns[17].Name = "FTYPE";
            HFIT.Columns[18].Name = "workorder";
            HFIT.Columns[19].Name = "socno";
            HFIT.Columns[20].Name = "Style";
            HFIT.Columns[21].Name = "processuid";
            HFIT.Columns[22].Name = "processdetuid";
            //HFIT.EditMode = DataGridViewEditMode.EditOnKeystroke;

            HFIT.Columns[0].Width = 40;

            HFIT.Columns[1].Width = 570;
            HFIT.Columns[2].Visible = false;
            HFIT.Columns[3].Width = 50;
            HFIT.Columns[4].Width = 100;
            if (Genclass.Dtype == 210)
            {
                HFIT.Columns[5].Visible = false;
            }
            else
            {
                HFIT.Columns[5].Width = 80;
            }
            HFIT.Columns[6].Width = 80;
            HFIT.Columns[7].Width = 80;
            HFIT.Columns[8].Width = 80;
            HFIT.Columns[9].Visible = false;
            HFIT.Columns[10].Visible = false;
            HFIT.Columns[11].Visible = false;
            HFIT.Columns[12].Visible = false;
            HFIT.Columns[13].Visible = false;
            HFIT.Columns[14].Visible = false;
            HFIT.Columns[15].Visible = false;
            HFIT.Columns[16].Visible = false;

            HFIT.Columns[18].Visible = false;
            HFIT.Columns[19].Visible = false;
            HFIT.Columns[20].Visible = false;
            HFIT.Columns[21].Visible = false;
            HFIT.Columns[22].Visible = false;
            if (Genclass.Dtype == 210)
            {
                HFIT.Columns[17].Visible = false;
            }
            else
            {
                HFIT.Columns[17].Visible = false;
            }
        }
        protected DataTable getParty()
        {
            DataTable dt = new DataTable();
            try
            {
                if (Genclass.type == 1)
                {
                    dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "SP_ITEMJOBISSUEGET", conn);
                    bsParty.DataSource = dt;
                }
                else if (Genclass.type == 2)
                {
                    dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "SP_PROCESSM", conn);
                    bsc.DataSource = dt;
                }
                else if (Genclass.type == 3)
                {
                    dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "SP_GETPARTYSUPPLIERProcessor", conn);
                    bsp.DataSource = dt;
                }
                else if (Genclass.type == 4 && Genclass.Dtype==560 || Genclass.type == 4 && Genclass.Dtype == 950)
                {
                    dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "sp_getwo123", conn);
                    bsc1.DataSource = dt;
                }
                else if (Genclass.type == 4 && Genclass.Dtype == 580)
                {
                    dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "sp_getwo1234", conn);
                    bsc1.DataSource = dt;
                }
                else if (Genclass.type == 4 && Genclass.Dtype == 600)
                {
                    dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "sp_getwo12345", conn);
                    bsc1.DataSource = dt;
                }
                else if (Genclass.type == 4 && Genclass.Dtype == 620)
                {
                    dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "sp_getwo123456", conn);
                    bsc1.DataSource = dt;
                }
                else if (Genclass.type == 4 && Genclass.Dtype == 210)
                {
                    dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "sp_getwo1234567", conn);
                    bsc1.DataSource = dt;
                }
             
                else if (Genclass.type == 5)
                {
                    dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "SP_ITEMJOBISSUEOUTGET", conn);
                    bsc2.DataSource = dt;
                }
                else if (Genclass.type == 7 && this.Text== "Fabric Process Deliveries")
                {
                    dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "SP_GETJOIDCITEMSworkorderno", conn);
                    bsc1.DataSource = dt;
                }
                else if (Genclass.type == 8 && this.Text == "Fabric Process Deliveries")
                {
                    dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "SP_GETJOIDCITEMSworkorderno", conn);
                    bsc1.DataSource = dt;
                }
                else if (Genclass.type == 7 && this.Text == "Yarn Process Delivery")
                {
                    dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "SP_GETJOIDCITEMSworkordernoyarn", conn);
                    bsc1.DataSource = dt;
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            return dt;
        }
        protected DataTable LoadGetJobCard(int tag)
        {

            chkact.Checked = true;
            if (chkact.Checked == true)
            {
                SP = 1;

            }
            else
            {
                SP = 0;

            }


            DataTable dt = new DataTable();
            try
            {

                SqlParameter[] para = {
                    new SqlParameter("@active",SP),
                      new SqlParameter("@DOCTYPEID",Genclass.Dtype),


                };
            
                    dt = db.GetData(CommandType.StoredProcedure, "SP_GETJOISSUEFRNTGRIDLOAD", para);
               

                LoadDataTable(dt);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        protected void LoadDataTable(DataTable dt)
        {
            try
            {
                HFGP.DataSource = null;
                HFGP.AutoGenerateColumns = false;
                HFGP.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
                HFGP.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
                HFGP.ColumnCount = 13;
                HFGP.Columns[0].Name = "Uid";
                HFGP.Columns[0].HeaderText = "Uid";
                HFGP.Columns[0].DataPropertyName = "Uid";

                HFGP.Columns[1].Name = "Docno";
                HFGP.Columns[1].HeaderText = "Docno";
                HFGP.Columns[1].DataPropertyName = "Docno";

                HFGP.Columns[2].Name = "Docdate";
                HFGP.Columns[2].HeaderText = "Docdate";
                HFGP.Columns[2].DataPropertyName = "Docdate";

                HFGP.Columns[3].Name = "name";
                HFGP.Columns[3].HeaderText = "Name";
                HFGP.Columns[3].DataPropertyName = "name";

                HFGP.Columns[4].Name = "Reference";
                HFGP.Columns[4].HeaderText = "Reference";
                HFGP.Columns[4].DataPropertyName = "Reference";

                HFGP.Columns[5].Name = "WorkOrderNo";
                HFGP.Columns[5].HeaderText = "WorkOrderNo";
                HFGP.Columns[5].DataPropertyName = "WorkOrderNo";

                HFGP.Columns[6].Name = "ProcessName";
                HFGP.Columns[6].HeaderText = "ProcessName";
                HFGP.Columns[6].DataPropertyName = "ProcessName";

                HFGP.Columns[7].Name = "supplieruid";
                HFGP.Columns[7].HeaderText = "supplieruid";
                HFGP.Columns[7].DataPropertyName = "supplieruid";

                HFGP.Columns[8].Name = "refid";
                HFGP.Columns[8].HeaderText = "refid";
                HFGP.Columns[8].DataPropertyName = "refid";

                HFGP.Columns[9].Name = "vehicleno";
                HFGP.Columns[9].HeaderText = "vehicleno";
                HFGP.Columns[9].DataPropertyName = "vehicleno";

                HFGP.Columns[10].Name = "mode";
                HFGP.Columns[10].HeaderText = "mode";
                HFGP.Columns[10].DataPropertyName = "mode";
                HFGP.Columns[11].Name = "refno";
                HFGP.Columns[11].HeaderText = "refno";
                HFGP.Columns[11].DataPropertyName = "refno";
                HFGP.Columns[12].Name = "refid";
                HFGP.Columns[12].HeaderText = "refid";
                HFGP.Columns[12].DataPropertyName = "refid";



                bs.DataSource = dt;

                HFGP.DataSource = bs;


                HFGP.Columns[0].Visible = false;
                HFGP.Columns[1].Width = 90;
                HFGP.Columns[2].Width = 90;
                HFGP.Columns[3].Width = 300;

                HFGP.Columns[4].Width = 150;
                HFGP.Columns[5].Width = 150;
                HFGP.Columns[6].Width = 200;
                HFGP.Columns[7].Visible = false;
                HFGP.Columns[8].Visible = false;
                HFGP.Columns[9].Width = 150;
                HFGP.Columns[10].Visible = false;
                HFGP.Columns[11].Visible = false;
                HFGP.Columns[12].Visible = false;
    
                HFGP.Columns[2].DefaultCellStyle.Format = "yyyy'/'MM'/'dd";


            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void Loadgrid()
        {
            try
            {
                conn.Close();
                conn.Open();
                Genclass.StrSrch = "";
                Genclass.FSSQLSortStr = "Docno";
                Genclass.FSSQLSortStr1 = "Docdate";
                Genclass.FSSQLSortStr2 = "Name";
                Genclass.FSSQLSortStr3 = "vehicleno";
                Genclass.FSSQLSortStr4 = "placeofsupply";
                Genclass.FSSQLSortStr5 = "Dcno";
                if (txtscr1.Text != "")
                {
                    if (Genclass.StrSrch == "")
                    {
                        Genclass.StrSrch = Genclass.FSSQLSortStr + " like '%" + txtscr1.Text + "%'";
                    }
                    else
                    {
                        Genclass.StrSrch = Genclass.StrSrch + " and " + Genclass.FSSQLSortStr + " like '%" + txtscr1.Text + "%'";
                    }
                }

                if (Txtscr2.Text != "")
                {
                    if (Genclass.StrSrch == "")
                    {
                        Genclass.StrSrch = Genclass.FSSQLSortStr1 + " like '%" + Txtscr2.Text + "%'";
                    }
                    else
                    {
                        Genclass.StrSrch = Genclass.StrSrch + " and " + Genclass.FSSQLSortStr1 + " like '%" + Txtscr2.Text + "%'";
                    }
                }

                if (txtscr5.Text != "")
                {
                    if (Genclass.StrSrch == "")
                    {
                        Genclass.StrSrch = Genclass.FSSQLSortStr2 + " like '%" + txtscr5.Text + "%'";
                    }
                    else
                    {
                        Genclass.StrSrch = Genclass.StrSrch + " and " + Genclass.FSSQLSortStr2 + " like '%" + txtscr5.Text + "%'";
                    }
                }

                if (txtscr6.Text != "")
                {
                    if (Genclass.StrSrch == "")
                    {
                        Genclass.StrSrch = Genclass.FSSQLSortStr3 + " like '%" + txtscr6.Text + "%'";
                    }
                    else
                    {
                        Genclass.StrSrch = Genclass.StrSrch + " and " + Genclass.FSSQLSortStr3 + " like '%" + txtscr6.Text + "%'";
                    }
                }
                if (txtbillqty.Text != "")
                {
                    if (Genclass.StrSrch == "")
                    {
                        Genclass.StrSrch = Genclass.FSSQLSortStr4 + " like '%" + txtbillqty.Text + "%'";
                    }
                    else
                    {
                        Genclass.StrSrch = Genclass.StrSrch + " and " + Genclass.FSSQLSortStr4 + " like '%" + txtbillqty.Text + "%'";
                    }
                }
                if (textBox2.Text != "")
                {
                    if (Genclass.StrSrch == "")
                    {
                        Genclass.StrSrch = Genclass.FSSQLSortStr5 + " like '%" + textBox2.Text + "%'";
                    }
                    else
                    {
                        Genclass.StrSrch = Genclass.StrSrch + " and " + Genclass.FSSQLSortStr5 + " like '%" + textBox2.Text + "%'";
                    }
                }
                if (txtscr1.Text != "")
                {
                    Genclass.StrSrch = " " + Genclass.StrSrch;
                }
                else if (Txtscr2.Text != "")
                {
                    Genclass.StrSrch = " " + Genclass.StrSrch;
                }
                else if (txtscr5.Text != "")
                {
                    Genclass.StrSrch = " " + Genclass.StrSrch;
                }
                else if (txtscr6.Text != "")
                {
                    Genclass.StrSrch = " " + Genclass.StrSrch;
                }
                else if (txtbillqty.Text != "")
                {
                    Genclass.StrSrch = " " + Genclass.StrSrch;
                }
                else if (textBox2.Text != "")
                {
                    Genclass.StrSrch = " " + Genclass.StrSrch;
                }
                else
                {
                    Genclass.StrSrch = "a.uid <> 0";
                }
                DateTime str9 = Convert.ToDateTime(dtpfnt.Text);
              
                    if (chkact.Checked == true)
                    {
                        string quy = "select a.uid,DOcno,DocDate as Docdate,B.name,Dcno as Reference,a.Remarks as WorkOrderNo,a.placeofsupply as ProcessName,a.supplieruid,a.refid,a.vehicleno,a.mode,a.refno,a.refid  from jo a inner join partym b on a.supplieruid=b.uid where doctypeid=" + Genclass.Dtype + "  and a.active=1  and  " + Genclass.StrSrch + "    order by docno desc";
                        Genclass.cmd = new SqlCommand(quy, conn);
                    }
                    else
                    {
                        string quy = "select a.uid,DOcno,DocDate as Docdate,B.name,Dcno as Reference,a.Remarks as WorkOrderNo,a.placeofsupply as ProcessName,a.supplieruid,a.refid,a.vehicleno,a.mode,a.refno,a.refid    from jo a inner join partym b on a.supplieruid=b.uid where doctypeid=" + Genclass.Dtype + "  and a.active=1  and  " + Genclass.StrSrch + "  order by docno desc";
                        Genclass.cmd = new SqlCommand(quy, conn);
                    }
                

                SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
                DataTable tap = new DataTable();
                aptr.Fill(tap);
                HFGP.AutoGenerateColumns = false;
                HFGP.Refresh();
                HFGP.DataSource = null;
                HFGP.Rows.Clear();
                HFGP.ColumnCount = tap.Columns.Count;
                Genclass.i = 0;
                foreach (DataColumn column in tap.Columns)
                {
                    HFGP.Columns[Genclass.i].Name = column.ColumnName;
                    HFGP.Columns[Genclass.i].HeaderText = column.ColumnName;
                    HFGP.Columns[Genclass.i].DataPropertyName = column.ColumnName;
                    Genclass.i = Genclass.i + 1;
                }
                HFGP.Columns[0].Visible = false;
                HFGP.Columns[1].Width = 90;
                HFGP.Columns[2].Width = 90;
                HFGP.Columns[3].Width = 300;
                //HFGP.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                HFGP.Columns[4].Width = 150;
                HFGP.Columns[5].Width = 150;
                HFGP.Columns[6].Width = 200;
                HFGP.Columns[7].Visible = false;
                HFGP.Columns[8].Visible = false;
                HFGP.Columns[9].Width = 150;
                HFGP.Columns[10].Visible = false;
                HFGP.Columns[11].Visible = false;
                HFGP.Columns[12].Visible = false;
                HFGP.DataSource = tap;
                HFGP.Columns[2].DefaultCellStyle.Format = "yyyy'/'MM'/'dd";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }
            finally
            {
                conn.Close();
                Genclass.cmd.Dispose();
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            mode = 1;
            Genpan.Visible = false;
            serialno.Visible = false;
            tabC.Visible = true;
            panadd.Visible = false;
            lkppnl.Visible = false;
            panel3.Visible = false;
            panel4.Visible = false;
            h = 0;
            Genclass.ClearTextBox(this, Editpnl);
            Genclass.STR = "";
            Genclass.Gendocno();
            txtgrn.Text = Genclass.ST;
            Editpnl.Visible = true;
            lkppnl.Visible = false;
            sum1 = 0;
            sum2 = 0;
            HFIT.Refresh();
            HFIT.DataSource = null;
            HFIT.Rows.Clear();
            RQGR.Refresh();
            RQGR.DataSource = null;
            RQGR.Rows.Clear();
            HFGP1.Refresh();
            HFGP1.DataSource = null;
            HFGP1.Rows.Clear();
       
            conn.Close();
            conn.Open();
            Docno.Clear();
         
            if (Docno.Columns.Count == 0)
            {
                Docno.Columns.Add("uid");
                Docno.Columns.Add("Docno");
            }
            conn.Close();
            conn.Open();

            qur.CommandText = "truncate table  jjissuetemp";
            qur.ExecuteNonQuery();

            qur.CommandText = "truncate table  reqdt";
            qur.ExecuteNonQuery();

            Chkedtact.Checked = true;
            RQGR.Refresh();
            RQGR.DataSource = null;
            RQGR.Rows.Clear();
            HFIT.Refresh();
            HFIT.DataSource = null;
            HFIT.Rows.Clear();
            HFGP1.Refresh();
            HFGP1.DataSource = null;
            HFGP1.Rows.Clear();
            Titlep3();
            Titlep();

           //if(Genclass.Dtype==210)
           // {


           //     label16.Visible = false;

           //     cbotype.Visible = false;
           //     txtuom.Location = new Point(613, 29);
           //     label25.Location = new Point(613, 3);

           //     txtbillqty.Location = new Point(704, 29);
           //     label5.Location = new Point(714, 8);
           //     txtbillqty.Width = 80;
           //     txtbillqty.Height = 27;
            
           //     txtrate.Location = new Point(817, 30);
           //     label4.Location = new Point(813, 3);
           //     button11.Location = new Point(900, 29);
           // }

            dtpgrndt.Focus();

        }

        private void txtname_Click(object sender, EventArgs e)
        {
            Genclass.type = 3;
            button22.Visible = true;
            DataTable dt = getParty();
            bsp.DataSource = dt;
            FillGrid2(dt, 1);
            Point loc = FindLocation(txtname);
            grSearch.Location = new Point(loc.X, loc.Y + 20);
            grSearch.Visible = true;

        }
        private Point FindLocation(Control ctrl)
        {
            if (ctrl.Parent is Form)
                return ctrl.Location;
            else
            {
                Point p = FindLocation(ctrl.Parent);
                p.X += ctrl.Location.X;
                p.Y += ctrl.Location.Y;
                return p;
            }
        }
        protected void FillGrid2(DataTable dt, int FillId)
        {
            try
            {
                DataGridCommon.DataSource = null;
                DataGridCommon.AutoGenerateColumns = false;
                DataGridCommon.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
                DataGridCommon.DefaultCellStyle.WrapMode = DataGridViewTriState.True;

                DataGridCommon.ColumnCount = 2;
                DataGridCommon.Columns[0].Name = "Uid";
                DataGridCommon.Columns[0].HeaderText = "Uid";
                DataGridCommon.Columns[0].DataPropertyName = "Uid";

                DataGridCommon.Columns[1].Name = "Name";
                DataGridCommon.Columns[1].HeaderText = "Name";
                DataGridCommon.Columns[1].DataPropertyName = "Name";
                DataGridCommon.Columns[1].Width = 300;





                DataGridCommon.DataSource = bsp;
                DataGridCommon.Columns[0].Visible = false;



            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        private void txtname_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.F10)
                {
                    grSearch.Visible = false;
                }
                else if (e.KeyCode == Keys.F2)
                {
                    SelectId = 1;
                    int Index = DataGridCommon.SelectedCells[0].RowIndex;

                    txtname.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtpuid.Text = DataGridCommon.Rows[Index].Cells[0].Value.ToString();

                    reqdt.Focus();
                    grSearch.Visible = false;
                    SelectId = 0;
                }
                else if (e.KeyCode == Keys.Escape)
                {
                    grSearch.Visible = false;
                }
                else if (e.KeyValue == 40)
                {
                    DataGridCommon.Select();
                }
                else if (e.KeyCode == Keys.Enter)
                {
                    txtname_Click(sender, e);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtname_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bsp.Filter = string.Format("Name LIKE '%{0}%' ", txtname.Text);



                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void DataGridCommon_DoubleClick(object sender, EventArgs e)
        {
            SelectId = 1;
            int Index = DataGridCommon.SelectedCells[0].RowIndex;

            if (Genclass.type == 1)
            {
                txtdcqty.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                txtitemid.Text = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                txtoutoutqty.Tag = DataGridCommon.Rows[Index].Cells[2].Value.ToString();
                txtoutoutqty.Text = DataGridCommon.Rows[Index].Cells[3].Value.ToString();
                txtcolor.Focus();

            }
            else if (Genclass.type == 8)
            {
                txtpgmsocno.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                txtpgmsocno.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                txtpgmwork.Text = DataGridCommon.Rows[Index].Cells[2].Value.ToString();
                txtstyle.Text = DataGridCommon.Rows[Index].Cells[3].Value.ToString();
                loadproces();
            }
            else if (Genclass.type == 7)
            {
                txtsocno.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                txtsocno.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                txtwor.Text = DataGridCommon.Rows[Index].Cells[2].Value.ToString();
                txtstyle.Text = DataGridCommon.Rows[Index].Cells[3].Value.ToString();
                loadproces();

            }
            else if (Genclass.type == 2)
            {
                txtbeam.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                txtbeam.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();

            }
            else if (Genclass.type == 3)
            {
                txtname.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                txtpuid.Text = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                reqdt.Focus();
            }
            else if (Genclass.type == 4)
            {
                txtitem.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                txtitem.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                txtorderdes.Text = DataGridCommon.Rows[Index].Cells[2].Value.ToString();
                //txtorderdes.Tag = DataGridCommon.Rows[Index].Cells[3].Value.ToString();
                tax1();
                cboprocess.Focus();

            }


            grSearch.Visible = false;
            SelectId = 0;
        }

        private void DataGridCommon_KeyDown(object sender, KeyEventArgs e)
        {

            if (e.KeyCode == Keys.Enter)
            {
                SelectId = 1;
                int Index = DataGridCommon.SelectedCells[0].RowIndex;

                if (Genclass.type == 1)
                {
                    txtdcqty.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtitemid.Text = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    txtoutoutqty.Tag = DataGridCommon.Rows[Index].Cells[2].Value.ToString();
                    txtoutoutqty.Text = DataGridCommon.Rows[Index].Cells[3].Value.ToString();
                    txtcolor.Focus();
                }
                else if (Genclass.type == 8)
                {
                    txtpgmsocno.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtpgmsocno.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    txtpgmwork.Text = DataGridCommon.Rows[Index].Cells[2].Value.ToString();
                    txtstyle.Text = DataGridCommon.Rows[Index].Cells[3].Value.ToString();
                    loadproces();
                }
                else if (Genclass.type == 7)
                {
                    txtsocno.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtsocno.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    txtwor.Text = DataGridCommon.Rows[Index].Cells[2].Value.ToString();
                    txtstyle.Text = DataGridCommon.Rows[Index].Cells[3].Value.ToString();
                    loadproces();

                }
                else if (Genclass.type == 2)
                {
                    txtbeam.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtbeam.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    button4.Focus();
                }
                else if (Genclass.type == 3)
                {
                    txtname.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtpuid.Text = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    reqdt.Focus();
                }
                else if (Genclass.type == 4)
                {
                    txtitem.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtitem.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    txtorderdes.Text = DataGridCommon.Rows[Index].Cells[2].Value.ToString();
                    //txtorderdes.Tag = DataGridCommon.Rows[Index].Cells[3].Value.ToString();
                    tax1();
                    cboprocess.Focus();
                }

                grSearch.Visible = false;
                SelectId = 0;
            }
        }

        private void txtitem_Click(object sender, EventArgs e)
        {
            Genclass.type = 4;
            button22.Visible = false;
            DataTable dt = getParty();
            bsc1.DataSource = dt;
            FillGrid3(dt, 1);
            Point loc = FindLocation(txtitem);
            grSearch.Location = new Point(loc.X, loc.Y + 20);
            grSearch.Visible = true;
            //grSearch.Text = "Name Search";
           
        }

        private void txtitem_KeyDown(object sender, KeyEventArgs e)
        {

            try
            {
                if (e.KeyCode == Keys.F10)
                {
                    grSearch.Visible = false;
                }
                else if (e.KeyCode == Keys.F2)
                {
                    SelectId = 1;
                    int Index = DataGridCommon.SelectedCells[0].RowIndex;
                    txtitem.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtitem.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    txtorderdes.Text = DataGridCommon.Rows[Index].Cells[2].Value.ToString();
                    tax1();
                    cboprocess.Focus();
                    grSearch.Visible = false;
                    SelectId = 0;
                }
                else if (e.KeyCode == Keys.Escape)
                {
                    grSearch.Visible = false;
                }
                else if (e.KeyValue == 40)
                {
                    DataGridCommon.Select();
                }
                else if (e.KeyCode == Keys.Enter)
                {
                   txtitem_Click(sender, e);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
        protected void FillGrid3(DataTable dt, int FillId)
        {
            try
            {
                DataGridCommon.DataSource = null;
                DataGridCommon.AutoGenerateColumns = false;


                DataGridCommon.ColumnCount = 4;
                DataGridCommon.Columns[0].Name = "Uid";
                DataGridCommon.Columns[0].HeaderText = "Uid";
                DataGridCommon.Columns[0].DataPropertyName = "Uid";

                DataGridCommon.Columns[1].Name = "socno";
                DataGridCommon.Columns[1].HeaderText = "socno";
                DataGridCommon.Columns[1].DataPropertyName = "socno";
                DataGridCommon.Columns[1].Width = 100;

                DataGridCommon.Columns[2].Name = "docno";
                DataGridCommon.Columns[2].HeaderText = "docno";
                DataGridCommon.Columns[2].DataPropertyName = "docno";
                DataGridCommon.Columns[2].Width = 100;

                DataGridCommon.Columns[3].Name = "style";
                DataGridCommon.Columns[3].HeaderText = "style";
                DataGridCommon.Columns[3].DataPropertyName = "style";
                DataGridCommon.Columns[3].Width = 150;

                //DataGridCommon.Columns[3].Name = "processid";
                //DataGridCommon.Columns[3].HeaderText = "processid";
                //DataGridCommon.Columns[3].DataPropertyName = "processid";
                DataGridCommon.DataSource = bsc1;
                DataGridCommon.Columns[0].Visible = false;
                //DataGridCommon.Columns[3].Visible = false;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void txtitem_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void txtdcqty_Click(object sender, EventArgs e)
        {
            label11.Visible = true;
            cboprocess.Visible = true;
            panel4.Visible = true;
            textBox1.Text = "";
            HFG9.DataSource = null;
            HFG9.AutoGenerateColumns = false;
            HFG9.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            HFG9.DefaultCellStyle.WrapMode = DataGridViewTriState.True;

            //getitems();
        }
        protected void FillGrid(DataTable dt, int FillId)
        {
            try
            {
                DataGridCommon.DataSource = null;
                DataGridCommon.AutoGenerateColumns = false;


                DataGridCommon.ColumnCount = 4;
                DataGridCommon.Columns[0].Name = "uid";
                DataGridCommon.Columns[0].HeaderText = "uid";
                DataGridCommon.Columns[0].DataPropertyName = "uid";
                DataGridCommon.Columns[1].Name = "itemspec1";
                DataGridCommon.Columns[1].HeaderText = "Itemname";
                DataGridCommon.Columns[1].DataPropertyName = "itemspec1";
                DataGridCommon.Columns[1].Width = 300;
                DataGridCommon.Columns[2].Name = "UOMID";
                DataGridCommon.Columns[2].HeaderText = "UOMID";
                DataGridCommon.Columns[2].DataPropertyName = "UOMID";

                DataGridCommon.Columns[3].Name = "GENERALNAME";
                DataGridCommon.Columns[3].HeaderText = "GENERALNAME";
                DataGridCommon.Columns[3].DataPropertyName = "GENERALNAME";



                DataGridCommon.DataSource = bsParty;
                DataGridCommon.Columns[0].Visible = false;
                DataGridCommon.Columns[2].Visible = false;
                DataGridCommon.Columns[3].Visible = false;



            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        private void txtdcqty_TextChanged(object sender, EventArgs e)
        {
            
        }
        private void getitems()
        {
            panel4.Visible = true;

            textBox1.Text = "";
            if(Genclass.Dtype == 210)
            {
                label11.Visible = false;
                cboprocess.Visible = false;
                textBox1.Location = new Point(14, 12);
                HFG9.Location = new Point(8, 45);
                HFG9.Width = 629;
                HFG9.Height = 218;

            }
            else
            {

                label11.Visible = true;
                cboprocess.Visible = true;
            }



            textBox1.Focus();
            if (Genclass.Dtype == 210)
            {

                SqlDataAdapter sd = new SqlDataAdapter("SP_GETJOIPLANITEMS210    ", conn);
                Genclass.FSSQLSortStr = "D.itemname";

                DataSet ds = new DataSet();
                sd.Fill(ds);
                sd.Fill(ds, "Issue");
                SqlCommandBuilder bldr = new SqlCommandBuilder(sd);
                HFG9.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
                HFG9.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
                HFG9.DataSource = ds.Tables["Issue"];
          
                HFG9.Columns[0].Visible = false;
                HFG9.Columns[2].Visible = false;
                HFG9.Columns[3].Visible = false;
                HFG9.Columns[4].Visible = false;
                HFG9.Columns[5].Visible = false;
                HFG9.Columns[6].Visible = false;
                HFG9.Columns[1].Width = 550;
                HFG9.Columns[7].Width = 120;
                HFG9.Columns[8].Width = 100;
            }
            else if (Genclass.Dtype == 560)
            {




                loadfabric();
            }
            else if (Genclass.Dtype == 580)
            {
                SqlDataAdapter sd = new SqlDataAdapter("SP_GETJOIPLANITEMS580  '" + txtbeam.Text + "','" + textBox16.Text + "'    ", conn);
                //Genclass.strsql = "SELECT   b.itemid AS UID ,(d.itemname + '  ' +'" + textBox16.Text + "' ) AS ITEMNAME,H.UID AS UOMID,H.GENERALNAME from jo a inner join   JOBISSUEDETTAB  b on a.uid=B.HEADID   and a.doctypeid=540  		   	 left join itemm c on b.Itemid=c.uid   left join generalm h on c.uom_uid=h.uid     left join JORKNITREC d on b.uid=d.REFID 	 left join jo e on d.HEADID=e.uid  and e.Doctypeid=550     left join partym k  on a.supplieruid=k.uid 	where a.remarks='" +  txtitem.Text  + "' 	   group by b.qty,b.Itemid,d.itemname,h.generalname,h.uid   ";
                Genclass.FSSQLSortStr = "D.itemname";

                DataSet ds = new DataSet();
                sd.Fill(ds);
                sd.Fill(ds, "Issue");
                SqlCommandBuilder bldr = new SqlCommandBuilder(sd);
                HFG9.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
                HFG9.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
                HFG9.DataSource = ds.Tables["Issue"];
 
                HFG9.Columns[0].Visible = false;
                HFG9.Columns[2].Visible = false;
                HFG9.Columns[3].Visible = false;
                HFG9.Columns[4].Visible = false;
                HFG9.Columns[5].Visible = false;
                HFG9.Columns[6].Visible = false;
                HFG9.Columns[1].Width = 650;
                HFG9.Columns[7].Width = 50;
            }
            else if (Genclass.Dtype == 600)
            {
                SqlDataAdapter sd = new SqlDataAdapter("SP_GETJOIPLANITEMS600  '" + txtbeam.Text + "','" + textBox16.Text + "'  ", conn);                //Genclass.strsql = "SELECT   b.itemid AS UID ,(d.itemname + '  ' +'" + textBox16.Text + "' ) AS ITEMNAME,H.UID AS UOMID,H.GENERALNAME from jo a inner join   JOBISSUEDETTAB  b on a.uid=B.HEADID   and a.doctypeid=540  		   	 left join itemm c on b.Itemid=c.uid   left join generalm h on c.uom_uid=h.uid     left join JORKNITREC d on b.uid=d.REFID 	 left join jo e on d.HEADID=e.uid  and e.Doctypeid=550     left join partym k  on a.supplieruid=k.uid 	where a.remarks='" +  txtitem.Text  + "' 	   group by b.qty,b.Itemid,d.itemname,h.generalname,h.uid   ";
                Genclass.FSSQLSortStr = "D.itemname";

                DataSet ds = new DataSet();
                sd.Fill(ds);
                sd.Fill(ds, "Issue");
                SqlCommandBuilder bldr = new SqlCommandBuilder(sd);
                HFG9.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
                HFG9.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
                HFG9.DataSource = ds.Tables["Issue"];
             
                HFG9.Columns[0].Visible = false;
                HFG9.Columns[2].Visible = false;
                HFG9.Columns[3].Visible = false;
                HFG9.Columns[4].Visible = false;
                HFG9.Columns[5].Visible = false;
                HFG9.Columns[6].Visible = false;
                HFG9.Columns[1].Width = 650;
                HFG9.Columns[7].Width = 50;
            }
            else if (Genclass.Dtype == 620)
            {
                SqlDataAdapter sd = new SqlDataAdapter("SP_GETJOIPLANITEMS620  '" + txtbeam.Text + "','" + textBox16.Text + "'  ", conn);                  //Genclass.strsql = "SELECT   b.itemid AS UID ,(d.itemname + '  ' +'" + textBox16.Text + "' ) AS ITEMNAME,H.UID AS UOMID,H.GENERALNAME from jo a inner join   JOBISSUEDETTAB  b on a.uid=B.HEADID   and a.doctypeid=540  		   	 left join itemm c on b.Itemid=c.uid   left join generalm h on c.uom_uid=h.uid     left join JORKNITREC d on b.uid=d.REFID 	 left join jo e on d.HEADID=e.uid  and e.Doctypeid=550     left join partym k  on a.supplieruid=k.uid 	where a.remarks='" +  txtitem.Text  + "' 	   group by b.qty,b.Itemid,d.itemname,h.generalname,h.uid   ";
                Genclass.FSSQLSortStr = "D.itemname";

                DataSet ds = new DataSet();
                sd.Fill(ds);
                sd.Fill(ds, "Issue");
                SqlCommandBuilder bldr = new SqlCommandBuilder(sd);
                HFG9.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
                HFG9.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
                HFG9.DataSource = ds.Tables["Issue"];
                HFG9.Columns[0].Visible = false;
                HFG9.Columns[2].Visible = false;
                HFG9.Columns[3].Visible = false;
                HFG9.Columns[4].Visible = false;
                HFG9.Columns[5].Visible = false;
                HFG9.Columns[6].Visible = false;
                HFG9.Columns[1].Width = 650;
                HFG9.Columns[7].Width = 50;
            }
            else if (Genclass.Dtype == 950)
            {
                SqlDataAdapter sd = new SqlDataAdapter(" SP_GETJOIPLANITEMS950  '" + txtitem.Text + "','" + cboprocess.Text + "' ", conn);
                Genclass.FSSQLSortStr = "D.itemname";

                DataSet ds = new DataSet();
                sd.Fill(ds);
                sd.Fill(ds, "Issue");
                SqlCommandBuilder bldr = new SqlCommandBuilder(sd);
                HFG9.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
                HFG9.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
                HFG9.DataSource = ds.Tables["Issue"];
        
                HFG9.Columns[0].Visible = false;
                HFG9.Columns[2].Visible = false;
                HFG9.Columns[3].Visible = false;
                HFG9.Columns[4].Visible = false;
                HFG9.Columns[5].Visible = false;
                HFG9.Columns[6].Visible = false;
                HFG9.Columns[1].Width = 650;
                HFG9.Columns[7].Width = 50;

            }


            
        }
        private void loadfabric()
        {

            if (cboprocess.Text != "" && cboprocess.ValueMember != "")
            {
                if (this.Text == "Fabric Process Deliveries")
                {
                    if (txtpgmsocno.Text != "")
                    {
                        Genclass.strsql1 = "select distinct seqno from (SELECT distinct max(seqno) seqno FROM PROCESSDET A INNER JOIN  PROCESSDETLIST B ON A.UID=B.HEADID   INNER JOIN  PROCESSM C ON B.PROCESSID=C.UID     where  c.processname='" + cboprocess.Text + "'  and a.docno ='" + txtpgmwork.Text + "' and a.socno='" + txtpgmsocno.Text + "')tab where seqno  is not  null ";
                        Genclass.cmd = new SqlCommand(Genclass.strsql1, conn);
                        SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
                        DataTable tap1 = new DataTable();
                        aptr1.Fill(tap1);
                        if (tap1.Rows.Count > 0)
                        {
                            Genclass.Prtid = Convert.ToInt32(tap1.Rows[0]["SEQNO"].ToString());

                        }

                        Genclass.strsql = "SP_GETJOIPLANITEMS10  '" + txtpgmwork.Text + "', '" + txtpgmsocno.Text + "','" + cboprocess.Text + "'," + Genclass.Prtid + ","+ txtpuid.Text +" ";
                        Genclass.FSSQLSortStr = "itemname";
                        Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                    }
                }
                else if (this.Text == "Yarn Process Delivery")
                {
                    Genclass.strsql = "SP_GETJOIYARNITEMS  '" + cboprocess.Text + "'";
                    Genclass.FSSQLSortStr = "itemname";
                    Genclass.cmd = new SqlCommand(Genclass.strsql, conn);

                }
                SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
                DataTable tap = new DataTable();
                aptr.Fill(tap);
                bsParty.DataSource = tap;

                HFG9.DataSource = null;
                HFG9.AutoGenerateColumns = false;
                HFG9.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
                HFG9.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
                HFG9.ColumnCount = 11;
                HFG9.Columns[0].Name = "itemid";
                HFG9.Columns[0].HeaderText = "itemid";
                HFG9.Columns[0].DataPropertyName = "itemid";
                HFG9.Columns[1].Name = "itemname";
                HFG9.Columns[1].HeaderText = "Itemname";
                HFG9.Columns[1].DataPropertyName = "itemname";

                HFG9.Columns[2].Name = "UOMID";
                HFG9.Columns[2].HeaderText = "UOMID";
                HFG9.Columns[2].DataPropertyName = "UOMID";
                HFG9.Columns[3].Name = "GENERALNAME";
                HFG9.Columns[3].HeaderText = "GENERALNAME";
                HFG9.Columns[3].DataPropertyName = "GENERALNAME";
                HFG9.Columns[4].Name = "usedid";
                HFG9.Columns[4].HeaderText = "usedid";
                HFG9.Columns[4].DataPropertyName = "usedid";
                HFG9.Columns[5].Name = "doctypeid";
                HFG9.Columns[5].HeaderText = "doctypeid";
                HFG9.Columns[5].DataPropertyName = "doctypeid";

                HFG9.Columns[6].Name = "refid";
                HFG9.Columns[6].HeaderText = "refid";
                HFG9.Columns[6].DataPropertyName = "refid";
                HFG9.Columns[7].Name = "ORDERQTY";
                HFG9.Columns[7].HeaderText = "ORDERQTY";
                HFG9.Columns[7].DataPropertyName = "ORDERQTY";
                HFG9.Columns[8].Name = "workorder";
                HFG9.Columns[8].HeaderText = "workorder";
                HFG9.Columns[8].DataPropertyName = "workorder";
                HFG9.Columns[9].Name = "socno";
                HFG9.Columns[9].HeaderText = "socno";
                HFG9.Columns[9].DataPropertyName = "socno";
                HFG9.Columns[10].Name = "Rate";
                HFG9.Columns[10].HeaderText = "Rate";
                HFG9.Columns[10].DataPropertyName = "Rate";

                HFG9.DataSource = bsParty;
                HFG9.Columns[0].Visible = false;
                HFG9.Columns[2].Visible = false;
                HFG9.Columns[3].Visible = false;
                HFG9.Columns[4].Visible = false;
                HFG9.Columns[5].Visible = false;
                HFG9.Columns[6].Visible = false;
                HFG9.Columns[1].Width = 400;
                HFG9.Columns[7].Width = 120;
                HFG9.Columns[8].Width = 100;
                HFG9.Columns[9].Width = 100;
                HFG9.Columns[10].Width = 100;
            }
        }
        private void txtdcqty_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.F10)
                {
                    grSearch.Visible = false;
                }
                else if (e.KeyCode == Keys.F2)
                {
                    SelectId = 1;
                    int Index = DataGridCommon.SelectedCells[0].RowIndex;
                    txtdcqty.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtitemid.Text = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    txtoutoutqty.Tag = DataGridCommon.Rows[Index].Cells[2].Value.ToString();
                    txtoutoutqty.Text = DataGridCommon.Rows[Index].Cells[3].Value.ToString();
                    txtdcqty.Tag = HFG9.Rows[Index].Cells[4].Value.ToString();

                    txtcolor.Focus();
                    grSearch.Visible = false;
                    SelectId = 0;
                }
                else if (e.KeyCode == Keys.Escape)
                {
                    grSearch.Visible = false;
                }
                else if (e.KeyValue == 40)
                {
                    DataGridCommon.Select();
                }
                else if (e.KeyCode == Keys.Enter)
                {
                    txtdcqty_Click(sender, e);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void button11_Click(object sender, EventArgs e)
        {
            if (txtdcqty.Text == "")
            {
                MessageBox.Show("Enter the Item");
                txtdcqty.Focus();
                return;
            }
            if (txtrate.Text == "")
            {

                txtrate.Text = "0";

            }


            if (txtuom.Text == "")
            {
                MessageBox.Show("Enter the qty");
                txtuom.Focus();
                return;
            }
            if (txtisstot.Text == "")
            {

                txtisstot.Text = "0";

            }
            if (txtbillqty.Text == "")
            {

                txtbillqty.Text = "0";

            }
            Genclass.strsql = "SELECT F.UID,F.ITEMNAME,F.ITEMID,B.SEQNO,isnull(sum(f.qty),0)-isnull(sum(h.qty),0) as qty FROM PROCESSDET A INNER JOIN  PROCESSDETLIST B ON A.UID=B.HEADID INNER JOIN  PROCESSM C ON B.PROCESSID=C.UID  INNER JOIN JO D ON C.PROCESSNAME=D.PLACEOFSUPPLY   AND A.DOCNO=D.REMARKS AND D.DOCTYPEID=540 INNER JOIN JOBISSUEDETTAB E ON D.UID=E.HEADID INNER JOIN JORKNITREC F ON E.UID=F.REFID  left join joiitems h on f.uid=h.refid  WHERE A.DOCNO='" + txtitem.Text + "' AND B.SEQNO=(" + Genclass.Prtid + "-10)  group by F.UID,F.ITEMNAME,F.ITEMID,B.SEQNO";
            Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
            SqlDataAdapter aptr3 = new SqlDataAdapter(Genclass.cmd);
            DataTable tap3 = new DataTable();
            aptr3.Fill(tap3);
            if (tap3.Rows.Count > 0)
            {
                sum1 = Convert.ToDouble(tap3.Rows[0]["qty"].ToString());

                uu = Convert.ToDouble(txtuom.Text);
                if (sum1 < uu)
                {
                    MessageBox.Show("Excess Qty");
                    return;
                }

            }

            Genclass.strsql = "select distinct b.uid FROM processdet a inner join processdetlist b on a.uid=b.headid  where b.itemid=" + txtitemid.Text + " and a.docno='" + textBox10.Text + "' and a.socno='" + txtbeam.Text + "'  and b.processid=" + cboprocess.SelectedValue + " ";
            Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
            SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
            DataTable tap1 = new DataTable();
            aptr1.Fill(tap1);


            if (tap1.Rows.Count > 0)
            {

                processid = Convert.ToInt32(tap1.Rows[0]["uid"].ToString());
            }
            if (mode == 2)
            {
                kk = HFIT.Rows.Count - 1;
                h = kk;
            }


            h = h + 1;
            var index = HFIT.Rows.Add();
            HFIT.Rows[index].Cells[0].Value = h;
            HFIT.Rows[index].Cells[1].Value = txtdcqty.Text;
            HFIT.Rows[index].Cells[2].Value = 0;
            HFIT.Rows[index].Cells[3].Value = txtoutoutqty.Text;
            HFIT.Rows[index].Cells[4].Value = cbouom.Text;
            HFIT.Rows[index].Cells[5].Value = cbotype.Text;
            HFIT.Rows[index].Cells[6].Value = txtuom.Text;

            HFIT.Rows[index].Cells[7].Value = txtbillqty.Text;
            HFIT.Rows[index].Cells[8].Value = txtrate.Text;

            HFIT.Rows[index].Cells[9].Value = txtoutoutqty.Tag;
            HFIT.Rows[index].Cells[10].Value = cbouom.SelectedValue;
            HFIT.Rows[index].Cells[11].Value = txtitemid.Text;
            HFIT.Rows[index].Cells[12].Value = 0;
            if (Genclass.Dtype == 210)
            {
                Genclass.strsql = "select isnull(processsname,0) as sname from processm    where processname='YARN DYEING'";
                Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
            }
            else
            {
                Genclass.strsql = "select isnull(processsname,0) as sname from processm    where processname='" + cboprocess.Text + "'";
                Genclass.cmd = new SqlCommand(Genclass.strsql, conn);

            }
            SqlDataAdapter aptr2 = new SqlDataAdapter(Genclass.cmd);
            DataTable tap2 = new DataTable();
            aptr2.Fill(tap2);
            if (tap2.Rows.Count > 0)
            {
                if (tap2.Rows[0]["sname"].ToString() != null)
                {
                    string kk;
                    kk = txtdcqty.Text;

                    if (Genclass.Dtype != 210)
                    {

                        kk = txtdcqty.Text + " " + tap2.Rows[0]["sname"].ToString();
                    }
                    HFIT.Rows[index].Cells[13].Value = kk.ToString();
                    HFIT.Rows[index].Cells[1].Value = HFIT.Rows[index].Cells[13].Value;
                }
                else
                {
                    HFIT.Rows[index].Cells[13].Value = 0;

                }
            }

            HFIT.Rows[index].Cells[14].Value = txtdcqty.Tag;

            if (cboprocess.Text != null)
            {
                HFIT.Rows[index].Cells[15].Value = cboprocess.SelectedValue;
            }
            else

            {
                HFIT.Rows[index].Cells[15].Value = 0;

            }


            if (txtitemid.Tag == null)
            {
                txtitemid.Tag = 0;
            }
            else
            {
                HFIT.Rows[index].Cells[16].Value = txtitemid.Tag;
            }


            HFIT.Rows[index].Cells[17].Value = "";
            HFIT.Rows[index].Cells[18].Value = textBox10.Text;
            HFIT.Rows[index].Cells[19].Value = txtbeam.Text;
            HFIT.Rows[index].Cells[20].Value = txtstyle.Text;
            HFIT.Rows[index].Cells[21].Value = cboprocess.SelectedValue;
            HFIT.Rows[index].Cells[22].Value = processid;

            conn.Close();
            conn.Open();
            qur.CommandText = "insert into   jjissuetemp  VALUES (" + txtitemid.Text + "," + txtuom.Text + "," + txtbillqty.Text + ") ";
            qur.ExecuteNonQuery();



            txtstyle.Text = "";
            txtmode.Text = "";

            txtoutoutqty.Text = "";

            txtisstot.Text = "";

            txtdcqty.Text = "";
            txtcolor.Text = "";

            txtuom.Text = "";
            txtbillqty.Text = "";
            txtrate.Text = "";

            txtdcqty.Focus();
        }

        private void button18_Click(object sender, EventArgs e)
        {
            try
            {
                SelectId = 1;
                int Index = DataGridCommon.SelectedCells[0].RowIndex;
                if (Genclass.type == 1)
                {
                    txtdcqty.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtitemid.Text = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    txtoutoutqty.Tag = DataGridCommon.Rows[Index].Cells[2].Value.ToString();
                    txtoutoutqty.Text = DataGridCommon.Rows[Index].Cells[3].Value.ToString();
                    txtcolor.Focus();
                }
                else if (Genclass.type == 2)
                {
                    txtbeam.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtbeam.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();

                }
                else if (Genclass.type == 3)
                {
                    txtname.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtpuid.Text = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    reqdt.Focus();

                }
                else if (Genclass.type == 4)
                {
                    txtitem.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtitem.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    txtorderdes.Text = DataGridCommon.Rows[Index].Cells[2].Value.ToString();
                    //txtorderdes.Tag = DataGridCommon.Rows[Index].Cells[3].Value.ToString();
                    tax1();
                    cboprocess.Focus();
                }
                else if (Genclass.type == 7)
                {
                    txtsocno.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtsocno.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    txtwor.Text = DataGridCommon.Rows[Index].Cells[2].Value.ToString();
                    txtstyle.Text = DataGridCommon.Rows[Index].Cells[3].Value.ToString();
                    loadproces();
                }
                else if (Genclass.type == 8)
                {
                    txtpgmsocno.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtpgmsocno.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    txtpgmwork.Text = DataGridCommon.Rows[Index].Cells[2].Value.ToString();
                    txtstyle.Text = DataGridCommon.Rows[Index].Cells[3].Value.ToString();
                    loadproces();
                }

                grSearch.Visible = false;
                SelectId = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
        public void tax1()
        {
            conn.Close();
            conn.Open();
            if (Genclass.Dtype == 560)
            {
                string qur = " select distinct  uid,Processname  from   (select distinct c.uid,c.Processname  from processdet a inner join processdetlist b on a.uid=b.headid inner join processm c on b.processid=c.uid  where  b.type='Fabric'  and c.uid<>5)tab order by uid ";
                SqlCommand cmd = new SqlCommand(qur, conn);
                SqlDataAdapter apt = new SqlDataAdapter(cmd);
                DataTable tab = new DataTable();
                apt.Fill(tab);
                cboprocess.DataSource = null;
                cboprocess.DataSource = tab;
                cboprocess.DisplayMember = "Processname";
                cboprocess.ValueMember = "uid";
                cboprocess.SelectedIndex = -1;
                conn.Close();
            }
           else if (Genclass.Dtype == 210)
            {
                string qur = " select distinct  uid,Processname  from   (select distinct c.uid,c.Processname  from processdet a inner join processdetlist b on a.uid=b.headid inner join processm c on b.processid=c.uid  where  b.type='Yarn' )tab order by uid ";
                SqlCommand cmd = new SqlCommand(qur, conn);
                SqlDataAdapter apt = new SqlDataAdapter(cmd);
                DataTable tab = new DataTable();
                apt.Fill(tab);
                cboprocess.DataSource = null;
                cboprocess.DataSource = tab;
                cboprocess.DisplayMember = "Processname";
                cboprocess.ValueMember = "uid";
                cboprocess.SelectedIndex = -1;
                conn.Close();
            }




        }
        private void btnHide_Click(object sender, EventArgs e)
        {
            grSearch.Visible = false;
        }

        private void button9_Click(object sender, EventArgs e)
        {
            panel3.Visible = false;
        }

        private void butedit_Click(object sender, EventArgs e)
        {
            lkppnl.Visible = false; 
            panel3.Visible = false;
            mode = 2;
            panadd.Visible = false;
            Genpan.Visible = false;
            Editpnl.Visible = true;
            panel4.Visible = false;
            tabC.Visible = true;
            conn.Close();
            conn.Open();

            //cbosGReturnItem.Items.Clear();
            int i = HFGP.SelectedCells[0].RowIndex;
            uid = HFGP.Rows[i].Cells[0].Value.ToString();
            txtgrnid.Text = HFGP.Rows[i].Cells[0].Value.ToString();
            txtgrn.Text = HFGP.Rows[i].Cells[1].Value.ToString();
            dtpgrndt.Text = HFGP.Rows[i].Cells[2].Value.ToString();

            txtname.Text = HFGP.Rows[i].Cells[3].Value.ToString();
            txtqty.Text = HFGP.Rows[i].Cells[4].Value.ToString();
            txtitem.Text = HFGP.Rows[i].Cells[5].Value.ToString();
           
            Genclass.address = HFGP.Rows[i].Cells[6].Value.ToString();
            //loadprocess();
            //cboprocess.Text = HFGP.Rows[i].Cells[6].Value.ToString();
            txtpuid.Text = HFGP.Rows[i].Cells[7].Value.ToString();
            txtitem.Tag = HFGP.Rows[i].Cells[8].Value.ToString();
            txtvehicle.Text = HFGP.Rows[i].Cells[9].Value.ToString();
            txtmodem.Text = HFGP.Rows[i].Cells[10].Value.ToString();
            txtorderdes.Text = HFGP.Rows[i].Cells[11].Value.ToString();
            txtorderdes.Tag = HFGP.Rows[i].Cells[12].Value.ToString();

            if (Genclass.Dtype == 210)
            {


                label16.Visible = false;

                cbotype.Visible = false;
                txtuom.Location = new Point(613, 29);
                label25.Location = new Point(613, 3);

                txtbillqty.Location = new Point(704, 29);
                label5.Location = new Point(714, 8);
                txtbillqty.Width = 80;
                txtbillqty.Height = 27;

                txtrate.Location = new Point(817, 30);
                label4.Location = new Point(813, 3);
                button11.Location = new Point(900, 29);
            }
            conn.Close();
            conn.Open();
            Docno.Clear();
            if (Docno.Columns.Count == 0)
            {
                Docno.Columns.Add("uid");
                Docno.Columns.Add("Docno");
            }
        
            conn.Close();
            conn.Open();

            qur.CommandText = "truncate table  jjissuetemp";
            qur.ExecuteNonQuery();

            qur.CommandText = "truncate table  reqdt";
            qur.ExecuteNonQuery();

            serialno.Visible = false;
            Chkedtact.Checked = true;
            HFIT.Refresh();
            HFIT.DataSource = null;
            HFIT.Rows.Clear();
            RQGR.Refresh();
            RQGR.DataSource = null;
            RQGR.Rows.Clear();
            HFGP1.Refresh();
            HFGP1.DataSource = null;
            HFGP1.Rows.Clear();
     
            sum1 = 0;
            sum2 = 0;
            Titlep();
            load();
      
            Titlep3();
            Genclass.strsql = "SP_GETJOBREDITDCLOAD  " + txtgrnid.Text + ", " + Genclass.Dtype + "  ";
            Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
            SqlDataAdapter aptr3 = new SqlDataAdapter(Genclass.cmd);
            DataTable tap3 = new DataTable();
            aptr3.Fill(tap3);
            if (tap3.Rows.Count > 0)
            {

                for (int m = 0; m < tap3.Rows.Count; m++)
                {
                    var index = RQGR.Rows.Add();
                    RQGR.Rows[index].Cells[0].Value = tap3.Rows[m]["slno"].ToString();
                    RQGR.Rows[index].Cells[1].Value = tap3.Rows[m]["itemid"].ToString();
                    RQGR.Rows[index].Cells[2].Value = tap3.Rows[m]["itemname"].ToString();
                    RQGR.Rows[index].Cells[3].Value = tap3.Rows[m]["Uom"].ToString();
                    RQGR.Rows[index].Cells[4].Value = tap3.Rows[m]["qty"].ToString();
                    RQGR.Rows[index].Cells[5].Value = tap3.Rows[m]["rolls"].ToString();
                    RQGR.Rows[index].Cells[6].Value = tap3.Rows[m]["uid"].ToString();
                    RQGR.Rows[index].Cells[8].Value = tap3.Rows[m]["socno"].ToString();
                    RQGR.Rows[index].Cells[9].Value = tap3.Rows[m]["workorder"].ToString();
                    RQGR.Rows[index].Cells[10].Value = tap3.Rows[m]["style"].ToString();
                    RQGR.Rows[index].Cells[11].Value = tap3.Rows[m]["processid"].ToString();
                    RQGR.Rows[index].Cells[12].Value = tap3.Rows[m]["processdetid"].ToString();

          
                }



            }
           
        }
        private void loadprocess()
        {

            string qur = " select distinct uid,Processname  from   (select distinct c.uid,c.Processname  from processdet a inner join processdetlist b on a.uid=b.headid inner join processm c on b.processid=c.uid  where c.processname='" + Genclass.address + "')tab order by uid ";
            SqlCommand cmd = new SqlCommand(qur, conn);
            SqlDataAdapter apt = new SqlDataAdapter(cmd);
            DataTable tab = new DataTable();
            apt.Fill(tab);
            cboprocess.DataSource = null;
            cboprocess.DataSource = tab;
            cboprocess.DisplayMember = "Processname";
            cboprocess.ValueMember = "uid";
            cboprocess.SelectedIndex = -1;
            conn.Close();
        }
        private void load()
        {
            Genclass.strsql = "SP_GETJOBREDITPLANLOAD "+ txtgrnid.Text + "," + Genclass.Dtype + "  ";
            Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
            SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
            DataTable tap1 = new DataTable();
            aptr1.Fill(tap1);

            Genclass.sum1 = 0;


            for (int k = 0; k < tap1.Rows.Count; k++)
            {
                var index = HFIT.Rows.Add();

                HFIT.Rows[index].Cells[0].Value = tap1.Rows[k]["slno"].ToString();

                HFIT.Rows[index].Cells[1].Value = tap1.Rows[k]["itemname"].ToString();

                HFIT.Rows[index].Cells[2].Value = tap1.Rows[k]["color"].ToString();
                HFIT.Rows[index].Cells[3].Value = tap1.Rows[k]["Uom"].ToString();

                HFIT.Rows[index].Cells[4].Value = tap1.Rows[k]["BillingUom"].ToString();

                HFIT.Rows[index].Cells[5].Value = tap1.Rows[k]["type"].ToString();


                HFIT.Rows[index].Cells[6].Value = tap1.Rows[k]["qty"].ToString();
                HFIT.Rows[index].Cells[7].Value = tap1.Rows[k]["billqty"].ToString();
                HFIT.Rows[index].Cells[8].Value = tap1.Rows[k]["Rate"].ToString();
                HFIT.Rows[index].Cells[9].Value = tap1.Rows[k]["uomid"].ToString();
                HFIT.Rows[index].Cells[10].Value = tap1.Rows[k]["billuomid"].ToString();
                HFIT.Rows[index].Cells[11].Value = tap1.Rows[k]["itemid"].ToString();
                HFIT.Rows[index].Cells[12].Value = tap1.Rows[k]["uid"].ToString();
                HFIT.Rows[index].Cells[13].Value = tap1.Rows[k]["itemdes"].ToString();
                HFIT.Rows[index].Cells[14].Value = tap1.Rows[k]["uesitemid"].ToString();
                HFIT.Rows[index].Cells[15].Value = tap1.Rows[k]["refid"].ToString();
                HFIT.Rows[index].Cells[16].Value = tap1.Rows[k]["doctypeid"].ToString();
                HFIT.Rows[index].Cells[17].Value = tap1.Rows[k]["ftype"].ToString();
                HFIT.Rows[index].Cells[18].Value = tap1.Rows[k]["socno"].ToString();
                HFIT.Rows[index].Cells[19].Value = tap1.Rows[k]["workorder"].ToString();
                HFIT.Rows[index].Cells[21].Value = tap1.Rows[k]["processid"].ToString();
                HFIT.Rows[index].Cells[22].Value = tap1.Rows[k]["processdetid"].ToString();
            }
        }
        private void button10_Click(object sender, EventArgs e)
        {
            int Index = HFGP1.SelectedCells[0].RowIndex;

            txtitem.Text = HFGP1.Rows[Index].Cells[1].Value.ToString();
            txtitem.Tag = HFGP1.Rows[Index].Cells[0].Value.ToString();

            cboprocess.Text = HFGP1.Rows[Index].Cells[2].Value.ToString();
            cboprocess.Tag = HFGP1.Rows[Index].Cells[3].Value.ToString();
            panel3.Visible = false;
            txtvehicle.Focus();
        }

        private void HFGP1_DoubleClick(object sender, EventArgs e)
        {

            int Index = HFGP1.SelectedCells[0].RowIndex;
            txtitem.Tag = HFGP1.Rows[Index].Cells[0].Value.ToString();
            txtitem.Text = HFGP1.Rows[Index].Cells[1].Value.ToString();

            cboprocess.Tag = HFGP1.Rows[Index].Cells[3].Value.ToString();
            cboprocess.Text = HFGP1.Rows[Index].Cells[2].Value.ToString();
          
            panel3.Visible = false;
            txtvehicle.Focus();
        }
        public void tax()
        {
            conn.Open();
            string qur = "select Generalname as tax,guid uid from generalm where   Typemuid in (7) and active=1 ";
            SqlCommand cmd = new SqlCommand(qur, conn);
            SqlDataAdapter apt = new SqlDataAdapter(cmd);
            DataTable tab = new DataTable();
            apt.Fill(tab);
            cbouom.DataSource = null;
            cbouom.DataSource = tab;
            cbouom.DisplayMember = "tax";
            cbouom.ValueMember = "uid";
            cbouom.SelectedIndex = -1;
            conn.Close();
        }
        private void btnHFGP1_KeyDown(object sender, KeyEventArgs e)
        {
            

        }

        private void txtoutitem_Click(object sender, EventArgs e)
        {
         


            panel3.Visible = true;
            txtscr11.Text = "";

            txtscr11.Focus();
            //string qur = "exec  SP_GETJOIDCITEMSworkorderno";
            //SqlCommand cmd = new SqlCommand(qur, conn);
            //SqlDataAdapter apt = new SqlDataAdapter(cmd);
            //DataTable tab = new DataTable();
            //apt.Fill(tab);
            //cbowo.DataSource = null;
            //cbowo.DataSource = tab;
            //cbowo.DisplayMember = "socno";
            //cbowo.ValueMember = "uid";
            //cbowo.SelectedIndex = -1;
            //conn.Close();




        }

        private void loadcono()
        {
            if (Genclass.Dtype == 210)
            {
                SqlDataAdapter sd = new SqlDataAdapter("SP_GETJOIDCITEMS1  ", conn);
                //Genclass.strsql = "select distinct  b.uid  as ITEMID,b.itemname,'' as qty,c.GENERALNAME,'' as uid from itemsmbom a inner join itemm b on a.useditem_uid=b.uid inner join generalm c on b.uom_uid=c.uid inner join   jjissuetemp d on a.itemm_uid=d.itemuid      where  b.itemgroup_uid=66";

                Genclass.FSSQLSortStr = "itemname";
                DataSet ds = new DataSet();
                sd.Fill(ds);
                sd.Fill(ds, "Issue");
                SqlCommandBuilder bldr = new SqlCommandBuilder(sd);
                HFGP1.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
                HFGP1.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
                HFGP1.DataSource = ds.Tables["Issue"];


                HFGP1.Columns[0].Visible = false;

                HFGP1.Columns[3].Visible = false;
                HFGP1.Columns[4].Visible = false;
                HFGP1.Columns[1].Width = 450;
                HFGP1.Columns[2].Width = 120;
                HFGP1.Columns[6].Width = 100;
                HFGP1.Columns[7].Width = 100;
                HFGP1.Columns[5].Visible = false;

            }
            else

            {
                SqlDataAdapter sd = new SqlDataAdapter("SP_GETJOIDCITEMS2", conn);

                Genclass.FSSQLSortStr = "itemname";
                DataSet ds = new DataSet();
                sd.Fill(ds);
                sd.Fill(ds, "Issue");
                SqlCommandBuilder bldr = new SqlCommandBuilder(sd);
                HFGP1.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
                HFGP1.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
                HFGP1.DataSource = ds.Tables["Issue"];
                //HFGP.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
                //this.HFGP1.DefaultCellStyle.WrapMode = DataGridViewTriState.True;

                HFGP1.Columns[0].Visible = false;
                HFGP1.Columns[1].Width = 450;
                HFGP1.Columns[2].Width = 120;
                HFGP1.Columns[3].Visible = false;
                HFGP1.Columns[4].Visible = false;
                HFGP1.Columns[5].Visible = false;
                HFGP1.Columns[6].Width = 100;

            }



        }
        protected void FillGrid4(DataTable dt, int FillId)
        {
            try
            {
                HFGP2.DataSource = null;
                HFGP2.AutoGenerateColumns = false;
                HFGP2.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
                HFGP2.DefaultCellStyle.WrapMode = DataGridViewTriState.True;

                HFGP2.ColumnCount = 5;
                HFGP2.Columns[0].Name = "uid";
                HFGP2.Columns[0].HeaderText = "uid";
                HFGP2.Columns[0].DataPropertyName = "uid";
                HFGP2.Columns[1].Name = "Itemname";
                HFGP2.Columns[1].HeaderText = "Itemname";
                HFGP2.Columns[1].DataPropertyName = "Itemname";
                HFGP2.Columns[1].Width = 300;

                HFGP2.Columns[2].Name = "qty";
                HFGP2.Columns[2].HeaderText = "qty";
                HFGP2.Columns[2].DataPropertyName = "qty";


                HFGP2.Columns[3].Name = "rolls";
                HFGP2.Columns[3].HeaderText = "rolls";
                HFGP2.Columns[3].DataPropertyName = "rolls";

                HFGP2.Columns[4].Name = "generalname";
                HFGP2.Columns[4].HeaderText = "Uom";
                HFGP2.Columns[4].DataPropertyName = "generalname";


                HFGP2.DataSource = bsc2;
                HFGP2.Columns[0].Visible = false;
                HFGP2.Columns[2].Visible = false;
                HFGP2.Columns[3].Visible = false;
                HFGP2.Columns[4].Visible = false;


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        private void txtoutitem_KeyDown(object sender, KeyEventArgs e)
        {
           
        }

        private void button17_Click(object sender, EventArgs e)
        {
         
            Genclass.strsql = "select distinct b.uid,b.processid FROM processdet a inner join processdetlist b on a.uid=b.headid  inner join workorderinM  c on a.uid = c.workid where b.itemid=" + txtsocno.Tag + " and a.docno='" + txtsocno.Text + "' and a.socno='" + txtwor.Text + "'  and c.process='" + cbopro1.Text + "' ";
            Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
            SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
            DataTable tap1 = new DataTable();
            aptr1.Fill(tap1);


            if (tap1.Rows.Count > 0)
            {

                processid = Convert.ToInt32(tap1.Rows[0]["uid"].ToString());
                proces = Convert.ToInt32(tap1.Rows[0]["processid"].ToString());
            }
            if (mode == 2)
            {
                 kk = RQGR.Rows.Count - 1;
                pp = kk;
            }
            if(txtoutrolls.Text=="")
            {

                txtoutrolls.Text = "0";
            }
          
                pp = pp + 1;
                var index = RQGR.Rows.Add();
                RQGR.Rows[index].Cells[0].Value = pp;
                RQGR.Rows[index].Cells[1].Value = txtoutitem.Tag;

                RQGR.Rows[index].Cells[2].Value = txtoutitem.Text;
                RQGR.Rows[index].Cells[3].Value = txtoutuom.Text;
                RQGR.Rows[index].Cells[4].Value = txtoutqty.Text;

                RQGR.Rows[index].Cells[5].Value = txtoutrolls.Text;
                RQGR.Rows[index].Cells[6].Value = 0;
                RQGR.Rows[index].Cells[7].Value = txtoutuom.Tag;
            RQGR.Rows[index].Cells[8].Value = txtsocno.Text;
            RQGR.Rows[index].Cells[9].Value = txtwor.Text;
            RQGR.Rows[index].Cells[10].Value = txtstyle.Text;
            RQGR.Rows[index].Cells[11].Value = proces;
            RQGR.Rows[index].Cells[12].Value = processid;
            txtstyle.Text = "";
            txtoutuom.Text = "";
            txtoutqty.Text = "";
            txtoutrolls.Text = "";
            txtoutitem.Text = "";
            txtwor.Text = "";
            cbowo.Text = "";
            txtoutitem.Focus();
        }

        private void button15_Click(object sender, EventArgs e)
        {
            try
            {
                SelectId = 1;
                int Index = HFGP2.SelectedCells[0].RowIndex;
                if (Genclass.type == 5)
                {
                    txtoutitem.Text = HFGP2.Rows[Index].Cells[1].Value.ToString();
                    txtoutitem.Tag = HFGP2.Rows[Index].Cells[0].Value.ToString();
                    txtoutqty.Text = HFGP2.Rows[Index].Cells[2].Value.ToString();
                    //txtoutrolls.Text = HFGP2.Rows[Index].Cells[3].Value.ToString();
                    txtoutuom.Text = HFGP2.Rows[Index].Cells[4].Value.ToString();
                    txtoutqty.Focus();
                }


                lkppnl.Visible = false;
                SelectId = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void buttnfinbk_Click(object sender, EventArgs e)
        {
            tabC.Visible = false;
            Editpnl.Visible = false;
            Genpan.Visible = true;
            panadd.Visible = true;

            LoadGetJobCard(1);
        }

        private void btnsave_Click(object sender, EventArgs e)
        {
            conn.Close();
            conn.Open();
            if (mode == 2)
            {


                string quyw = "SELECT* from  JOISSUEPGMITEMS a inner join JORECEIPT b on a.uid=b.refid where a.headid=" + txtgrnid.Text + "  ";
                Genclass.cmd = new SqlCommand(quyw, conn);
                SqlDataAdapter aptr3 = new SqlDataAdapter(Genclass.cmd);
                DataTable tap3 = new DataTable();
                aptr3.Fill(tap3);
                if (tap3.Rows.Count > 0)
                {
                    MessageBox.Show("Can not edit the Issue already rise the Receipt");
                    return;

                }


            }
           

            conn.Close();
            conn.Open();
          
            if (Chkedtact.Checked == true)
            {

                SP = 1;
            }
            else
            {
                SP = 0;

            }

            if (RQGR.RowCount <= 1)
            {
                MessageBox.Show("Enter the  dcitems");
                return;

            }
            if (HFIT.RowCount <= 1)
            {
                MessageBox.Show("Enter the plan items");
                return;

            }

            if (txtorderdes.Text == "")

            {
                txtorderdes.Text = "0";

            }
                if (txtvehicle.Text == "")

                {
                txtvehicle.Text = "0";

                }
            if (txtmodem.Text == "")

            {
                txtmodem.Text = "0";

            }
            if (txtterms.Text == "")

            {
                txtterms.Text = "0";

            }
            if (mode == 1)
            {


               

                SqlParameter[] para ={

                    new SqlParameter("@DOCNO",txtgrn.Text),
                    new SqlParameter("@DOCDATE",Convert.ToDateTime(dtpgrndt.Text)),
                    new SqlParameter("@REFNO",txtorderdes.Text),
                    new SqlParameter("@DCNO","0"),
                    new SqlParameter("@DCDATE",Convert.ToDateTime(reqdt.Text)),
                    new SqlParameter("@DOCTYPEID", Genclass.Dtype),
                    new SqlParameter("@SUPPLIERUID",txtpuid.Text),
                    new SqlParameter("@PLACEOFSUPPLY",cboprocess.Text),
                    new SqlParameter("@MODE",txtmodem.Text),
                    new SqlParameter("@REMARKS","0"),
                    new SqlParameter("@VEHICLENO",txtvehicle.Text),
                    new SqlParameter("@REASON", txtterms.Text),
                    new SqlParameter("@REFID",'0'),
                    new SqlParameter("@YEARID",Genclass.Yearid),
                    new SqlParameter("@ACTIVE", SP)


            };
                db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_JO", para, conn);

                string quy6 = "select * from jo where docno='" + txtgrn.Text + "' and doctypeid="+ Genclass.Dtype +"";
                Genclass.cmd = new SqlCommand(quy6, conn);
                SqlDataAdapter aptr3 = new SqlDataAdapter(Genclass.cmd);
                DataTable tap5 = new DataTable();
                aptr3.Fill(tap5);
                if (tap5.Rows.Count > 0)
                {
                    txtgrnid.Text = tap5.Rows[0]["uid"].ToString();

                }

                for (int i = 0; i < HFIT.RowCount - 1; i++)
                {
                    if (HFIT.Rows[i].Cells[15].Value.ToString() == "" || HFIT.Rows[i].Cells[15].Value ==null)
                    {
                        HFIT.Rows[i].Cells[15].Value = "0";

                    }
                    if (HFIT.Rows[i].Cells[14].Value.ToString() == "" || HFIT.Rows[i].Cells[14].Value == null)
                    {
                        HFIT.Rows[i].Cells[14].Value = "0";

                    }
                    if (HFIT.Rows[i].Cells[17].Value.ToString() == "" || HFIT.Rows[i].Cells[17].Value == null)
                    {
                        HFIT.Rows[i].Cells[17].Value = "0";

                    }

                    conn.Close();
                    conn.Open();
                    SqlParameter[] para1 ={
                    new SqlParameter("@slno", HFIT.Rows[i].Cells[0].Value),
                    new SqlParameter("@iTEMNAME",HFIT.Rows[i].Cells[1].Value),
                    new SqlParameter("@ITEMID", HFIT.Rows[i].Cells[11].Value),
                    new SqlParameter("@BILLUOMID", HFIT.Rows[i].Cells[10].Value),
                    new SqlParameter("@UOMID", HFIT.Rows[i].Cells[9].Value),
                    new SqlParameter("@QTY", HFIT.Rows[i].Cells[6].Value),
                    new SqlParameter("@BILLQTY",HFIT.Rows[i].Cells[7].Value),
                    new SqlParameter("@TYPE", HFIT.Rows[i].Cells[5].Value),
                    new SqlParameter("@RATE", HFIT.Rows[i].Cells[8].Value),
                    new SqlParameter("@HEADID",txtgrnid.Text),
                    new SqlParameter("@COLOR",HFIT.Rows[i].Cells[2].Value),
                    new SqlParameter("@ITEMDES",HFIT.Rows[i].Cells[13].Value),
                     new SqlParameter("@useitemid",HFIT.Rows[i].Cells[14].Value),

                     //int uu=Convert.ToInt16(HFIT.Rows[i].Cells[15].Value);
                       new SqlParameter("@refid",Convert.ToInt16(HFIT.Rows[i].Cells[15].Value)),
                     new SqlParameter("@doctypeid",Convert.ToInt16(HFIT.Rows[i].Cells[16].Value)),
                        //new SqlParameter("@FTYPE",Convert.ToString(HFIT.Rows[i].Cells[17].Value)),
                          new SqlParameter("@uom",Convert.ToString(HFIT.Rows[i].Cells[3].Value)),
                            new SqlParameter("@socno",HFIT.Rows[i].Cells[19].Value),
                     new SqlParameter("@workorder",HFIT.Rows[i].Cells[18].Value),
                           new SqlParameter("@processid",HFIT.Rows[i].Cells[21].Value),
                                new SqlParameter("@processdetid",HFIT.Rows[i].Cells[22].Value),

                };

                    db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_JOIM", para1, conn);
                }




                for (int i = 0; i < RQGR.RowCount - 1; i++)
                {

                    conn.Close();
                    conn.Open();
                    if(RQGR.Rows[i].Cells[7].Value=="")
                    {
                        RQGR.Rows[i].Cells[7].Value = "0";

                    }

                    SqlParameter[] para1 ={


                    new SqlParameter("@slno", RQGR.Rows[i].Cells[0].Value),
                    new SqlParameter("@iTEMNAME",RQGR.Rows[i].Cells[2].Value),
                    new SqlParameter("@ITEMID", RQGR.Rows[i].Cells[1].Value),

                    new SqlParameter("@QTY",RQGR.Rows[i].Cells[4].Value),
                    new SqlParameter("@ROLLS", RQGR.Rows[i].Cells[5].Value),


                     new SqlParameter("@HEADID", txtgrnid.Text),
                      new SqlParameter("@refid", Convert.ToInt16(RQGR.Rows[i].Cells[7].Value)),
                      new SqlParameter("@uom",Convert.ToString(RQGR.Rows[i].Cells[3].Value)),

                           new SqlParameter("@socno",RQGR.Rows[i].Cells[8].Value),
                     new SqlParameter("@workorder",RQGR.Rows[i].Cells[9].Value),
                     new SqlParameter("@style",RQGR.Rows[i].Cells[10].Value),
                            new SqlParameter("@processid",RQGR.Rows[i].Cells[11].Value),
                                new SqlParameter("@processdetid",RQGR.Rows[i].Cells[12].Value),
                };

                    db.ExecuteNonQuery(CommandType.StoredProcedure, "sp_joiitems", para1, conn);
                }


               
            }
            else

            {

            
                qq = RQGR.RowCount - 1;
                if (qq >= 1)
                {


                }
                else
                {
                    MessageBox.Show("Please Completed Process");
                    return;


                }
                conn.Close();
                conn.Open();

                SqlParameter[] para ={

                 new SqlParameter("@DOCNO",txtgrn.Text),
                    new SqlParameter("@DOCDATE",Convert.ToDateTime(dtpgrndt.Text)),
                    new SqlParameter("@REFNO",txtorderdes.Text),
                    new SqlParameter("@DCNO",txtqty.Text),
                    new SqlParameter("@DCDATE",Convert.ToDateTime(reqdt.Text)),
                    new SqlParameter("@DOCTYPEID", Genclass.Dtype),
                    new SqlParameter("@SUPPLIERUID",txtpuid.Text),
                    new SqlParameter("@PLACEOFSUPPLY",cboprocess.Text),
                    new SqlParameter("@MODE",txtmodem.Text),
                    new SqlParameter("@REMARKS",txtitem.Text),
                    new SqlParameter("@VEHICLENO",txtvehicle.Text),
                    new SqlParameter("@REASON",  txtterms.Text),
                    new SqlParameter("@REFID",'0'),
                    new SqlParameter("@YEARID",Genclass.Yearid),
                    new SqlParameter("@ACTIVE", SP),
                    new SqlParameter("@UID", txtgrnid.Text)


                };
                db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_JOUPDATE", para, conn);

                conn.Close();
                conn.Open();
                qur.CommandText = "delete from JOISSUEPGMITEMS where headid=" + txtgrnid.Text + "";
                qur.ExecuteNonQuery();
                qur.CommandText = "delete from joDcitems where headiD=" + txtgrnid.Text + " ";
                qur.ExecuteNonQuery();
         

                

                for (int i = 0; i < HFIT.RowCount - 1; i++)
                {
                    if (HFIT.Rows[i].Cells[15].Value.ToString() == "" || HFIT.Rows[i].Cells[15].Value == null)
                    {
                        HFIT.Rows[i].Cells[15].Value = "0";

                    }
                    if (HFIT.Rows[i].Cells[17].Value.ToString() == "" || HFIT.Rows[i].Cells[17].Value == null)
                    {
                        HFIT.Rows[i].Cells[17].Value = "0";

                    }

                    conn.Close();
                    conn.Open();
                    SqlParameter[] para1 ={
                    new SqlParameter("@slno", HFIT.Rows[i].Cells[0].Value),
                    new SqlParameter("@iTEMNAME",HFIT.Rows[i].Cells[1].Value),
                    new SqlParameter("@ITEMID", HFIT.Rows[i].Cells[11].Value),
                    new SqlParameter("@BILLUOMID", HFIT.Rows[i].Cells[10].Value),
                    new SqlParameter("@UOMID", HFIT.Rows[i].Cells[9].Value),
                    new SqlParameter("@QTY", HFIT.Rows[i].Cells[6].Value),
                    new SqlParameter("@BILLQTY",HFIT.Rows[i].Cells[7].Value),
                    new SqlParameter("@TYPE", HFIT.Rows[i].Cells[5].Value),
                    new SqlParameter("@RATE", HFIT.Rows[i].Cells[8].Value),
                    new SqlParameter("@HEADID",txtgrnid.Text),
                    new SqlParameter("@COLOR",HFIT.Rows[i].Cells[2].Value),
                    new SqlParameter("@ITEMDES",HFIT.Rows[i].Cells[13].Value),
                       new SqlParameter("@useitemid",HFIT.Rows[i].Cells[14].Value),
                              new SqlParameter("@refid",Convert.ToInt16(HFIT.Rows[i].Cells[15].Value)),
                     new SqlParameter("@doctypeid",Convert.ToInt16(HFIT.Rows[i].Cells[16].Value)),
                            //new SqlParameter("@ftype",Convert.ToString(HFIT.Rows[i].Cells[17].Value)),
                             new SqlParameter("@uom",Convert.ToString(HFIT.Rows[i].Cells[3].Value)),
                                new SqlParameter("@socno",HFIT.Rows[i].Cells[19].Value),
                     new SqlParameter("@workorder",HFIT.Rows[i].Cells[18].Value),
                         new SqlParameter("@processid",HFIT.Rows[i].Cells[21].Value),
                                new SqlParameter("@processdetid",HFIT.Rows[i].Cells[22].Value),

                };

                    db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_JOIM", para1, conn);
                }


                for (int i = 0; i < RQGR.RowCount - 1; i++)
                {

                    conn.Close();
                    conn.Open();

                    if (RQGR.Rows[i].Cells[7].Value == "")
                    {
                        RQGR.Rows[i].Cells[7].Value = "0";

                    }

                    SqlParameter[] para1 ={


                    new SqlParameter("@slno", RQGR.Rows[i].Cells[0].Value),
                    new SqlParameter("@iTEMNAME",RQGR.Rows[i].Cells[2].Value),
                    new SqlParameter("@ITEMID", RQGR.Rows[i].Cells[1].Value),

                    new SqlParameter("@QTY",RQGR.Rows[i].Cells[4].Value),
                    new SqlParameter("@ROLLS", RQGR.Rows[i].Cells[5].Value),


                     new SqlParameter("@HEADID", txtgrnid.Text),
                     new SqlParameter("@refid", Convert.ToInt16(RQGR.Rows[i].Cells[7].Value)),

                        new SqlParameter("@uom",Convert.ToString(RQGR.Rows[i].Cells[3].Value)),
                                    new SqlParameter("@socno",RQGR.Rows[i].Cells[8].Value),
                     new SqlParameter("@workorder",RQGR.Rows[i].Cells[9].Value),
                           new SqlParameter("@style",RQGR.Rows[i].Cells[10].Value),
                            new SqlParameter("@processid",RQGR.Rows[i].Cells[11].Value),
                                new SqlParameter("@processdetid",RQGR.Rows[i].Cells[12].Value),
                };

                    db.ExecuteNonQuery(CommandType.StoredProcedure, "sp_joiitems", para1, conn);
                }

              
            }
            conn.Close();
            conn.Open();
           if(txtgrnid.Text!="")
            { 
                if (mode == 1)
                {
                    if (Genclass.Dtype == 560 || Genclass.Dtype == 210)
                    {
                       


                        qur.CommandText = "exec SP_stocklegJoiIssuefabric " + txtgrnid.Text + "," + Genclass.Dtype + " ,1";
                        qur.ExecuteNonQuery();
                    }
                    else

                    {
                        qur.CommandText = "exec SP_stocklegJoiIssue " + txtgrnid.Text + "," + Genclass.Dtype + " ,1";
                        qur.ExecuteNonQuery();

                    }

                    qur.CommandText = "exec POST_SUPPLIERSTOCK_SP " + txtgrnid.Text + "," + Genclass.Dtype + " ";
                    qur.ExecuteNonQuery();
                }
                else
                {
                    qur.CommandText = "delete from SUPPLIER_STOCK_LEDGER where hdid= " + txtgrnid.Text + " and doctypeid=" + Genclass.Dtype + " ";
                    qur.ExecuteNonQuery();

                    if (Genclass.Dtype == 560 || Genclass.Dtype == 210)
                    {



                        qur.CommandText = "exec SP_stocklegJoiIssuefabric " + txtgrnid.Text + "," + Genclass.Dtype + " ,2";
                        qur.ExecuteNonQuery();
                    }
                    else

                    {
                        qur.CommandText = "exec SP_stocklegJoiIssue " + txtgrnid.Text + "," + Genclass.Dtype + " ,2";
                        qur.ExecuteNonQuery();

                    }

                    qur.CommandText = "exec POST_SUPPLIERSTOCK_SP " + txtgrnid.Text + "," + Genclass.Dtype + " ";
                    qur.ExecuteNonQuery();
                }

            }

            conn.Close();
            conn.Open();
            if (Genclass.Dtype == 560)
            {
                string qur1 = "select DISTINCT B.uid,c.processname from jo a inner join JOISSUEPGMITEMS b on a.uid=b.headid  left join processm c on b.refid=c.uid left join PROCESSDETLIST cc on c.uid=cc.processid and cc.headid=b.doctypeid where a.doctypeid=560  and a.uid=" + txtgrnid.Text + "  and  c.processname is not null ";
                SqlCommand cmd = new SqlCommand(qur1, conn);
                SqlDataAdapter apt = new SqlDataAdapter(cmd);
                DataTable tab = new DataTable();
                apt.Fill(tab);
                Genclass.name = "";

                for (int k = 0; k < tab.Rows.Count; k++)
                {
                    if (k == 0)
                    {
                        Genclass.name = tab.Rows[k]["processname"].ToString();

                    }
                    else
                    {

                        Genclass.name = Genclass.name + "/" + tab.Rows[k]["processname"].ToString();
                    }


                }

                conn.Close();
                conn.Open();
                if (mode == 1)
                {
                    qur.CommandText = "insert into FABRICPROCESSM values (" + txtgrnid.Text + ",'" + Genclass.name + "')";
                    qur.ExecuteNonQuery();
                }
                else
                {
                    qur.CommandText = "delete from FABRICPROCESSM  where refid=" + txtgrnid.Text + "";
                    qur.ExecuteNonQuery();

                    qur.CommandText = "insert into FABRICPROCESSM values (" + txtgrnid.Text + ",'" + Genclass.name + "')";
                    qur.ExecuteNonQuery();

                }

            
            }


            conn.Close();
            conn.Open();
            if (mode == 1)
            {
                qur.CommandText = "update doctypem set lastno=lastno+1 where doctypeid=" + Genclass.Dtype + " and finyear='19-20'";
                qur.ExecuteNonQuery();

            }
            MessageBox.Show("Save Record");
            Editpnl.Visible = false;
            tabC.Visible = false;
            Genpan.Visible = true;
            panadd.Visible = true;
            LoadGetJobCard(1);

        }

        private void buttnext1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dtpgrndt_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                e.Handled = true;
                SendKeys.Send("{TAB}");
            }
        }

        private void txtqty_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                e.Handled = true;
                SendKeys.Send("{TAB}");
            }
        }

        private void txtname_KeyPress(object sender, KeyPressEventArgs e)
        {
            //if (e.KeyChar == (char)13)
            //{
            //    e.Handled = true;
            //    SendKeys.Send("{TAB}");
            //}
        }

        private void reqdt_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                e.Handled = true;
                SendKeys.Send("{TAB}");
            }
        }

        private void txtitem_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                e.Handled = true;
                SendKeys.Send("{TAB}");
            }
        }

        private void txtvehicle_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                e.Handled = true;
                SendKeys.Send("{TAB}");
            }
        }

        private void txtmodem_KeyPress(object sender, KeyPressEventArgs e)
        {
            //if (e.KeyChar == (char)13)
            //{
            //    e.Handled = true;
            //    SendKeys.Send("{TAB}");
            //}
        }

        private void txtdcqty_KeyPress(object sender, KeyPressEventArgs e)
        {
            //if (e.KeyChar == (char)13)
            //{
            //    e.Handled = true;
            //    SendKeys.Send("{TAB}");
            //}

        }

        private void cbouom_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                e.Handled = true;
                SendKeys.Send("{TAB}");
            }

        }

        private void cbotype_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                e.Handled = true;
                SendKeys.Send("{TAB}");
            }
        }

        private void txtuom_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                e.Handled = true;
                SendKeys.Send("{TAB}");
            }
        }

        private void txtbillqty_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                e.Handled = true;
                SendKeys.Send("{TAB}");
            }
        }

        private void txtrate_KeyPress(object sender, KeyPressEventArgs e)
        {
            
        }

        private void button11_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void txtoutqty_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                e.Handled = true;
                SendKeys.Send("{TAB}");
            }
        }

        private void txtoutrolls_KeyPress(object sender, KeyPressEventArgs e)
        {
            
        }

        private void txtqty_TextChanged(object sender, EventArgs e)
        {

        }

        private void HFIT_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
           
        }

        private void txtmodem_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtmodem_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode==Keys.Enter)
            {

                txtdcqty.Focus();
            }
        }

        private void txtoutitem_TextChanged(object sender, EventArgs e)
        {
          
        }

        private void HFGP2_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                SelectId = 1;
                int Index = HFGP2.SelectedCells[0].RowIndex;
                if (Genclass.type == 5)
                {
                    txtoutitem.Text = HFGP2.Rows[Index].Cells[1].Value.ToString();
                    txtoutitem.Tag = HFGP2.Rows[Index].Cells[0].Value.ToString();
                    txtoutqty.Text = HFGP2.Rows[Index].Cells[2].Value.ToString();
                 
                    txtoutuom.Text = HFGP2.Rows[Index].Cells[4].Value.ToString();
                    txtoutqty.Focus();
                }


                lkppnl.Visible = false;
                SelectId = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

        }

        private void HFGP2_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SelectId = 1;
                int Index = HFGP2.SelectedCells[0].RowIndex;

                if (Genclass.type == 5)
                {
                    txtoutitem.Text = HFGP2.Rows[Index].Cells[1].Value.ToString();
                    txtoutitem.Tag = HFGP2.Rows[Index].Cells[0].Value.ToString();
                    txtoutqty.Text = HFGP2.Rows[Index].Cells[2].Value.ToString();
                 
                    txtoutuom.Text = HFGP2.Rows[Index].Cells[4].Value.ToString();
                    txtoutqty.Focus();
                }



                lkppnl.Visible = false;
                SelectId = 0;
            }
        }

        private void cbouom_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void label14_Click(object sender, EventArgs e)
        {

        }

        private void tabC_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tabC.SelectedTab == tabPage2)
            {
                txtoutitem.Focus();
            }
          
            else if (tabC.SelectedTab == tabPage3)
            {

                txtdcqty.Focus();
            }
        }

        private void txtscr11_TextChanged(object sender, EventArgs e)
        {
            bsc.Filter = string.Format("itemdes LIKE '%{0}%' ", txtscr11.Text);
        }

        private void txtscr11_KeyDown(object sender, KeyEventArgs e)
        {

        }

        private void txtcolor_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                e.Handled = true;
                SendKeys.Send("{TAB}");
            }
        }

        private void txtrate_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode==Keys.Enter)

            {
                button11_Click(sender, e);

            }
        }

        private void txtoutrolls_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)

            {
                button17_Click(sender, e);

            }
        }

        private void cboprocess_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(cboprocess.ValueMember!="")
            {
                if (cboprocess.Text != "")
                {
                    //if (this.Text == "Fabric Process Deliveries")
                    //{

                    //    Genclass.strsql = "SELECT B.SEQNO,c.processsname as sname,processname FROM PROCESSDET A INNER JOIN  PROCESSDETLIST B ON A.UID=B.HEADID INNER JOIN  PROCESSM C ON B.PROCESSID=C.UID   where b.uid=" + cboprocess.SelectedValue + " and c.processname='" + cboprocess.Text + "' and b.type='Fabric' ";
                    //    Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                    //    SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
                    //    DataTable tap = new DataTable();
                    //    aptr.Fill(tap);
                    //    if (tap.Rows.Count > 0)
                    //    {
                    //        Genclass.Prtid = Convert.ToInt32(tap.Rows[0]["SEQNO"].ToString());
                    //        textBox16.Text = tap.Rows[0]["sname"].ToString();

                    //        Genclass.address = tap.Rows[0]["processname"].ToString();



                    //    }
                    //}
                    //else if (this.Text == "Yarn Process Delivery")
                    //{


                    //    Genclass.strsql = "SELECT B.SEQNO,c.processsname as sname,processname FROM PROCESSDET A INNER JOIN  PROCESSDETLIST B ON A.UID=B.HEADID INNER JOIN  PROCESSM C ON B.PROCESSID=C.UID   where b.uid=" + cboprocess.SelectedValue + " and c.processname='" + cboprocess.Text + "' and b.type='Yarn' ";
                    //    Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                    //    SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
                    //    DataTable tap = new DataTable();
                    //    aptr.Fill(tap);
                    //    if (tap.Rows.Count > 0)
                    //    {
                    //        Genclass.Prtid = Convert.ToInt32(tap.Rows[0]["SEQNO"].ToString());
                    //        textBox16.Text = tap.Rows[0]["sname"].ToString();

                    //        Genclass.address = tap.Rows[0]["processname"].ToString();



                    //    }

                    //}

                    //if (cbopro1.ValueMember != "")
                    //{
                    //    if (cbopro1.Text != "")
                    //    {
                    //        if (Genclass.Dtype == 210)
                    //        {
                    //            Genclass.strsql1 = "SP_GETJOIDCITEMS1  '" + txtwor.Text + "', '" + txtsocno.Text + "' ,'" + cbopro1.Text + "'";
                    //            Genclass.cmd = new SqlCommand(Genclass.strsql1, conn);
                    //            SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
                    //            DataTable tap1 = new DataTable();
                    //            aptr1.Fill(tap1);

                    //            bsParty.DataSource = tap1;


                    //            HFGP1.DataSource = null;
                    //            HFGP1.AutoGenerateColumns = false;
                    //            HFGP1.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
                    //            HFGP1.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
                    //            HFGP1.ColumnCount = 8;
                    //            HFGP1.Columns[0].Name = "itemid";
                    //            HFGP1.Columns[0].HeaderText = "itemid";
                    //            HFGP1.Columns[0].DataPropertyName = "itemid";
                    //            HFGP1.Columns[1].Name = "itemname";
                    //            HFGP1.Columns[1].HeaderText = "Itemname";
                    //            HFGP1.Columns[1].DataPropertyName = "itemname";
                    //            HFGP1.Columns[1].Width = 650;
                    //            HFGP1.Columns[2].Name = "Stockqty";
                    //            HFGP1.Columns[2].HeaderText = "Stockqty";
                    //            HFGP1.Columns[2].DataPropertyName = "Stockqty";
                    //            HFGP1.Columns[2].Width = 100;
                    //            HFGP1.Columns[3].Name = "GENERALNAME";
                    //            HFGP1.Columns[3].HeaderText = "GENERALNAME";
                    //            HFGP1.Columns[3].DataPropertyName = "GENERALNAME";


                    //            HFGP1.Columns[4].Name = "UID";
                    //            HFGP1.Columns[4].HeaderText = "UID";
                    //            HFGP1.Columns[4].DataPropertyName = "UID";

                    //            HFGP1.Columns[4].Visible = false;
                    //            HFGP1.Columns[5].Name = "REMARKS";
                    //            HFGP1.Columns[5].HeaderText = "REMARKS";
                    //            HFGP1.Columns[5].DataPropertyName = "REMARKS";
                    //            HFGP1.Columns[5].Visible = false;
                    //            HFGP1.Columns[6].Name = "DOCNO";
                    //            HFGP1.Columns[6].HeaderText = "DOCNO";
                    //            HFGP1.Columns[6].DataPropertyName = "DOCNO";
                    //            HFGP1.Columns[7].Name = "grnno";
                    //            HFGP1.Columns[7].HeaderText = "grnno";
                    //            HFGP1.Columns[7].DataPropertyName = "grnno";
                    //            HFGP1.DataSource = bsParty;
                    //            HFGP1.Columns[7].Visible = false;
                    //            HFGP1.Columns[6].Visible = false;
                    //            HFGP1.Columns[0].Visible = false;
                    //            HFGP1.Columns[1].Width = 650;
                    //            HFGP1.Columns[2].Width = 120;
                    //            HFGP1.Columns[3].Visible = false;
                    //            HFGP1.Columns[4].Visible = false;
                    //            HFGP1.Columns[5].Visible = false;
                    //            HFGP1.Columns[6].Width = 100;

                    //        }
                    //        else

                    //        {
                    //            Genclass.strsql = "SELECT distinct seqno FROM PROCESSDET A INNER JOIN  PROCESSDETLIST B ON A.UID=B.HEADID  inner join   jjissuetemp hh on b.itemid=hh.itemuid    INNER JOIN  PROCESSM C ON B.PROCESSID=C.UID     where  c.processname='" + cbopro1.Text + "'  and a.docno ='" + txtwor.Text + "' and a.socno='" + txtsocno.Text + "' ";
                    //            Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                    //            SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
                    //            DataTable tap = new DataTable();
                    //            aptr.Fill(tap);
                    //            if (tap.Rows.Count > 0)
                    //            {
                    //                Genclass.Prtid = Convert.ToInt32(tap.Rows[0]["SEQNO"].ToString());
                    //                //textBox16.Text = tap.Rows[0]["sname"].ToString();

                    //                //Genclass.address = tap.Rows[0]["processname"].ToString();



                    //            }

                    //            Genclass.strsql1 = "SP_GETJOIDCITEMS2  '" + txtwor.Text + "', '" + txtsocno.Text + "' ,'" + cbopro1.Text + "','" + Genclass.Prtid + "' ";
                    //            Genclass.cmd = new SqlCommand(Genclass.strsql1, conn);
                    //            SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
                    //            DataTable tap1 = new DataTable();
                    //            aptr1.Fill(tap1);

                    //            bsParty.DataSource = tap1;


                    //            HFGP1.DataSource = null;
                    //            HFGP1.AutoGenerateColumns = false;
                    //            HFGP1.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
                    //            HFGP1.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
                    //            HFGP1.ColumnCount = 8;
                    //            HFGP1.Columns[0].Name = "itemid";
                    //            HFGP1.Columns[0].HeaderText = "itemid";
                    //            HFGP1.Columns[0].DataPropertyName = "itemid";
                    //            HFGP1.Columns[1].Name = "itemname";
                    //            HFGP1.Columns[1].HeaderText = "Itemname";
                    //            HFGP1.Columns[1].DataPropertyName = "itemname";
                    //            HFGP1.Columns[1].Width = 650;
                    //            HFGP1.Columns[2].Name = "Stockqty";
                    //            HFGP1.Columns[2].HeaderText = "Stockqty";
                    //            HFGP1.Columns[2].DataPropertyName = "Stockqty";
                    //            HFGP1.Columns[2].Width = 100;
                    //            HFGP1.Columns[3].Name = "GENERALNAME";
                    //            HFGP1.Columns[3].HeaderText = "GENERALNAME";
                    //            HFGP1.Columns[3].DataPropertyName = "GENERALNAME";


                    //            HFGP1.Columns[4].Name = "UID";
                    //            HFGP1.Columns[4].HeaderText = "UID";
                    //            HFGP1.Columns[4].DataPropertyName = "UID";

                    //            HFGP1.Columns[4].Visible = false;
                    //            HFGP1.Columns[5].Name = "REMARKS";
                    //            HFGP1.Columns[5].HeaderText = "REMARKS";
                    //            HFGP1.Columns[5].DataPropertyName = "REMARKS";
                    //            HFGP1.Columns[5].Visible = false;
                    //            HFGP1.Columns[6].Name = "DOCNO";
                    //            HFGP1.Columns[6].HeaderText = "DOCNO";
                    //            HFGP1.Columns[6].DataPropertyName = "DOCNO";
                    //            HFGP1.Columns[7].Name = "grnno";
                    //            HFGP1.Columns[7].HeaderText = "grnno";
                    //            HFGP1.Columns[7].DataPropertyName = "grnno";
                    //            HFGP1.DataSource = bsParty;
                    //            HFGP1.Columns[7].Visible = false;
                    //            HFGP1.Columns[6].Visible = false;
                    //            HFGP1.Columns[0].Visible = false;
                    //            HFGP1.Columns[1].Width = 650;
                    //            HFGP1.Columns[2].Width = 120;
                    //            HFGP1.Columns[3].Visible = false;
                    //            HFGP1.Columns[4].Visible = false;
                    //            HFGP1.Columns[5].Visible = false;
                    //            HFGP1.Columns[6].Width = 100;


                    //        }
                    //    }
                    //}
                    loadfabric();
                }
            }
        }

        private void button10_Click_1(object sender, EventArgs e)
        {
            try
            {
                SelectId = 1;
                int Index = HFGP1.SelectedCells[0].RowIndex;
                txtoutitem.Tag = HFGP1.Rows[Index].Cells[0].Value.ToString();
                txtoutitem.Text = HFGP1.Rows[Index].Cells[1].Value.ToString();
               
                    txtoutqty.Text = HFGP1.Rows[Index].Cells[2].Value.ToString();
                    txtoutuom.Text = HFGP1.Rows[Index].Cells[3].Value.ToString();
                    txtoutuom.Tag = HFGP1.Rows[Index].Cells[4].Value.ToString();
                txtoutqty.Tag = HFGP1.Rows[Index].Cells[5].Value.ToString();
              
                txtoutqty.Focus();
                


                panel3.Visible = false;
                SelectId = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void HFGP1_DoubleClick_1(object sender, EventArgs e)
        {
            try
            {
                SelectId = 1;
                int Index = HFGP1.SelectedCells[0].RowIndex;

          
                txtoutitem.Tag = HFGP1.Rows[Index].Cells[0].Value.ToString();
                txtoutitem.Text = HFGP1.Rows[Index].Cells[1].Value.ToString();

                txtoutqty.Text = HFGP1.Rows[Index].Cells[2].Value.ToString();
                txtoutuom.Text = HFGP1.Rows[Index].Cells[3].Value.ToString();
                txtoutuom.Tag = HFGP1.Rows[Index].Cells[4].Value.ToString();
                txtoutqty.Focus();
                if (Genclass.Dtype != 210)
                {
                    txtoutqty.Text = HFGP1.Rows[Index].Cells[2].Value.ToString();
                    txtoutuom.Tag = HFGP1.Rows[Index].Cells[4].Value.ToString();
                }
                txtoutqty.Focus();




                panel3.Visible = false;
                SelectId = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void HFGP1_KeyDown_1(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                int Index = HFGP1.SelectedCells[0].RowIndex;
                txtoutitem.Text = HFGP1.Rows[Index].Cells[1].Value.ToString();
                txtoutitem.Tag = HFGP1.Rows[Index].Cells[0].Value.ToString();
                txtoutqty.Text = HFGP1.Rows[Index].Cells[2].Value.ToString();
                txtoutuom.Text = HFGP1.Rows[Index].Cells[3].Value.ToString();
                txtoutuom.Tag = HFGP1.Rows[Index].Cells[4].Value.ToString();
                txtoutqty.Focus();

                panel3.Visible = false;
                
            }
        }

        private void txtscr11_KeyDown_1(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.F10)
                {
                    panel3.Visible = false;
                }
                else if (e.KeyCode == Keys.F2)
                {
                    SelectId = 1;
                    int Index = HFGP2.SelectedCells[0].RowIndex;
                    txtoutitem.Text = HFGP2.Rows[Index].Cells[1].Value.ToString();
                    txtoutitem.Tag = HFGP2.Rows[Index].Cells[0].Value.ToString();
                    txtoutqty.Text = HFGP2.Rows[Index].Cells[2].Value.ToString();
                    txtoutrolls.Text = HFGP2.Rows[Index].Cells[3].Value.ToString();
                    txtoutuom.Text = HFGP2.Rows[Index].Cells[4].Value.ToString();
                    txtoutqty.Focus();
                    lkppnl.Visible = false;
                    SelectId = 0;
                }
                else if (e.KeyCode == Keys.Escape)
                {
                    panel3.Visible = false;
                }
                else if (e.KeyValue == 40)
                {
                    HFGP1.Select();
                }
                else if (e.KeyCode == Keys.Enter)
                {
                    txtoutitem_Click(sender, e);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void button9_Click_1(object sender, EventArgs e)
        {
            panel3.Visible = false;
        }

        private void button19_Click(object sender, EventArgs e)
        {
            SelectId = 1;
            if(HFG9.Rows.Count > 1)
            {
                int Index = HFG9.SelectedCells[0].RowIndex;

                txtdcqty.Text = HFG9.Rows[Index].Cells[1].Value.ToString();
                txtitemid.Text = HFG9.Rows[Index].Cells[0].Value.ToString();
                txtoutoutqty.Tag = HFG9.Rows[Index].Cells[2].Value.ToString();
                txtoutoutqty.Text = HFG9.Rows[Index].Cells[3].Value.ToString();
                txtdcqty.Tag = HFG9.Rows[Index].Cells[4].Value.ToString();
                textBox10.Tag = HFG9.Rows[Index].Cells[5].Value.ToString();
                txtitemid.Tag = HFG9.Rows[Index].Cells[6].Value.ToString();
                txtuom.Text= HFG9.Rows[Index].Cells[7].Value.ToString();
                txtbillqty.Text = HFG9.Rows[Index].Cells[7].Value.ToString();
                textBox10.Text = HFG9.Rows[Index].Cells[8].Value.ToString();
                txtbeam.Text = HFG9.Rows[Index].Cells[9].Value.ToString();
                txtrate.Text = HFG9.Rows[Index].Cells[10].Value.ToString();
                txtcolor.Focus();
                panel4.Visible = false;
            }
        }

        private void HFG9_DockChanged(object sender, EventArgs e)
        {

        }

        private void HFG9_DoubleClick(object sender, EventArgs e)
        {
            SelectId = 1;
            int Index = HFG9.SelectedCells[0].RowIndex;

            txtdcqty.Text = HFG9.Rows[Index].Cells[1].Value.ToString();
            txtitemid.Text = HFG9.Rows[Index].Cells[0].Value.ToString();
            txtoutoutqty.Tag = HFG9.Rows[Index].Cells[2].Value.ToString();
            txtoutoutqty.Text = HFG9.Rows[Index].Cells[3].Value.ToString();
            txtdcqty.Tag = HFG9.Rows[Index].Cells[4].Value.ToString();
            textBox10.Tag = HFG9.Rows[Index].Cells[5].Value.ToString();
            txtitemid.Tag = HFG9.Rows[Index].Cells[6].Value.ToString();
            txtuom.Text = HFG9.Rows[Index].Cells[7].Value.ToString();
            txtbillqty.Text = HFG9.Rows[Index].Cells[7].Value.ToString();
            textBox10.Text = HFG9.Rows[Index].Cells[8].Value.ToString();
            txtbeam.Text = HFG9.Rows[Index].Cells[9].Value.ToString();
            txtrate.Text = HFG9.Rows[Index].Cells[10].Value.ToString();
            txtcolor.Focus();
            panel4.Visible = false;
        }

        private void HFG9_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SelectId = 1;
                int Index = HFG9.SelectedCells[0].RowIndex;
                txtdcqty.Text = HFG9.Rows[Index].Cells[1].Value.ToString();
                txtitemid.Text = HFG9.Rows[Index].Cells[0].Value.ToString();
                txtoutoutqty.Tag = HFG9.Rows[Index].Cells[2].Value.ToString();
                txtoutoutqty.Text = HFG9.Rows[Index].Cells[3].Value.ToString();
                txtdcqty.Tag = HFG9.Rows[Index].Cells[4].Value.ToString();
                textBox10.Tag = HFG9.Rows[Index].Cells[5].Value.ToString();
                txtitemid.Tag = HFG9.Rows[Index].Cells[6].Value.ToString();
                txtuom.Text = HFG9.Rows[Index].Cells[7].Value.ToString();
                txtrate.Text = HFG9.Rows[Index].Cells[10].Value.ToString();
                txtbillqty.Text = HFG9.Rows[Index].Cells[7].Value.ToString();
                textBox10.Text = HFG9.Rows[Index].Cells[8].Value.ToString();
                txtbeam.Text = HFG9.Rows[Index].Cells[9].Value.ToString();
                txtcolor.Focus();
                panel4.Visible = false;
            }
        }

        private void button21_Click(object sender, EventArgs e)
        {
            panel4.Visible = false;
        }

        private void DataGridCommon_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void TextBox1_TextChanged(object sender, EventArgs e)
        {
            bsParty.Filter = string.Format("itemspec1 LIKE '%{0}%' ", textBox1.Text);
        }

        private void txtscr11_TextChanged_1(object sender, EventArgs e)
        {
            bsc.Filter = string.Format("itemdes LIKE '%{0}%' ", txtscr11.Text);
        }

        private void HFGP1_Click(object sender, EventArgs e)
        {
            HFGP1_DoubleClick_1(sender, e);
        }

        private void HFGP1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void button13_Click(object sender, EventArgs e)
        {
            conn.Close();
            conn.Open();
            Genclass.Prtid = Convert.ToInt16(HFGP.CurrentRow.Cells[0].Value.ToString());
            if (Genclass.Dtype == 560)
            {
                string qur = "select  a.uid,c.processname from jo a inner join JOISSUEPGMITEMS b on a.uid=b.headid  left join processm1 c on b.refid=c.uid left join PROCESSDETLIST cc on c.uid=cc.processid and cc.headid=b.doctypeid where a.doctypeid=560  and a.uid=" + Genclass.Prtid + "  and  c.processname is not null  group BY seqno,a.uid,c.processname";
                SqlCommand cmd = new SqlCommand(qur, conn);
                SqlDataAdapter apt = new SqlDataAdapter(cmd);
                DataTable tab = new DataTable();
                apt.Fill(tab);
                Genclass.name = "";

                for (int k = 0; k < tab.Rows.Count; k++)
                {
                    if (k == 0)
                    {
                        Genclass.name = tab.Rows[k]["processname"].ToString();

                    }
                    else
                    {

                        Genclass.name = Genclass.name + "/" + tab.Rows[k]["processname"].ToString();
                    }


                }




            }
            Genclass.slno = 1;
            Crviewer crv = new Crviewer();
            crv.Show();
            conn.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            conn.Close();
            conn.Open();
            Genclass.Prtid = Convert.ToInt16(HFGP.CurrentRow.Cells[0].Value.ToString());
         

            if (Genclass.Dtype == 560)
            {
                string qur = "   select distinct B.UID,C.PROCESSNAME  PROCESS from jo a inner join JOISSUEPGMITEMS b on a.uid=b.headid     INNER JOIN PROCESSM C ON B.PROCESSID=C.UID  where a.uid=" + Genclass.Prtid + "   ORDER BY  B.UID";
                SqlCommand cmd = new SqlCommand(qur, conn);
                SqlDataAdapter apt = new SqlDataAdapter(cmd);
                DataTable tab = new DataTable();
                apt.Fill(tab);
                Genclass.name = "";

                for(int k=0;k<tab.Rows.Count; k++)
                {
                    if(k==0)
                    {
                        Genclass.name = tab.Rows[k]["process"].ToString();

                    }
                    else
                    {

                        Genclass.name = Genclass.name + "/" + tab.Rows[k]["process"].ToString();
                    }


                }




            }


            for (int i = 1; i < 4; i++)
            {
                string pp;
                Genclass.slno = i;






                if (Genclass.Dtype == 210)
                {
                    pp = "SP_GETISSURPTreIssue";
                }
                else

                {
                    pp = "SP_GETISSURPTfabIssue";

                }
                

                SqlDataAdapter da = new SqlDataAdapter(pp, conn);
                da.SelectCommand.CommandType = CommandType.StoredProcedure;
                da.SelectCommand.Parameters.Add("@uid", SqlDbType.Int).Value = Genclass.Prtid;
                da.SelectCommand.Parameters.Add("@DOCTYPEID", SqlDbType.Int).Value = Genclass.Dtype;
                

                if(Genclass.Dtype == 560)
                {
                    da.SelectCommand.Parameters.Add("@name", SqlDbType.NVarChar).Value = Genclass.name;

                }

                DataSet ds = new DataSet();
                da.Fill(ds, "PRINT");
                if (Genclass.Dtype == 210)
                {
                    doc.Load(Application.StartupPath + "\\JobOrderIssuePrtReIssue.rpt");
                }
                else

                {
                    doc.Load(Application.StartupPath + "\\JobOrderIssuePrtfabIssue.rpt");

                }


                SqlDataAdapter da1 = new SqlDataAdapter("SP_GETDCITEMSRPTIISUE", conn);
                da1.SelectCommand.CommandType = CommandType.StoredProcedure;
                da1.SelectCommand.Parameters.Add("@uid", SqlDbType.Int).Value = Genclass.Prtid;
                da1.SelectCommand.Parameters.Add("@DOCTYPEID", SqlDbType.Int).Value = Genclass.Dtype;
                DataTable ds1 = new DataTable("Subrpt");
                da1.Fill(ds1);
                ds.Tables.Add(ds1);




                doc.Subreports["NewDC.rpt"].SetDataSource(ds1);

                doc.SetDataSource(ds);

                doc.PrintToPrinter(1, false, 0, 0);
            }

        }

        private void Genpan_Paint(object sender, PaintEventArgs e)
        {

        }

        private void txtcolor_TextChanged(object sender, EventArgs e)
        {

        }

        private void cboprocess_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.F10)
                {
                    panel4.Visible = false;
                }
                else if (e.KeyCode == Keys.F2)
                {
                    if (HFG9.Rows.Count > 1)
                    {
                        int Index = HFG9.SelectedCells[0].RowIndex;

                        txtdcqty.Text = HFG9.Rows[Index].Cells[1].Value.ToString();
                        txtitemid.Text = HFG9.Rows[Index].Cells[0].Value.ToString();
                        txtoutoutqty.Tag = HFG9.Rows[Index].Cells[2].Value.ToString();
                        txtoutoutqty.Text = HFG9.Rows[Index].Cells[3].Value.ToString();
                        txtdcqty.Tag = HFG9.Rows[Index].Cells[4].Value.ToString();
                        txtitemid.Tag = HFG9.Rows[Index].Cells[5].Value.ToString();
                        textBox10.Tag = HFG9.Rows[Index].Cells[6].Value.ToString();
                        txtuom.Text = HFG9.Rows[Index].Cells[7].Value.ToString();
                        txtbillqty.Text = HFG9.Rows[Index].Cells[7].Value.ToString();
                        txtcolor.Focus();
                        panel4.Visible = false;
                    }
                }
                else if (e.KeyCode == Keys.Escape)
                {
                    panel4.Visible = false;
                }
                else if (e.KeyValue == 40)
                {
                    HFG9.Select();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void button22_Click(object sender, EventArgs e)
        {
            GeneralParametrs.MenyKey = 7;
            FrmParty contc = new FrmParty();
            contc.Show();
            contc.FormBorderStyle = FormBorderStyle.None;
            contc.StartPosition = FormStartPosition.Manual;
            contc.Location = new System.Drawing.Point(200, 80); 

        }

        private void butcan_Click(object sender, EventArgs e)
        {
            conn.Close();
            conn.Open();



            int i = HFGP.SelectedCells[0].RowIndex;

            Genclass.strsql = "SP_CHECKFABIRCISSUE  " + HFGP.Rows[i].Cells[0].Value.ToString() + "";
            Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
            SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
            DataTable tap1 = new DataTable();
            aptr1.Fill(tap1);
            if (tap1.Rows.Count == 0)
            {



                qur.CommandText = " EXEC SP_CHECKDELETEFABIRCISSUE  " + HFGP.Rows[i].Cells[0].Value.ToString() + "";
                qur.ExecuteNonQuery();


            }
            else

            {
                MessageBox.Show("Already Converted GRN");
                return;
            }

         
            LoadGetJobCard(1);
        }

        private void RQGR_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

            
        }

        private void HFIT_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            
        }

        private void RQGR_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (RQGR.CurrentRow.Cells[4].Value != null)
            {
                if (RQGR.CurrentCell.ColumnIndex == 4)

                {
                    DataGridViewCell cell = RQGR.CurrentRow.Cells[4];
                    RQGR.CurrentCell = cell;
                    RQGR.BeginEdit(true);
                }
            }
            if (RQGR.CurrentRow.Cells[17].Value != null)
            {
                if (RQGR.CurrentCell.ColumnIndex == 17)

                {
                    DataGridViewCell cell = RQGR.CurrentRow.Cells[17];
                    RQGR.CurrentCell = cell;
                    RQGR.BeginEdit(true);
                }
            }
        }

        private void txtdcqty_TextChanged_1(object sender, EventArgs e)
        {
            txtdcqty_TextChanged(sender, e);
        }

        private void txtdcqty_Click_1(object sender, EventArgs e)
        {
            txtdcqty_Click(sender, e);
        }

        private void txtdcqty_KeyDown_1(object sender, KeyEventArgs e)
        {
            txtdcqty_KeyDown(sender, e);
        }

        private void button11_Click_1(object sender, EventArgs e)
        {
            button11_Click(sender, e);
        }

        private void button21_Click_1(object sender, EventArgs e)
        {
            button21_Click(sender, e);
        }

        private void button19_Click_1(object sender, EventArgs e)
        {
            button19_Click(sender, e);
        }

        private void cbouom_SelectedIndexChanged_1(object sender, EventArgs e)
        {

        }

        private void cbotype_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void txtuom_TextChanged(object sender, EventArgs e)
        {

        }

        private void HFIT_CellContentClick_1(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void HFIT_CellClick_1(object sender, DataGridViewCellEventArgs e)
        {
            if (HFIT.CurrentRow.Cells[6].Value != null)
            {
                if (HFIT.CurrentCell.ColumnIndex == 6)

                {
                    DataGridViewCell cell = HFIT.CurrentRow.Cells[6];
                    HFIT.CurrentCell = cell;
                    HFIT.BeginEdit(true);
                }
            }

            if (HFIT.CurrentRow.Cells[7].Value != null)
            {
                if (HFIT.CurrentCell.ColumnIndex == 7)

                {
                    DataGridViewCell cell = HFIT.CurrentRow.Cells[7];
                    HFIT.CurrentCell = cell;
                    HFIT.BeginEdit(true);
                }
            }
            if (HFIT.CurrentRow.Cells[17].Value != null)
            {
                if (HFIT.CurrentCell.ColumnIndex == 17)

                {
                    DataGridViewCell cell = HFIT.CurrentRow.Cells[17];
                    HFIT.CurrentCell = cell;
                    HFIT.BeginEdit(true);
                }
            }
            if (HFIT.CurrentRow.Cells[8].Value != null)
            {
                if (HFIT.CurrentCell.ColumnIndex == 8)

                {
                    DataGridViewCell cell = HFIT.CurrentRow.Cells[8];
                    HFIT.CurrentCell = cell;
                    HFIT.BeginEdit(true);
                }
            }
        }

        private void DataGridCommon_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void HFG9_DoubleClick_1(object sender, EventArgs e)
        {
            HFG9_DoubleClick(sender, e);
        }

        private void HFG9_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void txtscr1_TextChanged(object sender, EventArgs e)
        {
           
            bs.Filter = string.Format("Docno Like '%{0}%'  or  Name Like '%{1}%' or  vehicleno Like '%{1}%' or  ProcessName Like '%{1}%' or  Reference Like '%{1}%'  ", txtscr1.Text, txtscr1.Text, txtscr1.Text, txtscr1.Text, txtscr1.Text);

        }

        private void textBox1_TextChanged_1(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bsParty.Filter = string.Format("itemname LIKE '%{0}%' ", textBox1.Text);



                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void cboprocess_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            cboprocess_SelectedIndexChanged(sender, e);
        }

        private void grSearch_Paint(object sender, PaintEventArgs e)
        {

        }

        private void cbopro1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbopro1.ValueMember != "" )
            {
                if (cbopro1.Text != "" &&  txtwor.Text !="" && txtsocno.Text != "")
                {
                    if (Genclass.Dtype == 210)
                    {
                        Genclass.strsql1 = "SP_GETJOIDCITEMS1  '" + txtwor.Text + "', '" + txtsocno.Text + "' ,'" + cbopro1.Text + "'";
                        Genclass.cmd = new SqlCommand(Genclass.strsql1, conn);
                        SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
                        DataTable tap1 = new DataTable();
                        aptr1.Fill(tap1);

                        bsParty.DataSource = tap1;


                        HFGP1.DataSource = null;
                        HFGP1.AutoGenerateColumns = false;
                        HFGP1.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
                        HFGP1.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
                        HFGP1.ColumnCount = 8;
                        HFGP1.Columns[0].Name = "itemid";
                        HFGP1.Columns[0].HeaderText = "itemid";
                        HFGP1.Columns[0].DataPropertyName = "itemid";
                        HFGP1.Columns[1].Name = "itemname";
                        HFGP1.Columns[1].HeaderText = "Itemname";
                        HFGP1.Columns[1].DataPropertyName = "itemname";
                        HFGP1.Columns[1].Width = 650;
                        HFGP1.Columns[2].Name = "Stockqty";
                        HFGP1.Columns[2].HeaderText = "Stockqty";
                        HFGP1.Columns[2].DataPropertyName = "Stockqty";
                        HFGP1.Columns[2].Width = 100;
                        HFGP1.Columns[3].Name = "GENERALNAME";
                        HFGP1.Columns[3].HeaderText = "GENERALNAME";
                        HFGP1.Columns[3].DataPropertyName = "GENERALNAME";


                        HFGP1.Columns[4].Name = "UID";
                        HFGP1.Columns[4].HeaderText = "UID";
                        HFGP1.Columns[4].DataPropertyName = "UID";

                        HFGP1.Columns[4].Visible = false;
                        HFGP1.Columns[5].Name = "REMARKS";
                        HFGP1.Columns[5].HeaderText = "REMARKS";
                        HFGP1.Columns[5].DataPropertyName = "REMARKS";
                        HFGP1.Columns[5].Visible = false;
                        HFGP1.Columns[6].Name = "DOCNO";
                        HFGP1.Columns[6].HeaderText = "DOCNO";
                        HFGP1.Columns[6].DataPropertyName = "DOCNO";
                        HFGP1.Columns[7].Name = "grnno";
                        HFGP1.Columns[7].HeaderText = "grnno";
                        HFGP1.Columns[7].DataPropertyName = "grnno";
                        HFGP1.DataSource = bsParty;
                        HFGP1.Columns[7].Visible = false;
                        HFGP1.Columns[6].Visible = false;
                        HFGP1.Columns[0].Visible = false;
                        HFGP1.Columns[1].Width = 650;
                        HFGP1.Columns[2].Width = 120;
                        HFGP1.Columns[3].Visible = false;
                        HFGP1.Columns[4].Visible = false;
                        HFGP1.Columns[5].Visible = false;
                        HFGP1.Columns[6].Width = 100;

                    }
                    else

                    {
                        Genclass.strsql = "SELECT distinct seqno FROM PROCESSDET A INNER JOIN  PROCESSDETLIST B ON A.UID=B.HEADID  inner join   jjissuetemp hh on b.itemid=hh.itemuid    INNER JOIN  PROCESSM C ON B.PROCESSID=C.UID     where  c.processname='" + cbopro1.Text + "'  and a.docno ='" + txtwor.Text + "' and a.socno='" + txtsocno.Text + "' ";
                        Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                        SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
                        DataTable tap = new DataTable();
                        aptr.Fill(tap);
                        if (tap.Rows.Count > 0)
                        {
                            Genclass.Prtid = Convert.ToInt32(tap.Rows[0]["SEQNO"].ToString());
                          
                        }

                        Genclass.strsql1 = "SP_GETJOIDCITEMS2  '" + txtwor.Text + "', '" + txtsocno.Text + "' ,'" + cbopro1.Text + "','" + Genclass.Prtid + "'," + txtpuid.Text + " ";
                        Genclass.cmd = new SqlCommand(Genclass.strsql1, conn);
                        SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
                        DataTable tap1 = new DataTable();
                        aptr1.Fill(tap1);

                        bsParty.DataSource = tap1;

                 
                        HFGP1.DataSource = null;
                        HFGP1.AutoGenerateColumns = false;
                        HFGP1.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
                        HFGP1.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
                        HFGP1.ColumnCount = 8;
                        HFGP1.Columns[0].Name = "itemid";
                        HFGP1.Columns[0].HeaderText = "itemid";
                        HFGP1.Columns[0].DataPropertyName = "itemid";
                        HFGP1.Columns[1].Name = "itemname";
                        HFGP1.Columns[1].HeaderText = "Itemname";
                        HFGP1.Columns[1].DataPropertyName = "itemname";
                        HFGP1.Columns[1].Width = 650;
                        HFGP1.Columns[2].Name = "Stockqty";
                        HFGP1.Columns[2].HeaderText = "Stockqty";
                        HFGP1.Columns[2].DataPropertyName = "Stockqty";
                        HFGP1.Columns[2].Width = 100;
                        HFGP1.Columns[3].Name = "GENERALNAME";
                        HFGP1.Columns[3].HeaderText = "GENERALNAME";
                        HFGP1.Columns[3].DataPropertyName = "GENERALNAME";


                        HFGP1.Columns[4].Name = "UID";
                        HFGP1.Columns[4].HeaderText = "UID";
                        HFGP1.Columns[4].DataPropertyName = "UID";

                        HFGP1.Columns[4].Visible = false;
                        HFGP1.Columns[5].Name = "REMARKS";
                        HFGP1.Columns[5].HeaderText = "REMARKS";
                        HFGP1.Columns[5].DataPropertyName = "REMARKS";
                        HFGP1.Columns[5].Visible = false;
                        HFGP1.Columns[6].Name = "DOCNO";
                        HFGP1.Columns[6].HeaderText = "DOCNO";
                        HFGP1.Columns[6].DataPropertyName = "DOCNO";
                        HFGP1.Columns[7].Name = "grnno";
                        HFGP1.Columns[7].HeaderText = "grnno";
                        HFGP1.Columns[7].DataPropertyName = "grnno";
                        HFGP1.DataSource = bsParty;
                        HFGP1.Columns[7].Visible = false;
                        HFGP1.Columns[6].Visible = false;
                        HFGP1.Columns[0].Visible = false;
                        HFGP1.Columns[1].Width = 650;
                        HFGP1.Columns[2].Width = 120;
                        HFGP1.Columns[3].Visible = false;
                        HFGP1.Columns[4].Visible = false;
                        HFGP1.Columns[5].Visible = false;
                        HFGP1.Columns[6].Width = 100;


                    }
                }
            }
        }

        private void cbowo_SelectedIndexChanged(object sender, EventArgs e)
        {
            
                
            }

        private void txtsocno_TextChanged(object sender, EventArgs e)
        {

            try
            {
                if (SelectId == 0)
                {
                    bsc1.Filter = string.Format("docno LIKE '%{0}%' ", txtvehicle.Text);



                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void loadproces()
        {

            if (this.Text == "Fabric Process Deliveries")
            {
                string qur = "exec  SP_GETJOIDCITEMSworkordernow '" + txtpgmsocno.Text + "'";
                SqlCommand cmd = new SqlCommand(qur, conn);
                SqlDataAdapter apt = new SqlDataAdapter(cmd);
                DataTable tab = new DataTable();
                apt.Fill(tab);
                if (tab.Rows.Count > 0)
                {
                    txtwor.Text = tab.Rows[0]["docno"].ToString();

                    string qur1 = "exec  SP_GETJOIDCITEMSPROCESS '" + txtpgmwork.Text + "','" + txtpgmsocno.Text + "'";
                    SqlCommand cmd1 = new SqlCommand(qur1, conn);
                    SqlDataAdapter apt1 = new SqlDataAdapter(cmd1);
                    DataTable tab1 = new DataTable();
                    apt1.Fill(tab1);
                    cbopro1.DataSource = null;
                    cbopro1.DataSource = tab1;
                    cbopro1.DisplayMember = "process";
                    cbopro1.ValueMember = "processid";
                    cbopro1.SelectedIndex = -1;
                    cboprocess.DataSource = null;
                    cboprocess.DataSource = tab1;
                    cboprocess.DisplayMember = "process";
                    cboprocess.ValueMember = "processid";
                    cboprocess.SelectedIndex = -1;

                }
            }
            else    if (this.Text == "Yarn Process Delivery")

            {

                string qur = "exec  SP_GETJOIDCITEMSworkordernowyarn '" + txtsocno.Text + "'";
                SqlCommand cmd = new SqlCommand(qur, conn);
                SqlDataAdapter apt = new SqlDataAdapter(cmd);
                DataTable tab = new DataTable();
                apt.Fill(tab);
                if (tab.Rows.Count > 0)
                {
                    txtwor.Text = tab.Rows[0]["docno"].ToString();

                    string qur1 = "exec  SP_GETJOIDCITEMSPROCESSyarn '" + txtwor.Text + "','" + txtsocno.Text + "'";
                    SqlCommand cmd1 = new SqlCommand(qur1, conn);
                    SqlDataAdapter apt1 = new SqlDataAdapter(cmd1);
                    DataTable tab1 = new DataTable();
                    apt1.Fill(tab1);
                    cbopro1.DataSource = null;
                    cbopro1.DataSource = tab1;
                    cbopro1.DisplayMember = "process";
                    cbopro1.ValueMember = "processid";
                    cbopro1.SelectedIndex = -1;
                    cboprocess.DataSource = null;
                    cboprocess.DataSource = tab1;
                    cboprocess.DisplayMember = "process";
                    cboprocess.ValueMember = "processid";
                    cboprocess.SelectedIndex = -1;

                }

            }
        }
        private void loadpgmitems()
        {

            if (this.Text == "Fabric Process Deliveries")
            {
                string qur = "exec  SP_GETJOIDCITEMSworkordernow '" + txtpgmsocno.Text + "'";
                SqlCommand cmd = new SqlCommand(qur, conn);
                SqlDataAdapter apt = new SqlDataAdapter(cmd);
                DataTable tab = new DataTable();
                apt.Fill(tab);
                if (tab.Rows.Count > 0)
                {
                    txtwor.Text = tab.Rows[0]["docno"].ToString();

                    string qur1 = "exec  SP_GETJOIDCITEMSPROCESS '" + txtpgmwork.Text + "','" + txtpgmsocno.Text + "'";
                    SqlCommand cmd1 = new SqlCommand(qur1, conn);
                    SqlDataAdapter apt1 = new SqlDataAdapter(cmd1);
                    DataTable tab1 = new DataTable();
                    apt1.Fill(tab1);
                    cbopro1.DataSource = null;
                    cbopro1.DataSource = tab1;
                    cbopro1.DisplayMember = "process";
                    cbopro1.ValueMember = "processid";
                    cbopro1.SelectedIndex = -1;

                }
            }
            else if (this.Text == "Yarn Process Delivery")

            {

                string qur = "exec  SP_GETJOIDCITEMSworkordernowyarn '" + txtpgmsocno.Text + "'";
                SqlCommand cmd = new SqlCommand(qur, conn);
                SqlDataAdapter apt = new SqlDataAdapter(cmd);
                DataTable tab = new DataTable();
                apt.Fill(tab);
                if (tab.Rows.Count > 0)
                {
                    txtwor.Text = tab.Rows[0]["docno"].ToString();

                    string qur1 = "exec  SP_GETJOIDCITEMSPROCESSyarn '" + txtpgmwork.Text + "','" + txtpgmsocno.Text + "'";
                    SqlCommand cmd1 = new SqlCommand(qur1, conn);
                    SqlDataAdapter apt1 = new SqlDataAdapter(cmd1);
                    DataTable tab1 = new DataTable();
                    apt1.Fill(tab1);
                    cbopro1.DataSource = null;
                    cbopro1.DataSource = tab1;
                    cbopro1.DisplayMember = "process";
                    cbopro1.ValueMember = "processid";
                    cbopro1.SelectedIndex = -1;

                }

            }
        }
        private void txtsocno_Click(object sender, EventArgs e)
        {
            Genclass.type = 7;
            button22.Visible = false;
            DataTable dt = getParty();
            bsc1.DataSource = dt;
            FillGrid3(dt, 1);
            Point loc = FindLocation(txtsocno);
            grSearch.Location = new Point(loc.X, loc.Y + 10);
            grSearch.Visible = true;
            grSearch.Text = "Name Search";
        }

        private void HFGP_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void button24_Click(object sender, EventArgs e)
        {
            conn.Close();
            conn.Open();
            Genclass.Prtid = Convert.ToInt16(HFGP.CurrentRow.Cells[0].Value.ToString());
            string qur = "   select distinct B.UID,C.PROCESSNAME AS PROCESS from jo a inner join JOISSUEPGMITEMS b on a.uid=b.headid     INNER JOIN PROCESSM C ON B.PROCESSID=C.UID  where a.uid=" + Genclass.Prtid + "   ORDER BY  B.UID";
            SqlCommand cmd = new SqlCommand(qur, conn);
            SqlDataAdapter apt = new SqlDataAdapter(cmd);
            DataTable tab = new DataTable();
            apt.Fill(tab);
            Genclass.name = "";

            for (int k = 0; k < tab.Rows.Count; k++)
            {
                if (k == 0)
                {
                    Genclass.name = tab.Rows[k]["process"].ToString();

                }
                else
                {

                    Genclass.name = Genclass.name + "/" + tab.Rows[k]["process"].ToString();
                }


            }

            Genclass.slno = 1;
            Crviewer crv = new Crviewer();
            crv.Show();



            conn.Close();
        }

        private void txtpgmsocno_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bsc1.Filter = string.Format("docno LIKE '%{0}%' ", txtpgmsocno.Text);



                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtpgmwork_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtwor_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtpgmsocno_KeyDown(object sender, KeyEventArgs e)
        {
            Genclass.type = 8;
            button22.Visible = false;
            DataTable dt = getParty();
            bsc1.DataSource = dt;
            FillGrid3(dt, 1);
            Point loc = FindLocation(txtpgmsocno);
            grSearch.Location = new Point(loc.X, loc.Y + 10);
            grSearch.Visible = true;
            grSearch.Text = "Name Search";
        }

       
    }
    }

