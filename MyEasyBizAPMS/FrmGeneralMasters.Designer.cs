﻿namespace MyEasyBizAPMS
{
    partial class FrmGeneralMasters
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmGeneralMasters));
            this.GrBack = new System.Windows.Forms.GroupBox();
            this.lblCompulsary = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblPer = new System.Windows.Forms.Label();
            this.txtTaxPercentage = new System.Windows.Forms.TextBox();
            this.lbltaxPer = new System.Windows.Forms.Label();
            this.lblName = new System.Windows.Forms.Label();
            this.CmbGeneral = new System.Windows.Forms.ComboBox();
            this.ChckActive = new System.Windows.Forms.CheckBox();
            this.lblShortName = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtShortName = new System.Windows.Forms.TextBox();
            this.txtName = new System.Windows.Forms.TextBox();
            this.panadd = new System.Windows.Forms.Panel();
            this.chckAc = new System.Windows.Forms.CheckBox();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnEdit = new System.Windows.Forms.Button();
            this.BtnDelete = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnAddCancel = new System.Windows.Forms.Button();
            this.GrFront = new System.Windows.Forms.GroupBox();
            this.TxtSearch = new System.Windows.Forms.TextBox();
            this.DataGridGeneral = new System.Windows.Forms.DataGridView();
            this.GrBack.SuspendLayout();
            this.panadd.SuspendLayout();
            this.GrFront.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridGeneral)).BeginInit();
            this.SuspendLayout();
            // 
            // GrBack
            // 
            this.GrBack.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.GrBack.Controls.Add(this.lblCompulsary);
            this.GrBack.Controls.Add(this.label4);
            this.GrBack.Controls.Add(this.label3);
            this.GrBack.Controls.Add(this.lblPer);
            this.GrBack.Controls.Add(this.txtTaxPercentage);
            this.GrBack.Controls.Add(this.lbltaxPer);
            this.GrBack.Controls.Add(this.lblName);
            this.GrBack.Controls.Add(this.CmbGeneral);
            this.GrBack.Controls.Add(this.ChckActive);
            this.GrBack.Controls.Add(this.lblShortName);
            this.GrBack.Controls.Add(this.label1);
            this.GrBack.Controls.Add(this.txtShortName);
            this.GrBack.Controls.Add(this.txtName);
            this.GrBack.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GrBack.Location = new System.Drawing.Point(11, 4);
            this.GrBack.Name = "GrBack";
            this.GrBack.Size = new System.Drawing.Size(572, 409);
            this.GrBack.TabIndex = 0;
            this.GrBack.TabStop = false;
            // 
            // lblCompulsary
            // 
            this.lblCompulsary.AutoSize = true;
            this.lblCompulsary.ForeColor = System.Drawing.Color.Red;
            this.lblCompulsary.Location = new System.Drawing.Point(487, 102);
            this.lblCompulsary.Name = "lblCompulsary";
            this.lblCompulsary.Size = new System.Drawing.Size(15, 18);
            this.lblCompulsary.TabIndex = 12;
            this.lblCompulsary.Text = "*";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(491, 193);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(15, 18);
            this.label4.TabIndex = 11;
            this.label4.Text = "*";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(491, 150);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(15, 18);
            this.label3.TabIndex = 10;
            this.label3.Text = "*";
            // 
            // lblPer
            // 
            this.lblPer.AutoSize = true;
            this.lblPer.Location = new System.Drawing.Point(249, 233);
            this.lblPer.Name = "lblPer";
            this.lblPer.Size = new System.Drawing.Size(19, 18);
            this.lblPer.TabIndex = 9;
            this.lblPer.Text = "%";
            this.lblPer.Visible = false;
            // 
            // txtTaxPercentage
            // 
            this.txtTaxPercentage.Location = new System.Drawing.Point(181, 229);
            this.txtTaxPercentage.Name = "txtTaxPercentage";
            this.txtTaxPercentage.Size = new System.Drawing.Size(65, 26);
            this.txtTaxPercentage.TabIndex = 7;
            this.txtTaxPercentage.Text = "0";
            this.txtTaxPercentage.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTaxPercentage.Visible = false;
            // 
            // lbltaxPer
            // 
            this.lbltaxPer.AutoSize = true;
            this.lbltaxPer.Location = new System.Drawing.Point(76, 232);
            this.lbltaxPer.Name = "lbltaxPer";
            this.lbltaxPer.Size = new System.Drawing.Size(101, 18);
            this.lbltaxPer.TabIndex = 8;
            this.lbltaxPer.Text = "Tax Percentage";
            this.lbltaxPer.Visible = false;
            // 
            // lblName
            // 
            this.lblName.Location = new System.Drawing.Point(57, 102);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(120, 18);
            this.lblName.TabIndex = 6;
            this.lblName.Text = "Item Classification";
            this.lblName.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblName.Visible = false;
            // 
            // CmbGeneral
            // 
            this.CmbGeneral.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CmbGeneral.FormattingEnabled = true;
            this.CmbGeneral.Location = new System.Drawing.Point(181, 99);
            this.CmbGeneral.Name = "CmbGeneral";
            this.CmbGeneral.Size = new System.Drawing.Size(306, 26);
            this.CmbGeneral.TabIndex = 5;
            this.CmbGeneral.Visible = false;
            // 
            // ChckActive
            // 
            this.ChckActive.AutoSize = true;
            this.ChckActive.Location = new System.Drawing.Point(181, 263);
            this.ChckActive.Name = "ChckActive";
            this.ChckActive.Size = new System.Drawing.Size(65, 22);
            this.ChckActive.TabIndex = 4;
            this.ChckActive.Text = "Active";
            this.ChckActive.UseVisualStyleBackColor = true;
            // 
            // lblShortName
            // 
            this.lblShortName.AutoSize = true;
            this.lblShortName.Location = new System.Drawing.Point(96, 190);
            this.lblShortName.Name = "lblShortName";
            this.lblShortName.Size = new System.Drawing.Size(81, 18);
            this.lblShortName.TabIndex = 3;
            this.lblShortName.Text = "Short Name";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(132, 149);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(45, 18);
            this.label1.TabIndex = 2;
            this.label1.Text = "Name";
            // 
            // txtShortName
            // 
            this.txtShortName.Location = new System.Drawing.Point(181, 187);
            this.txtShortName.Name = "txtShortName";
            this.txtShortName.Size = new System.Drawing.Size(306, 26);
            this.txtShortName.TabIndex = 1;
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(181, 145);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(306, 26);
            this.txtName.TabIndex = 0;
            // 
            // panadd
            // 
            this.panadd.BackColor = System.Drawing.Color.White;
            this.panadd.Controls.Add(this.chckAc);
            this.panadd.Controls.Add(this.btnAdd);
            this.panadd.Controls.Add(this.btnEdit);
            this.panadd.Controls.Add(this.BtnDelete);
            this.panadd.Controls.Add(this.btnExit);
            this.panadd.Controls.Add(this.btnSave);
            this.panadd.Controls.Add(this.btnAddCancel);
            this.panadd.Location = new System.Drawing.Point(9, 419);
            this.panadd.Name = "panadd";
            this.panadd.Size = new System.Drawing.Size(577, 36);
            this.panadd.TabIndex = 240;
            this.panadd.Paint += new System.Windows.Forms.PaintEventHandler(this.panadd_Paint);
            // 
            // chckAc
            // 
            this.chckAc.AutoSize = true;
            this.chckAc.Checked = true;
            this.chckAc.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chckAc.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chckAc.Location = new System.Drawing.Point(8, 8);
            this.chckAc.Name = "chckAc";
            this.chckAc.Size = new System.Drawing.Size(65, 22);
            this.chckAc.TabIndex = 16;
            this.chckAc.Text = "Active";
            this.chckAc.UseVisualStyleBackColor = true;
            // 
            // btnAdd
            // 
            this.btnAdd.BackColor = System.Drawing.Color.White;
            this.btnAdd.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnAdd.FlatAppearance.BorderSize = 0;
            this.btnAdd.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAdd.Image = ((System.Drawing.Image)(resources.GetObject("btnAdd.Image")));
            this.btnAdd.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAdd.Location = new System.Drawing.Point(294, 3);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(88, 31);
            this.btnAdd.TabIndex = 184;
            this.btnAdd.Text = "Add new";
            this.btnAdd.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAdd.UseVisualStyleBackColor = false;
            this.btnAdd.Click += new System.EventHandler(this.BtnAdd_Click);
            // 
            // btnEdit
            // 
            this.btnEdit.BackColor = System.Drawing.Color.White;
            this.btnEdit.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEdit.Image = ((System.Drawing.Image)(resources.GetObject("btnEdit.Image")));
            this.btnEdit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnEdit.Location = new System.Drawing.Point(382, 3);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(60, 31);
            this.btnEdit.TabIndex = 185;
            this.btnEdit.Text = "Edit";
            this.btnEdit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnEdit.UseVisualStyleBackColor = false;
            this.btnEdit.Click += new System.EventHandler(this.BtnEdit_Click);
            // 
            // BtnDelete
            // 
            this.BtnDelete.BackColor = System.Drawing.Color.White;
            this.BtnDelete.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Image = global::MyEasyBizAPMS.Properties.Resources.cancel;
            this.BtnDelete.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.BtnDelete.Location = new System.Drawing.Point(441, 3);
            this.BtnDelete.Name = "BtnDelete";
            this.BtnDelete.Size = new System.Drawing.Size(77, 31);
            this.BtnDelete.TabIndex = 209;
            this.BtnDelete.Text = "Delete";
            this.BtnDelete.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.BtnDelete.UseVisualStyleBackColor = false;
            this.BtnDelete.Click += new System.EventHandler(this.BtnDelete_Click);
            // 
            // btnExit
            // 
            this.btnExit.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnExit.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.Image = ((System.Drawing.Image)(resources.GetObject("btnExit.Image")));
            this.btnExit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExit.Location = new System.Drawing.Point(517, 3);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(57, 31);
            this.btnExit.TabIndex = 208;
            this.btnExit.Text = "Exit";
            this.btnExit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnExit.UseVisualStyleBackColor = false;
            this.btnExit.Click += new System.EventHandler(this.BtnExit_Click);
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnSave.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.Image = ((System.Drawing.Image)(resources.GetObject("btnSave.Image")));
            this.btnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSave.Location = new System.Drawing.Point(425, 3);
            this.btnSave.Margin = new System.Windows.Forms.Padding(4);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(81, 31);
            this.btnSave.TabIndex = 8;
            this.btnSave.Text = "Save";
            this.btnSave.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.BtnSave_Click);
            // 
            // btnAddCancel
            // 
            this.btnAddCancel.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnAddCancel.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddCancel.Image = ((System.Drawing.Image)(resources.GetObject("btnAddCancel.Image")));
            this.btnAddCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAddCancel.Location = new System.Drawing.Point(503, 3);
            this.btnAddCancel.Margin = new System.Windows.Forms.Padding(4);
            this.btnAddCancel.Name = "btnAddCancel";
            this.btnAddCancel.Size = new System.Drawing.Size(60, 31);
            this.btnAddCancel.TabIndex = 9;
            this.btnAddCancel.Text = "Back";
            this.btnAddCancel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAddCancel.UseVisualStyleBackColor = false;
            this.btnAddCancel.Click += new System.EventHandler(this.BtnAddCancel_Click);
            // 
            // GrFront
            // 
            this.GrFront.Controls.Add(this.TxtSearch);
            this.GrFront.Controls.Add(this.DataGridGeneral);
            this.GrFront.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GrFront.Location = new System.Drawing.Point(11, 4);
            this.GrFront.Name = "GrFront";
            this.GrFront.Size = new System.Drawing.Size(575, 409);
            this.GrFront.TabIndex = 5;
            this.GrFront.TabStop = false;
            // 
            // TxtSearch
            // 
            this.TxtSearch.Location = new System.Drawing.Point(6, 13);
            this.TxtSearch.Name = "TxtSearch";
            this.TxtSearch.Size = new System.Drawing.Size(560, 26);
            this.TxtSearch.TabIndex = 1;
            this.TxtSearch.TextChanged += new System.EventHandler(this.TxtSearch_TextChanged);
            // 
            // DataGridGeneral
            // 
            this.DataGridGeneral.AllowUserToAddRows = false;
            this.DataGridGeneral.BackgroundColor = System.Drawing.Color.White;
            this.DataGridGeneral.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridGeneral.Location = new System.Drawing.Point(6, 41);
            this.DataGridGeneral.Name = "DataGridGeneral";
            this.DataGridGeneral.RowHeadersVisible = false;
            this.DataGridGeneral.Size = new System.Drawing.Size(560, 362);
            this.DataGridGeneral.TabIndex = 0;
            // 
            // FrmGeneralMasters
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(593, 460);
            this.Controls.Add(this.panadd);
            this.Controls.Add(this.GrFront);
            this.Controls.Add(this.GrBack);
            this.Name = "FrmGeneralMasters";
            this.Load += new System.EventHandler(this.FrmGeneralMasters_Load);
            this.GrBack.ResumeLayout(false);
            this.GrBack.PerformLayout();
            this.panadd.ResumeLayout(false);
            this.panadd.PerformLayout();
            this.GrFront.ResumeLayout(false);
            this.GrFront.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridGeneral)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox GrBack;
        private System.Windows.Forms.Panel panadd;
        private System.Windows.Forms.Button btnEdit;
        private System.Windows.Forms.CheckBox chckAc;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Button btnAddCancel;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.TextBox txtShortName;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.CheckBox ChckActive;
        private System.Windows.Forms.Label lblShortName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox GrFront;
        private System.Windows.Forms.TextBox TxtSearch;
        private System.Windows.Forms.DataGridView DataGridGeneral;
        private System.Windows.Forms.Button BtnDelete;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.ComboBox CmbGeneral;
        private System.Windows.Forms.Label lblPer;
        private System.Windows.Forms.TextBox txtTaxPercentage;
        private System.Windows.Forms.Label lbltaxPer;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblCompulsary;
    }
}