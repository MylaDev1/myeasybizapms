﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.Data.SqlClient;


namespace MyEasyBizAPMS
{
    class Genclass
    {
        public static string title = "";
        public static string Str5 = "";
        public static string strfin;
        public static string CompanyName = string.Empty;
        public static string Barcode = string.Empty;
        public static string SortNo = string.Empty;
        public static string ReportType = string.Empty;
        public static DateTime ReortDate = new DateTime();
        public static int RpeortId = 0;
        public static string parameter = string.Empty;
        public static string parameter1 = string.Empty;
        public static Control Parent;
        public static int Dtype = 0;
        public static int type = 0;
        public static int i = 0;
        public static int h = 0;
        public static int k = 0;
        public static string TYPENAME = "";
        public static int pp = 0;
        public static int tt = 0;
        public static int Prtid = 0;
        public static int itemgrpid = 0;
        public static Boolean ckk;
        public static int tuid = 0;
        public static SqlCommand cmd;
        public static string STR = "";
        public static string StrSrch = "";
        public static string StrSrch1 = "";
        public static string StrSrch2 = "";
        public static string FSSQLSortStr = "";
        public static string FSSQLSortStr1 = "";
        public static string FSSQLSortStr2 = "";
        public static string FSSQLSortStr3 = "";
        public static string FSSQLSortStr4 = "";
        public static string FSSQLSortStr5 = "";
        public static string FSSQLSortStr6 = "";
        public static string FSSQLSortStr7 = "";
        public static string FSSQLSortStr8 = "";
        public static string FSSQLSortStr9 = "";
        public static string FSSQLSortStrtmp = "";
        public static string FSSQLSortStrtmp1 = "";
        public static string strsql = "";
        public static string strsql1 = "";
        public static string strsql2 = "";
        public static string strsql3 = "";
        public static string ST = "";
        public static string prfix = "";
        public static int Yearid;
        public static double sum1;
        public static double sum2;
        public static double sum3;
        public static double sum4;
        public static double sum5;
        public static int sum = 0;
        public static double val1;
        public static double val2;
        public static double val3;

        public static double val4;
        public static double val5;
        public static double val6;

        public static double val7;
        public static double val8;
        public static double val9;
        public static double val10;
        public static double val11;
        public static double val12;
        public static string address;
        public static int Qty1;
        public static int ty;
        public static int ty1;
        public static int ty2;
        public static int ty3;
        public static int slno = 0;
        public static int cat = 0;
        public static string name;
        public static DateTime prtfrmdate;
        public static DateTime prttodate;
        public static int itemgrid;
        public static string rrtt;
        public static string err;
        public static DateTime rrtt1;
        public static DateTime err1;
        public static Point FindLocation(Control ctrl)
        {
            if (ctrl.Parent is Form)
                return ctrl.Location;
            else
            {
                Point p = FindLocation(ctrl.Parent);
                p.X += ctrl.Location.X;
                p.Y += ctrl.Location.Y;
                return p;
            }
        }

        public static void buttonstyleform(Form Frmname)
        {
            foreach (Control ct in Frmname.Controls)
            {
                if (ct is Button)
                {
                    ct.TabStop = false;
                    (ct as Button).FlatStyle = FlatStyle.Flat;
                    (ct as Button).FlatAppearance.BorderSize = 0;
                    (ct as Button).FlatAppearance.BorderColor = Color.FromArgb(0, 255, 255, 255);
                }
            }
        }

        public static void buttonstylepanel(Panel whatfldone)
        {
            Parent = whatfldone;
            foreach (Control bt in Parent.Controls)
            {
                if (bt is Button)
                {
                    bt.TabStop = false;
                    (bt as Button).FlatStyle = FlatStyle.Flat;
                    (bt as Button).FlatAppearance.BorderSize = 0;
                    (bt as Button).FlatAppearance.BorderColor = Color.FromArgb(0, 255, 255, 255);
                }
            }
        }
        public static void Gendocno()
        {

            SqlConnection conn = new SqlConnection(GeneralParameters.ConnectionString);

            Genclass.strsql = "select Lastno,prfix from doctypem where doctypeId =" + Genclass.Dtype + "  and finyear='19-20'";
            Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
            SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
            DataTable tap = new DataTable();
            aptr.Fill(tap);
            Genclass.ST = tap.Rows[0]["Lastno"].ToString();
            Genclass.prfix = tap.Rows[0]["prfix"].ToString();
            int EL = 0;
            int a = 0;

            EL = Genclass.ST.Length;
            int b = 5 - EL;

            if (EL < 5)
            {


                for (a = 1; a < b; a++)
                {
                    Genclass.ST = "0" + Genclass.ST;
                }

            }

            Genclass.ST = tap.Rows[0]["prfix"].ToString() + "/" + Genclass.ST;
        }
        public static void ClearTextBox(Form Frmname, Panel whatfldone)
        {
            Genclass.Parent = whatfldone;
            foreach (Control ccontrol in Frmname.Controls)
            {

                if (ccontrol is Panel)
                {

                    foreach (Control c in Genclass.Parent.Controls)
                    {
                        if (c is TextBox || c is RichTextBox)
                        {
                            c.Text = "";
                        }
                    }
                }
            }

        }
        public static void Gendocnolist()
        {

            SqlConnection conn = new SqlConnection(GeneralParameters.ConnectionString);

            Genclass.strsql = "select b.Lastno,b.prfix from doctypem a inner join doctypemlist b on a.doctypeId=b.doctypeId  where a.doctypeId =" + Genclass.Dtype + "  and a.finyear='19-20' and  b.Description='" + Genclass.TYPENAME + "'";
            Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
            SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
            DataTable tap = new DataTable();
            aptr.Fill(tap);
            Genclass.ST = tap.Rows[0]["Lastno"].ToString();
            Genclass.prfix = tap.Rows[0]["prfix"].ToString();
            int EL = 0;
            int a = 0;

            EL = Genclass.ST.Length;
            int b = 5 - EL;

            if (EL < 5)
            {


                for (a = 1; a < b; a++)
                {
                    Genclass.ST = "0" + Genclass.ST;
                }

            }

            Genclass.ST = tap.Rows[0]["prfix"].ToString() + "/" + Genclass.ST;
        }

    }
}
