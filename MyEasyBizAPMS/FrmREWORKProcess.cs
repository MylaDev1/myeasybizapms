﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Drawing.Imaging;
using System.Configuration;
using System.Data.SqlClient;
using System.Diagnostics;
using excel = Microsoft.Office.Interop.Excel;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
namespace MyEasyBizAPMS
{
    public partial class FrmREWORKProcess : Form
    {
        private CrystalDecisions.Windows.Forms.CrystalReportViewer Cryview;

        ReportDocument doc = new ReportDocument();
        public FrmREWORKProcess()
        {
            this.BackColor = Color.White; ;
            InitializeComponent();
        }

        string uid = "";
        int mode = 0;
        string tpuid = "";

        int type = 0;
        DateTime str9;
        double sum1;
        double sum2;
        int SP;
        int Dtype = 0;
        int h = 0;
        int i = 0;
        int kk = 0;
        SqlConnection conn = new SqlConnection(GeneralParameters.ConnectionString);
        SqlCommand qur = new SqlCommand();
        private DataRow doc1;
        DataTable Docno = new DataTable();
        BindingSource bs = new BindingSource();
        BindingSource bsc = new BindingSource();
        BindingSource bsp = new BindingSource();
        BindingSource bsserial = new BindingSource();
        BindingSource bsc1 = new BindingSource();
        BindingSource bsc2 = new BindingSource();

        SQLDBHelper db = new SQLDBHelper();
        int Fillid;
        BindingSource bsParty = new BindingSource();
        string str1key;
        public int SelectId = 0;
        private void FrmREWORKProcess_Load(object sender, EventArgs e)
        {
            qur.Connection = conn;
      
            Genclass.buttonstyleform(this);
            Genclass.buttonstylepanel(panadd);
            panadd.Visible = true;
            Genpan.Visible = true;
            Editpnl.Visible = false;

            this.HFGP.DefaultCellStyle.Font = new Font("calibri", 10);
            this.HFGP.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 10, FontStyle.Bold);
            this.HFIT.DefaultCellStyle.Font = new Font("calibri", 10);
            this.HFIT.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 10, FontStyle.Bold);

            this.HFGP1.DefaultCellStyle.Font = new Font("calibri", 10);
            this.HFGP1.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 10, FontStyle.Bold);

            this.HFGP2.DefaultCellStyle.Font = new Font("calibri", 10);
            this.HFGP2.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 10, FontStyle.Bold);

            HFIT.RowHeadersVisible = false;
            HFGP.RowHeadersVisible = false;
            HFGP2.RowHeadersVisible = false;
            HFGP1.RowHeadersVisible = false;




            HFGP2.EnableHeadersVisualStyles = false;
            HFGP2.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.GradientActiveCaption;

            HFIT.EnableHeadersVisualStyles = false;
            HFIT.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.GradientActiveCaption;
            HFGP1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            HFGP1.EnableHeadersVisualStyles = false;
            HFGP1.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.GradientActiveCaption;

            HFGP.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            HFGP.EnableHeadersVisualStyles = false;
            HFGP.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.GradientActiveCaption;


            if(this.Text== "Knitting Rework Process")
            {
                Genclass.Dtype = 1230;
                label11.Text = "Knitting Rework Process";
            }
            else if (this.Text == "Knitting Rework Receipt")
            {
                Genclass.Dtype = 1240;
                label11.Text = "Knitting Rework Receipt";

            }
            else if (this.Text == "Fabric Rework Process")
            {
                Genclass.Dtype = 1250;
                label11.Text = "Fabric Rework Process";
            }
            else if (this.Text == "Fabric Rework Receipt")
            {
                Genclass.Dtype = 1260;
                label11.Text = "Fabric Rework Receipt";

            }



            dtpfnt.Format = DateTimePickerFormat.Custom;
            dtpfnt.CustomFormat = "dd/MM/yyyy";
            dtpgrndt.Format = DateTimePickerFormat.Custom;
            dtpgrndt.CustomFormat = "dd/MM/yyyy";
            dtpsupp.Format = DateTimePickerFormat.Custom;
            dtpsupp.CustomFormat = "dd/MM/yyyy";
            chkact.Checked = true;


            LoadGetJobCard(1);
            Titlep();
        }
        protected DataTable LoadGetJobCard(int tag)
        {

    
            chkact.Checked = true;
            if (chkact.Checked == true)
            {
                SP = 1;

            }
            else
            {
                SP = 0;

            }


            DataTable dt = new DataTable();
            try
            {

                SqlParameter[] para = {
                    new SqlParameter("@active",SP),
                         new SqlParameter("@doctypeid",Genclass.Dtype),


                };

                dt = db.GetData(CommandType.StoredProcedure, "SP_GETJORLOADWOPROCESS", para);


                LoadDataTable(dt);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        protected void LoadDataTable(DataTable dt)
        {
            try
            {
                HFGP.DataSource = null;
                HFGP.AutoGenerateColumns = false;
                HFGP.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
                HFGP.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
                HFGP.ColumnCount = 12;
                HFGP.Columns[0].Name = "Uid";
                HFGP.Columns[0].HeaderText = "Uid";
                HFGP.Columns[0].DataPropertyName = "Uid";

                HFGP.Columns[1].Name = "Docno";
                HFGP.Columns[1].HeaderText = "Docno";
                HFGP.Columns[1].DataPropertyName = "Docno";

                HFGP.Columns[2].Name = "Docdate";
                HFGP.Columns[2].HeaderText = "Docdate";
                HFGP.Columns[2].DataPropertyName = "Docdate";

                HFGP.Columns[3].Name = "name";
                HFGP.Columns[3].HeaderText = "name";
                HFGP.Columns[3].DataPropertyName = "name";

                HFGP.Columns[4].Name = "Reference";
                HFGP.Columns[4].HeaderText = "Reference";
                HFGP.Columns[4].DataPropertyName = "Reference";

                HFGP.Columns[5].Name = "WorkOrderNo";
                HFGP.Columns[5].HeaderText = "WorkOrderNo";
                HFGP.Columns[5].DataPropertyName = "WorkOrderNo";

                HFGP.Columns[6].Name = "ProcessName";
                HFGP.Columns[6].HeaderText = "ProcessName";
                HFGP.Columns[6].DataPropertyName = "ProcessName";

                HFGP.Columns[7].Name = "supplieruid";
                HFGP.Columns[7].HeaderText = "supplieruid";
                HFGP.Columns[7].DataPropertyName = "supplieruid";

                HFGP.Columns[8].Name = "refid";
                HFGP.Columns[8].HeaderText = "refid";
                HFGP.Columns[8].DataPropertyName = "refid";

                HFGP.Columns[9].Name = "vehicleno";
                HFGP.Columns[9].HeaderText = "vehicleno";
                HFGP.Columns[9].DataPropertyName = "vehicleno";

                HFGP.Columns[10].Name = "mode";
                HFGP.Columns[10].HeaderText = "mode";
                HFGP.Columns[10].DataPropertyName = "mode";

                HFGP.Columns[11].Name = "reason";
                HFGP.Columns[11].HeaderText = "reason";
                HFGP.Columns[11].DataPropertyName = "reason";




                bs.DataSource = dt;

                HFGP.DataSource = bs;


                HFGP.Columns[0].Visible = false;
           
          
                HFGP.Columns[1].Width = 90;
                HFGP.Columns[2].Width = 90;
                HFGP.Columns[3].Width = 450;
    
                HFGP.Columns[4].Width = 150;
                HFGP.Columns[5].Width = 150;
                HFGP.Columns[6].Visible = false;
                HFGP.Columns[7].Visible = false;
                HFGP.Columns[8].Visible = false;
                HFGP.Columns[9].Visible = false;
                HFGP.Columns[10].Visible = false;
                HFGP.Columns[11].Visible = false;
              

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        private void button3_Click(object sender, EventArgs e)
        {
            mode = 1;
            Genpan.Visible = false;

            panadd.Visible = false;
            h = 0;
            Genclass.ClearTextBox(this, Editpnl);
            Genclass.STR = "";
            Genclass.Gendocno();
            txtgrn.Text = Genclass.ST;
            Editpnl.Visible = true;
            lkppnl.Visible = false;
            sum1 = 0;
            sum2 = 0;
            HFIT.Refresh();
            HFIT.DataSource = null;
            HFIT.Rows.Clear();

            HFGP1.Refresh();
            HFGP1.DataSource = null;
            HFGP1.Rows.Clear();

            conn.Close();
            conn.Open();
            Docno.Clear();

            button13.Visible = false;
            if (Docno.Columns.Count == 0)
            {
                Docno.Columns.Add("uid");
                Docno.Columns.Add("Docno");
            }
            conn.Close();
            conn.Open();
            Chkedtact.Checked = true;




            HFIT.Refresh();
            HFIT.DataSource = null;
            HFIT.Rows.Clear();
            HFGP1.Refresh();
            HFGP1.DataSource = null;
            HFGP1.Rows.Clear();

            Titlep();


            dtpgrndt.Focus();
        }
        private void Titlep()
        {
            HFIT.AutoGenerateColumns = false;
            HFIT.Refresh();
            HFIT.DataSource = null;
            HFIT.Rows.Clear();
            HFIT.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            HFIT.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            HFIT.ColumnCount = 16;
            HFIT.Columns[0].Name = "SLNo";
            HFIT.Columns[1].Name = "Itemname";
            HFIT.Columns[2].Name = "BatchNo";
            HFIT.Columns[3].Name = "Plan Dia";
            HFIT.Columns[4].Name = "Actual Dia";
            HFIT.Columns[5].Name = "NoofRolls";
            HFIT.Columns[6].Name = "Uom";
            HFIT.Columns[7].Name = "Qty";
            HFIT.Columns[8].Name = "itemid";
            HFIT.Columns[9].Name = "uom_uid";
            HFIT.Columns[10].Name = "uid";
            HFIT.Columns[11].Name = "refid";
            HFIT.Columns[12].Name = "WORKID";
            HFIT.Columns[13].Name = "Socno";
            HFIT.Columns[14].Name = "Workorder";
            HFIT.Columns[15].Name = "Style";

            //HFIT.EditMode = DataGridViewEditMode.EditOnKeystroke;

            HFIT.Columns[0].Width = 80;

            HFIT.Columns[1].Width = 300;
            HFIT.Columns[2].Width = 70;
            //HFIT.Columns[3].Width = 80;
            //HFIT.Columns[4].Width = 80;
            HFIT.Columns[5].Width = 80;
            HFIT.Columns[6].Width = 80;
            HFIT.Columns[7].Width = 80;
            HFIT.Columns[8].Visible = false;
            HFIT.Columns[9].Visible = false;
            HFIT.Columns[10].Visible = false;
            HFIT.Columns[11].Visible = false;
            HFIT.Columns[12].Visible = false;
            HFIT.Columns[13].Visible = false;
            HFIT.Columns[14].Visible = false;
            HFIT.Columns[3].Visible = false;
            HFIT.Columns[4].Visible = false;
            HFIT.Columns[15].Width = 100;
        }
        private void txtname_Click(object sender, EventArgs e)
        {
            type = 1;
            DataTable dt = getParty();
            bsp.DataSource = dt;
            FillGrid2(dt, 1);
            Point loc = FindLocation(txtname);
            grSearch.Location = new Point(loc.X, loc.Y + 20);
            grSearch.Visible = true;
            grSearch.Text = "Name Search";
        }
        protected void FillGrid2(DataTable dt, int FillId)
        {
            try
            {
                DataGridCommon.DataSource = null;
                DataGridCommon.AutoGenerateColumns = false;


                DataGridCommon.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
                DataGridCommon.DefaultCellStyle.WrapMode = DataGridViewTriState.True;


                DataGridCommon.ColumnCount = 2;
                DataGridCommon.Columns[0].Name = "supplieruid";
                DataGridCommon.Columns[0].HeaderText = "supplieruid";
                DataGridCommon.Columns[0].DataPropertyName = "supplieruid";
                DataGridCommon.Columns[1].Name = "Name";
                DataGridCommon.Columns[1].HeaderText = "Name";
                DataGridCommon.Columns[1].DataPropertyName = "Name";
                DataGridCommon.Columns[1].Width = 350;
                //DataGridCommon.Columns[2].Name = "Dcno";
                //DataGridCommon.Columns[2].HeaderText = "Dcno";
                //DataGridCommon.Columns[2].DataPropertyName = "Dcno";
                //DataGridCommon.Columns[2].Width = 80;
                //DataGridCommon.Columns[3].Name = "uid";
                //DataGridCommon.Columns[3].HeaderText = "uid";
                //DataGridCommon.Columns[3].DataPropertyName = "uid";
                //DataGridCommon.Columns[4].Name = "docno";
                //DataGridCommon.Columns[4].HeaderText = "docno";
                //DataGridCommon.Columns[4].DataPropertyName = "docno";
                //DataGridCommon.Columns[5].Name = "Docdate";
                //DataGridCommon.Columns[5].HeaderText = "Docdate";
                //DataGridCommon.Columns[5].DataPropertyName = "Docdate";
                //DataGridCommon.Columns[5].Width = 90;

                DataGridCommon.DataSource = bsp;
                DataGridCommon.Columns[0].Visible = false;
                //DataGridCommon.Columns[3].Visible = false;
                //DataGridCommon.Columns[4].Visible = false;



            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        private void txtname_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.F10)
                {
                    grSearch.Visible = false;
                }
                else if (e.KeyCode == Keys.F2)
                {
                    SelectId = 1;
                    int Index = DataGridCommon.SelectedCells[0].RowIndex;

                    txtname.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtpuid.Text = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    //txtmillid.Text = DataGridCommon.Rows[Index].Cells[4].Value.ToString();
                    txtmode.Focus();
                    grSearch.Visible = false;
                    SelectId = 0;
                }
                else if (e.KeyCode == Keys.Escape)
                {
                    grSearch.Visible = false;
                }
                else if (e.KeyValue == 40)
                {
                    DataGridCommon.Select();
                }
                else if (e.KeyCode == Keys.Enter)
                {
                    txtname_Click(sender, e);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtname_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bsp.Filter = string.Format("Name LIKE '%{0}%' ", txtname.Text);



                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtdcqty_Click(object sender, EventArgs e)
        {
            lkppnl.Visible = true;
            txtwok.Text = "";
            txtstyle.Text = "";
            txtscono.Text = "";
            txtwok.Focus();
            HFGP2.AutoGenerateColumns = false;
            HFGP2.Refresh();
            HFGP2.DataSource = null;
            HFGP2.Rows.Clear();




        }
        protected void FillGrid(DataTable dt, int FillId)
        {
            try
            {
                DataGridCommon.DataSource = null;
                DataGridCommon.AutoGenerateColumns = false;


                DataGridCommon.ColumnCount = 4;
                DataGridCommon.Columns[0].Name = "uid";
                DataGridCommon.Columns[0].HeaderText = "uid";
                DataGridCommon.Columns[0].DataPropertyName = "uid";
                DataGridCommon.Columns[1].Name = "Itemname";
                DataGridCommon.Columns[1].HeaderText = "Itemname";
                DataGridCommon.Columns[1].DataPropertyName = "Itemname";
                DataGridCommon.Columns[1].Width = 300;
                DataGridCommon.Columns[2].Name = "UOMID";
                DataGridCommon.Columns[2].HeaderText = "UOMID";
                DataGridCommon.Columns[2].DataPropertyName = "UOMID";

                DataGridCommon.Columns[3].Name = "GENERALNAME";
                DataGridCommon.Columns[3].HeaderText = "GENERALNAME";
                DataGridCommon.Columns[3].DataPropertyName = "GENERALNAME";



                DataGridCommon.DataSource = bsParty;
                DataGridCommon.Columns[0].Visible = false;
                DataGridCommon.Columns[2].Visible = false;
                DataGridCommon.Columns[3].Visible = false;



            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private Point FindLocation(Control ctrl)
        {
            if (ctrl.Parent is Form)
                return ctrl.Location;
            else
            {
                Point p = FindLocation(ctrl.Parent);
                p.X += ctrl.Location.X;
                p.Y += ctrl.Location.Y;
                return p;
            }
        }
        protected DataTable getParty()
        {
            DataTable dt = new DataTable();
            try
            {
                if (type == 1 && Text== "Knitting Rework Process")
                {
                    dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "SP_KNITREWORKPROCESSSUPP", conn);
                    bsp.DataSource = dt;
                }
                else if (type == 1 && Text == "Knitting Rework Receipt")
                {
                    dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "SP_KNITREWORKRECEIPTSSUPP", conn);
                    bsp.DataSource = dt;
                }
                else if (type == 1 && Text == "Fabric Rework Process")
                {
                    dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "SP_FABREWORKPROCESSSUPP", conn);
                    bsp.DataSource = dt;
                }
                else if (type == 1 && Text == "Fabric Rework Receipt")
                {
                    dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "SP_FABREWORKRECEIPTSSUPP", conn);
                    bsp.DataSource = dt;
                }


                else if (type == 2 && Text == "Knitting Without Process")
                {
                    dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "sp_getwo", conn);
                    bsc1.DataSource = dt;
                }
                else if (type == 2 && Text == "Fabric Without Process")
                {
                    dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "sp_getwo", conn);
                    bsc1.DataSource = dt;
                }



            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            return dt;
        }
        private void txtdcqty_KeyDown(object sender, KeyEventArgs e)
        {

            try
            {
                if (e.KeyCode == Keys.F10)
                {
                    grSearch.Visible = false;
                }
                else if (e.KeyCode == Keys.F2)
                {
                    SelectId = 1;
                    int Index = DataGridCommon.SelectedCells[0].RowIndex;
                    txtdcqty.Text = HFGP2.Rows[Index].Cells[1].Value.ToString();
                    txtdcqty.Tag = HFGP2.Rows[Index].Cells[0].Value.ToString();
                    txtuom.Text = HFGP2.Rows[Index].Cells[2].Value.ToString();
                    txtitemid.Text = HFGP2.Rows[Index].Cells[3].Value.ToString();
                    txtprice.Text = HFGP2.Rows[Index].Cells[4].Value.ToString();
                    txtprice.Tag = HFGP2.Rows[Index].Cells[5].Value.ToString();
                    txtisstot.Text = HFGP2.Rows[Index].Cells[6].Value.ToString();

                    txtitem.Focus();
                    grSearch.Visible = false;
                    SelectId = 0;
                }
                else if (e.KeyCode == Keys.Escape)
                {
                    grSearch.Visible = false;
                }
                else if (e.KeyValue == 40)
                {
                    DataGridCommon.Select();
                }
                else if (e.KeyCode == Keys.Enter)
                {
                    txtdcqty_Click(sender, e);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtdcqty_TextChanged(object sender, EventArgs e)
        {
            //try
            //{
            //    if (SelectId == 0)
            //    {
            //        bsParty.Filter = string.Format("Itemname LIKE '%{0}%' ", txtdcqty.Text);



            //    }
            //}
            //catch (Exception ex)
            //{
            //    MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //    return;
            //}
        }

        private void txtitem_Click(object sender, EventArgs e)        {
            type = 2;
            DataTable dt = getParty();
            bsc1.DataSource = dt;
            FillGrid3(dt, 1);
            Point loc = FindLocation(txtitem);
            grSearch.Location = new Point(loc.X, loc.Y + 20);
            grSearch.Visible = true;
            grSearch.Text = "Name Search";
        }

        private void txtitem_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.F10)
                {
                    grSearch.Visible = false;
                }
                else if (e.KeyCode == Keys.F2)
                {
                    SelectId = 1;
                    int Index = DataGridCommon.SelectedCells[0].RowIndex;

                    txtitem.Text = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    txtitem.Tag = DataGridCommon.Rows[Index].Cells[1].Value.ToString();

                    txtmode.Focus();
                    lkppnl.Visible = false;
                    SelectId = 0;
                }
                else if (e.KeyCode == Keys.Escape)
                {
                    grSearch.Visible = false;
                }
                else if (e.KeyValue == 40)
                {
                    DataGridCommon.Select();
                }
                else if (e.KeyCode == Keys.Enter)
                {
                    txtitem_Click(sender, e);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
        protected void FillGrid3(DataTable dt, int FillId)
        {
            try
            {
                DataGridCommon.DataSource = null;
                DataGridCommon.AutoGenerateColumns = false;


                DataGridCommon.ColumnCount = 4;
                DataGridCommon.Columns[0].Name = "Uid";
                DataGridCommon.Columns[0].HeaderText = "Uid";
                DataGridCommon.Columns[0].DataPropertyName = "Uid";

                DataGridCommon.Columns[1].Name = "docno";
                DataGridCommon.Columns[1].HeaderText = "Docno";
                DataGridCommon.Columns[1].DataPropertyName = "docno";
                DataGridCommon.Columns[1].Width = 150;


                DataGridCommon.Columns[2].Name = "processname";
                DataGridCommon.Columns[2].HeaderText = "processname";
                DataGridCommon.Columns[2].DataPropertyName = "processname";




                DataGridCommon.Columns[3].Name = "processid";
                DataGridCommon.Columns[3].HeaderText = "processid";
                DataGridCommon.Columns[3].DataPropertyName = "processid";






                DataGridCommon.DataSource = bsc1;
                DataGridCommon.Columns[0].Visible = false;
                DataGridCommon.Columns[2].Visible = false;
                DataGridCommon.Columns[3].Visible = false;



            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        private void txtitem_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bsp.Filter = string.Format("docno LIKE '%{0}%' ", txtvehicle.Text);



                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void button18_Click(object sender, EventArgs e)
        {
            try
            {
                SelectId = 1;
                int Index = DataGridCommon.SelectedCells[0].RowIndex;
                if (type == 1)
                {
                    txtname.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtpuid.Text = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    txtitem.Focus();
                }
                else if (type == 2)
                {
                    txtitem.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtitem.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    txtmode.Focus();
                }
                else if (type == 10)
                {
                    txtvehicle.Text = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    txtdcno.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtwo.Text = DataGridCommon.Rows[Index].Cells[2].Value.ToString();
                    txtwo.Tag = DataGridCommon.Rows[Index].Cells[3].Value.ToString();
                    Genclass.address = DataGridCommon.Rows[Index].Cells[4].Value.ToString();
                   
                    txtitem.Focus();
                }
                else if (Genclass.type == 11)
                {
                    txtscono.Text = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    txtwok.Text = DataGridCommon.Rows[Index].Cells[2].Value.ToString();
                    txtwo.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtscono.Tag = DataGridCommon.Rows[Index].Cells[3].Value.ToString();
                    Genclass.address = DataGridCommon.Rows[Index].Cells[4].Value.ToString();
                    txtstyle.Text= DataGridCommon.Rows[Index].Cells[6].Value.ToString();

                    loadallitems();
                }
                grSearch.Visible = false;
                SelectId = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
        private void loadallitems()

        {

            if (txtname.Text == string.Empty)
            {
                MessageBox.Show("Select Supplier", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtname.Focus();
                return;
            }
            lkppnl.Visible = true;
            txtscr11.Text = "";
            txtscr11.Focus();


            if (Text == "Knitting Rework Process")
            {
                Genclass.strsql = "SP_GETKNITREWORKOROCESITEMS  " + txtpuid.Text + ",'" + txtscono.Text + "' ,'" + txtwok.Text + "','" + txtwo.Text + "' ";
                Genclass.FSSQLSortStr = "Itemdes";
            }
            else if (Text == "Knitting Rework Receipt")

            {
                Genclass.strsql = "SP_GETKNITREWORKRECEIPTITEMS  " + txtpuid.Text + ",'" + txtscono.Text + "' ,'" + txtwok.Text + "','" + txtwo.Text + "' ";
                Genclass.FSSQLSortStr = "Itemdes";
            }
            else if (Text == "Fabric Rework Process")

            {
                Genclass.strsql = "SP_GETFABREWORKOROCESITEMS  " + txtpuid.Text + ",'" + txtscono.Text + "' ,'" + txtwok.Text + "','" + txtwo.Text + "' ";
                Genclass.FSSQLSortStr = "Itemdes";
            }
            else if (Text == "Fabric Rework Receipt")

            {
                Genclass.strsql = "SP_GETFABREWORKRECEIPTITEMS  " + txtpuid.Text + ",'" + txtscono.Text + "' ,'" + txtwok.Text + "','" + txtwo.Text + "' ";
                Genclass.FSSQLSortStr = "Itemdes";
            }


            Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
            SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
            DataTable tap = new DataTable();
            aptr.Fill(tap);

            bsc.DataSource = tap;

            HFGP2.AutoGenerateColumns = false;
            HFGP2.Refresh();
            HFGP2.DataSource = null;
            HFGP2.Rows.Clear();
            HFGP2.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            HFGP2.DefaultCellStyle.WrapMode = DataGridViewTriState.True;


            HFGP2.ColumnCount = tap.Columns.Count;
            i = 0;

            foreach (DataColumn column in tap.Columns)
            {
                HFGP2.Columns[i].Name = column.ColumnName;
                HFGP2.Columns[i].HeaderText = column.ColumnName;
                HFGP2.Columns[i].DataPropertyName = column.ColumnName;
                i = i + 1;
            }
            HFGP2.DataSource = tap;

            HFGP2.Columns[0].Visible = false;
            HFGP2.Columns[2].Width = 100;

            HFGP2.Columns[3].Width = 100;
            HFGP2.Columns[4].Visible = false;
            HFGP2.Columns[5].Visible = false;
            HFGP2.Columns[6].Visible = false;
            HFGP2.Columns[1].Width = 250;
            HFGP2.Columns[7].Visible = false;
            HFGP2.Columns[8].Width = 100;
            HFGP2.Columns[9].Width = 200;
            HFGP2.Columns[10].Visible = false;
            conn.Close();
        }
        private void btnHide_Click(object sender, EventArgs e)
        {
            grSearch.Visible = false;
        }

        private void DataGridCommon_DoubleClick(object sender, EventArgs e)
        {
            SelectId = 1;
            int Index = DataGridCommon.SelectedCells[0].RowIndex;
            if (type == 1)
            {
                txtname.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                txtpuid.Text = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                txtmode.Focus();
            }
            else if (type == 2)
            {
                txtitem.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                txtitem.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                txtmode.Focus();
            }
            else if (type == 10)
            {
                txtdcno.Text = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                txtwo.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                txtwo.Tag = DataGridCommon.Rows[Index].Cells[3].Value.ToString();
                txtitem.Focus();
            }
            else if (type == 11)
            {
                txtscono.Text = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                txtwok.Text = DataGridCommon.Rows[Index].Cells[2].Value.ToString();
                txtwo.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                txtscono.Tag = DataGridCommon.Rows[Index].Cells[3].Value.ToString();
                Genclass.address = DataGridCommon.Rows[Index].Cells[4].Value.ToString();
                txtstyle.Text = DataGridCommon.Rows[Index].Cells[6].Value.ToString();
                loadallitems();
            }
            grSearch.Visible = false;
            SelectId = 0;
        }

        private void DataGridCommon_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                int Index = DataGridCommon.SelectedCells[0].RowIndex;
                if (type == 1)
                {
                    txtpuid.Text = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    txtname.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtmode.Focus();
                }
                else if (type == 2)
                {
                    txtitem.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtitem.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    txtmode.Focus();
                }
                else if (type == 10)
                {
                    txtdcno.Text = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    txtwo.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtwo.Tag = DataGridCommon.Rows[Index].Cells[3].Value.ToString();
                    txtitem.Focus();
                }
                else if (type == 11)
                {
                    txtscono.Text = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    txtwok.Text = DataGridCommon.Rows[Index].Cells[2].Value.ToString();
                    txtwo.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtscono.Tag = DataGridCommon.Rows[Index].Cells[3].Value.ToString();
                    Genclass.address = DataGridCommon.Rows[Index].Cells[4].Value.ToString();
                    txtstyle.Text = DataGridCommon.Rows[Index].Cells[6].Value.ToString();
                    loadallitems();
                }
                grSearch.Visible = false;
                SelectId = 0;
            }
        }

        private void button10_Click(object sender, EventArgs e)
        {

            int Index = HFGP2.SelectedCells[0].RowIndex;
            txtdcqty.Text = HFGP2.Rows[Index].Cells[1].Value.ToString();
            txtdcqty.Tag = HFGP2.Rows[Index].Cells[0].Value.ToString();
            txtuom.Text = HFGP2.Rows[Index].Cells[3].Value.ToString();
            txtoutputid.Text = HFGP2.Rows[Index].Cells[3].Value.ToString();
            txtitemid.Text = HFGP2.Rows[Index].Cells[4].Value.ToString();
            txtprice.Text = HFGP2.Rows[Index].Cells[5].Value.ToString();
            txtprice.Tag = HFGP2.Rows[Index].Cells[6].Value.ToString();
            txtisstot.Text = HFGP2.Rows[Index].Cells[7].Value.ToString();
            txtisstot.Tag = HFGP2.Rows[Index].Cells[10].Value.ToString();
            lkppnl.Visible = false;
            txtaddnotes.Focus();
        }

        private void HFGP2_DoubleClick(object sender, EventArgs e)
        {
            int Index = HFGP2.SelectedCells[0].RowIndex;

            txtdcqty.Text = HFGP2.Rows[Index].Cells[1].Value.ToString();
            txtdcqty.Tag = HFGP2.Rows[Index].Cells[0].Value.ToString();
            txtuom.Text = HFGP2.Rows[Index].Cells[3].Value.ToString();
            txtoutputid.Text = HFGP2.Rows[Index].Cells[3].Value.ToString();
            txtitemid.Text = HFGP2.Rows[Index].Cells[4].Value.ToString();
            txtprice.Text = HFGP2.Rows[Index].Cells[5].Value.ToString();
            txtprice.Tag = HFGP2.Rows[Index].Cells[6].Value.ToString();
            txtisstot.Text = HFGP2.Rows[Index].Cells[7].Value.ToString();
            txtisstot.Tag = HFGP2.Rows[Index].Cells[10].Value.ToString();
            lkppnl.Visible = false;
            txtaddnotes.Focus();
        }

        private void HFGP2_KeyDown(object sender, KeyEventArgs e)
        {

            if (e.KeyCode == Keys.Enter)
            {
                int Index = HFGP2.SelectedCells[0].RowIndex;
                txtdcqty.Text = HFGP2.Rows[Index].Cells[1].Value.ToString();
                txtdcqty.Tag = HFGP2.Rows[Index].Cells[0].Value.ToString();
                txtuom.Text = HFGP2.Rows[Index].Cells[3].Value.ToString();
                txtoutputid.Text = HFGP2.Rows[Index].Cells[3].Value.ToString();
                txtitemid.Text = HFGP2.Rows[Index].Cells[4].Value.ToString();
                txtprice.Text = HFGP2.Rows[Index].Cells[5].Value.ToString();
                txtprice.Tag = HFGP2.Rows[Index].Cells[6].Value.ToString();
                txtisstot.Text = HFGP2.Rows[Index].Cells[7].Value.ToString();
                txtisstot.Tag = HFGP2.Rows[Index].Cells[10].Value.ToString();
                lkppnl.Visible = false;
                txtaddnotes.Focus();
            }
        }

        private void buttnfinbk_Click(object sender, EventArgs e)
        {
            Editpnl.Visible = false;
            Genpan.Visible = true;
            panadd.Visible = true;
            LoadGetJobCard(1);
        }

        private void btnadd_Click(object sender, EventArgs e)
        {
            if (txtdcqty.Text == "")
            {
                MessageBox.Show("Enter the Item");
                txtdcqty.Focus();
                return;
            }
            if (txtaddnotes.Text == "")
            {
                MessageBox.Show("Enter the Batchno");
                txtaddnotes.Focus();
                return;
            }
            if (txtisstot.Text == "")
            {
                MessageBox.Show("Enter the dia");
                txtisstot.Focus();
                return;
            }
            //if (txtbags.Text == "")
            //{
            //    MessageBox.Show("Enter the Actual Dia");
            //    txtbags.Focus();
            //    return;
            //}
            if (textBox1.Text == "")
            {
                MessageBox.Show("Enter the rolls");
                textBox1.Focus();
                return;
            }
            if (txtuom.Text == "")
            {
                MessageBox.Show("Enter the Qty");
                txtuom.Focus();
                return;
            }


            double tt = Convert.ToDouble(txtoutputid.Text);
            double pp = Convert.ToDouble(txtuom.Text);
            double rr = Convert.ToDouble(txtoutputid.Text)*10/100;
            double ll = tt + rr;
            if (tt==pp || tt>pp || ll == pp)
            {


            }
            else if(ll < pp)

            {
                MessageBox.Show("Qty Exceed 10%");
                return;

            }


            kk = HFIT.Rows.Count - 1;
            h = kk;


          
                h = h + 1;
                var index = HFIT.Rows.Add();
                HFIT.Rows[index].Cells[0].Value = h;
                HFIT.Rows[index].Cells[1].Value = txtdcqty.Text;

                HFIT.Rows[index].Cells[2].Value = txtaddnotes.Text;
                HFIT.Rows[index].Cells[3].Value = txtisstot.Text;

                HFIT.Rows[index].Cells[4].Value = txtbags.Text;
                HFIT.Rows[index].Cells[5].Value = textBox1.Text;

                HFIT.Rows[index].Cells[6].Value = txtprice.Text;
                HFIT.Rows[index].Cells[7].Value = txtuom.Text;

                HFIT.Rows[index].Cells[8].Value = txtdcqty.Tag;
                HFIT.Rows[index].Cells[9].Value = txtprice.Tag;
                HFIT.Rows[index].Cells[10].Value = 0;
                HFIT.Rows[index].Cells[11].Value = txtitemid.Text;
                HFIT.Rows[index].Cells[12].Value = txtisstot.Tag;
            HFIT.Rows[index].Cells[13].Value = txtscono.Text;
            HFIT.Rows[index].Cells[14].Value = txtwok.Text;
            HFIT.Rows[index].Cells[15].Value = txtstyle.Text;
            txtdcqty.Text = "";
            txtstyle.Text = "";

            txtaddnotes.Text = "";
            txtbags.Text = "";
            txtuom.Text = "";
            txtisstot.Text = "";
            txtprice.Text = "";
            txtdcqty.Text = "";
            textBox1.Text = "";



            txtdcqty.Focus();
        }

        private void btnsave_Click(object sender, EventArgs e)
        {
            conn.Close();
            conn.Open();
            

    
            if (Chkedtact.Checked == true)
            {

                SP = 1;
            }
            else
            {
                SP = 0;

            }
            if (mode == 1)
            {



                SqlParameter[] para ={

                    new SqlParameter("@DOCNO",txtgrn.Text),
                    new SqlParameter("@DOCDATE",Convert.ToDateTime(dtpgrndt.Text)),
                    new SqlParameter("@REFNO","0"),
                    new SqlParameter("@DCNO",txtqty.Text),
                    new SqlParameter("@DCDATE",Convert.ToDateTime(dtpsupp.Text)),
                    new SqlParameter("@DOCTYPEID", Genclass.Dtype),
                    new SqlParameter("@SUPPLIERUID",txtpuid.Text),
                    new SqlParameter("@PLACEOFSUPPLY", Genclass.address),
                    new SqlParameter("@MODE",txtmode.Text),
                    new SqlParameter("@REMARKS",txtwo.Text),
                    new SqlParameter("@VEHICLENO",txtvehicle.Text),
                    new SqlParameter("@REASON",txtdcno.Text),
                    new SqlParameter("@REFID","0"),
                    new SqlParameter("@YEARID",Genclass.Yearid),
                    new SqlParameter("@ACTIVE", SP)


                };
                db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_JO", para, conn);

                string quy6 = "select * from jo where docno='" + txtgrn.Text + "' and doctypeid=" + Genclass.Dtype + "";
                Genclass.cmd = new SqlCommand(quy6, conn);
                SqlDataAdapter aptr3 = new SqlDataAdapter(Genclass.cmd);
                DataTable tap5 = new DataTable();
                aptr3.Fill(tap5);
                if (tap5.Rows.Count > 0)
                {
                    txtgrnid.Text = tap5.Rows[0]["uid"].ToString();

                }

                for (int i = 0; i < HFIT.RowCount - 1; i++)
                {

                    conn.Close();
                    conn.Open();

                    SqlParameter[] para1 ={


                  new SqlParameter("@slno", HFIT.Rows[i].Cells[0].Value),
                    new SqlParameter("@iTEMNAME",HFIT.Rows[i].Cells[1].Value),
                    new SqlParameter("@ITEMID", HFIT.Rows[i].Cells[8].Value),
                    new SqlParameter("@BATCHNO", HFIT.Rows[i].Cells[2].Value),
                    new SqlParameter("@PLANDIA", HFIT.Rows[i].Cells[3].Value),
                    new SqlParameter("@ACTDIA", HFIT.Rows[i].Cells[4].Value),
                    new SqlParameter("@ROLLS",HFIT.Rows[i].Cells[5].Value),
                    new SqlParameter("@QTY", HFIT.Rows[i].Cells[7].Value),
                    new SqlParameter("@HEADID", txtgrnid.Text),
                    new SqlParameter("@REFID",HFIT.Rows[i].Cells[11].Value),
                        new SqlParameter("@WORKID",HFIT.Rows[i].Cells[12].Value),
                       new SqlParameter("@uom",HFIT.Rows[i].Cells[6].Value),
                         new SqlParameter("@socno",HFIT.Rows[i].Cells[13].Value),
                       new SqlParameter("@workorder",HFIT.Rows[i].Cells[14].Value),
                          new SqlParameter("@style",HFIT.Rows[i].Cells[15].Value),

                };

                    db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_JORKNITREC", para1, conn);
                }

        


            }
            else

            {


                conn.Close();
                conn.Open();

                SqlParameter[] para ={


                    new SqlParameter("@DOCNO",txtgrn.Text),
                    new SqlParameter("@DOCDATE",Convert.ToDateTime(dtpgrndt.Text)),
                    new SqlParameter("@REFNO","0"),
                    new SqlParameter("@DCNO",txtqty.Text),
                    new SqlParameter("@DCDATE",Convert.ToDateTime(dtpsupp.Text)),
                    new SqlParameter("@DOCTYPEID",  Genclass.Dtype),
                    new SqlParameter("@SUPPLIERUID",txtpuid.Text),
                    new SqlParameter("@PLACEOFSUPPLY",Genclass.address),
                    new SqlParameter("@MODE",txtmode.Text),
                    new SqlParameter("@REMARKS",txtwo.Text),
                    new SqlParameter("@VEHICLENO",txtvehicle.Text),
                    new SqlParameter("@REASON",txtdcno.Text),
                    new SqlParameter("@REFID","0"),
                    new SqlParameter("@YEARID",Genclass.Yearid),
                    new SqlParameter("@ACTIVE", SP),
                    new SqlParameter("@UID", txtgrnid.Text)


                };
                db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_JOUPDATE", para, conn);

                conn.Close();
                conn.Open();
                qur.CommandText = "delete from JOKNITtingREC where headid=" + txtgrnid.Text + " ";
                qur.ExecuteNonQuery();

                //qur.CommandText = "delete from dcitemslistmatch where headid=" + txtgrnid.Text + "";
                //qur.ExecuteNonQuery();



                for (int i = 0; i < HFIT.RowCount - 1; i++)
                {

                    conn.Close();
                    conn.Open();

                    SqlParameter[] para1 ={



                    new SqlParameter("@slno", HFIT.Rows[i].Cells[0].Value),
                    new SqlParameter("@iTEMNAME",HFIT.Rows[i].Cells[1].Value),
                    new SqlParameter("@ITEMID", HFIT.Rows[i].Cells[8].Value),
                    new SqlParameter("@BATCHNO", HFIT.Rows[i].Cells[2].Value),
                    new SqlParameter("@PLANDIA", HFIT.Rows[i].Cells[3].Value),
                    new SqlParameter("@ACTDIA", HFIT.Rows[i].Cells[4].Value),
                    new SqlParameter("@ROLLS",HFIT.Rows[i].Cells[5].Value),
                    new SqlParameter("@QTY", HFIT.Rows[i].Cells[7].Value),
                    new SqlParameter("@HEADID", txtgrnid.Text),
                    new SqlParameter("@REFID",HFIT.Rows[i].Cells[11].Value),
                    new SqlParameter("@WORKID",HFIT.Rows[i].Cells[12].Value),
                    new SqlParameter("@uom",HFIT.Rows[i].Cells[6].Value),
                      new SqlParameter("@socno",HFIT.Rows[i].Cells[13].Value),
                       new SqlParameter("@workorder",HFIT.Rows[i].Cells[14].Value),
                        new SqlParameter("@style",HFIT.Rows[i].Cells[15].Value),

                };

                    db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_JORKNITREC", para1, conn);
                }

                //string quy77 = "select distinct a.headid,c.int as dcid,a.uid as recid,isnull(a.qty,0)/isnull(d.qtyper,0) as qty,b.uid as issid from JORKNITREC a inner join jobissuedettab b on a.refid=b.uid and a.itemid=b.itemid inner join dcitems c on b.headid=c.headi  where a.headid=" + txtgrnid.Text + "";
                //Genclass.cmd = new SqlCommand(quy77, conn);
                //SqlDataAdapter aptr77 = new SqlDataAdapter(Genclass.cmd);
                //DataTable tap77 = new DataTable();
                //aptr77.Fill(tap77);
                //if (tap77.Rows.Count > 0)
                //{
                //    for (int i = 0; i < tap77.Rows.Count; i++)
                //    {
                //        conn.Close();
                //        conn.Open();

                //        SqlParameter[] para1 ={

                //    new SqlParameter("@headid",tap77.Rows[i]["headid"].ToString()),
                //    new SqlParameter("@dcid", tap77.Rows[i]["dcid"].ToString()),

                //    new SqlParameter("@recid", tap77.Rows[i]["recid"].ToString()),
                //    new SqlParameter("@balqty",tap77.Rows[i]["qty"].ToString()),
                //    new SqlParameter("@issid", tap77.Rows[i]["issid"].ToString()),



                //};

                //        db.ExecuteNonQuery(CommandType.StoredProcedure, "sp_dcitemslistmatch", para1, conn);

                //    }
                //}







            }
            conn.Close();
            conn.Open();
            if (mode == 1)
            {
                qur.CommandText = "update doctypem set lastno=lastno+1 where doctypeid=" + Genclass.Dtype + " and finyear='19-20'";
                qur.ExecuteNonQuery();

            }
            conn.Close();
            conn.Open();
            if (txtgrnid.Text != "")
            {
                if (mode == 1)
                {

                    if (Genclass.Dtype == 1240 || Genclass.Dtype == 1260)
                    {
                        qur.CommandText = "exec SP_stocklegKNITFABRECEIPTREWORKPROCESS " + txtgrnid.Text + "," + Genclass.Dtype + "   ,1 ,'Receipt'";
                        qur.ExecuteNonQuery();
                    }
                    else if (Genclass.Dtype == 1230 || Genclass.Dtype == 1250)
                    {

                        qur.CommandText = "exec SP_stocklegKNITFABISSUEREWORKPROCESS " + txtgrnid.Text + "," + Genclass.Dtype + "   ,1 ,'Issue'";
                        qur.ExecuteNonQuery();
                    }


                    qur.CommandText = "exec POST_SUPPLIERSTOCK_SP " + txtgrnid.Text + "," + Genclass.Dtype + " ";
                    qur.ExecuteNonQuery();
                }
                else
                {
                    if (Genclass.Dtype == 1240 || Genclass.Dtype == 1260)
                    {
                        qur.CommandText = "exec SP_stocklegKNITFABRECEIPTREWORKPROCESS " + txtgrnid.Text + "," + Genclass.Dtype + "   ,2 ,'Receipt'";
                        qur.ExecuteNonQuery();
                    }
                    else if (Genclass.Dtype == 1230 || Genclass.Dtype == 1250)
                    {

                        qur.CommandText = "exec SP_stocklegKNITFABISSUEREWORKPROCESS " + txtgrnid.Text + "," + Genclass.Dtype + "   ,2 ,'Issue'";
                        qur.ExecuteNonQuery();
                    }
                    qur.CommandText = "delete from SUPPLIER_STOCK_LEDGER where hdid= " + txtgrnid.Text + " and doctypeid=" + Genclass.Dtype + " ";
                    qur.ExecuteNonQuery();
                

                    qur.CommandText = "exec POST_SUPPLIERSTOCK_SP " + txtgrnid.Text + "," + Genclass.Dtype + " ";
                    qur.ExecuteNonQuery();
                }

            }
            MessageBox.Show("Save Record");
            Editpnl.Visible = false;

            Genpan.Visible = true;
            panadd.Visible = true;
            LoadGetJobCard(1);



        }

        private void butedit_Click(object sender, EventArgs e)
        {
            mode = 2;
            Chkedtact.Checked = true;
            mode = 2;
            panadd.Visible = false;
            Genpan.Visible = false;
            Editpnl.Visible = true;
            lkppnl.Visible = false;
            conn.Close();
            conn.Open();
          

            //cbosGReturnItem.Items.Clear();
            int i = HFGP.SelectedCells[0].RowIndex;
            uid = HFGP.Rows[i].Cells[0].Value.ToString();
            txtgrnid.Text = HFGP.Rows[i].Cells[0].Value.ToString();
            txtgrn.Text = HFGP.Rows[i].Cells[1].Value.ToString();
            dtpgrndt.Text = HFGP.Rows[i].Cells[2].Value.ToString();

            txtname.Text = HFGP.Rows[i].Cells[3].Value.ToString();
            txtqty.Text = HFGP.Rows[i].Cells[4].Value.ToString();
            txtwo.Text = HFGP.Rows[i].Cells[5].Value.ToString();
            Genclass.address = HFGP.Rows[i].Cells[6].Value.ToString();

            txtpuid.Text = HFGP.Rows[i].Cells[7].Value.ToString();
            txtitem.Tag = HFGP.Rows[i].Cells[8].Value.ToString();
            txtvehicle.Text = HFGP.Rows[i].Cells[9].Value.ToString();
            txtmode.Text = HFGP.Rows[i].Cells[10].Value.ToString();
            txtdcno.Text = HFGP.Rows[i].Cells[11].Value.ToString();
            Chkedtact.Checked = true;
            HFIT.Refresh();
            HFIT.DataSource = null;
            HFIT.Rows.Clear();

            HFGP2.Refresh();
            HFGP2.DataSource = null;
            HFGP2.Rows.Clear();
            HFGP1.Refresh();
            HFGP1.DataSource = null;
            HFGP1.Rows.Clear();

            Titlep();
            load();

        }
        private void load()
        {
            Genclass.strsql = "SP_GETJOREDITLOAD " + txtgrnid.Text + " ";
            Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
            SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
            DataTable tap1 = new DataTable();
            aptr1.Fill(tap1);

            Genclass.sum1 = 0;


            for (int k = 0; k < tap1.Rows.Count; k++)
            {
                var index = HFIT.Rows.Add();

                HFIT.Rows[index].Cells[0].Value = tap1.Rows[k]["slno"].ToString();

                HFIT.Rows[index].Cells[1].Value = tap1.Rows[k]["itemname"].ToString();


                HFIT.Rows[index].Cells[2].Value = tap1.Rows[k]["batchno"].ToString();

                HFIT.Rows[index].Cells[3].Value = tap1.Rows[k]["plandia"].ToString();

                HFIT.Rows[index].Cells[4].Value = tap1.Rows[k]["actdia"].ToString();


                HFIT.Rows[index].Cells[5].Value = tap1.Rows[k]["rolls"].ToString();
                HFIT.Rows[index].Cells[6].Value = tap1.Rows[k]["generalname"].ToString();
                HFIT.Rows[index].Cells[7].Value = tap1.Rows[k]["qty"].ToString();
                HFIT.Rows[index].Cells[8].Value = tap1.Rows[k]["itemid"].ToString();
                HFIT.Rows[index].Cells[9].Value = tap1.Rows[k]["uom_uid"].ToString();
                HFIT.Rows[index].Cells[10].Value = tap1.Rows[k]["uid"].ToString();
                HFIT.Rows[index].Cells[11].Value = tap1.Rows[k]["refid"].ToString();
                HFIT.Rows[index].Cells[12].Value = tap1.Rows[k]["WORKID"].ToString();
                HFIT.Rows[index].Cells[13].Value = tap1.Rows[k]["socno"].ToString();
                HFIT.Rows[index].Cells[14].Value = tap1.Rows[k]["workorder"].ToString();
                HFIT.Rows[index].Cells[15].Value = tap1.Rows[k]["style"].ToString();

            }
        }

        private void dtpgrndt_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                e.Handled = true;
                SendKeys.Send("{TAB}");
            }
        }

        private void txtqty_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                e.Handled = true;
                SendKeys.Send("{TAB}");
            }
        }

        private void txtmode_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                e.Handled = true;
                SendKeys.Send("{TAB}");
            }
        }

        private void txtvehicle_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                e.Handled = true;
                SendKeys.Send("{TAB}");
            }
        }

        private void txtdcqty_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                e.Handled = true;
                SendKeys.Send("{TAB}");
            }
        }

        private void txtaddnotes_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                e.Handled = true;
                SendKeys.Send("{TAB}");
            }
        }

        private void txtisstot_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                e.Handled = true;
                SendKeys.Send("{TAB}");
            }
        }

        private void txtbags_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                e.Handled = true;
                SendKeys.Send("{TAB}");
            }
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                e.Handled = true;
                SendKeys.Send("{TAB}");
            }
        }

        private void txtprice_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                e.Handled = true;
                SendKeys.Send("{TAB}");
            }

        }

        private void txtuom_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void button11_Click(object sender, EventArgs e)
        {
            lkppnl.Visible = false;
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void buttnext1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void DataGridCommon_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dtpsupp_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                e.Handled = true;
                SendKeys.Send("{TAB}");
            }
        }

        private void txtscr11_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.F10)
                {
                    lkppnl.Visible = false;
                }
                else if (e.KeyCode == Keys.F2)
                {
                    SelectId = 1;


                    int Index = HFGP2.SelectedCells[0].RowIndex;
                    txtdcqty.Text = HFGP2.Rows[Index].Cells[1].Value.ToString();
                    txtdcqty.Tag = HFGP2.Rows[Index].Cells[0].Value.ToString();
                    txtuom.Text = HFGP2.Rows[Index].Cells[3].Value.ToString();
                    txtitemid.Text = HFGP2.Rows[Index].Cells[4].Value.ToString();
                    txtprice.Text = HFGP2.Rows[Index].Cells[5].Value.ToString();
                    txtprice.Tag = HFGP2.Rows[Index].Cells[6].Value.ToString();
                    txtisstot.Text = HFGP2.Rows[Index].Cells[7].Value.ToString();
                    lkppnl.Visible = false;
                    txtaddnotes.Focus();

                    SelectId = 0;
                }
                else if (e.KeyCode == Keys.Escape)
                {
                    lkppnl.Visible = false;
                }
                else if (e.KeyValue == 40)
                {
                    HFGP2.Select();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtscr11_TextChanged(object sender, EventArgs e)
        {
            bsc.Filter = string.Format("itemdes LIKE '%{0}%' ", txtscr11.Text);
        }

        private void txtuom_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtuom_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)

            {
                btnadd_Click(sender, e);

            }
        }

        private void button13_Click(object sender, EventArgs e)
        {

        }

        private void txtdcno_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtname.Text == string.Empty)
                {
                    MessageBox.Show("Select Supplier Name", "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtname.Focus();
                    return;
                }
                if (txtmode.Text == string.Empty)
                {
                    MessageBox.Show("Enter Supplier DC Number and Date", "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtmode.Focus();
                    return;
                }
                txtscr11.Text = "";
                type = 10;
                txtscr11.Focus();
                Genclass.strsql = "SP_GETKNITRECDCNO   " + txtpuid.Text + "";
                Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
                DataTable tap = new DataTable();
                aptr.Fill(tap);
                bsc.DataSource = tap;
                Point loc = FindLocation(txtdcno);
                grSearch.Location = new Point(loc.X, loc.Y + 20);
                grSearch.Visible = true;
                DataGridCommon.AutoGenerateColumns = false;
                DataGridCommon.Refresh();
                DataGridCommon.DataSource = null;
                DataGridCommon.Rows.Clear();
                DataGridCommon.ColumnCount = tap.Columns.Count;
                i = 0;
                foreach (DataColumn column in tap.Columns)
                {
                    DataGridCommon.Columns[i].Name = column.ColumnName;
                    DataGridCommon.Columns[i].HeaderText = column.ColumnName;
                    DataGridCommon.Columns[i].DataPropertyName = column.ColumnName;
                    i = i + 1;
                }
                DataGridCommon.DataSource = tap;
                DataGridCommon.Columns[0].Width = 100;
                DataGridCommon.Columns[1].Width = 100;
                DataGridCommon.Columns[2].Width = 100;
                DataGridCommon.Columns[3].Width = 100;
                DataGridCommon.Columns[4].Visible = false;
                DataGridCommon.Columns[5].Visible = false;
                conn.Close();
            }
            catch (Exception)
            {

                throw;
            }
        }

        private void butcan_Click(object sender, EventArgs e)
        {
            conn.Close();
            conn.Open();



            int i = HFGP.SelectedCells[0].RowIndex;

            qur.CommandText = "delete  from  JOKNITtingREC  where headid=" + HFGP.Rows[i].Cells[0].Value.ToString() + "";
            qur.ExecuteNonQuery();

            qur.CommandText = "delete  from  mfg_stock_ledger  where headtable_uid=" + HFGP.Rows[i].Cells[0].Value.ToString() + "";
            qur.ExecuteNonQuery();

            qur.CommandText = "delete  from  supplier_stock_ledger  where hdid=" + HFGP.Rows[i].Cells[0].Value.ToString() + "";
            qur.ExecuteNonQuery();

            qur.CommandText = "delete  from  jo  where uid=" + HFGP.Rows[i].Cells[0].Value.ToString() + "";
            qur.ExecuteNonQuery();

            LoadGetJobCard(1);
        }

        private void txtdcno_TextChanged(object sender, EventArgs e)
        {

        }

        private void Editpnl_Paint(object sender, PaintEventArgs e)
        {

        }

        private void chkact_CheckedChanged(object sender, EventArgs e)
        {
            LoadGetJobCard(1);
        }

        private void txtscr1_TextChanged(object sender, EventArgs e)
        {
            bs.Filter = string.Format("DOcno Like '%{0}%'  or  Docdate Like '%{1}%' or  name Like '%{1}%' or  Reference Like '%{1}%' or  WorkOrderNo Like '%{1}%'  or  ProcessName Like '%{1}%'  or  vehicleno Like '%{1}%' ", txtscr1.Text, txtscr1.Text, txtscr1.Text, txtscr1.Text, txtscr1.Text, txtscr1.Text, txtscr1.Text);

        }

        private void Genpan_Paint(object sender, PaintEventArgs e)
        {

        }

        private void HFGP2_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtscono_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtname.Text == string.Empty)
                {
                    MessageBox.Show("Select Supplier Name", "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtname.Focus();
                    return;
                }
                if (txtmode.Text == string.Empty)
                {
                    MessageBox.Show("Enter Supplier DC Number and Date", "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtmode.Focus();
                    return;
                }
                txtscr11.Text = "";
                type = 11;
                txtscr11.Focus();
                if (Text == "Knitting Rework Process")
                {
                    Genclass.strsql = "SP_GETKNITREWORKROCESSODCNO   " + txtpuid.Text + "";
                    Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                }
                else if (Text == "Knitting Rework Receipt")

                {
                    Genclass.strsql = "SP_GETKNITREWORKRECEIPTDCNO   " + txtpuid.Text + "";
                    Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                }
                else   if (Text == "Fabric Rework Process")
                {
                    Genclass.strsql = "SP_GETFABREWORKROCESSODCNO   " + txtpuid.Text + "";
                    Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                }
                else if (Text == "Fabric Rework Receipt")

                {
                    Genclass.strsql = "SP_GETFABREWORKRECEIPTDCNO   " + txtpuid.Text + "";
                    Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                }
                SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
                DataTable tap = new DataTable();
                aptr.Fill(tap);
                bsc.DataSource = tap;
                Point loc = FindLocation(txtscono);
                grSearch.Location = new Point(loc.X, loc.Y + 20);
                grSearch.Visible = true;
                DataGridCommon.AutoGenerateColumns = false;
                DataGridCommon.Refresh();
                DataGridCommon.DataSource = null;
                DataGridCommon.Rows.Clear();
                DataGridCommon.ColumnCount = tap.Columns.Count;
                i = 0;
                foreach (DataColumn column in tap.Columns)
                {
                    DataGridCommon.Columns[i].Name = column.ColumnName;
                    DataGridCommon.Columns[i].HeaderText = column.ColumnName;
                    DataGridCommon.Columns[i].DataPropertyName = column.ColumnName;
                    i = i + 1;
                }
                DataGridCommon.DataSource = tap;
                DataGridCommon.Columns[0].Visible = true;
                DataGridCommon.Columns[0].Width = 100;
                DataGridCommon.Columns[1].Width = 100;
                DataGridCommon.Columns[2].Width = 100;
                DataGridCommon.Columns[3].Visible = false;
                DataGridCommon.Columns[4].Visible = false;
                DataGridCommon.Columns[5].Visible = false;
                DataGridCommon.Columns[6].Width = 100;
                conn.Close();
            }
            catch (Exception)
            {

                throw;
            }
        }

        private void txtscono_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    if (txtscono.Text != "")
                    {
                        bsc.Filter = string.Format("socno LIKE '%{0}%' ", txtscono.Text);

                    }

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
    }
}
