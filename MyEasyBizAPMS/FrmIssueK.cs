﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.Data.SqlClient;
using CrystalDecisions.CrystalReports.Engine;

namespace MyEasyBizAPMS
{
    public partial class FrmIssueK : Form
    {

        private CrystalDecisions.Windows.Forms.CrystalReportViewer Cryview;

        ReportDocument doc = new ReportDocument();
        public FrmIssueK()
        {
            this.BackColor = Color.White; ;
            InitializeComponent();
        }

        string uid = "";
        int mode = 0;
        string tpuid = "";
        int SP;
        int type = 0;
        DateTime str9;
        double sum1;
        double sum2;
        int Dtype = 0;
        int i = 0;
        Double yy = 0;
        double uu = 0;
        double gg = 0;
        double kk = 0;
        double hh = 0;
        SqlConnection conn = new SqlConnection(GeneralParameters.ConnectionString);
        SqlCommand qur = new SqlCommand();
        private DataRow doc1;
        DataTable Docno = new DataTable();

        BindingSource bs = new BindingSource();
        BindingSource bsc = new BindingSource();
        BindingSource bsp = new BindingSource();
        BindingSource bsserial = new BindingSource();
        BindingSource bsc1 = new BindingSource();
        BindingSource bsc2 = new BindingSource();

        SQLDBHelper db = new SQLDBHelper();
        int Fillid;
        BindingSource bsParty = new BindingSource();
        string str1key;
        public int SelectId = 0;
        private void FrmIssueK_Load(object sender, EventArgs e)
        {
            qur.Connection = conn;

            Genclass.buttonstyleform(this);
            Genclass.buttonstylepanel(panadd);
            panadd.Visible = true;
            Genpan.Visible = true;
            Editpnl.Visible = false;
            panel4.Visible = false;
            tabC.Visible = false;
            this.HFG9.DefaultCellStyle.Font = new Font("calibri", 10);
            this.HFG9.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 10, FontStyle.Bold);
            this.HFGP.DefaultCellStyle.Font = new Font("calibri", 9);
            this.HFGP.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 9, FontStyle.Bold);
            this.HFIT.DefaultCellStyle.Font = new Font("calibri", 10);
            this.HFIT.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 10, FontStyle.Bold);
            this.RQGR.DefaultCellStyle.Font = new Font("calibri", 10);
            this.RQGR.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 10, FontStyle.Bold);
            this.HFGP1.DefaultCellStyle.Font = new Font("calibri", 10);
            this.HFGP1.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 10, FontStyle.Bold);
            this.HFTP.DefaultCellStyle.Font = new Font("calibri", 10);
            this.HFTP.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 10, FontStyle.Bold);
            HFIT.RowHeadersVisible = false;
            HFGP.RowHeadersVisible = false;
            RQGR.RowHeadersVisible = false;
            HFGP1.RowHeadersVisible = false;
            HFTP.RowHeadersVisible = false;
            HFG9.RowHeadersVisible = false;

            this.Dataserial.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 10, FontStyle.Bold);
            this.Dataserial.DefaultCellStyle.Font = new Font("calibri", 10);
            this.Dataserial.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 10, FontStyle.Bold);

            HFG9.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            HFG9.EnableHeadersVisualStyles = false;
            HFG9.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.GradientActiveCaption; HFIT.SelectionMode = DataGridViewSelectionMode.FullRowSelect;

            HFTP.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            HFTP.EnableHeadersVisualStyles = false;
            HFTP.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.GradientActiveCaption; HFIT.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            HFIT.EnableHeadersVisualStyles = false;
            HFIT.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.GradientActiveCaption;
            HFGP1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            HFGP1.EnableHeadersVisualStyles = false;
            HFGP1.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.GradientActiveCaption;
            RQGR.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            RQGR.EnableHeadersVisualStyles = false;
            RQGR.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.GradientActiveCaption;
            HFGP.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            HFGP.EnableHeadersVisualStyles = false;
            HFGP.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.GradientActiveCaption;
            Dataserial.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            Dataserial.EnableHeadersVisualStyles = false;
            Dataserial.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.GradientActiveCaption;
            Dataserial.Focus();
            Genclass.Dtype = 540;

            Dtype = 540;
            dtpfnt.Format = DateTimePickerFormat.Custom;
            dtpfnt.CustomFormat = "dd/MM/yyyy";
            dtpgrndt.Format = DateTimePickerFormat.Custom;
            dtpgrndt.CustomFormat = "dd/MM/yyyy";
            reqdt.Format = DateTimePickerFormat.Custom;
            reqdt.CustomFormat = "dd/MM/yyyy";
            chkact.Checked = true;
            tax();
            Titlep3();
            Titlep5();
            LoadGetJobCard(1);
            Titlep();
            type1();
            tabC.TabPages.Remove(tabPage3);

        }

        public void type1()
        {
            conn.Open();
            string qur = "select Generalname  as type,guid as uid from generalm where   Typemuid in (40)  and active=1 ";
            SqlCommand cmd = new SqlCommand(qur, conn);
            SqlDataAdapter apt = new SqlDataAdapter(cmd);
            DataTable tab = new DataTable();
            apt.Fill(tab);
            cbotype.DataSource = null;
            cbotype.DataSource = tab;
            cbotype.DisplayMember = "type";
            cbotype.ValueMember = "uid";
            cbotype.SelectedIndex = -1;
            conn.Close();
        }
        private void Titlep()
        {
            HFIT.AutoGenerateColumns = false;
            HFIT.Refresh();
            HFIT.DataSource = null;
            HFIT.Rows.Clear();
            HFIT.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            HFIT.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            HFIT.ColumnCount = 22;
            HFIT.Columns[0].Name = "SLNo";
            HFIT.Columns[1].Name = "Itemname";
            HFIT.Columns[2].Name = "Type";
            HFIT.Columns[3].Name = "Loop Length";
            HFIT.Columns[4].Name = "GSM";
            HFIT.Columns[5].Name = "Knit Dia";
            HFIT.Columns[6].Name = "Uom";
            HFIT.Columns[7].Name = "Qty";
            HFIT.Columns[8].Name = "No of Rolls";
            HFIT.Columns[9].Name = "Rate";
            HFIT.Columns[10].Name = "Tax";
            HFIT.Columns[11].Name = "Total";
            HFIT.Columns[12].Name = "itemid";
            HFIT.Columns[13].Name = "uomid";
            HFIT.Columns[14].Name = "taxid";
            HFIT.Columns[15].Name = "uid";
            HFIT.Columns[16].Name = "TAXVALUE";
            HFIT.Columns[17].Name = "Itemdes";
            HFIT.Columns[18].Name = "refid";
            HFIT.Columns[19].Name = "socno";
            HFIT.Columns[20].Name = "workorderno";
            HFIT.Columns[21].Name = "style";
            //HFIT.EditMode = DataGridViewEditMode.EditOnKeystroke;

            HFIT.Columns[0].Width = 40;

            HFIT.Columns[1].Width = 340;
            HFIT.Columns[2].Width = 70;
            HFIT.Columns[3].Width = 50;
            HFIT.Columns[4].Visible = false;
            HFIT.Columns[5].Width = 60;
            HFIT.Columns[6].Width = 60;
            HFIT.Columns[7].Width = 60;
            HFIT.Columns[8].Width = 70;
            HFIT.Columns[9].Width = 70;
            HFIT.Columns[10].Width = 70;
            HFIT.Columns[11].Width = 70;
            HFIT.Columns[12].Visible = false;
            HFIT.Columns[13].Visible = false;
            HFIT.Columns[14].Visible = false;
            HFIT.Columns[15].Visible = false;
            HFIT.Columns[16].Visible = false;
            HFIT.Columns[17].Visible = false;
            HFIT.Columns[18].Visible = false;
            HFIT.Columns[19].Visible = false;
            HFIT.Columns[20].Visible = false;
            HFIT.Columns[21].Visible = false;
        }
        protected DataTable getParty()
        {
            DataTable dt = new DataTable();
            try
            {
                if (type == 1)
                {
                    dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "SP_ITEMJOBISSUEGET", conn);
                    bsParty.DataSource = dt;
                }
                else if (type == 2)
                {
                    dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "SP_PROCESSM", conn);
                    bsc.DataSource = dt;
                }
                else if (type == 3)
                {
                    dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "SP_GETPARTYSUPPLIERRateapp", conn);
                    bsp.DataSource = dt;
                }
                else if (type == 4|| type == 7)
                { 
                        SqlParameter[] para = { new SqlParameter("@puid", txtpuid.Text) };
                    dt = db.getDataWithParameterCmdType(CommandType.StoredProcedure, "SP_GETWONEW", para);
                    bsc1.DataSource = dt;
               }
          
                else if (type == 5)
                {
                    dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "SP_ITEMJOBISSUEOUTGET", conn);
                    bsc2.DataSource = dt;
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            return dt;
        }

        protected DataTable LoadGetJobCard(int tag)
        {

      
            chkact.Checked = true;
            if (chkact.Checked == true)
            {
                SP = 1;

            }
            else
            {
                SP = 0;

            }


            DataTable dt = new DataTable();
            try
            {

                SqlParameter[] para = {
                    new SqlParameter("@active",SP),
               


                };

                dt = db.GetData(CommandType.StoredProcedure, "SP_GETKNITTLOAD", para);


                LoadDataTable(dt);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        protected void LoadDataTable(DataTable dt)
        {
            try
            {
                HFGP.DataSource = null;
                HFGP.AutoGenerateColumns = false;
                HFGP.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
                HFGP.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
                HFGP.ColumnCount = 12;
                HFGP.Columns[0].Name = "Uid";
                HFGP.Columns[0].HeaderText = "Uid";
                HFGP.Columns[0].DataPropertyName = "Uid";

                HFGP.Columns[1].Name = "DOcno";
                HFGP.Columns[1].HeaderText = "Docno";
                HFGP.Columns[1].DataPropertyName = "DOcno";

                HFGP.Columns[2].Name = "Docdate";
                HFGP.Columns[2].HeaderText = "Docdate";
                HFGP.Columns[2].DataPropertyName = "Docdate";

                HFGP.Columns[3].Name = "name";
                HFGP.Columns[3].HeaderText = "name";
                HFGP.Columns[3].DataPropertyName = "name";

                HFGP.Columns[4].Name = "Reference";
                HFGP.Columns[4].HeaderText = "Reference";
                HFGP.Columns[4].DataPropertyName = "Reference";

                HFGP.Columns[5].Name = "WorkOrderNo";
                HFGP.Columns[5].HeaderText = "WorkOrderNo";
                HFGP.Columns[5].DataPropertyName = "WorkOrderNo";

                HFGP.Columns[6].Name = "ProcessName";
                HFGP.Columns[6].HeaderText = "ProcessName";
                HFGP.Columns[6].DataPropertyName = "ProcessName";

                HFGP.Columns[7].Name = "supplieruid";
                HFGP.Columns[7].HeaderText = "supplieruid";
                HFGP.Columns[7].DataPropertyName = "supplieruid";

                HFGP.Columns[8].Name = "refid";
                HFGP.Columns[8].HeaderText = "refid";
                HFGP.Columns[8].DataPropertyName = "refid";

                HFGP.Columns[9].Name = "vehicleno";
                HFGP.Columns[9].HeaderText = "vehicleno";
                HFGP.Columns[9].DataPropertyName = "vehicleno";


                HFGP.Columns[10].Name = "mode";
                HFGP.Columns[10].HeaderText = "mode";
                HFGP.Columns[10].DataPropertyName = "mode";
                               
                HFGP.Columns[11].Name = "reason";
                HFGP.Columns[11].HeaderText = "reason";
                HFGP.Columns[11].DataPropertyName = "reason";

                bs.DataSource = dt;

                HFGP.DataSource = bs;

                HFGP.Columns[0].Visible = false;
                HFGP.Columns[1].Width = 90;
                HFGP.Columns[2].Width = 90;
                HFGP.Columns[3].Width = 500;
                //HFGP.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                HFGP.Columns[4].Width = 150;
                HFGP.Columns[5].Width = 100;
                HFGP.Columns[6].Width = 130;
                HFGP.Columns[7].Visible = false;
                HFGP.Columns[8].Visible = false;
                HFGP.Columns[9].Visible = false;
                HFGP.Columns[10].Visible = false;
                HFGP.Columns[11].Visible = false;
               
         

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void Loadgrid()
        {
            try
            {
                conn.Close();
                conn.Open();
                Genclass.StrSrch = "";
                Genclass.FSSQLSortStr = "Docno";
                Genclass.FSSQLSortStr1 = "Docdate";
                Genclass.FSSQLSortStr2 = "Name";
                Genclass.FSSQLSortStr3 = "vehicleno";
                Genclass.FSSQLSortStr4 = "placeofsupply";
                Genclass.FSSQLSortStr5 = "Dcno";
                if (txtscr1.Text != "")
                {
                    if (Genclass.StrSrch == "")
                    {
                        Genclass.StrSrch = Genclass.FSSQLSortStr + " like '%" + txtscr1.Text + "%'";
                    }
                    else
                    {
                        Genclass.StrSrch = Genclass.StrSrch + " and " + Genclass.FSSQLSortStr + " like '%" + txtscr1.Text + "%'";
                    }
                }

                if (Txtscr2.Text != "")
                {
                    if (Genclass.StrSrch == "")
                    {
                        Genclass.StrSrch = Genclass.FSSQLSortStr1 + " like '%" + Txtscr2.Text + "%'";
                    }
                    else
                    {
                        Genclass.StrSrch = Genclass.StrSrch + " and " + Genclass.FSSQLSortStr1 + " like '%" + Txtscr2.Text + "%'";
                    }
                }

                if (txtscr5.Text != "")
                {
                    if (Genclass.StrSrch == "")
                    {
                        Genclass.StrSrch = Genclass.FSSQLSortStr2 + " like '%" + txtscr5.Text + "%'";
                    }
                    else
                    {
                        Genclass.StrSrch = Genclass.StrSrch + " and " + Genclass.FSSQLSortStr2 + " like '%" + txtscr5.Text + "%'";
                    }
                }

                if (txtscr6.Text != "")
                {
                    if (Genclass.StrSrch == "")
                    {
                        Genclass.StrSrch = Genclass.FSSQLSortStr3 + " like '%" + txtscr6.Text + "%'";
                    }
                    else
                    {
                        Genclass.StrSrch = Genclass.StrSrch + " and " + Genclass.FSSQLSortStr3 + " like '%" + txtscr6.Text + "%'";
                    }
                }
                if (textBox1.Text != "")
                {
                    if (Genclass.StrSrch == "")
                    {
                        Genclass.StrSrch = Genclass.FSSQLSortStr4 + " like '%" + textBox1.Text + "%'";
                    }
                    else
                    {
                        Genclass.StrSrch = Genclass.StrSrch + " and " + Genclass.FSSQLSortStr4 + " like '%" + textBox1.Text + "%'";
                    }
                }
                if (textBox2.Text != "")
                {
                    if (Genclass.StrSrch == "")
                    {
                        Genclass.StrSrch = Genclass.FSSQLSortStr5 + " like '%" + textBox2.Text + "%'";
                    }
                    else
                    {
                        Genclass.StrSrch = Genclass.StrSrch + " and " + Genclass.FSSQLSortStr5 + " like '%" + textBox2.Text + "%'";
                    }
                }
                if (txtscr1.Text != "")
                {
                    Genclass.StrSrch = " " + Genclass.StrSrch;
                }
                else if (Txtscr2.Text != "")
                {
                    Genclass.StrSrch = " " + Genclass.StrSrch;
                }
                else if (txtscr5.Text != "")
                {
                    Genclass.StrSrch = " " + Genclass.StrSrch;
                }
                else if (txtscr6.Text != "")
                {
                    Genclass.StrSrch = " " + Genclass.StrSrch;
                }
                else if (textBox1.Text != "")
                {
                    Genclass.StrSrch = " " + Genclass.StrSrch;
                }
                else if (textBox2.Text != "")
                {
                    Genclass.StrSrch = " " + Genclass.StrSrch;
                }
                else
                {
                    Genclass.StrSrch = "a.uid <> 0";
                }
                 str9 = Convert.ToDateTime(dtpfnt.Text);
                if (Genclass.Dtype == 540)
                {
                    if (chkact.Checked == true)
                    {
                        string quy = "select a.uid,DOcno,DocDate as Docdate,B.name,Dcno as Reference,a.Remarks as WorkOrderNo,a.placeofsupply as ProcessName,a.supplieruid,a.refid,a.vehicleno,a.mode,a.reason  from jo a inner join partym b on a.supplieruid=b.uid where doctypeid=540  and a.active=1  and  " + Genclass.StrSrch + "  order by docno desc";
                        Genclass.cmd = new SqlCommand(quy, conn);
                    }
                    else
                    {
                        string quy = "select a.uid,DOcno,DocDate as Docdate,B.name,Dcno as Reference,a.Remarks as WorkOrderNo,a.placeofsupply as ProcessName,a.supplieruid,a.refid,a.vehicleno,a.mode,a.reason   from jo a inner join partym b on a.supplieruid=b.uid where doctypeid=540  and a.active=1  and  " + Genclass.StrSrch + "  order by docno desc";
                        Genclass.cmd = new SqlCommand(quy, conn);
                    }
                }

                SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
                DataTable tap = new DataTable();
                aptr.Fill(tap);
                HFGP.AutoGenerateColumns = false;
                HFGP.Refresh();
                HFGP.DataSource = null;
                HFGP.Rows.Clear();
                HFGP.ColumnCount = tap.Columns.Count;
                i = 0;
                foreach (DataColumn column in tap.Columns)
                {
                    HFGP.Columns[i].Name = column.ColumnName;
                    HFGP.Columns[i].HeaderText = column.ColumnName;
                    HFGP.Columns[i].DataPropertyName = column.ColumnName;
                    i = i + 1;
                }
                HFGP.Columns[0].Visible = false;
                HFGP.Columns[1].Width = 90;
                HFGP.Columns[2].Width = 90;
                HFGP.Columns[3].Width = 500;
                //HFGP.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                HFGP.Columns[4].Width = 150;
                HFGP.Columns[5].Width = 100;
                HFGP.Columns[6].Width = 130;
                HFGP.Columns[7].Visible = false;
                HFGP.Columns[8].Visible = false;
                HFGP.Columns[9].Visible = false;
                HFGP.Columns[10].Visible = false;
                HFGP.Columns[11].Visible = false;
                HFGP.DataSource = tap;
                HFGP.Columns[2].DefaultCellStyle.Format = "yyyy'/'MM'/'dd";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }
            finally
            {
                conn.Close();
                Genclass.cmd.Dispose();
            }
        }

        private void loadput()
        {
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            mode = 1;
            Genpan.Visible = false;
            serialno.Visible = false;
            tabC.Visible = true;
            panadd.Visible = false;
            Genclass.h = 0;
            Genclass.ClearTextBox(this, Editpnl);
            Genclass.STR = "";
            Genclass.Gendocno();
            txtgrn.Text = Genclass.ST;
            Editpnl.Visible = true;
            str1key = "F2";
            sum1 = 0;
            sum2 = 0;
            HFIT.Refresh();
            HFIT.DataSource = null;
            HFIT.Rows.Clear();
            RQGR.Refresh();
            RQGR.DataSource = null;
            RQGR.Rows.Clear();
            HFGP1.Refresh();
            HFGP1.DataSource = null;
            HFGP1.Rows.Clear();
            HFTP.Refresh();
            HFTP.DataSource = null;
            HFTP.Rows.Clear();
            conn.Close();
            conn.Open();
            Docno.Clear();
            label28.Visible = false;
            reqdt.Visible = false;
            button13.Visible = false;
            if (Docno.Columns.Count == 0)
            {
                Docno.Columns.Add("uid");
                Docno.Columns.Add("Docno");
            }
            conn.Close();
            conn.Open();

            qur.CommandText = "truncate table  jjissuetemp";
            qur.ExecuteNonQuery();

            qur.CommandText = "truncate table  reqdt";
            qur.ExecuteNonQuery();


            qur.CommandText = "truncate table  reqtempp";
            qur.ExecuteNonQuery();

            Chkedtact.Checked = true;
            RQGR.Refresh();
            RQGR.DataSource = null;
            RQGR.Rows.Clear();
            HFIT.Refresh();
            HFIT.DataSource = null;
            HFIT.Rows.Clear();
            HFGP1.Refresh();
            HFGP1.DataSource = null;
            HFGP1.Rows.Clear();
            Titlep3();
            Titlep();

            TabPage page2 = tabC.TabPages[1];
            if (page2.Visible == true)
            {
                txtoutitem.Focus();

            }
            //TabPage page3 = tabC.TabPages[2];
           

            dtpgrndt.Focus();

        }

        private void Btnaddrcan_Click(object sender, EventArgs e)
        {
            tabC.Visible = false;
            Editpnl.Visible = false;
            Genpan.Visible = true;
            panadd.Visible = true;

            LoadGetJobCard(1);
        }

        private void Buttnext1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Button9_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Txtname_Click(object sender, EventArgs e)
        {
            type = 3;
            button22.Visible = true;
            DataTable dt = getParty();
            bsp.DataSource = dt;
            FillGrid2(dt, 1);
            Point loc = FindLocation(txtname);
            grSearch.Location = new Point(loc.X, loc.Y + 10);
            grSearch.Visible = true;
        }

        private void Txtname_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.F10)
                {
                    grSearch.Visible = false;
                }
                else if (e.KeyCode == Keys.F2)
                {
                    SelectId = 1;
                    int Index = DataGridCommon.SelectedCells[0].RowIndex;

                    txtname.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtpuid.Text = DataGridCommon.Rows[Index].Cells[0].Value.ToString();

                    txtvehicle.Focus();
                    grSearch.Visible = false;
                    SelectId = 0;
                }
                else if (e.KeyCode == Keys.Escape)
                {
                    grSearch.Visible = false;
                }
                else if (e.KeyValue == 40)
                {
                    DataGridCommon.Select();
                }
                else if (e.KeyCode == Keys.Enter)
                {
                    Txtname_Click(sender, e);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void Txtname_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bsp.Filter = string.Format("Name LIKE '%{0}%' ", txtname.Text);



                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }


        private void Getitems()
        {
            panel4.Visible = true;

            txtscr11.Text = "";

            string pp;
            string ww1;

            txtscr11.Focus();

            Genclass.strsql1 = "SP_GETKNITISSUEPLANITEMS  '" + textBox5.Text + "'  ";
            Genclass.cmd = new SqlCommand(Genclass.strsql1, conn);
            SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
            DataTable tap1 = new DataTable();
            aptr1.Fill(tap1);
            if (tap1.Rows.Count > 0)
            {
                pp = Convert.ToString(tap1.Rows[0]["itemname"].ToString());

                Genclass.strfin = Convert.ToString(tap1.Rows[0]["color"].ToString());
                int rr2 = pp.Length;
                ww1 = pp.Substring(1, 10);
                Genclass.STR = pp.Substring(0, 10);

            }


            //Genclass.strsql2 = "select  * from processm1 where processname='" + CBOWNO.Text + "'";
            //Genclass.cmd = new SqlCommand(Genclass.strsql2, conn);
            //SqlDataAdapter aptr2 = new SqlDataAdapter(Genclass.cmd);
            //DataTable tap2 = new DataTable();
            //aptr2.Fill(tap2);

            //if (tap2.Rows.Count > 0)
            //{
            //    Genclass.Str5 = Convert.ToString(tap2.Rows[0]["processsname"].ToString());

            //}
            Genclass.strsql = "SP_GETKNITISSUEPLANITEMS1  '" + textBox5.Text + "'  ";
            Genclass.FSSQLSortStr = "b.itemname";
            Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
            SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
            DataTable tap = new DataTable();
            aptr.Fill(tap);
            if (tap.Rows.Count == 0)
            {
                string Query = "SP_GETKNITISSUEPLANITEMS2  " + txtitemid.Tag + " ";
                SQLDBHelper db = new SQLDBHelper();
                tap = new DataTable();
                tap = db.GetDataWithoutParam(CommandType.Text, Query, conn);
            }

            bsParty.DataSource = tap;



            HFG9.DataSource = null;
            HFG9.AutoGenerateColumns = false;
            HFG9.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            HFG9.DefaultCellStyle.WrapMode = DataGridViewTriState.True;

            HFG9.ColumnCount = 10;
            HFG9.Columns[0].Name = "uid";
            HFG9.Columns[0].HeaderText = "uid";
            HFG9.Columns[0].DataPropertyName = "uid";
            HFG9.Columns[1].Name = "itemname";
            HFG9.Columns[1].HeaderText = "Itemname";
            HFG9.Columns[1].DataPropertyName = "itemname";
            HFG9.Columns[1].Width = 550;
            HFG9.Columns[2].Name = "UOMID";
            HFG9.Columns[2].HeaderText = "UOMID";
            HFG9.Columns[2].DataPropertyName = "UOMID";

            HFG9.Columns[3].Name = "GENERALNAME";
            HFG9.Columns[3].HeaderText = "GENERALNAME";
            HFG9.Columns[3].DataPropertyName = "GENERALNAME";


            HFG9.Columns[4].Name = "OrderQty";
            HFG9.Columns[4].HeaderText = "OrderQty";
            HFG9.Columns[4].DataPropertyName = "OrderQty";
            HFG9.Columns[4].Width = 100;
     
            HFG9.Columns[5].Name = "detid";
            HFG9.Columns[5].HeaderText = "detid";
            HFG9.Columns[5].DataPropertyName = "detid";
            HFG9.Columns[5].Visible = false;
            HFG9.Columns[6].Name = "color";
            HFG9.Columns[6].HeaderText = "color";
            HFG9.Columns[6].DataPropertyName = "color";
            HFG9.Columns[6].Visible = false;

            HFG9.Columns[7].Name = "WORKID";
            HFG9.Columns[7].HeaderText = "WORKID";
            HFG9.Columns[7].DataPropertyName = "WORKID";
            HFG9.Columns[7].Visible = false;
            HFG9.Columns[8].Name = "WorkorderNo";
            HFG9.Columns[8].HeaderText = "WorkorderNo";
            HFG9.Columns[8].DataPropertyName = "WorkorderNo";

            HFG9.Columns[9].Name = "Rate";
            HFG9.Columns[9].HeaderText = "Rate";
            HFG9.Columns[9].DataPropertyName = "Rate";

            HFG9.DataSource = bsParty;
            HFG9.Columns[0].Visible = false;
            HFG9.Columns[2].Visible = false;
            HFG9.Columns[3].Visible = false;
            HFG9.Columns[9].Visible = false;





            conn.Close();

        }
        private void txtdcqty_Click(object sender, EventArgs e)
        {

            panel4.Visible = true;
            txtscr11.Text = "";
            txtscr11.Focus();

            HFG9.DataSource = null;
            HFG9.AutoGenerateColumns = false;
            HFG9.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            HFG9.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            textBox7.Text = string.Empty;
            textBox5.Text = string.Empty;
            textBox8.Text = string.Empty;

            //string qur = " sp_getwo " + txtpuid.Text + " ";
            //SqlCommand cmd = new SqlCommand(qur, conn);
            //SqlDataAdapter apt = new SqlDataAdapter(cmd);
            //DataTable tab = new DataTable();
            //apt.Fill(tab);
            //CBOWNO.DataSource = null;
            //CBOWNO.DataSource = tab;
            //CBOWNO.DisplayMember = "socno";
            //CBOWNO.ValueMember = "uid";
            //CBOWNO.SelectedIndex = -1;


        }

        private void txtdcqty_TextChanged(object sender, EventArgs e)
        {
        }
        private void Titlep1()
        {
            RQGR.AutoGenerateColumns = false;
            RQGR.Refresh();
            RQGR.DataSource = null;
            RQGR.Rows.Clear();
            RQGR.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            RQGR.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            RQGR.ColumnCount = 4;
            RQGR.Columns[0].Name = "Beamid";
            RQGR.Columns[1].Name = "Beam";
            RQGR.Columns[2].Name = "itemid";
            RQGR.Columns[3].Name = "jorlistid";
            RQGR.EditMode = DataGridViewEditMode.EditOnKeystroke;
            RQGR.Columns[1].ReadOnly = false;
            RQGR.Columns[2].ReadOnly = false;
            RQGR.Columns[0].Visible = false;
            RQGR.Columns[1].Width = 150;
            RQGR.Columns[2].Visible = false;
            RQGR.Columns[3].Visible = false;
        }

        private void Titlep3()
        {
            RQGR.ColumnCount = 11;

            RQGR.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            RQGR.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            RQGR.Columns[0].Name = "SLNO";
            RQGR.Columns[1].Name = "ITEMID";
            RQGR.Columns[2].Name = "ITEMNAME";
            RQGR.Columns[3].Name = "Uom";
            RQGR.Columns[4].Name = "QTY";
            RQGR.Columns[5].Name = "NoofBags";
            RQGR.Columns[6].Name = "uid";
            RQGR.Columns[7].Name = "workid";
            RQGR.Columns[8].Name = "socno";
            RQGR.Columns[9].Name = "workorder";
            RQGR.Columns[10].Name = "Style";

            RQGR.Columns[0].Width = 90;
            RQGR.Columns[1].Visible = false;

            RQGR.Columns[2].Width = 350;
            RQGR.Columns[3].Width = 60;
            RQGR.Columns[4].Width = 100;
            RQGR.Columns[5].Width = 100;
            RQGR.Columns[6].Visible = false;
            RQGR.Columns[7].Visible = false;
            RQGR.Columns[8].Width = 100;
            RQGR.Columns[9].Width = 100;
            RQGR.Columns[10].Width = 100;

        }
        public void tax()
        {
            conn.Open();
            string qur = "select Generalname as tax,guid uid from generalm where   Typemuid in (8) and active=1 ";
            SqlCommand cmd = new SqlCommand(qur, conn);
            SqlDataAdapter apt = new SqlDataAdapter(cmd);
            DataTable tab = new DataTable();
            apt.Fill(tab);
            cbotax.DataSource = null;
            cbotax.DataSource = tab;
            cbotax.DisplayMember = "tax";
            cbotax.ValueMember = "uid";
            cbotax.SelectedIndex = -1;
            conn.Close();
        }
        private void Titlep5()
        {
            HFTP.ColumnCount = 5;


            HFTP.Columns[0].Name = "SLNO";
            HFTP.Columns[1].Name = "ITEMID";
            HFTP.Columns[2].Name = "REQ DATE";
            HFTP.Columns[3].Name = "QTY";
            HFTP.Columns[4].Name = "uid";





            HFTP.Columns[0].Width = 90;
            HFTP.Columns[1].Visible = false;

            HFTP.Columns[2].Width = 150;
            HFTP.Columns[3].Width = 100;
            HFTP.Columns[4].Visible = false;



        }
        private void txtdcqty_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.F10)
                {
                    grSearch.Visible = false;
                }
                else if (e.KeyCode == Keys.F2)
                {
                    SelectId = 1;
                    int Index = DataGridCommon.SelectedCells[0].RowIndex;
                    txtdcqty.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtitemid.Text = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    txtoutoutqty.Tag = DataGridCommon.Rows[Index].Cells[2].Value.ToString();
                    txtoutoutqty.Text = DataGridCommon.Rows[Index].Cells[3].Value.ToString();

                    txtitem.Focus();
                    grSearch.Visible = false;
                    SelectId = 0;
                }
                else if (e.KeyCode == Keys.Escape)
                {
                    grSearch.Visible = false;
                }
                else if (e.KeyValue == 40)
                {
                    DataGridCommon.Select();
                }
                else if (e.KeyCode == Keys.Enter)
                {
                    txtdcqty_Click(sender, e);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnsave_Click(object sender, EventArgs e)
        {


            conn.Close();
            conn.Open();


            Dtype = 540;
            if (mode == 2)
            {


                string quyw = "SELECT* from  JOKNITTINGITEMS a inner join JOKNITtingREC b on a.uid=b.refid where a.headid=" + txtgrnid.Text + "  ";
                Genclass.cmd = new SqlCommand(quyw, conn);
                SqlDataAdapter aptr3 = new SqlDataAdapter(Genclass.cmd);
                DataTable tap3 = new DataTable();
                aptr3.Fill(tap3);
                if (tap3.Rows.Count > 0)
                {
                    MessageBox.Show("Can not edit the Issue already rise the Receipt");
                    return;

                }


            }

            conn.Close();
            conn.Open();
            int SP;
            if (Chkedtact.Checked == true)
            {

                SP = 1;
            }
            else
            {
                SP = 0;

            }
            int yy;
            yy = RQGR.RowCount - 1;
            if (RQGR.RowCount <= 1)
            {
                MessageBox.Show("Enter the  dcitems");
                return;

            }
            if (HFIT.RowCount <= 1)
            {
                MessageBox.Show("Enter the plan items");
                return;

            }




            if (mode == 1)
            {
                
                SqlParameter[] para ={
                    new SqlParameter("@DOCNO",txtgrn.Text),
                    new SqlParameter("@DOCDATE",Convert.ToDateTime(dtpgrndt.Text)),
                    new SqlParameter("@REFNO",txtwref.Text),
                    new SqlParameter("@DCNO",txtqty.Text),
                    new SqlParameter("@DCDATE",Convert.ToDateTime(dtpgrndt.Text)),
                    new SqlParameter("@DOCTYPEID", Dtype),
                    new SqlParameter("@SUPPLIERUID",txtpuid.Text),
                    new SqlParameter("@PLACEOFSUPPLY",cboprocess.Text),
                    new SqlParameter("@MODE",txtworder.Text),
                    new SqlParameter("@REMARKS",txtvehicle.Text),
                    new SqlParameter("@VEHICLENO",txtnobeams.Text),
                    new SqlParameter("@REASON", txtseqno.Text),
                    new SqlParameter("@REFID","0"),
                    new SqlParameter("@YEARID",Genclass.Yearid),
                    new SqlParameter("@ACTIVE", SP)
                };
                db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_JO", para, conn);

                string quy6 = "select * from jo where docno='" + txtgrn.Text + "' and doctypeid=540";
                Genclass.cmd = new SqlCommand(quy6, conn);
                SqlDataAdapter aptr3 = new SqlDataAdapter(Genclass.cmd);
                DataTable tap5 = new DataTable();
                aptr3.Fill(tap5);
                if (tap5.Rows.Count > 0)
                {
                    txtgrnid.Text = tap5.Rows[0]["uid"].ToString();
                }
                decimal itemid = 0;
                for (int i = 0; i < HFIT.RowCount - 1; i++)
                {
                    conn.Close();
                    conn.Open();
                
                    SqlParameter[] para1 ={
                        new SqlParameter("@slno", HFIT.Rows[i].Cells[0].Value),
                      new SqlParameter("@iTEMNAME",Convert.ToString(HFIT.Rows[i].Cells[1].Value)),
                        new SqlParameter("@ITEMID", HFIT.Rows[i].Cells[12].Value),
                        new SqlParameter("@COLOR", HFIT.Rows[i].Cells[3].Value),
                        new SqlParameter("@GSM", HFIT.Rows[i].Cells[4].Value),
                        new SqlParameter("@KNITDIA", HFIT.Rows[i].Cells[5].Value),
                        new SqlParameter("@QTY",HFIT.Rows[i].Cells[7].Value),
                        new SqlParameter("@ROLLS", HFIT.Rows[i].Cells[8].Value),
                        new SqlParameter("@RATE", HFIT.Rows[i].Cells[9].Value),
                        new SqlParameter("@TAXID",HFIT.Rows[i].Cells[14].Value),
                        new SqlParameter("@TAXVAL", Convert.ToDecimal(HFIT.Rows[i].Cells[16].Value)),
                        new SqlParameter("@TOTAL",Convert.ToDecimal( HFIT.Rows[i].Cells[11].Value)),
                        new SqlParameter("@HEADID", txtgrnid.Text),
                        new SqlParameter("@typeid",HFIT.Rows[i].Cells[2].Value),
                        new SqlParameter("@iTEMdes",HFIT.Rows[i].Cells[17].Value),
                        new SqlParameter("@refid",HFIT.Rows[i].Cells[18].Value),
                        new SqlParameter("@uom",HFIT.Rows[i].Cells[6].Value),
                         new SqlParameter("@socno",HFIT.Rows[i].Cells[19].Value),
                        new SqlParameter("@workorder",HFIT.Rows[i].Cells[20].Value),
                        new SqlParameter("@style",HFIT.Rows[i].Cells[21].Value),
                    };
                    db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_JOBISSUEDETTAB", para1, conn);
                    
                }

                for (int i = 0; i < RQGR.RowCount - 1; i++)
                {
                    conn.Close();
                    conn.Open();
                    SqlParameter[] para1 ={
                        new SqlParameter("@slno", RQGR.Rows[i].Cells[0].Value),
                        new SqlParameter("@iTEMNAME",RQGR.Rows[i].Cells[2].Value),
                        new SqlParameter("@ITEMID", RQGR.Rows[i].Cells[1].Value),
                        new SqlParameter("@QTY",RQGR.Rows[i].Cells[4].Value),
                        new SqlParameter("@ROLLS", RQGR.Rows[i].Cells[5].Value),
                        new SqlParameter("@HEADID", txtgrnid.Text),
                                 new SqlParameter("@workid", RQGR.Rows[i].Cells[7].Value),
                                     new SqlParameter("@uom",RQGR.Rows[i].Cells[3].Value),
                                       new SqlParameter("@socno",RQGR.Rows[i].Cells[9].Value),
                        new SqlParameter("@workorder",RQGR.Rows[i].Cells[8].Value),
                         new SqlParameter("@style",RQGR.Rows[i].Cells[10].Value),
                    };
                    db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_dcitems", para1, conn);
                }
            
                //SqlParameter[] parameters = {
                //    new SqlParameter("@HeaderId",txtgrnid.Text),
                //    new SqlParameter("@ItemId",itemid)
                //};
                //db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_JOBISSUEDETTAB_ItemUpdate", parameters, conn);
                if ((HFTP.RowCount - 1) > 1)
                {
                    for (int i = 0; i < HFTP.RowCount - 1; i++)
                    {
                        conn.Close();
                        conn.Open();
                        SqlParameter[] para1 ={
                            new SqlParameter("@slno", HFTP.Rows[i].Cells[0].Value),
                            new SqlParameter("@ITEMID", HFTP.Rows[i].Cells[1].Value),
                            new SqlParameter("@date", HFTP.Rows[i].Cells[2].Value),
                            new SqlParameter("@QTY",HFTP.Rows[i].Cells[3].Value),
                            new SqlParameter("@HEADID", txtgrnid.Text),
                        };
                        db.ExecuteNonQuery(CommandType.StoredProcedure, "sp_reqdateissue", para1, conn);
                    }
                }

                else
                {
                    conn.Close();
                    conn.Open();
                    string quy33 = "select * from reqdt";
                    Genclass.cmd = new SqlCommand(quy33, conn);
                    SqlDataAdapter aptr33 = new SqlDataAdapter(Genclass.cmd);
                    DataTable tap33 = new DataTable();
                    aptr33.Fill(tap33);
                    Genclass.tt = 0;
                    if (tap33.Rows.Count > 0)
                    {
                        conn.Close();
                        conn.Open();

                        string quy34 = "select itemid,qty from JOKNITTINGITEMS where headid=" + txtgrnid.Text + "";
                        Genclass.cmd = new SqlCommand(quy34, conn);
                        SqlDataAdapter aptr34 = new SqlDataAdapter(Genclass.cmd);
                        DataTable tap34 = new DataTable();
                        aptr34.Fill(tap34);
                        for (int p = 0; p < tap34.Rows.Count; p++)
                        {
                            conn.Close();
                            conn.Open();
                            Genclass.tt = Genclass.tt + 1;
                            qur.CommandText = "insert into reqdateissue values (" + Genclass.tt + "," + tap34.Rows[p]["itemid"].ToString() + ",'" + Convert.ToDateTime(reqdt.Text) + "'," + tap34.Rows[p]["qty"].ToString() + "," + txtgrnid.Text + ")";
                            qur.ExecuteNonQuery();
                        }
                    }
                }
            }
            else
            {
                int qq;
                qq = RQGR.RowCount - 1;
                if (qq >= 1)
                {
                }
                else
                {
                    MessageBox.Show("Please Completed Process");
                    return;
                }
                conn.Close();
                conn.Open();
                SqlParameter[] para ={
                    new SqlParameter("@DOCNO",txtgrn.Text),
                    new SqlParameter("@DOCDATE",Convert.ToDateTime(dtpgrndt.Text)),
                    new SqlParameter("@REFNO",txtwref.Text),
                    new SqlParameter("@DCNO",txtqty.Text),
                    new SqlParameter("@DCDATE",Convert.ToDateTime(dtpgrndt.Text)),
                    new SqlParameter("@DOCTYPEID", Dtype),
                    new SqlParameter("@SUPPLIERUID",txtpuid.Text),
                    new SqlParameter("@PLACEOFSUPPLY",cboprocess.Text),
                    new SqlParameter("@MODE",txtworder.Text),
                    new SqlParameter("@REMARKS",txtvehicle.Text),
                    new SqlParameter("@VEHICLENO",txtnobeams.Text),
                    new SqlParameter("@REASON",txtseqno.Text),
                    new SqlParameter("@REFID","0"),
                    new SqlParameter("@YEARID",Genclass.Yearid),
                    new SqlParameter("@ACTIVE", SP),
                    new SqlParameter("@UID", txtgrnid.Text)
                };
                db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_JOUPDATE", para, conn);
                conn.Close();
                conn.Open();
                qur.CommandText = "delete from JOKNITTINGITEMS where headid=" + txtgrnid.Text + "";
                qur.ExecuteNonQuery();
                qur.CommandText = "delete from reqdateissue where headi=" + txtgrnid.Text + " ";
                qur.ExecuteNonQuery();
                qur.CommandText = "delete from knitingdcitems where headid=" + txtgrnid.Text + " ";
                qur.ExecuteNonQuery();

                //qur.CommandText = "delete from dcitemsmatchlist where headid=" + txtgrnid.Text + " ";
                //qur.ExecuteNonQuery();
                for (int i = 0; i < HFIT.RowCount - 1; i++)
                {
                    conn.Close();
                    conn.Open();
                    SqlParameter[] para1 ={
                        new SqlParameter("@slno", HFIT.Rows[i].Cells[0].Value),
                        new SqlParameter("@iTEMNAME",Convert.ToString(HFIT.Rows[i].Cells[1].Value)),
                        new SqlParameter("@ITEMID", HFIT.Rows[i].Cells[12].Value),
                        new SqlParameter("@COLOR", HFIT.Rows[i].Cells[3].Value),
                        new SqlParameter("@GSM", HFIT.Rows[i].Cells[4].Value),
                        new SqlParameter("@KNITDIA", HFIT.Rows[i].Cells[5].Value),
                        new SqlParameter("@QTY",HFIT.Rows[i].Cells[7].Value),
                        new SqlParameter("@ROLLS", HFIT.Rows[i].Cells[8].Value),
                        new SqlParameter("@RATE", HFIT.Rows[i].Cells[9].Value),
                        new SqlParameter("@TAXID",HFIT.Rows[i].Cells[14].Value),
                        new SqlParameter("@TAXVAL", Convert.ToDecimal(HFIT.Rows[i].Cells[16].Value)),
                        new SqlParameter("@TOTAL",Convert.ToDecimal( HFIT.Rows[i].Cells[11].Value)),
                        new SqlParameter("@HEADID", txtgrnid.Text),
                        new SqlParameter("@typeid",HFIT.Rows[i].Cells[2].Value),
                        new SqlParameter("@iTEMdes",HFIT.Rows[i].Cells[17].Value),
                        new SqlParameter("@refid",HFIT.Rows[i].Cells[18].Value),
                                   new SqlParameter("@uom",HFIT.Rows[i].Cells[6].Value),
                                                  new SqlParameter("@socno",HFIT.Rows[i].Cells[19].Value),
                        new SqlParameter("@workorder",HFIT.Rows[i].Cells[20].Value),
                         new SqlParameter("@style",HFIT.Rows[i].Cells[21].Value),
                    };
                    db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_JOBISSUEDETTAB", para1, conn);
                }
                for (int i = 0; i < RQGR.RowCount - 1; i++)
                {
                    conn.Close();
                    conn.Open();
                    SqlParameter[] para1 ={
                        new SqlParameter("@slno", RQGR.Rows[i].Cells[0].Value),
                        new SqlParameter("@iTEMNAME",RQGR.Rows[i].Cells[2].Value),
                        new SqlParameter("@ITEMID", RQGR.Rows[i].Cells[1].Value),
                        new SqlParameter("@QTY",RQGR.Rows[i].Cells[4].Value),
                        new SqlParameter("@ROLLS", RQGR.Rows[i].Cells[5].Value),
                        new SqlParameter("@HEADID", txtgrnid.Text),
                        
                              new SqlParameter("@workid", RQGR.Rows[i].Cells[7].Value),
                                      new SqlParameter("@uom", RQGR.Rows[i].Cells[3].Value),
                                                     new SqlParameter("@socno",RQGR.Rows[i].Cells[9].Value),
                        new SqlParameter("@workorder",RQGR.Rows[i].Cells[8].Value),
                         new SqlParameter("@style",RQGR.Rows[i].Cells[10].Value),
                    };
                    db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_dcitems", para1, conn);
                }
               
                    if ((HFTP.RowCount - 1) > 1)
                {
                    for (int i = 0; i < HFTP.RowCount - 1; i++)
                    {
                        conn.Close();
                        conn.Open();
                        SqlParameter[] para1 ={
                            new SqlParameter("@slno", HFTP.Rows[i].Cells[0].Value),
                            new SqlParameter("@ITEMID", HFTP.Rows[i].Cells[1].Value),
                            new SqlParameter("@date", HFTP.Rows[i].Cells[2].Value),
                            new SqlParameter("@QTY",HFTP.Rows[i].Cells[3].Value),
                            new SqlParameter("@HEADID", txtgrnid.Text),
                        };
                        db.ExecuteNonQuery(CommandType.StoredProcedure, "sp_reqdateissue", para1, conn);
                    }

                }
                else
                {
                    conn.Close();
                    conn.Open();
                    string quy33 = "select * from reqdt";
                    Genclass.cmd = new SqlCommand(quy33, conn);
                    SqlDataAdapter aptr33 = new SqlDataAdapter(Genclass.cmd);
                    DataTable tap33 = new DataTable();
                    aptr33.Fill(tap33);
                    Genclass.tt = 0;
                    if (tap33.Rows.Count > 0)
                    {
                        conn.Close();
                        conn.Open();
                        string quy34 = "select itemid,qty from JOKNITTINGITEMS where headid=" + txtgrnid.Text + "";
                        Genclass.cmd = new SqlCommand(quy34, conn);
                        SqlDataAdapter aptr34 = new SqlDataAdapter(Genclass.cmd);
                        DataTable tap34 = new DataTable();
                        aptr34.Fill(tap34);
                        for (int p = 0; p < tap34.Rows.Count; p++)
                        {
                            conn.Close();
                            conn.Open();
                            Genclass.tt = Genclass.tt + 1;
                            qur.CommandText = "insert into reqdateissue values (" + Genclass.tt + "," + tap34.Rows[p]["itemid"].ToString() + ",'" + Convert.ToDateTime(reqdt.Text) + "'," + tap34.Rows[p]["qty"].ToString() + "," + txtgrnid.Text + ")";
                            qur.ExecuteNonQuery();
                        }
                    }
                }
            }
            conn.Close();
            conn.Open();
            if (mode == 1)
            {
                qur.CommandText = "update doctypem set lastno=lastno+1 where doctypeid=" + Dtype + " and finyear='19-20'";
                qur.ExecuteNonQuery();
         
            }

            conn.Close();
            conn.Open();
            if (txtgrnid.Text != "")
            {
                if (mode == 1)
                {
                    qur.CommandText = "exec SP_stocklegKnitIssue " + txtgrnid.Text + "," + Dtype + " ,1";
                    qur.ExecuteNonQuery();

                    qur.CommandText = "exec POST_SUPPLIERSTOCK_SP " + txtgrnid.Text + "," + Dtype + " ";
                    qur.ExecuteNonQuery();
                }
                else
                {
                    qur.CommandText = "delete from SUPPLIER_STOCK_LEDGER where hdid= " + txtgrnid.Text + " and doctypeid=" + Dtype + " ";
                    qur.ExecuteNonQuery();
                    qur.CommandText = "exec SP_stocklegKnitIssue " + txtgrnid.Text + "," + Dtype + ",2";
                    qur.ExecuteNonQuery();


                    qur.CommandText = "exec POST_SUPPLIERSTOCK_SP " + txtgrnid.Text + "," + Dtype + " ";
                    qur.ExecuteNonQuery();
                }

            }
            MessageBox.Show("Save Record");
            Editpnl.Visible = false;
            tabC.Visible = false;
            Genpan.Visible = true;
            panadd.Visible = true;
            LoadGetJobCard(1);
        }

        private void butedit_Click(object sender, EventArgs e)
        {
            mode = 2;
            panadd.Visible = false;
            Genpan.Visible = false;
            Editpnl.Visible = true;
            tabC.Visible = true;
            conn.Close();
            conn.Open();


            //cbosGReturnItem.Items.Clear();
            int i = HFGP.SelectedCells[0].RowIndex;
            uid = HFGP.Rows[i].Cells[0].Value.ToString();
            txtgrnid.Text = HFGP.Rows[i].Cells[0].Value.ToString();
            txtgrn.Text = HFGP.Rows[i].Cells[1].Value.ToString();
            dtpgrndt.Text = HFGP.Rows[i].Cells[2].Value.ToString();

            txtname.Text = HFGP.Rows[i].Cells[3].Value.ToString();
            txtqty.Text = HFGP.Rows[i].Cells[4].Value.ToString();
            txtvehicle.Text = HFGP.Rows[i].Cells[5].Value.ToString();

            cboprocess.Text = HFGP.Rows[i].Cells[6].Value.ToString();
            txtpuid.Text = HFGP.Rows[i].Cells[7].Value.ToString();
            txtvehicle.Tag = HFGP.Rows[i].Cells[8].Value.ToString();
            txtnobeams.Text = HFGP.Rows[i].Cells[9].Value.ToString();
            txtworder.Text = HFGP.Rows[i].Cells[10].Value.ToString();
            txtseqno.Text = HFGP.Rows[i].Cells[11].Value.ToString();
            conn.Close();
            conn.Open();
            Docno.Clear();
            if (Docno.Columns.Count == 0)
            {
                Docno.Columns.Add("uid");
                Docno.Columns.Add("Docno");
            }
            label28.Visible = false;
            reqdt.Visible = false;
            button13.Visible = false;
            conn.Close();
            conn.Open();

            //qur.CommandText = "truncate table  jjissuetemp";
            //qur.ExecuteNonQuery();

            qur.CommandText = "truncate table  reqdt";
            qur.ExecuteNonQuery();

            qur.CommandText = "truncate table  reqtempp";
            qur.ExecuteNonQuery();
            serialno.Visible = false;
            Chkedtact.Checked = true;
            HFIT.Refresh();
            HFIT.DataSource = null;
            HFIT.Rows.Clear();
            RQGR.Refresh();
            RQGR.DataSource = null;
            RQGR.Rows.Clear();
            HFGP1.Refresh();
            HFGP1.DataSource = null;
            HFGP1.Rows.Clear();
            HFTP.Refresh();
            HFTP.DataSource = null;
            HFTP.Rows.Clear();
            sum1 = 0;
            sum2 = 0;
            Titlep();
            load();
            Titlep5();
            Titlep3();
            Genclass.strsql = " SP_GETKNITDCEDITLOAD " + txtgrnid.Text + " ";
            Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
            SqlDataAdapter aptr3 = new SqlDataAdapter(Genclass.cmd);
            DataTable tap3 = new DataTable();
            aptr3.Fill(tap3);
            if (tap3.Rows.Count > 0)
            {

                for (int m = 0; m < tap3.Rows.Count; m++)
                {
                    var index = RQGR.Rows.Add();
                    RQGR.Rows[index].Cells[0].Value = tap3.Rows[m]["slno"].ToString();
                    RQGR.Rows[index].Cells[1].Value = tap3.Rows[m]["itemid"].ToString();
                    RQGR.Rows[index].Cells[2].Value = tap3.Rows[m]["itemname"].ToString();
                    RQGR.Rows[index].Cells[3].Value = tap3.Rows[m]["Uom"].ToString();
                    RQGR.Rows[index].Cells[4].Value = tap3.Rows[m]["qty"].ToString();
                    RQGR.Rows[index].Cells[5].Value = tap3.Rows[m]["rolls"].ToString();
                    RQGR.Rows[index].Cells[6].Value = tap3.Rows[m]["uid"].ToString();
                    RQGR.Rows[index].Cells[7].Value = tap3.Rows[m]["workid"].ToString();
                    RQGR.Rows[index].Cells[8].Value = tap3.Rows[m]["socno"].ToString();
                    RQGR.Rows[index].Cells[9].Value = tap3.Rows[m]["workorder"].ToString();
                    RQGR.Rows[index].Cells[10].Value = tap3.Rows[m]["style"].ToString();
                }



            }
            Genclass.strsql = "SP_GETKNITISSUEDATEEDITLOAD " + txtgrnid.Text + "  ";
            Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
            SqlDataAdapter aptr4 = new SqlDataAdapter(Genclass.cmd);
            DataTable tap4 = new DataTable();
            aptr4.Fill(tap4);
            if (tap4.Rows.Count > 0)
            {

                for (int m = 0; m < tap4.Rows.Count; m++)
                {
                    var index = HFTP.Rows.Add();
                    HFTP.Rows[index].Cells[0].Value = tap4.Rows[m]["slno"].ToString();
                    HFTP.Rows[index].Cells[1].Value = tap4.Rows[m]["itemid"].ToString();
                    HFTP.Rows[index].Cells[2].Value = tap4.Rows[m]["date"].ToString();
                    HFTP.Rows[index].Cells[3].Value = tap4.Rows[m]["qty"].ToString();
                    HFTP.Rows[index].Cells[4].Value = tap4.Rows[m]["uid"].ToString();

                }



            }

        }
        protected void FillGrid(DataTable dt, int FillId)
        {
            try
            {
                DataGridCommon.DataSource = null;
                DataGridCommon.AutoGenerateColumns = false;


                DataGridCommon.ColumnCount = 4;
                DataGridCommon.Columns[0].Name = "uid";
                DataGridCommon.Columns[0].HeaderText = "uid";
                DataGridCommon.Columns[0].DataPropertyName = "uid";
                DataGridCommon.Columns[1].Name = "itemspec1";
                DataGridCommon.Columns[1].HeaderText = "Itemname";
                DataGridCommon.Columns[1].DataPropertyName = "itemspec1";
                DataGridCommon.Columns[1].Width = 300;
                DataGridCommon.Columns[2].Name = "UOMID";
                DataGridCommon.Columns[2].HeaderText = "UOMID";
                DataGridCommon.Columns[2].DataPropertyName = "UOMID";

                DataGridCommon.Columns[3].Name = "GENERALNAME";
                DataGridCommon.Columns[3].HeaderText = "GENERALNAME";
                DataGridCommon.Columns[3].DataPropertyName = "GENERALNAME";



                DataGridCommon.DataSource = bsParty;
                DataGridCommon.Columns[0].Visible = false;
                DataGridCommon.Columns[2].Visible = false;
                DataGridCommon.Columns[3].Visible = false;



            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        protected void FillGrid4(DataTable dt, int FillId)
        {
            try
            {
                HFGP2.DataSource = null;
                HFGP2.AutoGenerateColumns = false;


                HFGP2.ColumnCount = 5;
                HFGP2.Columns[0].Name = "uid";
                HFGP2.Columns[0].HeaderText = "uid";
                HFGP2.Columns[0].DataPropertyName = "uid";
                HFGP2.Columns[1].Name = "Itemname";
                HFGP2.Columns[1].HeaderText = "Itemname";
                HFGP2.Columns[1].DataPropertyName = "Itemname";
                HFGP2.Columns[1].Width = 300;

                HFGP2.Columns[2].Name = "qty";
                HFGP2.Columns[2].HeaderText = "qty";
                HFGP2.Columns[2].DataPropertyName = "qty";


                HFGP2.Columns[3].Name = "rolls";
                HFGP2.Columns[3].HeaderText = "rolls";
                HFGP2.Columns[3].DataPropertyName = "rolls";

                HFGP2.Columns[4].Name = "generalname";
                HFGP2.Columns[4].HeaderText = "Uom";
                HFGP2.Columns[4].DataPropertyName = "generalname";


                HFGP2.DataSource = bsc2;
                HFGP2.Columns[0].Visible = false;
                HFGP2.Columns[2].Visible = false;
                HFGP2.Columns[3].Visible = false;
                HFGP2.Columns[4].Visible = false;


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        protected void FillGrid1(DataTable dt, int FillId)
        {
            try
            {
                DataGridCommon.DataSource = null;
                DataGridCommon.AutoGenerateColumns = false;


                DataGridCommon.ColumnCount = 4;
                DataGridCommon.Columns[0].Name = "Uid";
                DataGridCommon.Columns[0].HeaderText = "Uid";
                DataGridCommon.Columns[0].DataPropertyName = "Uid";

                DataGridCommon.Columns[1].Name = "Itemname";
                DataGridCommon.Columns[1].HeaderText = "Itemname";
                DataGridCommon.Columns[1].DataPropertyName = "Itemname";
                DataGridCommon.Columns[1].Width = 300;


                DataGridCommon.Columns[2].Name = "qty";
                DataGridCommon.Columns[2].HeaderText = "qty";
                DataGridCommon.Columns[2].DataPropertyName = "qty";



                DataGridCommon.Columns[3].Name = "rolls";
                DataGridCommon.Columns[3].HeaderText = "rolls";
                DataGridCommon.Columns[3].DataPropertyName = "rolls";


                DataGridCommon.DataSource = bsc;
                DataGridCommon.Columns[0].Visible = false;
                DataGridCommon.Columns[2].Visible = false;
                DataGridCommon.Columns[3].Visible = false;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        protected void FillGrid2(DataTable dt, int FillId)
        {
            try
            {
                DataGridCommon.DataSource = null;
                DataGridCommon.AutoGenerateColumns = false;
                DataGridCommon.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
                DataGridCommon.DefaultCellStyle.WrapMode = DataGridViewTriState.True;

                DataGridCommon.ColumnCount = 2;
                DataGridCommon.Columns[0].Name = "Uid";
                DataGridCommon.Columns[0].HeaderText = "Uid";
                DataGridCommon.Columns[0].DataPropertyName = "Uid";

                DataGridCommon.Columns[1].Name = "Name";
                DataGridCommon.Columns[1].HeaderText = "Name";
                DataGridCommon.Columns[1].DataPropertyName = "Name";
                DataGridCommon.Columns[1].Width = 300;





                DataGridCommon.DataSource = bsp;
                DataGridCommon.Columns[0].Visible = false;



            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        protected void FillGrid3(DataTable dt, int FillId)
        {
            try
            {
                DataGridCommon.DataSource = null;
                DataGridCommon.AutoGenerateColumns = false;

                DataGridCommon.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
                DataGridCommon.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
                DataGridCommon.ColumnCount = 12;
                DataGridCommon.Columns[0].Name = "Uid";
                DataGridCommon.Columns[0].HeaderText = "Uid";
                DataGridCommon.Columns[0].DataPropertyName = "Uid";

                DataGridCommon.Columns[1].Name = "socno";
                DataGridCommon.Columns[1].HeaderText = "socno";
                DataGridCommon.Columns[1].DataPropertyName = "socno";
                DataGridCommon.Columns[1].Width = 150;


                DataGridCommon.Columns[2].Name = "processname";
                DataGridCommon.Columns[2].HeaderText = "processname";
                DataGridCommon.Columns[2].DataPropertyName = "processname";
                DataGridCommon.Columns[2].Visible = false;



                DataGridCommon.Columns[3].Name = "processid";
                DataGridCommon.Columns[3].HeaderText = "processid";
                DataGridCommon.Columns[3].DataPropertyName = "processid";



                DataGridCommon.Columns[4].Name = "generalname";
                DataGridCommon.Columns[4].HeaderText = "generalname";
                DataGridCommon.Columns[4].DataPropertyName = "generalname";



                DataGridCommon.Columns[5].Name = "taxid";
                DataGridCommon.Columns[5].HeaderText = "taxid";
                DataGridCommon.Columns[5].DataPropertyName = "taxid";



                DataGridCommon.Columns[6].Name = "sname";
                DataGridCommon.Columns[6].HeaderText = "sname";
                DataGridCommon.Columns[6].DataPropertyName = "sname";


                DataGridCommon.Columns[7].Name = "seqno";
                DataGridCommon.Columns[7].HeaderText = "seqno";
                DataGridCommon.Columns[7].DataPropertyName = "seqno";


                DataGridCommon.Columns[8].Name = "Style";
                DataGridCommon.Columns[8].HeaderText = "Style";
                DataGridCommon.Columns[8].DataPropertyName = "Style";
                DataGridCommon.Columns[8].Width = 150;

                DataGridCommon.Columns[9].Name = "WorkUid";
                DataGridCommon.Columns[9].HeaderText = "WorkUid";
                DataGridCommon.Columns[9].DataPropertyName = "WorkUid";
                DataGridCommon.Columns[10].Name = "prgrpid";
                DataGridCommon.Columns[10].HeaderText = "prgrpid";
                DataGridCommon.Columns[10].DataPropertyName = "prgrpid";


                DataGridCommon.Columns[11].Name = "docno";
                DataGridCommon.Columns[11].HeaderText = "WOrkOrderNo";
                DataGridCommon.Columns[11].DataPropertyName = "docno";
                DataGridCommon.Columns[11].Width = 100;
                DataGridCommon.DataSource = bsc1;
                DataGridCommon.Columns[0].Visible = false;
                //DataGridCommon.Columns[2].Visible = false;
                DataGridCommon.Columns[3].Visible = false;
                DataGridCommon.Columns[4].Visible = false;
                DataGridCommon.Columns[5].Visible = false;
                DataGridCommon.Columns[6].Visible = false;
                DataGridCommon.Columns[7].Visible = false;
                DataGridCommon.Columns[9].Visible = false;
                DataGridCommon.Columns[10].Visible = false;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        private void load()
        {
            Genclass.strsql = "SP_GETKNITPLANEDITLOAD " + txtgrnid.Text + " ";
            Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
            SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
            DataTable tap1 = new DataTable();
            aptr1.Fill(tap1);

            sum1 = 0;


            for (int k = 0; k < tap1.Rows.Count; k++)
            {
                var index = HFIT.Rows.Add();

                HFIT.Rows[index].Cells[0].Value = tap1.Rows[k]["slno"].ToString();

                HFIT.Rows[index].Cells[1].Value = tap1.Rows[k]["itemname"].ToString();

                HFIT.Rows[index].Cells[2].Value = tap1.Rows[k]["typeid"].ToString();

                HFIT.Rows[index].Cells[3].Value = tap1.Rows[k]["color"].ToString();

                HFIT.Rows[index].Cells[4].Value = tap1.Rows[k]["gsm"].ToString();

                HFIT.Rows[index].Cells[5].Value = tap1.Rows[k]["knitdia"].ToString();


                HFIT.Rows[index].Cells[6].Value = tap1.Rows[k]["Uom"].ToString();
                HFIT.Rows[index].Cells[7].Value = tap1.Rows[k]["qty"].ToString();
                HFIT.Rows[index].Cells[8].Value = tap1.Rows[k]["Rolls"].ToString();
                HFIT.Rows[index].Cells[9].Value = tap1.Rows[k]["Rate"].ToString();
                HFIT.Rows[index].Cells[10].Value = tap1.Rows[k]["Tax"].ToString();
                HFIT.Rows[index].Cells[11].Value = tap1.Rows[k]["Total"].ToString();
                HFIT.Rows[index].Cells[12].Value = tap1.Rows[k]["itemid"].ToString();
                HFIT.Rows[index].Cells[13].Value = tap1.Rows[k]["uom_uid"].ToString();
                HFIT.Rows[index].Cells[14].Value = tap1.Rows[k]["taxid"].ToString();
                HFIT.Rows[index].Cells[15].Value = tap1.Rows[k]["uid"].ToString();
                HFIT.Rows[index].Cells[16].Value = tap1.Rows[k]["taxval"].ToString();
                HFIT.Rows[index].Cells[17].Value = tap1.Rows[k]["itemdes"].ToString();
                HFIT.Rows[index].Cells[18].Value = tap1.Rows[k]["refid"].ToString();
                HFIT.Rows[index].Cells[19].Value = tap1.Rows[k]["socno"].ToString();
                HFIT.Rows[index].Cells[20].Value = tap1.Rows[k]["workorder"].ToString();
                HFIT.Rows[index].Cells[21].Value = tap1.Rows[k]["style"].ToString();
            }
        }
        private Point FindLocation(Control ctrl)
        {
            if (ctrl.Parent is Form)
                return ctrl.Location;
            else
            {
                Point p = FindLocation(ctrl.Parent);
                p.X += ctrl.Location.X;
                p.Y += ctrl.Location.Y;
                return p;
            }
        }

        private void txtvehicle_Click(object sender, EventArgs e)
        {
            type = 4;
            button22.Visible = false;
            DataTable dt = getParty();
            bsc1.DataSource = dt;
            FillGrid3(dt, 1);
            Point loc = FindLocation(txtvehicle);
            grSearch.Location = new Point(loc.X, loc.Y + 10);
            grSearch.Visible = true;
            grSearch.Text = "Name Search";
        }

        private void txtvehicle_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.F10)
                {
                    grSearch.Visible = false;
                }
                else if (e.KeyCode == Keys.F2)
                {
                    SelectId = 1;
                    int Index = DataGridCommon.SelectedCells[0].RowIndex;

                    txtvehicle.Text = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    txtvehicle.Tag = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    cboprocess.Tag = DataGridCommon.Rows[Index].Cells[2].Value.ToString();
                    cboprocess.Text = DataGridCommon.Rows[Index].Cells[3].Value.ToString();
                    cbotax.Text = DataGridCommon.Rows[Index].Cells[4].Value.ToString();
                    cbotax.Tag = DataGridCommon.Rows[Index].Cells[5].Value.ToString();
                    txtbeam.Text = DataGridCommon.Rows[Index].Cells[6].Value.ToString();
                    txtseqno.Text = DataGridCommon.Rows[Index].Cells[7].Value.ToString();
                    txtitemid.Tag = DataGridCommon.Rows[Index].Cells[9].Value.ToString();
                    txtseqno.Tag = DataGridCommon.Rows[Index].Cells[10].Value.ToString();
                    txtnobeams.Focus();
                    grSearch.Visible = false;
                    SelectId = 0;
                }
                else if (e.KeyCode == Keys.Escape)
                {
                    grSearch.Visible = false;
                }
                else if (e.KeyValue == 40)
                {
                    DataGridCommon.Select();
                }
                else if (e.KeyCode == Keys.Enter)
                {
                    txtvehicle_Click(sender, e);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtvehicle_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bsc1.Filter = string.Format("docno LIKE '%{0}%' ", txtvehicle.Text);



                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnadd_Click(object sender, EventArgs e)
        {
            if (txtdcqty.Text == "")
            {
                MessageBox.Show("Enter the Item");
                txtdcqty.Focus();
                return;
            }
            if (cbotype.Text == "")
            {
                MessageBox.Show("Enter the type");
                cbotype.Focus();
                return;
            }

            if (txtitem.Text == "")
            {
                MessageBox.Show("Enter the looplength");
                txtitem.Focus();
                return;
            }

            //if (txtmode.Text == "")
            //{
            //    MessageBox.Show("Enter the GSM");
            //    txtmode.Focus();
            //    return;
            //}
            if (txtoutputitem.Text == "")
            {
                MessageBox.Show("Enter the Dia");
                txtoutputitem.Focus();
                return;
            }
            if (txtisstot.Text == "")
            {
                txtisstot.Text = "0";
            }
            if (txtprice.Text == "")
            {
                MessageBox.Show("Enter the Rate");
                txtprice.Focus();
                return;
            }
            if (txtuom.Text == "")
            {
                MessageBox.Show("Enter the qty");
                txtuom.Focus();
                return;
            }


            if (txtisstot.Text == "")
            {

                txtisstot.Text = "0";

            }
            if (txtprice.Text == "")
            {

                txtprice.Text = "0";

            }
            if (txtisstot.Text == "")
            {

                txtisstot.Text = "0";

            }
            Genclass.name = "";
            string qur1 = "select color FROM WORKORDERYARNcolor WHERE HEADID=" + txtbeam.Tag + " ";
            SqlCommand cmd = new SqlCommand(qur1, conn);
            SqlDataAdapter apt5 = new SqlDataAdapter(cmd);
            DataTable tab5 = new DataTable();
            apt5.Fill(tab5);

            for (int K = 0; K < tab5.Rows.Count; K++)
            {
                if (K == 0)
                {
                    Genclass.name = tab5.Rows[0]["color"].ToString();
                }
                else
                {
                    Genclass.name = Genclass.name + "," + tab5.Rows[K]["color"].ToString();

                }

            }
          

            //Genclass.strsql = "select itemspec1  AS ITEMNAME,uid from itemm     where uid=" + txtitemid.Text + "";
            //Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
            //SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
            //DataTable tap1 = new DataTable();
            //aptr1.Fill(tap1);


            int kk = HFIT.Rows.Count - 1;
            Genclass.h = kk;


        
                Genclass.h = Genclass.h + 1;
                var index = HFIT.Rows.Add();
                HFIT.Rows[index].Cells[0].Value = Genclass.h;
                Genclass.strsql3 = "select  * from processm where processname='" + cboprocess.Text + "'";
                Genclass.cmd = new SqlCommand(Genclass.strsql3, conn);
                SqlDataAdapter aptr3 = new SqlDataAdapter(Genclass.cmd);
                DataTable tap3 = new DataTable();
                aptr3.Fill(tap3);

                if (tap3.Rows.Count > 0)
                {
                    Genclass.Str5 = Convert.ToString(tap3.Rows[0]["processsname"].ToString());

                }
                HFIT.Rows[index].Cells[1].Value = txtdcqty.Text + " "+ Genclass.name +" "  + txtoutputitem.Text + " Dia " + "KNITTED  "  + cbotype.Text ;
                
                HFIT.Rows[index].Cells[2].Value = cbotype.Text;


            

                HFIT.Rows[index].Cells[3].Value = txtitem.Text;
                    HFIT.Rows[index].Cells[4].Value = txtmode.Text;
                HFIT.Rows[index].Cells[5].Value = txtoutputitem.Text;
                HFIT.Rows[index].Cells[6].Value = txtoutoutqty.Text;

                HFIT.Rows[index].Cells[7].Value = txtuom.Text;
                HFIT.Rows[index].Cells[8].Value = txtisstot.Text;
                //txttgstp.Text = tap1.Rows[i]["value"].ToString();
                HFIT.Rows[index].Cells[9].Value = txtprice.Text;
                HFIT.Rows[index].Cells[10].Value = cbotax.Text;
                HFIT.Rows[index].Cells[11].Value = txtbags.Text;
                HFIT.Rows[index].Cells[12].Value = txtitemid.Text;
                HFIT.Rows[index].Cells[13].Value = txtoutoutqty.Tag;
                HFIT.Rows[index].Cells[14].Value = cbotax.SelectedValue;
                HFIT.Rows[index].Cells[15].Value = 0;
                HFIT.Rows[index].Cells[16].Value = txtaddnotes.Text;

                

                    HFIT.Rows[index].Cells[17].Value = txtdcqty.Text;
                
                HFIT.Rows[index].Cells[18].Value = txtbeam.Tag;
            HFIT.Rows[index].Cells[19].Value = textBox7.Text;
            HFIT.Rows[index].Cells[20].Value = textBox5.Text;
            HFIT.Rows[index].Cells[21].Value = textBox8.Text;
            conn.Close();
                conn.Open();
                qur.CommandText = "insert into   jjissuetemp  VALUES (" + txtitemid.Text + "," + txtuom.Text + "," + txtisstot.Text + ") ";
                qur.ExecuteNonQuery();


            textBox8.Text = "";
            txtitem.Text = "";
            txtmode.Text = "";
            txtoutputitem.Text = "";
            txtoutoutqty.Text = "";
            txtbags.Text = "";
            txtuom.Text = "";
            txtisstot.Text = "";
            txtprice.Text = "";
            txtdcqty.Text = "";

            //cbosGReturnItem.Items.Clear();
            //if (HFIT.RowCount > 1)
            //{
            //    Genclass.strsql = "select itemuid,itemname from jjissuetemp ";
            //    Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
            //    SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
            //    DataTable tap = new DataTable();
            //    aptr.Fill(tap);

            //    if (tap.Rows.Count > 0)
            //    {
            //        cbosGReturnItem.DataSource = null;
            //        cbosGReturnItem.DataSource = tap;
            //        cbosGReturnItem.DisplayMember = "itemname";
            //        cbosGReturnItem.ValueMember = "itemuid";
            //        cbosGReturnItem.SelectedIndex = -1;
            //        conn.Close();

            //    }


            //}

            txtdcqty.Focus();
        }

        private void DataGridCommon_DoubleClick(object sender, EventArgs e)
        {
            SelectId = 1;
            int Index = DataGridCommon.SelectedCells[0].RowIndex;

            if (type == 1)
            {
                txtdcqty.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                txtitemid.Text = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                txtoutoutqty.Tag = DataGridCommon.Rows[Index].Cells[2].Value.ToString();
                txtoutoutqty.Text = DataGridCommon.Rows[Index].Cells[3].Value.ToString();
                cbotype.Focus();

            }

            else if (type == 7)
            {
                textBox7.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                textBox7.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                dtpgrndt.Tag = DataGridCommon.Rows[Index].Cells[9].Value.ToString();
                cboprocess.Text = DataGridCommon.Rows[Index].Cells[2].Value.ToString();
                cboprocess.Tag = DataGridCommon.Rows[Index].Cells[3].Value.ToString();
                cbotax.Text = DataGridCommon.Rows[Index].Cells[4].Value.ToString();
                cbotax.Tag = DataGridCommon.Rows[Index].Cells[5].Value.ToString();
                txtbeam.Text = DataGridCommon.Rows[Index].Cells[6].Value.ToString();
                txtseqno.Text = DataGridCommon.Rows[Index].Cells[7].Value.ToString();
                textBox8.Text = DataGridCommon.Rows[Index].Cells[8].Value.ToString();
                txtitemid.Tag = DataGridCommon.Rows[Index].Cells[9].Value.ToString();
                textBox5.Text = DataGridCommon.Rows[Index].Cells[11].Value.ToString();
                Getitems();

            }
            else if (type == 2)
            {
                txtbeam.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                txtbeam.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();

            }
            else if (type == 3)
            {
                txtname.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                txtpuid.Text = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                txtvehicle.Focus();
            }
            else if (type == 4)
            {
                txtvehicle.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                txtvehicle.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                dtpgrndt.Tag = DataGridCommon.Rows[Index].Cells[9].Value.ToString();
                cboprocess.Text = DataGridCommon.Rows[Index].Cells[2].Value.ToString();
                cboprocess.Tag = DataGridCommon.Rows[Index].Cells[3].Value.ToString();
                cbotax.Text = DataGridCommon.Rows[Index].Cells[4].Value.ToString();
                cbotax.Tag = DataGridCommon.Rows[Index].Cells[5].Value.ToString();
                txtbeam.Text = DataGridCommon.Rows[Index].Cells[6].Value.ToString();
                txtseqno.Text = DataGridCommon.Rows[Index].Cells[7].Value.ToString();
                txtwref.Text = DataGridCommon.Rows[Index].Cells[8].Value.ToString();
                txtitemid.Tag = DataGridCommon.Rows[Index].Cells[9].Value.ToString();
                cbowono.Text = DataGridCommon.Rows[Index].Cells[11].Value.ToString();
                loadoutputitem();

            }


            grSearch.Visible = false;
            SelectId = 0;
        }

        private void DataGridCommon_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SelectId = 1;
                int Index = DataGridCommon.SelectedCells[0].RowIndex;

                if (type == 1)
                {
                    txtdcqty.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtitemid.Text = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    txtoutoutqty.Tag = DataGridCommon.Rows[Index].Cells[2].Value.ToString();
                    txtoutoutqty.Text = DataGridCommon.Rows[Index].Cells[3].Value.ToString();
                    cbotype.Focus();
                }
                else if (type == 7)
                {
                    textBox7.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    textBox7.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    dtpgrndt.Tag = DataGridCommon.Rows[Index].Cells[9].Value.ToString();
                    cboprocess.Text = DataGridCommon.Rows[Index].Cells[2].Value.ToString();
                    cboprocess.Tag = DataGridCommon.Rows[Index].Cells[3].Value.ToString();
                    cbotax.Text = DataGridCommon.Rows[Index].Cells[4].Value.ToString();
                    cbotax.Tag = DataGridCommon.Rows[Index].Cells[5].Value.ToString();
                    txtbeam.Text = DataGridCommon.Rows[Index].Cells[6].Value.ToString();
                    txtseqno.Text = DataGridCommon.Rows[Index].Cells[7].Value.ToString();
                    textBox8.Text = DataGridCommon.Rows[Index].Cells[8].Value.ToString();
                    txtitemid.Tag = DataGridCommon.Rows[Index].Cells[9].Value.ToString();
                    textBox5.Text = DataGridCommon.Rows[Index].Cells[11].Value.ToString();
                    Getitems();

                }
                else if (type == 2)
                {
                    txtbeam.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtbeam.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    button4.Focus();
                }
                else if (type == 3)
                {
                    txtname.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtpuid.Text = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    txtvehicle.Focus();
                }
                else if (type == 4)
                {
                    txtvehicle.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtvehicle.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    dtpgrndt.Tag = DataGridCommon.Rows[Index].Cells[9].Value.ToString();
                    cboprocess.Text = DataGridCommon.Rows[Index].Cells[2].Value.ToString();
                    cboprocess.Tag = DataGridCommon.Rows[Index].Cells[3].Value.ToString();
                    cbotax.Text = DataGridCommon.Rows[Index].Cells[4].Value.ToString();
                    cbotax.Tag = DataGridCommon.Rows[Index].Cells[5].Value.ToString();
                    txtbeam.Text = DataGridCommon.Rows[Index].Cells[6].Value.ToString();
                    txtseqno.Text = DataGridCommon.Rows[Index].Cells[7].Value.ToString();
                    txtwref.Text = DataGridCommon.Rows[Index].Cells[8].Value.ToString();
                    txtitemid.Tag = DataGridCommon.Rows[Index].Cells[9].Value.ToString();
                    cbowono.Text = DataGridCommon.Rows[Index].Cells[11].Value.ToString();
                    loadoutputitem();
                }

                grSearch.Visible = false;
                SelectId = 0;
            }
        }

        private void btnHide_Click(object sender, EventArgs e)
        {
            grSearch.Visible = false;
        }

        private void txtoutitem_Click(object sender, EventArgs e)
        {
            lkppnl.Visible = true;
            textBox4.Text = "";
            textBox4.Focus();
            HFGP2.DataSource = null;
            HFGP2.AutoGenerateColumns = false;
            HFGP2.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            HFGP2.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            txtvehicle.Text = string.Empty;
            txtwref.Text = string.Empty;
            cbowono.Text = string.Empty;
            //string qur = " sp_getwo "+ txtpuid.Text + " ";
            //SqlCommand cmd = new SqlCommand(qur, conn);
            //SqlDataAdapter apt = new SqlDataAdapter(cmd);
            //DataTable tab = new DataTable();
            //apt.Fill(tab);
            //cbowono.DataSource = null;
            //cbowono.DataSource = tab;
            //cbowono.DisplayMember = "socno";
            //cbowono.ValueMember = "uid";
            //cbowono.SelectedIndex = -1;

        }

        private void loaditem()

        {

            //lkppnl.Visible = true;
            //textBox4.Text = "";
            //textBox4.Focus();
            //conn.Close();
            //conn.Open();
            //string qur = "select a.Uid,a.docno,isnull(c.processname,'') as processname  from processdet a  left join processdetlist b on a.uid=b.headid  inner join workorderinm cc on a.uid=cc.workid inner join rateappd pp on cc.uid=pp.refid  and cc.itemid=pp.itmid inner join processm c on cc.process=c.processname  and b.processid=c.uid  left join   generalm d on c.taxuid=d.guid where c.uid=5   and pp.status='Approved' and a.socno='" + txtvehicle.Text + "' and c.fabric=1 order by pp.Uid desc";
            //SqlCommand cmd = new SqlCommand(qur, conn);
            //SqlDataAdapter apt = new SqlDataAdapter(cmd);
            //DataTable tab = new DataTable();
            //apt.Fill(tab);
            //cbowono.DataSource = null;
            //cbowono.DataSource = tab;
            //cbowono.DisplayMember = "docno";
            //cbowono.ValueMember = "uid";
            //cbowono.SelectedIndex = -1;
        }
        private void loadoutputitem()
        {
            if (cbowono.Text == "")
            {

                MessageBox.Show("Enter the workorderNo");
                return;
            }


            if (str1key == "F2")
            {
                lkppnl.Visible = true;
                //Genclass.strsql = "select  distinct b.itemid as uid,(b.itemname) as itemname  ,e.uid as uomid,e.generalname,isnull(sum(b.qty),0)-isnull(sum(kk.Qty),0) as PendingQty from jo a inner join joimrec b on a.uid=b.headid   INNER JOIN ITEMM f ON b.itemid = f.UID INNER JOIN GENERALM E ON f.UOM_UID = E.UID inner join itemsmbom k on f.uid = k.itemm_uid    inner join itemm p on k.useditem_uid = p.uid   left join JOBISSUEDETTAB kk on b.itemid=kk.itemid and b.useitemid=kk.refid  where a.remarks =  '" + txtvehicle.Text + "'   and b.useitemid <>k.useditem_uid group by b.itemid,b.itemname ,e.uid ,e.generalname having isnull(sum(b.qty),0)-isnull(sum(kk.Qty),0)>0  ";
                Genclass.strsql = "SP_GETKNITISSUEDCITEMS2  '" + txtitemid.Tag + "'";
                Genclass.FSSQLSortStr = "itemname";
            }
            else
            {
                Genclass.strsql = "SP_GETKNITISSUEDCITEMS1  "+ txtpuid.Text +",'" + txtvehicle.Text + "'";
                Genclass.FSSQLSortStr = "itemname";
            }

            Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
            SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
            DataTable tap = new DataTable();
            aptr.Fill(tap);
            if (tap.Rows.Count == 0)
            {
                string Query = "SP_GETKNITISSUEDCITEMS2 " + txtitemid.Tag + "  ";

                tap = new DataTable();
                tap = db.GetDataWithoutParam(CommandType.Text, Query, conn);
            }
            bsParty.DataSource = tap;

            HFGP2.DataSource = null;
            HFGP2.AutoGenerateColumns = false;
            HFGP2.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            HFGP2.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            HFGP2.ColumnCount = 8;
            HFGP2.Columns[0].Name = "uid";
            HFGP2.Columns[0].HeaderText = "uid";
            HFGP2.Columns[0].DataPropertyName = "uid";
            HFGP2.Columns[1].Name = "itemname";
            HFGP2.Columns[1].HeaderText = "Itemname";
            HFGP2.Columns[1].DataPropertyName = "itemname";
            HFGP2.Columns[1].Width = 500;
            HFGP2.Columns[2].Name = "UOMID";
            HFGP2.Columns[2].HeaderText = "UOMID";
            HFGP2.Columns[2].DataPropertyName = "UOMID";

            HFGP2.Columns[3].Name = "GENERALNAME";
            HFGP2.Columns[3].HeaderText = "GENERALNAME";
            HFGP2.Columns[3].DataPropertyName = "GENERALNAME";


            HFGP2.Columns[4].Name = "STOCKQTY";
            HFGP2.Columns[4].HeaderText = "STOCKQTY";
            HFGP2.Columns[4].DataPropertyName = "STOCKQTY";
            HFGP2.Columns[4].Width = 100;



            HFGP2.Columns[5].Name = "workorder";
            HFGP2.Columns[5].HeaderText = "workorder";
            HFGP2.Columns[5].DataPropertyName = "workorder";
            HFGP2.Columns[5].Width = 100;


            HFGP2.Columns[6].Name = "WORKID";
            HFGP2.Columns[6].HeaderText = "WORKID";
            HFGP2.Columns[6].DataPropertyName = "WORKID";
            HFGP2.Columns[7].Name = "GRNNo";
            HFGP2.Columns[7].HeaderText = "GRNNo";
            HFGP2.Columns[7].DataPropertyName = "GRNNo";

            HFGP2.Columns[7].Width = 100;
            HFGP2.DataSource = bsParty;
            HFGP2.Columns[0].Visible = false;
            HFGP2.Columns[2].Visible = false;
            HFGP2.Columns[3].Visible = false;
            HFGP2.Columns[6].Visible = false;





        }
        private void txtoutitem_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bsc2.Filter = string.Format("Itemname LIKE '%{0}%' ", txtoutitem.Text);



                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtoutitem_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.F10)
                {
                    lkppnl.Visible = false;
                }
                else if (e.KeyCode == Keys.F2)
                {
                    SelectId = 1;
                    int Index = HFGP2.SelectedCells[0].RowIndex;

                    txtoutitem.Text = HFGP2.Rows[Index].Cells[1].Value.ToString();
                    txtoutitem.Tag = HFGP2.Rows[Index].Cells[0].Value.ToString();
                    txtoutqty.Text = HFGP2.Rows[Index].Cells[2].Value.ToString();

                    txtoutuom.Text = HFGP2.Rows[Index].Cells[3].Value.ToString();
                    txtoutqty.Focus();
                    lkppnl.Visible = false;
                    SelectId = 0;
                }
                else if (e.KeyCode == Keys.Escape)
                {
                    lkppnl.Visible = false;
                }
                else if (e.KeyValue == 40)
                {
                    HFGP2.Select();
                }
                else if (e.KeyCode == Keys.Enter)
                {
                    txtoutitem_Click(sender, e);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (txtoutitem.Text == "")
            {
                MessageBox.Show("Enter the Item");
                txtoutitem.Focus();
                return;
            }
            if (txtoutqty.Text == "")
            {
                MessageBox.Show("Enter the Qty");
                txtoutqty.Focus();
                return;
            }
            if (txtoutrolls.Text == "")
            {
                MessageBox.Show("Enter the Rolls");
                txtoutrolls.Focus();
                return;
            }


            //Genclass.strsql = "select itemname + ' - '+ ISNULL(ITEMCODE,'') AS ITEMNAME,uid from itemm  where uid=" + txtoutitem.Tag + "";
            //Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
            //SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
            //DataTable tap1 = new DataTable();
            //aptr1.Fill(tap1);

            int kk = RQGR.Rows.Count - 1;
            Genclass.pp = kk;


         
                Genclass.pp = Genclass.pp + 1;
                var index = RQGR.Rows.Add();
                RQGR.Rows[index].Cells[0].Value = Genclass.pp;
                RQGR.Rows[index].Cells[1].Value = txtoutitem.Tag;

                RQGR.Rows[index].Cells[2].Value = txtoutitem.Text;
                RQGR.Rows[index].Cells[3].Value = txtoutuom.Text;
                RQGR.Rows[index].Cells[4].Value = txtoutqty.Text;

                RQGR.Rows[index].Cells[5].Value = txtoutrolls.Text;
                RQGR.Rows[index].Cells[6].Value = 0;
                RQGR.Rows[index].Cells[7].Value = txtitemid.Tag;
                RQGR.Rows[index].Cells[8].Value = cbowono.Text;
                RQGR.Rows[index].Cells[9].Value = txtvehicle.Text;
            RQGR.Rows[index].Cells[10].Value = txtwref.Text;

            txtoutqty.Text = "";
            txtoutrolls.Text = "";
            txtoutitem.Text = "";
            txtvehicle.Text = "";
            txtoutitem.Focus();
        }

        private void button12_Click(object sender, EventArgs e)
        {
            Genclass.strsql = "select isnull(sum(qty),0) as qty from reqtempp  where itemid=" + cbosGReturnItem.SelectedValue + " and itemid<>0.00";
            Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
            SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
            DataTable tap = new DataTable();
            aptr.Fill(tap);
            if (tap.Rows.Count > 0)
            {

                if (tap.Rows[0]["qty"].ToString() != "0.00")
                {
                    yy = Convert.ToDouble(tap.Rows[0]["qty"].ToString());

                    //double hh = Convert.ToDouble(yy);


                    uu = Convert.ToDouble(txtreqty.Text);

                    if (yy == uu)

                    {
                        MessageBox.Show("Req Date Qty Completed Please Choose another Item");
                        return;

                    }


                }



                int kk = HFTP.Rows.Count - 1;
                Genclass.tt = kk;

                Genclass.strsql = "select itemname + ' - '+ ISNULL(ITEMCODE,'') AS ITEMNAME,uid from itemm  where uid=" + cbosGReturnItem.SelectedValue + "";
                Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
                DataTable tap1 = new DataTable();
                aptr1.Fill(tap1);



                for (int i = 0; i < tap1.Rows.Count; i++)
                {
                    Genclass.tt = Genclass.tt + 1;
                    var index = HFTP.Rows.Add();
                    HFTP.Rows[index].Cells[0].Value = Genclass.tt;
                    HFTP.Rows[index].Cells[1].Value = tap1.Rows[i]["uid"].ToString();

                    HFTP.Rows[index].Cells[2].Value = stpreq.Text;
                    HFTP.Rows[index].Cells[3].Value = txtreqrty.Text;
                    HFTP.Rows[index].Cells[4].Value = 0;

                    conn.Close();
                    conn.Open();

                    qur.CommandText = "insert into   reqtempp  VALUES (" + cbosGReturnItem.SelectedValue + "," + stpreq.Text + "," + txtreqrty.Text + ") ";
                    qur.ExecuteNonQuery();

                }

                stpreq.Text = "";
                txtreqrty.Text = "";
                stpreq.Focus();

            }
        }

        private void dtpgrndt_KeyDown(object sender, KeyEventArgs e)
        {

            if (e.KeyCode == Keys.Enter)
            {
                txtqty.Focus();
            }
        }

        private void txtqty_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtname.Focus();
            }
        }

        private void txtitem_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtmode.Focus();
            }
        }

        private void txtmode_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtoutputitem.Focus();
            }
        }

        private void txtoutputitem_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtuom.Focus();
            }
        }

        private void txtoutoutqty_KeyDown(object sender, KeyEventArgs e)
        {

        }

        private void txtuom_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtisstot.Focus();
            }
        }

        private void txtisstot_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtprice.Focus();
            }
        }

        private void txtprice_KeyDown(object sender, KeyEventArgs e)
        {

        }

        private void cbotax_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (txtuom.Text != "" && txtprice.Text != "" && cbotax.SelectedValue != null)

            {

                double hh = Convert.ToDouble(txtuom.Text) * Convert.ToDouble(txtprice.Text);

                string qur = "select * from generalm where   Typemuid in (8) and active=1  and guid=" + cbotax.SelectedValue + " ";
                SqlCommand cmd = new SqlCommand(qur, conn);
                SqlDataAdapter apt = new SqlDataAdapter(cmd);
                DataTable tab = new DataTable();
                apt.Fill(tab);
                if (tab.Rows.Count > 0)
                {
                    gg = ((hh) * Convert.ToDouble(tab.Rows[0]["f1"].ToString())) / 100;
                    kk = (hh) + gg;
                    txtbags.Text = kk.ToString();
                    txtaddnotes.Text = gg.ToString();

                }
              
            }

        }

        private void txtbags_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnadd_Click(sender, e);
            }
        }

        private void btnadd_KeyDown(object sender, KeyEventArgs e)
        {

        }

        private void txtoutqty_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtoutrolls.Focus();
            }
        }

        private void txtoutrolls_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                button4_Click(sender, e);
            }
        }

        private void stpreq_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtreqrty.Focus();
            }
        }

        private void txtreqrty_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                button12_Click(sender, e);
            }
        }

        private void button15_Click(object sender, EventArgs e)
        {
            lkppnl.Visible = false;
        }

        private void button14_Click(object sender, EventArgs e)
        {
            try
            {
                SelectId = 1;
                int Index = HFGP2.SelectedCells[0].RowIndex;
                txtoutitem.Tag = HFGP2.Rows[Index].Cells[0].Value.ToString();
                txtoutitem.Text = HFGP2.Rows[Index].Cells[1].Value.ToString();
             
                txtoutqty.Text = HFGP2.Rows[Index].Cells[4].Value.ToString();

                txtoutuom.Text = HFGP2.Rows[Index].Cells[3].Value.ToString();
                txtoutuom.Tag = HFGP2.Rows[Index].Cells[6].Value.ToString();
                txtoutqty.Focus();


                lkppnl.Visible = false;
                SelectId = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void HFGP2_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                SelectId = 1;
                int Index = HFGP2.SelectedCells[0].RowIndex;


                txtoutitem.Text = HFGP2.Rows[Index].Cells[1].Value.ToString();
                txtoutitem.Tag = HFGP2.Rows[Index].Cells[0].Value.ToString();
                //txtoutqty.Text = HFGP2.Rows[Index].Cells[2].Value.ToString();
                txtoutqty.Text = HFGP2.Rows[Index].Cells[4].Value.ToString();
                txtoutuom.Text = HFGP2.Rows[Index].Cells[3].Value.ToString();
                txtoutuom.Tag = HFGP2.Rows[Index].Cells[6].Value.ToString();
                txtoutqty.Focus();



                lkppnl.Visible = false;
                SelectId = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void HFGP2_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SelectId = 1;
                int Index = HFGP2.SelectedCells[0].RowIndex;


                txtoutitem.Text = HFGP2.Rows[Index].Cells[1].Value.ToString();
                txtoutitem.Tag = HFGP2.Rows[Index].Cells[0].Value.ToString();
                //txtoutqty.Text = HFGP2.Rows[Index].Cells[2].Value.ToString();
                txtoutqty.Text = HFGP2.Rows[Index].Cells[4].Value.ToString();
                txtoutuom.Text = HFGP2.Rows[Index].Cells[3].Value.ToString();
                txtoutuom.Tag = HFGP2.Rows[Index].Cells[6].Value.ToString();
                txtoutqty.Focus();




                lkppnl.Visible = false;
                SelectId = 0;
            }
        }

        private void cbosGReturnItem_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbosGReturnItem.SelectedValue != null)
            {
                for (int p = 0; p < HFIT.RowCount - 1; p++)
                {
                    string tt = Convert.ToString(cbosGReturnItem.SelectedValue);
                    if (HFIT.Rows[p].Cells[12].Value.ToString() == tt)
                    {
                        txtreqty.Text = HFIT.Rows[p].Cells[7].Value.ToString();

                    }

                }

            }
            stpreq.Focus();
        }

        private void button18_Click(object sender, EventArgs e)
        {
            try
            {
                SelectId = 1;
                int Index = DataGridCommon.SelectedCells[0].RowIndex;
                if (type == 1)
                {
                    txtdcqty.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtitemid.Text = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    txtoutoutqty.Tag = DataGridCommon.Rows[Index].Cells[2].Value.ToString();
                    txtoutoutqty.Text = DataGridCommon.Rows[Index].Cells[3].Value.ToString();
                  
                    cbotype.Focus();
                }
                else if (type == 2)
                {
                    txtbeam.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtbeam.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();

                }
                else if (type == 3)
                {
                    txtname.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtpuid.Text = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    txtvehicle.Focus();

                }
                else if (type == 4)
                {
                    txtvehicle.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtvehicle.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    dtpgrndt.Tag = DataGridCommon.Rows[Index].Cells[9].Value.ToString();
                    cboprocess.Text = DataGridCommon.Rows[Index].Cells[2].Value.ToString();
                    cboprocess.Tag = DataGridCommon.Rows[Index].Cells[3].Value.ToString();
                    cbotax.Text = DataGridCommon.Rows[Index].Cells[4].Value.ToString();
                    cbotax.Tag = DataGridCommon.Rows[Index].Cells[5].Value.ToString();
                    txtbeam.Text = DataGridCommon.Rows[Index].Cells[6].Value.ToString();
                    txtseqno.Text = DataGridCommon.Rows[Index].Cells[7].Value.ToString();
                    txtwref.Text = DataGridCommon.Rows[Index].Cells[8].Value.ToString();
                    txtitemid.Tag = DataGridCommon.Rows[Index].Cells[9].Value.ToString();
                    cbowono.Text = DataGridCommon.Rows[Index].Cells[11].Value.ToString();
                    loadoutputitem();
                }
                else if (type == 7)
                {
                    textBox7.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    textBox7.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    dtpgrndt.Tag = DataGridCommon.Rows[Index].Cells[9].Value.ToString();
                    cboprocess.Text = DataGridCommon.Rows[Index].Cells[2].Value.ToString();
                    cboprocess.Tag = DataGridCommon.Rows[Index].Cells[3].Value.ToString();
                    cbotax.Text = DataGridCommon.Rows[Index].Cells[4].Value.ToString();
                    cbotax.Tag = DataGridCommon.Rows[Index].Cells[5].Value.ToString();
                    txtbeam.Text = DataGridCommon.Rows[Index].Cells[6].Value.ToString();
                    txtseqno.Text = DataGridCommon.Rows[Index].Cells[7].Value.ToString();
                    textBox8.Text = DataGridCommon.Rows[Index].Cells[8].Value.ToString();
                    txtitemid.Tag = DataGridCommon.Rows[Index].Cells[9].Value.ToString();
                    textBox5.Text = DataGridCommon.Rows[Index].Cells[11].Value.ToString();
                    Getitems();

                }

                grSearch.Visible = false;
                SelectId = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtuom_KeyPress(object sender, KeyPressEventArgs e)
        {
            //e.Handled = !char.IsDigit(e.KeyChar);
            //(char.IsDigit(e.KeyChar) == false || e.KeyChar != '\b' || e.KeyChar == Convert.ToChar(Keys.Delete)

            //if (char.IsDigit(e.KeyChar) == false || e.KeyChar == 8 || e.KeyChar == Convert.ToChar(Keys.Delete))
            //{
            //    e.Handled = true;
            //}

            {
                if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && e.KeyChar != '.')
                    e.Handled = true;
            }

        }

        private void txtisstot_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && e.KeyChar != '.')
                e.Handled = true;
        }

        private void txtprice_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && e.KeyChar != '.')
                e.Handled = true;
            if (e.KeyChar == (char)13)
            {
                e.Handled = true;
                SendKeys.Send("{TAB}");
            }

        }

        private void txtbags_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && e.KeyChar != '.')
                e.Handled = true;
        }

        private void txtoutqty_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && e.KeyChar != '.')
                e.Handled = true;
        }

        private void txtoutrolls_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && e.KeyChar != '.')
                e.Handled = true;
        }

        private void txtreqty_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && e.KeyChar != '.')
                e.Handled = true;
        }

        private void txtreqrty_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && e.KeyChar != '.')
                e.Handled = true;
        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox2.Checked == true)
            {
                label28.Visible = true;
                reqdt.Visible = true;
                button13.Visible = true;

            }
            else
            {

                label28.Visible = false;
                reqdt.Visible = false;
                button13.Visible = false;
            }
        }

        private void button13_Click(object sender, EventArgs e)
        {
            if (checkBox2.Checked == true)
            {

                conn.Close();
                conn.Open();

                qur.CommandText = "truncate table reqdt ";
                qur.ExecuteNonQuery();

                qur.CommandText = "insert into reqdt values ('" + reqdt.Text + "')";
                qur.ExecuteNonQuery();

            }
        }

        private void HFIT_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (HFIT.CurrentRow.Cells[0].Value != null || HFIT.CurrentRow.Cells[0].Value.ToString() != "" && HFIT.CurrentRow.Cells[0].Value.ToString() != null && HFIT.CurrentCell.ColumnIndex == 2)
            {
                if (HFIT.CurrentCell.ColumnIndex == 2)

                {
                    DataGridViewCell cell = HFIT.CurrentRow.Cells[2];
                    HFIT.CurrentCell = cell;
                    HFIT.BeginEdit(true);
                }



                if (HFIT.CurrentCell.ColumnIndex == 3)

                {
                    DataGridViewCell cell = HFIT.CurrentRow.Cells[3];
                    HFIT.CurrentCell = cell;
                    HFIT.BeginEdit(true);
                }



                if (HFIT.CurrentCell.ColumnIndex == 4)

                {
                    DataGridViewCell cell = HFIT.CurrentRow.Cells[4];
                    HFIT.CurrentCell = cell;
                    HFIT.BeginEdit(true);
                }


                if (HFIT.CurrentCell.ColumnIndex == 6)

                {
                    DataGridViewCell cell = HFIT.CurrentRow.Cells[6];
                    HFIT.CurrentCell = cell;
                    HFIT.BeginEdit(true);
                }



                if (HFIT.CurrentCell.ColumnIndex == 7)

                {
                    DataGridViewCell cell = HFIT.CurrentRow.Cells[7];
                    HFIT.CurrentCell = cell;
                    HFIT.BeginEdit(true);
                }


                if (HFIT.CurrentCell.ColumnIndex == 8)

                {
                    DataGridViewCell cell = HFIT.CurrentRow.Cells[8];
                    HFIT.CurrentCell = cell;
                    HFIT.BeginEdit(true);
                }

            }
        }

        private void HFIT_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void HFIT_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (HFIT.RowCount > 1)

            {
        gg = 0;
        kk = 0;
        if (HFIT.CurrentCell.ColumnIndex == 1 && HFIT.CurrentRow.Cells[1].Value != null || HFIT.CurrentCell.ColumnIndex == 6 && HFIT.CurrentRow.Cells[6].Value != null || HFIT.CurrentCell.ColumnIndex == 7 && HFIT.CurrentRow.Cells[7].Value != null)
                {
                    if (HFIT.CurrentRow.Cells[6].Value.ToString() != "0")
                    {
                        if (HFIT.CurrentRow.Cells[6].Value.ToString() != "0" || HFIT.CurrentRow.Cells[6].Value.ToString() != "")
                        {
                            conn.Close();
                            conn.Open();

                            qur.CommandText = "update  jjissuetemp  set qty=" + HFIT.CurrentRow.Cells[6].Value.ToString() + "  where itemuid=" + HFIT.CurrentRow.Cells[11].Value.ToString() + " ";
                            qur.ExecuteNonQuery();

                            double hh = Convert.ToDouble(HFIT.CurrentRow.Cells[6].Value.ToString()) * Convert.ToDouble(HFIT.CurrentRow.Cells[8].Value.ToString());
                            string qur1 = "select * from generalm where   Typem_uid in (6) and active=1  and uid=" + HFIT.CurrentRow.Cells[13].Value.ToString() + " ";
                            SqlCommand cmd = new SqlCommand(qur1, conn);
                            SqlDataAdapter apt = new SqlDataAdapter(cmd);
                            DataTable tab = new DataTable();
                            apt.Fill(tab);
                            if (tab.Rows.Count > 0)
                            {
                                gg = ((hh) * Convert.ToDouble(tab.Rows[0]["f1"].ToString())) / 100;
                                kk = (hh) + gg;
                                txtbags.Text = kk.ToString();
                                HFIT.CurrentRow.Cells[10].Value = txtbags.Text;
                                txtaddnotes.Text = gg.ToString();

                            }


                        }


                    }
                    else if (HFIT.CurrentRow.Cells[7].Value.ToString() != "0")
                    {
                        if (HFIT.CurrentRow.Cells[7].Value.ToString() != "0" || HFIT.CurrentRow.Cells[7].Value.ToString() != "")
                        {
                            conn.Close();
                            conn.Open();

                            qur.CommandText = "update  jjissuetemp  set  rolls=" + HFIT.CurrentRow.Cells[7].Value.ToString() + "  where itemuid=" + HFIT.CurrentRow.Cells[11].Value.ToString() + "  ";
                            qur.ExecuteNonQuery();


                        }


                    }
                    else if (HFIT.CurrentRow.Cells[8].Value.ToString() != "0")
                    {
                        if (HFIT.CurrentRow.Cells[8].Value.ToString() != "0" || HFIT.CurrentRow.Cells[8].Value.ToString() != "")
                        {
                            conn.Close();
                            conn.Open();

                            double hh = Convert.ToDouble(HFIT.CurrentRow.Cells[6].Value.ToString()) * Convert.ToDouble(HFIT.CurrentRow.Cells[8].Value.ToString());
                            string qur1 = "select * from generalm where   Typem_uid in (6) and active=1  and uid=" + HFIT.CurrentRow.Cells[13].Value.ToString() + " ";
                            SqlCommand cmd = new SqlCommand(qur1, conn);
                            SqlDataAdapter apt = new SqlDataAdapter(cmd);
                            DataTable tab = new DataTable();
                            apt.Fill(tab);
                            if (tab.Rows.Count > 0)
                            {
                                gg = ((hh) * Convert.ToDouble(tab.Rows[0]["f1"].ToString())) / 100;
                                kk = (hh) + gg;
                                txtbags.Text = kk.ToString();
                                HFIT.CurrentRow.Cells[10].Value = txtbags.Text;
                                txtaddnotes.Text = gg.ToString();

                            }


                        }


                    }

                }
            }
        }

        private void txtuom_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtitem_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar) || !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar))
                e.Handled = true;
        }

        private void HFIT_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                conn.Close();
                conn.Open();

                qur.CommandText = "delete from   jjissuetemp    where itemuid=" + HFIT.CurrentRow.Cells[11].Value.ToString() + "  ";
                qur.ExecuteNonQuery();

                int hh;
                hh = (HFIT.Rows.Count - 1);
                Genclass.h = (hh - 1);
            }


        }

        private void RQGR_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                conn.Close();
                conn.Open();


                int hh;
                hh = (RQGR.Rows.Count - 1);
                Genclass.pp = (hh - 1);
            }
        }

        private void txtnobeams_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                textBox3.Focus();
            }
        }

        private void txtqty_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox3_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtdcqty.Focus();
            }
        }

        private void cbotype_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtitem.Focus();
            }
        }

        private void txtprice_TextChanged(object sender, EventArgs e)
        {
            if (txtprice.Text != "")
            {
                if (txtuom.Text != "" && txtprice.Text != "" && cbotax.SelectedValue != null)

                {
            gg = 0;
            kk = 0;
            hh = 0;

                     hh = Convert.ToDouble(txtuom.Text) * Convert.ToDouble(txtprice.Text);

                    string qur = "select * from generalm where   Typem_uid in (6) and active=1  and uid=" + cbotax.SelectedValue + " ";
                    SqlCommand cmd = new SqlCommand(qur, conn);
                    SqlDataAdapter apt = new SqlDataAdapter(cmd);
                    DataTable tab = new DataTable();
                    apt.Fill(tab);
                    if (tab.Rows.Count > 0)
                    {
                         gg = ((hh) * Convert.ToDouble(tab.Rows[0]["f1"].ToString())) / 100;
                         kk = (hh) + gg;
                        txtbags.Text = kk.ToString();
                        txtaddnotes.Text = gg.ToString();

                    }
                }
            }
        }

        private void tabC_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tabC.SelectedTab == tabPage2)
            {
                txtoutitem.Focus();
            }
            else if (tabC.SelectedTab == tabPage3)
            {
                cbosGReturnItem.Focus();
            }
            else if (tabC.SelectedTab == tabPage1)
            {

                txtdcqty.Focus();
            }
        }

        private void cbotax_TextChanged(object sender, EventArgs e)
        {

        }

        private void tabPage2_Click(object sender, EventArgs e)
        {

        }

        private void cbotax_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (HFGP.Rows.Count == 1)
            {
                MessageBox.Show("No Record");
                return;
            }
            Genclass.Dtype = 540;
            conn.Close();
            conn.Open();
            Genclass.Prtid = Convert.ToInt16(HFGP.CurrentRow.Cells[0].Value.ToString());
            SqlDataAdapter da = new SqlDataAdapter("SP_GETISSURPTre", conn);
            da.SelectCommand.CommandType = CommandType.StoredProcedure;
            da.SelectCommand.Parameters.Add("@uid", SqlDbType.Int).Value = Genclass.Prtid;

            DataSet ds = new DataSet();
            da.Fill(ds, "PRINT");

            doc.Load(Application.StartupPath + "\\JobOrderIssuePrtHalfPg.rpt");
            SqlDataAdapter da1 = new SqlDataAdapter("SP_GETDCITEMSRPT", conn);
            da1.SelectCommand.CommandType = CommandType.StoredProcedure;
            da1.SelectCommand.Parameters.Add("@uid", SqlDbType.Int).Value = Genclass.Prtid;
            DataTable ds1 = new DataTable("Subrpt");
            da1.Fill(ds1);
            ds.Tables.Add(ds1);




            doc.Subreports["NewDC.rpt"].SetDataSource(ds1);

            doc.SetDataSource(ds);

            doc.PrintToPrinter(1, false, 0, 0);
        }

        private void button11_Click(object sender, EventArgs e)
        {
            conn.Close();
            conn.Open();
            Genclass.Prtid = Convert.ToInt16(HFGP.CurrentRow.Cells[0].Value.ToString());


            Genclass.slno = 1;
            Crviewer crv = new Crviewer();
            crv.Show();



            conn.Close();
        }

        private void label33_Click(object sender, EventArgs e)
        {

        }

        private void button16_Click(object sender, EventArgs e)
        {
            SelectId = 1;
            int Index = HFG9.SelectedCells[0].RowIndex;

            

            txtdcqty.Text = HFG9.Rows[Index].Cells[1].Value.ToString();
            txtitemid.Text = HFG9.Rows[Index].Cells[0].Value.ToString();
            txtoutoutqty.Tag = HFG9.Rows[Index].Cells[2].Value.ToString();
            txtoutoutqty.Text = HFG9.Rows[Index].Cells[3].Value.ToString();
            //txtuom.Text = HFG9.Rows[Index].Cells[4].Value.ToString();
            txtuom.Tag = HFG9.Rows[Index].Cells[5].Value.ToString();
            txtbeam.Text = HFG9.Rows[Index].Cells[6].Value.ToString();
            txtbeam.Tag = HFG9.Rows[Index].Cells[7].Value.ToString();
            txtprice.Text = HFG9.Rows[Index].Cells[9].Value.ToString();
            cbotype.Focus();
            panel4.Visible = false;
        }

        private void button17_Click(object sender, EventArgs e)
        {
            panel4.Visible = false;
        }

        private void HFG9_KeyDown(object sender, KeyEventArgs e)
        {

            if (e.KeyCode == Keys.Enter)

            {
                SelectId = 1;
                int Index = HFG9.SelectedCells[0].RowIndex;

                txtdcqty.Text = HFG9.Rows[Index].Cells[1].Value.ToString();
                txtitemid.Text = HFG9.Rows[Index].Cells[0].Value.ToString();
                txtoutoutqty.Tag = HFG9.Rows[Index].Cells[2].Value.ToString();
                txtoutoutqty.Text = HFG9.Rows[Index].Cells[3].Value.ToString();
                //txtuom.Text = HFG9.Rows[Index].Cells[4].Value.ToString();
                txtuom.Tag = HFG9.Rows[Index].Cells[5].Value.ToString();
                txtbeam.Text = HFG9.Rows[Index].Cells[6].Value.ToString();
                txtbeam.Tag = HFG9.Rows[Index].Cells[7].Value.ToString();
                txtprice.Text = HFG9.Rows[Index].Cells[9].Value.ToString();
                cbotype.Focus();
                panel4.Visible = false;
            }


        }

        private void HFG9_DoubleClick(object sender, EventArgs e)
        {
            SelectId = 1;
            int Index = HFG9.SelectedCells[0].RowIndex;


            txtdcqty.Text = HFG9.Rows[Index].Cells[1].Value.ToString();
            txtitemid.Text = HFG9.Rows[Index].Cells[0].Value.ToString();
            txtoutoutqty.Tag = HFG9.Rows[Index].Cells[2].Value.ToString();
            txtoutoutqty.Text = HFG9.Rows[Index].Cells[3].Value.ToString();
            //txtuom.Text = HFG9.Rows[Index].Cells[4].Value.ToString();
            txtuom.Tag = HFG9.Rows[Index].Cells[5].Value.ToString();
            txtbeam.Text = HFG9.Rows[Index].Cells[6].Value.ToString();
            txtbeam.Tag = HFG9.Rows[Index].Cells[7].Value.ToString();
            txtprice.Text = HFG9.Rows[Index].Cells[9].Value.ToString();
            cbotype.Focus();
            panel4.Visible = false;
        }

        private void DataGridCommon_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void txtscr11_TextChanged(object sender, EventArgs e)
        {
            bsParty.Filter = string.Format("itemname LIKE '%{0}%' ", txtscr11.Text);
        }

        private void txtscr11_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.F10)
                {
                    panel4.Visible = false;
                }
                else if (e.KeyCode == Keys.F2)
                {
                    SelectId = 1;


                    int Index = HFG9.SelectedCells[0].RowIndex;
                    txtdcqty.Text = HFG9.Rows[Index].Cells[1].Value.ToString();
                    txtitemid.Text = HFG9.Rows[Index].Cells[0].Value.ToString();
                    txtoutoutqty.Tag = HFG9.Rows[Index].Cells[2].Value.ToString();
                    txtoutoutqty.Text = HFG9.Rows[Index].Cells[3].Value.ToString();
                    txtuom.Tag = HFG9.Rows[Index].Cells[5].Value.ToString();
                    //txtuom.Text = HFG9.Rows[Index].Cells[4].Value.ToString();
                    cbotype.Focus();
                    panel4.Visible = false;
                    txtaddnotes.Focus();

                    SelectId = 0;
                }
                else if (e.KeyCode == Keys.Escape)
                {
                    panel4.Visible = false;
                }
                else if (e.KeyValue == 40)
                {
                    HFG9.Select();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void txtoutputitem_TextChanged(object sender, EventArgs e)
        {

        }

        private void label28_Click(object sender, EventArgs e)
        {

        }

        private void reqdt_ValueChanged(object sender, EventArgs e)
        {

        }

        private void Genpan_Paint(object sender, PaintEventArgs e)
        {

        }

        private void txtscr6_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox4_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.F10)
                {
                    lkppnl.Visible = false;
                }
                else if (e.KeyCode == Keys.F2)
                {
                    if (HFGP2.Rows.Count > 1)
                    {
                        int Index = HFGP2.SelectedCells[0].RowIndex;

                        txtoutitem.Text = HFGP2.Rows[Index].Cells[1].Value.ToString();
                        txtoutitem.Tag = HFGP2.Rows[Index].Cells[0].Value.ToString();
                        txtoutqty.Text = HFGP2.Rows[Index].Cells[4].Value.ToString();

                        txtoutuom.Text = HFGP2.Rows[Index].Cells[3].Value.ToString();
                        txtoutqty.Focus();


                        lkppnl.Visible = false;
                        SelectId = 0;
                    }
                }
                else if (e.KeyCode == Keys.Escape)
                {
                    lkppnl.Visible = false;
                }
                else if (e.KeyValue == 40)
                {
                    HFGP2.Select();
                }
                else if (e.KeyCode == Keys.Escape)
                {
                    lkppnl.Visible = false;
                    return;

                }
                txtoutitem_Click(sender, e);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            
        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {
            bsParty.Filter = string.Format("Itemname LIKE '%{0}%' ", textBox4.Text);
        }

        private void BtnSelect_Click(object sender, EventArgs e)
        {

        }

        private void DataGridCommonNew_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {

        }

        private void button10_Click(object sender, EventArgs e)
        {

        }

        private void Editpnl_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button22_Click(object sender, EventArgs e)
        {
            GeneralParametrs.MenyKey = 7;
            FrmParty contc = new FrmParty();
            contc.FormBorderStyle = FormBorderStyle.None;
            contc.StartPosition = FormStartPosition.Manual;
            contc.Location = new System.Drawing.Point(200, 80);
        }

        private void button14_Click_1(object sender, EventArgs e)
        {
            button14_Click(sender, e);
        }

        private void button4_Click_1(object sender, EventArgs e)
        {
            button4_Click(sender, e);
        }

        private void button15_Click_1(object sender, EventArgs e)
        {
            button15_Click(sender, e);
        }

        private void txtoutitem_TextChanged_1(object sender, EventArgs e)
        {
            txtoutitem_TextChanged(sender, e);
        }

        private void textBox4_TextChanged_1(object sender, EventArgs e)
        {
            textBox4_TextChanged(sender, e);
        }

        private void HFGP2_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void HFGP2_DoubleClick_1(object sender, EventArgs e)
        {
            HFGP2_DoubleClick(sender, e);
        }

        private void HFGP2_KeyDown_1(object sender, KeyEventArgs e)
        {
            HFGP2_KeyDown(sender, e);
        }

        private void btnadd_Click_1(object sender, EventArgs e)
        {
            btnadd_Click(sender, e);
        }

        private void button17_Click_1(object sender, EventArgs e)
        {
            button17_Click(sender, e);

        }

        private void button16_Click_1(object sender, EventArgs e)
        {
            button16_Click(sender, e);
        }

        private void txtdcqty_TextChanged_1(object sender, EventArgs e)
        {
            txtdcqty_TextChanged(sender, e);
        }

        private void txtdcqty_KeyDown_1(object sender, KeyEventArgs e)
        {
            txtdcqty_KeyDown(sender, e);
        }

        private void txtdcqty_Click_1(object sender, EventArgs e)
        {
            txtdcqty_Click(sender, e);
        }

        private void cbotax_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            cbotax_SelectedIndexChanged(sender, e);
        }

        private void HFG9_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void HFG9_DoubleClick_1(object sender, EventArgs e)
        {
            HFG9_DoubleClick(sender, e);
        }

        private void HFG9_KeyDown_1(object sender, KeyEventArgs e)
        {
            HFG9_KeyDown(sender, e);
        }

        private void txtoutitem_Click_1(object sender, EventArgs e)
        {
            txtoutitem_Click(sender, e);
        }

        private void txtoutitem_KeyDown_1(object sender, KeyEventArgs e)
        {
            txtoutitem_KeyDown(sender, e);
        }

        private void txtscr1_TextChanged(object sender, EventArgs e)
        {
            bs.Filter = string.Format("DOcno Like '%{0}%'  or  Docdate Like '%{1}%' or  name Like '%{1}%' or  Reference Like '%{1}%' or  WorkOrderNo Like '%{1}%'  or  ProcessName Like '%{1}%'  or  vehicleno Like '%{1}%' ", txtscr1.Text, txtscr1.Text, txtscr1.Text, txtscr1.Text, txtscr1.Text, txtscr1.Text, txtscr1.Text);
        }

        private void chkact_CheckedChanged(object sender, EventArgs e)
        {
            LoadGetJobCard(1);
        }

        private void butexit_Click(object sender, EventArgs e)
        {
            conn.Close();
            conn.Open();
            int i = HFGP.SelectedCells[0].RowIndex;

            Genclass.strsql = "SP_CHECKKNITTINGISSUE  " + HFGP.Rows[i].Cells[0].Value.ToString() + "";
            Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
            SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
            DataTable tap1 = new DataTable();
            aptr1.Fill(tap1);
            if (tap1.Rows.Count == 0)
            {

      

                qur.CommandText = " EXEC SP_CHECKDELETEKNITTINGISSUE  " + HFGP.Rows[i].Cells[0].Value.ToString() + "";
                qur.ExecuteNonQuery();

                
            }
            else

            {
                MessageBox.Show("Already Converted GRN");
                return;
            }
            LoadGetJobCard(1);
        }

        private void cbowono_SelectedIndexChanged(object sender, EventArgs e)
        {
           
        }

        private void CBOWNO_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (CBOWNO.Text != "" && CBOWNO.SelectedValue != null && CBOWNO.ValueMember != "" && CBOWNO.Text != null && CBOWNO.DisplayMember != "")
            {
                string qur = "select a.Uid,a.docno,isnull(c.processname,'') as processname  from processdet a  left join processdetlist b on a.uid=b.headid  inner join workorderinm cc on a.uid=cc.workid inner join rateappd pp on cc.uid=pp.refid  and cc.itemid=pp.itmid inner join processm c on cc.process=c.processname  and b.processid=c.uid  left join   generalm d on c.taxuid=d.guid where c.uid=5   and pp.status='Approved' and a.socno='" + CBOWNO.Text + "' and c.fabric=1 order by pp.Uid desc";
                SqlCommand cmd = new SqlCommand(qur, conn);
                SqlDataAdapter apt = new SqlDataAdapter(cmd);
                DataTable tab = new DataTable();
                apt.Fill(tab);
                if (tab.Rows.Count > 0)
                {

                    textBox5.Text = tab.Rows[0]["docno"].ToString();

                    textBox5.Tag = tab.Rows[0]["Uid"].ToString();
                    Getitems();
                }


                
            }
        }

        private void textBox5_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bsc1.Filter = string.Format("docno LIKE '%{0}%' ", textBox5.Text);



                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void cbowono_TextChanged(object sender, EventArgs e)
        {
            if (cbowono.Text != "" &&   cbowono.Text != null )

            {
                string qur = "select a.Uid,a.docno,isnull(c.processname,'') as processname  from processdet a  left join processdetlist b on a.uid=b.headid  inner join workorderinm cc on a.uid=cc.workid inner join rateappd pp on cc.uid=pp.refid  and cc.itemid=pp.itmid inner join processm c on cc.process=c.processname  and b.processid=c.uid  left join   generalm d on c.taxuid=d.guid where c.uid=5   and pp.status='Approved' and a.socno='" + cbowono.Text + "' and c.fabric=1 order by pp.Uid desc";
                SqlCommand cmd = new SqlCommand(qur, conn);
                SqlDataAdapter apt = new SqlDataAdapter(cmd);
                DataTable tab = new DataTable();
                apt.Fill(tab);
                if (tab.Rows.Count > 0)
                {

                    txtvehicle.Text = tab.Rows[0]["docno"].ToString();

                    txtvehicle.Tag = tab.Rows[0]["Uid"].ToString();
                    loadoutputitem();
                }

            }
        }

        private void textBox7_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bsc1.Filter = string.Format("docno LIKE '%{0}%' ", textBox7.Text);



                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void textBox7_Click(object sender, EventArgs e)
        {
            type = 7;
            button22.Visible = false;
            DataTable dt = getParty();
            bsc1.DataSource = dt;
            FillGrid3(dt, 1);
            Point loc = FindLocation(textBox7);
            grSearch.Location = new Point(loc.X, loc.Y + 10);
            grSearch.Visible = true;
            grSearch.Text = "Name Search";
        }

        private void txtwref_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
