﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Drawing.Imaging;
using System.Configuration;
using System.Data.SqlClient;
using System.Diagnostics;
using excel = Microsoft.Office.Interop.Excel;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;

namespace MyEasyBizAPMS
{
    public partial class FrmCon : Form
    {
        public FrmCon()
        {
            this.BackColor = Color.White; ;
            InitializeComponent();
        }
        SqlConnection conn = new SqlConnection(GeneralParameters.ConnectionString);
        SqlCommand qur = new SqlCommand();
        SQLDBHelper db = new SQLDBHelper();
        BindingSource bs = new BindingSource();
        BindingSource bsc = new BindingSource();
        private void FrmCon_Load(object sender, EventArgs e)
        {
            contracttype();
            optype();
            HFIT.RowHeadersVisible = false;
            HFIT.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            HFIT.EnableHeadersVisualStyles = false;
            HFIT.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.GradientActiveCaption; HFIT.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            this.HFIT.DefaultCellStyle.Font = new Font("calibri", 10);
            this.HFIT.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 10, FontStyle.Bold);
            RQGR.RowHeadersVisible = false;
            RQGR.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            RQGR.EnableHeadersVisualStyles = false;
            RQGR.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.GradientActiveCaption; HFIT.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            this.RQGR.DefaultCellStyle.Font = new Font("calibri", 10);
            this.RQGR.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 10, FontStyle.Bold);
            LoadGetJobCard(1);
            LoadGetJobCard1(1);
            contracttype();
            optype();

        }

        public void contracttype()
        {
            conn.Open();
            string qur = "select Generalname as tax,guid uid from generalm where   Typemuid in (37) and active=1 ";
            SqlCommand cmd = new SqlCommand(qur, conn);
            SqlDataAdapter apt = new SqlDataAdapter(cmd);
            DataTable tab = new DataTable();
            apt.Fill(tab);
            cbopro1.DataSource = null;
            cbopro1.DataSource = tab;
            cbopro1.DisplayMember = "tax";
            cbopro1.ValueMember = "uid";
            cbopro1.SelectedIndex = -1;
            conn.Close();
        }
        public void optype()
        {
            conn.Open();
            string qur = "select Generalname as tax,guid uid from generalm where   Typemuid in (38) and active=1 ";
            SqlCommand cmd = new SqlCommand(qur, conn);
            SqlDataAdapter apt = new SqlDataAdapter(cmd);
            DataTable tab = new DataTable();
            apt.Fill(tab);
            cbouom.DataSource = null;
            cbouom.DataSource = tab;
            cbouom.DisplayMember = "tax";
            cbouom.ValueMember = "uid";
            cbouom.SelectedIndex = -1;
            conn.Close();
        }

        private void button11_Click(object sender, EventArgs e)
        {


           


            int pp1; int sp;
            if (cboty.Text == "Active")
            {
                pp1 = 1;
            }
            else
            {

                pp1 = 0;
            }

 

            if(txtdcqty.Tag==null)
            {

                sp = 0;
            }
            else

            {
                sp = Convert.ToInt32(txtdcqty.Tag);

            }
            SqlParameter[] para1 ={


                    new SqlParameter("@uid", sp),
                    new SqlParameter("@contractnameid",cbocon.SelectedValue),
                    new SqlParameter("@workername",txtdcqty.Text),
                    new SqlParameter("@operatortypeid",cbouom.SelectedValue),

                    new SqlParameter("@activestatus", pp1)
                         };
            db.ExecuteNonQuery(CommandType.StoredProcedure, "sp_ContractWorker", para1, conn);
            txtdcqty.Text = String.Empty;

        

            LoadGetJobCard1(1);
        }

        protected DataTable LoadGetJobCard(int tag)
        {

           

            DataTable dt = new DataTable();
            try
            {

                SqlParameter[] para = {
                    new SqlParameter("@active","1"),
                    


                };

                dt = db.GetData(CommandType.StoredProcedure, "sp_GetContractorM", para);


                LoadDataTable(dt);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        protected void LoadDataTable(DataTable dt)
        {
            try
            {
                RQGR.DataSource = null;
                RQGR.AutoGenerateColumns = false;
                RQGR.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
                RQGR.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
                RQGR.ColumnCount = 6;

                RQGR.Columns[0].Name = "Uid";
                RQGR.Columns[0].HeaderText = "Uid";
                RQGR.Columns[0].DataPropertyName = "Uid";

                RQGR.Columns[1].Name = "ContractType";
                RQGR.Columns[1].HeaderText = "ContractType";
                RQGR.Columns[1].DataPropertyName = "ContractType";

                RQGR.Columns[2].Name = "ContractName";
                RQGR.Columns[2].HeaderText = "ContractName";
                RQGR.Columns[2].DataPropertyName = "ContractName";

                RQGR.Columns[3].Name = "MobNo";
                RQGR.Columns[3].HeaderText = "MobNo";
                RQGR.Columns[3].DataPropertyName = "MobNo";

                RQGR.Columns[4].Name = "Status";
                RQGR.Columns[4].HeaderText = "Status";
                RQGR.Columns[4].DataPropertyName = "Status";

                RQGR.Columns[5].Name = "contracttypeid";
                RQGR.Columns[5].HeaderText = "contracttypeid";
                RQGR.Columns[5].DataPropertyName = "contracttypeid";

               
          



                bs.DataSource = dt;

                RQGR.DataSource = bs;


                RQGR.Columns[0].Visible = false;
                RQGR.Columns[1].Width = 200;
                RQGR.Columns[2].Width = 300;
                RQGR.Columns[3].Width = 100;

                RQGR.Columns[4].Width = 150;
   
               
                RQGR.Columns[5].Visible = false;



            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected DataTable LoadGetJobCard1(int tag)
        {



            DataTable dt = new DataTable();
            try
            {

                SqlParameter[] para = {
                    new SqlParameter("@active","1"),



                };

                dt = db.GetData(CommandType.StoredProcedure, "sp_GetContractWorker", para);


                LoadDataTable1(dt);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        protected void LoadDataTable1(DataTable dt)
        {
            try
            {
                HFIT.DataSource = null;
                HFIT.AutoGenerateColumns = false;
                HFIT.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
                HFIT.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
                HFIT.ColumnCount = 7;

                HFIT.Columns[0].Name = "Uid";
                HFIT.Columns[0].HeaderText = "Uid";
                HFIT.Columns[0].DataPropertyName = "Uid";

                HFIT.Columns[1].Name = "ContractName";
                HFIT.Columns[1].HeaderText = "ContractName";
                HFIT.Columns[1].DataPropertyName = "ContractName";

                HFIT.Columns[2].Name = "WorkerName";
                HFIT.Columns[2].HeaderText = "WorkerName";
                HFIT.Columns[2].DataPropertyName = "WorkerName";

                HFIT.Columns[3].Name = "OperatorType";
                HFIT.Columns[3].HeaderText = "OperatorType";
                HFIT.Columns[3].DataPropertyName = "OperatorType";

                HFIT.Columns[4].Name = "Status";
                HFIT.Columns[4].HeaderText = "Status";
                HFIT.Columns[4].DataPropertyName = "Status";

                HFIT.Columns[5].Name = "contractnameid";
                HFIT.Columns[5].HeaderText = "contractnameid";
                HFIT.Columns[5].DataPropertyName = "contractnameid";


                HFIT.Columns[6].Name = "operatortypeid";
                HFIT.Columns[6].HeaderText = "operatortypeid";
                HFIT.Columns[6].DataPropertyName = "operatortypeid";



                bsc.DataSource = dt;

                HFIT.DataSource = bsc;


                HFIT.Columns[0].Visible = false;
                HFIT.Columns[1].Width = 300;
                HFIT.Columns[2].Width = 300;
                HFIT.Columns[3].Width = 120;

                HFIT.Columns[4].Width = 150;


                HFIT.Columns[5].Visible = false;
                HFIT.Columns[6].Visible = false;


            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void button17_Click(object sender, EventArgs e)
        {
            int pp;
            if (cbost.Text == "Active")
            {
                pp = 1;
            }
            else
            {

                pp = 0;
            }

            int sp1;


            if (txtoutitem.Tag == null)
            {

                sp1 = 0;
            }
            else

            {
                sp1 = Convert.ToInt32(txtoutitem.Tag);

            }
            SqlParameter[] para ={


                    new SqlParameter("@uid",sp1),
                    new SqlParameter("@contracttypeid",cbopro1.SelectedValue),
                    new SqlParameter("@contractname",txtoutitem.Text),
                    new SqlParameter("@mobno",txtoutqty.Text),

                    new SqlParameter("@activestatus", pp)
                         };
            db.ExecuteNonQuery(CommandType.StoredProcedure, "sp_ContractorM", para, conn);

            txtoutitem.Text = string.Empty;
            txtoutqty.Text=string.Empty;
            LoadGetJobCard(1);

         
            string qur = "select distinct uid,contractname from contractorm  where activestatus=1 ";
            SqlCommand cmd = new SqlCommand(qur, conn);
            SqlDataAdapter apt = new SqlDataAdapter(cmd);
            DataTable tab = new DataTable();
            apt.Fill(tab);
            cbocon.DataSource = null;
            cbocon.DataSource = tab;
            cbocon.DisplayMember = "contractname";
            cbocon.ValueMember = "uid";
            cbocon.SelectedIndex = -1;
            
        

    }

        private void RQGR_DoubleClick(object sender, EventArgs e)
        {
            int index3 = RQGR.SelectedCells[0].RowIndex;
            txtoutitem.Tag = RQGR.Rows[index3].Cells[0].Value.ToString();
            cbopro1.Text = RQGR.Rows[index3].Cells[1].Value.ToString();
            txtoutitem.Text = RQGR.Rows[index3].Cells[2].Value.ToString();
            txtoutqty.Text = RQGR.Rows[index3].Cells[3].Value.ToString();
            cbost.Text = RQGR.Rows[index3].Cells[4].Value.ToString();
            cbopro1.SelectedValue = RQGR.Rows[index3].Cells[5].Value.ToString();
     


            RQGR.Rows.RemoveAt(index3);
            RQGR.ClearSelection();
        }

        private void HFIT_DoubleClick(object sender, EventArgs e)
        {
            int index3 = HFIT.SelectedCells[0].RowIndex;
            txtdcqty.Tag = HFIT.Rows[index3].Cells[0].Value.ToString();
            cbocon.Text = HFIT.Rows[index3].Cells[1].Value.ToString();
            txtdcqty.Text = HFIT.Rows[index3].Cells[2].Value.ToString();
            cbouom.Text = HFIT.Rows[index3].Cells[3].Value.ToString();
            cboty.Text = HFIT.Rows[index3].Cells[4].Value.ToString();
            cbocon.SelectedValue = HFIT.Rows[index3].Cells[5].Value.ToString();
            cbouom.SelectedValue = HFIT.Rows[index3].Cells[6].Value.ToString();


            HFIT.Rows.RemoveAt(index3);
            HFIT.ClearSelection();
        }

        private void buttnext1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
