﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Windows.Forms;

namespace MyEasyBizAPMS
{
    public partial class FrmShortage : Form
    {
        public FrmShortage()
        {
            InitializeComponent();
            this.TabControlBudjet.TabStyle = typeof(Syncfusion.Windows.Forms.Tools.OneNoteStyleRenderer);
            this.SfdDataGridShortage.Style.HeaderStyle.Font.FontStyle = FontStyle.Bold;
            this.SfdDataGridShortage.Style.HeaderStyle.BackColor = Color.DeepSkyBlue;
        }
        SqlConnection connection = new SqlConnection(GeneralParameters.ConnectionString);
        BindingSource bsDocNo = new BindingSource();
        BindingSource bsFabric = new BindingSource();
        BindingSource bsTrims = new BindingSource();
        SQLDBHelper db = new SQLDBHelper();
        int Fillid = 0; int SelectId = 0;
        DataTable dtFabricOrder = new DataTable();
        DataTable dtTrimsOrder = new DataTable();
        private void FrmShortage_Load(object sender, EventArgs e)
        {
            GrApprove.Visible = false;
            LoadDataGridFabricPurchase();
            LoadDatGridTrims();
            GetShortageFront();
            GrFront.Visible = true;
            grBack.Visible = false;
        }

        protected void LoadDataGridFabricPurchase()
        {
            DataGridFabricPurchase.Columns.Clear();
            DataGridFabricPurchase.AutoGenerateColumns = false;
            DataGridFabricPurchase.ColumnCount = 7;
            DataGridFabricPurchase.Columns[0].Name = "Uid";
            DataGridFabricPurchase.Columns[0].HeaderText = "Uid";
            DataGridFabricPurchase.Columns[0].Visible = false;

            DataGridFabricPurchase.Columns[1].Name = "FabricUid";
            DataGridFabricPurchase.Columns[1].HeaderText = "FabricUid";
            DataGridFabricPurchase.Columns[1].Visible = false;

            DataGridFabricPurchase.Columns[2].Name = "FbricBomUid";
            DataGridFabricPurchase.Columns[2].HeaderText = "FbricBomUid";
            DataGridFabricPurchase.Columns[2].Visible = false;

            DataGridFabricPurchase.Columns[3].Name = "Fabric Name";
            DataGridFabricPurchase.Columns[3].HeaderText = "Fabric Name";
            DataGridFabricPurchase.Columns[3].Width = 740;

            DataGridFabricPurchase.Columns[4].Name = "Colour";
            DataGridFabricPurchase.Columns[4].HeaderText = "Colour";
            DataGridFabricPurchase.Columns[4].Width = 100;

            DataGridFabricPurchase.Columns[5].Name = "Qty";
            DataGridFabricPurchase.Columns[5].HeaderText = "Qty";
            DataGridFabricPurchase.Columns[5].Width = 100;
            DataGridFabricPurchase.Columns[5].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            DataGridFabricPurchase.Columns[6].Name = "Uom";
            DataGridFabricPurchase.Columns[6].HeaderText = "Uom";
            DataGridFabricPurchase.Columns[6].Visible = false;
        }

        protected void LoadDatGridTrims()
        {
            DataGridTrims.Columns.Clear();
            DataGridTrims.AutoGenerateColumns = false;
            DataGridTrims.ColumnCount = 5;
            DataGridTrims.Columns[0].Name = "Uid";
            DataGridTrims.Columns[0].HeaderText = "Uid";
            DataGridTrims.Columns[0].Visible = false;

            DataGridTrims.Columns[1].Name = "TrimsUid";
            DataGridTrims.Columns[1].HeaderText = "TrimsUid";
            DataGridTrims.Columns[1].Visible = false;

            DataGridTrims.Columns[2].Name = "Uom";
            DataGridTrims.Columns[2].HeaderText = "Uom";
            DataGridTrims.Columns[2].Visible = false;

            DataGridTrims.Columns[3].Name = "ItemSpec";
            DataGridTrims.Columns[3].HeaderText = "ItemSpec";
            DataGridTrims.Columns[3].Width = 740;

            DataGridTrims.Columns[4].Name = "Qty";
            DataGridTrims.Columns[4].HeaderText = "Qty";
            DataGridTrims.Columns[4].Width = 100;
        }

        private void BtnFabricOk_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtFabric.Text != string.Empty && txtFabricQty.Text != string.Empty)
                {
                    int Index = DataGridFabricPurchase.Rows.Add();
                    DataGridViewRow dataGridViewRow = DataGridFabricPurchase.Rows[Index];
                    dataGridViewRow.Cells[0].Value = "0";
                    dataGridViewRow.Cells[1].Value = ttxColour.Tag;
                    dataGridViewRow.Cells[2].Value = txtFabric.Tag;
                    dataGridViewRow.Cells[3].Value = txtFabric.Text;
                    dataGridViewRow.Cells[4].Value = ttxColour.Text;
                    dataGridViewRow.Cells[5].Value = txtFabricQty.Text;
                    dataGridViewRow.Cells[6].Value = txtFabricQty.Tag;

                    txtFabric.Text = string.Empty;
                    ttxColour.Text = string.Empty;
                    txtFabricQty.Text = string.Empty;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void TxtDocNo_Click(object sender, EventArgs e)
        {
            try
            {
                string Query = "Select Uid,DocNo,DocDate,StyleDesc from OrderM Where Uid in (Select OrderMuid from OrderMBudjet) and DocSts = 'Entry Completed' order by Uid desc";
                DataTable dt = db.GetDataWithoutParam(CommandType.Text, Query, connection);
                bsDocNo.DataSource = dt;
                FillGrid(dt, 1);
                Point loc = Genclass.FindLocation(txtOrderNo);
                grSearch.Location = new Point(loc.X - 10, loc.Y + 20);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

        }

        private void FillGrid(DataTable dt, int FillId)
        {
            try
            {
                DataGridCommon.Columns.Clear();
                DataGridCommon.DataSource = null;
                DataGridCommon.AutoGenerateColumns = false;
                if (FillId == 1)
                {
                    Fillid = 1;
                    DataGridCommon.ColumnCount = 3;
                    DataGridCommon.Columns[0].Name = "Uid";
                    DataGridCommon.Columns[0].HeaderText = "Uid";
                    DataGridCommon.Columns[0].DataPropertyName = "Uid";
                    DataGridCommon.Columns[0].Visible = false;
                    DataGridCommon.Columns[0].Width = 0;

                    DataGridCommon.Columns[1].Name = "DocNo";
                    DataGridCommon.Columns[1].HeaderText = "DocNo";
                    DataGridCommon.Columns[1].DataPropertyName = "DocNo";
                    DataGridCommon.Columns[1].Width = 100;

                    DataGridCommon.Columns[2].Name = "StyleDesc";
                    DataGridCommon.Columns[2].HeaderText = "StyleDesc";
                    DataGridCommon.Columns[2].DataPropertyName = "StyleDesc";
                    DataGridCommon.Columns[2].Width = 210;

                    DataGridCommon.DataSource = bsDocNo;
                }
                else if (FillId == 2)
                {
                    Fillid = 2;
                    DataGridCommon.ColumnCount = 13;
                    DataGridCommon.Columns[0].Name = "Uid";
                    DataGridCommon.Columns[0].HeaderText = "Uid";
                    DataGridCommon.Columns[0].DataPropertyName = "Uid";
                    DataGridCommon.Columns[0].Visible = false;
                    DataGridCommon.Columns[0].Width = 0;

                    DataGridCommon.Columns[1].Name = "FabRICUid";
                    DataGridCommon.Columns[1].HeaderText = "FabRICUid";
                    DataGridCommon.Columns[1].DataPropertyName = "FabRICUid";
                    DataGridCommon.Columns[1].Visible = false;

                    DataGridCommon.Columns[2].Name = "Componentuid";
                    DataGridCommon.Columns[2].HeaderText = "Componentuid";
                    DataGridCommon.Columns[2].DataPropertyName = "Componentuid";
                    DataGridCommon.Columns[2].Visible = false;

                    DataGridCommon.Columns[3].Name = "ComboUid";
                    DataGridCommon.Columns[3].HeaderText = "ComboUid";
                    DataGridCommon.Columns[3].DataPropertyName = "ComboUid";
                    DataGridCommon.Columns[3].Visible = false;

                    DataGridCommon.Columns[4].Name = "SizeUid";
                    DataGridCommon.Columns[4].HeaderText = "SizeUid";
                    DataGridCommon.Columns[4].DataPropertyName = "SizeUid";
                    DataGridCommon.Columns[4].Visible = false;

                    DataGridCommon.Columns[5].Name = "FabColourUid";
                    DataGridCommon.Columns[5].HeaderText = "FabColourUid";
                    DataGridCommon.Columns[5].DataPropertyName = "FabColourUid";
                    DataGridCommon.Columns[5].Visible = false;

                    DataGridCommon.Columns[6].Name = "Fabric";
                    DataGridCommon.Columns[6].HeaderText = "Fabric";
                    DataGridCommon.Columns[6].DataPropertyName = "Fabric";
                    DataGridCommon.Columns[6].Width = 680;

                    DataGridCommon.Columns[7].Name = "Dia";
                    DataGridCommon.Columns[7].HeaderText = "Dia";
                    DataGridCommon.Columns[7].DataPropertyName = "Dia";
                    DataGridCommon.Columns[7].Visible = false;

                    DataGridCommon.Columns[8].Name = "FabricWt";
                    DataGridCommon.Columns[8].HeaderText = "FabricWt";
                    DataGridCommon.Columns[8].DataPropertyName = "FabricWt";
                    DataGridCommon.Columns[8].Visible = false;

                    DataGridCommon.Columns[9].Name = "Tolerance";
                    DataGridCommon.Columns[9].HeaderText = "Tolerance";
                    DataGridCommon.Columns[9].DataPropertyName = "Tolerance";
                    DataGridCommon.Columns[9].Visible = false;

                    DataGridCommon.Columns[10].Name = "Ftype";
                    DataGridCommon.Columns[10].HeaderText = "Ftype";
                    DataGridCommon.Columns[10].DataPropertyName = "Ftype";
                    DataGridCommon.Columns[10].Visible = false;

                    DataGridCommon.Columns[11].Name = "Colour";
                    DataGridCommon.Columns[11].HeaderText = "Colour";
                    DataGridCommon.Columns[11].DataPropertyName = "Colour";
                    DataGridCommon.Columns[11].Visible = false;

                    DataGridCommon.Columns[12].Name = "Uom";
                    DataGridCommon.Columns[12].HeaderText = "Uom";
                    DataGridCommon.Columns[12].DataPropertyName = "Uom";
                    DataGridCommon.Columns[12].Visible = false;

                    DataGridCommon.DataSource = bsFabric;
                    DataGridCommon.RowsDefaultCellStyle.WrapMode = DataGridViewTriState.True;
                    DataGridCommon.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCellsExceptHeaders;
                }
                else if (FillId == 3)
                {
                    Fillid = 3;
                    DataGridCommon.ColumnCount = 3;
                    DataGridCommon.Columns[0].Name = "Uid";
                    DataGridCommon.Columns[0].HeaderText = "Uid";
                    DataGridCommon.Columns[0].DataPropertyName = "Uid";
                    DataGridCommon.Columns[0].Visible = false;
                    DataGridCommon.Columns[0].Width = 0;

                    DataGridCommon.Columns[1].Name = "ItemSpec";
                    DataGridCommon.Columns[1].HeaderText = "ItemSpec";
                    DataGridCommon.Columns[1].DataPropertyName = "ItemSpec";
                    DataGridCommon.Columns[1].Width = 680;

                    DataGridCommon.Columns[2].Name = "Uom";
                    DataGridCommon.Columns[2].HeaderText = "Uom";
                    DataGridCommon.Columns[2].DataPropertyName = "Uom";
                    DataGridCommon.Columns[2].Visible = false;

                    DataGridCommon.DataSource = bsTrims;
                    DataGridCommon.RowsDefaultCellStyle.WrapMode = DataGridViewTriState.True;
                    DataGridCommon.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCellsExceptHeaders;
                }
                if (FillId == 2 || FillId == 3)
                {
                    grSearch.Width = 750;
                    DataGridCommon.Width = 710;
                }
                grSearch.Visible = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void BtnSelect_Click(object sender, EventArgs e)
        {
            try
            {
                SelectId = 1;
                int Index = DataGridCommon.SelectedCells[0].RowIndex;
                if (Fillid == 1)
                {
                    txtOrderNo.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtOrderNo.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                }
                else if (Fillid == 2)
                {
                    txtFabric.Text = DataGridCommon.Rows[Index].Cells[6].Value.ToString();
                    txtFabric.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    ttxColour.Text = DataGridCommon.Rows[Index].Cells[11].Value.ToString();
                    ttxColour.Tag = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtFabricQty.Tag = DataGridCommon.Rows[Index].Cells[12].Value.ToString();
                }
                else if (Fillid == 3)
                {
                    txtTrims.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtTrims.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    txtTrimsQty.Tag = DataGridCommon.Rows[Index].Cells[2].Value.ToString();
                }

                grSearch.Visible = false;
                SelectId = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void TxtFabric_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtOrderNo.Text != string.Empty)
                {
                    SqlParameter[] sqlParameters = { new SqlParameter("@OrderMUid", txtOrderNo.Tag) };
                    DataTable table = db.GetDataWithParam(CommandType.StoredProcedure, "Proc_GetFabricForShortage", sqlParameters, connection);
                    bsFabric.DataSource = table;
                    dtFabricOrder = table;
                    FillGrid(table, 2);
                    Point loc = Genclass.FindLocation(txtFabric);
                    grSearch.Location = new Point(loc.X - 10, loc.Y + 20);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void BtnHide_Click(object sender, EventArgs e)
        {
            grSearch.Visible = false;
        }

        private void DataGridCommon_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            BtnSelect_Click(sender, e);
        }

        private void TxtTrims_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtOrderNo.Text != string.Empty)
                {
                    SqlParameter[] sqlParameters = { new SqlParameter("@OrderMUid", txtOrderNo.Tag) };
                    DataTable table = db.GetDataWithParam(CommandType.StoredProcedure, "Proc_GetTrimsForShortage", sqlParameters, connection);
                    bsTrims.DataSource = table;
                    dtTrimsOrder = table;
                    FillGrid(table, 3);
                    Point loc = Genclass.FindLocation(txtTrims);
                    grSearch.Location = new Point(loc.X - 10, loc.Y + 20);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtTrims.Text != string.Empty && txtTrimsQty.Text != string.Empty)
                {
                    int Index = DataGridTrims.Rows.Add();
                    DataGridViewRow dataGridViewRow = DataGridTrims.Rows[Index];
                    dataGridViewRow.Cells[0].Value = "0";
                    dataGridViewRow.Cells[1].Value = txtTrims.Tag;
                    dataGridViewRow.Cells[2].Value = txtTrimsQty.Tag;
                    dataGridViewRow.Cells[3].Value = txtTrims.Text;
                    dataGridViewRow.Cells[4].Value = txtTrimsQty.Text;
                    txtTrims.Text = string.Empty;
                    txtTrimsQty.Text = string.Empty;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void TxtOrderNo_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bsDocNo.Filter = string.Format("DocNo Like '%{0}%'", txtOrderNo.Text);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void TxtFabric_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bsFabric.Filter = string.Format("Fabric Like '%{0}%'", txtFabric.Text);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void TxtTrims_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bsTrims.Filter = string.Format("ItemSpec Like '%{0}%'", txtTrims.Text);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void SpliAdd_Click(object sender, EventArgs e)
        {
            try
            {
                txtDocNo.Tag = "0";
                txtDocNo.Text = GeneralParameters.GetDocNo(1410, connection);
                GrFront.Visible = false;
                grBack.Visible = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void SplitSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtOrderNo.Text == string.Empty)
                {
                    MessageBox.Show("Select Order No !", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtOrderNo.Focus();
                    return;
                }
                else if (DataGridFabricPurchase.Rows.Count == 0 && DataGridTrims.Rows.Count == 0)
                {
                    MessageBox.Show("Fabric and Trims can't Empty !", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtOrderNo.Focus();
                    return;
                }
                else
                {
                    SqlParameter[] sqlParameters = {
                        new SqlParameter("@Uid",txtDocNo.Tag),
                        new SqlParameter("@DocNo",txtDocNo.Text),
                        new SqlParameter("@OrderMuid",txtOrderNo.Tag),
                        new SqlParameter("@DocDate",Convert.ToDateTime(dtpDocDate.Text).ToString("yyyy-MM-dd")),
                        new SqlParameter("@Sts","Pending"),
                        new SqlParameter("@ReturnId",SqlDbType.BigInt)
                    };
                    sqlParameters[5].Direction = ParameterDirection.Output;
                    int ReturnUid = db.ExecuteNonQuery(CommandType.StoredProcedure, "Proc_OrderMShortage", sqlParameters, connection,5);

                    for (int i = 0; i < DataGridFabricPurchase.Rows.Count; i++)
                    {
                        SqlParameter[] sqlParameters1 = {
                            new SqlParameter("@Uid",DataGridFabricPurchase.Rows[i].Cells[0].Value.ToString()),
                            new SqlParameter("@OrderMuid",txtOrderNo.Tag),
                            new SqlParameter("@ShortageUid",ReturnUid),
                            new SqlParameter("@FabricUid",DataGridFabricPurchase.Rows[i].Cells[1].Value.ToString()),
                            new SqlParameter("@BomUid",DataGridFabricPurchase.Rows[i].Cells[2].Value.ToString()),
                            new SqlParameter("@ItemName",DataGridFabricPurchase.Rows[i].Cells[3].Value.ToString()),
                            new SqlParameter("@Colour",DataGridFabricPurchase.Rows[i].Cells[4].Value.ToString()),
                            new SqlParameter("@Qty",DataGridFabricPurchase.Rows[i].Cells[5].Value.ToString()),
                            new SqlParameter("@Uom",DataGridFabricPurchase.Rows[i].Cells[6].Value.ToString())
                        };
                        db.ExecuteNonQuery(CommandType.StoredProcedure, "Proc_OrderMShortageFabric", sqlParameters1, connection);
                    }

                    for (int i = 0; i < DataGridTrims.Rows.Count; i++)
                    {
                        SqlParameter[] sqlParameters1 = {
                            new SqlParameter("@Uid",DataGridTrims.Rows[i].Cells[0].Value.ToString()),
                            new SqlParameter("@OrderMuid",txtOrderNo.Tag),
                            new SqlParameter("@ShortageUid",ReturnUid),
                            new SqlParameter("@TrimsUid",DataGridTrims.Rows[i].Cells[1].Value.ToString()),
                            new SqlParameter("@Uom",DataGridTrims.Rows[i].Cells[2].Value.ToString()),
                            new SqlParameter("@ItemName",DataGridTrims.Rows[i].Cells[3].Value.ToString()),
                            new SqlParameter("@Qty",DataGridTrims.Rows[i].Cells[4].Value.ToString()),
                        };
                        db.ExecuteNonQuery(CommandType.StoredProcedure, "Proc_OrderMShortageTrims", sqlParameters1, connection);
                    }
                    if (txtDocNo.Tag.ToString() == "0")
                    {
                        string Query = "Update DocTypeM Set Lastno = Lastno+1 Where DocTypeId = 1410";
                        db.ExecuteNonQuery(CommandType.Text, Query, connection);
                    }
                    MessageBox.Show("Record has been saved successfully", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtOrderNo.Text = string.Empty;
                    txtDocNo.Text = string.Empty;
                    DataGridFabricPurchase.Rows.Clear();
                    DataGridTrims.Rows.Clear();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void SpliAdd_DropDowItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            try
            {
                if (e.ClickedItem.Text == "Close")
                {
                    this.Close();
                }
                else
                {
                    GrFront.Visible = false;
                    grBack.Visible = true;
                    SelectId = 1;
                    if (SfdDataGridShortage.SelectedIndex == -1)
                    {
                        MessageBox.Show("Select a row", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                    var selectedItem = SfdDataGridShortage.SelectedItems[0];
                    var dataRow = (selectedItem as DataRowView).Row;
                    decimal Uid = Convert.ToDecimal(dataRow["uid"].ToString());
                 txtOrderNo.Text = dataRow["OrderNo"].ToString();
                    txtDocNo.Text =dataRow["DocNo"].ToString();
                    dtpDocDate.Text = dataRow["DocDate"].ToString();
                    string status;
                    status = dataRow["sts"].ToString();
                    if (status == "Pending")
                    {

                        SqlParameter[] sqlParameters = { new SqlParameter("@uid", Uid) };
                        DataSet dataTable = db.GetDataWithParamMultiple(CommandType.StoredProcedure, "Proc_GetShortage", sqlParameters, connection);
                        ////DataTable dtq = GetorderStyle(CUid);
                        DataTable dt = dataTable.Tables[1];
                        DataTable dtcon = dataTable.Tables[2];

                        DataGridFabricPurchase.Rows.Clear();
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {

                            int Index = DataGridFabricPurchase.Rows.Add();
                            DataGridViewRow dataGridViewRow = DataGridFabricPurchase.Rows[Index];
                            dataGridViewRow.Cells[0].Value = dt.Rows[i]["uid"].ToString();
                            dataGridViewRow.Cells[1].Value = dt.Rows[i]["FabricUid"].ToString();
                            dataGridViewRow.Cells[2].Value = dt.Rows[i]["BomUid"].ToString();
                            dataGridViewRow.Cells[3].Value = dt.Rows[i]["ItemName"].ToString();
                            dataGridViewRow.Cells[4].Value = dt.Rows[i]["Colour"].ToString();
                            dataGridViewRow.Cells[5].Value = dt.Rows[i]["Qty"].ToString();
                            dataGridViewRow.Cells[6].Value = dt.Rows[i]["Uom"].ToString();

                        }
                        DataGridTrims.Rows.Clear();
                        for (int i = 0; i < dtcon.Rows.Count; i++)
                        {
                            int Index = DataGridTrims.Rows.Add();
                            DataGridViewRow dataGridViewRow = DataGridTrims.Rows[Index];
                            dataGridViewRow.Cells[0].Value = dtcon.Rows[i]["uid"].ToString();
                            dataGridViewRow.Cells[1].Value = dtcon.Rows[i]["TrimsUid"].ToString();
                            dataGridViewRow.Cells[2].Value = dtcon.Rows[i]["uom"].ToString();
                            dataGridViewRow.Cells[3].Value = dtcon.Rows[i]["ItemName"].ToString();
                            dataGridViewRow.Cells[4].Value = dtcon.Rows[i]["Qty"].ToString();

                        }
                    }
                    else
                    {
                        MessageBox.Show("Entry Already Approved");
                        return;

                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void SplitSave_DropDowItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            try
            {
                if (e.ClickedItem.Text == "Back")
                {
                    GetShortageFront();
                    GrFront.Visible = true;
                    grBack.Visible = false;
                }
                else
                {

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected void GetShortageFront()
        {
            try
            {
                SqlParameter[] sqlParameters = { new SqlParameter("@Uid", "0") };
                DataTable dataTable = db.GetDataWithParam(CommandType.StoredProcedure, "Proc_GetShortage", sqlParameters, connection);
                SfdDataGridShortage.DataSource = null;
                SfdDataGridShortage.DataSource = dataTable;
                SfdDataGridShortage.Columns[0].Visible = false;
                SfdDataGridShortage.Columns[2].Width = 570;
                SfdDataGridShortage.Columns[3].Width = 150;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void SfdDataGridShortage_QueryCellStyle(object sender, Syncfusion.WinForms.DataGrid.Events.QueryCellStyleEventArgs e)
        {
            try
            {
                if (e.Column.MappingName == "Sts")
                {
                    if (e.DisplayText == "Pending")
                    {
                        e.Style.BackColor = Color.Orange;
                        e.Style.TextColor = Color.Black;
                    }
                    else
                    {
                        e.Style.BackColor = Color.LightGreen;
                        e.Style.TextColor = Color.DarkSlateBlue;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void SfdDataGridShortage_CellDoubleClick(object sender, Syncfusion.WinForms.DataGrid.Events.CellClickEventArgs e)
        {
            try
            {
                if (e.DataColumn.Index == 5)
                {
                    var selectedItem = SfdDataGridShortage.SelectedItems[0];
                    var dataRow = (selectedItem as DataRowView).Row;
                    decimal BudgetUid = Convert.ToDecimal(dataRow["Uid"].ToString());
                    lblOrderNo.Text = dataRow["OrderNo"].ToString();
                    lblStyle.Text = dataRow["StyleDesc"].ToString();
                    lblSeason.Text = dataRow["DocDate"].ToString();
                    //lblOrderQty.Text = dataRow["ORDERQTY"].ToString();
                    //lblReqQty.Text = dataRow["PLANQTY"].ToString();
                    //lblIncome.Text = dataRow["INCOME"].ToString();
                    //lblExpense.Text = dataRow["EXPENCES"].ToString();
                    //lblPLValue.Text = dataRow["ProfitLossValue"].ToString();
                    //lblPLPer.Text = dataRow["Percentage"].ToString();
                    CmbStatus.Text = dataRow["Sts"].ToString();
                    GrApprove.Visible = true;

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void splitButton1_Click(object sender, EventArgs e)
        {
            var selectedItem = SfdDataGridShortage.SelectedItems[0];
            var dataRow = (selectedItem as DataRowView).Row;
    
            decimal BudgetUid = Convert.ToDecimal(dataRow["Uid"].ToString());
            SqlParameter[] sqlParameters = {
                  
                    new SqlParameter("@status",CmbStatus.Text),
                    new SqlParameter("@remarks",txtRemarks.Text),
                    new SqlParameter("@uid",BudgetUid),
                };
            db.ExecuteNonQuery(CommandType.StoredProcedure, "sp_shortageAppoved", sqlParameters, connection);

            GrApprove.Visible = false;
            lblOrderNo.Text = string.Empty;
            lblStyle.Text = string.Empty;
            lblSeason.Text = string.Empty;
            lblOrderQty.Text = string.Empty;
            lblReqQty.Text = string.Empty;
            lblIncome.Text = string.Empty;
            lblExpense.Text = string.Empty;
            lblPLValue.Text = string.Empty;
            lblPLPer.Text = string.Empty;
            CmbStatus.Text = string.Empty;
            GetShortageFront();
        }

        private void SfdDataGridShortage_Click(object sender, EventArgs e)
        {

        }

        private void splitButton1_DropDowItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            try
            {
                if (e.ClickedItem.Text == "Back")
                {
                    GrApprove.Visible = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
    }
}
