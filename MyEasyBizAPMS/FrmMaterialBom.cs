﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MyEasyBizAPMS
{
    public partial class FrmMaterialBom : Form
    {
        public FrmMaterialBom()
        {
            InitializeComponent();
            this.SfdDataGridBOM.Style.HeaderStyle.Font.FontStyle = FontStyle.Bold;
            this.SfdDataGridBOM.Style.HeaderStyle.BackColor = Color.DeepSkyBlue;
            this.tabControlAdv1.TabStyle = typeof(Syncfusion.Windows.Forms.Tools.OneNoteStyleRenderer);
        }
        int Fillid = 0; int SelectId = 0;
        private int LoadId = 0;
        SqlConnection sqlconnection = new SqlConnection(GeneralParameters.ConnectionString);
        SQLDBHelper db = new SQLDBHelper();
        BindingSource bsDocNo = new BindingSource();
        BindingSource bsClass = new BindingSource();
        BindingSource bsItem = new BindingSource();
        DataTable nm = new DataTable();
        DataTable dtOrderQty = new DataTable();
        DataTable dtStyleQty = new DataTable();
        DataTable dtComboQty = new DataTable();
        DataTable dtSizeQty = new DataTable();

        private void FrmMaterialBom_Load(object sender, EventArgs e)
        {
            LoadBOMItemGrid();
            //LoadAttribute();
            LodCombo();
            grFront.Visible = true;
            grBack.Visible = false;
            LoadBOM();
            LoadDatagridOrderStyle();
            LoadDataGridOrderCombo();
            LoadDataGridOrderSize();
            LoadDatagridStyleWise();
            LoadDataGridComboWise();
            LoadDataGridSizeWise();
            LoadDatagridComboStyle();
            LoadDataGridSizeCombo();
        }

        public DataTable LoadGeneralM()
        {
            DataTable dt = new DataTable();
            try
            {
                dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "SP_GetGeneralsTrims", sqlconnection);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            return dt;
        }

        protected void LoadBOMItemGrid()
        {
            try
            {
                SelectId = 1;
                DataGridMBOMItem.AutoGenerateColumns = false;
                DataGridMBOMItem.ColumnCount = 25;

                DataGridMBOMItem.Columns[0].Name = "SlNo";
                DataGridMBOMItem.Columns[0].HeaderText = "SlNo";
                DataGridMBOMItem.Columns[0].Width = 50;

                DataGridMBOMItem.Columns[1].Name = "Name";
                DataGridMBOMItem.Columns[1].HeaderText = "Name";

                DataGridMBOMItem.Columns[2].Name = "Item Spec";
                DataGridMBOMItem.Columns[2].HeaderText = "Item Spec";
                DataGridMBOMItem.Columns[2].Width = 250;

                DataGridMBOMItem.Columns[3].Name = "Per Piece";
                DataGridMBOMItem.Columns[3].HeaderText = "Per Piece";

                DataGridMBOMItem.Columns[4].Name = "Qty Consumed";
                DataGridMBOMItem.Columns[4].HeaderText = "Qty Consumed";

                DataGridMBOMItem.Columns[5].Name = "Excess";
                DataGridMBOMItem.Columns[5].HeaderText = "% Excess";

                DataGridMBOMItem.Columns[6].Name = "Rate";
                DataGridMBOMItem.Columns[6].HeaderText = "Rate";

                DataGridMBOMItem.Columns[7].Name = "ReqQty";
                DataGridMBOMItem.Columns[7].HeaderText = "Req. Qty";

                DataGridMBOMItem.Columns[8].Name = "ExcessQty";
                DataGridMBOMItem.Columns[8].HeaderText = "Excess Qty";
                DataGridMBOMItem.Columns[8].Width = 70;

                DataGridMBOMItem.Columns[9].Name = "TotalQty";
                DataGridMBOMItem.Columns[9].HeaderText = "Total Qty";
                DataGridMBOMItem.Columns[9].Width = 70;

                DataGridMBOMItem.Columns[10].Name = "Total Value";
                DataGridMBOMItem.Columns[10].HeaderText = "Total Value";
                DataGridMBOMItem.Columns[10].Width = 70;

                DataGridMBOMItem.Columns[11].Name = "CtgyID";
                DataGridMBOMItem.Columns[11].HeaderText = "CtgyID";
                DataGridMBOMItem.Columns[11].Visible = false;

                DataGridMBOMItem.Columns[12].Name = "StyleUid";
                DataGridMBOMItem.Columns[12].HeaderText = "StyleUid";
                DataGridMBOMItem.Columns[12].Visible = false;

                DataGridMBOMItem.Columns[13].Name = "ItemId";
                DataGridMBOMItem.Columns[13].HeaderText = "ItemId";
                DataGridMBOMItem.Columns[13].Visible = false;

                DataGridMBOMItem.Columns[14].Name = "Colour";
                DataGridMBOMItem.Columns[14].HeaderText = "Colour";
                DataGridMBOMItem.Columns[14].Visible = true;

                DataGridMBOMItem.Columns[15].Name = "Type";
                DataGridMBOMItem.Columns[15].HeaderText = "Type";
                DataGridMBOMItem.Columns[15].Visible = false;

                DataGridMBOMItem.Columns[16].Name = "Style";
                DataGridMBOMItem.Columns[16].HeaderText = "Style";

                DataGridMBOMItem.Columns[17].Name = "Combo";
                DataGridMBOMItem.Columns[17].HeaderText = "Combo";

                DataGridMBOMItem.Columns[18].Name = "Size";
                DataGridMBOMItem.Columns[18].HeaderText = "Size";
                DataGridMBOMItem.Columns[18].Width = 50;

                DataGridMBOMItem.Columns[19].Name = "ComboUid";
                DataGridMBOMItem.Columns[19].HeaderText = "ComboUid";
                DataGridMBOMItem.Columns[19].Visible = false;

                DataGridMBOMItem.Columns[20].Name = "SizeUid";
                DataGridMBOMItem.Columns[20].HeaderText = "SizeUid";
                DataGridMBOMItem.Columns[20].Visible = false;

                DataGridMBOMItem.Columns[21].Name = "Uid";
                DataGridMBOMItem.Columns[21].HeaderText = "Uid";
                DataGridMBOMItem.Columns[21].Visible = false;

                DataGridMBOMItem.Columns[22].Name = "UOM";
                DataGridMBOMItem.Columns[22].HeaderText = "UOM";
                DataGridMBOMItem.Columns[22].Visible = true;
                DataGridMBOMItem.Columns[22].Width = 50;

                DataGridMBOMItem.Columns[23].Name = "Notes";
                DataGridMBOMItem.Columns[23].HeaderText = "Notes";
                DataGridMBOMItem.Columns[23].Width = 150;

                DataGridMBOMItem.Columns[24].Name = "MaterialType";
                DataGridMBOMItem.Columns[24].HeaderText = "MaterialType";
                DataGridMBOMItem.Columns[24].Visible = false;
                SelectId = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                throw;
            }
        }

        protected void LodCombo()
        {
            try
            {
                //DataGridCombo.AutoGenerateColumns = false;
                //DataGridCombo.ColumnCount = 3;
                //DataGridViewCheckBoxColumn boxColumn = new DataGridViewCheckBoxColumn
                //{
                //    HeaderText = "Chck",
                //    Name = "Chck",
                //    Width = 50
                //};
                //DataGridCombo.Columns.Insert(0, boxColumn);
                //DataGridCombo.Columns[1].Name = "Combo";
                //DataGridCombo.Columns[1].HeaderText = "Combo";
                //DataGridCombo.Columns[1].Width = 145;

                //DataGridCombo.Columns[2].Name = "ComboUid";
                //DataGridCombo.Columns[2].HeaderText = "ComboUid";
                //DataGridCombo.Columns[2].Visible = false;

                //DataGridCombo.Columns[3].Name = "Qty";
                //DataGridCombo.Columns[3].HeaderText = "Qty";
                //DataGridCombo.Columns[3].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                throw;
            }
        }

        private void TxtOrderNO_Click(object sender, EventArgs e)
        {
            try
            {
                string Query = "Select Uid,DocNo,DocDate,StyleDesc from OrderM Where Uid Not in (Select OrderMuid from OrderMMaterialBOM) and DocSts = 'Entry Completed' order by Uid desc";
                DataTable dt = db.GetDataWithoutParam(CommandType.Text, Query, sqlconnection);
                bsDocNo.DataSource = dt;
                FillGrid(dt, 1);
                Point loc = Genclass.FindLocation(txtOrderNO);
                grSearch.Location = new Point(loc.X - 10, loc.Y + 20);
                grSearch.Visible = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                throw;
            }
        }

        private void FillGrid(DataTable dt, int FillId)
        {
            try
            {
                DataGridCommon.DataSource = null;
                DataGridCommon.AutoGenerateColumns = false;
                if (FillId == 1)
                {
                    Fillid = 1;
                    DataGridCommon.ColumnCount = 3;
                    DataGridCommon.Columns[0].Name = "Uid";
                    DataGridCommon.Columns[0].HeaderText = "Uid";
                    DataGridCommon.Columns[0].DataPropertyName = "Uid";
                    DataGridCommon.Columns[0].Visible = false;
                    DataGridCommon.Columns[0].Width = 0;
                    DataGridCommon.Columns[1].Name = "DocNo";
                    DataGridCommon.Columns[1].HeaderText = "DocNo";
                    DataGridCommon.Columns[1].DataPropertyName = "DocNo";
                    DataGridCommon.Columns[1].Width = 100;
                    DataGridCommon.Columns[2].Name = "StyleDesc";
                    DataGridCommon.Columns[2].HeaderText = "StyleDesc";
                    DataGridCommon.Columns[2].DataPropertyName = "StyleDesc";
                    DataGridCommon.Columns[2].Width = 210;
                    DataGridCommon.DataSource = bsDocNo;
                }
                if (FillId == 2)
                {
                    Fillid = 2;
                    DataGridCommon.ColumnCount = 2;
                    DataGridCommon.Columns[0].Name = "Uid";
                    DataGridCommon.Columns[0].HeaderText = "Uid";
                    DataGridCommon.Columns[0].DataPropertyName = "Guid";
                    DataGridCommon.Columns[0].Visible = false;
                    DataGridCommon.Columns[0].Width = 0;
                    DataGridCommon.Columns[1].Name = "Category";
                    DataGridCommon.Columns[1].HeaderText = "Category";
                    DataGridCommon.Columns[1].DataPropertyName = "GeneralName";
                    DataGridCommon.Columns[1].Width = 220;
                    DataGridCommon.DataSource = bsClass;
                }
                if (FillId == 3)
                {
                    Fillid = 3;
                    DataGridCommon.ColumnCount = 2;
                    DataGridCommon.Columns[0].Name = "Uid";
                    DataGridCommon.Columns[0].HeaderText = "Uid";
                    DataGridCommon.Columns[0].DataPropertyName = "UID";
                    DataGridCommon.Columns[0].Visible = false;
                    DataGridCommon.Columns[0].Width = 0;
                    DataGridCommon.Columns[1].Name = "Category";
                    DataGridCommon.Columns[1].HeaderText = "Category";
                    DataGridCommon.Columns[1].DataPropertyName = "ItemName";
                    DataGridCommon.Columns[1].Width = 320;
                    DataGridCommon.DataSource = bsItem;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void TxtOrderNO_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bsDocNo.Filter = string.Format("DocNo LIKE '%{0}%'", txtOrderNO.Text);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void BtnSelect_Click(object sender, EventArgs e)
        {
            try
            {
                SelectId = 1;
                int Index = DataGridCommon.SelectedCells[0].RowIndex;
                if (Fillid == 1)
                {
                    SelectId = 1;
                    txtOrderNO.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtOrderNO.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    GetOrderMEdit(Convert.ToDecimal(txtOrderNO.Tag));
                    SqlParameter[] sqlParameters = { new SqlParameter("@OrderMuid", txtOrderNO.Tag) };
                    DataSet dataSetQty = db.GetDataWithParamMultiple(CommandType.StoredProcedure, "Proc_GetOrderTotalQtyForMBOM", sqlParameters, sqlconnection);
                    dtOrderQty = dataSetQty.Tables[0];
                    dtStyleQty = dataSetQty.Tables[1];
                    dtComboQty = dataSetQty.Tables[2];
                    dtSizeQty = dataSetQty.Tables[3];
                    CmbType.SelectedIndex = 0;
                    SelectId = 0;
                }
                else if (Fillid == 2)
                {
                    txtCtgy.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtCtgy.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                }
                else if (Fillid == 3)
                {
                    txtItemName.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtItemName.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    GetAttribute(Convert.ToDecimal(txtItemName.Tag));
                }
                grSearch.Visible = false;
                SelectId = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void GetAttribute(decimal v)
        {
            try
            {
                //DataGridAttribute.Rows.Clear();
                //SqlParameter[] parameters = { new SqlParameter("@ItemUId", v) };
                //DataTable dt = db.GetDataWithParam(CommandType.StoredProcedure, "SP_GetAttributeForBOM", parameters, sqlconnection);
                //for (int i = 0; i < dt.Rows.Count; i++)
                //{
                //    int index = DataGridAttribute.Rows.Add();
                //    DataGridViewRow row = (DataGridViewRow)DataGridAttribute.Rows[index];
                //    row.Cells[0].Value = dt.Rows[i]["Attribute"].ToString();
                //    row.Cells[1].Value = "";
                //    row.Cells[2].Value = dt.Rows[i]["Uid"].ToString();
                //    row.Cells[3].Value = dt.Rows[i]["UserDefined"].ToString();
                //}
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }

        protected void GetOrderMEdit(decimal OrderMuid)
        {
            try
            {
                decimal CUid = Convert.ToDecimal(OrderMuid);
                SqlParameter[] parameters = { new SqlParameter("@OrderUid", CUid) };
                DataSet ds = db.GetDataWithParamMultiple(CommandType.StoredProcedure, "SP_GetOrderMEdit", parameters, sqlconnection);
                DataTable dt = ds.Tables[0];
                DataTable dtStyle = ds.Tables[1];
                LoadId = 1;
                string Query = "Select Uid,ComboUid,ComboQty from StyleCombo Where OrderMUid =" + CUid + "";
                DataTable dtCombo = db.GetDataWithoutParam(CommandType.Text, Query, sqlconnection);
                //CmbStyle.DisplayMember = "StyleName";
                //CmbStyle.ValueMember = "Uid";
                //CmbStyle.DataSource = dtStyle;
                //for (int i = 0; i < dtCombo.Rows.Count; i++)
                //{
                //    int idex = DataGridCombo.Rows.Add();
                //    DataGridViewRow row = (DataGridViewRow)DataGridCombo.Rows[idex];
                //    row.Cells[1].Value = dtCombo.Rows[i]["ComboUid"].ToString();
                //    row.Cells[2].Value = dtCombo.Rows[i]["Uid"].ToString();
                //    row.Cells[3].Value = dtCombo.Rows[i]["ComboQty"].ToString();
                //    //DataGridCombo.Rows.Add(row);

                //}
                LoadId = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void DataGridCommon_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                SelectId = 1;
                int Index = DataGridCommon.SelectedCells[0].RowIndex;
                if (Fillid == 1)
                {
                    txtOrderNO.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtOrderNO.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    GetOrderMEdit(Convert.ToDecimal(txtOrderNO.Tag));
                    SqlParameter[] sqlParameters = { new SqlParameter("@OrderMuid", txtOrderNO.Tag) };
                    DataSet dataSetQty = db.GetDataWithParamMultiple(CommandType.StoredProcedure, "Proc_GetOrderTotalQtyForMBOM", sqlParameters, sqlconnection);
                    dtOrderQty = dataSetQty.Tables[0];
                    dtStyleQty = dataSetQty.Tables[1];
                    dtComboQty = dataSetQty.Tables[2];
                    dtSizeQty = dataSetQty.Tables[3];
                    CmbType.SelectedIndex = 0;
                    CmbCategory_SelectedIndexChanged(sender, e);
                }
                else if (Fillid == 2)
                {
                    txtCtgy.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtCtgy.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                }
                else if (Fillid == 3)
                {
                    txtItemName.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtItemName.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    GetAttribute(Convert.ToDecimal(txtItemName.Tag));
                }
                grSearch.Visible = false;
                SelectId = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void TxtCtgy_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dt = LoadGeneralM();
                DataTable dtclass = dt.Select("TypeMUid = 13").CopyToDataTable();
                bsClass.DataSource = dtclass;
                FillGrid(dtclass, 2);
                Point loc = Genclass.FindLocation(txtCtgy);
                grSearch.Location = new Point(loc.X - 10, loc.Y + 10);
                grSearch.Visible = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void TxtItemName_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtCtgy.Text != string.Empty)
                {
                    string Query = "select UID,ItemName from TrimsM Where CategoryUid  =" + txtCtgy.Tag + "";
                    DataTable dt = db.GetDataWithoutParam(CommandType.Text, Query, sqlconnection);
                    bsItem.DataSource = dt;
                    FillGrid(dt, 3);
                    Point loc = Genclass.FindLocation(txtItemName);
                    grSearch.Location = new Point(loc.X - 10, loc.Y + 20);
                    grSearch.Visible = true;
                }
                else
                {
                    MessageBox.Show("Select Catgory", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtCtgy.Focus();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void TxtCtgy_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bsClass.Filter = string.Format("GeneralName LIKE '%{0}%'", txtCtgy.Text);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void TxtItemName_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bsItem.Filter = string.Format("ItemName LIKE '%{0}%'", txtItemName.Text);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void BtnHide_Click(object sender, EventArgs e)
        {
            grSearch.Visible = false;
        }

        private void DataGridAttribute_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                DataGridViewComboBoxCell l_objGridDropbox = new DataGridViewComboBoxCell();
                //if (DataGridAttribute.CurrentRow.Cells[2].Value != null)
                //{
                //    int valueid = Convert.ToInt32(DataGridAttribute.CurrentRow.Cells[2].Value.ToString());
                //    bool usedefined = Convert.ToBoolean(DataGridAttribute.CurrentRow.Cells[3].Value.ToString());
                //    if (e.ColumnIndex == 1 && usedefined == false)
                //    {
                //        nm = Ctype(Convert.ToDecimal(txtItemName.Tag), valueid);
                //        l_objGridDropbox.DisplayMember = "AttributeValue";
                //        l_objGridDropbox.ValueMember = "ValueUid";
                //        l_objGridDropbox.DataSource = nm;
                //        DataGridAttribute[e.ColumnIndex, e.RowIndex] = l_objGridDropbox;
                //    }
                //}
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private DataTable Ctype(decimal ItemUid, decimal ValueId)
        {
            DataTable tab = new DataTable();
            SqlParameter[] parameters = { new SqlParameter("@ItemUId", ItemUid), new SqlParameter("@AttributeValueId", ValueId) };
            string Query = @"Select b.UID as ValueUid,b.AttributeValue from ItemsMAttribute a 
                            Inner join  ItemsMAttributeValues b on a.Uid = b.AttributeValueUID
                            inner join  GeneralM c on a.AttributeUid = c.GUid and c.TypemUid = 28
                            Where a.ItemUId = " + ItemUid + " and b.AttributeValueUID = " + ValueId + "";
            DataTable dt = db.GetDataWithoutParam(CommandType.Text, Query, sqlconnection);
            return dt;
        }

        private void DataGridAttribute_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            try
            {
                //if (DataGridAttribute.CurrentCell.ColumnIndex == 1 && DataGridAttribute.CurrentRow.Cells[3].Value != null)
                //{
                //    if (DataGridAttribute.CurrentCell.ColumnIndex == DataGridAttribute.CurrentRow.Cells[1].ColumnIndex)
                //    {
                //        if (e.Control is ComboBox)
                //        {
                //            //cbxRow5.SelectionChangeCommitted += new EventHandler(CbxItemCode_SelectionChangeCommitted);
                //            //cbxRow5.SelectionChangeCommitted -= new EventHandler(CbxItemCode_SelectionChangeCommitted);
                //        }
                //    }
                //}
            }
            catch (Exception)
            {
                //MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                //return;
            }
        }

        private void CbxItemCode_SelectionChangeCommitted(object sender, EventArgs e)
        {
            //this.DataGridAttribute.CurrentRow.Cells[1].Value = this.cbxRow5.Text;
        }

        private void TxtOrderNO_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode.Equals(Keys.Up))
                {
                    MoveUp(DataGridCommon);
                    DataGridCommon.PerformLayout();
                }
                if (e.KeyCode.Equals(Keys.Down))
                {
                    MoveDown(DataGridCommon);
                    DataGridCommon.PerformLayout();
                }
                if (e.KeyCode.Equals(Keys.Enter))
                {
                    SelectId = 1;
                    SelectId = 1;
                    int Index = DataGridCommon.SelectedCells[0].RowIndex;
                    if (Fillid == 1)
                    {
                        txtOrderNO.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                        txtOrderNO.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                        GetOrderMEdit(Convert.ToDecimal(txtOrderNO.Tag));
                    }
                    else if (Fillid == 2)
                    {
                        txtCtgy.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                        txtCtgy.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    }
                    else if (Fillid == 3)
                    {
                        txtItemName.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                        txtItemName.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                        GetAttribute(Convert.ToDecimal(txtItemName.Tag));
                    }
                    grSearch.Visible = false;
                    SelectId = 0;
                }
                e.Handled = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void MoveUp(DataGridView dataGridView)
        {
            if (dataGridView.RowCount > 0)
            {
                int rowCount = dataGridView.Rows.Count;
                int index = dataGridView.SelectedCells[0].OwningRow.Index;

                if (index == 0)
                {
                    return;
                }
                dataGridView.ClearSelection();
                dataGridView.Rows[index - 1].Selected = true;
            }
        }

        private void MoveDown(DataGridView dataGridView)
        {
            if (dataGridView.RowCount > 0)
            {
                int rowCount = dataGridView.Rows.Count;
                int index = dataGridView.SelectedCells[0].OwningRow.Index;
                if (index == (rowCount - 1)) // include the header row
                {
                    return;
                }
                dataGridView.ClearSelection();
                dataGridView.Rows[index + 1].Selected = true;
            }
        }

        protected bool CheckGridValue()
        {
            bool entryFound = false;
            try
            {
                foreach (DataGridViewRow row in DataGridMBOMItem.Rows)
                {
                    object val2 = row.Cells[2].Value;
                    object val3 = row.Cells[14].Value;
                    object val4 = row.Cells[1].Value;
                    if (val2 != null && val2.ToString() == txtItemName.Text && val3 != null && val3.ToString() == txtMBomClour.Text && val4.ToString() == CmbType.Text)
                    {
                        entryFound = true;
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            return entryFound;
        }

        private void BtnAddItem_Click(object sender, EventArgs e)
        {
            try
            {
                bool checkvalue = CheckGridValue();
                if (checkvalue == false)
                {
                    if (txtItemName.Text != string.Empty)
                    {
                        if (CmbType.Text == "Order Wise")
                        {
                            decimal Qty = Convert.ToDecimal(dtOrderQty.Rows[0]["OrderQty"].ToString());
                            if (txtExcess.Text == string.Empty)
                            {
                                txtExcess.Text = "0";
                            }
                            decimal Excess = Convert.ToDecimal(txtExcess.Text);
                            decimal v = (Qty * Excess) / 100;
                            decimal tvalue = (Qty + v) * Convert.ToDecimal(txtQtyUsed.Text);
                            SqlParameter[] sqlParameters = { new SqlParameter("@CategoryUid", txtCtgy.Tag) };
                            DataTable dataTable = db.GetDataWithParam(CommandType.StoredProcedure, "Proc_GetAlternateUomQty", sqlParameters, sqlconnection);
                            if (!string.IsNullOrEmpty(dataTable.Rows[0]["BaseUomQty"].ToString()))
                            {
                                decimal uom = Convert.ToDecimal(dataTable.Rows[0]["BaseUomQty"].ToString());
                                Qty = tvalue / uom;
                            }
                            else
                            {
                                Qty = tvalue / Convert.ToDecimal(txtQtyPer.Text);
                            }
                            if (txtRate.Text == string.Empty)
                            {
                                txtRate.Text = "0";
                            }
                            decimal totalValue = (Convert.ToDecimal(txtRate.Text) * Qty);
                            int index = DataGridMBOMItem.Rows.Add();
                            DataGridViewRow row = (DataGridViewRow)DataGridMBOMItem.Rows[index];
                            row.Cells[0].Value = DataGridMBOMItem.Rows.Count;
                            row.Cells[1].Value = "OrderWise";
                            row.Cells[2].Value = txtItemName.Text;
                            row.Cells[3].Value = txtQtyPer.Text;
                            row.Cells[4].Value = txtQtyUsed.Text;
                            row.Cells[5].Value = txtExcess.Text;
                            row.Cells[6].Value = txtRate.Text;
                            row.Cells[7].Value = Qty.ToString("0");
                            row.Cells[8].Value = "0";
                            row.Cells[9].Value = Qty.ToString("0");
                            row.Cells[10].Value = totalValue.ToString("0.00");
                            row.Cells[11].Value = txtCtgy.Tag;
                            row.Cells[12].Value = "0";
                            row.Cells[13].Value = txtItemName.Tag;
                            row.Cells[14].Value = "";
                            row.Cells[15].Value = CmbType.Text;
                            row.Cells[16].Value = "";
                            row.Cells[17].Value = "";
                            row.Cells[18].Value = "";
                            row.Cells[19].Value = "0";
                            row.Cells[20].Value = "0";
                            row.Cells[21].Value = "0";
                            row.Cells[22].Value = dataTable.Rows[0]["UOM"].ToString();
                            row.Cells[23].Value = "";
                            row.Cells[24].Value = cbomaterialtype.Text;
                        }
                        else if (CmbType.Text == "Style Wise")
                        {
                            foreach (DataGridViewRow row2 in DataGridStyleWise.Rows)
                            {
                                DataGridViewCheckBoxCell cellComponent = row2.Cells[0] as DataGridViewCheckBoxCell;
                                if (cellComponent.Value != null)
                                {
                                    if (cellComponent.Value.ToString() == "True")
                                    {
                                        decimal Qty = 0;
                                        DataTable dt = dtStyleQty.Select("Uid =" + row2.Cells[2].Value.ToString() + "").CopyToDataTable();
                                        Qty = Convert.ToDecimal(dt.Rows[0]["StylePlanQty"].ToString());
                                        if (txtExcess.Text == string.Empty)
                                        {
                                            txtExcess.Text = "0";
                                        }
                                        decimal Excess = Convert.ToDecimal(txtExcess.Text);
                                        decimal v = (Qty * Excess) / 100;
                                        decimal tvalue = (Qty + v) * Convert.ToDecimal(txtQtyUsed.Text);
                                        SqlParameter[] sqlParameters = { new SqlParameter("@CategoryUid", txtCtgy.Tag) };
                                        DataTable dataTable = db.GetDataWithParam(CommandType.StoredProcedure, "Proc_GetAlternateUomQty", sqlParameters, sqlconnection);
                                        if (!string.IsNullOrEmpty(dataTable.Rows[0]["BaseUomQty"].ToString()))
                                        {
                                            decimal uom = Convert.ToDecimal(dataTable.Rows[0]["BaseUomQty"].ToString());
                                            Qty = tvalue / uom;
                                        }
                                        else
                                        {
                                            Qty = tvalue / Convert.ToDecimal(txtQtyPer.Text);
                                        }
                                        if (txtRate.Text == string.Empty)
                                        {
                                            txtRate.Text = "0";
                                        }
                                        decimal totalValue = (Convert.ToDecimal(txtRate.Text) * Qty);
                                        int index = DataGridMBOMItem.Rows.Add();
                                        DataGridViewRow row = (DataGridViewRow)DataGridMBOMItem.Rows[index];
                                        row.Cells[0].Value = DataGridMBOMItem.Rows.Count;
                                        row.Cells[1].Value = "StyleWise";
                                        row.Cells[2].Value = txtItemName.Text;
                                        row.Cells[3].Value = txtQtyPer.Text;
                                        row.Cells[4].Value = txtQtyUsed.Text;
                                        row.Cells[5].Value = txtExcess.Text;
                                        row.Cells[6].Value = txtRate.Text;
                                        row.Cells[7].Value = Qty.ToString("0");
                                        row.Cells[8].Value = "0";
                                        row.Cells[9].Value = Qty.ToString("0");
                                        row.Cells[10].Value = totalValue.ToString("0.00");
                                        row.Cells[11].Value = txtCtgy.Tag;
                                        row.Cells[12].Value = row2.Cells[2].Value.ToString();
                                        row.Cells[13].Value = txtItemName.Tag;
                                        row.Cells[14].Value = "";
                                        row.Cells[15].Value = CmbType.Text;
                                        row.Cells[16].Value = row2.Cells[1].Value.ToString(); ;
                                        row.Cells[17].Value = "";
                                        row.Cells[18].Value = "";
                                        row.Cells[19].Value = "0";
                                        row.Cells[20].Value = "0";
                                        row.Cells[21].Value = "0";
                                        row.Cells[22].Value = dataTable.Rows[0]["UOM"].ToString();
                                        row.Cells[23].Value = "";
                                        row.Cells[24].Value = cbomaterialtype.Text;
                                    }
                                }
                            }
                        }
                        else if (CmbType.Text == "Combo Wise")
                        {
                            foreach (DataGridViewRow row1 in DataGridComboStyle.Rows)
                            {
                                DataGridViewCheckBoxCell cellComponent1 = row1.Cells[0] as DataGridViewCheckBoxCell;
                                if (cellComponent1.Value != null)
                                {
                                    if (cellComponent1.Value.ToString() == "True")
                                    {
                                        foreach (DataGridViewRow row2 in DataGridComboWise.Rows)
                                        {
                                            DataGridViewCheckBoxCell cellComponent = row2.Cells[0] as DataGridViewCheckBoxCell;
                                            if (cellComponent.Value != null)
                                            {
                                                if (cellComponent.Value.ToString() == "True")
                                                {
                                                    DataTable dt = dtComboQty.Select("Uid =" + row2.Cells[2].Value.ToString() + "").CopyToDataTable();
                                                    decimal Qty = Convert.ToDecimal(dt.Rows[0]["ComboQty"].ToString());
                                                    if (txtExcess.Text == string.Empty)
                                                    {
                                                        txtExcess.Text = "0";
                                                    }
                                                    decimal Excess = Convert.ToDecimal(txtExcess.Text);
                                                    decimal v = (Qty * Excess) / 100;
                                                    if (txtQtyUsed.Text == string.Empty)
                                                    {
                                                        txtQtyUsed.Text = "0";
                                                    }
                                                    decimal tvalue = (Qty + v) * Convert.ToDecimal(txtQtyUsed.Text);
                                                    SqlParameter[] sqlParameters = { new SqlParameter("@CategoryUid", txtCtgy.Tag) };
                                                    DataTable dataTable = db.GetDataWithParam(CommandType.StoredProcedure, "Proc_GetAlternateUomQty", sqlParameters, sqlconnection);
                                                    if (!string.IsNullOrEmpty(dataTable.Rows[0]["BaseUomQty"].ToString()))
                                                    {
                                                        decimal uom = Convert.ToDecimal(dataTable.Rows[0]["BaseUomQty"].ToString());
                                                        Qty = tvalue / uom;
                                                    }
                                                    else
                                                    {
                                                        Qty = tvalue / Convert.ToDecimal(txtQtyPer.Text);
                                                    }
                                                    if (txtRate.Text == string.Empty)
                                                    {
                                                        txtRate.Text = "0";
                                                    }
                                                    decimal totalValue = (Convert.ToDecimal(txtRate.Text) * Qty);
                                                    int index = DataGridMBOMItem.Rows.Add();
                                                    DataGridViewRow row = (DataGridViewRow)DataGridMBOMItem.Rows[index];
                                                    row.Cells[0].Value = DataGridMBOMItem.Rows.Count;
                                                    row.Cells[1].Value = "ComboWise";
                                                    row.Cells[2].Value = txtItemName.Text;
                                                    row.Cells[3].Value = txtQtyPer.Text;
                                                    row.Cells[4].Value = txtQtyUsed.Text;
                                                    row.Cells[5].Value = txtExcess.Text;
                                                    row.Cells[6].Value = txtRate.Text;
                                                    row.Cells[7].Value = Qty.ToString("0.00");
                                                    row.Cells[8].Value = "0";
                                                    row.Cells[9].Value = Qty.ToString("0");
                                                    row.Cells[10].Value = totalValue.ToString("0.00");
                                                    row.Cells[11].Value = txtCtgy.Tag;
                                                    row.Cells[12].Value = row1.Cells[2].Value.ToString();
                                                    row.Cells[13].Value = txtItemName.Tag;
                                                    row.Cells[14].Value = txtMBomClour.Text;
                                                    row.Cells[15].Value = CmbType.Text;
                                                    row.Cells[16].Value = row1.Cells[1].Value.ToString();
                                                    row.Cells[17].Value = row2.Cells[1].Value.ToString();
                                                    row.Cells[18].Value = "";
                                                    row.Cells[19].Value = row2.Cells[2].Value.ToString();
                                                    row.Cells[20].Value = "0";
                                                    row.Cells[21].Value = "0";
                                                    row.Cells[22].Value = dataTable.Rows[0]["UOM"].ToString();
                                                    row.Cells[23].Value = "";
                                                    row.Cells[24].Value = cbomaterialtype.Text;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        else if (CmbType.Text == "Size Wise")
                        {
                            foreach (DataGridViewRow row1 in DataGridSizeCombo.Rows)
                            {
                                DataGridViewCheckBoxCell cellComponent1 = row1.Cells[0] as DataGridViewCheckBoxCell;
                                if (cellComponent1.Value != null)
                                {
                                    if (cellComponent1.Value.ToString() == "True")
                                    {
                                        foreach (DataGridViewRow row2 in DataGridSizeWise.Rows)
                                        {
                                            DataGridViewCheckBoxCell cellComponent = row2.Cells[0] as DataGridViewCheckBoxCell;
                                            if (cellComponent.Value != null)
                                            {
                                                decimal Qty;
                                                if (cellComponent.Value.ToString() == "True")
                                                {
                                                    DataRow[] dataRows = dtSizeQty.Select("SizeUid =" + row2.Cells[2].Value.ToString() + " and ComboUid=" + row1.Cells[2].Value.ToString() + "");
                                                    if (dataRows.Length == 0)
                                                    {
                                                        Qty = 0;
                                                    }
                                                    else
                                                    {
                                                        DataTable dt = dtSizeQty.Select("SizeUid =" + row2.Cells[2].Value.ToString() + " and ComboUid=" + row1.Cells[2].Value.ToString() + "").CopyToDataTable();
                                                        Qty = Convert.ToDecimal(dt.Rows[0]["SizeQty"].ToString());
                                                    }

                                                    if (txtExcess.Text == string.Empty)
                                                    {
                                                        txtExcess.Text = "0";
                                                    }
                                                    decimal Excess = Convert.ToDecimal(txtExcess.Text);
                                                    decimal v = (Qty * Excess) / 100;
                                                    decimal tvalue = (Qty + v) * Convert.ToDecimal(txtQtyUsed.Text);
                                                    SqlParameter[] sqlParameters = { new SqlParameter("@CategoryUid", txtCtgy.Tag) };
                                                    DataTable dataTable = db.GetDataWithParam(CommandType.StoredProcedure, "Proc_GetAlternateUomQty", sqlParameters, sqlconnection);
                                                    if (!string.IsNullOrEmpty(dataTable.Rows[0]["BaseUomQty"].ToString()))
                                                    {
                                                        decimal uom = Convert.ToDecimal(dataTable.Rows[0]["BaseUomQty"].ToString());
                                                        Qty = tvalue / uom;
                                                    }
                                                    else
                                                    {
                                                        Qty = tvalue / Convert.ToDecimal(txtQtyPer.Text);
                                                    }
                                                    if (txtRate.Text == string.Empty)
                                                    {
                                                        txtRate.Text = "0";
                                                    }
                                                    decimal totalValue = (Convert.ToDecimal(txtRate.Text) * Qty);
                                                    int index = DataGridMBOMItem.Rows.Add();
                                                    DataGridViewRow row = (DataGridViewRow)DataGridMBOMItem.Rows[index];
                                                    row.Cells[0].Value = DataGridMBOMItem.Rows.Count;
                                                    row.Cells[1].Value = "SizeWise";
                                                    row.Cells[2].Value = txtItemName.Text;
                                                    row.Cells[3].Value = txtQtyPer.Text;
                                                    row.Cells[4].Value = txtQtyUsed.Text;
                                                    row.Cells[5].Value = txtExcess.Text;
                                                    row.Cells[6].Value = txtRate.Text;
                                                    row.Cells[7].Value = Qty.ToString("0");
                                                    row.Cells[8].Value = "0";
                                                    row.Cells[9].Value = Qty.ToString("0");
                                                    row.Cells[10].Value = totalValue.ToString("0.00");
                                                    row.Cells[11].Value = txtCtgy.Tag;
                                                    row.Cells[12].Value = "0";
                                                    row.Cells[13].Value = txtItemName.Tag;
                                                    row.Cells[14].Value = "";
                                                    row.Cells[15].Value = CmbType.Text;

                                                    row.Cells[16].Value = "";
                                                    row.Cells[17].Value = row1.Cells[1].Value.ToString();
                                                    row.Cells[18].Value = row2.Cells[1].Value.ToString();
                                                    row.Cells[19].Value = row1.Cells[2].Value.ToString();
                                                    row.Cells[20].Value = row2.Cells[2].Value.ToString();
                                                    row.Cells[21].Value = "0";
                                                    row.Cells[22].Value = dataTable.Rows[0]["UOM"].ToString();
                                                    row.Cells[23].Value = row2.Cells[3].Value.ToString();
                                                    row.Cells[24].Value = cbomaterialtype.Text;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void DataGridAttribute_MouseLeave(object sender, EventArgs e)
        {
            try
            {
                string Namenn = string.Empty;
                //for (int i = 0; i < DataGridAttribute.Rows.Count ; i++)
                //{
                //    string SelectedText = string.Empty;
                //    if (DataGridAttribute.Rows[i].Cells[3].Value.ToString() == "False")
                //    {
                //        Namenn += Convert.ToString((DataGridAttribute.Rows[i].Cells[1] as DataGridViewComboBoxCell).FormattedValue.ToString());
                //    }
                //    else
                //    {
                //        Namenn += DataGridAttribute.Rows[i].Cells[1].Value.ToString();
                //    }
                //}
                //txtItemDescription.Text = txtItemName.Text + "( " + Namenn + " )";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void SplitSave_DropDowItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            try
            {
                if (e.ClickedItem.Text == "Back")
                {
                    grFront.Visible = true;
                    grBack.Visible = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void SplitSave_Click(object sender, EventArgs e)
        {
            try
            {
                for (int i = 0; i < DataGridMBOMItem.Rows.Count; i++)
                {
                    SqlParameter[] sqlParameters = {
                        new SqlParameter("@Uid",DataGridMBOMItem.Rows[i].Cells[21].Value.ToString()),
                        new SqlParameter("@OrderMUid",txtOrderNO.Tag),
                        new SqlParameter("@RefId","0"),
                        new SqlParameter("@ItemUid",DataGridMBOMItem.Rows[i].Cells[13].Value.ToString()),
                        new SqlParameter("@ItemSpec",DataGridMBOMItem.Rows[i].Cells[2].Value.ToString()),
                        new SqlParameter("@QtyPer",DataGridMBOMItem.Rows[i].Cells[3].Value.ToString()),
                        new SqlParameter("@QtConsumed",DataGridMBOMItem.Rows[i].Cells[4].Value.ToString()),
                        new SqlParameter("@Excess",DataGridMBOMItem.Rows[i].Cells[5].Value.ToString()),
                        new SqlParameter("@Rate",DataGridMBOMItem.Rows[i].Cells[6].Value.ToString()),
                        new SqlParameter("@ReqQty",DataGridMBOMItem.Rows[i].Cells[7].Value.ToString()),
                        new SqlParameter("@ExcessQty",DataGridMBOMItem.Rows[i].Cells[8].Value.ToString()),
                        new SqlParameter("@TotalQty",DataGridMBOMItem.Rows[i].Cells[9].Value.ToString()),
                        new SqlParameter("@TotalValue",DataGridMBOMItem.Rows[i].Cells[10].Value.ToString()),
                        new SqlParameter("@CtgyID",DataGridMBOMItem.Rows[i].Cells[11].Value.ToString()),
                        new SqlParameter("@CreateDate",Convert.ToDateTime( DateTime.Now)),
                        new SqlParameter("@UserID",GeneralParameters.UserdId),
                        new SqlParameter("@DocNo",txtDcoNo.Text),
                        new SqlParameter("@DocDate",Convert.ToDateTime(DocDate.Text)),
                        new SqlParameter("@OrderType",DataGridMBOMItem.Rows[i].Cells[15].Value.ToString()),
                        new SqlParameter("@Colour",DataGridMBOMItem.Rows[i].Cells[14].Value.ToString()),
                        new SqlParameter("@StyleUid",DataGridMBOMItem.Rows[i].Cells[12].Value.ToString()),
                        new SqlParameter("@ComboUid",DataGridMBOMItem.Rows[i].Cells[19].Value.ToString()),
                        new SqlParameter("@SizeUid",DataGridMBOMItem.Rows[i].Cells[20].Value.ToString()),
                        new SqlParameter("@UOM",DataGridMBOMItem.Rows[i].Cells[22].Value.ToString()),
                        new SqlParameter("@Notes",DataGridMBOMItem.Rows[i].Cells[23].Value.ToString()),
                        new SqlParameter("@MaterialType",DataGridMBOMItem.Rows[i].Cells[24].Value.ToString())
                    };
                    db.ExecuteNonQuery(CommandType.StoredProcedure, "Proc_OrderMMaterialBomNew", sqlParameters, sqlconnection);
                }
                if (txtOrderNO.Tag.ToString() == "0")
                {
                    string Query = "Update DocTypeM Set Lastno = Lastno+1 Where DocTypeId = 3";
                    db.ExecuteNonQuery(CommandType.Text, Query, sqlconnection);
                }
                MessageBox.Show("Record has been saved successfully", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                DataGridMBOMItem.Rows.Clear();
                grFront.Visible = true;
                grBack.Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void SpliAdd_DropDowItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            try
            {
                if (e.ClickedItem.Text == "Close")
                {
                    this.Close();
                }
                else if (e.ClickedItem.Text == "Edit")
                {
                    if (SfdDataGridBOM.SelectedIndex == -1)
                    {
                        MessageBox.Show("Select a row", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }

                    var selectedItem = SfdDataGridBOM.SelectedItems[0];
                    var dataRow = (selectedItem as DataRowView).Row;
                    decimal CUid = Convert.ToDecimal(dataRow["OrderMuid"].ToString());
                    string Query = "select * from OrderMBudjet Where OrdermUid =" + CUid + "";
                    DataTable dataTable = db.GetDataWithoutParam(CommandType.Text, Query, sqlconnection);
                    if (dataTable.Rows.Count > 0)
                    {
                        if (dataTable.Rows[0]["Sts"].ToString() == "Pending")
                        {
                            SqlParameter[] sqlParametes = { new SqlParameter("@OrderMUid", CUid) };
                            DataTable data = db.GetDataWithParam(CommandType.StoredProcedure, "Proc_OrderMMaterialBOMEdit", sqlParametes, sqlconnection);
                            txtOrderNO.Text = dataRow["OrderNo"].ToString();
                            txtOrderNO.Tag = CUid;
                            txtDcoNo.Text = dataRow["DocNo"].ToString();
                            OrderDate.Text = dataRow["OrderDt"].ToString();
                            DocDate.Text = dataRow["DocDate"].ToString();

                            SqlParameter[] sqlParameters = { new SqlParameter("@OrderMuid", txtOrderNO.Tag) };
                            DataSet dataSetQty = db.GetDataWithParamMultiple(CommandType.StoredProcedure, "Proc_GetOrderTotalQtyForMBOM", sqlParameters, sqlconnection);
                            dtOrderQty = dataSetQty.Tables[0];
                            dtStyleQty = dataSetQty.Tables[1];
                            dtComboQty = dataSetQty.Tables[2];
                            dtSizeQty = dataSetQty.Tables[3];
                            DataGridMBOMItem.Rows.Clear();
                            SelectId = 1;
                            for (int i = 0; i < data.Rows.Count; i++)
                            {
                                int index = DataGridMBOMItem.Rows.Add();
                                DataGridViewRow row = (DataGridViewRow)DataGridMBOMItem.Rows[index];
                                row.Cells[0].Value = DataGridMBOMItem.Rows.Count;
                                row.Cells[1].Value = data.Rows[i]["OrderType"].ToString();
                                row.Cells[2].Value = data.Rows[i]["ItemSpec"].ToString();
                                row.Cells[3].Value = data.Rows[i]["QtyPer"].ToString();
                                row.Cells[4].Value = data.Rows[i]["QtConsumed"].ToString();
                                row.Cells[5].Value = data.Rows[i]["Excess"].ToString();
                                row.Cells[6].Value = data.Rows[i]["Rate"].ToString();
                                row.Cells[7].Value = data.Rows[i]["ReqQty"].ToString();
                                row.Cells[8].Value = data.Rows[i]["ExcessQty"].ToString();
                                row.Cells[9].Value = data.Rows[i]["TotalQty"].ToString();
                                row.Cells[10].Value = data.Rows[i]["TotalValue"].ToString();
                                row.Cells[11].Value = data.Rows[i]["CtgyId"].ToString();
                                row.Cells[12].Value = data.Rows[i]["StyleUid"].ToString();
                                row.Cells[13].Value = data.Rows[i]["ItemUid"].ToString();
                                row.Cells[14].Value = data.Rows[i]["Colour"].ToString();
                                row.Cells[15].Value = data.Rows[i]["OrderType"].ToString();
                                row.Cells[16].Value = data.Rows[i]["StyleName"].ToString();
                                row.Cells[17].Value = data.Rows[i]["Combo"].ToString();
                                row.Cells[18].Value = data.Rows[i]["SizeName"].ToString();
                                row.Cells[19].Value = data.Rows[i]["Comboid"].ToString();
                                row.Cells[20].Value = data.Rows[i]["SizeUid"].ToString();
                                row.Cells[21].Value = data.Rows[i]["Uid"].ToString();
                                row.Cells[22].Value = data.Rows[i]["UOM"].ToString();
                                row.Cells[23].Value = data.Rows[i]["Notes"].ToString();
                                row.Cells[24].Value = data.Rows[i]["MaterialType"].ToString();
                            }
                            grBack.Visible = true;
                            grFront.Visible = false;
                            SelectId = 0;
                        }
                        else
                        {
                            MessageBox.Show("Budget Approved can't Edit", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            return;

                        }
                    }
                    else
                    {
                        SqlParameter[] sqlParametes = { new SqlParameter("@OrderMUid", CUid) };
                        DataTable data = db.GetDataWithParam(CommandType.StoredProcedure, "Proc_OrderMMaterialBOMEdit", sqlParametes, sqlconnection);
                        txtOrderNO.Text = dataRow["OrderNo"].ToString();
                        txtOrderNO.Tag = CUid;
                        txtDcoNo.Text = dataRow["DocNo"].ToString();
                        OrderDate.Text = dataRow["OrderDt"].ToString();
                        DocDate.Text = dataRow["DocDate"].ToString();

                        SqlParameter[] sqlParameters = { new SqlParameter("@OrderMuid", txtOrderNO.Tag) };
                        DataSet dataSetQty = db.GetDataWithParamMultiple(CommandType.StoredProcedure, "Proc_GetOrderTotalQtyForMBOM", sqlParameters, sqlconnection);
                        dtOrderQty = dataSetQty.Tables[0];
                        dtStyleQty = dataSetQty.Tables[1];
                        dtComboQty = dataSetQty.Tables[2];
                        dtSizeQty = dataSetQty.Tables[3];
                        DataGridMBOMItem.Rows.Clear();
                        for (int i = 0; i < data.Rows.Count; i++)
                        {
                            int index = DataGridMBOMItem.Rows.Add();
                            DataGridViewRow row = (DataGridViewRow)DataGridMBOMItem.Rows[index];
                            row.Cells[0].Value = DataGridMBOMItem.Rows.Count;
                            row.Cells[1].Value = data.Rows[i]["OrderType"].ToString();
                            row.Cells[2].Value = data.Rows[i]["ItemSpec"].ToString();
                            row.Cells[3].Value = data.Rows[i]["QtyPer"].ToString();
                            row.Cells[4].Value = data.Rows[i]["QtConsumed"].ToString();
                            row.Cells[5].Value = data.Rows[i]["Excess"].ToString();
                            row.Cells[6].Value = data.Rows[i]["Rate"].ToString();
                            row.Cells[7].Value = data.Rows[i]["ReqQty"].ToString();
                            row.Cells[8].Value = data.Rows[i]["ExcessQty"].ToString();
                            row.Cells[9].Value = data.Rows[i]["TotalQty"].ToString();
                            row.Cells[10].Value = data.Rows[i]["TotalValue"].ToString();
                            row.Cells[11].Value = data.Rows[i]["CtgyId"].ToString();
                            row.Cells[12].Value = data.Rows[i]["StyleUid"].ToString();
                            row.Cells[13].Value = data.Rows[i]["ItemUid"].ToString();
                            row.Cells[14].Value = data.Rows[i]["Colour"].ToString();
                            row.Cells[15].Value = data.Rows[i]["OrderType"].ToString();
                            row.Cells[16].Value = data.Rows[i]["StyleName"].ToString();
                            row.Cells[17].Value = data.Rows[i]["Combo"].ToString();
                            row.Cells[18].Value = data.Rows[i]["SizeName"].ToString();
                            row.Cells[19].Value = data.Rows[i]["Comboid"].ToString();
                            row.Cells[20].Value = data.Rows[i]["SizeUid"].ToString();
                            row.Cells[21].Value = data.Rows[i]["Uid"].ToString();
                            row.Cells[22].Value = data.Rows[i]["UOM"].ToString();
                            row.Cells[23].Value = data.Rows[i]["Notes"].ToString();
                        }
                        grBack.Visible = true;
                        grFront.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected void EditMBOm()
        {
            SqlParameter[] sqlParametes = { new SqlParameter("@OrderMUid", txtOrderNO.Tag) };
            DataTable data = db.GetDataWithParam(CommandType.StoredProcedure, "Proc_OrderMMaterialBOMEdit", sqlParametes, sqlconnection);
            DataGridMBOMItem.Rows.Clear();
            for (int i = 0; i < data.Rows.Count; i++)
            {
                int index = DataGridMBOMItem.Rows.Add();
                DataGridViewRow row = (DataGridViewRow)DataGridMBOMItem.Rows[index];
                row.Cells[0].Value = DataGridMBOMItem.Rows.Count;
                row.Cells[1].Value = data.Rows[i]["OrderType"].ToString();
                row.Cells[2].Value = data.Rows[i]["ItemSpec"].ToString();
                row.Cells[3].Value = data.Rows[i]["QtyPer"].ToString();
                row.Cells[4].Value = data.Rows[i]["QtConsumed"].ToString();
                row.Cells[5].Value = data.Rows[i]["Excess"].ToString();
                row.Cells[6].Value = data.Rows[i]["Rate"].ToString();
                row.Cells[7].Value = data.Rows[i]["ReqQty"].ToString();
                row.Cells[8].Value = data.Rows[i]["ExcessQty"].ToString();
                row.Cells[9].Value = data.Rows[i]["TotalQty"].ToString();
                row.Cells[10].Value = data.Rows[i]["TotalValue"].ToString();
                row.Cells[11].Value = data.Rows[i]["CtgyId"].ToString();
                row.Cells[12].Value = data.Rows[i]["StyleUid"].ToString();
                row.Cells[13].Value = data.Rows[i]["ItemUid"].ToString();
                row.Cells[14].Value = data.Rows[i]["Colour"].ToString();
                row.Cells[15].Value = data.Rows[i]["OrderType"].ToString();
                row.Cells[16].Value = data.Rows[i]["StyleName"].ToString();
                row.Cells[17].Value = data.Rows[i]["Combo"].ToString();
                row.Cells[18].Value = data.Rows[i]["SizeName"].ToString();
                row.Cells[19].Value = data.Rows[i]["Comboid"].ToString();
                row.Cells[20].Value = data.Rows[i]["SizeUid"].ToString();
                row.Cells[21].Value = data.Rows[i]["Uid"].ToString();
                row.Cells[22].Value = data.Rows[i]["UOM"].ToString();
                row.Cells[23].Value = data.Rows[i]["Notes"].ToString();
            }
        }

        private void SpliAdd_Click(object sender, EventArgs e)
        {
            try
            {
                decimal DocTyeId = 3;
                grFront.Visible = false;
                grBack.Visible = true;
                CmbType.SelectedIndex = 1;
                DataGridMBOMItem.Rows.Clear();
                txtItemName.Text = string.Empty;
                txtCtgy.Text = string.Empty;
                txtQtyUsed.Text = string.Empty;
                txtRate.Text = string.Empty;
                txtQtyPer.Text = string.Empty;
                txtExcess.Text = string.Empty;
                txtOrderNO.Text = string.Empty;
                txtDcoNo.Text = string.Empty;
                txtOrderNO.Tag = "0";
                txtDcoNo.Text = GeneralParameters.GetDocNo(DocTyeId, sqlconnection);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void SpliAdd_Click_1(object sender, EventArgs e)
        {
            SpliAdd_Click(sender, e);
        }

        private void SplitSave_Click_1(object sender, EventArgs e)
        {
            SplitSave_Click(sender, e);
        }

        protected void LoadBOM()
        {
            try
            {
                DataTable dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "SP_OrderMMaterialBom", sqlconnection);
                SfdDataGridBOM.DataSource = null;
                SfdDataGridBOM.DataSource = dt;
                SfdDataGridBOM.Columns[3].Width = 230;
                SfdDataGridBOM.Columns[5].Width = 500;
                SfdDataGridBOM.Columns[6].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void CmbCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    tabControlAdv1.TabPages.Remove(this.tabSizeWise);
                    tabControlAdv1.TabPages.Remove(this.tabStyleWise);
                    tabControlAdv1.TabPages.Remove(this.tabComboWise);
                    tabControlAdv1.TabPages.Remove(this.tabOrderWise);
                    if (CmbType.Text == "Order Wise")
                    {
                        tabControlAdv1.TabPages.Insert(0, this.tabOrderWise);

                        DataTable dataTable = GetorderStyle(Convert.ToDecimal(txtOrderNO.Tag));
                        FillStyleGrid(DataGridOrderStyle, dataTable);

                        DataTable dataTable1 = GetorderCombo(Convert.ToDecimal(txtOrderNO.Tag));
                        FillCmboGrid(DataGridOrderCombo, dataTable1);

                        DataTable dataTable2 = GetorderSize(Convert.ToDecimal(txtOrderNO.Tag));
                        FillSizeGrid(DataGridOrderSize, dataTable2);
                    }
                    else if (CmbType.Text == "Style Wise")
                    {
                        tabControlAdv1.TabPages.Insert(0, this.tabStyleWise);

                        DataTable dataTable = GetorderStyle(Convert.ToDecimal(txtOrderNO.Tag));
                        FillStyleGrid(DataGridStyleWise, dataTable);
                    }
                    else if (CmbType.Text == "Combo Wise")
                    {
                        tabControlAdv1.TabPages.Insert(0, this.tabComboWise);
                        DataTable dataTable = GetorderStyle(Convert.ToDecimal(txtOrderNO.Tag));
                        FillStyleGrid(DataGridComboStyle, dataTable);

                        DataTable dataTable1 = GetorderCombo(Convert.ToDecimal(txtOrderNO.Tag));
                        FillCmboGrid(DataGridComboWise, dataTable1);
                    }
                    else if (CmbType.Text == "Size Wise")
                    {
                        tabControlAdv1.TabPages.Insert(0, this.tabSizeWise);
                        DataTable dataTable1 = GetorderCombo(Convert.ToDecimal(txtOrderNO.Tag));
                        FillCmboGrid(DataGridSizeCombo, dataTable1);

                        DataTable dataTable2 = GetorderSize(Convert.ToDecimal(txtOrderNO.Tag));
                        FillSizeGrid(DataGridSizeWise, dataTable2);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected void LoadDatagridStyleWise()
        {
            try
            {
                DataGridStyleWise.AutoGenerateColumns = false;
                DataGridStyleWise.ColumnCount = 2;
                DataGridViewCheckBoxColumn checkBoxColumn = new DataGridViewCheckBoxColumn
                {
                    Name = "Chck",
                    HeaderText = "Chck",
                    Width = 50
                };
                DataGridStyleWise.Columns.Insert(0, checkBoxColumn);

                DataGridStyleWise.Columns[1].Name = "Style";
                DataGridStyleWise.Columns[1].HeaderText = "Style";
                DataGridStyleWise.Columns[1].Width = 150;

                DataGridStyleWise.Columns[2].Name = "StyleUid";
                DataGridStyleWise.Columns[2].HeaderText = "StyleUid";
                DataGridStyleWise.Columns[2].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected void LoadDatagridOrderStyle()
        {
            try
            {
                DataGridOrderStyle.AutoGenerateColumns = false;
                DataGridOrderStyle.ColumnCount = 2;
                DataGridViewCheckBoxColumn checkBoxColumn = new DataGridViewCheckBoxColumn
                {
                    Name = "Chck",
                    HeaderText = "Chck",
                    Width = 50
                };
                DataGridOrderStyle.Columns.Insert(0, checkBoxColumn);

                DataGridOrderStyle.Columns[1].Name = "Style";
                DataGridOrderStyle.Columns[1].HeaderText = "Style";
                DataGridOrderStyle.Columns[1].Width = 150;

                DataGridOrderStyle.Columns[2].Name = "StyleUid";
                DataGridOrderStyle.Columns[2].HeaderText = "StyleUid";
                DataGridOrderStyle.Columns[2].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected void LoadDatagridComboStyle()
        {
            try
            {
                DataGridComboStyle.AutoGenerateColumns = false;
                DataGridComboStyle.ColumnCount = 2;
                DataGridViewCheckBoxColumn checkBoxColumn = new DataGridViewCheckBoxColumn
                {
                    Name = "Chck",
                    HeaderText = "Chck",
                    Width = 50
                };
                DataGridComboStyle.Columns.Insert(0, checkBoxColumn);

                DataGridComboStyle.Columns[1].Name = "Style";
                DataGridComboStyle.Columns[1].HeaderText = "Style";
                DataGridComboStyle.Columns[1].Width = 150;

                DataGridComboStyle.Columns[2].Name = "StyleUid";
                DataGridComboStyle.Columns[2].HeaderText = "StyleUid";
                DataGridComboStyle.Columns[2].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected void LoadDataGridComboWise()
        {
            try
            {
                DataGridComboWise.AutoGenerateColumns = false;
                DataGridComboWise.ColumnCount = 2;
                DataGridViewCheckBoxColumn checkBoxColumn = new DataGridViewCheckBoxColumn
                {
                    Name = "Chck",
                    HeaderText = "Chck",
                    Width = 50
                };
                DataGridComboWise.Columns.Insert(0, checkBoxColumn);

                DataGridComboWise.Columns[1].Name = "Combo";
                DataGridComboWise.Columns[1].HeaderText = "Combo";
                DataGridComboWise.Columns[1].Width = 150;

                DataGridComboWise.Columns[2].Name = "ComboUid";
                DataGridComboWise.Columns[2].HeaderText = "ComboUid";
                DataGridComboWise.Columns[2].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected void LoadDataGridSizeCombo()
        {
            try
            {
                DataGridSizeCombo.AutoGenerateColumns = false;
                DataGridSizeCombo.ColumnCount = 2;
                DataGridViewCheckBoxColumn checkBoxColumn = new DataGridViewCheckBoxColumn
                {
                    Name = "Chck",
                    HeaderText = "Chck",
                    Width = 50
                };
                DataGridSizeCombo.Columns.Insert(0, checkBoxColumn);

                DataGridSizeCombo.Columns[1].Name = "Combo";
                DataGridSizeCombo.Columns[1].HeaderText = "Combo";
                DataGridSizeCombo.Columns[1].Width = 150;

                DataGridSizeCombo.Columns[2].Name = "ComboUid";
                DataGridSizeCombo.Columns[2].HeaderText = "ComboUid";
                DataGridSizeCombo.Columns[2].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected void LoadDataGridOrderCombo()
        {
            try
            {
                DataGridOrderCombo.AutoGenerateColumns = false;
                DataGridOrderCombo.ColumnCount = 2;
                DataGridViewCheckBoxColumn checkBoxColumn = new DataGridViewCheckBoxColumn
                {
                    Name = "Chck",
                    HeaderText = "Chck",
                    Width = 50
                };
                DataGridOrderCombo.Columns.Insert(0, checkBoxColumn);

                DataGridOrderCombo.Columns[1].Name = "Combo";
                DataGridOrderCombo.Columns[1].HeaderText = "Combo";
                DataGridOrderCombo.Columns[1].Width = 150;

                DataGridOrderCombo.Columns[2].Name = "ComboUid";
                DataGridOrderCombo.Columns[2].HeaderText = "ComboUid";
                DataGridOrderCombo.Columns[2].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected void LoadDataGridSizeWise()
        {
            try
            {
                DataGridSizeWise.AutoGenerateColumns = false;
                DataGridSizeWise.ColumnCount = 3;
                DataGridViewCheckBoxColumn checkBoxColumn = new DataGridViewCheckBoxColumn
                {
                    Name = "Chck",
                    HeaderText = "Chck",
                    Width = 50
                };
                DataGridSizeWise.Columns.Insert(0, checkBoxColumn);

                DataGridSizeWise.Columns[1].Name = "Size";
                DataGridSizeWise.Columns[1].HeaderText = "Size";
                DataGridSizeWise.Columns[1].Width = 150;

                DataGridSizeWise.Columns[2].Name = "SizeUid";
                DataGridSizeWise.Columns[2].HeaderText = "SizeUid";
                DataGridSizeWise.Columns[2].Visible = false;

                DataGridSizeWise.Columns[3].Name = "Notes";
                DataGridSizeWise.Columns[3].HeaderText = "Notes";
                DataGridSizeWise.Columns[3].Visible = true;
                DataGridSizeWise.Columns[3].Width = 250;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected void LoadDataGridOrderSize()
        {
            try
            {
                DataGridOrderSize.AutoGenerateColumns = false;
                DataGridOrderSize.ColumnCount = 2;
                DataGridViewCheckBoxColumn checkBoxColumn = new DataGridViewCheckBoxColumn
                {
                    Name = "Chck",
                    HeaderText = "Chck",
                    Width = 50
                };
                DataGridOrderSize.Columns.Insert(0, checkBoxColumn);

                DataGridOrderSize.Columns[1].Name = "Size";
                DataGridOrderSize.Columns[1].HeaderText = "Size";
                DataGridOrderSize.Columns[1].Width = 150;

                DataGridOrderSize.Columns[2].Name = "SizeUid";
                DataGridOrderSize.Columns[2].HeaderText = "SizeUid";
                DataGridOrderSize.Columns[2].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected void FillStyleGrid(DataGridView dataGridView, DataTable dataTable)
        {
            try
            {
                if (dataTable.Rows.Count > 0)
                {
                    dataGridView.Rows.Clear();
                    for (int i = 0; i < dataTable.Rows.Count; i++)
                    {
                        int Index = dataGridView.Rows.Add();
                        DataGridViewRow dataGrid = (DataGridViewRow)dataGridView.Rows[Index];
                        dataGrid.Cells[1].Value = dataTable.Rows[i]["StyleName"].ToString();
                        dataGrid.Cells[2].Value = dataTable.Rows[i]["Uid"].ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        protected void FillCmboGrid(DataGridView dataGridView, DataTable dataTable)
        {
            try
            {
                if (dataTable.Rows.Count > 0)
                {
                    dataGridView.Rows.Clear();
                    for (int i = 0; i < dataTable.Rows.Count; i++)
                    {
                        int Index = dataGridView.Rows.Add();
                        DataGridViewRow dataGrid = (DataGridViewRow)dataGridView.Rows[Index];
                        dataGrid.Cells[1].Value = dataTable.Rows[i]["ComboUid"].ToString();
                        dataGrid.Cells[2].Value = dataTable.Rows[i]["Uid"].ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }


        protected void FillSizeGrid(DataGridView dataGridView, DataTable dataTable)
        {
            try
            {
                if (dataTable.Rows.Count > 0)
                {
                    dataGridView.Rows.Clear();
                    for (int i = 0; i < dataTable.Rows.Count; i++)
                    {
                        int Index = dataGridView.Rows.Add();
                        DataGridViewRow dataGrid = (DataGridViewRow)dataGridView.Rows[Index];
                        dataGrid.Cells[1].Value = dataTable.Rows[i]["SizeName"].ToString();
                        dataGrid.Cells[2].Value = dataTable.Rows[i]["Uid"].ToString();
                        dataGrid.Cells[3].Value = "";
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        protected DataTable GetorderStyle(decimal orderId)
        {
            DataTable dataTable = new DataTable();
            try
            {
                SqlParameter[] parameters = { new SqlParameter("@OrderMUid", orderId) };
                dataTable = db.GetDataWithParam(CommandType.StoredProcedure, "SP_GetOrderMStyles", parameters, sqlconnection);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            return dataTable;
        }


        protected DataTable GetorderCombo(decimal orderId)
        {
            DataTable dataTable = new DataTable();
            try
            {
                string Query = "Select * from StyleCombo Where OrderMuid =" + orderId + "";
                dataTable = db.GetDataWithoutParam(CommandType.Text, Query, sqlconnection);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            return dataTable;
        }

        protected DataTable GetorderSize(decimal orderId)
        {
            DataTable dataTable = new DataTable();
            try
            {
                string Query = "Select * from StyleSize Where OrdermUid =" + orderId + "";
                dataTable = db.GetDataWithoutParam(CommandType.Text, Query, sqlconnection);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            return dataTable;
        }

        private void BtnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                if (SfdDataGridBOM.SelectedIndex != -1)
                {
                    var selectedItem = SfdDataGridBOM.SelectedItems[0];
                    var dataRow = (selectedItem as DataRowView).Row;
                    decimal CUid = Convert.ToDecimal(dataRow["OrdermUid"].ToString());
                    GeneralParameters.ReportName = "MaterialBOM";
                    GeneralParameters.ReportUid = CUid;
                    FrmReportviwer frmReportviwer = new FrmReportviwer
                    {
                        StartPosition = FormStartPosition.CenterScreen
                    };
                    frmReportviwer.ShowDialog();
                }

            }
            catch (Exception)
            {
                throw;
            }
        }

        private void DataGridMBOMItem_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Delete)
                {
                    int Index = DataGridMBOMItem.SelectedCells[0].RowIndex;
                    int value = Convert.ToInt32(DataGridMBOMItem.Rows[Index].Cells[21].Value);
                    if (value == 0)
                    {
                        DataGridMBOMItem.Rows.RemoveAt(DataGridMBOMItem.SelectedCells[0].RowIndex);
                        DataGridMBOMItem.ClearSelection();
                    }
                    else
                    {
                        DialogResult result = MessageBox.Show("Do you want to delete this record", "Infromation", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                        if (result == DialogResult.Yes)
                        {
                            string query = "delete from OrderMMaterialBom Where Uid=" + value + "";
                            db.ExecuteNonQuery(CommandType.Text, query, sqlconnection);
                            DataGridMBOMItem.Rows.RemoveAt(DataGridMBOMItem.SelectedCells[0].RowIndex);
                            DataGridMBOMItem.ClearSelection();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void BtnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                if (SfdDataGridBOM.SelectedIndex == -1)
                {
                    MessageBox.Show("Select a row", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                else
                {

                    var selectedItem = SfdDataGridBOM.SelectedItems[0];
                    var dataRow = (selectedItem as DataRowView).Row;
                    decimal CUid = Convert.ToDecimal(dataRow["OrderMuid"].ToString());
                    DialogResult dialogResult = MessageBox.Show("Dou youwant delete this record", "Information", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                    if (dialogResult == DialogResult.Yes)
                    {
                        string Query = "delete from OrderMMaterialBom Where OrderMuid = " + CUid + "";
                        db.ExecuteNonQuery(CommandType.Text, Query, sqlconnection);
                        LoadBOM();
                    }
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void ChckUpdateQty_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (ChckUpdateQty.Checked == true)
                {
                    for (int i = 0; i < DataGridMBOMItem.Rows.Count; i++)
                    {
                        int Index = DataGridMBOMItem.SelectedCells[0].RowIndex;
                        if (DataGridMBOMItem.Rows[Index].Cells[1].Value.ToString() == "Size Wise")
                        {
                            DataTable table = dtSizeQty.Select("SizeUid=" + DataGridMBOMItem.Rows[Index].Cells[20].Value.ToString() + " and ComboUid=" + DataGridMBOMItem.Rows[Index].Cells[19].Value.ToString() + "").CopyToDataTable();
                            decimal Qty;
                            decimal Excess;
                            if (table.Rows.Count > 0)
                            {
                                Qty = Convert.ToDecimal(table.Rows[0]["SizeQty"].ToString());
                            }
                            else
                            {
                                Qty = 0;
                            }
                            if (DataGridMBOMItem.Rows[Index].Cells[5].Value.ToString() == string.Empty)
                            {
                                Excess = 0;
                            }
                            decimal rate;
                            Excess = Convert.ToDecimal(DataGridMBOMItem.Rows[Index].Cells[5].Value.ToString());
                            decimal v = (Qty * Excess) / 100;
                            decimal tvalue = (Qty + v) * Convert.ToDecimal(DataGridMBOMItem.Rows[Index].Cells[4].Value.ToString());
                            SqlParameter[] sqlParameters = { new SqlParameter("@CategoryUid", DataGridMBOMItem.Rows[Index].Cells[11].Value.ToString()) };
                            DataTable dataTable = db.GetDataWithParam(CommandType.StoredProcedure, "Proc_GetAlternateUomQty", sqlParameters, sqlconnection);
                            if (!string.IsNullOrEmpty(dataTable.Rows[0]["BaseUomQty"].ToString()))
                            {
                                decimal uom = Convert.ToDecimal(dataTable.Rows[0]["BaseUomQty"].ToString());
                                Qty = tvalue / uom;
                            }
                            else
                            {
                                Qty = tvalue / Convert.ToDecimal(DataGridMBOMItem.Rows[Index].Cells[3].Value.ToString());
                            }
                            if (DataGridMBOMItem.Rows[Index].Cells[6].Value.ToString() == string.Empty)
                            {
                                rate = 0;
                            }
                            else
                            {
                                rate = Convert.ToDecimal(DataGridMBOMItem.Rows[Index].Cells[6].Value.ToString());
                            }
                            decimal totalValue = (rate * Qty);
                            string Query = @"Update OrderMMaterialBom Set ReqQty=" + Qty + ",TotalValue = " + totalValue + " Where OrderMUid = " + txtOrderNO.Tag + " and SizeUid = " + DataGridMBOMItem.Rows[Index].Cells[20].Value.ToString() + " and ComboId = " + DataGridMBOMItem.Rows[Index].Cells[19].Value.ToString() + " and OrderType='" + DataGridMBOMItem.Rows[Index].Cells[1].Value.ToString() + "'";
                            db.ExecuteNonQuery(CommandType.Text, Query, sqlconnection);
                        }
                    }
                    MessageBox.Show("Completed Successfully", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    EditMBOm();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void DataGridMBOMItem_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex == 8)
                {
                    DataGridMBOMItem.Rows[e.RowIndex].Cells[e.ColumnIndex].ReadOnly = false;
                }
                else
                {
                    DataGridMBOMItem.Rows[e.RowIndex].Cells[e.ColumnIndex].ReadOnly = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void DataGridMBOMItem_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex == 8 && SelectId == 0)
                {
                    decimal ReqQty = Convert.ToDecimal(DataGridMBOMItem.Rows[e.RowIndex].Cells[7].Value.ToString());
                    decimal ExcessQty = Convert.ToDecimal(DataGridMBOMItem.Rows[e.RowIndex].Cells[8].Value.ToString());
                    decimal TotalQty = ReqQty + ExcessQty;
                    DataGridMBOMItem.Rows[e.RowIndex].Cells[9].Value = TotalQty;
                    decimal Rate = Convert.ToDecimal(DataGridMBOMItem.Rows[e.RowIndex].Cells[6].Value.ToString());
                    DataGridMBOMItem.Rows[e.RowIndex].Cells[10].Value = (TotalQty * Rate).ToString("0.00");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
    }
}
