﻿namespace MyEasyBizAPMS
{
    partial class Frmlookup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frmlookup));
            this.Hfgp = new System.Windows.Forms.DataGridView();
            this.btnaddrcan = new System.Windows.Forms.Button();
            this.btnadd = new System.Windows.Forms.Button();
            this.txtscr1 = new System.Windows.Forms.TextBox();
            this.cbogrp = new System.Windows.Forms.ComboBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.Hfgp)).BeginInit();
            this.SuspendLayout();
            // 
            // Hfgp
            // 
            this.Hfgp.BackgroundColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.Hfgp.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Hfgp.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.Hfgp.Location = new System.Drawing.Point(2, 42);
            this.Hfgp.Name = "Hfgp";
            this.Hfgp.Size = new System.Drawing.Size(833, 248);
            this.Hfgp.TabIndex = 0;
            this.Hfgp.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.Hfgp_CellContentClick);
            this.Hfgp.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.Hfgp_CellEnter);
            this.Hfgp.DoubleClick += new System.EventHandler(this.Hfgp_DoubleClick);
            // 
            // btnaddrcan
            // 
            this.btnaddrcan.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnaddrcan.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnaddrcan.Image = ((System.Drawing.Image)(resources.GetObject("btnaddrcan.Image")));
            this.btnaddrcan.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnaddrcan.Location = new System.Drawing.Point(558, 294);
            this.btnaddrcan.Name = "btnaddrcan";
            this.btnaddrcan.Size = new System.Drawing.Size(60, 32);
            this.btnaddrcan.TabIndex = 87;
            this.btnaddrcan.Text = "Back";
            this.btnaddrcan.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnaddrcan.UseVisualStyleBackColor = false;
            this.btnaddrcan.Click += new System.EventHandler(this.btnaddrcan_Click);
            // 
            // btnadd
            // 
            this.btnadd.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnadd.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnadd.Image = ((System.Drawing.Image)(resources.GetObject("btnadd.Image")));
            this.btnadd.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnadd.Location = new System.Drawing.Point(497, 294);
            this.btnadd.Name = "btnadd";
            this.btnadd.Size = new System.Drawing.Size(55, 32);
            this.btnadd.TabIndex = 88;
            this.btnadd.Text = "Ok";
            this.btnadd.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnadd.UseVisualStyleBackColor = false;
            this.btnadd.Click += new System.EventHandler(this.btnadd_Click_1);
            // 
            // txtscr1
            // 
            this.txtscr1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtscr1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtscr1.Location = new System.Drawing.Point(2, 13);
            this.txtscr1.Margin = new System.Windows.Forms.Padding(4);
            this.txtscr1.Name = "txtscr1";
            this.txtscr1.Size = new System.Drawing.Size(345, 26);
            this.txtscr1.TabIndex = 89;
            this.txtscr1.TextChanged += new System.EventHandler(this.txtscr1_TextChanged);
            // 
            // cbogrp
            // 
            this.cbogrp.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbogrp.FormattingEnabled = true;
            this.cbogrp.Location = new System.Drawing.Point(384, 98);
            this.cbogrp.Name = "cbogrp";
            this.cbogrp.Size = new System.Drawing.Size(234, 21);
            this.cbogrp.TabIndex = 90;
            this.cbogrp.SelectedIndexChanged += new System.EventHandler(this.cbogrp_SelectedIndexChanged);
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(363, 105);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(15, 14);
            this.checkBox1.TabIndex = 91;
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // textBox1
            // 
            this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.Location = new System.Drawing.Point(348, 13);
            this.textBox1.Margin = new System.Windows.Forms.Padding(4);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(338, 26);
            this.textBox1.TabIndex = 92;
            this.textBox1.Visible = false;
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // Frmlookup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(834, 348);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.txtscr1);
            this.Controls.Add(this.btnadd);
            this.Controls.Add(this.btnaddrcan);
            this.Controls.Add(this.Hfgp);
            this.Controls.Add(this.cbogrp);
            this.Controls.Add(this.checkBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Frmlookup";
            this.Text = "LookupItems";
            this.Load += new System.EventHandler(this.Frmlookup_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Hfgp)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView Hfgp;
        private System.Windows.Forms.Button btnaddrcan;
        private System.Windows.Forms.Button btnadd;
        private System.Windows.Forms.TextBox txtscr1;
        private System.Windows.Forms.ComboBox cbogrp;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.TextBox textBox1;
    }
}