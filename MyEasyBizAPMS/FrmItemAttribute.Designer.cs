﻿namespace MyEasyBizAPMS
{
    partial class FrmItemAttribute
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Syncfusion.Windows.Forms.Tools.SplitButtonRenderer splitButtonRenderer5 = new Syncfusion.Windows.Forms.Tools.SplitButtonRenderer();
            Syncfusion.Windows.Forms.Tools.SplitButtonRenderer splitButtonRenderer6 = new Syncfusion.Windows.Forms.Tools.SplitButtonRenderer();
            this.SpitButtonSave = new Syncfusion.Windows.Forms.Tools.SplitButton();
            this.toolstripBack = new Syncfusion.Windows.Forms.Tools.toolstripitem();
            this.SplitAdd = new Syncfusion.Windows.Forms.Tools.SplitButton();
            this.toolstripEdit = new Syncfusion.Windows.Forms.Tools.toolstripitem();
            this.toolstripClose = new Syncfusion.Windows.Forms.Tools.toolstripitem();
            this.grBack = new System.Windows.Forms.GroupBox();
            this.ChckActive = new System.Windows.Forms.CheckBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtShortName = new System.Windows.Forms.TextBox();
            this.txtName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.DataGridItemCategory = new System.Windows.Forms.DataGridView();
            this.grFront = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.Cmbcategory = new System.Windows.Forms.ComboBox();
            this.sfDataGrid1 = new Syncfusion.WinForms.DataGrid.SfDataGrid();
            this.ChckSelectAll = new System.Windows.Forms.CheckBox();
            this.grBack.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridItemCategory)).BeginInit();
            this.grFront.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sfDataGrid1)).BeginInit();
            this.SuspendLayout();
            // 
            // SpitButtonSave
            // 
            this.SpitButtonSave.BackColor = System.Drawing.SystemColors.Control;
            this.SpitButtonSave.BeforeTouchSize = new System.Drawing.Size(95, 33);
            this.SpitButtonSave.DropDownItems.Add(this.toolstripBack);
            this.SpitButtonSave.DropDownPosition = Syncfusion.Windows.Forms.Tools.Position.Bottom;
            this.SpitButtonSave.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SpitButtonSave.ForeColor = System.Drawing.Color.Black;
            this.SpitButtonSave.Location = new System.Drawing.Point(535, 404);
            this.SpitButtonSave.MinimumSize = new System.Drawing.Size(75, 23);
            this.SpitButtonSave.Name = "SpitButtonSave";
            splitButtonRenderer5.SplitButton = this.SpitButtonSave;
            this.SpitButtonSave.Renderer = splitButtonRenderer5;
            this.SpitButtonSave.ShowDropDownOnButtonClick = false;
            this.SpitButtonSave.Size = new System.Drawing.Size(95, 33);
            this.SpitButtonSave.TabIndex = 6;
            this.SpitButtonSave.Text = "Save";
            this.SpitButtonSave.DropDowItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.SpitButtonSave_DropDowItemClicked);
            this.SpitButtonSave.Click += new System.EventHandler(this.SpitButtonSave_Click);
            // 
            // toolstripBack
            // 
            this.toolstripBack.Name = "toolstripBack";
            this.toolstripBack.Size = new System.Drawing.Size(23, 23);
            this.toolstripBack.Text = "Back";
            // 
            // SplitAdd
            // 
            this.SplitAdd.BackColor = System.Drawing.SystemColors.Control;
            this.SplitAdd.BeforeTouchSize = new System.Drawing.Size(100, 34);
            this.SplitAdd.DropDownItems.Add(this.toolstripEdit);
            this.SplitAdd.DropDownItems.Add(this.toolstripClose);
            this.SplitAdd.DropDownPosition = Syncfusion.Windows.Forms.Tools.Position.Bottom;
            this.SplitAdd.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SplitAdd.ForeColor = System.Drawing.Color.Black;
            this.SplitAdd.Location = new System.Drawing.Point(546, 414);
            this.SplitAdd.MinimumSize = new System.Drawing.Size(75, 23);
            this.SplitAdd.Name = "SplitAdd";
            splitButtonRenderer6.SplitButton = this.SplitAdd;
            this.SplitAdd.Renderer = splitButtonRenderer6;
            this.SplitAdd.ShowDropDownOnButtonClick = false;
            this.SplitAdd.Size = new System.Drawing.Size(100, 34);
            this.SplitAdd.TabIndex = 1;
            this.SplitAdd.Text = "Add";
            this.SplitAdd.DropDowItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.SplitAdd_DropDowItemClicked);
            this.SplitAdd.Click += new System.EventHandler(this.SplitAdd_Click);
            // 
            // toolstripEdit
            // 
            this.toolstripEdit.Name = "toolstripEdit";
            this.toolstripEdit.Size = new System.Drawing.Size(23, 23);
            this.toolstripEdit.Text = "Edit";
            // 
            // toolstripClose
            // 
            this.toolstripClose.Name = "toolstripClose";
            this.toolstripClose.Size = new System.Drawing.Size(23, 23);
            this.toolstripClose.Text = "Close";
            // 
            // grBack
            // 
            this.grBack.Controls.Add(this.ChckSelectAll);
            this.grBack.Controls.Add(this.ChckActive);
            this.grBack.Controls.Add(this.SpitButtonSave);
            this.grBack.Controls.Add(this.label3);
            this.grBack.Controls.Add(this.label2);
            this.grBack.Controls.Add(this.txtShortName);
            this.grBack.Controls.Add(this.txtName);
            this.grBack.Controls.Add(this.label1);
            this.grBack.Controls.Add(this.DataGridItemCategory);
            this.grBack.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grBack.Location = new System.Drawing.Point(12, 5);
            this.grBack.Name = "grBack";
            this.grBack.Size = new System.Drawing.Size(656, 453);
            this.grBack.TabIndex = 0;
            this.grBack.TabStop = false;
            // 
            // ChckActive
            // 
            this.ChckActive.AutoSize = true;
            this.ChckActive.Location = new System.Drawing.Point(77, 193);
            this.ChckActive.Name = "ChckActive";
            this.ChckActive.Size = new System.Drawing.Size(65, 22);
            this.ChckActive.TabIndex = 7;
            this.ChckActive.Text = "Active";
            this.ChckActive.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(77, 123);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(81, 18);
            this.label3.TabIndex = 5;
            this.label3.Text = "Short Name";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(77, 56);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(45, 18);
            this.label2.TabIndex = 4;
            this.label2.Text = "Name";
            // 
            // txtShortName
            // 
            this.txtShortName.Location = new System.Drawing.Point(77, 144);
            this.txtShortName.Name = "txtShortName";
            this.txtShortName.Size = new System.Drawing.Size(266, 26);
            this.txtShortName.TabIndex = 3;
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(77, 77);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(266, 26);
            this.txtName.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(369, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(95, 18);
            this.label1.TabIndex = 1;
            this.label1.Text = "Item Category";
            // 
            // DataGridItemCategory
            // 
            this.DataGridItemCategory.AllowUserToAddRows = false;
            this.DataGridItemCategory.BackgroundColor = System.Drawing.Color.White;
            this.DataGridItemCategory.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridItemCategory.Location = new System.Drawing.Point(365, 46);
            this.DataGridItemCategory.Name = "DataGridItemCategory";
            this.DataGridItemCategory.RowHeadersVisible = false;
            this.DataGridItemCategory.Size = new System.Drawing.Size(285, 338);
            this.DataGridItemCategory.TabIndex = 0;
            // 
            // grFront
            // 
            this.grFront.Controls.Add(this.label4);
            this.grFront.Controls.Add(this.Cmbcategory);
            this.grFront.Controls.Add(this.SplitAdd);
            this.grFront.Controls.Add(this.sfDataGrid1);
            this.grFront.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grFront.Location = new System.Drawing.Point(12, 5);
            this.grFront.Name = "grFront";
            this.grFront.Size = new System.Drawing.Size(656, 453);
            this.grFront.TabIndex = 7;
            this.grFront.TabStop = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(337, 18);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(95, 18);
            this.label4.TabIndex = 3;
            this.label4.Text = "Item Category";
            // 
            // Cmbcategory
            // 
            this.Cmbcategory.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cmbcategory.FormattingEnabled = true;
            this.Cmbcategory.Location = new System.Drawing.Point(438, 14);
            this.Cmbcategory.Name = "Cmbcategory";
            this.Cmbcategory.Size = new System.Drawing.Size(210, 26);
            this.Cmbcategory.TabIndex = 2;
            this.Cmbcategory.SelectedIndexChanged += new System.EventHandler(this.Cmbcategory_SelectedIndexChanged);
            // 
            // sfDataGrid1
            // 
            this.sfDataGrid1.AccessibleName = "Table";
            this.sfDataGrid1.AllowFiltering = true;
            this.sfDataGrid1.AllowGrouping = false;
            this.sfDataGrid1.Location = new System.Drawing.Point(6, 43);
            this.sfDataGrid1.Name = "sfDataGrid1";
            this.sfDataGrid1.Size = new System.Drawing.Size(642, 368);
            this.sfDataGrid1.TabIndex = 0;
            this.sfDataGrid1.Text = "sfDataGrid1";
            // 
            // ChckSelectAll
            // 
            this.ChckSelectAll.AutoSize = true;
            this.ChckSelectAll.Location = new System.Drawing.Point(554, 22);
            this.ChckSelectAll.Name = "ChckSelectAll";
            this.ChckSelectAll.Size = new System.Drawing.Size(85, 22);
            this.ChckSelectAll.TabIndex = 8;
            this.ChckSelectAll.Text = "Select All";
            this.ChckSelectAll.UseVisualStyleBackColor = true;
            this.ChckSelectAll.CheckedChanged += new System.EventHandler(this.ChckSelectAll_CheckedChanged);
            // 
            // FrmItemAttribute
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(680, 473);
            this.Controls.Add(this.grFront);
            this.Controls.Add(this.grBack);
            this.Name = "FrmItemAttribute";
            this.Text = "FrmItemAttribute";
            this.Load += new System.EventHandler(this.FrmItemAttribute_Load);
            this.grBack.ResumeLayout(false);
            this.grBack.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridItemCategory)).EndInit();
            this.grFront.ResumeLayout(false);
            this.grFront.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sfDataGrid1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grBack;
        private System.Windows.Forms.DataGridView DataGridItemCategory;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtShortName;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private Syncfusion.Windows.Forms.Tools.SplitButton SpitButtonSave;
        private Syncfusion.Windows.Forms.Tools.toolstripitem toolstripBack;
        private System.Windows.Forms.GroupBox grFront;
        private Syncfusion.Windows.Forms.Tools.SplitButton SplitAdd;
        private Syncfusion.Windows.Forms.Tools.toolstripitem toolstripEdit;
        private Syncfusion.Windows.Forms.Tools.toolstripitem toolstripClose;
        private Syncfusion.WinForms.DataGrid.SfDataGrid sfDataGrid1;
        private System.Windows.Forms.ComboBox Cmbcategory;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox ChckActive;
        private System.Windows.Forms.CheckBox ChckSelectAll;
    }
}