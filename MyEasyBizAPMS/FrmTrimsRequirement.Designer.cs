﻿namespace MyEasyBizAPMS
{
    partial class FrmTrimsRequirement
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Syncfusion.Windows.Forms.Tools.SplitButtonRenderer splitButtonRenderer1 = new Syncfusion.Windows.Forms.Tools.SplitButtonRenderer();
            Syncfusion.Windows.Forms.Tools.SplitButtonRenderer splitButtonRenderer2 = new Syncfusion.Windows.Forms.Tools.SplitButtonRenderer();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.splitButton1 = new Syncfusion.Windows.Forms.Tools.SplitButton();
            this.toolstripEdit = new Syncfusion.Windows.Forms.Tools.toolstripitem();
            this.toolstripClose = new Syncfusion.Windows.Forms.Tools.toolstripitem();
            this.SplitSave = new Syncfusion.Windows.Forms.Tools.SplitButton();
            this.toolstripBack = new Syncfusion.Windows.Forms.Tools.toolstripitem();
            this.grFront = new System.Windows.Forms.GroupBox();
            this.SfdDataGridTrims = new Syncfusion.WinForms.DataGrid.SfDataGrid();
            this.label1 = new System.Windows.Forms.Label();
            this.txtCategory = new System.Windows.Forms.TextBox();
            this.DataGridAttribute = new System.Windows.Forms.DataGridView();
            this.grSearch = new System.Windows.Forms.GroupBox();
            this.btnSelect = new System.Windows.Forms.Button();
            this.DataGridCommon = new System.Windows.Forms.DataGridView();
            this.btnHide = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.CmbClassification = new System.Windows.Forms.ComboBox();
            this.grBack = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.BtnOk = new System.Windows.Forms.Button();
            this.CmbValues = new System.Windows.Forms.ComboBox();
            this.CmbAttribute = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtItemName = new System.Windows.Forms.RichTextBox();
            this.txtShortName = new System.Windows.Forms.TextBox();
            this.grFront.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SfdDataGridTrims)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridAttribute)).BeginInit();
            this.grSearch.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridCommon)).BeginInit();
            this.grBack.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitButton1
            // 
            this.splitButton1.BackColor = System.Drawing.SystemColors.Control;
            this.splitButton1.BeforeTouchSize = new System.Drawing.Size(75, 29);
            this.splitButton1.DropDownItems.Add(this.toolstripEdit);
            this.splitButton1.DropDownItems.Add(this.toolstripClose);
            this.splitButton1.DropDownPosition = Syncfusion.Windows.Forms.Tools.Position.Bottom;
            this.splitButton1.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.splitButton1.ForeColor = System.Drawing.Color.Black;
            this.splitButton1.Location = new System.Drawing.Point(667, 411);
            this.splitButton1.MinimumSize = new System.Drawing.Size(75, 23);
            this.splitButton1.Name = "splitButton1";
            splitButtonRenderer1.SplitButton = this.splitButton1;
            this.splitButton1.Renderer = splitButtonRenderer1;
            this.splitButton1.ShowDropDownOnButtonClick = false;
            this.splitButton1.Size = new System.Drawing.Size(75, 29);
            this.splitButton1.TabIndex = 1;
            this.splitButton1.Text = "Add";
            this.splitButton1.DropDowItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.SplitButton1_DropDowItemClicked);
            this.splitButton1.Click += new System.EventHandler(this.SplitButton1_Click);
            // 
            // toolstripEdit
            // 
            this.toolstripEdit.Name = "toolstripEdit";
            this.toolstripEdit.Size = new System.Drawing.Size(23, 23);
            this.toolstripEdit.Text = "Edit";
            // 
            // toolstripClose
            // 
            this.toolstripClose.Name = "toolstripClose";
            this.toolstripClose.Size = new System.Drawing.Size(23, 23);
            this.toolstripClose.Text = "Close";
            // 
            // SplitSave
            // 
            this.SplitSave.BackColor = System.Drawing.SystemColors.Control;
            this.SplitSave.BeforeTouchSize = new System.Drawing.Size(92, 38);
            this.SplitSave.DropDownItems.Add(this.toolstripBack);
            this.SplitSave.DropDownPosition = Syncfusion.Windows.Forms.Tools.Position.Bottom;
            this.SplitSave.Font = new System.Drawing.Font("Segoe UI Emoji", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SplitSave.ForeColor = System.Drawing.Color.Black;
            this.SplitSave.Location = new System.Drawing.Point(650, 399);
            this.SplitSave.MinimumSize = new System.Drawing.Size(75, 23);
            this.SplitSave.Name = "SplitSave";
            splitButtonRenderer2.SplitButton = this.SplitSave;
            this.SplitSave.Renderer = splitButtonRenderer2;
            this.SplitSave.ShowDropDownOnButtonClick = false;
            this.SplitSave.Size = new System.Drawing.Size(92, 38);
            this.SplitSave.TabIndex = 0;
            this.SplitSave.Text = "Save";
            this.SplitSave.DropDowItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.SplitSave_DropDowItemClicked);
            this.SplitSave.Click += new System.EventHandler(this.SplitSave_Click);
            // 
            // toolstripBack
            // 
            this.toolstripBack.Name = "toolstripBack";
            this.toolstripBack.Size = new System.Drawing.Size(23, 23);
            this.toolstripBack.Text = "Back";
            // 
            // grFront
            // 
            this.grFront.Controls.Add(this.splitButton1);
            this.grFront.Controls.Add(this.SfdDataGridTrims);
            this.grFront.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grFront.Location = new System.Drawing.Point(6, -2);
            this.grFront.Name = "grFront";
            this.grFront.Size = new System.Drawing.Size(748, 445);
            this.grFront.TabIndex = 1;
            this.grFront.TabStop = false;
            // 
            // SfdDataGridTrims
            // 
            this.SfdDataGridTrims.AccessibleName = "Table";
            this.SfdDataGridTrims.AllowFiltering = true;
            this.SfdDataGridTrims.AllowSorting = false;
            this.SfdDataGridTrims.Location = new System.Drawing.Point(6, 14);
            this.SfdDataGridTrims.Name = "SfdDataGridTrims";
            this.SfdDataGridTrims.Size = new System.Drawing.Size(736, 393);
            this.SfdDataGridTrims.TabIndex = 0;
            this.SfdDataGridTrims.Text = "sfDataGrid1";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(86, 65);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(55, 15);
            this.label1.TabIndex = 1;
            this.label1.Text = "Category";
            // 
            // txtCategory
            // 
            this.txtCategory.Location = new System.Drawing.Point(146, 61);
            this.txtCategory.Name = "txtCategory";
            this.txtCategory.Size = new System.Drawing.Size(346, 23);
            this.txtCategory.TabIndex = 2;
            this.txtCategory.Click += new System.EventHandler(this.TxtCategory_Click);
            this.txtCategory.TextChanged += new System.EventHandler(this.TxtCategory_TextChanged);
            // 
            // DataGridAttribute
            // 
            this.DataGridAttribute.AllowUserToAddRows = false;
            this.DataGridAttribute.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.DeepSkyBlue;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DataGridAttribute.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.DataGridAttribute.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DataGridAttribute.DefaultCellStyle = dataGridViewCellStyle2;
            this.DataGridAttribute.EnableHeadersVisualStyles = false;
            this.DataGridAttribute.Location = new System.Drawing.Point(146, 134);
            this.DataGridAttribute.Name = "DataGridAttribute";
            this.DataGridAttribute.RowHeadersVisible = false;
            this.DataGridAttribute.Size = new System.Drawing.Size(399, 201);
            this.DataGridAttribute.TabIndex = 17;
            this.DataGridAttribute.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridAttribute_CellClick);
            this.DataGridAttribute.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridAttribute_CellContentClick);
            this.DataGridAttribute.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DataGridAttribute_KeyDown);
            this.DataGridAttribute.MouseLeave += new System.EventHandler(this.DataGridAttribute_MouseLeave);
            // 
            // grSearch
            // 
            this.grSearch.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.grSearch.Controls.Add(this.btnSelect);
            this.grSearch.Controls.Add(this.DataGridCommon);
            this.grSearch.Controls.Add(this.btnHide);
            this.grSearch.Location = new System.Drawing.Point(147, 138);
            this.grSearch.Name = "grSearch";
            this.grSearch.Size = new System.Drawing.Size(346, 229);
            this.grSearch.TabIndex = 34;
            this.grSearch.TabStop = false;
            this.grSearch.Visible = false;
            this.grSearch.Enter += new System.EventHandler(this.GrSearch_Enter);
            // 
            // btnSelect
            // 
            this.btnSelect.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnSelect.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSelect.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnSelect.Location = new System.Drawing.Point(281, 197);
            this.btnSelect.Name = "btnSelect";
            this.btnSelect.Size = new System.Drawing.Size(61, 28);
            this.btnSelect.TabIndex = 396;
            this.btnSelect.Text = "Select";
            this.btnSelect.UseVisualStyleBackColor = false;
            this.btnSelect.Click += new System.EventHandler(this.BtnSelect_Click);
            // 
            // DataGridCommon
            // 
            this.DataGridCommon.AllowUserToAddRows = false;
            this.DataGridCommon.BackgroundColor = System.Drawing.Color.White;
            this.DataGridCommon.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridCommon.Location = new System.Drawing.Point(4, 12);
            this.DataGridCommon.MultiSelect = false;
            this.DataGridCommon.Name = "DataGridCommon";
            this.DataGridCommon.ReadOnly = true;
            this.DataGridCommon.RowHeadersVisible = false;
            this.DataGridCommon.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridCommon.Size = new System.Drawing.Size(336, 183);
            this.DataGridCommon.TabIndex = 0;
            this.DataGridCommon.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.DataGridCommon_CellMouseClick);
            this.DataGridCommon.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.DataGridCommon_CellMouseDoubleClick);
            // 
            // btnHide
            // 
            this.btnHide.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnHide.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHide.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnHide.Location = new System.Drawing.Point(6, 197);
            this.btnHide.Name = "btnHide";
            this.btnHide.Size = new System.Drawing.Size(65, 28);
            this.btnHide.TabIndex = 395;
            this.btnHide.Text = "Close";
            this.btnHide.UseVisualStyleBackColor = false;
            this.btnHide.Click += new System.EventHandler(this.BtnHide_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(71, 417);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(70, 15);
            this.label3.TabIndex = 36;
            this.label3.Text = "Short Name";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(49, 30);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(92, 15);
            this.label4.TabIndex = 38;
            this.label4.Text = "CLASSIFICATION";
            // 
            // CmbClassification
            // 
            this.CmbClassification.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CmbClassification.FormattingEnabled = true;
            this.CmbClassification.Location = new System.Drawing.Point(146, 26);
            this.CmbClassification.Name = "CmbClassification";
            this.CmbClassification.Size = new System.Drawing.Size(279, 23);
            this.CmbClassification.TabIndex = 37;
            this.CmbClassification.SelectedIndexChanged += new System.EventHandler(this.CmbClassification_SelectedIndexChanged);
            // 
            // grBack
            // 
            this.grBack.Controls.Add(this.grSearch);
            this.grBack.Controls.Add(this.label6);
            this.grBack.Controls.Add(this.label5);
            this.grBack.Controls.Add(this.BtnOk);
            this.grBack.Controls.Add(this.CmbValues);
            this.grBack.Controls.Add(this.CmbAttribute);
            this.grBack.Controls.Add(this.label2);
            this.grBack.Controls.Add(this.txtItemName);
            this.grBack.Controls.Add(this.CmbClassification);
            this.grBack.Controls.Add(this.label4);
            this.grBack.Controls.Add(this.label3);
            this.grBack.Controls.Add(this.txtShortName);
            this.grBack.Controls.Add(this.DataGridAttribute);
            this.grBack.Controls.Add(this.txtCategory);
            this.grBack.Controls.Add(this.label1);
            this.grBack.Controls.Add(this.SplitSave);
            this.grBack.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grBack.Location = new System.Drawing.Point(6, -2);
            this.grBack.Name = "grBack";
            this.grBack.Size = new System.Drawing.Size(748, 445);
            this.grBack.TabIndex = 0;
            this.grBack.TabStop = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(333, 91);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(43, 15);
            this.label6.TabIndex = 45;
            this.label6.Text = "Values";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(148, 91);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(55, 15);
            this.label5.TabIndex = 44;
            this.label5.Text = "Attribute";
            // 
            // BtnOk
            // 
            this.BtnOk.Location = new System.Drawing.Point(504, 108);
            this.BtnOk.Name = "BtnOk";
            this.BtnOk.Size = new System.Drawing.Size(45, 25);
            this.BtnOk.TabIndex = 43;
            this.BtnOk.Text = "Ok";
            this.BtnOk.UseVisualStyleBackColor = true;
            this.BtnOk.Click += new System.EventHandler(this.BtnOk_Click);
            // 
            // CmbValues
            // 
            this.CmbValues.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CmbValues.FormattingEnabled = true;
            this.CmbValues.Location = new System.Drawing.Point(336, 109);
            this.CmbValues.Name = "CmbValues";
            this.CmbValues.Size = new System.Drawing.Size(168, 23);
            this.CmbValues.TabIndex = 42;
            this.CmbValues.SelectedIndexChanged += new System.EventHandler(this.CmbValues_SelectedIndexChanged);
            // 
            // CmbAttribute
            // 
            this.CmbAttribute.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CmbAttribute.FormattingEnabled = true;
            this.CmbAttribute.Location = new System.Drawing.Point(146, 109);
            this.CmbAttribute.Name = "CmbAttribute";
            this.CmbAttribute.Size = new System.Drawing.Size(189, 23);
            this.CmbAttribute.TabIndex = 41;
            this.CmbAttribute.SelectedIndexChanged += new System.EventHandler(this.CmbAttribute_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(76, 352);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 15);
            this.label2.TabIndex = 40;
            this.label2.Text = "Item Name";
            // 
            // txtItemName
            // 
            this.txtItemName.Enabled = false;
            this.txtItemName.Location = new System.Drawing.Point(146, 346);
            this.txtItemName.Name = "txtItemName";
            this.txtItemName.ReadOnly = true;
            this.txtItemName.Size = new System.Drawing.Size(399, 58);
            this.txtItemName.TabIndex = 39;
            this.txtItemName.Text = "";
            // 
            // txtShortName
            // 
            this.txtShortName.Location = new System.Drawing.Point(146, 413);
            this.txtShortName.Name = "txtShortName";
            this.txtShortName.Size = new System.Drawing.Size(399, 23);
            this.txtShortName.TabIndex = 35;
            // 
            // FrmTrimsRequirement
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(760, 450);
            this.Controls.Add(this.grBack);
            this.Controls.Add(this.grFront);
            this.Name = "FrmTrimsRequirement";
            this.Text = "Trims Requirement";
            this.Load += new System.EventHandler(this.FrmTrimsRequirement_Load);
            this.grFront.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.SfdDataGridTrims)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridAttribute)).EndInit();
            this.grSearch.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DataGridCommon)).EndInit();
            this.grBack.ResumeLayout(false);
            this.grBack.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private Syncfusion.Windows.Forms.Tools.toolstripitem toolstripBack;
        private System.Windows.Forms.GroupBox grFront;
        private Syncfusion.Windows.Forms.Tools.SplitButton splitButton1;
        private Syncfusion.Windows.Forms.Tools.toolstripitem toolstripEdit;
        private Syncfusion.Windows.Forms.Tools.toolstripitem toolstripClose;
        private Syncfusion.WinForms.DataGrid.SfDataGrid SfdDataGridTrims;
        private Syncfusion.Windows.Forms.Tools.SplitButton SplitSave;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtCategory;
        private System.Windows.Forms.DataGridView DataGridAttribute;
        private System.Windows.Forms.GroupBox grSearch;
        private System.Windows.Forms.Button btnSelect;
        private System.Windows.Forms.DataGridView DataGridCommon;
        private System.Windows.Forms.Button btnHide;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox CmbClassification;
        private System.Windows.Forms.GroupBox grBack;
        private System.Windows.Forms.TextBox txtShortName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RichTextBox txtItemName;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button BtnOk;
        private System.Windows.Forms.ComboBox CmbValues;
        private System.Windows.Forms.ComboBox CmbAttribute;
    }
}