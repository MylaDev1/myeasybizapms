﻿using Syncfusion.WinForms.DataGrid;
using Syncfusion.WinForms.DataGrid.Events;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Windows.Forms;

namespace MyEasyBizAPMS
{
    public partial class FrmTrims : Form
    {
        public FrmTrims()
        {
            InitializeComponent();
            this.tabControlAdv1.TabStyle = typeof(Syncfusion.Windows.Forms.Tools.OneNoteStyleRenderer);
            this.SfDataGridTrims.Style.HeaderStyle.Font.FontStyle = FontStyle.Bold;
            this.SfDataGridTrims.Style.HeaderStyle.BackColor = Color.DeepSkyBlue;
        }
        int Fillid = 0; int SelectId = 0;
        SQLDBHelper db = new SQLDBHelper();
        SqlConnection connection = new SqlConnection(GeneralParameters.ConnectionString);
        BindingSource bsAttribute = new BindingSource();
        BindingSource bsTax = new BindingSource();
        RowAutoFitOptions autoFitOptions = new RowAutoFitOptions();
        List<string> excludeColumns = new List<string>();
        DataTable dtCate = new DataTable();
        int Loadid = 0;
        DataTable dtAttValue = new DataTable();
        DataTable dtAttValue1 = new DataTable();
        int Mode = 0;

        private void FrmTrims_Load(object sender, EventArgs e)
        {
            Loadid = 1;
            LoadItemM();
            LoadAttribute();
            LoadAtributeValue();
            LoadAlternateUom();
            grFront.Visible = true;
            grBack.Visible = false;
            DataTable dt = LoadGeneralM();
            DataTable dtclass = dt.Select("TypeMUid = 9").CopyToDataTable();
            CmbClassification.DisplayMember = "GeneralName";
            CmbClassification.ValueMember = "Guid";
            CmbClassification.DataSource = dtclass;

            dtCate = dt.Select("TypeMUid = 13").CopyToDataTable();

            DataTable dtTax = dt.Select("TypeMUid = 8").CopyToDataTable();
            CmbTax.DisplayMember = "GeneralName";
            CmbTax.ValueMember = "Guid";
            CmbTax.DataSource = dtTax;
            DataTable dtUom = dt.Select("TypeMUid = 7").CopyToDataTable();
            CmbUom.DisplayMember = "GeneralName";
            CmbUom.ValueMember = "Guid";
            CmbUom.DataSource = dtUom;

            dtAttValue.Columns.Add("SlNo", typeof(int));
            dtAttValue.Columns.Add("Values", typeof(string));
            dtAttValue.Columns.Add("AttributeId", typeof(decimal));
            dtAttValue.Columns.Add("Uid", typeof(decimal));
            Loadid = 0;
        }

        public DataTable LoadGeneralM()
        {
            DataTable dt = new DataTable();
            try
            {
                dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "SP_GetGeneralsTrims", connection);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            return dt;
        }

        private void SplitAdd_Click(object sender, EventArgs e)
        {
            try
            {
                Loadid = 1;
                Mode = 1;
                txtItemName.Tag = "0";
                grFront.Visible = false;
                grBack.Visible = true;
                ClearControl();
                CmbClassification.Select();
                ChckActive.Checked = true;
                DataGridAttributeValue.Rows.Clear();
                DataGridAttribute.Rows.Clear();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected void LoadItemM()
        {
            try
            {
                DataTable dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "SP_GetItemsM", connection);
                SfDataGridTrims.DataSource = null;
                SfDataGridTrims.DataSource = dt;
                SfDataGridTrims.Columns[5].Width = 180;
                SfDataGridTrims.Columns[6].Width = 150;
                SfDataGridTrims.Columns[7].Width = 150;
                SfDataGridTrims.Columns[8].Width = 150;
                SfDataGridTrims.Columns[9].Width = 75;
                SfDataGridTrims.Columns[10].Width = 75;
                SfDataGridTrims.Columns[0].Visible = false;
                SfDataGridTrims.Columns[1].Visible = false;
                SfDataGridTrims.Columns[2].Visible = false;
                SfDataGridTrims.Columns[3].Visible = false;
                SfDataGridTrims.Columns[4].Visible = false;
                foreach (var column in this.SfDataGridTrims.Columns)
                {
                    if (!column.MappingName.Equals("Uid") && !column.MappingName.Equals("TaxUid"))
                        excludeColumns.Add(column.MappingName);
                }

                (SfDataGridTrims.Columns["ItemName"] as GridTextColumn).AllowTextWrapping = true;
                SfDataGridTrims.QueryRowHeight += SfDataGrid_QueryRowHeight;
                gridRowResizingOptions.ExcludeColumns = excludeColumns;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        int height;
        RowAutoFitOptions gridRowResizingOptions = new RowAutoFitOptions();
        void SfDataGrid_QueryRowHeight(object sender, QueryRowHeightEventArgs e)
        {
            if (this.SfDataGridTrims.TableControl.IsTableSummaryIndex(e.RowIndex))
            {
                e.Height = 40;
                e.Handled = true;
            }
            else if (this.SfDataGridTrims.AutoSizeController.GetAutoRowHeight(e.RowIndex, gridRowResizingOptions, out height))
            {
                if (height > this.SfDataGridTrims.RowHeight)
                {
                    e.Height = height;
                    e.Handled = true;
                }
            }
        }

        private void BtnSave_DropDowItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            try
            {
                if (e.ClickedItem.Text == "Back")
                {
                    grFront.Visible = true;
                    grBack.Visible = false;
                    LoadItemM();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected void LoadAttribute()
        {
            try
            {

                DataGridAttribute.ColumnCount = 5;
                DataGridAttribute.Columns[0].Name = "SlNo";
                DataGridAttribute.Columns[0].HeaderText = "SlNo";
                DataGridAttribute.Columns[0].Width = 50;

                DataGridAttribute.Columns[1].Name = "Attribute";
                DataGridAttribute.Columns[1].HeaderText = "Attribute";
                DataGridAttribute.Columns[1].Width = 270;

                DataGridAttribute.Columns[2].Name = "AttributeID";
                DataGridAttribute.Columns[2].HeaderText = "AttributeID";
                DataGridAttribute.Columns[2].Visible = false;

                DataGridAttribute.Columns[3].Name = "AttributeUid";
                DataGridAttribute.Columns[3].HeaderText = "AttributeUid";
                DataGridAttribute.Columns[3].Visible = false;

                DataGridViewCheckBoxColumn dgvCB1 = new DataGridViewCheckBoxColumn
                {
                    Name = "USERDEFINED",
                    //dgvCB.DataSource = dt;
                    HeaderText = "User Defined",
                    Width = 120
                };
                DataGridAttribute.Columns.Insert(4, dgvCB1);

                DataGridAttribute.Columns[5].Name = "SORT ORDER";
                DataGridAttribute.Columns[5].HeaderText = "SORT ORDER";
                DataGridAttribute.Columns[5].Width = 125;
                DataGridAttribute.Columns[4].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected void LoadItemName()
        {
            try
            {
                txtItemName.Text = CmbClassification.Text + " " + CmbCategory.Text;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void CmbClassification_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (Loadid == 0)
                {
                    DataTable newTable = new DataTable();
                    CmbCategory.DataSource = null;
                    System.Data.DataRow[] tblROWS = dtCate.Select("F2 =" + CmbClassification.SelectedValue + "");

                    if (tblROWS.Length > 0)
                    {
                        newTable = dtCate.Select("F2 =" + CmbClassification.SelectedValue + "").CopyToDataTable();
                        CmbCategory.DisplayMember = "GeneralName";
                        CmbCategory.ValueMember = "Guid";
                        CmbCategory.DataSource = newTable;
                    }
                }
                LoadItemName();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void CmbCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    LoadItemName();
                    if (CmbCategory.SelectedIndex != -1)
                    {
                        DataTable dt = LoadGeneralM();
                        DataTable Attribue = dt.Select("TypeMUid = 28").CopyToDataTable();
                        DataTable newTable = new DataTable();
                        System.Data.DataRow[] tblROWS = Attribue.Select("F3 =" + CmbCategory.SelectedValue + "");
                        if (tblROWS.Length > 0)
                        {
                            newTable = Attribue.Select("F3 =" + CmbCategory.SelectedValue + "").CopyToDataTable();
                            bsAttribute.DataSource = newTable;
                            CmbAttributeAdd.DataSource = null;
                            CmbAttributeAdd.DisplayMember = "GeneralName";
                            CmbAttributeAdd.ValueMember = "Guid";
                            CmbAttributeAdd.DataSource = newTable;
                            //FillGrid(Attribue, 1);
                        }
                        //grSearch.Visible = true;
                        //grSearch.BringToFront();
                    }
                    else
                    {
                        MessageBox.Show("Select Category", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        CmbCategory.Focus();
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void CmbTax_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    LoadItemName();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void SplitAdd_DropDowItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            try
            {
                if (e.ClickedItem.Text == "Close")
                {
                    this.Close();
                }
                else if (e.ClickedItem.Text == "Edit")
                {
                    Mode = 2;
                    ClearControl();
                    if (SfDataGridTrims.SelectedIndex != -1)
                    {
                        var selectedItem = SfDataGridTrims.SelectedItems[0];
                        var dataRow = (selectedItem as DataRowView).Row;
                        decimal CUid = Convert.ToDecimal(dataRow["UID"].ToString());
                        SqlParameter[] parameters = {
                            new SqlParameter("@Uid",CUid)
                        };
                        DataSet ds = db.GetDataWithParamMultiple(CommandType.StoredProcedure, "SP_GetItemsMForEdit", parameters, connection);
                        FillDetails(ds);
                        CmbAttribute_SelectedIndexChanged(sender, e);
                        //CmbUom_SelectedIndexChanged(sender, e);
                        LoadItemName();
                    }
                    else
                    {
                        MessageBox.Show("Select the Row to edit", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void FillDetails(DataSet dataSet)
        {
            try
            {
                SelectId = 1;

                DataTable dtItem = dataSet.Tables[0];
                DataTable dtAttribute = dataSet.Tables[1];
                DataTable dtAttributeValues = dataSet.Tables[2];
                DataTable dtAlternateUom = dataSet.Tables[3];
                if (dtItem.Rows.Count > 0)
                {
                    txtItemName.Text = dtItem.Rows[0]["ItemName"].ToString();
                    txtShortName.Text = dtItem.Rows[0]["ItemSName"].ToString();
                    txtItemName.Tag = dtItem.Rows[0]["Uid"].ToString();
                    CmbClassification.SelectedValue = dtItem.Rows[0]["ItemTypeUID"].ToString();
                    CmbCategory.SelectedValue = dtItem.Rows[0]["ItemCategoryUID"].ToString();
                    CmbTax.SelectedValue = dtItem.Rows[0]["TaxUID"].ToString();
                    CmbUom.SelectedValue = dtItem.Rows[0]["BaseUomUID"].ToString();
                    bool active = Convert.ToBoolean(dtItem.Rows[0]["Active"].ToString());
                    if (active == false)
                    {
                        ChckActive.Checked = false;
                    }
                    else
                    {
                        ChckActive.Checked = true;
                    }
                    grFront.Visible = false;
                    grBack.Visible = true;
                }

                if (dtAttribute.Rows.Count > 0)
                {
                    for (int i = 0; i < dtAttribute.Rows.Count; i++)
                    {
                        DataGridViewRow row = (DataGridViewRow)DataGridAttribute.Rows[0].Clone();
                        DataGridViewCheckBoxCell chk = (DataGridViewCheckBoxCell)row.Cells[4];
                        row.Cells[0].Value = i + 1;
                        row.Cells[1].Value = dtAttribute.Rows[i]["Attribute"].ToString();
                        row.Cells[2].Value = dtAttribute.Rows[i]["AttributeUid"].ToString();
                        row.Cells[3].Value = dtAttribute.Rows[i]["UID"].ToString();
                        bool val = Convert.ToBoolean(dtAttribute.Rows[i]["UserDefined"].ToString());
                        if (val == true)
                        {
                            chk.Value = true;
                        }
                        else
                        {
                            chk.Value = false;
                        }
                        row.Cells[5].Value = dtAttribute.Rows[i]["SeqNo"].ToString();
                        DataGridAttribute.Rows.Add(row);
                    }
                }
                if (Mode == 2)
                {
                    dtAttValue.Columns.Clear();
                    dtAttValue.Columns.Add("SlNo", typeof(int));
                    dtAttValue.Columns.Add("Values", typeof(string));
                    dtAttValue.Columns.Add("AttributeId", typeof(decimal));
                    dtAttValue.Columns.Add("Uid", typeof(decimal));
                    for (int i = 0; i < dtAttributeValues.Rows.Count; i++)
                    {
                        System.Data.DataRow dataRow = dtAttValue.NewRow();
                        dataRow[0] = i + 1;
                        dataRow[1] = dtAttributeValues.Rows[i]["AttributeValue"].ToString();
                        dataRow[2] = dtAttributeValues.Rows[i]["AttributeUid"].ToString();
                        dataRow[3] = dtAttributeValues.Rows[i]["Uid"].ToString();
                        dtAttValue.Rows.Add(dataRow);
                    }
                }
                else
                {

                }
                if (dtAttributeValues.Rows.Count > 0)
                {

                    for (int i = 0; i < dtAttributeValues.Rows.Count; i++)
                    {
                        DataGridViewRow row = (DataGridViewRow)DataGridAttributeValue.Rows[0].Clone();
                        row.Cells[0].Value = i + 1;
                        row.Cells[1].Value = dtAttributeValues.Rows[i]["AttributeValue"].ToString();
                        row.Cells[2].Value = dtAttributeValues.Rows[i]["AttributeUid"].ToString();
                        DataGridAttributeValue.Rows.Add(row);
                    }
                    DataTable dt = new DataTable();
                    dt.Columns.Add("Attribute", typeof(string));
                    dt.Columns.Add("AttributeId", typeof(decimal));
                    foreach (DataGridViewRow row in DataGridAttribute.Rows)
                    {
                        DataGridViewCheckBoxCell chk = (DataGridViewCheckBoxCell)row.Cells[4];
                        if (chk.Value != null)
                        {
                            string value = chk.Value.ToString();
                            if (value != "True")
                            {
                                System.Data.DataRow row1 = dt.NewRow();
                                row1["Attribute"] = row.Cells[1].Value.ToString();
                                row1["AttributeId"] = row.Cells[2].Value.ToString();
                                dt.Rows.Add(row1);
                            }
                        }

                    }
                    CmbAttribute.DataSource = null;
                    CmbAttribute.DisplayMember = "Attribute";
                    CmbAttribute.ValueMember = "AttributeId";
                    CmbAttribute.DataSource = dt;
                }

                if (dtAlternateUom.Rows.Count > 0)
                {
                    for (int i = 0; i < dtAlternateUom.Rows.Count; i++)
                    {
                        bool entryfound = GridCheckValue(dtAlternateUom.Rows[i]["Uom"].ToString());
                        if (entryfound == false)
                        {
                            DataGridViewRow row = (DataGridViewRow)DataGridAlternateUom.Rows[0].Clone();
                            row.Cells[0].Value = i + 1;
                            row.Cells[1].Value = dtAlternateUom.Rows[i]["Uom"].ToString();
                            row.Cells[2].Value = Convert.ToDecimal(dtAlternateUom.Rows[i]["AlternateQty"].ToString()).ToString("0.00");
                            row.Cells[3].Value = Convert.ToDecimal(dtAlternateUom.Rows[i]["BaseUomQty"].ToString()).ToString("0.00");
                            row.Cells[4].Value = dtAlternateUom.Rows[i]["AlternateUomUID"].ToString();
                            DataGridAlternateUom.Rows.Add(row);
                            CheckUom(dtAlternateUom.Rows[i]["Uom"].ToString(), Convert.ToDecimal(dtAlternateUom.Rows[i]["AlternateUomUID"].ToString()));
                        }
                    }
                }
                CmbStockUom.SelectedValue = dtItem.Rows[0]["StockUom"].ToString();
                CmbSalesUom.SelectedValue = dtItem.Rows[0]["SalesUom"].ToString();
                CmbPalnningUom.SelectedValue = dtItem.Rows[0]["PlanningUom"].ToString();
                CmbPurchaseUom.SelectedValue = dtItem.Rows[0]["PurchaseUom"].ToString();

                SelectId = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void DataGridAttribute_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (DataGridAttribute.CurrentCell.ColumnIndex == 1)
                    {
                        DataTable dt = LoadGeneralM();
                        DataTable Attribue = dt.Select("TypeMUid = 28").CopyToDataTable();
                        bsAttribute.DataSource = Attribue;
                        FillGrid(Attribue, 1);
                        grSearch.Visible = true;
                        grSearch.BringToFront();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void FillGrid(DataTable dt, int FillId)
        {
            try
            {
                DataGridCommon.DataSource = null;
                DataGridCommon.AutoGenerateColumns = false;
                if (FillId == 1)
                {
                    Fillid = 1;
                    DataGridCommon.ColumnCount = 2;
                    DataGridCommon.Columns[0].Name = "CUid";
                    DataGridCommon.Columns[0].HeaderText = "CUid";
                    DataGridCommon.Columns[0].DataPropertyName = "GUid";
                    DataGridCommon.Columns[0].Visible = false;
                    DataGridCommon.Columns[1].Name = "Name";
                    DataGridCommon.Columns[1].HeaderText = "Name";
                    DataGridCommon.Columns[1].DataPropertyName = "GeneralName";
                    DataGridCommon.Columns[1].Width = 220;
                    DataGridCommon.DataSource = bsAttribute;
                }
                else if (FillId == 2)
                {
                    DataGridSearchUom.DataSource = null;
                    DataGridSearchUom.AutoGenerateColumns = false;
                    Fillid = 2;
                    DataGridSearchUom.ColumnCount = 2;
                    DataGridSearchUom.Columns[0].Name = "CUid";
                    DataGridSearchUom.Columns[0].HeaderText = "CUid";
                    DataGridSearchUom.Columns[0].DataPropertyName = "GUid";
                    DataGridSearchUom.Columns[0].Visible = false;

                    DataGridSearchUom.Columns[1].Name = "Name";
                    DataGridSearchUom.Columns[1].HeaderText = "Name";
                    DataGridSearchUom.Columns[1].DataPropertyName = "GeneralName";
                    DataGridSearchUom.Columns[1].Width = 220;
                    DataGridSearchUom.DataSource = bsTax;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void BtnSelect_Click(object sender, EventArgs e)
        {
            try
            {
                SelectId = 1;
                int Index = DataGridCommon.SelectedCells[0].RowIndex;
                if (Fillid == 1)
                {
                    txtAttribute.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtAttribute.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                }
                grSearch.Visible = false;
                SelectId = 0;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void BtnHide_Click(object sender, EventArgs e)
        {
            grSearch.Visible = false;
            grSearch.SendToBack();
        }

        private void TxtSearch_TextChanged(object sender, EventArgs e)
        {
            try
            {
                bsAttribute.Filter = string.Format("GeneralName Like '%{0}%'", txtAttribute.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void DataGridAttribute_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void TabControlAdv1_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (tabControlAdv1.SelectedIndex == 1)
                {

                    SelectId = 1;
                    if (Loadid == 1)
                    {
                        DataTable dt = new DataTable();
                        dt.Columns.Add("Attribute", typeof(string));
                        dt.Columns.Add("AttributeId", typeof(decimal));
                        if (tabControlAdv1.SelectedTab.TabIndex == 2)
                        {
                            foreach (DataGridViewRow row in DataGridAttribute.Rows)
                            {
                                DataGridViewCheckBoxCell chk = (DataGridViewCheckBoxCell)row.Cells[4];
                                if (chk.Value != null)
                                {
                                    string value = chk.Value.ToString();
                                    if (value != "True")
                                    {
                                        System.Data.DataRow row1 = dt.NewRow();
                                        row1["Attribute"] = row.Cells[1].Value.ToString();
                                        row1["AttributeId"] = row.Cells[2].Value.ToString();
                                        dt.Rows.Add(row1);
                                    }
                                }

                            }
                            CmbAttribute.DataSource = null;
                            CmbAttribute.DisplayMember = "Attribute";
                            CmbAttribute.ValueMember = "AttributeId";
                            CmbAttribute.DataSource = dt;

                        }
                    }
                    else
                    {
                        DataTable dt = new DataTable();
                        dt.Columns.Add("Attribute", typeof(string));
                        dt.Columns.Add("AttributeId", typeof(decimal));
                        if (tabControlAdv1.SelectedTab.TabIndex == 2)
                        {
                            foreach (DataGridViewRow row in DataGridAttribute.Rows)
                            {
                                DataGridViewCheckBoxCell chk = (DataGridViewCheckBoxCell)row.Cells[4];
                                if (chk.Value != null)
                                {
                                    string value = chk.Value.ToString();
                                    if (value != "True")
                                    {
                                        System.Data.DataRow row1 = dt.NewRow();
                                        row1["Attribute"] = row.Cells[1].Value.ToString();
                                        row1["AttributeId"] = row.Cells[2].Value.ToString();
                                        dt.Rows.Add(row1);
                                    }
                                }

                            }
                            CmbAttribute.DataSource = null;
                            CmbAttribute.DisplayMember = "Attribute";
                            CmbAttribute.ValueMember = "AttributeId";
                            CmbAttribute.DataSource = dt;
                            SelectId = 0;
                        }
                    }
                    SelectId = 0;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void CmbUom_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    DataTable dt = new DataTable();
                    dt.Columns.Add("Uom", typeof(string));
                    dt.Columns.Add("UomId", typeof(decimal));
                    if (CmbUom.SelectedIndex != -1)
                    {
                        System.Data.DataRow row = dt.NewRow();
                        row["Uom"] = CmbUom.Text;
                        row["UomId"] = CmbUom.SelectedValue;
                        dt.Rows.Add(row);
                    }
                    FillAlternateUom(dt);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected void LoadAtributeValue()
        {
            try
            {
                DataGridAttributeValue.DataSource = null;
                DataGridAttributeValue.ColumnCount = 4;
                DataGridAttributeValue.Columns[0].Name = "SlNo";
                DataGridAttributeValue.Columns[0].HeaderText = "SlNo";
                DataGridAttributeValue.Columns[0].Width = 70;

                DataGridAttributeValue.Columns[1].Name = "Attribute Values";
                DataGridAttributeValue.Columns[1].HeaderText = "Attribute Values";
                DataGridAttributeValue.Columns[1].Width = 310;

                DataGridAttributeValue.Columns[2].Name = "Attribute";
                DataGridAttributeValue.Columns[2].HeaderText = "Attribute";
                DataGridAttributeValue.Columns[2].Visible = false;

                DataGridAttributeValue.Columns[3].Name = "Uid";
                DataGridAttributeValue.Columns[3].HeaderText = "Uid";
                DataGridAttributeValue.Columns[3].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected void LoadAlternateUom()
        {
            try
            {
                DataGridAlternateUom.DataSource = null;
                DataGridAlternateUom.ColumnCount = 5;
                DataGridAlternateUom.Columns[0].Name = "SlNo";
                DataGridAlternateUom.Columns[0].HeaderText = "SlNo";
                DataGridAlternateUom.Columns[0].Width = 70;

                DataGridAlternateUom.Columns[1].Name = "Alternate UoM";
                DataGridAlternateUom.Columns[1].HeaderText = "Alternate UoM";
                DataGridAlternateUom.Columns[1].Width = 150;

                DataGridAlternateUom.Columns[2].Name = "Alternate Uom Qty";
                DataGridAlternateUom.Columns[2].HeaderText = "Alternate Uom Qty";

                DataGridAlternateUom.Columns[3].Name = "Base Uom Qty";
                DataGridAlternateUom.Columns[3].HeaderText = "Base Uom Qty";

                DataGridAlternateUom.Columns[4].Name = "AlternateUoMId";
                DataGridAlternateUom.Columns[4].HeaderText = "AlternateUoMId";
                DataGridAlternateUom.Columns[4].Visible = false;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (CmbClassification.SelectedIndex != -1 || CmbCategory.SelectedIndex != -1 || CmbUom.SelectedIndex != -1)
                {
                    if(Mode == 1)
                    {
                        SqlParameter[] sqlParametersD = { new SqlParameter("@ItemName", txtItemName.Text) };
                        DataTable dataTable = db.GetDataWithParam(CommandType.StoredProcedure, "Proc_CheckDuplicateItem", sqlParametersD, connection);
                        if (dataTable.Rows.Count > 0)
                        {
                            MessageBox.Show("Item Name Already Found", "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            return;
                        }
                    }
                    
                    bool Active = false;
                    if (ChckActive.Checked == true)
                    {
                        Active = true;
                    }
                    else
                    {
                        Active = false;
                    }
                    SqlParameter[] sqlParameters = {
                        new SqlParameter("@Uid",txtItemName.Tag),
                        new SqlParameter("@ItemTypeUID",CmbClassification.SelectedValue),
                        new SqlParameter("@ItemCategoryUID",CmbCategory.SelectedValue),
                        new SqlParameter("@TaxUID",CmbTax.SelectedValue),
                        new SqlParameter("@ItemName",txtItemName.Text),
                        new SqlParameter("@ItemSName",txtShortName.Text),
                        new SqlParameter("@BaseUomUID",CmbUom.SelectedValue),
                        new SqlParameter("@Active",Active),
                        new SqlParameter("@CreatedDate",DateTime.Now),
                        new SqlParameter("@CreatedBy",GeneralParameters.UserdId),
                        new SqlParameter("@ReturnId",SqlDbType.Decimal),
                        new SqlParameter("@StockUom",CmbStockUom.SelectedValue),
                        new SqlParameter("@PurchaseUom",CmbPurchaseUom.SelectedValue),
                        new SqlParameter("@SalesUom",CmbSalesUom.SelectedValue),
                        new SqlParameter("@PlanningUom",CmbPalnningUom.SelectedValue)
                    };
                    sqlParameters[10].Direction = ParameterDirection.Output;
                    int Id = db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_ItemsM", sqlParameters, connection, 10);
                    decimal attributeUid = 0;
                    DataTable dtAttribute = new DataTable();
                    DataTable dtAttributeValues = new DataTable();
                    DataTable dtAlternateUom = new DataTable();
                    dtAttribute.Columns.Add("SlNo", typeof(int));
                    dtAttribute.Columns.Add("Attribute", typeof(string));
                    dtAttribute.Columns.Add("AttributeId", typeof(decimal));
                    dtAttribute.Columns.Add("UserDefined", typeof(bool));
                    dtAttribute.Columns.Add("SortOrder", typeof(int));
                    dtAttribute.Columns.Add("Uid", typeof(decimal));

                    dtAttributeValues.Columns.Add("SlNo", typeof(int));
                    dtAttributeValues.Columns.Add("AttributeValues", typeof(string));
                    dtAttributeValues.Columns.Add("AttributeId", typeof(int));
                    dtAttributeValues.Columns.Add("Uid", typeof(decimal));

                    dtAlternateUom.Columns.Add("SlNo", typeof(int));
                    dtAlternateUom.Columns.Add("AlternateUoM", typeof(string));
                    dtAlternateUom.Columns.Add("AlternateUomQty", typeof(decimal));
                    dtAlternateUom.Columns.Add("BaseUomQty", typeof(decimal));
                    dtAlternateUom.Columns.Add("AlternateUoMId", typeof(decimal));

                    for (int i = 0; i < DataGridAttribute.Rows.Count - 1; i++)
                    {
                        bool UserDefined = false;
                        string value = DataGridAttribute.Rows[i].Cells[4].Value.ToString();
                        if (value == "True")
                        {
                            UserDefined = true;
                        }
                        else
                        {
                            UserDefined = false;
                        }
                        System.Data.DataRow dataRow = dtAttribute.NewRow();
                        dataRow["SlNo"] = DataGridAttribute.Rows[i].Cells[0].Value.ToString();
                        dataRow["Attribute"] = DataGridAttribute.Rows[i].Cells[1].Value.ToString();
                        dataRow["AttributeId"] = DataGridAttribute.Rows[i].Cells[2].Value.ToString();
                        dataRow["UserDefined"] = UserDefined;
                        dataRow["SortOrder"] = DataGridAttribute.Rows[i].Cells[5].Value.ToString();
                        dataRow["Uid"] = DataGridAttribute.Rows[i].Cells[3].Value.ToString();
                        dtAttribute.Rows.Add(dataRow);
                    }
                    if (Mode == 1)
                    {
                        for (int i = 0; i < dtAttValue.Rows.Count; i++)
                        {
                            System.Data.DataRow dataRow = dtAttributeValues.NewRow();
                            dataRow["SlNo"] = dtAttValue.Rows[i]["SlNo"].ToString();
                            dataRow["AttributeValues"] = dtAttValue.Rows[i]["Values"].ToString(); ;
                            dataRow["AttributeId"] = dtAttValue.Rows[i]["Attributeid"].ToString();
                            dataRow["Uid"] = "0";
                            dtAttributeValues.Rows.Add(dataRow);
                        }
                    }
                    else
                    {
                        for (int i = 0; i < dtAttValue.Rows.Count; i++)
                        {
                            System.Data.DataRow dataRow = dtAttributeValues.NewRow();
                            dataRow["SlNo"] = i + 1;
                            dataRow["AttributeValues"] = dtAttValue.Rows[i]["Values"].ToString();
                            dataRow["AttributeId"] = dtAttValue.Rows[i]["AttributeId"].ToString();
                            dataRow["Uid"] = dtAttValue.Rows[i]["Uid"].ToString();
                            dtAttributeValues.Rows.Add(dataRow);
                        }
                    }

                    for (int i = 0; i < DataGridAlternateUom.Rows.Count - 1; i++)
                    {
                        System.Data.DataRow dataRow = dtAlternateUom.NewRow();
                        dataRow["SlNo"] = DataGridAlternateUom.Rows[i].Cells[0].Value.ToString();
                        dataRow["AlternateUoM"] = DataGridAlternateUom.Rows[i].Cells[1].Value.ToString();
                        dataRow["AlternateUomQty"] = DataGridAlternateUom.Rows[i].Cells[2].Value.ToString();
                        dataRow["BaseUomQty"] = DataGridAlternateUom.Rows[i].Cells[3].Value.ToString();
                        dataRow["AlternateUoMId"] = DataGridAlternateUom.Rows[i].Cells[4].Value.ToString();
                        dtAlternateUom.Rows.Add(dataRow);
                    }
                    for (int i = 0; i < dtAttribute.Rows.Count; i++)
                    {
                        SqlParameter[] parameters = {
                            new SqlParameter("@Uid",dtAttribute.Rows[i]["Uid"].ToString()),
                            new SqlParameter("@AttributeUid",dtAttribute.Rows[i]["AttributeId"].ToString()),
                            new SqlParameter("@UserDefined",dtAttribute.Rows[i]["UserDefined"].ToString()),
                            new SqlParameter("@SeqNo",dtAttribute.Rows[i]["SortOrder"].ToString()),
                            new SqlParameter("@ItemUid",Id),
                            new SqlParameter("@ReturnUid",SqlDbType.Decimal)
                        };
                        parameters[5].Direction = ParameterDirection.Output;
                        attributeUid = db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_ItemsMAttribute", parameters, connection, 5);
                    }

                    for (int i = 0; i < dtAttributeValues.Rows.Count; i++)
                    {
                        SqlParameter[] parameters = {
                            new SqlParameter("@UID",dtAttributeValues.Rows[i]["Uid"].ToString()),
                            new SqlParameter("@ItemUID",Id),
                            new SqlParameter("@AttributeUID",dtAttributeValues.Rows[i]["AttributeId"].ToString()),
                            new SqlParameter("@AttributeValue",dtAttributeValues.Rows[i]["AttributeValues"].ToString()),
                            new SqlParameter("@AttributeValueUID",Id)
                        };
                        db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_ItemsMAttributeValues", parameters, connection);
                    }

                    for (int i = 0; i < dtAlternateUom.Rows.Count; i++)
                    {
                        SqlParameter[] parameters = {
                            new SqlParameter("@UID","0"),
                            new SqlParameter("@ItemUID",Id),
                            new SqlParameter("@AlternateUomUID",dtAlternateUom.Rows[i]["AlternateUoMId"].ToString()),
                            new SqlParameter("@AlternateQty",dtAlternateUom.Rows[i]["AlternateUomQty"].ToString()),
                            new SqlParameter("@BaseUomQty",dtAlternateUom.Rows[i]["BaseUomQty"].ToString()),
                            new SqlParameter("@Active","1")
                        };
                        db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_ItemsMUom", parameters, connection);
                    }
                }
                MessageBox.Show("Record Saved Successfully", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                ClearControl();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected void ClearControl()
        {
            Loadid = 1;
            SelectId = 1;
            CmbCategory.SelectedValue = -1;
            CmbClassification.SelectedValue = -1;
            CmbTax.SelectedValue = -1;
            CmbUom.SelectedValue = -1;
            txtItemName.Text = string.Empty;
            txtShortName.Text = string.Empty;
            DataGridAttribute.Rows.Clear();
            DataGridAttributeValue.Rows.Clear();
            DataGridAlternateUom.Rows.Clear();
            CmbSalesUom.SelectedIndex = -1;
            CmbStockUom.SelectedIndex = -1;
            CmbPurchaseUom.SelectedIndex = -1;
            CmbPurchaseUom.SelectedIndex = -1;
            Loadid = 0;
            SelectId = 0;
        }

        private void DataGridAlternateUom_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex == 1)
                {
                    DataTable dt = LoadGeneralM();
                    DataTable Tax = dt.Select("TypeMUid = 7").CopyToDataTable();
                    bsTax.DataSource = Tax;
                    FillGrid(Tax, 2);
                    GrSearchUom.Visible = true;
                    GrSearchUom.BringToFront();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void DataGridAlternateUom_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                if (e.ColumnIndex == 1)
                {
                    DataTable dt = LoadGeneralM();
                    DataTable Tax = dt.Select("TypeMUid = 7").CopyToDataTable();
                    bsTax.DataSource = Tax;
                    FillGrid(Tax, 2);
                    GrSearchUom.Visible = true;
                    GrSearchUom.BringToFront();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void BtnSelectSearch_Click(object sender, EventArgs e)
        {
            try
            {
                int Index = DataGridSearchUom.SelectedCells[0].RowIndex;
                if (Fillid == 2)
                {
                    bool entryfound = GridCheckValue(DataGridSearchUom.Rows[Index].Cells[1].Value.ToString());
                    if (entryfound == false)
                    {
                        int iColumn = DataGridAlternateUom.CurrentCell.ColumnIndex;
                        int iRow = DataGridAlternateUom.CurrentCell.RowIndex;
                        DataGridAlternateUom[iColumn, iRow].Value = DataGridSearchUom.Rows[Index].Cells[1].Value.ToString();
                        DataGridAlternateUom[4, iRow].Value = DataGridSearchUom.Rows[Index].Cells[0].Value.ToString();
                        //DataGridViewRow row = (DataGridViewRow)DataGridAlternateUom.Rows[iRow];
                        //row.Cells[0].Value = DataGridAlternateUom.Rows.Count - 1;
                        //row.Cells[1].Value = DataGridSearchUom.Rows[Index].Cells[1].Value.ToString();
                        //row.Cells[4].Value = DataGridSearchUom.Rows[Index].Cells[0].Value.ToString();
                        //DataGridAlternateUom.Rows.Add(row);
                        GrSearchUom.Visible = false;
                        GrSearchUom.SendToBack();
                        DataGridAlternateUom.ClearSelection();
                        DataGridAlternateUom.CurrentCell = DataGridAlternateUom[iColumn + 1, iRow];
                        CheckUom(DataGridSearchUom.Rows[Index].Cells[1].Value.ToString(), Convert.ToDecimal(DataGridSearchUom.Rows[Index].Cells[0].Value.ToString()));
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected void CheckUom(string Uom, decimal Value)
        {
            try
            {
                DataTable dt = new DataTable();
                dt.Columns.Add("Uom", typeof(string));
                dt.Columns.Add("UomId", typeof(decimal));
                for (int i = 0; i < DataGridAlternateUom.Rows.Count - 1; i++)
                {
                    System.Data.DataRow row = dt.NewRow();
                    row["Uom"] = DataGridAlternateUom.Rows[i].Cells[1].Value.ToString();
                    row["UomId"] = DataGridAlternateUom.Rows[i].Cells[4].Value.ToString();
                    dt.Rows.Add(row);
                }
                ////}
                if (CmbUom.Text != Uom)
                {
                    System.Data.DataRow row = dt.NewRow();
                    row["Uom"] = CmbUom.Text;
                    row["UomId"] = CmbUom.SelectedValue;
                    dt.Rows.Add(row);
                }
                FillAlternateUom(dt);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected void FillAlternateUom(DataTable dt)
        {
            DataTable dtStock = dt.Copy();
            DataTable dtPurchase = dt.Copy();
            DataTable dtSales = dt.Copy();
            DataTable dtPlanning = dt.Copy();
            CmbStockUom.DataSource = null;
            CmbStockUom.DisplayMember = "Uom";
            CmbStockUom.ValueMember = "UomId";
            CmbStockUom.DataSource = dtStock;

            CmbPurchaseUom.DataSource = null;
            CmbPurchaseUom.DisplayMember = "Uom";
            CmbPurchaseUom.ValueMember = "UomId";
            CmbPurchaseUom.DataSource = dtPurchase;

            CmbSalesUom.DataSource = null;
            CmbSalesUom.DisplayMember = "Uom";
            CmbSalesUom.ValueMember = "UomId";
            CmbSalesUom.DataSource = dtSales;

            CmbPalnningUom.DataSource = null;
            CmbPalnningUom.DisplayMember = "Uom";
            CmbPalnningUom.ValueMember = "UomId";
            CmbPalnningUom.DataSource = dtPlanning;
        }

        protected bool GridCheckValue(string Uom)
        {
            bool entryFound = false;
            try
            {
                foreach (DataGridViewRow row in DataGridAlternateUom.Rows)
                {
                    object ValueStr = row.Cells[1].Value;
                    if (ValueStr != null && ValueStr.ToString() == Uom)
                    {
                        //MessageBox.Show("Entry already exist");
                        entryFound = true;
                        break;
                    }
                }

                if (!entryFound)
                {
                    entryFound = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            return entryFound;
        }

        private void Control_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Up)
            {
                this.SelectNextControl((Control)sender, false, true, true, true);
            }
            else if (e.KeyCode == Keys.Down)
            {
                this.SelectNextControl((Control)sender, true, true, true, true);
            }
            else if (e.KeyCode == Keys.Enter)
            {
                this.SelectNextControl((Control)sender, true, true, true, true);
            }
        }

        private void BtnSearchClose_Click(object sender, EventArgs e)
        {
            GrSearchUom.Visible = false;
        }

        private void TxtSearch_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    DataGridCommon.Focus();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void DataGridCommon_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    int Index = DataGridCommon.SelectedCells[0].RowIndex;
                    int iColumn = DataGridAttribute.CurrentCell.ColumnIndex;
                    int iRow = DataGridAttribute.CurrentCell.RowIndex;
                    DataGridViewRow row = (DataGridViewRow)DataGridAttribute.Rows[0].Clone();
                    row.Cells[0].Value = DataGridAttribute.Rows.Count;
                    row.Cells[1].Value = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    row.Cells[2].Value = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    row.Cells[3].Value = "0";
                    DataGridAttribute.Rows.Add(row);
                    grSearch.Visible = false;
                    grSearch.SendToBack();
                    DataGridAttribute.ClearSelection();
                    DataGridAttribute.CurrentCell = DataGridAttribute[iColumn + 3, iRow];
                }
                else if (e.KeyCode == Keys.Down)
                {
                    MoveDown(DataGridCommon);
                }
                else if (e.KeyCode == Keys.Up)
                {
                    MoveUp(DataGridCommon);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void MoveUp(DataGridView dataGridView)
        {
            if (dataGridView.RowCount > 0)
            {
                int rowCount = dataGridView.Rows.Count;
                int index = dataGridView.SelectedCells[0].OwningRow.Index;

                if (index == 0)
                {
                    return;
                }
                dataGridView.ClearSelection();
                dataGridView.Rows[index - 1].Selected = true;
            }
        }

        private void MoveDown(DataGridView dataGridView)
        {
            if (dataGridView.RowCount > 0)
            {
                int rowCount = dataGridView.Rows.Count;
                int index = dataGridView.SelectedCells[0].OwningRow.Index;
                if (index == (rowCount - 2)) // include the header row
                {
                    return;
                }
                dataGridView.ClearSelection();
                dataGridView.Rows[index + 1].Selected = true;
            }
        }

        private void TxtAttribute_Click(object sender, EventArgs e)
        {
            try
            {
                if (CmbCategory.SelectedIndex != -1)
                {
                    DataTable dt = LoadGeneralM();
                    DataTable Attribue = dt.Select("TypeMUid = 28").CopyToDataTable();
                    DataTable newTable = new DataTable();
                    System.Data.DataRow[] tblROWS = Attribue.Select("F3 =" + CmbCategory.SelectedValue + "");
                    if (tblROWS.Length > 0)
                    {
                        newTable = Attribue.Select("F3 =" + CmbCategory.SelectedValue + "").CopyToDataTable();
                        bsAttribute.DataSource = newTable;
                        CmbAttributeAdd.DataSource = null;
                        CmbAttributeAdd.DisplayMember = "GeneralName";
                        CmbAttributeAdd.ValueMember = "Guid";
                        CmbAttributeAdd.DataSource = newTable;
                        //FillGrid(Attribue, 1);
                    }
                    //grSearch.Visible = true;
                    //grSearch.BringToFront();
                }
                else
                {
                    MessageBox.Show("Select Category", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    CmbCategory.Focus();
                    return;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void BtnAttributeOk_Click(object sender, EventArgs e)
        {
            try
            {
                bool userdefined = false;
                if (chckUserefined.Checked == true)
                {
                    userdefined = true;
                }
                else
                {
                    userdefined = false;
                }
                if (txtAttribute.Text != string.Empty || CmbSortOrder.SelectedIndex != -1)
                {
                    DataGridViewRow row = (DataGridViewRow)DataGridAttribute.Rows[0].Clone();
                    row.Cells[0].Value = DataGridAttribute.Rows.Count;
                    row.Cells[1].Value = CmbAttributeAdd.Text;
                    row.Cells[2].Value = CmbAttributeAdd.SelectedValue;
                    row.Cells[3].Value = "0";
                    row.Cells[4].Value = userdefined;
                    row.Cells[5].Value = CmbSortOrder.Text;
                    DataGridAttribute.Rows.Add(row);
                    SelectId = 1;
                    txtAttribute.Text = string.Empty;
                    chckUserefined.Checked = false;
                    CmbSortOrder.SelectedIndex = -1;
                    SelectId = 0;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void TxtAttribute_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bsAttribute.Filter = string.Format("GeneralName Like '%{0}%'", txtAttribute.Text);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void DataGridCommon_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                SelectId = 1;
                int Index = DataGridCommon.SelectedCells[0].RowIndex;
                if (Fillid == 1)
                {
                    txtAttribute.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtAttribute.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                }
                grSearch.Visible = false;
                SelectId = 0;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void CmbAttribute_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0 && Mode == 2)
                {
                    if (dtAttValue.Rows.Count > 0)
                    {
                        System.Data.DataRow[] myResultSet = dtAttValue.Select("AttributeId = " + CmbAttribute.SelectedValue + "");
                        DataTable dt = new DataTable();
                        DataGridAttributeValue.Rows.Clear();
                        if (myResultSet.Length > 0)
                        {
                            dt = dtAttValue.Select("AttributeId = " + CmbAttribute.SelectedValue + "", "Uid").CopyToDataTable();
                            for (int i = 0; i < dt.Rows.Count; i++)
                            {
                                DataGridViewRow dataGridViewRow = (DataGridViewRow)DataGridAttributeValue.Rows[0].Clone();
                                dataGridViewRow.Cells[0].Value = i + 1;
                                dataGridViewRow.Cells[1].Value = dt.Rows[i]["Values"].ToString();
                                dataGridViewRow.Cells[2].Value = dt.Rows[i]["AttributeId"].ToString();
                                dataGridViewRow.Cells[3].Value = "0";
                                DataGridAttributeValue.Rows.Add(dataGridViewRow);
                            }
                        }
                        else
                        {
                            DataGridAttributeValue.Rows.Clear();
                        }
                    }
                }
                else
                {
                    if (SelectId == 0)
                    {
                        if (CmbAttribute.SelectedValue != null)
                        {
                            System.Data.DataRow[] myResultSet = dtAttValue.Select("AttributeId = " + CmbAttribute.SelectedValue + "");
                            if (myResultSet.Length > 0)
                            {
                                DataTable dt = new DataTable();
                                DataGridAttributeValue.Rows.Clear();
                                if (myResultSet.Length > 0)
                                {
                                    dt = dtAttValue.Select("AttributeId = " + CmbAttribute.SelectedValue + "", "Slno").CopyToDataTable();
                                    for (int i = 0; i < dt.Rows.Count; i++)
                                    {
                                        DataGridViewRow dataGridViewRow = (DataGridViewRow)DataGridAttributeValue.Rows[0].Clone();
                                        dataGridViewRow.Cells[0].Value = i + 1;
                                        dataGridViewRow.Cells[1].Value = dt.Rows[i]["Values"].ToString();
                                        dataGridViewRow.Cells[2].Value = dt.Rows[i]["AttributeId"].ToString();
                                        dataGridViewRow.Cells[3].Value = "0";
                                        DataGridAttributeValue.Rows.Add(dataGridViewRow);
                                    }
                                }
                                SelectId = 0;
                            }
                            else
                            {
                                DataGridAttributeValue.Rows.Clear();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void BtnOkAttributeVal_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtSlno.Text != string.Empty || txtAttributeval.Text != string.Empty)
                {
                    System.Data.DataRow dataRow = dtAttValue.NewRow();
                    dataRow["SlNo"] = txtSlno.Text;
                    dataRow["Values"] = txtAttributeval.Text;
                    dataRow["AttributeId"] = CmbAttribute.SelectedValue;
                    dataRow["Uid"] = "0";
                    dtAttValue.Rows.Add(dataRow);

                    DataGridViewRow dataGridViewRow = (DataGridViewRow)DataGridAttributeValue.Rows[0].Clone();
                    dataGridViewRow.Cells[0].Value = txtSlno.Text;
                    dataGridViewRow.Cells[1].Value = txtAttributeval.Text;
                    dataGridViewRow.Cells[2].Value = CmbAttribute.SelectedValue;
                    dataGridViewRow.Cells[3].Value = "0";
                    DataGridAttributeValue.Rows.Add(dataGridViewRow);
                    txtSlno.Text = string.Empty;
                    txtAttributeval.Text = string.Empty;
                    txtSlno.Focus();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
    }
}
