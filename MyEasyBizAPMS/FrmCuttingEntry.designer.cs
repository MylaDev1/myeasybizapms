﻿namespace MyEasyBizAPMS
{
    partial class FrmCuttingEntry
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmCuttingEntry));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.Genpan = new System.Windows.Forms.Panel();
            this.txtscr1 = new System.Windows.Forms.TextBox();
            this.HFGP = new System.Windows.Forms.DataGridView();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.flowLayoutPanel4 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel5 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel6 = new System.Windows.Forms.FlowLayoutPanel();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.label57 = new System.Windows.Forms.Label();
            this.dtpfnt = new System.Windows.Forms.DateTimePicker();
            this.txtscr8 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.txtscr7 = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.txtscr5 = new System.Windows.Forms.TextBox();
            this.Txtscr2 = new System.Windows.Forms.TextBox();
            this.txtscr4 = new System.Windows.Forms.TextBox();
            this.Txtscr3 = new System.Windows.Forms.TextBox();
            this.panadd = new System.Windows.Forms.Panel();
            this.button13 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.buttnext1 = new System.Windows.Forms.Button();
            this.chkact = new System.Windows.Forms.CheckBox();
            this.butcan = new System.Windows.Forms.Button();
            this.butedit = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.Editpnl = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.txtoutsourcety = new System.Windows.Forms.TextBox();
            this.cbocontratorname = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cboworktype = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cbocuttype = new System.Windows.Forms.ComboBox();
            this.label39 = new System.Windows.Forms.Label();
            this.cbojobtype = new System.Windows.Forms.ComboBox();
            this.label38 = new System.Windows.Forms.Label();
            this.tabC = new System.Windows.Forms.TabControl();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.lkppnl = new System.Windows.Forms.Panel();
            this.button10 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.HFGP2 = new System.Windows.Forms.DataGridView();
            this.label69 = new System.Windows.Forms.Label();
            this.label67 = new System.Windows.Forms.Label();
            this.cbopono = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtoutuom = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.RQGR = new System.Windows.Forms.DataGridView();
            this.txtoutqty = new System.Windows.Forms.TextBox();
            this.label33 = new System.Windows.Forms.Label();
            this.txtoutrolls = new System.Windows.Forms.TextBox();
            this.label34 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.txtoutitem = new System.Windows.Forms.TextBox();
            this.reqdt = new System.Windows.Forms.DateTimePicker();
            this.label35 = new System.Windows.Forms.Label();
            this.button17 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.txtitem = new System.Windows.Forms.TextBox();
            this.txtrectot = new System.Windows.Forms.TextBox();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.label16 = new System.Windows.Forms.Label();
            this.componentgrid = new System.Windows.Forms.DataGridView();
            this.label14 = new System.Windows.Forms.Label();
            this.DataGridSizeCombo = new System.Windows.Forms.DataGridView();
            this.label15 = new System.Windows.Forms.Label();
            this.DataGridSizeWise = new System.Windows.Forms.DataGridView();
            this.button11 = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.cbofabric = new System.Windows.Forms.ComboBox();
            this.grSearch = new System.Windows.Forms.Panel();
            this.button18 = new System.Windows.Forms.Button();
            this.btnHide = new System.Windows.Forms.Button();
            this.DataGridCommon = new System.Windows.Forms.DataGridView();
            this.button22 = new System.Windows.Forms.Button();
            this.HFIT = new System.Windows.Forms.DataGridView();
            this.txtwt = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtuomm = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.txtoutoutqty = new System.Windows.Forms.TextBox();
            this.txtdcqty = new System.Windows.Forms.TextBox();
            this.txtuom = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtbillqty = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtqty = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.Phone = new System.Windows.Forms.Label();
            this.txtgrn = new System.Windows.Forms.TextBox();
            this.dtpgrndt = new System.Windows.Forms.DateTimePicker();
            this.txtterms = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.txtbeamid = new System.Windows.Forms.TextBox();
            this.txtoutputid = new System.Windows.Forms.TextBox();
            this.txtitemid = new System.Windows.Forms.TextBox();
            this.txtmillid = new System.Windows.Forms.TextBox();
            this.txtpuid = new System.Windows.Forms.TextBox();
            this.txtgrnid = new System.Windows.Forms.TextBox();
            this.txtnobeams = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.buttnfinbk = new System.Windows.Forms.Button();
            this.btnsave = new System.Windows.Forms.Button();
            this.Chkedtact = new System.Windows.Forms.CheckBox();
            this.Genpan.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HFGP)).BeginInit();
            this.panel2.SuspendLayout();
            this.panadd.SuspendLayout();
            this.Editpnl.SuspendLayout();
            this.tabC.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.lkppnl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HFGP2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RQGR)).BeginInit();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.componentgrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridSizeCombo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridSizeWise)).BeginInit();
            this.grSearch.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridCommon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HFIT)).BeginInit();
            this.SuspendLayout();
            // 
            // Genpan
            // 
            this.Genpan.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.Genpan.Controls.Add(this.txtscr1);
            this.Genpan.Controls.Add(this.HFGP);
            this.Genpan.Controls.Add(this.panel2);
            this.Genpan.Controls.Add(this.button5);
            this.Genpan.Controls.Add(this.button6);
            this.Genpan.Controls.Add(this.button7);
            this.Genpan.Controls.Add(this.button8);
            this.Genpan.Controls.Add(this.label57);
            this.Genpan.Controls.Add(this.dtpfnt);
            this.Genpan.Controls.Add(this.txtscr8);
            this.Genpan.Controls.Add(this.textBox2);
            this.Genpan.Controls.Add(this.txtscr7);
            this.Genpan.Controls.Add(this.textBox3);
            this.Genpan.Controls.Add(this.txtscr5);
            this.Genpan.Controls.Add(this.Txtscr2);
            this.Genpan.Controls.Add(this.txtscr4);
            this.Genpan.Controls.Add(this.Txtscr3);
            this.Genpan.Location = new System.Drawing.Point(1, 2);
            this.Genpan.Margin = new System.Windows.Forms.Padding(4);
            this.Genpan.Name = "Genpan";
            this.Genpan.Size = new System.Drawing.Size(924, 576);
            this.Genpan.TabIndex = 216;
            // 
            // txtscr1
            // 
            this.txtscr1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtscr1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtscr1.Location = new System.Drawing.Point(5, 12);
            this.txtscr1.Margin = new System.Windows.Forms.Padding(5);
            this.txtscr1.Name = "txtscr1";
            this.txtscr1.Size = new System.Drawing.Size(914, 26);
            this.txtscr1.TabIndex = 1;
            // 
            // HFGP
            // 
            this.HFGP.BackgroundColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.HFGP.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.HFGP.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnF2;
            this.HFGP.Location = new System.Drawing.Point(1, 47);
            this.HFGP.Margin = new System.Windows.Forms.Padding(4);
            this.HFGP.Name = "HFGP";
            this.HFGP.ReadOnly = true;
            this.HFGP.Size = new System.Drawing.Size(919, 525);
            this.HFGP.TabIndex = 3;
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel2.Controls.Add(this.label12);
            this.panel2.Controls.Add(this.label13);
            this.panel2.Controls.Add(this.flowLayoutPanel4);
            this.panel2.Controls.Add(this.flowLayoutPanel5);
            this.panel2.Controls.Add(this.flowLayoutPanel6);
            this.panel2.Location = new System.Drawing.Point(66, 390);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(60, 30);
            this.panel2.TabIndex = 214;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(4, 5);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(15, 16);
            this.label12.TabIndex = 163;
            this.label12.Text = "1";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Black;
            this.label13.Location = new System.Drawing.Point(27, 5);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(29, 16);
            this.label13.TabIndex = 162;
            this.label13.Text = "of 1";
            // 
            // flowLayoutPanel4
            // 
            this.flowLayoutPanel4.Location = new System.Drawing.Point(61, 30);
            this.flowLayoutPanel4.Name = "flowLayoutPanel4";
            this.flowLayoutPanel4.Size = new System.Drawing.Size(200, 100);
            this.flowLayoutPanel4.TabIndex = 2;
            // 
            // flowLayoutPanel5
            // 
            this.flowLayoutPanel5.Location = new System.Drawing.Point(38, 35);
            this.flowLayoutPanel5.Name = "flowLayoutPanel5";
            this.flowLayoutPanel5.Size = new System.Drawing.Size(200, 100);
            this.flowLayoutPanel5.TabIndex = 1;
            // 
            // flowLayoutPanel6
            // 
            this.flowLayoutPanel6.Location = new System.Drawing.Point(62, -129);
            this.flowLayoutPanel6.Name = "flowLayoutPanel6";
            this.flowLayoutPanel6.Size = new System.Drawing.Size(200, 100);
            this.flowLayoutPanel6.TabIndex = 0;
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.Color.White;
            this.button5.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button5.FlatAppearance.BorderSize = 0;
            this.button5.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button5.Image = ((System.Drawing.Image)(resources.GetObject("button5.Image")));
            this.button5.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button5.Location = new System.Drawing.Point(19, 390);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(19, 31);
            this.button5.TabIndex = 213;
            this.button5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button5.UseVisualStyleBackColor = false;
            // 
            // button6
            // 
            this.button6.BackColor = System.Drawing.Color.White;
            this.button6.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button6.FlatAppearance.BorderSize = 0;
            this.button6.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button6.Image = ((System.Drawing.Image)(resources.GetObject("button6.Image")));
            this.button6.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button6.Location = new System.Drawing.Point(44, 390);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(18, 31);
            this.button6.TabIndex = 212;
            this.button6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button6.UseVisualStyleBackColor = false;
            // 
            // button7
            // 
            this.button7.BackColor = System.Drawing.Color.White;
            this.button7.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button7.FlatAppearance.BorderSize = 0;
            this.button7.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button7.Image = ((System.Drawing.Image)(resources.GetObject("button7.Image")));
            this.button7.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button7.Location = new System.Drawing.Point(156, 390);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(19, 31);
            this.button7.TabIndex = 211;
            this.button7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button7.UseVisualStyleBackColor = false;
            // 
            // button8
            // 
            this.button8.BackColor = System.Drawing.Color.White;
            this.button8.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button8.FlatAppearance.BorderSize = 0;
            this.button8.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button8.Image = ((System.Drawing.Image)(resources.GetObject("button8.Image")));
            this.button8.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button8.Location = new System.Drawing.Point(132, 390);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(18, 31);
            this.button8.TabIndex = 210;
            this.button8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button8.UseVisualStyleBackColor = false;
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label57.Location = new System.Drawing.Point(411, 167);
            this.label57.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(42, 21);
            this.label57.TabIndex = 223;
            this.label57.Text = "Date";
            // 
            // dtpfnt
            // 
            this.dtpfnt.CustomFormat = "MMM/yyyy";
            this.dtpfnt.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpfnt.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpfnt.Location = new System.Drawing.Point(458, 164);
            this.dtpfnt.Margin = new System.Windows.Forms.Padding(4);
            this.dtpfnt.Name = "dtpfnt";
            this.dtpfnt.Size = new System.Drawing.Size(104, 26);
            this.dtpfnt.TabIndex = 222;
            this.dtpfnt.Value = new System.DateTime(2017, 7, 4, 0, 0, 0, 0);
            // 
            // txtscr8
            // 
            this.txtscr8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtscr8.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtscr8.Location = new System.Drawing.Point(651, 230);
            this.txtscr8.Margin = new System.Windows.Forms.Padding(5);
            this.txtscr8.Name = "txtscr8";
            this.txtscr8.Size = new System.Drawing.Size(100, 26);
            this.txtscr8.TabIndex = 225;
            // 
            // textBox2
            // 
            this.textBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox2.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox2.Location = new System.Drawing.Point(694, 162);
            this.textBox2.Margin = new System.Windows.Forms.Padding(5);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(101, 26);
            this.textBox2.TabIndex = 227;
            // 
            // txtscr7
            // 
            this.txtscr7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtscr7.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtscr7.Location = new System.Drawing.Point(398, 230);
            this.txtscr7.Margin = new System.Windows.Forms.Padding(5);
            this.txtscr7.Name = "txtscr7";
            this.txtscr7.Size = new System.Drawing.Size(164, 26);
            this.txtscr7.TabIndex = 224;
            // 
            // textBox3
            // 
            this.textBox3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox3.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox3.Location = new System.Drawing.Point(500, 158);
            this.textBox3.Margin = new System.Windows.Forms.Padding(5);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(150, 26);
            this.textBox3.TabIndex = 226;
            // 
            // txtscr5
            // 
            this.txtscr5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtscr5.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtscr5.Location = new System.Drawing.Point(150, 158);
            this.txtscr5.Margin = new System.Windows.Forms.Padding(5);
            this.txtscr5.Name = "txtscr5";
            this.txtscr5.Size = new System.Drawing.Size(350, 26);
            this.txtscr5.TabIndex = 90;
            // 
            // Txtscr2
            // 
            this.Txtscr2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Txtscr2.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txtscr2.Location = new System.Drawing.Point(61, 158);
            this.Txtscr2.Margin = new System.Windows.Forms.Padding(5);
            this.Txtscr2.Name = "Txtscr2";
            this.Txtscr2.Size = new System.Drawing.Size(90, 26);
            this.Txtscr2.TabIndex = 87;
            // 
            // txtscr4
            // 
            this.txtscr4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtscr4.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtscr4.Location = new System.Drawing.Point(272, 139);
            this.txtscr4.Margin = new System.Windows.Forms.Padding(5);
            this.txtscr4.Name = "txtscr4";
            this.txtscr4.Size = new System.Drawing.Size(90, 22);
            this.txtscr4.TabIndex = 100;
            // 
            // Txtscr3
            // 
            this.Txtscr3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Txtscr3.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txtscr3.Location = new System.Drawing.Point(183, 139);
            this.Txtscr3.Margin = new System.Windows.Forms.Padding(5);
            this.Txtscr3.Name = "Txtscr3";
            this.Txtscr3.Size = new System.Drawing.Size(90, 22);
            this.Txtscr3.TabIndex = 88;
            // 
            // panadd
            // 
            this.panadd.BackColor = System.Drawing.Color.White;
            this.panadd.Controls.Add(this.button13);
            this.panadd.Controls.Add(this.button1);
            this.panadd.Controls.Add(this.buttnext1);
            this.panadd.Controls.Add(this.chkact);
            this.panadd.Controls.Add(this.butcan);
            this.panadd.Controls.Add(this.butedit);
            this.panadd.Controls.Add(this.button3);
            this.panadd.Location = new System.Drawing.Point(4, 579);
            this.panadd.Name = "panadd";
            this.panadd.Size = new System.Drawing.Size(921, 37);
            this.panadd.TabIndex = 237;
            // 
            // button13
            // 
            this.button13.BackColor = System.Drawing.Color.White;
            this.button13.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button13.Image = ((System.Drawing.Image)(resources.GetObject("button13.Image")));
            this.button13.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button13.Location = new System.Drawing.Point(381, 0);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(117, 30);
            this.button13.TabIndex = 225;
            this.button13.Text = "Print Preview";
            this.button13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button13.UseVisualStyleBackColor = false;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.White;
            this.button1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Image = ((System.Drawing.Image)(resources.GetObject("button1.Image")));
            this.button1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button1.Location = new System.Drawing.Point(313, 1);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(65, 30);
            this.button1.TabIndex = 216;
            this.button1.Text = "Print";
            this.button1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button1.UseVisualStyleBackColor = false;
            // 
            // buttnext1
            // 
            this.buttnext1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttnext1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttnext1.Image = ((System.Drawing.Image)(resources.GetObject("buttnext1.Image")));
            this.buttnext1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttnext1.Location = new System.Drawing.Point(504, 1);
            this.buttnext1.Name = "buttnext1";
            this.buttnext1.Size = new System.Drawing.Size(57, 30);
            this.buttnext1.TabIndex = 208;
            this.buttnext1.Text = "Exit";
            this.buttnext1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttnext1.UseVisualStyleBackColor = false;
            this.buttnext1.Click += new System.EventHandler(this.buttnext1_Click);
            // 
            // chkact
            // 
            this.chkact.AutoSize = true;
            this.chkact.BackColor = System.Drawing.Color.White;
            this.chkact.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkact.Location = new System.Drawing.Point(8, 9);
            this.chkact.Name = "chkact";
            this.chkact.Size = new System.Drawing.Size(66, 22);
            this.chkact.TabIndex = 187;
            this.chkact.Text = "Active";
            this.chkact.UseVisualStyleBackColor = false;
            // 
            // butcan
            // 
            this.butcan.BackColor = System.Drawing.Color.White;
            this.butcan.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.butcan.Image = ((System.Drawing.Image)(resources.GetObject("butcan.Image")));
            this.butcan.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.butcan.Location = new System.Drawing.Point(228, 0);
            this.butcan.Name = "butcan";
            this.butcan.Size = new System.Drawing.Size(77, 30);
            this.butcan.TabIndex = 186;
            this.butcan.Text = "Delete";
            this.butcan.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.butcan.UseVisualStyleBackColor = false;
            this.butcan.Click += new System.EventHandler(this.butcan_Click);
            // 
            // butedit
            // 
            this.butedit.BackColor = System.Drawing.Color.White;
            this.butedit.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.butedit.Image = ((System.Drawing.Image)(resources.GetObject("butedit.Image")));
            this.butedit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.butedit.Location = new System.Drawing.Point(168, 0);
            this.butedit.Name = "butedit";
            this.butedit.Size = new System.Drawing.Size(55, 30);
            this.butedit.TabIndex = 185;
            this.butedit.Text = "Edit";
            this.butedit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.butedit.UseVisualStyleBackColor = false;
            this.butedit.Click += new System.EventHandler(this.butedit_Click);
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.White;
            this.button3.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button3.FlatAppearance.BorderSize = 0;
            this.button3.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.Image = ((System.Drawing.Image)(resources.GetObject("button3.Image")));
            this.button3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button3.Location = new System.Drawing.Point(75, 0);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(89, 30);
            this.button3.TabIndex = 184;
            this.button3.Text = "Add New";
            this.button3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // Editpnl
            // 
            this.Editpnl.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.Editpnl.Controls.Add(this.label4);
            this.Editpnl.Controls.Add(this.txtoutsourcety);
            this.Editpnl.Controls.Add(this.cbocontratorname);
            this.Editpnl.Controls.Add(this.label3);
            this.Editpnl.Controls.Add(this.cboworktype);
            this.Editpnl.Controls.Add(this.label1);
            this.Editpnl.Controls.Add(this.cbocuttype);
            this.Editpnl.Controls.Add(this.label39);
            this.Editpnl.Controls.Add(this.cbojobtype);
            this.Editpnl.Controls.Add(this.label38);
            this.Editpnl.Controls.Add(this.tabC);
            this.Editpnl.Controls.Add(this.label9);
            this.Editpnl.Controls.Add(this.txtqty);
            this.Editpnl.Controls.Add(this.label2);
            this.Editpnl.Controls.Add(this.Phone);
            this.Editpnl.Controls.Add(this.txtgrn);
            this.Editpnl.Controls.Add(this.dtpgrndt);
            this.Editpnl.Controls.Add(this.txtterms);
            this.Editpnl.Controls.Add(this.label20);
            this.Editpnl.Controls.Add(this.txtbeamid);
            this.Editpnl.Controls.Add(this.txtoutputid);
            this.Editpnl.Controls.Add(this.txtitemid);
            this.Editpnl.Controls.Add(this.txtmillid);
            this.Editpnl.Controls.Add(this.txtpuid);
            this.Editpnl.Controls.Add(this.txtgrnid);
            this.Editpnl.Controls.Add(this.txtnobeams);
            this.Editpnl.Controls.Add(this.label17);
            this.Editpnl.Location = new System.Drawing.Point(1, -5);
            this.Editpnl.Name = "Editpnl";
            this.Editpnl.Size = new System.Drawing.Size(924, 583);
            this.Editpnl.TabIndex = 238;
            this.Editpnl.Paint += new System.Windows.Forms.PaintEventHandler(this.Editpnl_Paint);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(741, 50);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(91, 15);
            this.label4.TabIndex = 5611;
            this.label4.Text = "OutSource Type";
            // 
            // txtoutsourcety
            // 
            this.txtoutsourcety.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtoutsourcety.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtoutsourcety.Location = new System.Drawing.Point(740, 70);
            this.txtoutsourcety.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtoutsourcety.Name = "txtoutsourcety";
            this.txtoutsourcety.Size = new System.Drawing.Size(108, 23);
            this.txtoutsourcety.TabIndex = 5610;
            // 
            // cbocontratorname
            // 
            this.cbocontratorname.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbocontratorname.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbocontratorname.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbocontratorname.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbocontratorname.FormattingEnabled = true;
            this.cbocontratorname.Location = new System.Drawing.Point(584, 69);
            this.cbocontratorname.Name = "cbocontratorname";
            this.cbocontratorname.Size = new System.Drawing.Size(149, 23);
            this.cbocontratorname.TabIndex = 5609;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(584, 49);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(93, 15);
            this.label3.TabIndex = 5608;
            this.label3.Text = "Contractor Type";
            // 
            // cboworktype
            // 
            this.cboworktype.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboworktype.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboworktype.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboworktype.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboworktype.FormattingEnabled = true;
            this.cboworktype.Items.AddRange(new object[] {
            "Shift",
            "Incharge"});
            this.cboworktype.Location = new System.Drawing.Point(740, 24);
            this.cboworktype.Name = "cboworktype";
            this.cboworktype.Size = new System.Drawing.Size(108, 23);
            this.cboworktype.TabIndex = 5607;
            this.cboworktype.SelectedIndexChanged += new System.EventHandler(this.comboBox3_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(737, 6);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(34, 15);
            this.label1.TabIndex = 5606;
            this.label1.Text = " Type";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // cbocuttype
            // 
            this.cbocuttype.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbocuttype.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbocuttype.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbocuttype.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbocuttype.FormattingEnabled = true;
            this.cbocuttype.Items.AddRange(new object[] {
            "PANEL FROM FABRIC ROLL",
            "BIT FROM FABRIC ROLL",
            "BIT FROM BIT",
            "PANEL FROM BIT",
            "END BIT"});
            this.cbocuttype.Location = new System.Drawing.Point(429, 25);
            this.cbocuttype.Name = "cbocuttype";
            this.cbocuttype.Size = new System.Drawing.Size(304, 23);
            this.cbocuttype.TabIndex = 5605;
            this.cbocuttype.SelectedIndexChanged += new System.EventHandler(this.cbocuttype_SelectedIndexChanged);
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.Location = new System.Drawing.Point(429, 7);
            this.label39.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(72, 15);
            this.label39.TabIndex = 5604;
            this.label39.Text = "Cutting Type";
            // 
            // cbojobtype
            // 
            this.cbojobtype.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbojobtype.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbojobtype.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbojobtype.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbojobtype.FormattingEnabled = true;
            this.cbojobtype.Items.AddRange(new object[] {
            "Regular",
            "Rework"});
            this.cbojobtype.Location = new System.Drawing.Point(268, 25);
            this.cbojobtype.Name = "cbojobtype";
            this.cbojobtype.Size = new System.Drawing.Size(149, 23);
            this.cbojobtype.TabIndex = 5603;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.Location = new System.Drawing.Point(268, 7);
            this.label38.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(36, 15);
            this.label38.TabIndex = 5602;
            this.label38.Text = "Work";
            // 
            // tabC
            // 
            this.tabC.Controls.Add(this.tabPage2);
            this.tabC.Controls.Add(this.tabPage3);
            this.tabC.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabC.Location = new System.Drawing.Point(14, 98);
            this.tabC.Name = "tabC";
            this.tabC.SelectedIndex = 0;
            this.tabC.Size = new System.Drawing.Size(897, 480);
            this.tabC.TabIndex = 443;
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.tabPage2.Controls.Add(this.lkppnl);
            this.tabPage2.Controls.Add(this.txtoutuom);
            this.tabPage2.Controls.Add(this.label32);
            this.tabPage2.Controls.Add(this.RQGR);
            this.tabPage2.Controls.Add(this.txtoutqty);
            this.tabPage2.Controls.Add(this.label33);
            this.tabPage2.Controls.Add(this.txtoutrolls);
            this.tabPage2.Controls.Add(this.label34);
            this.tabPage2.Controls.Add(this.label28);
            this.tabPage2.Controls.Add(this.txtoutitem);
            this.tabPage2.Controls.Add(this.reqdt);
            this.tabPage2.Controls.Add(this.label35);
            this.tabPage2.Controls.Add(this.button17);
            this.tabPage2.Controls.Add(this.button4);
            this.tabPage2.Controls.Add(this.label7);
            this.tabPage2.Controls.Add(this.txtitem);
            this.tabPage2.Controls.Add(this.txtrectot);
            this.tabPage2.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabPage2.Location = new System.Drawing.Point(4, 24);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(889, 452);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Fabric";
            // 
            // lkppnl
            // 
            this.lkppnl.BackColor = System.Drawing.SystemColors.HighlightText;
            this.lkppnl.Controls.Add(this.button10);
            this.lkppnl.Controls.Add(this.button2);
            this.lkppnl.Controls.Add(this.HFGP2);
            this.lkppnl.Controls.Add(this.label69);
            this.lkppnl.Controls.Add(this.label67);
            this.lkppnl.Controls.Add(this.cbopono);
            this.lkppnl.Controls.Add(this.label10);
            this.lkppnl.Location = new System.Drawing.Point(3, 84);
            this.lkppnl.Name = "lkppnl";
            this.lkppnl.Size = new System.Drawing.Size(626, 319);
            this.lkppnl.TabIndex = 991;
            // 
            // button10
            // 
            this.button10.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button10.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button10.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button10.Location = new System.Drawing.Point(543, 286);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(76, 28);
            this.button10.TabIndex = 396;
            this.button10.Text = "Select ";
            this.button10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button10.UseVisualStyleBackColor = false;
            this.button10.Click += new System.EventHandler(this.button10_Click_1);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button2.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Image = ((System.Drawing.Image)(resources.GetObject("button2.Image")));
            this.button2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button2.Location = new System.Drawing.Point(5, 284);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(69, 27);
            this.button2.TabIndex = 395;
            this.button2.Text = "Close";
            this.button2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // HFGP2
            // 
            this.HFGP2.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.HFGP2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.HFGP2.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.HFGP2.Location = new System.Drawing.Point(2, 9);
            this.HFGP2.Name = "HFGP2";
            this.HFGP2.Size = new System.Drawing.Size(614, 269);
            this.HFGP2.TabIndex = 97;
            this.HFGP2.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.HFGP2_CellContentClick);
            this.HFGP2.Click += new System.EventHandler(this.HFGP2_Click);
            this.HFGP2.DoubleClick += new System.EventHandler(this.HFGP2_DoubleClick);
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label69.ForeColor = System.Drawing.Color.White;
            this.label69.Location = new System.Drawing.Point(64, 85);
            this.label69.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(81, 21);
            this.label69.TabIndex = 196;
            this.label69.Text = "Itemname";
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label67.ForeColor = System.Drawing.Color.White;
            this.label67.Location = new System.Drawing.Point(8, 184);
            this.label67.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(124, 21);
            this.label67.TabIndex = 198;
            this.label67.Text = "Press ESC to Exit";
            // 
            // cbopono
            // 
            this.cbopono.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbopono.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbopono.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbopono.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbopono.FormattingEnabled = true;
            this.cbopono.Items.AddRange(new object[] {
            "Yarn",
            "Fabric",
            "Trims"});
            this.cbopono.Location = new System.Drawing.Point(93, 109);
            this.cbopono.Name = "cbopono";
            this.cbopono.Size = new System.Drawing.Size(303, 26);
            this.cbopono.TabIndex = 1234;
            this.cbopono.SelectedIndexChanged += new System.EventHandler(this.cbopono_SelectedIndexChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(79, 106);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(37, 18);
            this.label10.TabIndex = 1233;
            this.label10.Text = "Type";
            // 
            // txtoutuom
            // 
            this.txtoutuom.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtoutuom.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtoutuom.Location = new System.Drawing.Point(569, 25);
            this.txtoutuom.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtoutuom.Name = "txtoutuom";
            this.txtoutuom.Size = new System.Drawing.Size(84, 23);
            this.txtoutuom.TabIndex = 990;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.Location = new System.Drawing.Point(572, 1);
            this.label32.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(32, 15);
            this.label32.TabIndex = 426;
            this.label32.Text = "Uom";
            // 
            // RQGR
            // 
            this.RQGR.BackgroundColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.RQGR.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.RQGR.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.RQGR.Location = new System.Drawing.Point(27, 54);
            this.RQGR.Name = "RQGR";
            this.RQGR.Size = new System.Drawing.Size(828, 369);
            this.RQGR.TabIndex = 416;
            this.RQGR.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.RQGR_CellClick);
            this.RQGR.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.RQGR_CellContentClick);
            // 
            // txtoutqty
            // 
            this.txtoutqty.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtoutqty.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtoutqty.Location = new System.Drawing.Point(654, 25);
            this.txtoutqty.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtoutqty.Name = "txtoutqty";
            this.txtoutqty.Size = new System.Drawing.Size(71, 23);
            this.txtoutqty.TabIndex = 418;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(731, 1);
            this.label33.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(35, 15);
            this.label33.TabIndex = 422;
            this.label33.Text = "Rolls";
            // 
            // txtoutrolls
            // 
            this.txtoutrolls.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtoutrolls.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtoutrolls.Location = new System.Drawing.Point(726, 25);
            this.txtoutrolls.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtoutrolls.Name = "txtoutrolls";
            this.txtoutrolls.Size = new System.Drawing.Size(74, 23);
            this.txtoutrolls.TabIndex = 419;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.Location = new System.Drawing.Point(664, 1);
            this.label34.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(46, 15);
            this.label34.TabIndex = 421;
            this.label34.Text = "Weight";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(639, 158);
            this.label28.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(102, 19);
            this.label28.TabIndex = 432;
            this.label28.Text = "Required Date";
            // 
            // txtoutitem
            // 
            this.txtoutitem.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtoutitem.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtoutitem.Location = new System.Drawing.Point(24, 25);
            this.txtoutitem.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtoutitem.Name = "txtoutitem";
            this.txtoutitem.Size = new System.Drawing.Size(544, 23);
            this.txtoutitem.TabIndex = 417;
            this.txtoutitem.Click += new System.EventHandler(this.txtoutitem_Click);
            this.txtoutitem.TextChanged += new System.EventHandler(this.txtoutitem_TextChanged);
            this.txtoutitem.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtoutitem_KeyDown);
            // 
            // reqdt
            // 
            this.reqdt.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.reqdt.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.reqdt.Location = new System.Drawing.Point(522, 148);
            this.reqdt.Name = "reqdt";
            this.reqdt.Size = new System.Drawing.Size(122, 27);
            this.reqdt.TabIndex = 3;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.Location = new System.Drawing.Point(29, 1);
            this.label35.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(73, 15);
            this.label35.TabIndex = 418;
            this.label35.Text = "FabricName";
            // 
            // button17
            // 
            this.button17.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button17.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button17.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button17.Location = new System.Drawing.Point(800, 24);
            this.button17.Name = "button17";
            this.button17.Size = new System.Drawing.Size(45, 29);
            this.button17.TabIndex = 420;
            this.button17.Text = "OK";
            this.button17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button17.UseVisualStyleBackColor = false;
            this.button17.Click += new System.EventHandler(this.button17_Click);
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button4.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button4.Location = new System.Drawing.Point(679, 145);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(34, 32);
            this.button4.TabIndex = 434;
            this.button4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button4.UseVisualStyleBackColor = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(553, 313);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(91, 21);
            this.label7.TabIndex = 900;
            this.label7.Text = "Work Order";
            this.label7.Visible = false;
            // 
            // txtitem
            // 
            this.txtitem.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtitem.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtitem.Location = new System.Drawing.Point(557, 339);
            this.txtitem.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtitem.Name = "txtitem";
            this.txtitem.Size = new System.Drawing.Size(111, 27);
            this.txtitem.TabIndex = 4;
            this.txtitem.Visible = false;
            // 
            // txtrectot
            // 
            this.txtrectot.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtrectot.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtrectot.Location = new System.Drawing.Point(557, 148);
            this.txtrectot.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtrectot.Name = "txtrectot";
            this.txtrectot.Size = new System.Drawing.Size(90, 22);
            this.txtrectot.TabIndex = 226;
            // 
            // tabPage3
            // 
            this.tabPage3.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.tabPage3.Controls.Add(this.label16);
            this.tabPage3.Controls.Add(this.componentgrid);
            this.tabPage3.Controls.Add(this.label14);
            this.tabPage3.Controls.Add(this.DataGridSizeCombo);
            this.tabPage3.Controls.Add(this.label15);
            this.tabPage3.Controls.Add(this.DataGridSizeWise);
            this.tabPage3.Controls.Add(this.button11);
            this.tabPage3.Controls.Add(this.label6);
            this.tabPage3.Controls.Add(this.cbofabric);
            this.tabPage3.Controls.Add(this.grSearch);
            this.tabPage3.Controls.Add(this.HFIT);
            this.tabPage3.Controls.Add(this.txtwt);
            this.tabPage3.Controls.Add(this.label11);
            this.tabPage3.Controls.Add(this.txtuomm);
            this.tabPage3.Controls.Add(this.label8);
            this.tabPage3.Controls.Add(this.label19);
            this.tabPage3.Controls.Add(this.txtoutoutqty);
            this.tabPage3.Controls.Add(this.txtdcqty);
            this.tabPage3.Controls.Add(this.txtuom);
            this.tabPage3.Controls.Add(this.label25);
            this.tabPage3.Controls.Add(this.label29);
            this.tabPage3.Controls.Add(this.label5);
            this.tabPage3.Controls.Add(this.txtbillqty);
            this.tabPage3.Location = new System.Drawing.Point(4, 24);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(889, 452);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "ToCutting";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(226, 29);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(69, 15);
            this.label16.TabIndex = 5618;
            this.label16.Text = "Component";
            // 
            // componentgrid
            // 
            this.componentgrid.AllowUserToAddRows = false;
            this.componentgrid.BackgroundColor = System.Drawing.Color.White;
            this.componentgrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.componentgrid.Location = new System.Drawing.Point(226, 44);
            this.componentgrid.Name = "componentgrid";
            this.componentgrid.RowHeadersVisible = false;
            this.componentgrid.Size = new System.Drawing.Size(232, 150);
            this.componentgrid.TabIndex = 5617;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(5, 26);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(45, 15);
            this.label14.TabIndex = 5616;
            this.label14.Text = "Combo";
            // 
            // DataGridSizeCombo
            // 
            this.DataGridSizeCombo.AllowUserToAddRows = false;
            this.DataGridSizeCombo.BackgroundColor = System.Drawing.Color.White;
            this.DataGridSizeCombo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridSizeCombo.Location = new System.Drawing.Point(8, 44);
            this.DataGridSizeCombo.Name = "DataGridSizeCombo";
            this.DataGridSizeCombo.RowHeadersVisible = false;
            this.DataGridSizeCombo.Size = new System.Drawing.Size(212, 150);
            this.DataGridSizeCombo.TabIndex = 5615;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(464, 29);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(28, 15);
            this.label15.TabIndex = 5614;
            this.label15.Text = "Size";
            // 
            // DataGridSizeWise
            // 
            this.DataGridSizeWise.AllowUserToAddRows = false;
            this.DataGridSizeWise.BackgroundColor = System.Drawing.Color.White;
            this.DataGridSizeWise.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridSizeWise.Location = new System.Drawing.Point(464, 44);
            this.DataGridSizeWise.Name = "DataGridSizeWise";
            this.DataGridSizeWise.RowHeadersVisible = false;
            this.DataGridSizeWise.Size = new System.Drawing.Size(327, 150);
            this.DataGridSizeWise.TabIndex = 5613;
            // 
            // button11
            // 
            this.button11.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button11.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button11.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button11.Location = new System.Drawing.Point(799, 90);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(87, 28);
            this.button11.TabIndex = 5567;
            this.button11.Text = "Add Items";
            this.button11.UseVisualStyleBackColor = false;
            this.button11.Click += new System.EventHandler(this.button11_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(4, 3);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(73, 15);
            this.label6.TabIndex = 5607;
            this.label6.Text = "FabricName";
            // 
            // cbofabric
            // 
            this.cbofabric.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbofabric.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbofabric.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbofabric.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbofabric.FormattingEnabled = true;
            this.cbofabric.Location = new System.Drawing.Point(80, 3);
            this.cbofabric.Name = "cbofabric";
            this.cbofabric.Size = new System.Drawing.Size(806, 23);
            this.cbofabric.TabIndex = 5606;
            this.cbofabric.SelectedIndexChanged += new System.EventHandler(this.cbofabric_SelectedIndexChanged);
            // 
            // grSearch
            // 
            this.grSearch.BackColor = System.Drawing.Color.White;
            this.grSearch.Controls.Add(this.button18);
            this.grSearch.Controls.Add(this.btnHide);
            this.grSearch.Controls.Add(this.DataGridCommon);
            this.grSearch.Controls.Add(this.button22);
            this.grSearch.Location = new System.Drawing.Point(29, 281);
            this.grSearch.Name = "grSearch";
            this.grSearch.Size = new System.Drawing.Size(454, 55);
            this.grSearch.TabIndex = 5595;
            this.grSearch.Visible = false;
            // 
            // button18
            // 
            this.button18.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button18.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button18.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button18.Location = new System.Drawing.Point(379, 234);
            this.button18.Name = "button18";
            this.button18.Size = new System.Drawing.Size(70, 28);
            this.button18.TabIndex = 394;
            this.button18.Text = "Select";
            this.button18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button18.UseVisualStyleBackColor = false;
            this.button18.Click += new System.EventHandler(this.button18_Click);
            // 
            // btnHide
            // 
            this.btnHide.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnHide.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHide.Image = ((System.Drawing.Image)(resources.GetObject("btnHide.Image")));
            this.btnHide.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnHide.Location = new System.Drawing.Point(8, 235);
            this.btnHide.Name = "btnHide";
            this.btnHide.Size = new System.Drawing.Size(70, 27);
            this.btnHide.TabIndex = 393;
            this.btnHide.Text = "Close";
            this.btnHide.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnHide.UseVisualStyleBackColor = false;
            this.btnHide.Click += new System.EventHandler(this.btnHide_Click);
            // 
            // DataGridCommon
            // 
            this.DataGridCommon.AllowUserToAddRows = false;
            this.DataGridCommon.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.DataGridCommon.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridCommon.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.DataGridCommon.Location = new System.Drawing.Point(10, 3);
            this.DataGridCommon.Name = "DataGridCommon";
            this.DataGridCommon.ReadOnly = true;
            this.DataGridCommon.RowHeadersVisible = false;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.DataGridCommon.RowsDefaultCellStyle = dataGridViewCellStyle2;
            this.DataGridCommon.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridCommon.Size = new System.Drawing.Size(441, 229);
            this.DataGridCommon.TabIndex = 0;
            this.DataGridCommon.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridCommon_CellContentClick);
            this.DataGridCommon.Click += new System.EventHandler(this.DataGridCommon_Click);
            // 
            // button22
            // 
            this.button22.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button22.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button22.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button22.Location = new System.Drawing.Point(173, 201);
            this.button22.Name = "button22";
            this.button22.Size = new System.Drawing.Size(105, 28);
            this.button22.TabIndex = 395;
            this.button22.Text = "Add Supplier";
            this.button22.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button22.UseVisualStyleBackColor = false;
            // 
            // HFIT
            // 
            this.HFIT.BackgroundColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.HFIT.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.HFIT.Location = new System.Drawing.Point(5, 200);
            this.HFIT.Name = "HFIT";
            this.HFIT.Size = new System.Drawing.Size(881, 251);
            this.HFIT.TabIndex = 5587;
            this.HFIT.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.HFIT_CellClick);
            this.HFIT.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.HFIT_CellContentClick);
            this.HFIT.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.HFIT_CellValueChanged);
            this.HFIT.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.HFIT_EditingControlShowing);
            // 
            // txtwt
            // 
            this.txtwt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtwt.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtwt.Location = new System.Drawing.Point(768, 281);
            this.txtwt.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtwt.Name = "txtwt";
            this.txtwt.Size = new System.Drawing.Size(65, 27);
            this.txtwt.TabIndex = 5611;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(769, 257);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(55, 19);
            this.label11.TabIndex = 5612;
            this.label11.Text = "Weight";
            // 
            // txtuomm
            // 
            this.txtuomm.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtuomm.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.txtuomm.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.txtuomm.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtuomm.FormattingEnabled = true;
            this.txtuomm.Location = new System.Drawing.Point(561, 281);
            this.txtuomm.Name = "txtuomm";
            this.txtuomm.Size = new System.Drawing.Size(71, 27);
            this.txtuomm.TabIndex = 5610;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(568, 259);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(39, 19);
            this.label8.TabIndex = 5609;
            this.label8.Text = "Uom";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(302, 259);
            this.label19.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(83, 19);
            this.label19.TabIndex = 5585;
            this.label19.Text = "Component";
            // 
            // txtoutoutqty
            // 
            this.txtoutoutqty.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtoutoutqty.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtoutoutqty.Location = new System.Drawing.Point(302, 281);
            this.txtoutoutqty.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtoutoutqty.Name = "txtoutoutqty";
            this.txtoutoutqty.Size = new System.Drawing.Size(257, 27);
            this.txtoutoutqty.TabIndex = 5569;
            this.txtoutoutqty.Click += new System.EventHandler(this.txtoutoutqty_Click);
            this.txtoutoutqty.TextChanged += new System.EventHandler(this.txtoutoutqty_TextChanged);
            // 
            // txtdcqty
            // 
            this.txtdcqty.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtdcqty.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtdcqty.Location = new System.Drawing.Point(8, 281);
            this.txtdcqty.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtdcqty.Name = "txtdcqty";
            this.txtdcqty.Size = new System.Drawing.Size(286, 27);
            this.txtdcqty.TabIndex = 5558;
            this.txtdcqty.Click += new System.EventHandler(this.txtdcqty_Click);
            this.txtdcqty.TextChanged += new System.EventHandler(this.txtdcqty_TextChanged);
            this.txtdcqty.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtdcqty_KeyDown);
            // 
            // txtuom
            // 
            this.txtuom.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtuom.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtuom.Location = new System.Drawing.Point(701, 281);
            this.txtuom.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtuom.Name = "txtuom";
            this.txtuom.Size = new System.Drawing.Size(59, 27);
            this.txtuom.TabIndex = 5562;
            this.txtuom.TextChanged += new System.EventHandler(this.txtuom_TextChanged);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(708, 259);
            this.label25.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(32, 19);
            this.label25.TabIndex = 5581;
            this.label25.Text = "Qty";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(13, 259);
            this.label29.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(92, 19);
            this.label29.TabIndex = 5590;
            this.label29.Text = "ComboName";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(640, 259);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(34, 19);
            this.label5.TabIndex = 5594;
            this.label5.Text = "Size";
            // 
            // txtbillqty
            // 
            this.txtbillqty.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtbillqty.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtbillqty.Location = new System.Drawing.Point(639, 281);
            this.txtbillqty.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtbillqty.Name = "txtbillqty";
            this.txtbillqty.Size = new System.Drawing.Size(59, 27);
            this.txtbillqty.TabIndex = 5563;
            this.txtbillqty.Click += new System.EventHandler(this.txtbillqty_Click);
            this.txtbillqty.TextChanged += new System.EventHandler(this.txtbillqty_TextChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(19, 49);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(41, 15);
            this.label9.TabIndex = 900;
            this.label9.Text = "SocNo";
            // 
            // txtqty
            // 
            this.txtqty.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtqty.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtqty.Location = new System.Drawing.Point(18, 69);
            this.txtqty.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtqty.Name = "txtqty";
            this.txtqty.Size = new System.Drawing.Size(108, 23);
            this.txtqty.TabIndex = 1;
            this.txtqty.Click += new System.EventHandler(this.txtqty_Click);
            this.txtqty.TextChanged += new System.EventHandler(this.txtqty_TextChanged);
            this.txtqty.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtqty_KeyDown);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(135, 7);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 15);
            this.label2.TabIndex = 189;
            this.label2.Text = "Doc Date";
            // 
            // Phone
            // 
            this.Phone.AutoSize = true;
            this.Phone.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Phone.Location = new System.Drawing.Point(19, 7);
            this.Phone.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Phone.Name = "Phone";
            this.Phone.Size = new System.Drawing.Size(43, 15);
            this.Phone.TabIndex = 188;
            this.Phone.Text = "DocNo";
            // 
            // txtgrn
            // 
            this.txtgrn.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtgrn.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtgrn.Location = new System.Drawing.Point(19, 25);
            this.txtgrn.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtgrn.Name = "txtgrn";
            this.txtgrn.Size = new System.Drawing.Size(110, 23);
            this.txtgrn.TabIndex = 187;
            // 
            // dtpgrndt
            // 
            this.dtpgrndt.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpgrndt.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpgrndt.Location = new System.Drawing.Point(135, 25);
            this.dtpgrndt.Name = "dtpgrndt";
            this.dtpgrndt.Size = new System.Drawing.Size(122, 23);
            this.dtpgrndt.TabIndex = 0;
            // 
            // txtterms
            // 
            this.txtterms.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtterms.Enabled = false;
            this.txtterms.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtterms.Location = new System.Drawing.Point(135, 69);
            this.txtterms.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtterms.Name = "txtterms";
            this.txtterms.Size = new System.Drawing.Size(441, 23);
            this.txtterms.TabIndex = 992;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(140, 51);
            this.label20.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(68, 15);
            this.label20.TabIndex = 993;
            this.label20.Text = "Style RefNo";
            // 
            // txtbeamid
            // 
            this.txtbeamid.Location = new System.Drawing.Point(564, 265);
            this.txtbeamid.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtbeamid.Name = "txtbeamid";
            this.txtbeamid.Size = new System.Drawing.Size(87, 20);
            this.txtbeamid.TabIndex = 222;
            // 
            // txtoutputid
            // 
            this.txtoutputid.Location = new System.Drawing.Point(268, 287);
            this.txtoutputid.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtoutputid.Name = "txtoutputid";
            this.txtoutputid.Size = new System.Drawing.Size(87, 20);
            this.txtoutputid.TabIndex = 221;
            // 
            // txtitemid
            // 
            this.txtitemid.Location = new System.Drawing.Point(478, 287);
            this.txtitemid.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtitemid.Name = "txtitemid";
            this.txtitemid.Size = new System.Drawing.Size(87, 20);
            this.txtitemid.TabIndex = 220;
            // 
            // txtmillid
            // 
            this.txtmillid.Location = new System.Drawing.Point(417, 220);
            this.txtmillid.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtmillid.Name = "txtmillid";
            this.txtmillid.Size = new System.Drawing.Size(87, 20);
            this.txtmillid.TabIndex = 219;
            // 
            // txtpuid
            // 
            this.txtpuid.Location = new System.Drawing.Point(402, 265);
            this.txtpuid.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtpuid.Name = "txtpuid";
            this.txtpuid.Size = new System.Drawing.Size(87, 20);
            this.txtpuid.TabIndex = 218;
            // 
            // txtgrnid
            // 
            this.txtgrnid.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtgrnid.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtgrnid.Location = new System.Drawing.Point(369, 264);
            this.txtgrnid.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtgrnid.Name = "txtgrnid";
            this.txtgrnid.Size = new System.Drawing.Size(101, 22);
            this.txtgrnid.TabIndex = 230;
            // 
            // txtnobeams
            // 
            this.txtnobeams.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtnobeams.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtnobeams.Location = new System.Drawing.Point(588, 232);
            this.txtnobeams.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtnobeams.Name = "txtnobeams";
            this.txtnobeams.Size = new System.Drawing.Size(90, 22);
            this.txtnobeams.TabIndex = 228;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(491, 232);
            this.label17.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(98, 21);
            this.label17.TabIndex = 227;
            this.label17.Text = "No of Beams";
            // 
            // buttnfinbk
            // 
            this.buttnfinbk.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttnfinbk.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttnfinbk.Image = ((System.Drawing.Image)(resources.GetObject("buttnfinbk.Image")));
            this.buttnfinbk.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttnfinbk.Location = new System.Drawing.Point(852, 580);
            this.buttnfinbk.Margin = new System.Windows.Forms.Padding(4);
            this.buttnfinbk.Name = "buttnfinbk";
            this.buttnfinbk.Size = new System.Drawing.Size(60, 30);
            this.buttnfinbk.TabIndex = 336;
            this.buttnfinbk.Text = "Back";
            this.buttnfinbk.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttnfinbk.UseVisualStyleBackColor = false;
            this.buttnfinbk.Click += new System.EventHandler(this.buttnfinbk_Click);
            // 
            // btnsave
            // 
            this.btnsave.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnsave.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnsave.Image = ((System.Drawing.Image)(resources.GetObject("btnsave.Image")));
            this.btnsave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnsave.Location = new System.Drawing.Point(775, 580);
            this.btnsave.Margin = new System.Windows.Forms.Padding(4);
            this.btnsave.Name = "btnsave";
            this.btnsave.Size = new System.Drawing.Size(69, 30);
            this.btnsave.TabIndex = 337;
            this.btnsave.Text = "Save";
            this.btnsave.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnsave.UseVisualStyleBackColor = false;
            this.btnsave.Click += new System.EventHandler(this.btnsave_Click);
            // 
            // Chkedtact
            // 
            this.Chkedtact.AutoSize = true;
            this.Chkedtact.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Chkedtact.Location = new System.Drawing.Point(709, 587);
            this.Chkedtact.Name = "Chkedtact";
            this.Chkedtact.Size = new System.Drawing.Size(62, 20);
            this.Chkedtact.TabIndex = 338;
            this.Chkedtact.Text = "Active";
            this.Chkedtact.UseVisualStyleBackColor = true;
            // 
            // FrmCuttingEntry
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(923, 610);
            this.Controls.Add(this.panadd);
            this.Controls.Add(this.buttnfinbk);
            this.Controls.Add(this.btnsave);
            this.Controls.Add(this.Chkedtact);
            this.Controls.Add(this.Genpan);
            this.Controls.Add(this.Editpnl);
            this.Name = "FrmCuttingEntry";
            this.Text = "Cutting Entry";
            this.Load += new System.EventHandler(this.FrmCuttingEntry_Load);
            this.Genpan.ResumeLayout(false);
            this.Genpan.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HFGP)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panadd.ResumeLayout(false);
            this.panadd.PerformLayout();
            this.Editpnl.ResumeLayout(false);
            this.Editpnl.PerformLayout();
            this.tabC.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.lkppnl.ResumeLayout(false);
            this.lkppnl.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HFGP2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RQGR)).EndInit();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.componentgrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridSizeCombo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridSizeWise)).EndInit();
            this.grSearch.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DataGridCommon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HFIT)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel Genpan;
        private System.Windows.Forms.TextBox txtscr1;
        private System.Windows.Forms.DataGridView HFGP;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel4;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel5;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel6;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.DateTimePicker dtpfnt;
        private System.Windows.Forms.TextBox txtscr8;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox txtscr7;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox txtscr5;
        private System.Windows.Forms.TextBox Txtscr2;
        private System.Windows.Forms.TextBox txtscr4;
        private System.Windows.Forms.TextBox Txtscr3;
        private System.Windows.Forms.Panel panadd;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button buttnext1;
        private System.Windows.Forms.CheckBox chkact;
        private System.Windows.Forms.Button butcan;
        private System.Windows.Forms.Button butedit;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Panel Editpnl;
        private System.Windows.Forms.TabControl tabC;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TextBox txtoutuom;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.DataGridView RQGR;
        private System.Windows.Forms.TextBox txtoutqty;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.TextBox txtoutrolls;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.TextBox txtoutitem;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Button button17;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtitem;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txtoutoutqty;
        private System.Windows.Forms.DataGridView HFIT;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.TextBox txtdcqty;
        private System.Windows.Forms.TextBox txtuom;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtbillqty;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.DateTimePicker reqdt;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtqty;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label Phone;
        private System.Windows.Forms.TextBox txtgrn;
        private System.Windows.Forms.DateTimePicker dtpgrndt;
        private System.Windows.Forms.TextBox txtterms;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox txtbeamid;
        private System.Windows.Forms.TextBox txtoutputid;
        private System.Windows.Forms.TextBox txtitemid;
        private System.Windows.Forms.TextBox txtmillid;
        private System.Windows.Forms.TextBox txtpuid;
        private System.Windows.Forms.TextBox txtgrnid;
        private System.Windows.Forms.TextBox txtnobeams;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtrectot;
        private System.Windows.Forms.ComboBox cbocuttype;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.ComboBox cbojobtype;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.ComboBox cboworktype;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cbocontratorname;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button buttnfinbk;
        private System.Windows.Forms.Button btnsave;
        private System.Windows.Forms.CheckBox Chkedtact;
        private System.Windows.Forms.Panel lkppnl;
        private System.Windows.Forms.ComboBox cbopono;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.DataGridView HFGP2;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.Panel grSearch;
        private System.Windows.Forms.Button button18;
        private System.Windows.Forms.Button btnHide;
        private System.Windows.Forms.DataGridView DataGridCommon;
        private System.Windows.Forms.Button button22;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtoutsourcety;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cbofabric;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox txtuomm;
        private System.Windows.Forms.TextBox txtwt;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.DataGridView componentgrid;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.DataGridView DataGridSizeCombo;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.DataGridView DataGridSizeWise;
    }
}