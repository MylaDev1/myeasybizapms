﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Drawing.Imaging;
using System.Configuration;
using System.Data.SqlClient;
using System.Diagnostics;
using excel = Microsoft.Office.Interop.Excel;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;

namespace MyEasyBizAPMS
{
    public partial class FrmProOrderEntry : Form
    {
        public FrmProOrderEntry()
        {
            this.BackColor = Color.White; ;
            InitializeComponent();
        }
        int LoadId = 0;
        string uid = "";
        int FillId;
        int mode = 0;
        string tpuid = "";
        int Dtype = 0;
        int i;
        int k;
        int h;
        double sum1;

        SqlConnection conn = new SqlConnection(GeneralParameters.ConnectionString);
        SqlCommand qur = new SqlCommand();
        private DataRow doc1;
        private DataRow doc2;
        BindingSource bs = new BindingSource();
        DataTable Docno = new DataTable();
        DataTable Docno1 = new DataTable();
        BindingSource bsc = new BindingSource();
        BindingSource bsp = new BindingSource();
        BindingSource bsserial = new BindingSource();
        BindingSource IN = new BindingSource();
        BindingSource OUT = new BindingSource();
        BindingSource bsFabric = new BindingSource();
        SQLDBHelper db = new SQLDBHelper();
        int Fillid;
        BindingSource bsParty = new BindingSource();
        string str1key;
        public int SelectId = 0;
        private void Titlep5()
        {
            DataGridViewCheckBoxColumn checkColumn = new DataGridViewCheckBoxColumn();
            checkColumn.Name = "CHK";
            checkColumn.HeaderText = "CHK";
            checkColumn.Width = 50;
            checkColumn.ReadOnly = false;
            checkColumn.FillWeight = 10; //if the datagridview is resized (on form resize) the checkbox won't take up too much; value is relative to the other columns' fill values
     


        }
        private void FrmProOrderEntry_Load(object sender, EventArgs e)
        {
            qur.Connection = conn;

            Genclass.buttonstyleform(this);
            Genclass.buttonstylepanel(panadd);
            panadd.Visible = true;
            matix.Visible = false;
            tabControl1.TabPages.Remove(tabPage1);
            this.HFGP.DefaultCellStyle.Font = new Font("calibri", 10);
            this.HFGP.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 10, FontStyle.Bold);
            this.HFIT.DefaultCellStyle.Font = new Font("calibri", 10);
            this.HFIT.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 10, FontStyle.Bold);
            this.RQGR.DefaultCellStyle.Font = new Font("calibri", 10);
            this.RQGR.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 10, FontStyle.Bold);
            this.HFIT1.DefaultCellStyle.Font = new Font("calibri", 10);
            this.HFIT1.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 10, FontStyle.Bold);
            this.DatGridComponent.DefaultCellStyle.Font = new Font("calibri", 10);
            this.DatGridComponent.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 10, FontStyle.Bold);
            this.dataGridreqdate.DefaultCellStyle.Font = new Font("calibri", 10);
            this.dataGridreqdate.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 10, FontStyle.Bold);
            this.DatGridBitProcess.DefaultCellStyle.Font = new Font("calibri", 10);
            this.DatGridBitProcess.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 10, FontStyle.Bold);
            this.DataGridSizeMatrix.DefaultCellStyle.Font = new Font("calibri", 10);
            this.DataGridSizeMatrix.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 10, FontStyle.Bold);
            DataGridSizeMatrix.RowHeadersVisible = false;
            DatGridComponent.RowHeadersVisible = false;
            DatGridBitProcess.RowHeadersVisible = false;
            dataGridreqdate.RowHeadersVisible = false;
            HFIT1.RowHeadersVisible = false;
            HFGP.RowHeadersVisible = false;
            HFIT.RowHeadersVisible = false;
            RQGR.RowHeadersVisible = false;
            chkgrd.RowHeadersVisible = false;
           Genpan.Visible = true;
            Editpan.Visible = false;
            Genclass.Dtype = 1330;
            Dtype = 1330;
            cbotype.Text = "Yarn";
            chkact.Checked = true;
         
            Titlep();
            Titlep1();
            TitlepStage();


            HFGP.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            HFGP.EnableHeadersVisualStyles = false;
            HFGP.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.GradientActiveCaption;

            HFIT1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            HFIT1.EnableHeadersVisualStyles = false;
            HFIT1.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.GradientActiveCaption;

            dtpgrndt.Format = DateTimePickerFormat.Custom;
            dtpgrndt.CustomFormat = "dd/MM/yyyy";

            HFGP.Focus();

            LoadGetJobCard(1);
            prgrp();
            dabricitems();
            Titlep5();
            subprocess();
            LoadDataGridcomponent();
            LoadDataGridBitProcess();



        }
        private void dabricitems()
        {
            //conn.Open();
            //string qur = "  select distinct  a.uid,itemspec1 from itemsm a inner join itemsmuom b on a.uid=b.itemuid where a.itemgroup_uid=67 and active=1 ";
            //SqlCommand cmd = new SqlCommand(qur, conn);
            //SqlDataAdapter apt = new SqlDataAdapter(cmd);
            //DataTable tab = new DataTable();
            //apt.Fill(tab);
            //cbofabric.DataSource = null;
            //cbofabric.DataSource = tab;
            //cbofabric.DisplayMember = "itemspec1";
            //cbofabric.ValueMember = "uid";
            //cbofabric.SelectedIndex = -1;
            //conn.Close();
        }
        public void prgrp()
        {
            conn.Close();
            conn.Open();
            string qur = "select Generalname,guid uid from generalm where   Typemuid in (30) and userid=" + GeneralParameters.UserdId + " and active=1  order by guid  ";
            SqlCommand cmd = new SqlCommand(qur, conn);
            SqlDataAdapter apt = new SqlDataAdapter(cmd);
            DataTable tab = new DataTable();
            apt.Fill(tab);
            cbosGReturnItem.DataSource = null;
            cbosGReturnItem.DataSource = tab;
            cbosGReturnItem.DisplayMember = "Generalname";
            cbosGReturnItem.ValueMember = "uid";
            cbosGReturnItem.SelectedIndex = -1;
         
        }
        protected DataTable LoadGetJobCard(int tag)
        {

          

            DataTable dt = new DataTable();
            try
            {


                dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "SP_GETGRIDLOADORDERM", conn);
             

                LoadDataTable(dt);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        protected void LoadDataTable(DataTable dt)
        {
            try
            {
                HFGP.DataSource = null;
                HFGP.AutoGenerateColumns = false;
                HFGP.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
                HFGP.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
                HFGP.ColumnCount = 8;
                HFGP.Columns[0].Name = "Uid";
                HFGP.Columns[0].HeaderText = "Uid";
                HFGP.Columns[0].DataPropertyName = "Uid";

                HFGP.Columns[1].Name = "Docno";
                HFGP.Columns[1].HeaderText = "Docno";
                HFGP.Columns[1].DataPropertyName = "Docno";

                HFGP.Columns[2].Name = "Docdate";
                HFGP.Columns[2].HeaderText = "Docdate";
                HFGP.Columns[2].DataPropertyName = "Docdate";

                HFGP.Columns[3].Name = "SOCNO";
                HFGP.Columns[3].HeaderText = "SocNo";
                HFGP.Columns[3].DataPropertyName = "SOCNO";

                HFGP.Columns[4].Name = "STYLE";
                HFGP.Columns[4].HeaderText = "Style";
                HFGP.Columns[4].DataPropertyName = "STYLE";

                HFGP.Columns[5].Name = "REQDATE";
                HFGP.Columns[5].HeaderText = "ReqDate";
                HFGP.Columns[5].DataPropertyName = "REQDATE";


                HFGP.Columns[6].Name = "ORDERMID";
                HFGP.Columns[6].HeaderText = "ORDERMID";
                HFGP.Columns[6].DataPropertyName = "ORDERMID";


                HFGP.Columns[7].Name = "styleid";
                HFGP.Columns[7].HeaderText = "styleid";
                HFGP.Columns[7].DataPropertyName = "styleid";
                bs.DataSource = dt;

                HFGP.DataSource = bs;
           
        
                HFGP.Columns[0].Visible = false;
                HFGP.Columns[1].Width = 90;
                HFGP.Columns[2].Width = 90;
                HFGP.Columns[3].Width = 338;

                HFGP.Columns[4].Width = 374;
                HFGP.Columns[5].Width = 90;
                HFGP.Columns[6].Visible = false;
                HFGP.Columns[7].Visible = false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        

        private void Titlep()
        {
            HFIT.AutoGenerateColumns = false;
            HFIT.Refresh();
            HFIT.DataSource = null;
            HFIT.Rows.Clear();
            HFIT.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            HFIT.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            HFIT.ColumnCount = 8;
            HFIT.Columns[0].Name = "uid";
            HFIT.Columns[1].Name = "SeqNo";
            HFIT.Columns[2].Name = "CMT Process";
            HFIT.Columns[3].Name = "Qty";
            HFIT.Columns[4].Name = "refid";
            HFIT.Columns[5].Name = "coid";
            HFIT.Columns[6].Name = "Stage";
            HFIT.Columns[7].Name = "uom";

            HFIT.Columns[0].Visible = false;
            HFIT.Columns[1].Width = 80; 
            HFIT.Columns[2].Width = 200;
            HFIT.Columns[3].Width = 110;
            HFIT.Columns[4].Visible = false;
            HFIT.Columns[5].Visible = false;
            HFIT.Columns[6].Width = 200;
            HFIT.Columns[7].Visible = false;
        }

        private void TitlepStage()
        {
            dataGridreqdate.AutoGenerateColumns = false;
            dataGridreqdate.Refresh();
            dataGridreqdate.DataSource = null;
            dataGridreqdate.Rows.Clear();
            dataGridreqdate.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            dataGridreqdate.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            dataGridreqdate.ColumnCount = 4;
            dataGridreqdate.Columns[0].Name = "uid";
            dataGridreqdate.Columns[1].Name = "Stages";
            dataGridreqdate.Columns[2].Name = "ReqDate";
            dataGridreqdate.Columns[3].Name = "Headid";


            dataGridreqdate.Columns[0].Visible = false;
            dataGridreqdate.Columns[1].Width = 200;
            dataGridreqdate.Columns[2].Width = 140;
            dataGridreqdate.Columns[3].Visible = false;

        }
        private void Titlep1()
        {
            HFIT1.AutoGenerateColumns = false;
            HFIT1.Refresh();
            HFIT1.DataSource = null;
            HFIT1.Rows.Clear();
            HFIT1.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            HFIT1.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            HFIT1.ColumnCount = 10;
            HFIT1.Columns[0].Name = "Refid";
            HFIT1.Columns[1].Name = "Seq No";
            HFIT1.Columns[2].Name = "Process Name";
            HFIT1.Columns[3].Name = "Stage";
            HFIT1.Columns[4].Name = "SubProcess";
            HFIT1.Columns[5].Name = "ReqDate";
            HFIT1.Columns[6].Name = "processuid";
            HFIT1.Columns[7].Name = "headid";
            HFIT1.Columns[8].Name = "subid";
            HFIT1.Columns[9].Name = "uid";
    
            HFIT1.Columns[0].Visible = false;
            HFIT1.Columns[1].Width = 70;
            HFIT1.Columns[2].Width = 280;
            HFIT1.Columns[3].Width = 150;
            HFIT1.Columns[4].Width = 150;
            HFIT1.Columns[5].Width = 150;
            HFIT1.Columns[6].Visible = false;
            HFIT1.Columns[7].Visible = false;
            HFIT1.Columns[8].Visible = false;
            HFIT1.Columns[9].Visible = false;
       
        }
       
       
        private void btnadd_Click(object sender, EventArgs e)
        {
            if (HFIT.Rows.Count - 1 > 0)
            {
                for (k = 0; k < HFIT.RowCount - 1; k++)

                {
                    if (HFIT.Rows[k].Cells[1].Value.ToString() == txtdcqty.Text)
                    {
                        MessageBox.Show("Seq No Already Exist");
                        return;

                    }

                }

            }




            if (txtaddnotes.Tag != "")
            {
                Genclass.strsql = "select processname,uid from processm1 where uid=" + txtaddnotes.Tag + "";

                Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
                DataTable tap1 = new DataTable();
                aptr1.Fill(tap1);


                if (tap1.Rows.Count > 0)
                {
                    i = i + 1;
                    k = k + 1;
                    var index = HFIT.Rows.Add();



                    HFIT.Rows[index].Cells[0].Value = i;


                    HFIT.Rows[index].Cells[1].Value = txtdcqty.Text;

                    HFIT.Rows[index].Cells[2].Value = tap1.Rows[0]["processname"].ToString();
                    HFIT.Rows[index].Cells[3].Value = txtprocessdet.Text;
                    HFIT.Rows[index].Cells[4].Value = tap1.Rows[0]["uid"].ToString();
                    HFIT.Rows[index].Cells[5].Value = 0;
                    HFIT.Rows[index].Cells[6].Value = cbosGReturnItem.Text;
                    HFIT.Rows[index].Cells[7].Value = cbosGReturnItem.SelectedValue;
                    HFIT.Rows[index].Cells[8].Value = cbotype.Text;
                    var index1 = HFIT1.Rows.Add();

                    HFIT1.Rows[index1].Cells[0].Value = k;


                    HFIT1.Rows[index1].Cells[1].Value = txtdcqty.Text;

                    HFIT1.Rows[index1].Cells[2].Value = tap1.Rows[0]["processname"].ToString();
                    HFIT1.Rows[index1].Cells[3].Value = txtprocessdet.Text;
                    HFIT1.Rows[index1].Cells[4].Value = tap1.Rows[0]["uid"].ToString();
                    HFIT1.Rows[index1].Cells[5].Value = 0;
                    HFIT1.Rows[index1].Cells[6].Value = cbosGReturnItem.Text;
                    HFIT1.Rows[index1].Cells[7].Value = cbosGReturnItem.SelectedValue;
                    HFIT1.Rows[index1].Cells[8].Value = cbotype.Text;

                    doc1 = Docno.NewRow();

                    //Genclass.Barcode = HFIT1.Rows[index1].Cells[2].Value.ToString();
                    //doc1["uid"] = HFIT1.Rows[index1].Cells[8].Value;
                    //doc1["docno"] = Genclass.Barcode;
                    //Docno.Rows.Add(doc1);
                    //cboin.DataSource = Docno;
                    //cboin.DisplayMember = "docno";
                    //cboout.DataSource = Docno;
                    //cboout.DisplayMember = "docno";

                    //doc2 = Docno1.NewRow();
                    //doc2["uid"] = HFIT1.Rows[index1].Cells[8].Value;
                    //doc2["docno"] = HFIT1.Rows[index1].Cells[8].Value;
                    //Docno1.Rows.Add(doc2);
                    //type.DataSource = Docno1;
                    //type.DisplayMember = "docno";
                    //cbotypeout.DataSource = Docno1;
                    //cbotypeout.DisplayMember = "docno";
                    ////type.DisplayMember = "uid";



                    txtqty.Text = "";
                    txtdcqty.Text = "";
                    //txtqty.Text = "";
                    txtprocessdet.Text = "";
                    txtaddnotes.Text = "";
                }
            }

            txtdcqty.Focus();

        }

        private void txtaddnotes_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bsc.Filter = string.Format("processname LIKE '%{0}%' ", txtaddnotes.Text);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtaddnotes_Click(object sender, EventArgs e)
        {
            Genclass.type = 1;
            button22.Visible = false;
            DataTable dt = getParty();
            bsc.DataSource = dt;
            FillGrid1(dt, 1);
            Point loc = FindLocation(txtaddnotes);
            grNewSearch.Location = new Point(loc.X, loc.Y + 20);
            grNewSearch.Visible = true;
        }
        private Point FindLocation(Control ctrl)
        {
            if (ctrl.Parent is Form)
                return ctrl.Location;
            else
            {
                Point p = FindLocation(ctrl.Parent);
                p.X += ctrl.Location.X;
                p.Y += ctrl.Location.Y;
                return p;
            }
        }

        protected void FillGrid1(DataTable dt, int Fillid)
        {
            try
            {
                DataGridCommonNew.DataSource = null;
                DataGridCommonNew.AutoGenerateColumns = false;
                DataGridCommonNew.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
                DataGridCommonNew.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
              
                    DataGridCommonNew.ColumnCount = 4;
                    DataGridCommonNew.Columns[0].Name = "Uid";
                    DataGridCommonNew.Columns[0].HeaderText = "Uid";
                    DataGridCommonNew.Columns[0].DataPropertyName = "Uid";
                    DataGridCommonNew.Columns[1].Name = "OrderNO";
                    DataGridCommonNew.Columns[1].HeaderText = "OrderNO";
                    DataGridCommonNew.Columns[1].DataPropertyName = "OrderNO";
                    DataGridCommonNew.Columns[1].Width = 100;
                DataGridCommonNew.Columns[2].Name = "StyleName";
                DataGridCommonNew.Columns[2].HeaderText = "StyleName";
                DataGridCommonNew.Columns[2].DataPropertyName = "StyleName";
                DataGridCommonNew.Columns[2].Width = 200;
                DataGridCommonNew.Columns[3].Name = "ordermstyleuid";
                DataGridCommonNew.Columns[3].HeaderText = "ordermstyleuid";
                DataGridCommonNew.Columns[3].DataPropertyName = "ordermstyleuid";
                DataGridCommonNew.DataSource = bsc;
                    DataGridCommonNew.Columns[0].Visible = false;
                DataGridCommonNew.Columns[3].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
       
        protected DataTable getParty()
        {
            DataTable dt = new DataTable();
            try
            {


             
                    dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "sp_getorderno", conn);
                    bsc.DataSource = dt;
                



            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            return dt;
        }

        private void button18_Click(object sender, EventArgs e)
        {
            try
            {
                SelectId = 1;
                int Index = DataGridCommon.SelectedCells[0].RowIndex;

                txtaddnotes.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                txtaddnotes.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();



                grSearch.Visible = false;
                SelectId = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void DataGridCommon_DoubleClick(object sender, EventArgs e)
        {
            SelectId = 1;
            int Index = DataGridCommon.SelectedCells[0].RowIndex;

            txtaddnotes.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
            txtaddnotes.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();

            txtprocessdet.Focus();
            loadprocess();
            loadcomponent();
            loadBitprocess();

            grSearch.Visible = false;
            SelectId = 0;
        }

        private void DataGridCommon_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SelectId = 1;
                int Index = DataGridCommon.SelectedCells[0].RowIndex;


                txtaddnotes.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                txtaddnotes.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                txtprocessdet.Focus();
                loadprocess();
                loadcomponent();
                loadBitprocess();
                grSearch.Visible = false;
                SelectId = 0;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            HFIT1.AutoGenerateColumns = false;
            HFIT1.Refresh();
            HFIT1.DataSource = null;
            HFIT1.Rows.Clear();
            HFIT.AutoGenerateColumns = false;
            HFIT.Refresh();
            HFIT.DataSource = null;
            HFIT.Rows.Clear();
            mode = 1;
            Genpan.Visible = false;
            i = 0;
            k = 0;
            tabControl1.TabPages.Remove(tabPage1);
            panadd.Visible = false;
            Genclass.ClearTextBox(this, Editpan);
            Genclass.STR = "";
            Genclass.Gendocno();
            txtgrn.Text = Genclass.ST;
            Editpan.Visible = true;
            Genclass.sum1 = 0;
            Genclass.sum2 = 0;
            txtaddnotes.Text = "";
            txtaddnotes.Text = "";
            txtprocessdet.Text = "";

          
            Titlep();
            Titlep1();
            dtpgrndt.Focus();
        }

        private void buttnext1_Click(object sender, EventArgs e)
        {

            this.Dispose();
            Docno.Dispose();
        }

        private void btnsave_Click(object sender, EventArgs e)
        {
            conn.Close();
            conn.Open();
  

           
            if (mode == 1)
            {

               
                SqlParameter[] para ={
                    new SqlParameter("@UID","0"),
                    new SqlParameter("@DOCNO",txtgrn.Text),
                    new SqlParameter("@DOCDATE",Convert.ToDateTime(dtpgrndt.Text)),
                    new SqlParameter("@SOCNO", textBox2.Text),
                    new SqlParameter("@STYLE", txtnar.Text),
                    new SqlParameter("@REQDATE", Convert.ToDateTime(Reqdt.Text)),
                    new SqlParameter("@ORDERMID", textBox2.Tag),
                    new SqlParameter("@styleid", txtnar.Tag),
                    new SqlParameter("@Returnid",SqlDbType.Int),



                };

                para[8].Direction = ParameterDirection.Output;
                int ReturnId = db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_SAVEUPDATEORDERENTRYM", para, conn, 8);
                Genclass.h = ReturnId;


                int ll = 0;
                for (int i = 0; i < HFIT.RowCount - 1; i++)
                {
                    ll = ll + 10;
                    conn.Close();
                    conn.Open();

                    if (HFIT.Rows[i].Cells[3].Value.ToString() == "")

                    {
                        HFIT.Rows[i].Cells[3].Value = "0";

                    }
                  
                 
                    SqlParameter[] para1 ={

                    new SqlParameter("@HEADID",Genclass.h),
                    new SqlParameter("@SEQNO",ll),
                      new SqlParameter("@COORDINATES", HFIT.Rows[i].Cells[2].Value),
                    new SqlParameter("@REFERID", HFIT.Rows[i].Cells[4].Value),
                    new SqlParameter("@coid", HFIT.Rows[i].Cells[5].Value),
                                  new SqlParameter("@operations", HFIT.Rows[i].Cells[6].Value),
                 new SqlParameter("@Qty", HFIT.Rows[i].Cells[3].Value),
                  new SqlParameter("@uom", HFIT.Rows[i].Cells[7].Value),

                };

                    db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_SAVEUPDATEPROORDERENTRYDPROCESS", para1, conn);
                }

                for (int i = 0; i < dataGridreqdate.RowCount - 1; i++)
                {
                    conn.Close();
                    conn.Open();

                 

                    SqlParameter[] para4 ={
                      
                    
                    new SqlParameter("@stage",dataGridreqdate.Rows[i].Cells[1].Value),
                      new SqlParameter("@reqdt", dataGridreqdate.Rows[i].Cells[2].Value),
                      new SqlParameter("@HEADID",Genclass.h),
                   
                  

                };

                    db.ExecuteNonQuery(CommandType.StoredProcedure, "sp_ProductionOrderEntryReqDate", para4, conn);
                }


                for (int i = 0; i < HFIT1.RowCount - 1; i++)
                {

                    conn.Close();
                    conn.Open();

                    if (HFIT1.Rows[i].Cells[3].Value.ToString()=="")

                    {
                        HFIT1.Rows[i].Cells[3].Value = "0";

                    }
                    if (HFIT1.Rows[i].Cells[6].Value.ToString() == "")

                    {
                        HFIT1.Rows[i].Cells[6].Value = "0";

                    }

                    SqlParameter[] para1 ={
       
                    new SqlParameter("@REFERID",HFIT1.Rows[i].Cells[0].Value),
                    new SqlParameter("@SEQNO",HFIT1.Rows[i].Cells[1].Value),
                      new SqlParameter("@PROCESSNAME", HFIT1.Rows[i].Cells[2].Value),
                    new SqlParameter("@STAGE", HFIT1.Rows[i].Cells[3].Value),
                    new SqlParameter("@SUBPROCESS", HFIT1.Rows[i].Cells[4].Value),
                    new SqlParameter("@REQDATE",Convert.ToDateTime(HFIT1.Rows[i].Cells[5].Value).ToString("yyyy-MM-dd") ),
                    new SqlParameter("@PROCESSUID", HFIT1.Rows[i].Cells[6].Value),
                       new SqlParameter("@HEADID",Genclass.h),
                    new SqlParameter("@SUBID",HFIT1.Rows[i].Cells[8].Value ),

                   
                };

                    db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_SAVEUPDATEPROORDERENTRYD", para1, conn);
                }


                
                
            }
            else

            {

                if (mode == 2)
                {
                    qur.CommandText = "delete from PROORDERENTRYDPROCESS where headid=" + Genclass.h + "";
                    qur.ExecuteNonQuery();
                    qur.CommandText = "delete from PROORDERENTRYD where headid=" + Genclass.h + "";
                    qur.ExecuteNonQuery();

                    qur.CommandText = "delete from ProductionOrderEntryReqDate where headid=" + Genclass.h + "";
                    qur.ExecuteNonQuery();

                }

                SqlParameter[] para ={
                    new SqlParameter("@UID",Genclass.h),
                    new SqlParameter("@DOCNO",txtgrn.Text),
                    new SqlParameter("@DOCDATE",Convert.ToDateTime(dtpgrndt.Text)),
                    new SqlParameter("@SOCNO", textBox2.Text),
                    new SqlParameter("@STYLE", txtnar.Text),
                    new SqlParameter("@REQDATE", Convert.ToDateTime(Reqdt.Text)),
                    new SqlParameter("@ORDERMID", textBox2.Tag),
                    new SqlParameter("@styleid", txtnar.Tag),




                };
                db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_SAVEUPDATEORDERENTRYMUPDATE", para, conn);

                int ll = 0;
                for (int i = 0; i < HFIT.RowCount - 1; i++)
                {
                    ll = ll + 10;
                    conn.Close();
                    conn.Open();

                    if (HFIT.Rows[i].Cells[3].Value.ToString() == "")

                    {
                        HFIT.Rows[i].Cells[3].Value = "0";

                    }
                  

                    SqlParameter[] para1 ={

                    new SqlParameter("@HEADID",Genclass.h),
                    new SqlParameter("@SEQNO", ll),
                      new SqlParameter("@COORDINATES", HFIT.Rows[i].Cells[2].Value),
                    new SqlParameter("@REFERID", HFIT.Rows[i].Cells[4].Value),
                    new SqlParameter("@coid", HFIT.Rows[i].Cells[5].Value),

                    new SqlParameter("@operations", HFIT.Rows[i].Cells[6].Value),
                       new SqlParameter("@Qty", HFIT.Rows[i].Cells[3].Value),
                                   new SqlParameter("@uom", HFIT.Rows[i].Cells[7].Value),
                };

                    db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_SAVEUPDATEPROORDERENTRYDPROCESS", para1, conn);
                }

                for (int i = 0; i < dataGridreqdate.RowCount - 1; i++)
                {
                    conn.Close();
                    conn.Open();



                    SqlParameter[] para4 ={


                    new SqlParameter("@stage",dataGridreqdate.Rows[i].Cells[1].Value),
                      new SqlParameter("@reqdt", dataGridreqdate.Rows[i].Cells[2].Value),
                      new SqlParameter("@HEADID",Genclass.h),



                };

                    db.ExecuteNonQuery(CommandType.StoredProcedure, "sp_ProductionOrderEntryReqDate", para4, conn);
                }


                for (int i = 0; i < HFIT1.RowCount - 1; i++)
                {

                    conn.Close();
                    conn.Open();

                    if (HFIT1.Rows[i].Cells[3].Value.ToString() == "")

                    {
                        HFIT1.Rows[i].Cells[3].Value = "0";

                    }
                    if (HFIT1.Rows[i].Cells[6].Value.ToString() == "")

                    {
                        HFIT1.Rows[i].Cells[6].Value = "0";

                    }

                    SqlParameter[] para1 ={

                    new SqlParameter("@REFERID",HFIT1.Rows[i].Cells[0].Value),
                    new SqlParameter("@SEQNO",HFIT1.Rows[i].Cells[1].Value),
                      new SqlParameter("@PROCESSNAME", HFIT1.Rows[i].Cells[2].Value),
                    new SqlParameter("@STAGE", HFIT1.Rows[i].Cells[3].Value),
                    new SqlParameter("@SUBPROCESS", HFIT1.Rows[i].Cells[4].Value),
                    new SqlParameter("@REQDATE",Convert.ToDateTime(HFIT1.Rows[i].Cells[5].Value).ToString("yyyy-MM-dd") ),
                    new SqlParameter("@PROCESSUID", HFIT1.Rows[i].Cells[6].Value),
                       new SqlParameter("@HEADID",Genclass.h),
                    new SqlParameter("@SUBID",HFIT1.Rows[i].Cells[8].Value ),


                };

                    db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_SAVEUPDATEPROORDERENTRYD", para1, conn);
                }






            }

            if (mode == 1)
            {
                qur.CommandText = "update doctypem set lastno= lastno + 1 where doctypeid=" + Dtype + "  and finyear='19-20'";
                qur.ExecuteNonQuery();


            }
            MessageBox.Show("Records Saved");
            Editpan.Visible = false;
            Genpan.Visible = true;
            panadd.Visible = true;
            LoadGetJobCard(1);

        }

        private void butedit_Click(object sender, EventArgs e)
        {
            HFIT1.AutoGenerateColumns = false;
            HFIT1.Refresh();
            HFIT1.DataSource = null;
            HFIT1.Rows.Clear();
            mode = 2;
            tabControl1.TabPages.Remove(tabPage1);
            int Index = HFGP.SelectedCells[0].RowIndex;
            Genclass.h = Convert.ToInt32(HFGP.Rows[Index].Cells[0].Value.ToString());
            txtgrn.Text = HFGP.Rows[Index].Cells[1].Value.ToString();
            dtpgrndt.Text = HFGP.Rows[Index].Cells[2].Value.ToString();
            txtnar.Text = HFGP.Rows[Index].Cells[4].Value.ToString();
            textBox2.Text = HFGP.Rows[Index].Cells[3].Value.ToString();
            Reqdt.Text = HFGP.Rows[Index].Cells[5].Value.ToString();
            textBox2.Tag = HFGP.Rows[Index].Cells[6].Value.ToString();
            txtnar.Tag = HFGP.Rows[Index].Cells[7].Value.ToString();
            Genpan.Visible = false;
            panadd.Visible = false;
            Editpan.Visible = true;
            
            Genclass.i = 0;
            Genclass.k = 0;
    
            txtaddnotes.Text = "";
            txtprocessdet.Text = "";
            HFIT1.Refresh();
            HFIT1.DataSource = null;
            HFIT1.Rows.Clear();
            HFIT1.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            HFIT1.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            Genclass.strsql = "SP_EDITPROORDERENTRYD  " + Genclass.h + " ";
            Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
            SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
            DataTable tap1 = new DataTable();
            aptr1.Fill(tap1);

            if (tap1.Rows.Count > 0)
            {
                i = 0;
         
                for (k = 0; k < tap1.Rows.Count; k++)
                {
                    var index = HFIT1.Rows.Add();
                    HFIT1.Rows[index].Cells[0].Value = tap1.Rows[k]["REFERID"].ToString();
                    HFIT1.Rows[index].Cells[1].Value = tap1.Rows[k]["SEQNO"].ToString();
                    HFIT1.Rows[index].Cells[2].Value = tap1.Rows[k]["PROCESSNAME"].ToString();
                    HFIT1.Rows[index].Cells[3].Value = tap1.Rows[k]["STAGE"].ToString();
                    HFIT1.Rows[index].Cells[4].Value = tap1.Rows[k]["SUBPROCESS"].ToString();
                    HFIT1.Rows[index].Cells[5].Value = tap1.Rows[k]["REQDATE"].ToString();
                    HFIT1.Rows[index].Cells[6].Value = tap1.Rows[k]["PROCESSUID"].ToString();
                    HFIT1.Rows[index].Cells[7].Value = tap1.Rows[k]["HEADID"].ToString();
                    HFIT1.Rows[index].Cells[8].Value = tap1.Rows[k]["SUBID"].ToString();
                    HFIT1.Rows[index].Cells[9].Value = tap1.Rows[k]["UID"].ToString();
                

                }


            }
            
    
            loadtit();

            string qur2 = "sp_getloadprocesscombo " + textBox2.Tag + " ";
            SqlCommand cmd2 = new SqlCommand(qur2, conn);
            SqlDataAdapter apt2 = new SqlDataAdapter(cmd2);
            DataTable tab2 = new DataTable();
            apt2.Fill(tab2);

            if (tab2.Rows.Count > 0)
            {

                cbprocess.DataSource = null;
                cbprocess.DataSource = tab2;
                cbprocess.DisplayMember = "Coordinates";
                cbprocess.ValueMember = "guid";
                cbprocess.SelectedIndex = -1;
            }

            LoadDataGridBitProcess();
            LoadDataGridcomponent();
            loadcomponent();
            loadBitprocess();
            TitlepStage();
            loadsredt();


        }


        private void loadtit()
        {
            HFIT.Refresh();
            HFIT.DataSource = null;
            HFIT.Rows.Clear();
            HFIT.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            HFIT.DefaultCellStyle.WrapMode = DataGridViewTriState.True;

            Genclass.strsql1 = "SP_EDITPROCESS  " + Genclass.h + "";
            Genclass.cmd = new SqlCommand(Genclass.strsql1, conn);
            SqlDataAdapter aptr2 = new SqlDataAdapter(Genclass.cmd);
            DataTable tap2 = new DataTable();
            aptr2.Fill(tap2);

            if (tap2.Rows.Count > 0)
            {
                sum1 = 0;

                for ( k = 0; k < tap2.Rows.Count; k++)
                {
                   
                    var index = HFIT.Rows.Add();
                    HFIT.Rows[index].Cells[0].Value = tap2.Rows[k]["UID"].ToString();
                    HFIT.Rows[index].Cells[1].Value = tap2.Rows[k]["SEQNO"].ToString();
                    HFIT.Rows[index].Cells[2].Value = tap2.Rows[k]["COORDINATES"].ToString();
                    HFIT.Rows[index].Cells[3].Value = tap2.Rows[k]["qty"].ToString();
                    HFIT.Rows[index].Cells[4].Value = tap2.Rows[k]["REFERID"].ToString();
                    HFIT.Rows[index].Cells[5].Value = tap2.Rows[k]["COID"].ToString();
                    HFIT.Rows[index].Cells[6].Value = tap2.Rows[k]["Operations"].ToString();
                    HFIT.Rows[index].Cells[7].Value = tap2.Rows[k]["uom"].ToString();
                }

            }
        }

        private void loadsredt()
        {
            dataGridreqdate.Refresh();
            dataGridreqdate.DataSource = null;
            dataGridreqdate.Rows.Clear();
            dataGridreqdate.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            dataGridreqdate.DefaultCellStyle.WrapMode = DataGridViewTriState.True;

            Genclass.strsql1 = "sp_ProductionOrderEntryReqDateEdit  " + Genclass.h + "";
            Genclass.cmd = new SqlCommand(Genclass.strsql1, conn);
            SqlDataAdapter aptr2 = new SqlDataAdapter(Genclass.cmd);
            DataTable tap2 = new DataTable();
            aptr2.Fill(tap2);

            if (tap2.Rows.Count > 0)
            {
                sum1 = 0;

                for (k = 0; k < tap2.Rows.Count; k++)
                {

                    var index = dataGridreqdate.Rows.Add();
                    dataGridreqdate.Rows[index].Cells[0].Value = tap2.Rows[k]["UID"].ToString();
                    dataGridreqdate.Rows[index].Cells[1].Value = tap2.Rows[k]["stage"].ToString();
                    dataGridreqdate.Rows[index].Cells[2].Value = tap2.Rows[k]["reqdt"].ToString();
                    dataGridreqdate.Rows[index].Cells[3].Value = tap2.Rows[k]["headid"].ToString();
                 

                }

            }
        }

        private void btnaddrcan_Click(object sender, EventArgs e)
        {
            Editpan.Visible = false;
            Genpan.Visible = true;
            panadd.Visible = true;
            LoadGetJobCard(1);
        }

        private void button9_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dtpgrndt_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                e.Handled = true;
                SendKeys.Send("{TAB}");
            }
        }

        private void txtdcno_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                e.Handled = true;
                SendKeys.Send("{TAB}");
            }
        }

        private void txtnar_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                e.Handled = true;
                SendKeys.Send("{TAB}");
            }
        }

        private void txtdcqty_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                e.Handled = true;
                SendKeys.Send("{TAB}");
            }
        }

        private void txtaddnotes_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                e.Handled = true;
                SendKeys.Send("{TAB}");
            }
        }

        private void txtprocessdet_KeyDown(object sender, KeyEventArgs e)
        {

        }

        private void cbosGReturnItem_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                e.Handled = true;
                SendKeys.Send("{TAB}");
            }
        }

        private void btnadd_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                e.Handled = true;
                SendKeys.Send("{TAB}");
            }
        }

        private void txtprocessdet_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                e.Handled = true;
                SendKeys.Send("{TAB}");
            }
        }

        private void txtaddnotes_KeyDown(object sender, KeyEventArgs e)
        {
           
                try
                {
                    if (e.KeyCode == Keys.F10)
                    {
                        DataGridCommonNew.Visible = false;
                    }
                    else if (e.KeyCode == Keys.F2)
                    {
                    FillId = 1;
                        SelectId = 1;
                        int Index = DataGridCommon.SelectedCells[0].RowIndex;
                        txtaddnotes.Text = DataGridCommonNew.Rows[Index].Cells[1].Value.ToString();
                        txtaddnotes.Tag = DataGridCommonNew.Rows[Index].Cells[0].Value.ToString();
                        txtprocessdet.Focus();
                        grNewSearch.Visible = false;
                        SelectId = 0;
                    }
                    else if (e.KeyCode == Keys.Escape)
                    {
                        DataGridCommonNew.Visible = false;
                    }

                    else if (e.KeyValue == 40)
                    {
                        DataGridCommonNew.Select();
                    }
                    else if (e.KeyCode == Keys.Enter)
                    {
                        txtaddnotes_Click(sender, e);
                    }
                e.Handled = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnHide_Click(object sender, EventArgs e)
        {
            grSearch.Visible = false;
        }

        private void cbotype_SelectedIndexChanged(object sender, EventArgs e)
        {
            conn.Close();
            conn.Open();
            qur.CommandText = "truncate table     woprocess ";
            qur.ExecuteNonQuery();

            if (cbotype.Text=="Yarn" && textBox2.Text!="")
            {
                string qur = "exec sp_getwoitemyarn " + textBox2.Tag + ",'" + textBox2.Text + "'";
                SqlCommand cmd = new SqlCommand(qur, conn);
                SqlDataAdapter apt = new SqlDataAdapter(cmd);
                DataTable tab = new DataTable();
                apt.Fill(tab);
                cboitem.DataSource = null;
                cboitem.DataSource = tab;
                cboitem.DisplayMember = "itemname";
                cboitem.ValueMember = "uid";
                cboitem.SelectedIndex = -1;

            }
            else if (cbotype.Text == "Fabric" && textBox2.Text != "")
            {
                string qur = "exec sp_getwoitemfabric  "+ textBox2.Tag +",'"+ textBox2.Text + "'";
                SqlCommand cmd = new SqlCommand(qur, conn);
                SqlDataAdapter apt = new SqlDataAdapter(cmd);
                DataTable tab = new DataTable();
                apt.Fill(tab);
                cboitem.DataSource = null;
                cboitem.DataSource = tab;
                cboitem.DisplayMember = "itemname";
                cboitem.ValueMember = "uid";
                cboitem.SelectedIndex = -1;

            }
            //loadprocess();

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button5_Click(object sender, EventArgs e)
        {
            //Genclass.strsql = "select itemname,uid from iTEMM where uid=" + TXTIN.Tag + "";

            //Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
            //SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
            //DataTable tap1 = new DataTable();
            //aptr1.Fill(tap1);


            //if (tap1.Rows.Count > 0)
            //{

            //    var index = HFITIN1.Rows.Add();



            //    HFITIN1.Rows[index].Cells[0].Value = TXTIN.Text;


            //    HFITIN1.Rows[index].Cells[1].Value = TXTINQTY.Text;
            //    txtsamqty.Text = TXTINQTY.Text;
            //    HFITIN1.Rows[index].Cells[2].Value = TXTIN.Tag;
            //    HFITIN1.Rows[index].Cells[3].Value = 0; ;
            //    HFITIN1.Rows[index].Cells[4].Value = 0; ;
            //    HFITIN1.Rows[index].Cells[5].Value = 0;
            //    HFITIN1.Rows[index].Cells[6].Value = 0;


            //    TXTIN.Text = "";
            //    TXTINQTY.Text = "0";

            //    conn.Close();
            //    conn.Open();

            //    qur.CommandText = "insert into wotemp values (" + TXTIN.Tag + ")";
            //    qur.ExecuteNonQuery();

            }

        private void button8_Click(object sender, EventArgs e)
        {

        }

        private void TXTOUT_Click(object sender, EventArgs e)
        {

        }

        private void TXTOUT_TextChanged(object sender, EventArgs e)
        {

        }

        private void TXTOUT_KeyDown(object sender, KeyEventArgs e)
        {

        }

        private void btnSelect_Click(object sender, EventArgs e)
        {
            try
            {
                SelectId = 1;
                int Index = DataGridCommonNew.SelectedCells[0].RowIndex;

                textBox2.Text = DataGridCommonNew.Rows[Index].Cells[1].Value.ToString();
                textBox2.Tag = DataGridCommonNew.Rows[Index].Cells[0].Value.ToString();
                txtnar.Text = DataGridCommonNew.Rows[Index].Cells[2].Value.ToString();
                txtnar.Tag = DataGridCommonNew.Rows[Index].Cells[3].Value.ToString();
                txtprocessdet.Focus();
                loadprocess();
                loadcomponent();
                loadBitprocess();
                SelectId = 0;
                grNewSearch.Visible = false;
                SelectId = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }


        private void loadprocess()
        {
            int pp = 0;
            HFIT.AutoGenerateColumns = false;
            HFIT.Refresh();
            HFIT.DataSource = null;
            HFIT.Rows.Clear();
            HFIT.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            HFIT.DefaultCellStyle.WrapMode = DataGridViewTriState.True;

            string qur1 = "sp_getloadprocess " + textBox2.Tag + " ";
            SqlCommand cmd1 = new SqlCommand(qur1, conn);
            SqlDataAdapter apt1 = new SqlDataAdapter(cmd1);
            DataTable tab1 = new DataTable();
            apt1.Fill(tab1);

            if (tab1.Rows.Count > 0)
            {

               

                for (k = 0; k < tab1.Rows.Count; k++)
                {

                    pp = pp + 10;
                    var index1 = HFIT.Rows.Add();
                    HFIT.Rows[index1].Cells[0].Value = "0";
                    HFIT.Rows[index1].Cells[1].Value = pp;
                    HFIT.Rows[index1].Cells[2].Value = tab1.Rows[k]["CMTProcess"].ToString();
                    HFIT.Rows[index1].Cells[3].Value = tab1.Rows[k]["Qty"].ToString(); ;
                    HFIT.Rows[index1].Cells[4].Value = tab1.Rows[k]["ordermuid"].ToString();
                    HFIT.Rows[index1].Cells[5].Value = tab1.Rows[k]["guid"].ToString();
                    HFIT.Rows[index1].Cells[6].Value = tab1.Rows[k]["Operations"].ToString();
                    HFIT.Rows[index1].Cells[7].Value = tab1.Rows[k]["Coordinates"].ToString();
                }
            }

            string qur2 = "sp_getloadprocesscombo " + textBox2.Tag + " ";
            SqlCommand cmd2 = new SqlCommand(qur2, conn);
            SqlDataAdapter apt2 = new SqlDataAdapter(cmd2);
            DataTable tab2 = new DataTable();
            apt2.Fill(tab2);

            if (tab2.Rows.Count > 0)
            {

                cbocomponent.DataSource = null;
                cbocomponent.DataSource = tab2;
                cbocomponent.DisplayMember = "CMTProcess";
                cbocomponent.ValueMember = "guid";
                cbocomponent.SelectedIndex = -1;
            }


            }


        private void loadcomponent()

        {

            int pp = 0;
            DatGridComponent.AutoGenerateColumns = false;
            DatGridComponent.Refresh();
            DatGridComponent.DataSource = null;
            DatGridComponent.Rows.Clear();
            DatGridComponent.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            DatGridComponent.DefaultCellStyle.WrapMode = DataGridViewTriState.True;

            string qur1 = "Proc_OrderMGarmentProcessEditProduction " + textBox2.Tag + " ,'Garment' ";
            SqlCommand cmd1 = new SqlCommand(qur1, conn);
            SqlDataAdapter apt1 = new SqlDataAdapter(cmd1);
            DataTable tab1 = new DataTable();
            apt1.Fill(tab1);

            if (tab1.Rows.Count > 0)
            {



                for (k = 0; k < tab1.Rows.Count; k++)
                {

                    int index1 = DatGridComponent.Rows.Add();
                    DatGridComponent.Rows[index1].Cells[0].Value = tab1.Rows[k]["Component"].ToString();
                    DatGridComponent.Rows[index1].Cells[1].Value = tab1.Rows[k]["process"].ToString();
                    DatGridComponent.Rows[index1].Cells[2].Value = tab1.Rows[k]["Colour"].ToString();
                    DatGridComponent.Rows[index1].Cells[3].Value = tab1.Rows[k]["Qty"].ToString();
                    DatGridComponent.Rows[index1].Cells[4].Value = tab1.Rows[k]["Rate"].ToString();
                    DatGridComponent.Rows[index1].Cells[5].Value = tab1.Rows[k]["ProcessLoss"].ToString();
                    DatGridComponent.Rows[index1].Cells[6].Value = tab1.Rows[k]["ComponentUid"].ToString();
                    DatGridComponent.Rows[index1].Cells[7].Value = tab1.Rows[k]["GarmentProcessUid"].ToString();
                    DatGridComponent.Rows[index1].Cells[8].Value = tab1.Rows[k]["Uid"].ToString();


                }

            }

        }
        private void loadBitprocess()

        {

            int pp = 0;
            DatGridBitProcess.AutoGenerateColumns = false;
            DatGridBitProcess.Refresh();
            DatGridBitProcess.DataSource = null;
            DatGridBitProcess.Rows.Clear();
            DatGridBitProcess.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            DatGridBitProcess.DefaultCellStyle.WrapMode = DataGridViewTriState.True;

            string qur1 = "Proc_OrderMGarmentProcessEditProduction " + textBox2.Tag + " ,'BitProcess'";
            SqlCommand cmd1 = new SqlCommand(qur1, conn);
            SqlDataAdapter apt1 = new SqlDataAdapter(cmd1);
            DataTable tab1 = new DataTable();
            apt1.Fill(tab1);

            if (tab1.Rows.Count > 0)
            {



                for (k = 0; k < tab1.Rows.Count; k++)
                {


                    int Index = DatGridBitProcess.Rows.Add();
                    DatGridBitProcess.Rows[Index].Cells[0].Value = tab1.Rows[k]["BitDesc"].ToString();
                    DatGridBitProcess.Rows[Index].Cells[1].Value = tab1.Rows[k]["process"].ToString();
                    DatGridBitProcess.Rows[Index].Cells[2].Value = tab1.Rows[k]["Colour"].ToString();
                    DatGridBitProcess.Rows[Index].Cells[3].Value = tab1.Rows[k]["Qty"].ToString();
                    DatGridBitProcess.Rows[Index].Cells[4].Value = tab1.Rows[k]["Uom"].ToString();
                    DatGridBitProcess.Rows[Index].Cells[5].Value = tab1.Rows[k]["GarmentProcessUid"].ToString();
                    DatGridBitProcess.Rows[Index].Cells[6].Value = tab1.Rows[k]["UomId"].ToString();
                    DatGridBitProcess.Rows[Index].Cells[7].Value = tab1.Rows[k]["Uid"].ToString();


                }
            }

        }

        protected void LoadDataGridcomponent()
        {
            DatGridComponent.AutoGenerateColumns = false;
            DatGridComponent.Refresh();
            DatGridComponent.DataSource = null;
            DatGridComponent.Rows.Clear();
            DatGridComponent.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            DatGridComponent.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            DatGridComponent.ColumnCount = 9;

            DatGridComponent.Columns[0].Name = "Component";
            DatGridComponent.Columns[0].HeaderText = "Component";
            DatGridComponent.Columns[0].Width = 250;

            DatGridComponent.Columns[1].Name = "ProcessName";
            DatGridComponent.Columns[1].HeaderText = "Process";
            DatGridComponent.Columns[1].Width = 200;

            DatGridComponent.Columns[2].Name = "Colour";
            DatGridComponent.Columns[2].HeaderText = "Colour";

            DatGridComponent.Columns[3].Name = "Qty";
            DatGridComponent.Columns[3].HeaderText = "Qty";

            DatGridComponent.Columns[4].Name = "Rate";
            DatGridComponent.Columns[4].HeaderText = "Rate";
            DatGridComponent.Columns[4].Visible = false;

            DatGridComponent.Columns[5].Name = "ProcessLoss";
            DatGridComponent.Columns[5].HeaderText = "ProcessLoss";
            DatGridComponent.Columns[5].Visible = false;

            DatGridComponent.Columns[6].Name = "ComponentUid";
            DatGridComponent.Columns[6].HeaderText = "ComponentUid";
            DatGridComponent.Columns[6].Visible = false;

            DatGridComponent.Columns[7].Name = "ProcessId";
            DatGridComponent.Columns[7].HeaderText = "ProcessId";
            DatGridComponent.Columns[7].Visible = false;

            DatGridComponent.Columns[8].Name = "Uid";
            DatGridComponent.Columns[8].HeaderText = "Uid";
            DatGridComponent.Columns[8].Visible = false;
        }

        protected void LoadDataGridBitProcess()
        {

            DatGridBitProcess.AutoGenerateColumns = false;
            DatGridBitProcess.Refresh();
            DatGridBitProcess.DataSource = null;
            DatGridBitProcess.Rows.Clear();
            DatGridBitProcess.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            DatGridBitProcess.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            DatGridBitProcess.ColumnCount = 8;

      
            DatGridBitProcess.Columns[0].Name = "Description";
            DatGridBitProcess.Columns[0].HeaderText = "Description";
            DatGridBitProcess.Columns[0].Width = 250;

            DatGridBitProcess.Columns[1].Name = "ProcessName";
            DatGridBitProcess.Columns[1].HeaderText = "Process";
            DatGridBitProcess.Columns[1].Width = 200;

            DatGridBitProcess.Columns[2].Name = "Colour";
            DatGridBitProcess.Columns[2].HeaderText = "Colour";

            DatGridBitProcess.Columns[3].Name = "Qty";
            DatGridBitProcess.Columns[3].HeaderText = "Qty";

            DatGridBitProcess.Columns[4].Name = "Uom";
            DatGridBitProcess.Columns[4].HeaderText = "Uom";
            DatGridBitProcess.Columns[4].Visible = true;

            DatGridBitProcess.Columns[5].Name = "ProcessId";
            DatGridBitProcess.Columns[5].HeaderText = "ProcessId";
            DatGridBitProcess.Columns[5].Visible = false;

            DatGridBitProcess.Columns[6].Name = "UomId";
            DatGridBitProcess.Columns[6].HeaderText = "UomId";
            DatGridBitProcess.Columns[6].Visible = false;

            DatGridBitProcess.Columns[7].Name = "Uid";
            DatGridBitProcess.Columns[7].HeaderText = "Uid";
            DatGridBitProcess.Columns[7].Visible = false;
        }
        public void subprocess()
        {
            conn.Close();
            conn.Open();
            string qur = "select Generalname,guid uid from generalm where   Typemuid in (36)  and active=1  order by guid  ";
            SqlCommand cmd = new SqlCommand(qur, conn);
            SqlDataAdapter apt = new SqlDataAdapter(cmd);
            DataTable tab = new DataTable();
            apt.Fill(tab);
            cbotype.DataSource = null;
            cbotype.DataSource = tab;
            cbotype.DisplayMember = "Generalname";
            cbotype.ValueMember = "uid";
            cbotype.SelectedIndex = -1;

        }
        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bsFabric.Filter = string.Format("Itemname LIKE '%{0}%' ", textBox2.Text);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void textBox2_Click(object sender, EventArgs e)
        {
            Genclass.type = 1;
            button22.Visible = false;
            DataTable dt = getParty();
            bsc.DataSource = dt;
            FillGrid1(dt, 1);
            Point loc = FindLocation(textBox2);
            grNewSearch.Location = new Point(loc.X, loc.Y + 20);
            grNewSearch.Visible = true;
        }

        private void textBox2_KeyDown(object sender, KeyEventArgs e)
        {
           
        }

        private void DataGridCommonNew_Click(object sender, EventArgs e)
        {
            try
            {
                SelectId = 1;
                int Index = DataGridCommonNew.SelectedCells[0].RowIndex;

                textBox2.Text = DataGridCommonNew.Rows[Index].Cells[1].Value.ToString();
                textBox2.Tag = DataGridCommonNew.Rows[Index].Cells[0].Value.ToString();
                txtnar.Text = DataGridCommonNew.Rows[Index].Cells[2].Value.ToString();
                txtnar.Tag = DataGridCommonNew.Rows[Index].Cells[3].Value.ToString();
                txtprocessdet.Focus();
                loadprocess();
                loadcomponent();
                loadBitprocess();
                SelectId = 0;
                grNewSearch.Visible = false;
                SelectId = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }

        private void button8_Click_1(object sender, EventArgs e)
        {
            var index = HFIT1.Rows.Add();



            HFIT1.Rows[index].Cells[0].Value = textBox2.Tag;


            HFIT1.Rows[index].Cells[1].Value = cboitem.Text;
            HFIT1.Rows[index].Cells[2].Value = cbprocess.Text;
            HFIT1.Rows[index].Cells[3].Value = cbostage.Text;
            HFIT1.Rows[index].Cells[4].Value = cbotype.Text;
            HFIT1.Rows[index].Cells[5].Value = req2.Text; 
            HFIT1.Rows[index].Cells[6].Value = cbprocess.SelectedValue;
            HFIT1.Rows[index].Cells[7].Value = 0;
            HFIT1.Rows[index].Cells[8].Value = cbotype.SelectedValue;
            HFIT1.Rows[index].Cells[9].Value = 0;

        }

        private void DataGridCommonNew_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void butexit_Click(object sender, EventArgs e)
        {
            string message = "Are you sure to cancel this Entry ?";
            string caption = "Dilama";
            MessageBoxButtons buttons = MessageBoxButtons.YesNo;
            DialogResult result;

            result = MessageBox.Show(message, caption, buttons);

            if (result == System.Windows.Forms.DialogResult.Yes)
            {

                int i = HFGP.SelectedCells[0].RowIndex;
                uid = HFGP.Rows[i].Cells[0].Value.ToString();

                conn.Close();
                conn.Open();

                qur.CommandText = "delete  from PROORDERENTRYDPROCESS  where headid=" + Genclass.h + " ";
                qur.ExecuteNonQuery();
                qur.CommandText = "delete  from PROORDERENTRYD  where headid=" + Genclass.h + " ";
                qur.ExecuteNonQuery();
                qur.CommandText = "delete  from PROORDERENTRYm  where uid=" + Genclass.h + " ";
                qur.ExecuteNonQuery();

            
                MessageBox.Show("OrderEntry Cancelled");
            }
            LoadGetJobCard(1);
        }

        private void button18_Click_1(object sender, EventArgs e)
        {

        }

        private void Genpan_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button10_Click(object sender, EventArgs e)
        {
            grNewSearch.Visible = false;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            matix.Visible = true;
          
                SqlParameter[] parameters = { new SqlParameter("@OrderStyleUid", txtnar.Tag), new SqlParameter("@OrderMUid", textBox2.Tag) };
                DataTable dt = db.GetDataWithParam(CommandType.StoredProcedure, "SP_GetStyleSizeforCombo", parameters, conn);
                if (dt.Rows.Count > 0)
                {
                    DataGridSizeMatrix.Columns.Clear();
                    DataGridSizeMatrix.AutoGenerateColumns = false;
                    int Column1 = dt.Rows.Count + 5;
                    DataGridSizeMatrix.ColumnCount = Column1;
                    DataGridSizeMatrix.Columns[0].Name = "SlNo";
                    DataGridSizeMatrix.Columns[0].HeaderText = "SlNo";
                    DataGridSizeMatrix.Columns[0].Width = 80;

                    DataGridSizeMatrix.Columns[1].Name = "Combo";
                    DataGridSizeMatrix.Columns[1].HeaderText = "Combo";
                    DataGridSizeMatrix.Columns[1].Width = 200;
                    int j = 1;
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        j += 1;
                        DataGridSizeMatrix.Columns[j].Name = dt.Rows[i]["SizeName"].ToString();
                        DataGridSizeMatrix.Columns[j].HeaderText = dt.Rows[i]["SizeName"].ToString();
                        DataGridSizeMatrix.Columns[j].Width = 50;
                    }
                    DataGridSizeMatrix.Columns[j + 1].Name = "Total";
                    DataGridSizeMatrix.Columns[j + 1].HeaderText = "Total";
                    DataGridSizeMatrix.Columns[j + 1].Width = 100;

                    DataGridSizeMatrix.Columns[j + 2].Name = "ComboUid";
                    DataGridSizeMatrix.Columns[j + 2].HeaderText = "ComboUid";
                    DataGridSizeMatrix.Columns[j + 2].Visible = false;

                    DataGridSizeMatrix.Columns[j + 3].Name = "CottonBox";
                    DataGridSizeMatrix.Columns[j + 3].HeaderText = "CottonBox";
                }
                SqlParameter[] parameters1 = { new SqlParameter("@OrderStyleUid", txtnar.Tag) };
                DataTable dt1 = db.GetDataWithParam(CommandType.StoredProcedure, "SP_StyleSizeCombo", parameters1, conn);
                int Count = DataGridSizeMatrix.Columns.Count;
                DataGridSizeMatrix.Rows.Clear();

               for (int i = 0; i < dt1.Rows.Count; i++)
                {
                    DataGridViewRow row = (DataGridViewRow)DataGridSizeMatrix.Rows[0].Clone();
                    row.Cells[0].Value = i + 1;
                    row.Cells[1].Value = dt1.Rows[i]["Combo"].ToString();
                    row.Cells[Count - 2].Value = dt1.Rows[i]["ComboUid"].ToString();
                    DataGridSizeMatrix.Rows.Add(row);
                }
                LoadId = 1;
                SqlParameter[] parameters2 = { new SqlParameter("@AssortUid", textBox2.Tag), new SqlParameter("@StyleUid", txtnar.Tag) };
                int cnt = dt.Rows.Count + 5;
                DataTable dt2 = db.GetDataWithParam(CommandType.StoredProcedure, "SP_GetSizeMatrixQtyNew", parameters2, conn);
                for (int i = 0; i < dt2.Rows.Count; i++)
                {
                    string Name = dt2.Rows[i]["SizeName"].ToString();
                    string ComUid = dt2.Rows[i]["ComboId"].ToString();
                    for (int j = 0; j < DataGridSizeMatrix.Columns.Count; j++)
                    {
                        string ColName = DataGridSizeMatrix.Columns[j].Name.ToString();
                        if (Name == ColName)
                        {
                            for (int k = 0; k < DataGridSizeMatrix.Rows.Count - 1; k++)
                            {
                                string comboid = DataGridSizeMatrix.Rows[k].Cells["ComboUid"].Value.ToString();
                                if (comboid == ComUid)
                                {
                                    DataTable dataTable1 = dt2.Select("ComboId=" + comboid + "").CopyToDataTable();
                                    DataGridSizeMatrix.Rows[k].Cells[Name].Value = dt2.Rows[i]["Qty"].ToString();
                                    DataGridSizeMatrix.Rows[k].Cells["Total"].Value = dataTable1.Compute("SUM(Qty)", string.Empty).ToString();
                                    DataGridSizeMatrix.Rows[k].Cells["CottonBox"].Value = dt2.Rows[i]["CBoxQty"].ToString();
                                }
                            }
                        }
                    }
                }
                LoadId = 0;
            

        }

        private void button6_Click(object sender, EventArgs e)
        {
            matix.Visible = false;
        }

        private void button7_Click(object sender, EventArgs e)
        {
            var index = dataGridreqdate.Rows.Add();
            dataGridreqdate.Rows[index].Cells[0].Value = "0";
            dataGridreqdate.Rows[index].Cells[1].Value = cbocomponent.Text;
            dataGridreqdate.Rows[index].Cells[2].Value = streqdt.Text;
            dataGridreqdate.Rows[index].Cells[3].Value = "0";

        }

        private void HFIT_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (HFIT.CurrentRow.Cells[0].Value != null && HFIT.CurrentCell.ColumnIndex == 1)
            {
                if (HFIT.CurrentCell.ColumnIndex == 1)

                {
                    DataGridViewCell cell = HFIT.CurrentRow.Cells[1];
                    HFIT.CurrentCell = cell;
                    HFIT.BeginEdit(true);
                }

            }
        }

        private void HFIT_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
