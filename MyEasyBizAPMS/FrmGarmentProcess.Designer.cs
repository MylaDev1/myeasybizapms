﻿namespace MyEasyBizAPMS
{
    partial class FrmGarmentProcess
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Syncfusion.Windows.Forms.Tools.SplitButtonRenderer splitButtonRenderer1 = new Syncfusion.Windows.Forms.Tools.SplitButtonRenderer();
            Syncfusion.Windows.Forms.Tools.SplitButtonRenderer splitButtonRenderer2 = new Syncfusion.Windows.Forms.Tools.SplitButtonRenderer();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.SplitSave = new Syncfusion.Windows.Forms.Tools.SplitButton();
            this.toolstripBack = new Syncfusion.Windows.Forms.Tools.toolstripitem();
            this.SplitAdd = new Syncfusion.Windows.Forms.Tools.SplitButton();
            this.toolstripEdit = new Syncfusion.Windows.Forms.Tools.toolstripitem();
            this.toolstripClose = new Syncfusion.Windows.Forms.Tools.toolstripitem();
            this.Grback = new System.Windows.Forms.GroupBox();
            this.grSearch = new System.Windows.Forms.GroupBox();
            this.btnSelect = new System.Windows.Forms.Button();
            this.DataGridCommon = new System.Windows.Forms.DataGridView();
            this.btnHide = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.dtpOrderDate = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.txtOrderNo = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.CmbStyle = new System.Windows.Forms.ComboBox();
            this.txtDocNo = new System.Windows.Forms.TextBox();
            this.tabControlAdv1 = new Syncfusion.Windows.Forms.Tools.TabControlAdv();
            this.tabPageComponent = new Syncfusion.Windows.Forms.Tools.TabPageAdv();
            this.txtProcessLoss = new System.Windows.Forms.TextBox();
            this.CmbComponent = new System.Windows.Forms.ComboBox();
            this.DataGridGarment = new System.Windows.Forms.DataGridView();
            this.label11 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.txtProcess = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtColour = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtQty = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.BtnOk = new System.Windows.Forms.Button();
            this.txtRate = new System.Windows.Forms.TextBox();
            this.tabPageBit = new Syncfusion.Windows.Forms.Tools.TabPageAdv();
            this.DataGridBitProcess = new System.Windows.Forms.DataGridView();
            this.BtnBitProcessOk = new System.Windows.Forms.Button();
            this.CmbUom = new System.Windows.Forms.ComboBox();
            this.txtBitDescription = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtBitProcess = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtBitColour = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtBitQty = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.grFront = new System.Windows.Forms.GroupBox();
            this.SfdDataGridGarments = new Syncfusion.WinForms.DataGrid.SfDataGrid();
            this.tabPageAdv1 = new Syncfusion.Windows.Forms.Tools.TabPageAdv();
            this.gatloss = new System.Windows.Forms.TextBox();
            this.cbogarment = new System.Windows.Forms.ComboBox();
            this.DataGridGar = new System.Windows.Forms.DataGridView();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.GarProcess = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.Garcolour = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.garqty = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.garrate = new System.Windows.Forms.TextBox();
            this.Grback.SuspendLayout();
            this.grSearch.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridCommon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabControlAdv1)).BeginInit();
            this.tabControlAdv1.SuspendLayout();
            this.tabPageComponent.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridGarment)).BeginInit();
            this.tabPageBit.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridBitProcess)).BeginInit();
            this.grFront.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SfdDataGridGarments)).BeginInit();
            this.tabPageAdv1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridGar)).BeginInit();
            this.SuspendLayout();
            // 
            // SplitSave
            // 
            this.SplitSave.BackColor = System.Drawing.SystemColors.Control;
            this.SplitSave.BeforeTouchSize = new System.Drawing.Size(75, 32);
            this.SplitSave.DropDownItems.Add(this.toolstripBack);
            this.SplitSave.DropDownPosition = Syncfusion.Windows.Forms.Tools.Position.Bottom;
            this.SplitSave.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SplitSave.ForeColor = System.Drawing.Color.Black;
            this.SplitSave.Location = new System.Drawing.Point(801, 509);
            this.SplitSave.MinimumSize = new System.Drawing.Size(75, 23);
            this.SplitSave.Name = "SplitSave";
            splitButtonRenderer1.SplitButton = this.SplitSave;
            this.SplitSave.Renderer = splitButtonRenderer1;
            this.SplitSave.ShowDropDownOnButtonClick = false;
            this.SplitSave.Size = new System.Drawing.Size(75, 32);
            this.SplitSave.TabIndex = 427;
            this.SplitSave.Text = "Save";
            this.SplitSave.DropDowItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.SplitSave_DropDowItemClicked);
            this.SplitSave.Click += new System.EventHandler(this.SplitSave_Click);
            // 
            // toolstripBack
            // 
            this.toolstripBack.Name = "toolstripBack";
            this.toolstripBack.Size = new System.Drawing.Size(23, 23);
            this.toolstripBack.Text = "Back";
            // 
            // SplitAdd
            // 
            this.SplitAdd.BackColor = System.Drawing.SystemColors.Control;
            this.SplitAdd.BeforeTouchSize = new System.Drawing.Size(92, 36);
            this.SplitAdd.DropDownItems.Add(this.toolstripEdit);
            this.SplitAdd.DropDownItems.Add(this.toolstripClose);
            this.SplitAdd.DropDownPosition = Syncfusion.Windows.Forms.Tools.Position.Bottom;
            this.SplitAdd.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SplitAdd.ForeColor = System.Drawing.Color.Black;
            this.SplitAdd.Location = new System.Drawing.Point(785, 505);
            this.SplitAdd.MinimumSize = new System.Drawing.Size(75, 23);
            this.SplitAdd.Name = "SplitAdd";
            splitButtonRenderer2.SplitButton = this.SplitAdd;
            this.SplitAdd.Renderer = splitButtonRenderer2;
            this.SplitAdd.ShowDropDownOnButtonClick = false;
            this.SplitAdd.Size = new System.Drawing.Size(92, 36);
            this.SplitAdd.TabIndex = 1;
            this.SplitAdd.Text = "Add";
            this.SplitAdd.DropDowItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.SplitAdd_DropDowItemClicked);
            this.SplitAdd.Click += new System.EventHandler(this.SplitAdd_Click);
            // 
            // toolstripEdit
            // 
            this.toolstripEdit.Name = "toolstripEdit";
            this.toolstripEdit.Size = new System.Drawing.Size(23, 23);
            this.toolstripEdit.Text = "Edit";
            // 
            // toolstripClose
            // 
            this.toolstripClose.Name = "toolstripClose";
            this.toolstripClose.Size = new System.Drawing.Size(23, 23);
            this.toolstripClose.Text = "Close";
            // 
            // Grback
            // 
            this.Grback.Controls.Add(this.grSearch);
            this.Grback.Controls.Add(this.SplitSave);
            this.Grback.Controls.Add(this.label3);
            this.Grback.Controls.Add(this.label4);
            this.Grback.Controls.Add(this.dtpOrderDate);
            this.Grback.Controls.Add(this.label2);
            this.Grback.Controls.Add(this.txtOrderNo);
            this.Grback.Controls.Add(this.label1);
            this.Grback.Controls.Add(this.CmbStyle);
            this.Grback.Controls.Add(this.txtDocNo);
            this.Grback.Controls.Add(this.tabControlAdv1);
            this.Grback.Location = new System.Drawing.Point(12, 5);
            this.Grback.Name = "Grback";
            this.Grback.Size = new System.Drawing.Size(886, 551);
            this.Grback.TabIndex = 0;
            this.Grback.TabStop = false;
            // 
            // grSearch
            // 
            this.grSearch.Controls.Add(this.btnSelect);
            this.grSearch.Controls.Add(this.DataGridCommon);
            this.grSearch.Controls.Add(this.btnHide);
            this.grSearch.Location = new System.Drawing.Point(271, 22);
            this.grSearch.Name = "grSearch";
            this.grSearch.Size = new System.Drawing.Size(348, 52);
            this.grSearch.TabIndex = 38;
            this.grSearch.TabStop = false;
            this.grSearch.Visible = false;
            // 
            // btnSelect
            // 
            this.btnSelect.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnSelect.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSelect.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnSelect.Location = new System.Drawing.Point(281, 195);
            this.btnSelect.Name = "btnSelect";
            this.btnSelect.Size = new System.Drawing.Size(61, 28);
            this.btnSelect.TabIndex = 396;
            this.btnSelect.Text = "Select";
            this.btnSelect.UseVisualStyleBackColor = false;
            this.btnSelect.Click += new System.EventHandler(this.BtnSelect_Click);
            // 
            // DataGridCommon
            // 
            this.DataGridCommon.AllowUserToAddRows = false;
            this.DataGridCommon.BackgroundColor = System.Drawing.Color.White;
            this.DataGridCommon.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridCommon.Location = new System.Drawing.Point(4, 12);
            this.DataGridCommon.MultiSelect = false;
            this.DataGridCommon.Name = "DataGridCommon";
            this.DataGridCommon.ReadOnly = true;
            this.DataGridCommon.RowHeadersVisible = false;
            this.DataGridCommon.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridCommon.Size = new System.Drawing.Size(336, 183);
            this.DataGridCommon.TabIndex = 0;
            this.DataGridCommon.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.DataGridCommon_CellMouseDoubleClick);
            // 
            // btnHide
            // 
            this.btnHide.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnHide.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHide.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnHide.Location = new System.Drawing.Point(3, 195);
            this.btnHide.Name = "btnHide";
            this.btnHide.Size = new System.Drawing.Size(65, 28);
            this.btnHide.TabIndex = 395;
            this.btnHide.Text = "Close";
            this.btnHide.UseVisualStyleBackColor = false;
            this.btnHide.Click += new System.EventHandler(this.btnHide_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(616, 26);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(33, 15);
            this.label3.TabIndex = 412;
            this.label3.Text = "Style";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(219, 26);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(46, 15);
            this.label4.TabIndex = 410;
            this.label4.Text = "Doc No";
            // 
            // dtpOrderDate
            // 
            this.dtpOrderDate.Enabled = false;
            this.dtpOrderDate.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpOrderDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpOrderDate.Location = new System.Drawing.Point(477, 22);
            this.dtpOrderDate.Name = "dtpOrderDate";
            this.dtpOrderDate.Size = new System.Drawing.Size(121, 23);
            this.dtpOrderDate.TabIndex = 406;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(408, 26);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(67, 15);
            this.label2.TabIndex = 409;
            this.label2.Text = "Order Date";
            // 
            // txtOrderNo
            // 
            this.txtOrderNo.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtOrderNo.Location = new System.Drawing.Point(77, 22);
            this.txtOrderNo.Name = "txtOrderNo";
            this.txtOrderNo.Size = new System.Drawing.Size(136, 23);
            this.txtOrderNo.TabIndex = 405;
            this.txtOrderNo.Click += new System.EventHandler(this.TxtOrderNo_Click);
            this.txtOrderNo.TextChanged += new System.EventHandler(this.TxtOrderNo_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(19, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 15);
            this.label1.TabIndex = 407;
            this.label1.Text = "Order No";
            // 
            // CmbStyle
            // 
            this.CmbStyle.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CmbStyle.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CmbStyle.FormattingEnabled = true;
            this.CmbStyle.Location = new System.Drawing.Point(651, 22);
            this.CmbStyle.Name = "CmbStyle";
            this.CmbStyle.Size = new System.Drawing.Size(205, 23);
            this.CmbStyle.TabIndex = 411;
            // 
            // txtDocNo
            // 
            this.txtDocNo.Enabled = false;
            this.txtDocNo.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDocNo.Location = new System.Drawing.Point(267, 22);
            this.txtDocNo.Name = "txtDocNo";
            this.txtDocNo.Size = new System.Drawing.Size(136, 23);
            this.txtDocNo.TabIndex = 408;
            // 
            // tabControlAdv1
            // 
            this.tabControlAdv1.ActiveTabForeColor = System.Drawing.Color.Empty;
            this.tabControlAdv1.BeforeTouchSize = new System.Drawing.Size(876, 452);
            this.tabControlAdv1.CloseButtonForeColor = System.Drawing.Color.Empty;
            this.tabControlAdv1.CloseButtonHoverForeColor = System.Drawing.Color.Empty;
            this.tabControlAdv1.CloseButtonPressedForeColor = System.Drawing.Color.Empty;
            this.tabControlAdv1.Controls.Add(this.tabPageComponent);
            this.tabControlAdv1.Controls.Add(this.tabPageBit);
            this.tabControlAdv1.Controls.Add(this.tabPageAdv1);
            this.tabControlAdv1.InActiveTabForeColor = System.Drawing.Color.Empty;
            this.tabControlAdv1.Location = new System.Drawing.Point(6, 53);
            this.tabControlAdv1.Name = "tabControlAdv1";
            this.tabControlAdv1.SeparatorColor = System.Drawing.SystemColors.ControlDark;
            this.tabControlAdv1.ShowSeparator = false;
            this.tabControlAdv1.Size = new System.Drawing.Size(876, 452);
            this.tabControlAdv1.TabIndex = 429;
            // 
            // tabPageComponent
            // 
            this.tabPageComponent.Controls.Add(this.txtProcessLoss);
            this.tabPageComponent.Controls.Add(this.CmbComponent);
            this.tabPageComponent.Controls.Add(this.DataGridGarment);
            this.tabPageComponent.Controls.Add(this.label11);
            this.tabPageComponent.Controls.Add(this.label5);
            this.tabPageComponent.Controls.Add(this.label10);
            this.tabPageComponent.Controls.Add(this.txtProcess);
            this.tabPageComponent.Controls.Add(this.label9);
            this.tabPageComponent.Controls.Add(this.txtColour);
            this.tabPageComponent.Controls.Add(this.label8);
            this.tabPageComponent.Controls.Add(this.txtQty);
            this.tabPageComponent.Controls.Add(this.label7);
            this.tabPageComponent.Controls.Add(this.BtnOk);
            this.tabPageComponent.Controls.Add(this.txtRate);
            this.tabPageComponent.Image = null;
            this.tabPageComponent.ImageSize = new System.Drawing.Size(16, 16);
            this.tabPageComponent.Location = new System.Drawing.Point(1, 27);
            this.tabPageComponent.Name = "tabPageComponent";
            this.tabPageComponent.ShowCloseButton = true;
            this.tabPageComponent.Size = new System.Drawing.Size(873, 423);
            this.tabPageComponent.TabIndex = 1;
            this.tabPageComponent.Text = "Component";
            this.tabPageComponent.ThemesEnabled = false;
            // 
            // txtProcessLoss
            // 
            this.txtProcessLoss.Location = new System.Drawing.Point(768, 30);
            this.txtProcessLoss.Name = "txtProcessLoss";
            this.txtProcessLoss.Size = new System.Drawing.Size(81, 23);
            this.txtProcessLoss.TabIndex = 419;
            this.txtProcessLoss.Visible = false;
            // 
            // CmbComponent
            // 
            this.CmbComponent.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CmbComponent.FormattingEnabled = true;
            this.CmbComponent.Location = new System.Drawing.Point(14, 30);
            this.CmbComponent.Name = "CmbComponent";
            this.CmbComponent.Size = new System.Drawing.Size(235, 23);
            this.CmbComponent.TabIndex = 428;
            this.CmbComponent.SelectedIndexChanged += new System.EventHandler(this.CmbComponent_SelectedIndexChanged);
            // 
            // DataGridGarment
            // 
            this.DataGridGarment.AllowUserToAddRows = false;
            this.DataGridGarment.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.DeepSkyBlue;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DataGridGarment.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.DataGridGarment.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.LightSkyBlue;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DataGridGarment.DefaultCellStyle = dataGridViewCellStyle2;
            this.DataGridGarment.EnableHeadersVisualStyles = false;
            this.DataGridGarment.Location = new System.Drawing.Point(14, 54);
            this.DataGridGarment.Name = "DataGridGarment";
            this.DataGridGarment.RowHeadersVisible = false;
            this.DataGridGarment.Size = new System.Drawing.Size(849, 355);
            this.DataGridGarment.TabIndex = 426;
            this.DataGridGarment.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.DataGridGarment_CellMouseDoubleClick);
            this.DataGridGarment.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DataGridGarment_KeyDown);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(768, 10);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(77, 15);
            this.label11.TabIndex = 425;
            this.label11.Text = "Process Loss";
            this.label11.Visible = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(13, 10);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(69, 15);
            this.label5.TabIndex = 414;
            this.label5.Text = "Component";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(685, 10);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(31, 15);
            this.label10.TabIndex = 424;
            this.label10.Text = "Rate";
            this.label10.Visible = false;
            // 
            // txtProcess
            // 
            this.txtProcess.Location = new System.Drawing.Point(251, 30);
            this.txtProcess.Name = "txtProcess";
            this.txtProcess.Size = new System.Drawing.Size(219, 23);
            this.txtProcess.TabIndex = 415;
            this.txtProcess.Click += new System.EventHandler(this.txtProcess_Click);
            this.txtProcess.TextChanged += new System.EventHandler(this.TxtProcess_TextChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(602, 10);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(26, 15);
            this.label9.TabIndex = 423;
            this.label9.Text = "Qty";
            // 
            // txtColour
            // 
            this.txtColour.Location = new System.Drawing.Point(471, 30);
            this.txtColour.Name = "txtColour";
            this.txtColour.Size = new System.Drawing.Size(130, 23);
            this.txtColour.TabIndex = 416;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(471, 10);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(44, 15);
            this.label8.TabIndex = 422;
            this.label8.Text = "Colour";
            // 
            // txtQty
            // 
            this.txtQty.Location = new System.Drawing.Point(602, 30);
            this.txtQty.Name = "txtQty";
            this.txtQty.Size = new System.Drawing.Size(82, 23);
            this.txtQty.TabIndex = 417;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(251, 10);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(50, 15);
            this.label7.TabIndex = 421;
            this.label7.Text = "Process";
            // 
            // BtnOk
            // 
            this.BtnOk.Location = new System.Drawing.Point(685, 29);
            this.BtnOk.Name = "BtnOk";
            this.BtnOk.Size = new System.Drawing.Size(36, 25);
            this.BtnOk.TabIndex = 420;
            this.BtnOk.Text = "Ok";
            this.BtnOk.UseVisualStyleBackColor = true;
            this.BtnOk.Click += new System.EventHandler(this.BtnOk_Click);
            // 
            // txtRate
            // 
            this.txtRate.Location = new System.Drawing.Point(685, 30);
            this.txtRate.Name = "txtRate";
            this.txtRate.Size = new System.Drawing.Size(82, 23);
            this.txtRate.TabIndex = 418;
            this.txtRate.Visible = false;
            // 
            // tabPageBit
            // 
            this.tabPageBit.Controls.Add(this.DataGridBitProcess);
            this.tabPageBit.Controls.Add(this.BtnBitProcessOk);
            this.tabPageBit.Controls.Add(this.CmbUom);
            this.tabPageBit.Controls.Add(this.txtBitDescription);
            this.tabPageBit.Controls.Add(this.label6);
            this.tabPageBit.Controls.Add(this.txtBitProcess);
            this.tabPageBit.Controls.Add(this.label12);
            this.tabPageBit.Controls.Add(this.txtBitColour);
            this.tabPageBit.Controls.Add(this.label13);
            this.tabPageBit.Controls.Add(this.txtBitQty);
            this.tabPageBit.Controls.Add(this.label14);
            this.tabPageBit.Image = null;
            this.tabPageBit.ImageSize = new System.Drawing.Size(16, 16);
            this.tabPageBit.Location = new System.Drawing.Point(1, 27);
            this.tabPageBit.Name = "tabPageBit";
            this.tabPageBit.ShowCloseButton = true;
            this.tabPageBit.Size = new System.Drawing.Size(873, 423);
            this.tabPageBit.TabIndex = 2;
            this.tabPageBit.Text = "Bit Process";
            this.tabPageBit.ThemesEnabled = false;
            // 
            // DataGridBitProcess
            // 
            this.DataGridBitProcess.AllowUserToAddRows = false;
            this.DataGridBitProcess.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.DeepSkyBlue;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DataGridBitProcess.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.DataGridBitProcess.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.LightSkyBlue;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DataGridBitProcess.DefaultCellStyle = dataGridViewCellStyle4;
            this.DataGridBitProcess.EnableHeadersVisualStyles = false;
            this.DataGridBitProcess.Location = new System.Drawing.Point(15, 57);
            this.DataGridBitProcess.Name = "DataGridBitProcess";
            this.DataGridBitProcess.RowHeadersVisible = false;
            this.DataGridBitProcess.Size = new System.Drawing.Size(836, 355);
            this.DataGridBitProcess.TabIndex = 438;
            this.DataGridBitProcess.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.DataGridBitProcess_CellMouseDoubleClick);
            // 
            // BtnBitProcessOk
            // 
            this.BtnBitProcessOk.Location = new System.Drawing.Point(811, 30);
            this.BtnBitProcessOk.Name = "BtnBitProcessOk";
            this.BtnBitProcessOk.Size = new System.Drawing.Size(36, 25);
            this.BtnBitProcessOk.TabIndex = 5;
            this.BtnBitProcessOk.Text = "Ok";
            this.BtnBitProcessOk.UseVisualStyleBackColor = true;
            this.BtnBitProcessOk.Click += new System.EventHandler(this.BtnBitProcessOk_Click);
            // 
            // CmbUom
            // 
            this.CmbUom.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CmbUom.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CmbUom.FormattingEnabled = true;
            this.CmbUom.Location = new System.Drawing.Point(695, 31);
            this.CmbUom.Name = "CmbUom";
            this.CmbUom.Size = new System.Drawing.Size(116, 23);
            this.CmbUom.TabIndex = 4;
            // 
            // txtBitDescription
            // 
            this.txtBitDescription.Location = new System.Drawing.Point(15, 31);
            this.txtBitDescription.Name = "txtBitDescription";
            this.txtBitDescription.Size = new System.Drawing.Size(243, 23);
            this.txtBitDescription.TabIndex = 0;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(12, 11);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(70, 15);
            this.label6.TabIndex = 429;
            this.label6.Text = "Description";
            // 
            // txtBitProcess
            // 
            this.txtBitProcess.Location = new System.Drawing.Point(260, 31);
            this.txtBitProcess.Name = "txtBitProcess";
            this.txtBitProcess.Size = new System.Drawing.Size(219, 23);
            this.txtBitProcess.TabIndex = 1;
            this.txtBitProcess.Click += new System.EventHandler(this.TxtBitProcess_Click);
            this.txtBitProcess.TextChanged += new System.EventHandler(this.TxtBitProcess_TextChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(611, 11);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(26, 15);
            this.label12.TabIndex = 435;
            this.label12.Text = "Qty";
            // 
            // txtBitColour
            // 
            this.txtBitColour.Location = new System.Drawing.Point(480, 31);
            this.txtBitColour.Name = "txtBitColour";
            this.txtBitColour.Size = new System.Drawing.Size(130, 23);
            this.txtBitColour.TabIndex = 2;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(480, 11);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(44, 15);
            this.label13.TabIndex = 434;
            this.label13.Text = "Colour";
            // 
            // txtBitQty
            // 
            this.txtBitQty.Location = new System.Drawing.Point(611, 31);
            this.txtBitQty.Name = "txtBitQty";
            this.txtBitQty.Size = new System.Drawing.Size(82, 23);
            this.txtBitQty.TabIndex = 3;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(260, 11);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(50, 15);
            this.label14.TabIndex = 433;
            this.label14.Text = "Process";
            // 
            // grFront
            // 
            this.grFront.Controls.Add(this.SplitAdd);
            this.grFront.Controls.Add(this.SfdDataGridGarments);
            this.grFront.Location = new System.Drawing.Point(12, 5);
            this.grFront.Name = "grFront";
            this.grFront.Size = new System.Drawing.Size(886, 550);
            this.grFront.TabIndex = 1;
            this.grFront.TabStop = false;
            // 
            // SfdDataGridGarments
            // 
            this.SfdDataGridGarments.AccessibleName = "Table";
            this.SfdDataGridGarments.AllowFiltering = true;
            this.SfdDataGridGarments.Location = new System.Drawing.Point(8, 16);
            this.SfdDataGridGarments.Name = "SfdDataGridGarments";
            this.SfdDataGridGarments.Size = new System.Drawing.Size(868, 482);
            this.SfdDataGridGarments.TabIndex = 0;
            this.SfdDataGridGarments.Text = "sfDataGrid1";
            // 
            // tabPageAdv1
            // 
            this.tabPageAdv1.Controls.Add(this.gatloss);
            this.tabPageAdv1.Controls.Add(this.cbogarment);
            this.tabPageAdv1.Controls.Add(this.DataGridGar);
            this.tabPageAdv1.Controls.Add(this.label15);
            this.tabPageAdv1.Controls.Add(this.label16);
            this.tabPageAdv1.Controls.Add(this.label17);
            this.tabPageAdv1.Controls.Add(this.GarProcess);
            this.tabPageAdv1.Controls.Add(this.label18);
            this.tabPageAdv1.Controls.Add(this.Garcolour);
            this.tabPageAdv1.Controls.Add(this.label19);
            this.tabPageAdv1.Controls.Add(this.garqty);
            this.tabPageAdv1.Controls.Add(this.label20);
            this.tabPageAdv1.Controls.Add(this.button1);
            this.tabPageAdv1.Controls.Add(this.garrate);
            this.tabPageAdv1.Image = null;
            this.tabPageAdv1.ImageSize = new System.Drawing.Size(16, 16);
            this.tabPageAdv1.Location = new System.Drawing.Point(1, 27);
            this.tabPageAdv1.Name = "tabPageAdv1";
            this.tabPageAdv1.ShowCloseButton = true;
            this.tabPageAdv1.Size = new System.Drawing.Size(873, 423);
            this.tabPageAdv1.TabIndex = 3;
            this.tabPageAdv1.Text = "Garment";
            this.tabPageAdv1.ThemesEnabled = false;
            // 
            // gatloss
            // 
            this.gatloss.Location = new System.Drawing.Point(766, 32);
            this.gatloss.Name = "gatloss";
            this.gatloss.Size = new System.Drawing.Size(81, 23);
            this.gatloss.TabIndex = 434;
            this.gatloss.Visible = false;
            // 
            // cbogarment
            // 
            this.cbogarment.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbogarment.FormattingEnabled = true;
            this.cbogarment.Location = new System.Drawing.Point(12, 32);
            this.cbogarment.Name = "cbogarment";
            this.cbogarment.Size = new System.Drawing.Size(235, 23);
            this.cbogarment.TabIndex = 442;
            // 
            // DataGridGar
            // 
            this.DataGridGar.AllowUserToAddRows = false;
            this.DataGridGar.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.DeepSkyBlue;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DataGridGar.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.DataGridGar.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.LightSkyBlue;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DataGridGar.DefaultCellStyle = dataGridViewCellStyle6;
            this.DataGridGar.EnableHeadersVisualStyles = false;
            this.DataGridGar.Location = new System.Drawing.Point(12, 56);
            this.DataGridGar.Name = "DataGridGar";
            this.DataGridGar.RowHeadersVisible = false;
            this.DataGridGar.Size = new System.Drawing.Size(849, 355);
            this.DataGridGar.TabIndex = 441;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(766, 12);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(77, 15);
            this.label15.TabIndex = 440;
            this.label15.Text = "Process Loss";
            this.label15.Visible = false;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(11, 12);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(54, 15);
            this.label16.TabIndex = 429;
            this.label16.Text = "Garment";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(683, 12);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(31, 15);
            this.label17.TabIndex = 439;
            this.label17.Text = "Rate";
            this.label17.Visible = false;
            // 
            // GarProcess
            // 
            this.GarProcess.Location = new System.Drawing.Point(249, 32);
            this.GarProcess.Name = "GarProcess";
            this.GarProcess.Size = new System.Drawing.Size(219, 23);
            this.GarProcess.TabIndex = 430;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(600, 12);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(26, 15);
            this.label18.TabIndex = 438;
            this.label18.Text = "Qty";
            // 
            // Garcolour
            // 
            this.Garcolour.Location = new System.Drawing.Point(469, 32);
            this.Garcolour.Name = "Garcolour";
            this.Garcolour.Size = new System.Drawing.Size(130, 23);
            this.Garcolour.TabIndex = 431;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(469, 12);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(44, 15);
            this.label19.TabIndex = 437;
            this.label19.Text = "Colour";
            // 
            // garqty
            // 
            this.garqty.Location = new System.Drawing.Point(600, 32);
            this.garqty.Name = "garqty";
            this.garqty.Size = new System.Drawing.Size(82, 23);
            this.garqty.TabIndex = 432;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(249, 12);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(50, 15);
            this.label20.TabIndex = 436;
            this.label20.Text = "Process";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(683, 31);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(36, 25);
            this.button1.TabIndex = 435;
            this.button1.Text = "Ok";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // garrate
            // 
            this.garrate.Location = new System.Drawing.Point(683, 32);
            this.garrate.Name = "garrate";
            this.garrate.Size = new System.Drawing.Size(82, 23);
            this.garrate.TabIndex = 433;
            this.garrate.Visible = false;
            // 
            // FrmGarmentProcess
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(901, 558);
            this.Controls.Add(this.grFront);
            this.Controls.Add(this.Grback);
            this.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "FrmGarmentProcess";
            this.Text = "FrmGarmentProcess";
            this.Load += new System.EventHandler(this.FrmGarmentProcess_Load);
            this.Grback.ResumeLayout(false);
            this.Grback.PerformLayout();
            this.grSearch.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DataGridCommon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabControlAdv1)).EndInit();
            this.tabControlAdv1.ResumeLayout(false);
            this.tabPageComponent.ResumeLayout(false);
            this.tabPageComponent.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridGarment)).EndInit();
            this.tabPageBit.ResumeLayout(false);
            this.tabPageBit.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridBitProcess)).EndInit();
            this.grFront.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.SfdDataGridGarments)).EndInit();
            this.tabPageAdv1.ResumeLayout(false);
            this.tabPageAdv1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridGar)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox Grback;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DateTimePicker dtpOrderDate;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtOrderNo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox CmbStyle;
        private System.Windows.Forms.TextBox txtDocNo;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button BtnOk;
        private System.Windows.Forms.TextBox txtProcessLoss;
        private System.Windows.Forms.TextBox txtRate;
        private System.Windows.Forms.TextBox txtQty;
        private System.Windows.Forms.TextBox txtColour;
        private System.Windows.Forms.TextBox txtProcess;
        private Syncfusion.Windows.Forms.Tools.SplitButton SplitSave;
        private Syncfusion.Windows.Forms.Tools.toolstripitem toolstripBack;
        private System.Windows.Forms.GroupBox grSearch;
        private System.Windows.Forms.Button btnSelect;
        private System.Windows.Forms.DataGridView DataGridCommon;
        private System.Windows.Forms.Button btnHide;
        private System.Windows.Forms.GroupBox grFront;
        private Syncfusion.WinForms.DataGrid.SfDataGrid SfdDataGridGarments;
        private Syncfusion.Windows.Forms.Tools.SplitButton SplitAdd;
        private Syncfusion.Windows.Forms.Tools.toolstripitem toolstripEdit;
        private Syncfusion.Windows.Forms.Tools.toolstripitem toolstripClose;
        private System.Windows.Forms.ComboBox CmbComponent;
        private Syncfusion.Windows.Forms.Tools.TabControlAdv tabControlAdv1;
        private Syncfusion.Windows.Forms.Tools.TabPageAdv tabPageComponent;
        private System.Windows.Forms.DataGridView DataGridGarment;
        private Syncfusion.Windows.Forms.Tools.TabPageAdv tabPageBit;
        private System.Windows.Forms.TextBox txtBitDescription;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtBitProcess;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtBitColour;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtBitQty;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ComboBox CmbUom;
        private System.Windows.Forms.Button BtnBitProcessOk;
        private System.Windows.Forms.DataGridView DataGridBitProcess;
        private Syncfusion.Windows.Forms.Tools.TabPageAdv tabPageAdv1;
        private System.Windows.Forms.TextBox gatloss;
        private System.Windows.Forms.ComboBox cbogarment;
        private System.Windows.Forms.DataGridView DataGridGar;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox GarProcess;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox Garcolour;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox garqty;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox garrate;
    }
}