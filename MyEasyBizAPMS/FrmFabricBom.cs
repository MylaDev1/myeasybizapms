﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MyEasyBizAPMS
{
    public partial class FrmFabricBom : Form
    {
        public FrmFabricBom()
        {
            InitializeComponent();
            this.tabControlAdv1.TabStyle = typeof(Syncfusion.Windows.Forms.Tools.OneNoteStyleRenderer);
            this.SfdDataGridFabricBom.Style.HeaderStyle.Font.FontStyle = FontStyle.Bold;
            this.SfdDataGridFabricBom.Style.HeaderStyle.BackColor = Color.DeepSkyBlue;
        }
        DataTable DtFabric = new DataTable();
        SQLDBHelper db = new SQLDBHelper();
        SqlConnection connection = new SqlConnection(GeneralParameters.ConnectionString);
        BindingSource bsDocNo = new BindingSource();
        BindingSource bsComponent = new BindingSource();
        BindingSource bsFabric = new BindingSource();
        BindingSource bsYarn = new BindingSource();
        BindingSource bsProcess = new BindingSource();
        BindingSource bsYarnA = new BindingSource();
        DataTable dtFilter = new DataTable();
        int Fillid = 0;
        int SelectId = 0;
        private void FillGrid(DataTable dt, int FillId)
        {
            try
            {
                SelectId = 1;
                DataGridCommon.DataSource = null;
                DataGridCommon.AutoGenerateColumns = false;
                if (FillId == 1)
                {
                    Fillid = 1;
                    DataGridCommon.ColumnCount = 3;
                    DataGridCommon.Columns[0].Name = "Uid";
                    DataGridCommon.Columns[0].HeaderText = "Uid";
                    DataGridCommon.Columns[0].DataPropertyName = "Uid";
                    DataGridCommon.Columns[0].Visible = false;
                    DataGridCommon.Columns[0].Width = 0;
                    DataGridCommon.Columns[1].Name = "DocNo";
                    DataGridCommon.Columns[1].HeaderText = "DocNo";
                    DataGridCommon.Columns[1].DataPropertyName = "DocNo";
                    DataGridCommon.Columns[1].Width = 100;
                    DataGridCommon.Columns[2].Name = "StyleDesc";
                    DataGridCommon.Columns[2].HeaderText = "StyleDesc";
                    DataGridCommon.Columns[2].DataPropertyName = "StyleDesc";
                    DataGridCommon.Columns[2].Width = 210;
                    DataGridCommon.DataSource = bsDocNo;
                }
                else if (FillId == 2)
                {
                    Fillid = 2;
                    DataGridCommon.ColumnCount = 3;
                    DataGridCommon.Columns[0].Name = "Uid";
                    DataGridCommon.Columns[0].HeaderText = "Uid";
                    DataGridCommon.Columns[0].DataPropertyName = "ComponentId";
                    DataGridCommon.Columns[0].Visible = false;

                    DataGridCommon.Columns[1].Name = "Name";
                    DataGridCommon.Columns[1].HeaderText = "Name";
                    DataGridCommon.Columns[1].DataPropertyName = "Component";
                    DataGridCommon.Columns[1].Width = 320;
                    DataGridCommon.Columns[2].Name = "ShortName";
                    DataGridCommon.Columns[2].HeaderText = "ShortName";
                    DataGridCommon.Columns[2].DataPropertyName = "ComboUid";
                    DataGridCommon.Columns[2].Visible = false;
                    DataGridCommon.DataSource = bsComponent;
                }
                else if (FillId == 3)
                {
                    Fillid = 3;
                    DataGridCommon.ColumnCount = 3;
                    DataGridCommon.Columns[0].Name = "Uid";
                    DataGridCommon.Columns[0].HeaderText = "Uid";
                    DataGridCommon.Columns[0].DataPropertyName = "FabricUid";
                    DataGridCommon.Columns[0].Visible = false;

                    DataGridCommon.Columns[1].Name = "Name";
                    DataGridCommon.Columns[1].HeaderText = "Name";
                    DataGridCommon.Columns[1].DataPropertyName = "FabricName";
                    DataGridCommon.Columns[1].Width = 580;
                    DataGridCommon.Columns[2].Name = "ShortName";
                    DataGridCommon.Columns[2].HeaderText = "ShortName";
                    DataGridCommon.Columns[2].DataPropertyName = "ComboUid";
                    DataGridCommon.Columns[2].Visible = false;
                    DataGridCommon.DataSource = bsFabric;

                }
                else if (FillId == 4)
                {
                    Fillid = 4;
                    DataGridCommon.ColumnCount = 4;
                    DataGridCommon.Columns[0].Name = "YarnId";
                    DataGridCommon.Columns[0].HeaderText = "YarnId";
                    DataGridCommon.Columns[0].DataPropertyName = "YarnId";
                    DataGridCommon.Columns[0].Visible = false;

                    DataGridCommon.Columns[1].Name = "Name";
                    DataGridCommon.Columns[1].HeaderText = "Name";
                    DataGridCommon.Columns[1].DataPropertyName = "YarnName";
                    DataGridCommon.Columns[1].Width = 320;
                    DataGridCommon.Columns[2].Name = "ShortName";
                    DataGridCommon.Columns[2].HeaderText = "ShortName";
                    DataGridCommon.Columns[2].Visible = false;

                    DataGridCommon.Columns[3].Name = "Uid";
                    DataGridCommon.Columns[3].HeaderText = "Uid";
                    DataGridCommon.Columns[3].DataPropertyName = "Uid";
                    DataGridCommon.Columns[3].Visible = false;

                    DataGridCommon.DataSource = bsYarn;
                }
                else if (FillId == 5)
                {
                    Fillid = 5;
                    DataGridCommon.ColumnCount = 3;
                    DataGridCommon.Columns[0].Name = "Uid";
                    DataGridCommon.Columns[0].HeaderText = "Uid";
                    DataGridCommon.Columns[0].DataPropertyName = "Uid";
                    DataGridCommon.Columns[0].Visible = false;

                    DataGridCommon.Columns[1].Name = "Name";
                    DataGridCommon.Columns[1].HeaderText = "Name";
                    DataGridCommon.Columns[1].DataPropertyName = "ProcessName";
                    DataGridCommon.Columns[1].Width = 320;
                    DataGridCommon.Columns[2].Name = "ShortName";
                    DataGridCommon.Columns[2].HeaderText = "ShortName";
                    DataGridCommon.Columns[1].DataPropertyName = "ProcessName";
                    DataGridCommon.Columns[2].Visible = false;
                    DataGridCommon.DataSource = bsProcess;
                }
                else if (FillId == 6)
                {
                    Fillid = 6;
                    DataGridCommon.ColumnCount = 3;
                    DataGridCommon.Columns[0].Name = "Uid";
                    DataGridCommon.Columns[0].HeaderText = "Uid";
                    DataGridCommon.Columns[0].DataPropertyName = "Yarnid";
                    DataGridCommon.Columns[0].Visible = false;

                    DataGridCommon.Columns[1].Name = "Name";
                    DataGridCommon.Columns[1].HeaderText = "Name";
                    DataGridCommon.Columns[1].DataPropertyName = "YarnName";
                    DataGridCommon.Columns[1].Width = 320;
                    DataGridCommon.Columns[2].Name = "ShortName";
                    DataGridCommon.Columns[2].HeaderText = "ShortName";
                    DataGridCommon.Columns[2].Visible = false;
                    DataGridCommon.DataSource = bsYarnA;
                }
                else if (FillId == 7)
                {
                    Fillid = 7;
                    DataGridCommon.ColumnCount = 3;
                    DataGridCommon.Columns[0].Name = "Uid";
                    DataGridCommon.Columns[0].HeaderText = "Uid";
                    DataGridCommon.Columns[0].DataPropertyName = "FabricUid";
                    DataGridCommon.Columns[0].Visible = false;

                    DataGridCommon.Columns[1].Name = "Name";
                    DataGridCommon.Columns[1].HeaderText = "Name";
                    DataGridCommon.Columns[1].DataPropertyName = "FabricName";
                    DataGridCommon.Columns[1].Width = 480;

                    DataGridCommon.Columns[2].Name = "ShortName";
                    DataGridCommon.Columns[2].HeaderText = "ShortName";
                    DataGridCommon.Columns[2].DataPropertyName = "FabricName";
                    DataGridCommon.Columns[2].Visible = false;
                    DataGridCommon.DataSource = bsFabric;
                }
                else if (FillId == 8)
                {
                    Fillid = 8;
                    DataGridCommon.ColumnCount = 3;
                    DataGridCommon.Columns[0].Name = "Uid";
                    DataGridCommon.Columns[0].HeaderText = "Uid";
                    DataGridCommon.Columns[0].DataPropertyName = "Uid";
                    DataGridCommon.Columns[0].Visible = false;

                    DataGridCommon.Columns[1].Name = "ProcessName";
                    DataGridCommon.Columns[1].HeaderText = "Name";
                    DataGridCommon.Columns[1].DataPropertyName = "ProcessName";
                    DataGridCommon.Columns[1].Width = 320;
                    DataGridCommon.Columns[2].Name = "ProcessSName";
                    DataGridCommon.Columns[2].HeaderText = "ShortName";
                    DataGridCommon.Columns[2].DataPropertyName = "ProcessSName";
                    DataGridCommon.Columns[2].Visible = false;
                    DataGridCommon.DataSource = bsProcess;
                }
                else if (FillId == 9)
                {
                    Fillid = 9;
                    DataGridCommon.ColumnCount = 3;
                    DataGridCommon.Columns[0].Name = "Uid";
                    DataGridCommon.Columns[0].HeaderText = "Uid";
                    DataGridCommon.Columns[0].DataPropertyName = "GUid";
                    DataGridCommon.Columns[0].Visible = false;

                    DataGridCommon.Columns[1].Name = "Name";
                    DataGridCommon.Columns[1].HeaderText = "Name";
                    DataGridCommon.Columns[1].DataPropertyName = "GeneralName";
                    DataGridCommon.Columns[1].Width = 320;
                    DataGridCommon.Columns[2].Name = "ShortName";
                    DataGridCommon.Columns[2].HeaderText = "ShortName";
                    DataGridCommon.Columns[2].DataPropertyName = "ShortName";
                    DataGridCommon.Columns[2].Visible = false;
                    DataGridCommon.DataSource = bsComponent;
                }
                if (FillId == 7)
                {
                    DataGridCommon.Width = 490;
                    grSearch.Width = 500;
                }
                else if (FillId == 3)
                {
                    DataGridCommon.Width = 590;
                    grSearch.Width = 600;
                }
                else
                {
                    DataGridCommon.Width = 336;
                    grSearch.Width = 346;
                }
                SelectId = 0;
                grSearch.Visible = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected void LoadCombo(decimal orderId)
        {
            try
            {
                SqlParameter[] parameters = { new SqlParameter("@OrderUomUid", orderId) };
                DataTable dt = db.GetDataWithParam(CommandType.StoredProcedure, "Proc_GetComboForFabricBomClour", parameters, connection);
                //CmbFabricCombo.DataSource = null;
                //CmbFabricCombo.DisplayMember = "Combo";
                //CmbFabricCombo.ValueMember = "Uid";
                //CmbFabricCombo.DataSource = dt;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void BtnSelect_Click(object sender, EventArgs e)
        {
            try
            {
                SelectId = 1;
                int Index = DataGridCommon.SelectedCells[0].RowIndex;
                if (Fillid == 1)
                {
                    txtDocNo.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtDocNo.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    GetOrderMEdit(Convert.ToDecimal(txtDocNo.Tag));
                    CmbStyle_SelectedIndexChanged(sender, e);
                    LoadCombo(Convert.ToDecimal(txtDocNo.Tag));
                    LoadSize();
                    CmbFabricColourType_SelectedIndexChanged(sender, e);
                }
                else if (Fillid == 2)
                {
                    txtComponent.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtComponent.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                }
                else if (Fillid == 3)
                {
                    txtFabric.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtFabric.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    txtDiaApply.Tag = DataGridCommon.Rows[Index].Cells[2].Value.ToString();
                }
                else if (Fillid == 4)
                {
                    txtYarn.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtYarn.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                }
                else if (Fillid == 5)
                {
                    txtProcess.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtProcess.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                }
                else if (Fillid == 6)
                {
                    txtYarnAllec.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtYarnAllec.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                }
                else if (Fillid == 7)
                {
                    txtFabricProcess.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtFabricProcess.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                }
                else if (Fillid == 8)
                {
                    txtFProcess.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtFProcess.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                }
                else if (Fillid == 9)
                {
                    //txtFabricComponentColour.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    //txtFabricComponentColour.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                }
                grSearch.Visible = false;
                SelectId = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void TxtDocNo_Enter(object sender, EventArgs e)
        {
            try
            {
                string Query = "Select Uid,DocNo,DocDate,StyleDesc from OrderM Where Uid Not in (Select OrderMuid from FabricBomM) and DocSts = 'Entry Completed' order by DocDate desc";
                DataTable dt = db.GetDataWithoutParam(CommandType.Text, Query, connection);
                bsDocNo.DataSource = dt;
                FillGrid(dt, 1);
                Point loc = Genclass.FindLocation(txtDocNo);
                grSearch.Location = new Point(loc.X - 10, loc.Y + 20);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected void LadComponentGrid()
        {
            try
            {
                DataGridComponent.Columns.Clear();
                DataGridComponent.AutoGenerateColumns = false;
                DataGridComponent.ColumnCount = 2;

                DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn = new DataGridViewCheckBoxColumn();
                dataGridViewCheckBoxColumn.HeaderText = "Chck";
                dataGridViewCheckBoxColumn.Name = "Chck";
                dataGridViewCheckBoxColumn.Width = 50;
                DataGridComponent.Columns.Insert(0, dataGridViewCheckBoxColumn);

                DataGridComponent.Columns[1].Name = "Component";
                DataGridComponent.Columns[1].HeaderText = "Component";
                DataGridComponent.Columns[1].Width = 200;

                DataGridComponent.Columns[2].Name = "ComponentID";
                DataGridComponent.Columns[2].HeaderText = "ComponentID";
                DataGridComponent.Columns[2].Visible = false;
            }
            catch (Exception)
            {

                throw;
            }
        }

        protected void GetOrderMEdit(decimal OrderMuid)
        {
            try
            {
                SelectId = 1;
                decimal CUid = Convert.ToDecimal(OrderMuid);
                SqlParameter[] parameters = { new SqlParameter("@OrderUid", CUid) };
                DataSet ds = db.GetDataWithParamMultiple(CommandType.StoredProcedure, "SP_GetOrderMEdit", parameters, connection);
                DataTable dt = ds.Tables[0];
                DataTable dtStyle = ds.Tables[1];
                FillTextboxs(dt);
                DataTable dtq = GetorderStyle(CUid);
                DataTable dataTable = GetFabricByUid(CUid);
                CmbStyle.DisplayMember = "StyleName";
                CmbStyle.ValueMember = "Uid";
                CmbStyle.DataSource = dtq;
                dtFilter = dataTable;
                //CmbFabricCombo.DataSource = null;
                //CmbFabricCombo.DisplayMember = "FabricName";
                //CmbFabricCombo.ValueMember = "Uid";
                //CmbFabricCombo.DataSource = dataTable;
                //CmbFabricCombo.SelectedIndex = -1;

                DataTable dtStructure = new DataTable();
                dtStructure.Columns.Add("Structure", typeof(string));
                dtStructure.Columns.Add("StructureUid", typeof(int));

                for (int i = 0; i < dataTable.Rows.Count; i++)
                {
                    DataRow dataRow = dtStructure.NewRow();
                    dataRow[0] = dataTable.Rows[i]["Structure"].ToString();
                    dataRow[1] = dataTable.Rows[i]["StructureUid"].ToString();
                    dtStructure.Rows.Add(dataRow);
                }
                DataView view = new DataView(dtStructure);
                DataTable distinctValues = view.ToTable(true, "Structure", "StructureUid");
                CmbStructureType.DataSource = null;
                CmbStructureType.DisplayMember = "Structure";
                CmbStructureType.ValueMember = "StructureUid";
                CmbStructureType.DataSource = distinctValues;

                SqlParameter[] sqparameters = { new SqlParameter("@TypeMUid", 29), new SqlParameter("@Active", true) };
                DataTable dtC = db.GetDataWithParam(CommandType.StoredProcedure, "SP_GetGeneralM", sqparameters, connection);
                DataGridComponent.Rows.Clear();
                for (int i = 0; i < dtC.Rows.Count; i++)
                {
                    int index = DataGridComponent.Rows.Add();
                    DataGridViewRow dataGridViewRow = (DataGridViewRow)DataGridComponent.Rows[index];
                    dataGridViewRow.Cells[1].Value = dtC.Rows[i]["GeneralName"].ToString();
                    dataGridViewRow.Cells[2].Value = dtC.Rows[i]["GUid"].ToString();
                }
                SelectId = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private DataTable GetFabricByUid(decimal cUid)
        {
            DataTable dataTable = new DataTable();
            try
            {
                SqlParameter[] sqlParameters = { new SqlParameter("@OrderMUid", cUid) };
                dataTable = db.GetDataWithParam(CommandType.StoredProcedure, "Proc_GetFabricbyOrderId", sqlParameters, connection);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            return dataTable;
        }

        private void FillTextboxs(DataTable dt)
        {
            try
            {
                txtDocNo.Text = dt.Rows[0]["DocNo"].ToString();
                txtDocNo.Tag = dt.Rows[0]["Uid"].ToString();
                dtpDocDate.Text = Convert.ToDateTime(dt.Rows[0]["DocDate"].ToString()).ToString();
                //txtOrderNo.Text = dt.Rows[0]["OrderNo"].ToString();
                dtpOrderDate.Text = Convert.ToDateTime(dt.Rows[0]["OrderDt"].ToString()).ToString();
                txtCustomer.Text = dt.Rows[0]["CompanyName"].ToString();
                txtCustomer.Tag = dt.Rows[0]["CustomerUid"].ToString();
                cmbSeason.SelectedValue = dt.Rows[0]["SeasonUid"].ToString();
                txtSeason.Text = cmbSeason.Text;
                dtpDeliveryDate.Text = Convert.ToDateTime(dt.Rows[0]["DeliveryDt"].ToString()).ToString();
                txtMerchindiser.Text = dt.Rows[0]["Merchindiser"].ToString();
                txtMerchindiser.Tag = dt.Rows[0]["MerchindiserUid"].ToString();
                cmbSts.SelectedValue = dt.Rows[0]["StatusUid"].ToString();
                txtOrderSts.Text = cmbSts.Text;
                cmbOrderType.SelectedValue = dt.Rows[0]["TypeUid"].ToString();
                txtOrderType.Text = cmbOrderType.Text;
                txtYear.Text = dt.Rows[0]["SeasonYer"].ToString();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected DataTable GetorderStyle(decimal orderId)
        {
            DataTable dataTable = new DataTable();
            try
            {
                SqlParameter[] parameters = { new SqlParameter("@OrderMUid", orderId) };
                dataTable = db.GetDataWithParam(CommandType.StoredProcedure, "SP_GetOrderMStyles", parameters, connection);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            return dataTable;
        }

        private void TxtDocNo_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bsDocNo.Filter = string.Format("DocNo LIKE '%{0}%'", txtDocNo.Text);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        protected void LoadGeneralsOrderEntry()
        {
            try
            {
                DataTable dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "SP_GetGeneralsForOrderEntry", connection);
                DataTable dtSeason = dt.Select("TypeMUid = " + (int)OrderEntryGeneralM.Season + "").CopyToDataTable();
                DataTable dtOrderStatus = dt.Select("TypeMUid = " + (int)OrderEntryGeneralM.OrderStatus + "").CopyToDataTable();
                DataTable dtOrderType = dt.Select("TypeMUid = " + (int)OrderEntryGeneralM.OrderType + "").CopyToDataTable();
                DataTable dtCombo = dt.Select("TypeMUid = " + (int)OrderEntryGeneralM.Combo + "").CopyToDataTable();
                cmbSeason.DataSource = null;
                cmbSeason.DisplayMember = "GeneralName";
                cmbSeason.ValueMember = "GUid";
                cmbSeason.DataSource = dtSeason;

                cmbSts.DataSource = null;
                cmbSts.DisplayMember = "GeneralName";
                cmbSts.ValueMember = "GUid";
                cmbSts.DataSource = dtOrderStatus;

                cmbOrderType.DataSource = null;
                cmbOrderType.DisplayMember = "GeneralName";
                cmbOrderType.ValueMember = "GUid";
                cmbOrderType.DataSource = dtOrderType;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void FrmFabricBom_Load(object sender, EventArgs e)
        {
            GetFabricBom();
            DataGridCommon.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            LoadGeneralsOrderEntry();
            LoadComponentGrid();
            LadComponentGrid();
            LoadYarComponentGrid();
            LoadDataGridYarnProcess();
            LoadDataGridFabricProcess();
            LoadFabricColourGrid();
            LoadCombo();
            LoadYarnProcessCombo();
            LoadDataGridYarnColourGrid();
            grFront.Visible = true;
            grBack.Visible = false;
            CmbFabricColourType.SelectedIndex = 0;
            LoadSizeforOrder();
            LoadComponentGridSummary();
            DataGridFabricColourSummary.Visible = true;
            DataGridFabric.Visible = false;
        }

        protected void GetFabricBom()
        {
            try
            {
                DataTable dataTable = db.GetDataWithoutParam(CommandType.StoredProcedure, "Proc_GetFabricBomM", connection);
                SfdDataGridFabricBom.DataSource = null;
                SfdDataGridFabricBom.DataSource = dataTable;
                SfdDataGridFabricBom.Columns[0].Width = 100;
                SfdDataGridFabricBom.Columns[1].Width = 100;
                SfdDataGridFabricBom.Columns[2].Width = 500;
                SfdDataGridFabricBom.Columns[6].Width = 280;
                SfdDataGridFabricBom.Columns[4].Width = 100;
                SfdDataGridFabricBom.Columns[3].Visible = false;
                SfdDataGridFabricBom.Columns[5].Visible = false;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void LoadSizeforOrder()
        {
            try
            {
                DataGridSize.AutoGenerateColumns = false;
                DataGridSize.ColumnCount = 3;
                DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn = new DataGridViewCheckBoxColumn();
                dataGridViewCheckBoxColumn.Name = "Chck";
                dataGridViewCheckBoxColumn.HeaderText = "Check";
                dataGridViewCheckBoxColumn.Width = 50;
                DataGridSize.Columns.Insert(0, dataGridViewCheckBoxColumn);
                DataGridSize.Columns[1].Name = "Size";
                DataGridSize.Columns[1].HeaderText = "Size";

                DataGridSize.Columns[2].Name = "SizeUid";
                DataGridSize.Columns[2].HeaderText = "SizeUid";
                DataGridSize.Columns[2].Visible = false;

                DataGridSize.Columns[3].Name = "Dia";
                DataGridSize.Columns[3].HeaderText = "Dia";
                DataGridSize.Columns[3].Visible = true;
                DataGridSize.Columns[3].Width = 80;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information");
                return;
            }
        }

        protected void LoadDataGridYarnColourGrid()
        {
            try
            {
                DataGridYarnColour.AutoGenerateColumns = false;
                DataGridYarnColour.ColumnCount = 7;

                DataGridYarnColour.Columns[0].Name = "Combo";
                DataGridYarnColour.Columns[0].HeaderText = "Combo";
                DataGridYarnColour.Columns[0].Width = 260;

                DataGridYarnColour.Columns[1].Name = "YarnName";
                DataGridYarnColour.Columns[1].HeaderText = "YarnName";
                DataGridYarnColour.Columns[1].Width = 260;

                DataGridYarnColour.Columns[2].Name = "Colour";
                DataGridYarnColour.Columns[2].HeaderText = "Colour";
                DataGridYarnColour.Columns[2].Width = 260;

                DataGridYarnColour.Columns[3].Name = "%";
                DataGridYarnColour.Columns[3].HeaderText = "%";
                DataGridYarnColour.Columns[3].Width = 50;

                DataGridYarnColour.Columns[4].Name = "YarnId";
                DataGridYarnColour.Columns[4].HeaderText = "YarnId";
                DataGridYarnColour.Columns[4].Visible = false;

                DataGridYarnColour.Columns[5].Name = "ComboUid";
                DataGridYarnColour.Columns[5].HeaderText = "ComboUid";
                DataGridYarnColour.Columns[5].Visible = false;

                DataGridYarnColour.Columns[6].Name = "Uid";
                DataGridYarnColour.Columns[6].HeaderText = "Uid";
                DataGridYarnColour.Columns[6].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information");
                return;
            }
        }

        protected void LoadSize()
        {
            try
            {
                string Query = "Select * from StyleSize Where OrdermUid =" + txtDocNo.Tag + "";
                DataTable dataTable = db.GetDataWithoutParam(CommandType.Text, Query, connection);
                DataGridSize.Rows.Clear();
                for (int i = 0; i < dataTable.Rows.Count; i++)
                {
                    int Index = DataGridSize.Rows.Add();
                    DataGridViewRow dataGridViewRow = DataGridSize.Rows[Index];
                    dataGridViewRow.Cells[1].Value = dataTable.Rows[i]["SizeName"].ToString();
                    dataGridViewRow.Cells[2].Value = dataTable.Rows[i]["Uid"].ToString();
                    dataGridViewRow.Cells[3].Value = "";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information");
                return;
            }
        }

        protected void LoadYarnProcessCombo()
        {
            try
            {
                DataGridYarnProcessCombo.DataSource = null;
                DataGridYarnProcessCombo.AutoGenerateColumns = false;
                DataGridYarnProcessCombo.ColumnCount = 2;
                DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn = new DataGridViewCheckBoxColumn
                {
                    HeaderText = "Chck",
                    Name = "Chck",
                    Width = 40
                };
                DataGridYarnProcessCombo.Columns.Insert(0, dataGridViewCheckBoxColumn);

                DataGridYarnProcessCombo.Columns[1].Name = "Combo";
                DataGridYarnProcessCombo.Columns[1].HeaderText = "Combo";
                DataGridYarnProcessCombo.Columns[1].Width = 160;

                DataGridYarnProcessCombo.Columns[2].Name = "ComboID";
                DataGridYarnProcessCombo.Columns[2].HeaderText = "ComboID";
                DataGridYarnProcessCombo.Columns[2].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information");
                return;
            }
        }

        protected void LoadCombo()
        {
            try
            {
                DataGridComboColour.DataSource = null;
                DataGridComboColour.AutoGenerateColumns = false;
                DataGridComboColour.ColumnCount = 2;
                DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn = new DataGridViewCheckBoxColumn
                {
                    HeaderText = "Chck",
                    Name = "Chck",
                    Width = 40
                };
                DataGridComboColour.Columns.Insert(0, dataGridViewCheckBoxColumn);

                DataGridComboColour.Columns[1].Name = "Combo";
                DataGridComboColour.Columns[1].HeaderText = "Combo";
                DataGridComboColour.Columns[1].Width = 160;

                DataGridComboColour.Columns[2].Name = "ComboID";
                DataGridComboColour.Columns[2].HeaderText = "ComboID";
                DataGridComboColour.Columns[2].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information");
                return;
            }
        }

        private void BtnHide_Click(object sender, EventArgs e)
        {
            grSearch.Visible = false;
        }

        protected void LoadFabricColourGrid()
        {
            try
            {
                DatagridFabricColour.AutoGenerateColumns = false;
                DatagridFabricColour.ColumnCount = 13;

                DatagridFabricColour.Columns[0].Name = "SlNo";
                DatagridFabricColour.Columns[0].HeaderText = "SlNo";
                DatagridFabricColour.Columns[0].Width = 50;

                DatagridFabricColour.Columns[1].Name = "Fabric";
                DatagridFabricColour.Columns[1].HeaderText = "Fabric";
                DatagridFabricColour.Columns[1].Width = 470;

                DatagridFabricColour.Columns[2].Name = "Combo";
                DatagridFabricColour.Columns[2].HeaderText = "Combo";
                DatagridFabricColour.Columns[2].Width = 150;

                DatagridFabricColour.Columns[3].Name = "Component";
                DatagridFabricColour.Columns[3].HeaderText = "Component";
                DatagridFabricColour.Columns[3].Width = 100;

                DatagridFabricColour.Columns[4].Name = "Type";
                DatagridFabricColour.Columns[4].HeaderText = "Type";
                DatagridFabricColour.Columns[4].Width = 90;

                DatagridFabricColour.Columns[5].Name = "NoOfColours";
                DatagridFabricColour.Columns[5].HeaderText = "No Of Colours";
                DatagridFabricColour.Columns[5].Width = 80;

                DatagridFabricColour.Columns[6].Name = "FabricID";
                DatagridFabricColour.Columns[6].HeaderText = "FabricID";
                DatagridFabricColour.Columns[6].Visible = false;

                DatagridFabricColour.Columns[7].Name = "ComboID";
                DatagridFabricColour.Columns[7].HeaderText = "ComboID";
                DatagridFabricColour.Columns[7].Visible = false;

                DatagridFabricColour.Columns[8].Name = "ComponentID";
                DatagridFabricColour.Columns[8].HeaderText = "ComponentID";
                DatagridFabricColour.Columns[8].Visible = false;

                DatagridFabricColour.Columns[9].Name = "Colour";
                DatagridFabricColour.Columns[9].HeaderText = "Colour";
                DatagridFabricColour.Columns[9].Visible = true;

                DatagridFabricColour.Columns[10].Name = "StyleUid";
                DatagridFabricColour.Columns[10].HeaderText = "StyleUid";
                DatagridFabricColour.Columns[10].Visible = false;

                DatagridFabricColour.Columns[11].Name = "Final Process";
                DatagridFabricColour.Columns[11].HeaderText = "Final Process";
                DatagridFabricColour.Columns[11].Visible = false;

                DatagridFabricColour.Columns[12].Name = "RollDesign";
                DatagridFabricColour.Columns[12].HeaderText = "RollDesign";
                DatagridFabricColour.Columns[12].Visible = false;

                //DatagridFabricColour.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                //DatagridFabricColour.Columns[1].DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected void LoadColourCompotion(string text)
        {
            try
            {

                if (text == "SOLID" || text == "MELANGE")
                {
                    label35.Visible = false;
                    txtFabricColourComposition.Visible = false;
                    label34.Visible = false;
                    button1.Visible = false;
                    txtComboClr.Visible = false;
                    DatGridColourCompostion.Visible = false;
                    DataGridComponent.Columns.Clear();
                    DataGridComponent.AutoGenerateColumns = false;
                    DataGridComponent.ColumnCount = 4;

                    DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn = new DataGridViewCheckBoxColumn();
                    dataGridViewCheckBoxColumn.HeaderText = "Chck";
                    dataGridViewCheckBoxColumn.Name = "Chck";
                    dataGridViewCheckBoxColumn.Width = 50;
                    DataGridComponent.Columns.Insert(0, dataGridViewCheckBoxColumn);

                    DataGridComponent.Columns[1].Name = "Component";
                    DataGridComponent.Columns[1].HeaderText = "Component";
                    DataGridComponent.Columns[1].Width = 150;

                    DataGridComponent.Columns[2].Name = "ComponentID";
                    DataGridComponent.Columns[2].HeaderText = "ComponentID";
                    DataGridComponent.Columns[2].Visible = false;

                    DataGridComponent.Columns[3].Name = "Colour";
                    DataGridComponent.Columns[3].HeaderText = "Colour";
                    DataGridComponent.Columns[3].Width = 150;
                    DataGridComponent.Columns[3].Visible = true;

                    DataGridComponent.Columns[4].Name = "Role Design";
                    DataGridComponent.Columns[4].HeaderText = "Role Design";
                    DataGridComponent.Columns[4].Width = 200;
                    DataGridComponent.Columns[4].Visible = false;

                    SqlParameter[] sqparameters = { new SqlParameter("@TypeMUid", 29), new SqlParameter("@Active", true) };
                    DataTable dtC = db.GetDataWithParam(CommandType.StoredProcedure, "SP_GetGeneralM", sqparameters, connection);
                    DataGridComponent.Rows.Clear();
                    for (int i = 0; i < dtC.Rows.Count; i++)
                    {
                        int index = DataGridComponent.Rows.Add();
                        DataGridViewRow dataGridViewRow = (DataGridViewRow)DataGridComponent.Rows[index];
                        dataGridViewRow.Cells[1].Value = dtC.Rows[i]["GeneralName"].ToString();
                        dataGridViewRow.Cells[2].Value = dtC.Rows[i]["GUid"].ToString();
                        dataGridViewRow.Cells[3].Value = "";
                        dataGridViewRow.Cells[4].Value = "";
                    }
                }
                else
                {
                    label35.Visible = false;
                    txtFabricColourComposition.Visible = false;
                    label34.Visible = false;
                    button1.Visible = false;
                    txtComboClr.Visible = false;
                    DatGridColourCompostion.Visible = true;
                    label35.Visible = false;
                    txtFabricColourComposition.Visible = false;

                    DatGridColourCompostion.Visible = false;
                    DataGridComponent.Columns.Clear();
                    DataGridComponent.AutoGenerateColumns = false;
                    DataGridComponent.ColumnCount = 4;

                    DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn = new DataGridViewCheckBoxColumn();
                    dataGridViewCheckBoxColumn.HeaderText = "Chck";
                    dataGridViewCheckBoxColumn.Name = "Chck";
                    dataGridViewCheckBoxColumn.Width = 50;
                    DataGridComponent.Columns.Insert(0, dataGridViewCheckBoxColumn);

                    DataGridComponent.Columns[1].Name = "Component";
                    DataGridComponent.Columns[1].HeaderText = "Component";
                    DataGridComponent.Columns[1].Width = 150;

                    DataGridComponent.Columns[2].Name = "ComponentID";
                    DataGridComponent.Columns[2].HeaderText = "ComponentID";
                    DataGridComponent.Columns[2].Visible = false;

                    DataGridComponent.Columns[3].Name = "Colour";
                    DataGridComponent.Columns[3].HeaderText = "Colour";
                    DataGridComponent.Columns[3].Width = 150;
                    DataGridComponent.Columns[3].Visible = true;


                    DataGridComponent.Columns[4].Name = "Role Design";
                    DataGridComponent.Columns[4].HeaderText = "Role Design";
                    DataGridComponent.Columns[4].Width = 200;
                    DataGridComponent.Columns[4].Visible = false;

                    SqlParameter[] sqparameters = { new SqlParameter("@TypeMUid", 29), new SqlParameter("@Active", true) };
                    DataTable dtC = db.GetDataWithParam(CommandType.StoredProcedure, "SP_GetGeneralM", sqparameters, connection);
                    DataGridComponent.Rows.Clear();
                    for (int i = 0; i < dtC.Rows.Count; i++)
                    {
                        int index = DataGridComponent.Rows.Add();
                        DataGridViewRow dataGridViewRow = (DataGridViewRow)DataGridComponent.Rows[index];
                        dataGridViewRow.Cells[1].Value = dtC.Rows[i]["GeneralName"].ToString();
                        dataGridViewRow.Cells[2].Value = dtC.Rows[i]["GUid"].ToString();
                        dataGridViewRow.Cells[3].Value = "";
                        dataGridViewRow.Cells[4].Value = "";
                    }

                    DatGridColourCompostion.Columns.Clear();
                    DatGridColourCompostion.AutoGenerateColumns = false;
                    DatGridColourCompostion.ColumnCount = 4;

                    DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn1 = new DataGridViewCheckBoxColumn();
                    dataGridViewCheckBoxColumn.HeaderText = "Chck";
                    dataGridViewCheckBoxColumn.Name = "Chck";
                    dataGridViewCheckBoxColumn.Width = 50;
                    DatGridColourCompostion.Columns.Insert(0, dataGridViewCheckBoxColumn1);

                    DatGridColourCompostion.Columns[1].Name = "SlNo";
                    DatGridColourCompostion.Columns[1].HeaderText = "SlNo";
                    DatGridColourCompostion.Columns[1].Width = 45;

                    DatGridColourCompostion.Columns[2].Name = "Colour";
                    DatGridColourCompostion.Columns[2].HeaderText = "Colour";
                    DatGridColourCompostion.Columns[2].Width = 130;

                    DatGridColourCompostion.Columns[3].Name = "Compostion";
                    DatGridColourCompostion.Columns[3].HeaderText = "Compostion";
                    DatGridColourCompostion.Columns[3].Width = 85;

                    DatGridColourCompostion.Columns[4].Name = "ID";
                    DatGridColourCompostion.Columns[4].HeaderText = "ID";
                    DatGridColourCompostion.Columns[4].Visible = false;
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected void LoadComponentGridSummary()
        {
            try
            {
                DataGridFabricColourSummary.DataSource = null;
                DataGridFabricColourSummary.AutoGenerateColumns = false;
                DataGridFabricColourSummary.ColumnCount = 3;

                DataGridFabricColourSummary.Columns[0].Name = "Component";
                DataGridFabricColourSummary.Columns[0].HeaderText = "Component";
                DataGridFabricColourSummary.Columns[0].Width = 150;

                DataGridFabricColourSummary.Columns[1].Name = "Fabric";
                DataGridFabricColourSummary.Columns[1].HeaderText = "Fabric";
                DataGridFabricColourSummary.Columns[1].Width = 450;

                DataGridFabricColourSummary.Columns[2].Name = "Combo";
                DataGridFabricColourSummary.Columns[2].HeaderText = "Combo";
                DataGridFabricColourSummary.Columns[2].Width = 120;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected void LoadComponentGrid()
        {
            try
            {
                DataGridFabric.DataSource = null;
                DataGridFabric.AutoGenerateColumns = false;
                DataGridFabric.ColumnCount = 16;
                DataGridFabric.Columns[0].Name = "SlNo";
                DataGridFabric.Columns[0].HeaderText = "SlNo";
                DataGridFabric.Columns[0].Width = 45;

                DataGridFabric.Columns[1].Name = "Component";
                DataGridFabric.Columns[1].HeaderText = "Component";
                DataGridFabric.Columns[1].Width = 100;

                DataGridFabric.Columns[2].Name = "Fabric Name";
                DataGridFabric.Columns[2].HeaderText = "Fabric Name";
                DataGridFabric.Columns[2].Width = 400;

                DataGridFabric.Columns[3].Name = "GSM";
                DataGridFabric.Columns[3].HeaderText = "GSM";
                DataGridFabric.Columns[3].Width = 60;
                DataGridFabric.Columns[3].DefaultCellStyle.Format = "0";

                DataGridFabric.Columns[4].Name = "Tolerence";
                DataGridFabric.Columns[4].HeaderText = "Tolerence";
                DataGridFabric.Columns[4].Width = 70;

                DataGridFabric.Columns[5].Name = "Type";
                DataGridFabric.Columns[5].HeaderText = "Type";
                DataGridFabric.Columns[5].Width = 100;

                DataGridFabric.Columns[6].Name = "Pc Wt(gms)";
                DataGridFabric.Columns[6].HeaderText = "Pc Wt(gms)";
                DataGridFabric.Columns[6].Width = 70;
                DataGridFabric.Columns[6].DefaultCellStyle.Format = "N2";

                DataGridFabric.Columns[7].Name = "ComponentId";
                DataGridFabric.Columns[7].HeaderText = "ComponentId";
                DataGridFabric.Columns[7].Visible = false;

                DataGridFabric.Columns[8].Name = "Uid";
                DataGridFabric.Columns[8].HeaderText = "Uid";
                DataGridFabric.Columns[8].Visible = false;

                DataGridFabric.Columns[9].Name = "FabricUid";
                DataGridFabric.Columns[9].HeaderText = "FabricUid";
                DataGridFabric.Columns[9].Visible = false;

                DataGridFabric.Columns[10].Name = "FinalProcess";
                DataGridFabric.Columns[10].HeaderText = "FinalProcess";
                DataGridFabric.Columns[10].Visible = false;

                DataGridFabric.Columns[11].Name = "Size Name";
                DataGridFabric.Columns[11].HeaderText = "Size Name";
                DataGridFabric.Columns[11].Width = 50;

                DataGridFabric.Columns[12].Name = "SizeUid";
                DataGridFabric.Columns[12].HeaderText = "SizeUid";
                DataGridFabric.Columns[12].Visible = false;

                DataGridFabric.Columns[13].Name = "Dia";
                DataGridFabric.Columns[13].HeaderText = "Dia";
                DataGridFabric.Columns[13].Visible = true;
                DataGridFabric.Columns[13].Width = 50;

                DataGridFabric.Columns[14].Name = "Combo";
                DataGridFabric.Columns[14].HeaderText = "Combo";
                DataGridFabric.Columns[14].Visible = true;

                DataGridFabric.Columns[15].Name = "ComboUid";
                DataGridFabric.Columns[15].HeaderText = "ComboUid";
                DataGridFabric.Columns[15].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected void LoadYarComponentGrid()
        {
            try
            {
                DataGridYarnComponent.DataSource = null;
                DataGridYarnComponent.AutoGenerateColumns = false;
                DataGridYarnComponent.ColumnCount = 6;
                DataGridYarnComponent.Columns[0].Name = "SlNo";
                DataGridYarnComponent.Columns[0].HeaderText = "SlNo";
                DataGridYarnComponent.Columns[0].Width = 50;

                DataGridYarnComponent.Columns[1].Name = "YarnName";
                DataGridYarnComponent.Columns[1].HeaderText = "YarnName";
                DataGridYarnComponent.Columns[1].Width = 230;

                DataGridYarnComponent.Columns[2].Name = "Colour";
                DataGridYarnComponent.Columns[2].HeaderText = "Colour";
                DataGridYarnComponent.Columns[2].Width = 175;

                DataGridYarnComponent.Columns[3].Name = "Mixing %";
                DataGridYarnComponent.Columns[3].HeaderText = "Mixing %";
                DataGridYarnComponent.Columns[3].Width = 140;

                DataGridYarnComponent.Columns[4].Name = "Uid";
                DataGridYarnComponent.Columns[4].HeaderText = "Uid";
                DataGridYarnComponent.Columns[4].Visible = false;

                DataGridYarnComponent.Columns[5].Name = "YarnId";
                DataGridYarnComponent.Columns[5].HeaderText = "YarnId";
                DataGridYarnComponent.Columns[5].Visible = false;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected bool CheckGridValue()
        {
            bool entryFound = false;
            try
            {
                foreach (DataGridViewRow row in DataGridFabric.Rows)
                {
                    object val2 = row.Cells[1].Value;
                    object val3 = row.Cells[2].Value;
                    object val4 = row.Cells[11].Value;
                    if (val2 != null && val2.ToString() == txtComponent.Text && val3 != null && val3.ToString() == txtFabric.Text)
                    {
                        entryFound = true;
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            return entryFound;
        }

        private void BtnOk_Click(object sender, EventArgs e)
        {
            try
            {
                string FPreocess = string.Empty;
                if (chckFabFinalProcess.Checked == true)
                {
                    FPreocess = "Y";
                }
                else
                {
                    FPreocess = "N";
                }
                foreach (DataGridViewRow row2 in DataGridSize.Rows)
                {
                    DataGridViewCheckBoxCell cellComponent = row2.Cells[0] as DataGridViewCheckBoxCell;
                    if (cellComponent.Value != null)
                    {
                        if (cellComponent.Value.ToString() == "True")
                        {
                            //bool entryfound = CheckGridValue();
                            bool entryfound = false;
                            if (entryfound == false)
                            {
                                SqlParameter[] sqlParameters = {
                                    new SqlParameter("@Uid","0"),
                                    new SqlParameter("@OrderMuid",txtDocNo.Tag),
                                    new SqlParameter("@OrderMStyleuid",CmbStyle.SelectedValue),
                                    new SqlParameter("@Componentuid",txtComponent.Tag),
                                    new SqlParameter("@FabRICUid",txtFabric.Tag),
                                    new SqlParameter("@GSM",txtGSM.Text),
                                    new SqlParameter("@Tolerance",txtToterence.Text),
                                    new SqlParameter("@FabricWt",txttAvgWt.Text),
                                    new SqlParameter("@FType",cmbType.Text),
                                    new SqlParameter("@IsFinal",FPreocess),
                                    new SqlParameter("@SizeUid",row2.Cells[2].Value.ToString()),
                                    new SqlParameter("@Dia",row2.Cells[3].Value.ToString()),
                                    new SqlParameter("@ComboUid",txtDiaApply.Tag),
                                    new SqlParameter("@FabColourUid",CmbFabAll.SelectedValue)
                                };
                                db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_OrderMFabric", sqlParameters, connection);
                                SqlParameter[] sqlParameters1 = {
                                    new SqlParameter("@StyleComboUid",txtDiaApply.Tag),
                                    new SqlParameter("@StyleSizeUid",row2.Cells[2].Value.ToString())
                                };
                                DataTable dataTable = db.GetDataWithParam(CommandType.StoredProcedure, "Proc_GetSizeQty", sqlParameters1, connection);
                                decimal qty;
                                decimal pcWt;
                                decimal totalpcwt;
                                if (dataTable.Rows.Count > 0)
                                {
                                    qty = Convert.ToDecimal(dataTable.Rows[0]["Qty"].ToString());
                                    pcWt = Convert.ToDecimal(txttAvgWt.Text);
                                    totalpcwt = qty * pcWt;
                                }
                                else
                                {
                                    qty = 0;
                                    pcWt = 0;
                                    totalpcwt = qty * pcWt;
                                }
                                SqlParameter[] sqls = {
                                    new SqlParameter("@Uid","0"),
                                    new SqlParameter("@StyleUid",CmbStyle.SelectedValue),
                                    new SqlParameter("@FabricUid",txtFabric.Tag),
                                    new SqlParameter("@ComboUid",txtDiaApply.Tag),
                                    new SqlParameter("@ComponentUid",txtComponent.Tag),
                                    new SqlParameter("@Weght",totalpcwt),
                                    new SqlParameter("@OrderMUid",CmbStyle.SelectedValue),
                                    new SqlParameter("@SizeUid",row2.Cells[2].Value.ToString()),
                                };
                                db.ExecuteNonQuery(CommandType.StoredProcedure, "Proc_OrderMFabricWeight", sqls, connection);
                            }
                            else
                            {
                                MessageBox.Show("Duplicate record", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                txtComponent.Focus();
                            }
                            chckFabFinalProcess.Checked = false;
                        }
                    }
                }
                txtComponent.Focus();
                SqlParameter[] parameters = { new SqlParameter("@OrderMuid", txtDocNo.Tag) };
                DataTable dt = db.GetDataWithParam(CommandType.StoredProcedure, "SP_GetComponent", parameters, connection);
                if (dt.Rows.Count > 0)
                {
                    FillComponentGrid(dt);
                }
                SqlParameter[] parameters2 = { new SqlParameter("@OrderMuid", txtDocNo.Tag) };
                DataTable dt3 = db.GetDataWithParam(CommandType.StoredProcedure, "SP_GetComponentSummary", parameters2, connection);
                if (dt.Rows.Count > 0)
                {
                    FillComponentGridSummary(dt3);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void FillComponentGrid(DataTable dt)
        {
            try
            {
                DataGridFabric.Rows.Clear();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataGridViewRow row = (DataGridViewRow)DataGridFabric.Rows[0].Clone();
                    row.Cells[0].Value = i + 1;
                    row.Cells[1].Value = dt.Rows[i]["Component"].ToString();
                    row.Cells[2].Value = dt.Rows[i]["Fabric"].ToString();
                    row.Cells[3].Value = Convert.ToDecimal(dt.Rows[i]["GSM"].ToString()).ToString("0");
                    row.Cells[4].Value = dt.Rows[i]["Tolerance"].ToString();
                    row.Cells[5].Value = dt.Rows[i]["FType"].ToString();
                    row.Cells[6].Value = Convert.ToDecimal(dt.Rows[i]["FabricWt"].ToString()).ToString("0.00");
                    row.Cells[7].Value = dt.Rows[i]["Componentuid"].ToString();
                    row.Cells[8].Value = dt.Rows[i]["Uid"].ToString();
                    row.Cells[9].Value = dt.Rows[i]["FabRICUid"].ToString();
                    row.Cells[10].Value = dt.Rows[i]["IsFinal"].ToString();
                    row.Cells[11].Value = dt.Rows[i]["SizeName"].ToString();
                    row.Cells[12].Value = dt.Rows[i]["SizeUid"].ToString();
                    row.Cells[13].Value = dt.Rows[i]["Dia"].ToString();
                    row.Cells[14].Value = dt.Rows[i]["Combo"].ToString();
                    row.Cells[15].Value = dt.Rows[i]["ComboUid"].ToString();
                    DataGridFabric.Rows.Add(row);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void TxtComponent_Click(object sender, EventArgs e)
        {
            try
            {
                SqlParameter[] parameters = { new SqlParameter("@OrderMUid", txtDocNo.Tag) };
                DataTable dt = db.GetDataWithParam(CommandType.StoredProcedure, "Proc_GetComponentFabric", parameters, connection);
                bsComponent.DataSource = dt;
                DtFabric = dt;
                CmbFabAll.DataSource = null;
                CmbFabAll.DisplayMember = "Fbric";
                CmbFabAll.ValueMember = "Uid";
                CmbFabAll.DataSource = DtFabric;
                //FillGrid(dt, 2);
                //Point point = GeneralParameters.FindLocation(txtComponent);
                //grSearch.Location = new Point(point.X - 10, point.Y + 20);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void TxtFabric_Click(object sender, EventArgs e)
        {
            try
            {
                SqlParameter[] parameters = { new SqlParameter("@OrderMUid", txtDocNo.Tag), new SqlParameter("@StyleUid", CmbStyle.SelectedValue) };
                DataTable dt = db.GetDataWithParam(CommandType.StoredProcedure, "Proc_GetFabricComponent", parameters, connection);
                bsFabric.DataSource = dt;
                FillGrid(dt, 3);
                Point point = GeneralParameters.FindLocation(txtFabric);
                grSearch.Location = new Point(point.X - 10, point.Y + 20);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void TxtGSM_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && e.KeyChar != '.')
                {
                    e.Handled = true;
                }

                if (e.KeyChar == '.' && (sender as TextBox).Text.IndexOf('.') > -1)
                {
                    e.Handled = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void Control_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Up)
            {
                this.SelectNextControl((Control)sender, false, true, true, true);
            }
            else if (e.KeyCode == Keys.Down)
            {
                this.SelectNextControl((Control)sender, true, true, true, true);
            }
            else if (e.KeyCode == Keys.Enter)
            {
                this.SelectNextControl((Control)sender, true, true, true, true);
            }
        }

        private void SplitSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtDocNo.Text != string.Empty)
                {
                    SqlParameter[] sqlParameters = {
                        new SqlParameter("@Uid",txtOrderNo.Tag),
                        new SqlParameter("@OrderMUid",txtDocNo.Tag),
                        new SqlParameter("@DocNo",txtOrderNo.Text),
                        new SqlParameter("@DocDate",Convert.ToDateTime(dtpDocDate.Text).ToString("yyyy-MM-dd")),
                        new SqlParameter("@IsFabFinal",false),
                        new SqlParameter("@IsFabColourFinal",false),
                        new SqlParameter("@IsFabProcessFinal",false),
                        new SqlParameter("@IsYarnFinal",false),
                        new SqlParameter("@IsOverFinal",false),
                    };
                    db.ExecuteNonQuery(CommandType.StoredProcedure, "Proc_FabricBomM", sqlParameters, connection);
                }
                SqlParameter[] sqlParameters1 = { new SqlParameter("@ORDERMUID", txtDocNo.Tag), new SqlParameter("@STYLEUID", CmbStyle.SelectedValue) };
                db.ExecuteNonQuery(CommandType.StoredProcedure, "PROC_FABRICPROCESSLOSSCALC", sqlParameters1, connection);
                //string doc = txtDocNo.Text;
                //string N = doc.Substring(doc.Length - 5);
                //GeneralParameters.UpdateDocNo(2, Convert.ToDecimal(N), connection);
                MessageBox.Show("Record has been saved successfully", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                grBack.Visible = false;
                grFront.Visible = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void CmbStyle_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    DataGridFabric.Rows.Clear();
                    DataGridYarnComponent.Rows.Clear();
                    DataGridYarnProcess.Rows.Clear();
                    DataGridFabricProcess.Rows.Clear();

                    SqlParameter[] sqlParameters = { new SqlParameter("@OrdermUid", txtDocNo.Tag) };
                    DataTable dataTable = db.GetDataWithParam(CommandType.StoredProcedure, "Proc_GetStyleCombo", sqlParameters, connection);
                    DataGridComboColour.Rows.Clear();
                    for (int i = 0; i < dataTable.Rows.Count; i++)
                    {
                        int Index = DataGridComboColour.Rows.Add();
                        DataGridViewRow row = (DataGridViewRow)DataGridComboColour.Rows[Index];
                        row.Cells[1].Value = dataTable.Rows[i]["ComboUid"].ToString();
                        row.Cells[2].Value = dataTable.Rows[i]["Uid"].ToString();
                    }

                    CmbYarnProcessCombo.DataSource = null;
                    CmbYarnProcessCombo.DisplayMember = "ComboUid";
                    CmbYarnProcessCombo.ValueMember = "Uid";
                    CmbYarnProcessCombo.DataSource = dataTable;

                    //for (int i = 0; i < dataTable.Rows.Count; i++)
                    //{
                    //    int Index = DataGridYarnProcessCombo.Rows.Add();
                    //    DataGridViewRow row = (DataGridViewRow)DataGridYarnProcessCombo.Rows[Index];
                    //    row.Cells[1].Value = dataTable.Rows[i]["ComboUid"].ToString();
                    //    row.Cells[2].Value = dataTable.Rows[i]["Uid"].ToString();
                    //}

                    SqlParameter[] Parameters = { new SqlParameter("@OrderMUid", txtDocNo.Tag) };
                    DataTable data = db.GetDataWithParam(CommandType.StoredProcedure, "Proc_FabricBomEdit", Parameters, connection);
                    DatagridFabricColour.Rows.Clear();
                    if (data.Rows.Count > 0)
                    {
                        for (int i = 0; i < data.Rows.Count; i++)
                        {
                            int Index = DatagridFabricColour.Rows.Add();
                            DataGridViewRow row = (DataGridViewRow)DatagridFabricColour.Rows[Index];
                            row.Cells[0].Value = i + 1;
                            row.Cells[1].Value = data.Rows[i]["FabricName"].ToString();
                            row.Cells[2].Value = data.Rows[i]["Combo"].ToString();
                            row.Cells[3].Value = data.Rows[i]["Component"].ToString();
                            row.Cells[4].Value = data.Rows[i]["Type"].ToString();
                            row.Cells[5].Value = data.Rows[i]["NofColour"].ToString();
                            row.Cells[6].Value = data.Rows[i]["FabricUid"].ToString();
                            row.Cells[7].Value = data.Rows[i]["ComboUid"].ToString();
                            row.Cells[8].Value = data.Rows[i]["ComponentId"].ToString();
                            row.Cells[9].Value = data.Rows[i]["Colour"].ToString();
                            row.Cells[10].Value = data.Rows[i]["StyleUid"].ToString();
                            row.Cells[11].Value = data.Rows[i]["IsFinal"].ToString();
                            row.Cells[12].Value = data.Rows[i]["RollDesign"].ToString();
                        }
                    }
                    SqlParameter[] parameters = { new SqlParameter("@OrderMuid", txtDocNo.Tag) };
                    DataTable dt = db.GetDataWithParam(CommandType.StoredProcedure, "SP_GetComponent", parameters, connection);
                    if (dt.Rows.Count > 0)
                    {
                        FillComponentGrid(dt);
                    }
                    SqlParameter[] parameters2 = { new SqlParameter("@OrderMuid", txtDocNo.Tag) };
                    DataTable dt3 = db.GetDataWithParam(CommandType.StoredProcedure, "SP_GetComponentSummary", parameters2, connection);
                    if (dt.Rows.Count > 0)
                    {
                        FillComponentGridSummary(dt3);
                    }
                    SqlParameter[] parameters1 = { new SqlParameter("@OrderMUid", txtDocNo.Tag) };
                    DataTable dt2 = db.GetDataWithParam(CommandType.StoredProcedure, "SP_GetOrderMFabricProcess", parameters1, connection);
                    FillFabricProcessGrid(dt2);
                    SqlParameter[] parameters3 = { new SqlParameter("@OrderMUid", txtDocNo.Tag) };
                    DataTable dt4 = db.GetDataWithParam(CommandType.StoredProcedure, "SP_GetOrderMYarnProcess", parameters3, connection);
                    FillYarnProcessGrid(dt4);
                    string Query = @"Select b.ComboUid as Combo,c.YarnName,a.Colour,a.Composition,a.YarnId,a.Comboid,a.Uid from OrderMFabricYarnColour a 
                                    inner join StyleCombo b on a.OrderMuid = b.OrdermUid and a.ComboId = b.Uid
                                    inner join YarnMaster c on a.Yarnid = c.Yarnid
                                    Where a.OrderMuid =" + txtDocNo.Tag + "";
                    DataTable dtColour = db.GetDataWithoutParam(CommandType.Text, Query, connection);
                    FillYarnColour(dtColour);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void FillYarnColour(DataTable dtColour)
        {
            try
            {
                DataGridYarnColour.Rows.Clear();
                for (int i = 0; i < dtColour.Rows.Count; i++)
                {
                    int Index1 = DataGridYarnColour.Rows.Add();
                    DataGridViewRow row2 = DataGridYarnColour.Rows[Index1];
                    row2.Cells[0].Value = dtColour.Rows[i]["Combo"].ToString();
                    row2.Cells[1].Value = dtColour.Rows[i]["YarnName"].ToString();
                    row2.Cells[2].Value = dtColour.Rows[i]["Colour"].ToString();
                    row2.Cells[3].Value = dtColour.Rows[i]["Composition"].ToString();
                    row2.Cells[4].Value = dtColour.Rows[i]["YarnId"].ToString();
                    row2.Cells[5].Value = dtColour.Rows[i]["Comboid"].ToString();
                    row2.Cells[6].Value = dtColour.Rows[i]["Uid"].ToString();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void FillComponentGridSummary(DataTable dt3)
        {
            try
            {
                for (int i = 0; i < dt3.Rows.Count; i++)
                {
                    int Index = DataGridFabricColourSummary.Rows.Add();
                    DataGridViewRow row = (DataGridViewRow)DataGridFabricColourSummary.Rows[Index];
                    row.Cells[0].Value = dt3.Rows[i]["Component"].ToString();
                    row.Cells[1].Value = dt3.Rows[i]["Fabric"].ToString();
                    row.Cells[2].Value = dt3.Rows[i]["ComboUid"].ToString();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void TabControlAdv1_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (txtDocNo.Tag != null)
                {
                    if (tabControlAdv1.SelectedIndex == 2)
                    {
                        string Query = "Select Distinct Colour from OrderMFabricBomColour Where OrderMUid =" + txtDocNo.Tag + "";
                        DataTable data = db.GetDataWithoutParam(CommandType.Text, Query, connection);
                        DataRow dataRow = data.NewRow();
                        dataRow["Colour"] = "Blank";
                        data.Rows.Add(dataRow);
                        CmbProcessColour.DisplayMember = "Colour";
                        CmbProcessColour.ValueMember = "Colour";
                        CmbProcessColour.DataSource = data;

                        SqlParameter[] sqlParameters = { new SqlParameter("@OrderMUid", txtDocNo.Tag), new SqlParameter("@StyleUid", CmbStyle.SelectedValue) };
                        DataTable dt = db.GetDataWithParam(CommandType.StoredProcedure, "Proc_GetFabricComponent", sqlParameters, connection);
                        bsFabric.DataSource = dt;
                        //FillGrid(dt, 7);
                        //Point point = GeneralParameters.FindLocation(txtFabricProcess);
                        //grSearch.Location = new Point(point.X - 10, point.Y + 20);
                        //grSearch.Visible = true;
                        CmbFabricProcess.DataSource = null;
                        CmbFabricProcess.DisplayMember = "FabricName";
                        CmbFabricProcess.ValueMember = "FabricUid";
                        CmbFabricProcess.DataSource = dt;
                    }
                    else if (tabControlAdv1.SelectedIndex == 1)
                    {
                        SelectId = 1;
                        LoadSize();
                        SqlParameter[] parameters = { new SqlParameter("@OrderMUid", txtDocNo.Tag) };
                        DataTable dt = db.GetDataWithParam(CommandType.StoredProcedure, "Proc_GetComponentFabric", parameters, connection);
                        bsComponent.DataSource = dt;
                        DtFabric = dt;
                        CmbFabAll.DataSource = null;
                        CmbFabAll.DisplayMember = "Fbric";
                        CmbFabAll.ValueMember = "Uid";
                        CmbFabAll.DataSource = DtFabric;
                        CmbFabAll.SelectedIndex = -1;
                        SelectId = 0;
                    }
                    else if (tabControlAdv1.SelectedIndex == 3)
                    {
                        SqlParameter[] parameters = { new SqlParameter("@OrderMuid", txtDocNo.Tag) };
                        DataTable dt = db.GetDataWithParam(CommandType.StoredProcedure, "Proc_GetYarnforBom", parameters, connection);

                        CmbYarn.DisplayMember = "YarnName";
                        CmbYarn.ValueMember = "YarnId";
                        CmbYarn.DataSource = dt;

                        DataTable dtne = dt;

                        //CmbFabBOMYarn.DisplayMember = "YarnName";
                        //CmbFabBOMYarn.ValueMember = "YarnId";
                        //CmbFabBOMYarn.DataSource = dtne;

                        string Query = "Select Distinct Colour from OrderMFabricBomColour Where OrderMUid =" + txtDocNo.Tag + "";
                        DataTable data = db.GetDataWithoutParam(CommandType.Text, Query, connection);
                        CmboYarnProcessColor.DisplayMember = "Colour";
                        CmboYarnProcessColor.ValueMember = "Uid";
                        CmboYarnProcessColor.DataSource = data;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
        protected DataTable LoadDatatable()
        {
            DataTable dt = new DataTable();
            try
            {
                int Active = 1;
                SqlParameter[] para = { new SqlParameter("@Active", Active) };
                dt = db.GetDataWithParam(CommandType.StoredProcedure, "SP_GetYarnMaster", para, connection);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            return dt;
        }

        private void DataGridYarnComponent_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex == 1)
                {
                    DataGridYarnComponent.Rows[e.RowIndex].Cells[0].Value = DataGridYarnComponent.Rows.Count - 1;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        protected void LoadDataGridYarnProcess()
        {
            try
            {
                DataGridYarnProcess.DataSource = null;
                DataGridYarnProcess.AutoGenerateColumns = false;
                DataGridYarnProcess.ColumnCount = 10;
                DataGridYarnProcess.Columns[0].Name = "SlNo";
                DataGridYarnProcess.Columns[0].HeaderText = "SlNo";
                DataGridYarnProcess.Columns[0].Width = 50;

                DataGridYarnProcess.Columns[1].Name = "SeqNo";
                DataGridYarnProcess.Columns[1].HeaderText = "SeqNo";
                DataGridYarnProcess.Columns[1].Width = 90;

                DataGridYarnProcess.Columns[2].Name = "Yarn";
                DataGridYarnProcess.Columns[2].HeaderText = "Yarn";
                DataGridYarnProcess.Columns[2].Width = 275;

                DataGridYarnProcess.Columns[3].Name = "Process";
                DataGridYarnProcess.Columns[3].HeaderText = "Process";
                DataGridYarnProcess.Columns[3].Width = 260;

                DataGridYarnProcess.Columns[4].Name = "Colour";
                DataGridYarnProcess.Columns[4].HeaderText = "Colour";
                DataGridYarnProcess.Columns[4].Width = 145;

                DataGridYarnProcess.Columns[5].Name = "Process Loss %";
                DataGridYarnProcess.Columns[5].HeaderText = "Process Loss %";
                DataGridYarnProcess.Columns[5].Width = 125;

                DataGridYarnProcess.Columns[6].Name = "YarnID";
                DataGridYarnProcess.Columns[6].HeaderText = "YarnID";
                DataGridYarnProcess.Columns[6].Visible = false;

                DataGridYarnProcess.Columns[7].Name = "ProcessId";
                DataGridYarnProcess.Columns[7].HeaderText = "ProcessId";
                DataGridYarnProcess.Columns[7].Visible = false;

                DataGridYarnProcess.Columns[8].Name = "Uid";
                DataGridYarnProcess.Columns[8].HeaderText = "Uid";
                DataGridYarnProcess.Columns[8].Visible = false;

                DataGridYarnProcess.Columns[9].Name = "Stage";
                DataGridYarnProcess.Columns[9].HeaderText = "Stage";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void TxtYarn_Click(object sender, EventArgs e)
        {
            try
            {
                SqlParameter[] parameters = { new SqlParameter("@OrderMuid", txtDocNo.Tag) };
                DataTable dt = db.GetDataWithParam(CommandType.StoredProcedure, "Proc_GetYarnforBom", parameters, connection);
                bsYarn.DataSource = dt;
                FillGrid(dt, 4);
                Point point = GeneralParameters.FindLocation(txtYarn);
                grSearch.Location = new Point(point.X - 10, point.Y + 20);
                grSearch.Visible = true;

                SqlParameter[] parameters1 = { new SqlParameter("@OrderMuid", txtDocNo.Tag) };
                DataTable dt1 = db.GetDataWithParam(CommandType.StoredProcedure, "SP_GetOrderMYarn", parameters, connection);

                CmboYarnProcessColor.DisplayMember = "Colour";
                CmboYarnProcessColor.ValueMember = "Uid";
                CmboYarnProcessColor.DataSource = dt;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void TxtProcess_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "SP_GetProcessM", connection);
                bsProcess.DataSource = dt;
                FillGrid(dt, 5);
                Point point = GeneralParameters.FindLocation(txtProcess);
                grSearch.Location = new Point(point.X - 10, point.Y + 20);
                grSearch.Visible = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void BtnYarnProcess_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtSeqNo.Text != string.Empty || txtYarn.Text != string.Empty)
                {
                    if (txtProcess.Tag == null)
                    {
                        txtProcess.Tag = "0";
                    }
                    if (txtProcessLoss.Text == string.Empty)
                    {
                        txtProcessLoss.Text = "0";
                    }
                    SqlParameter[] parameters = {
                        new SqlParameter("@Uid","0"),
                        new SqlParameter("@OrderMuid",txtDocNo.Tag),
                        new SqlParameter("@OrderMStyleuid",CmbStyle.SelectedValue),
                        new SqlParameter("@SeqNo",txtSeqNo.Text),
                        new SqlParameter("@Yarnuid",CmbYarn.SelectedValue),
                        new SqlParameter("@ProcessUid",txtProcess.Tag),
                        new SqlParameter("@YarnColour",CmboYarnProcessColor.Text),
                        new SqlParameter("@ProcessLoss",txtProcessLoss.Text),
                        new SqlParameter("@Stage",CmbStage.Text)
                    };
                    db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_OrderMYarnProcess", parameters, connection);
                    SqlParameter[] parameters1 = { new SqlParameter("@OrderMUid", txtDocNo.Tag) };

                    SqlParameter[] sqlParameters2 = {
                        new SqlParameter("@ORDERMUID",txtDocNo.Tag),
                        new SqlParameter("@STYLEUID",CmbStyle.SelectedValue),
                    };
                    db.ExecuteNonQuery(CommandType.StoredProcedure, "PROC_YARNPROCESSLOSSCALC", sqlParameters2, connection);
                    DataTable dt = db.GetDataWithParam(CommandType.StoredProcedure, "SP_GetOrderMYarnProcess", parameters1, connection);
                    FillYarProcessGrid(dt);
                    txtSeqNo.Text = string.Empty;
                    txtYarn.Text = string.Empty;
                    txtProcess.Text = string.Empty;
                    //txtColour.Text = string.Empty;
                    txtProcessLoss.Text = string.Empty;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void FillYarProcessGrid(DataTable dt)
        {
            try
            {
                DataGridYarnProcess.Rows.Clear();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataGridViewRow row = (DataGridViewRow)DataGridYarnProcess.Rows[0].Clone();
                    row.Cells[0].Value = i + 1;
                    row.Cells[1].Value = dt.Rows[i]["SeqNo"].ToString();
                    row.Cells[2].Value = dt.Rows[i]["YarnName"].ToString();
                    row.Cells[3].Value = dt.Rows[i]["ProcessName"].ToString();
                    row.Cells[4].Value = dt.Rows[i]["YarnColour"].ToString();
                    row.Cells[5].Value = dt.Rows[i]["ProcessLoss"].ToString();
                    row.Cells[6].Value = dt.Rows[i]["Yarnuid"].ToString();
                    row.Cells[7].Value = dt.Rows[i]["ProcessUid"].ToString();
                    row.Cells[8].Value = dt.Rows[i]["Uid"].ToString();
                    row.Cells[9].Value = dt.Rows[i]["Stage"].ToString();
                    DataGridYarnProcess.Rows.Add(row);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void TxtYarnAllec_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dt = LoadDatatable();
                bsYarnA.DataSource = dt;
                FillGrid(dt, 6);
                Point point = GeneralParameters.FindLocation(txtYarnAllec);
                grSearch.Location = new Point(point.X - 10, point.Y + 20);
                grSearch.Visible = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void TxtYarnAllec_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bsYarnA.Filter = string.Format("YarnName Like '%{0}%'", txtYarnAllec.Text);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void TxtYarn_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bsYarn.Filter = string.Format("YarnName Like '%{0}%'", txtYarn.Text);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void TxtProcess_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bsProcess.Filter = string.Format("ProcessName Like '%{0}%'", txtProcess.Text);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void TxtFabric_TextChanged(object sender, EventArgs e)
        {
            try
            {
                //if (SelectId == 0)
                //{
                //    bsFabric.Filter = string.Format("FabricName Like '%{0}%'", txtFabric.Text);
                //}
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void TxtComponent_TextChanged(object sender, EventArgs e)
        {
            try
            {
                //if (SelectId == 0)
                //{
                //    bsComponent.Filter = string.Format("Component Like '%{0}%'", txtComponent.Text);
                //}
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void BtnYarnOk_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtYarnAllec.Text != string.Empty || txtClr.Text != string.Empty || txtMixing.Text != string.Empty)
                {
                    SqlParameter[] parameters = {
                        new SqlParameter("@Uid","0"),
                        new SqlParameter("@OrderMuid",txtDocNo.Tag),
                        new SqlParameter("@OrderMFabricuid",CmbYarnComp.SelectedValue),
                        new SqlParameter("@YarnUid",txtYarnAllec.Tag),
                        new SqlParameter("@YarnColour",txtClr.Text),
                        new SqlParameter("@YarnMixing",txtMixing.Text)
                    };
                    db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_OrderMYarn", parameters, connection);
                    DataTable dt = LoadYarnGrid();
                    FillYarnGrid(dt);
                    txtYarnAllec.Text = string.Empty;
                    txtYarnAllec.Tag = string.Empty;
                    txtClr.Text = string.Empty;
                    txtMixing.Text = string.Empty;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void FillYarnGrid(DataTable dt)
        {
            try
            {
                DataGridYarnComponent.Rows.Clear();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataGridViewRow row = (DataGridViewRow)DataGridYarnComponent.Rows[0].Clone();
                    row.Cells[0].Value = i + 1;
                    row.Cells[1].Value = dt.Rows[i]["YarnName"].ToString();
                    row.Cells[2].Value = dt.Rows[i]["YarnColour"].ToString();
                    row.Cells[3].Value = dt.Rows[i]["YarnMixing"].ToString();
                    row.Cells[4].Value = dt.Rows[i]["Uid"].ToString();
                    row.Cells[5].Value = dt.Rows[i]["YarnUid"].ToString();
                    DataGridYarnComponent.Rows.Add(row);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected DataTable LoadYarnGrid()
        {
            DataTable dt = new DataTable();
            try
            {
                SqlParameter[] parameters = { new SqlParameter("@OrderMuid", txtDocNo.Tag) };
                dt = db.GetDataWithParam(CommandType.StoredProcedure, "SP_GetOrderMYarn", parameters, connection);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            return dt;
        }

        private void CmbYarnComp_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (CmbYarnComp.SelectedIndex != -1)
                {

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected void LoadDataGridFabricProcess()
        {
            try
            {
                DataGridFabricProcess.DataSource = null;
                DataGridFabricProcess.AutoGenerateColumns = false;
                DataGridFabricProcess.ColumnCount = 10;
                DataGridFabricProcess.Columns[0].Name = "SlNo";
                DataGridFabricProcess.Columns[0].HeaderText = "SlNo";
                DataGridFabricProcess.Columns[0].Width = 50;

                DataGridFabricProcess.Columns[1].Name = "SeqNo";
                DataGridFabricProcess.Columns[1].HeaderText = "SeqNo";
                DataGridFabricProcess.Columns[1].Width = 50;

                DataGridFabricProcess.Columns[2].Name = "Fabric";
                DataGridFabricProcess.Columns[2].HeaderText = "Fabric";
                DataGridFabricProcess.Columns[2].Width = 500;

                DataGridFabricProcess.Columns[3].Name = "Process";
                DataGridFabricProcess.Columns[3].HeaderText = "Process";
                DataGridFabricProcess.Columns[3].Width = 150;

                DataGridFabricProcess.Columns[4].Name = "Colour";
                DataGridFabricProcess.Columns[4].HeaderText = "Colour";
                DataGridFabricProcess.Columns[4].Width = 145;

                DataGridFabricProcess.Columns[5].Name = "Process Loss %";
                DataGridFabricProcess.Columns[5].HeaderText = "Process Loss %";
                DataGridFabricProcess.Columns[5].Width = 100;

                DataGridFabricProcess.Columns[6].Name = "FabricId";
                DataGridFabricProcess.Columns[6].HeaderText = "FabricId";
                DataGridFabricProcess.Columns[6].Visible = false;

                DataGridFabricProcess.Columns[7].Name = "ProcessId";
                DataGridFabricProcess.Columns[7].HeaderText = "ProcessId";
                DataGridFabricProcess.Columns[7].Visible = false;

                DataGridFabricProcess.Columns[8].Name = "Uid";
                DataGridFabricProcess.Columns[8].HeaderText = "Uid";
                DataGridFabricProcess.Columns[8].Visible = false;

                DataGridFabricProcess.Columns[9].Name = "Final Process";
                DataGridFabricProcess.Columns[9].HeaderText = "Final Process";
                DataGridFabricProcess.Columns[9].Visible = false;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void BtnFabricProcess_Click(object sender, EventArgs e)
        {
            try
            {
                string FProcess = string.Empty;
                if (chckFabProcessFinal.Checked == true)
                {
                    FProcess = "Y";
                }
                else
                {
                    FProcess = "N";
                }
                if (txtFabricSeqNo.Text != string.Empty || CmbFabricProcess.Text != string.Empty || txtFProcess.Text != string.Empty || txtFabricProcessLoss.Text != string.Empty)
                {
                    SqlParameter[] parameters = {
                        new SqlParameter("@Uid","0"),
                        new SqlParameter("@OrderMuid",txtDocNo.Tag),
                        new SqlParameter("@OrderMStyleuid",CmbStyle.SelectedValue),
                        new SqlParameter("@SeqNo",txtFabricSeqNo.Text),
                        new SqlParameter("@OrderMFabricUid",CmbFabricProcess.SelectedValue),
                        new SqlParameter("@ProcessUid",txtFProcess.Tag),
                        new SqlParameter("@FabricColour",CmbProcessColour.Text),
                        new SqlParameter("@ProcessLoss",txtFabricProcessLoss.Text),
                        new SqlParameter("@IsFinal",FProcess)
                    };
                    db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_OrderMFabricProcess", parameters, connection);
                    SqlParameter[] parameters1 = { new SqlParameter("@OrderMUid", txtDocNo.Tag) };
                    DataTable dt = db.GetDataWithParam(CommandType.StoredProcedure, "SP_GetOrderMFabricProcess", parameters1, connection);
                    FillFabricProcessGrid(dt);
                    txtFabricSeqNo.Text = string.Empty;
                    txtFProcess.Text = string.Empty;
                    CmbProcessColour.SelectedIndex = -1;
                    txtFabricProcessLoss.Text = string.Empty;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void FillFabricProcessGrid(DataTable dt)
        {
            try
            {
                DataGridFabricProcess.Rows.Clear();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataGridViewRow row = (DataGridViewRow)DataGridFabricProcess.Rows[0].Clone();
                    row.Cells[0].Value = i + 1;
                    row.Cells[1].Value = dt.Rows[i]["SeqNo"].ToString();
                    row.Cells[2].Value = dt.Rows[i]["FabricName"].ToString();
                    row.Cells[3].Value = dt.Rows[i]["ProcessName"].ToString();
                    row.Cells[4].Value = dt.Rows[i]["FabricColour"].ToString();
                    row.Cells[5].Value = dt.Rows[i]["ProcessLoss"].ToString();
                    row.Cells[6].Value = dt.Rows[i]["OrderMFabricUid"].ToString();
                    row.Cells[7].Value = dt.Rows[i]["ProcessUid"].ToString();
                    row.Cells[8].Value = dt.Rows[i]["Uid"].ToString();
                    row.Cells[9].Value = dt.Rows[i]["IsFinal"].ToString();
                    DataGridFabricProcess.Rows.Add(row);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void FillYarnProcessGrid(DataTable dt)
        {
            try
            {
                DataGridYarnProcess.Rows.Clear();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataGridViewRow row = (DataGridViewRow)DataGridYarnProcess.Rows[0].Clone();
                    row.Cells[0].Value = i + 1;
                    row.Cells[1].Value = dt.Rows[i]["SeqNo"].ToString();
                    row.Cells[2].Value = dt.Rows[i]["YarnName"].ToString();
                    row.Cells[3].Value = dt.Rows[i]["ProcessName"].ToString();
                    row.Cells[4].Value = dt.Rows[i]["YarnColour"].ToString();
                    row.Cells[5].Value = dt.Rows[i]["ProcessLoss"].ToString();
                    row.Cells[6].Value = dt.Rows[i]["Yarnuid"].ToString();
                    row.Cells[7].Value = dt.Rows[i]["ProcessUid"].ToString();
                    row.Cells[8].Value = dt.Rows[i]["Uid"].ToString();
                    DataGridYarnProcess.Rows.Add(row);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void TxtFabricProcess_Click(object sender, EventArgs e)
        {
            try
            {
                SqlParameter[] sqlParameters = { new SqlParameter("@OrderMUid", txtDocNo.Tag), new SqlParameter("@StyleUid", CmbStyle.SelectedValue) };
                DataTable dt = db.GetDataWithParam(CommandType.StoredProcedure, "Proc_GetFabricComponent", sqlParameters, connection);
                bsFabric.DataSource = dt;
                FillGrid(dt, 7);
                Point point = GeneralParameters.FindLocation(txtFabricProcess);
                grSearch.Location = new Point(point.X - 10, point.Y + 20);
                grSearch.Visible = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void TxtFProcess_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "SP_GetProcessMFabric", connection);
                bsProcess.DataSource = dt;
                FillGrid(dt, 8);
                Point point = GeneralParameters.FindLocation(txtFProcess);
                grSearch.Location = new Point(point.X - 10, point.Y + 20);
                grSearch.Visible = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtFabricComponentColour_Enter(object sender, EventArgs e)
        {
            try
            {
                SqlParameter[] parameters = { new SqlParameter("@TypeMUid", 29), new SqlParameter("@Active", true) };
                DataTable dt = db.GetDataWithParam(CommandType.StoredProcedure, "SP_GetGeneralM", parameters, connection);
                bsComponent.DataSource = dt;
                FillGrid(dt, 9);
                Point point = GeneralParameters.FindLocation(txtClr);
                grSearch.Location = new Point(point.X - 10, point.Y + 20);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void DataGridCommon_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                SelectId = 1;
                int Index = DataGridCommon.SelectedCells[0].RowIndex;
                if (Fillid == 1)
                {
                    txtDocNo.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtDocNo.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    GetOrderMEdit(Convert.ToDecimal(txtDocNo.Tag));
                    CmbStyle_SelectedIndexChanged(sender, e);
                    LoadCombo(Convert.ToDecimal(txtDocNo.Tag));
                }
                else if (Fillid == 2)
                {
                    txtComponent.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtComponent.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                }
                else if (Fillid == 3)
                {
                    txtFabric.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtFabric.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                }
                else if (Fillid == 4)
                {
                    txtYarn.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtYarn.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                }
                else if (Fillid == 5)
                {
                    txtProcess.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtProcess.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                }
                else if (Fillid == 6)
                {
                    txtYarnAllec.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtYarnAllec.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                }
                else if (Fillid == 7)
                {
                    txtFabricProcess.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtFabricProcess.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                }
                else if (Fillid == 8)
                {
                    txtFProcess.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtFProcess.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                }
                else if (Fillid == 9)
                {
                    //txtFabricComponentColour.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    //txtFabricComponentColour.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                }
                grSearch.Visible = false;
                SelectId = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void BtnFabricColour_Click(object sender, EventArgs e)
        {
            try
            {
                string FProcess = string.Empty;
                string RollDesign = string.Empty;
                if (chckFinalProcess.Checked == true)
                {
                    FProcess = "Y";
                }
                else
                {
                    FProcess = "N";
                }
                string Colour = string.Empty;
                if (CmbFabricColourType.Text == "SOLID" || CmbFabricColourType.Text == "MELANGE")
                {

                }
                else
                {
                    //foreach (DataGridViewRow item in DatGridColourCompostion.Rows)
                    //{

                    //    if (Colour == string.Empty)
                    //    {
                    //        Colour = item.Cells[2].Value.ToString() + " " + item.Cells[3].Value.ToString() + "%";
                    //    }
                    //    else
                    //    {
                    //        Colour = Colour + "," + item.Cells[2].Value.ToString() + " " + item.Cells[3].Value.ToString() + "%";
                    //    }
                    //}
                }

                if (CmbFabricCombo.SelectedIndex != -1 && CmbFabricColourType.SelectedIndex != -1)
                {
                    foreach (DataGridViewRow row2 in DataGridComponent.Rows)
                    {
                        DataGridViewCheckBoxCell cellComponent = row2.Cells[0] as DataGridViewCheckBoxCell;
                        if (cellComponent.Value != null)
                        {
                            if (cellComponent.Value.ToString() == "True")
                            {
                                foreach (DataGridViewRow row1 in DataGridComboColour.Rows)
                                {
                                    DataGridViewCheckBoxCell cell = row1.Cells[0] as DataGridViewCheckBoxCell;
                                    if (cell.Value != null)
                                    {
                                        if (cell.Value.ToString() == "True")
                                        {
                                            int Index = DatagridFabricColour.Rows.Add();
                                            DataGridViewRow row = (DataGridViewRow)DatagridFabricColour.Rows[Index];
                                            if (CmbFabricColourType.Text == "SOLID" || CmbFabricColourType.Text == "MELANGE")
                                            {
                                                Colour = row2.Cells[3].Value.ToString();
                                                if (row2.Cells[4].Visible == false)
                                                {
                                                    RollDesign = "";
                                                }
                                                else
                                                {
                                                    if (string.IsNullOrEmpty(row2.Cells[4].Value.ToString()))
                                                    {
                                                        RollDesign = "";
                                                    }
                                                    else
                                                    {
                                                        RollDesign = row2.Cells[4].Value.ToString();
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                Colour = row2.Cells[3].Value.ToString();
                                                if (row2.Cells[4].Visible == false)
                                                {
                                                    RollDesign = "";
                                                }
                                                else
                                                {
                                                    if (string.IsNullOrEmpty(row2.Cells[4].Value.ToString()))
                                                    {
                                                        RollDesign = "";
                                                    }
                                                    else
                                                    {
                                                        RollDesign = row2.Cells[4].Value.ToString();
                                                    }
                                                }
                                            }
                                            row.Cells[0].Value = DatagridFabricColour.Rows.Count;
                                            row.Cells[1].Value = CmbFabricCombo.Text;
                                            row.Cells[2].Value = row1.Cells[1].Value.ToString();
                                            row.Cells[3].Value = row2.Cells[1].Value.ToString();
                                            row.Cells[4].Value = CmbFabricColourType.Text;
                                            row.Cells[5].Value = DatGridColourCompostion.Rows.Count;
                                            row.Cells[6].Value = CmbFabricCombo.SelectedValue;
                                            row.Cells[7].Value = row1.Cells[2].Value.ToString();
                                            row.Cells[8].Value = row2.Cells[2].Value.ToString();
                                            row.Cells[9].Value = Colour;
                                            row.Cells[10].Value = CmbStyle.SelectedValue;
                                            row.Cells[11].Value = FProcess;
                                            row.Cells[12].Value = RollDesign;

                                            SqlParameter[] sqlParameters ={
                                                new SqlParameter("@ComboUid",row1.Cells[2].Value.ToString()),
                                                new SqlParameter("@ComponentId",row2.Cells[2].Value.ToString()),
                                                new SqlParameter("@Type",CmbFabricColourType.Text),
                                                new SqlParameter("@NofColour",DatGridColourCompostion.Rows.Count),
                                                new SqlParameter("@OrdermUid",txtDocNo.Tag),
                                                new SqlParameter("@Uid","0"),
                                                new SqlParameter("@ReturnID",SqlDbType.Decimal),
                                                new SqlParameter("@Colour",Colour),
                                                new SqlParameter("@FabricUid",CmbFabricCombo.SelectedValue),
                                                new SqlParameter("@StyleUid",CmbStyle.SelectedValue),
                                                new SqlParameter("@IsFinal",FProcess),
                                                new SqlParameter("@RollDesign",RollDesign),
                                            };
                                            sqlParameters[6].Direction = ParameterDirection.Output;
                                            decimal id = db.ExecuteNonQuery(CommandType.StoredProcedure, "Proc_OrderMFabricBomColour", sqlParameters, connection);

                                            if (CmbFabricColourType.Text == "SOLID" || CmbFabricColourType.Text == "MELANGE")
                                            {
                                                foreach (DataGridViewRow item in DatGridColourCompostion.Rows)
                                                {
                                                    DataGridViewCheckBoxCell cellC = item.Cells[0] as DataGridViewCheckBoxCell;
                                                    if (cellC.Value != null)
                                                    {
                                                        if (cellC.Value.ToString() == "True")
                                                        {
                                                            SqlParameter[] parameters = {
                                                                new SqlParameter("@Uid","0"),
                                                                new SqlParameter("@OrdermFabColourUid",id),
                                                                new SqlParameter("@Colour",item.Cells[2].Value.ToString()),
                                                                new SqlParameter("@Composition","0"),
                                                                new SqlParameter("@OrderMUid",txtDocNo.Tag),
                                                                new SqlParameter("@YarnType",CmbFabricColourType.Text)
                                                            };
                                                            db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_OrderMFabricBomColoursList", parameters, connection);
                                                        }
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                foreach (DataGridViewRow item in DatGridColourCompostion.Rows)
                                                {
                                                    DataGridViewCheckBoxCell cellC = item.Cells[0] as DataGridViewCheckBoxCell;
                                                    if (cellComponent.Value != null)
                                                    {
                                                        if (cellComponent.Value.ToString() == "True")
                                                        {
                                                            if (cellC.Value.ToString() == "True")
                                                            {
                                                                SqlParameter[] parameters = {
                                                                    new SqlParameter("@Uid","0"),
                                                                    new SqlParameter("@OrdermFabColourUid",id),
                                                                    new SqlParameter("@Colour",item.Cells[2].Value.ToString()),
                                                                    new SqlParameter("@Composition",item.Cells[3].Value.ToString() ),
                                                                    new SqlParameter("@OrderMUid",txtDocNo.Tag),
                                                                    new SqlParameter("@YarnType",CmbFabricColourType.Text)
                                                                };
                                                                db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_OrderMFabricBomColoursList", parameters, connection);
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                foreach (DataGridViewRow item in DataGridComponent.Rows)
                {
                    item.Cells[3].Value = "";
                }
                CmbFabricCombo.SelectedIndex = -1;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void TxtFabricColourComposition_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void SplitSave_DropDowItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            try
            {
                if (e.ClickedItem.Text == "Back")
                {
                    grFront.Visible = true;
                    grBack.Visible = false;
                    grSearch.Visible = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void TabPageFabricColour_Click(object sender, EventArgs e)
        {

        }

        private void SplitAdd_Click(object sender, EventArgs e)
        {
            try
            {
                txtDocNo.Text = string.Empty;
                DatagridFabricColour.Rows.Clear();
                DataGridFabric.Rows.Clear();
                DataGridFabricColourSummary.Rows.Clear();
                DataGridYarnProcess.Rows.Clear();
                DataGridFabricProcess.Rows.Clear();
                decimal DocTyeId = 2;
                grFront.Visible = false;
                grBack.Visible = true;
                txtOrderNo.Text = GeneralParameters.GetDocNo(DocTyeId, connection);
                txtOrderNo.Tag = "0";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }

        private void SplitAdd_DropDowItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            try
            {
                if (e.ClickedItem.Text == "Edit")
                {
                    SelectId = 0;
                    txtOrderNo.Text = string.Empty;
                    txtDocNo.Text = string.Empty;
                    DatagridFabricColour.Rows.Clear();
                    DataGridFabric.Rows.Clear();
                    DataGridFabricColourSummary.Rows.Clear();
                    DataGridYarnProcess.Rows.Clear();
                    DataGridFabricProcess.Rows.Clear();
                    if (SfdDataGridFabricBom.SelectedIndex != -1)
                    {
                        var selectedItem = SfdDataGridFabricBom.SelectedItems[0];
                        var dataRow = (selectedItem as DataRowView).Row;
                        decimal CUid = Convert.ToDecimal(dataRow["OrdermUid"].ToString());
                        string Query = "select * from OrderMBudjet Where OrdermUid =" + CUid + "";
                        DataTable dataTable = db.GetDataWithoutParam(CommandType.Text, Query, connection);
                        if (dataTable.Rows.Count > 0)
                        {
                            if (dataTable.Rows[0]["Sts"].ToString() == "Pending")
                            {

                                txtOrderNo.Text = dataRow["DocNo"].ToString();
                                txtOrderNo.Tag = dataRow["Uid"].ToString();
                                GetOrderMEdit(CUid);
                                CmbStyle_SelectedIndexChanged(sender, e);
                                LoadCombo(Convert.ToDecimal(txtDocNo.Tag));
                                LoadSize();
                                grFront.Visible = false;
                                grBack.Visible = true;
                            }
                            else
                            {
                                MessageBox.Show("Budget Approved can't Edit", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                return;
                            }
                        }
                        else
                        {
                            txtOrderNo.Text = dataRow["DocNo"].ToString();
                            txtOrderNo.Tag = dataRow["Uid"].ToString();
                            GetOrderMEdit(CUid);
                            CmbStyle_SelectedIndexChanged(sender, e);
                            LoadCombo(Convert.ToDecimal(txtDocNo.Tag));
                            LoadSize();
                            LoadColourCompotion(CmbFabricColourType.Text);
                            grFront.Visible = false;
                            grBack.Visible = true;
                        }
                        SelectId = 1;
                    }
                    else
                    {
                        MessageBox.Show("Select a Row", "Inforation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                }
                else
                {
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }

        protected bool CheckGridColourValue()
        {
            bool entryFound = false;
            try
            {
                foreach (DataGridViewRow item in DataGridComponent.Rows)
                {
                    object component = item.Cells[1].Value;
                    if (component != null)
                    {
                        foreach (DataGridViewRow row in DatGridColourCompostion.Rows)
                        {
                            if (CmbFabricColourType.Text == "SOLID" || CmbFabricColourType.Text == "MELANGE")
                            {
                                object colour = row.Cells[2].Value;
                                object compo = row.Cells[3].Value;
                                if (colour != null && colour.ToString() == txtComboClr.Text && component == compo)
                                {
                                    entryFound = true;
                                    break;
                                }
                            }
                            else
                            {
                                object colour = row.Cells[2].Value;
                                object compo = row.Cells[5].Value;
                                if (colour != null && colour.ToString() == txtComboClr.Text && component == compo)
                                {
                                    entryFound = true;
                                    break;
                                }
                            }
                        }
                    }
                    else
                    {
                        entryFound = false;
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            return entryFound;
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            try
            {
                if (CmbFabricColourType.Text == "SOLID" || CmbFabricColourType.Text == "MELANGE")
                {

                }
                else
                {
                    bool check = CheckGridColourValue();
                    if (check == false)
                    {
                        string component = string.Empty;
                        decimal componentid = 0;
                        foreach (DataGridViewRow item in DataGridComponent.Rows)
                        {
                            DataGridViewCheckBoxCell cellComponent = item.Cells[0] as DataGridViewCheckBoxCell;
                            if (cellComponent.Value != null)
                            {
                                if (cellComponent.Value.ToString() == "True")
                                {
                                    component = item.Cells[1].Value.ToString();
                                    componentid = Convert.ToDecimal(item.Cells[2].Value.ToString());
                                }
                            }
                        }

                        if (txtComboClr.Text != string.Empty && txtFabricColourComposition.Text != string.Empty)
                        {
                            decimal sum = 0;
                            for (int i = 0; i < DatGridColourCompostion.Rows.Count; i++)
                            {
                                sum += Convert.ToDecimal(DatGridColourCompostion.Rows[i].Cells[3].Value.ToString());
                            }
                            sum += Convert.ToDecimal(txtFabricColourComposition.Text);
                            if (sum <= 100)
                            {
                                int Index = DatGridColourCompostion.Rows.Add();
                                DataGridViewRow row = (DataGridViewRow)DatGridColourCompostion.Rows[Index];
                                row.Cells[1].Value = DatGridColourCompostion.Rows.Count;
                                row.Cells[2].Value = txtComboClr.Text;
                                row.Cells[3].Value = txtFabricColourComposition.Text;
                                row.Cells[4].Value = "0";
                                row.Cells[5].Value = component;
                                row.Cells[6].Value = componentid;
                            }
                            else
                            {
                                MessageBox.Show("Composition Exeed 100%", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                return;
                            }
                        }
                    }
                    else
                    {
                        MessageBox.Show("Already Found Component and Colour", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                }
                txtComboClr.Text = string.Empty;
                txtFabricColourComposition.Text = string.Empty;
                txtComboClr.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }

        private void CmbFabricColourType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if(SelectId == 0)
                {
                    LoadColourCompotion(CmbFabricColourType.Text);
                    CmbStructureType_SelectedIndexChanged(sender, e);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }

        private void Label1_Click(object sender, EventArgs e)
        {

        }

        private void DatagridFabricColour_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                DialogResult result = MessageBox.Show("Do you want to delete this row ?", "Information", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
                if (result == DialogResult.Yes)
                {
                    int Index = DatagridFabricColour.SelectedCells[0].RowIndex;
                    SqlParameter[] parameters = {
                        new SqlParameter("@ComboUid",DatagridFabricColour.Rows[Index].Cells[7].Value.ToString()),
                        new SqlParameter("@Componentid",DatagridFabricColour.Rows[Index].Cells[8].Value.ToString()),
                        new SqlParameter("@Type",DatagridFabricColour.Rows[Index].Cells[4].Value.ToString()),
                        new SqlParameter("@orderMuid",txtDocNo.Tag),
                        new SqlParameter("@Fabricuid",DatagridFabricColour.Rows[Index].Cells[6].Value.ToString()),
                        new SqlParameter("@Colour",DatagridFabricColour.Rows[Index].Cells[9].Value.ToString()),
                    };
                    db.ExecuteNonQuery(CommandType.StoredProcedure, "Proc_DeleteFabricBomColour", parameters, connection);
                    DatagridFabricColour.Rows.RemoveAt(Index);
                    DatagridFabricColour.ClearSelection();
                }
                else
                {
                    DatagridFabricColour.ClearSelection();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }

        private void DataGridFabric_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                int Index = DataGridFabric.SelectedCells[0].RowIndex;
                DialogResult dialogResult = MessageBox.Show("Do you want to delete this record ?", "Information", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
                if (dialogResult == DialogResult.Yes)
                {
                    string Query = "Delete from OrderMFabric Where Uid =" + DataGridFabric.Rows[Index].Cells[8].Value.ToString() + "";
                    db.ExecuteNonQuery(CommandType.Text, Query, connection);
                    MessageBox.Show("Deleted Successfully", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    DataGridFabric.Rows.RemoveAt(Index);
                    DataGridFabric.ClearSelection();
                }
                else
                {
                    DataGridFabric.ClearSelection();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }

        private void DataGridFabricProcess_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                int Index = DataGridFabricProcess.SelectedCells[0].RowIndex;
                DialogResult dialogResult = MessageBox.Show("Do you want to delete this record ?", "Information", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
                if (dialogResult == DialogResult.Yes)
                {
                    string Query = "Delete from OrderMFabricProcess Where Uid =" + DataGridFabricProcess.Rows[Index].Cells[8].Value.ToString() + "";
                    db.ExecuteNonQuery(CommandType.Text, Query, connection);
                    MessageBox.Show("Deleted Successfully", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    DataGridFabricProcess.Rows.RemoveAt(Index);
                    DataGridFabricProcess.ClearSelection();
                }
                else
                {
                    DataGridFabricProcess.ClearSelection();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }

        private void ChckDetailFabricClr_CheckedChanged(object sender, EventArgs e)
        {
            if (ChckDetailFabricClr.Checked == true)
            {
                DataGridFabricColourSummary.Visible = false;
                DataGridFabric.Visible = true;
            }
            else
            {
                DataGridFabricColourSummary.Visible = true;
                DataGridFabric.Visible = false;
            }
        }

        private void ChckSelectAll_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (ChckSelectAll.Checked == true)
                {
                    for (int i = 0; i < DataGridSize.RowCount; i++)
                    {
                        DataGridSize[0, i].Value = true;
                    }
                }
                else
                {
                    for (int i = 0; i < DataGridSize.RowCount; i++)
                    {
                        DataGridSize[0, i].Value = false;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }

        private void ChckApplyAll_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (ChckApplyAll.Checked == true)
                {
                    for (int i = 0; i < DataGridSize.RowCount; i++)
                    {
                        DataGridSize[3, i].Value = txtDiaApply.Text;
                    }
                }
                else
                {
                    for (int i = 0; i < DataGridSize.RowCount; i++)
                    {
                        DataGridSize[3, i].Value = "0";
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }

        private void grFront_Enter(object sender, EventArgs e)
        {

        }

        private void BtnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                if (SfdDataGridFabricBom.SelectedIndex != -1)
                {
                    var selectedItem = SfdDataGridFabricBom.SelectedItems[0];
                    var dataRow = (selectedItem as DataRowView).Row;
                    decimal CUid = Convert.ToDecimal(dataRow["OrdermUid"].ToString());
                    GeneralParameters.ReportName = "FabricBOM";
                    GeneralParameters.ReportUid = CUid;
                    FrmReportviwer frmReportviwer = new FrmReportviwer
                    {
                        StartPosition = FormStartPosition.CenterScreen
                    };
                    frmReportviwer.ShowDialog();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        private void CmbFabAll_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    if (CmbFabAll.SelectedIndex != -1)
                    {
                        DataRow[] dataRows = DtFabric.Select("Uid=" + CmbFabAll.SelectedValue + "");
                        if (dataRows.Length > 0)
                        {
                            DataTable data = DtFabric.Select("Uid=" + CmbFabAll.SelectedValue + "").CopyToDataTable();
                            txtComponent.Text = data.Rows[0]["Componenet"].ToString();
                            txtComponent.Tag = data.Rows[0]["ComponentId"].ToString();
                            txtFabric.Text = data.Rows[0]["FabricName"].ToString();
                            txtFabric.Tag = data.Rows[0]["FabricUid"].ToString();
                            txtDiaApply.Tag = data.Rows[0]["ComboUid"].ToString();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void DataGridFabricProcess_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex == 5)
                {
                    string Query = "Update OrderMFabricProcess Set ProcessLoss = " + DataGridFabricProcess.Rows[e.RowIndex].Cells[5].Value + " Where Uid = " + DataGridFabricProcess.Rows[e.RowIndex].Cells[8].Value + "";
                    db.ExecuteNonQuery(CommandType.Text, Query, connection);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void BtnYarnColourCompOk_Click(object sender, EventArgs e)
        {
            try
            {
                decimal sum = 0;
                //foreach (DataGridViewRow row1 in DataGridYarnProcessCombo.Rows)
                //{
                //    DataGridViewCheckBoxCell cell = row1.Cells[0] as DataGridViewCheckBoxCell;
                //    if (cell.Value != null)
                //    {
                //        if (cell.Value.ToString() == "True")
                //        {
                if (txtyarnColour.Text != string.Empty && txtYarnComposition.Text != string.Empty)
                {
                    for (int i = 0; i < DataGridYarnColour.Rows.Count; i++)
                    {
                        sum += Convert.ToDecimal(DataGridYarnColour.Rows[i].Cells[3].Value.ToString());
                    }
                    sum += Convert.ToDecimal(txtYarnComposition.Text);
                    if (sum <= 100)
                    {
                        int Index1 = DataGridYarnColour.Rows.Add();
                        DataGridViewRow row2 = DataGridYarnColour.Rows[Index1];
                        row2.Cells[0].Value = CmbYarnProcessCombo.Text;
                        row2.Cells[1].Value = CmbFabBOMYarn.Text;
                        row2.Cells[2].Value = txtyarnColour.Text;
                        row2.Cells[3].Value = txtYarnComposition.Text;
                        row2.Cells[4].Value = CmbFabBOMYarn.SelectedValue;
                        row2.Cells[5].Value = CmbYarnProcessCombo.SelectedValue;
                        row2.Cells[6].Value = "0";

                        SqlParameter[] sqlParameters = {
                                        new SqlParameter("@Uid","0"),
                                        new SqlParameter("@OrderMUid",txtDocNo.Tag),
                                        new SqlParameter("@YarnId",CmbFabBOMYarn.SelectedValue),
                                        new SqlParameter("@Colour",txtyarnColour.Text),
                                        new SqlParameter("@Composition",txtYarnComposition.Text),
                                        new SqlParameter("@ComboId",CmbYarnProcessCombo.SelectedValue)
                                    };
                        db.ExecuteNonQuery(CommandType.StoredProcedure, "Proc_OrderMFabricYarnColour", sqlParameters, connection);
                    }
                    else
                    {
                        MessageBox.Show("Composition Exeed 100%", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                }
                //}
                //}
                //}
                txtyarnColour.Text = string.Empty;
                txtYarnComposition.Text = string.Empty;
                txtyarnColour.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }

        private void CmbYarn_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (CmbYarn.SelectedIndex != -1)
                {
                    string Query = "Select a.Uid,Colour Colour,Composition  from OrderMFabricYarnColour  a inner join StyleCombo b on a.ComboId = b.Uid Where YarnId = " + CmbYarn.SelectedValue + " and a.OrderMuid=" + txtDocNo.Tag + "";
                    DataTable dt = db.GetDataWithoutParam(CommandType.Text, Query, connection);
                    CmboYarnProcessColor.DisplayMember = "Colour";
                    CmboYarnProcessColor.ValueMember = "Uid";
                    CmboYarnProcessColor.DataSource = dt;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.StackTrace, ex.Message);
                return;
            }
        }

        private void ChckRollForm_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (ChckRollForm.Checked == true)
                {
                    DataGridComponent.Columns[4].Visible = true;
                }
                else
                {
                    DataGridComponent.Columns[4].Visible = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.StackTrace, ex.Message, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void DataGridYarnProcess_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Delete)
                {
                    DialogResult result = MessageBox.Show("Do you want to delete this row ?", "Information", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
                    if (result == DialogResult.Yes)
                    {
                        int Index = DataGridYarnProcess.SelectedCells[0].RowIndex;
                        int Uid = Convert.ToInt32(DataGridYarnProcess.Rows[Index].Cells[8].Value.ToString());
                        if (Uid == 0)
                        {
                            DataGridYarnProcess.Rows.RemoveAt(Index);
                            DataGridYarnProcess.ClearSelection();
                        }
                        else
                        {
                            SqlParameter[] parameters = {
                                new SqlParameter("@Uid",DataGridYarnProcess.Rows[Index].Cells[8].Value.ToString()),
                                new SqlParameter("@OrderMuid",txtDocNo.Tag),
                            };
                            db.ExecuteNonQuery(CommandType.StoredProcedure, "Proc_DeleteFabricBomYarnProcess", parameters, connection);
                            DataGridYarnProcess.Rows.RemoveAt(Index);
                            DataGridYarnProcess.ClearSelection();
                        }
                    }
                    else
                    {
                        DataGridYarnProcess.ClearSelection();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void CmbYarnProcessCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0 && CmbYarnProcessCombo.SelectedIndex != -1)
                {
                    SqlParameter[] sqlParameters = {
                        new SqlParameter("@OrderMuid",txtDocNo.Tag),
                        new SqlParameter("@ComboUid",CmbYarnProcessCombo.SelectedValue),
                    };
                    DataTable dataTable = db.GetDataWithParam(CommandType.StoredProcedure, "Proc_GetYarnforYarnProcess", sqlParameters, connection);
                    CmbFabBOMYarn.DataSource = null;
                    CmbFabBOMYarn.DisplayMember = "YarnName";
                    CmbFabBOMYarn.ValueMember = "YarnId";
                    CmbFabBOMYarn.DataSource = dataTable;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void DataGridYarnColour_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Delete)
                {
                    DialogResult result = MessageBox.Show("Do you want to delete this row ?", "Information", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
                    if (result == DialogResult.Yes)
                    {
                        int Index = DataGridYarnColour.SelectedCells[0].RowIndex;
                        int Uid = Convert.ToInt32(DataGridYarnColour.Rows[Index].Cells[6].Value.ToString());
                        if (Uid == 0)
                        {
                            DataGridYarnColour.Rows.RemoveAt(Index);
                            DataGridYarnColour.ClearSelection();
                        }
                        else
                        {
                            string Query = "Delete from OrderMFabricYarnColour Where OrderMuid =" + txtDocNo.Tag + " and Uid =" + Uid + "";
                            db.ExecuteNonQuery(CommandType.Text, Query, connection);
                            DataGridYarnColour.Rows.RemoveAt(Index);
                            DataGridYarnColour.ClearSelection();
                        }
                    }
                    else
                    {
                        DataGridYarnColour.ClearSelection();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void CmbStructureType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if(SelectId == 0 && CmbStructureType.SelectedIndex != -1)
                {
                    DataTable dataTable = dtFilter.Select("StructureUid=" + CmbStructureType.SelectedValue + " and FabType='" + CmbFabricColourType.Text + "'").CopyToDataTable();
                    CmbFabricCombo.DataSource = null;
                    CmbFabricCombo.DisplayMember = "FabricName";
                    CmbFabricCombo.ValueMember = "Uid";
                    CmbFabricCombo.DataSource = dataTable;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void TxtFProcess_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0 && txtFProcess.Text!= string.Empty)
                {
                    bsProcess.Filter = string.Format("ProcessName Like '%{0}%'", txtFProcess.Text);
                }
                else if (txtFProcess.Text == string.Empty)
                {
                    bsProcess.RemoveFilter();
                }
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}