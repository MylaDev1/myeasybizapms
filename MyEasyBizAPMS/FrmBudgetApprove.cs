﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Windows.Forms;

namespace MyEasyBizAPMS
{
    public partial class FrmBudgetApprove : Form
    {
        public FrmBudgetApprove()
        {
            InitializeComponent();
            this.SfDataGridPreBudjet.Style.HeaderStyle.Font.FontStyle = FontStyle.Bold;
            this.SfDataGridPreBudjet.Style.HeaderStyle.BackColor = Color.DeepSkyBlue;
        }
        SqlConnection connection = new SqlConnection(GeneralParameters.ConnectionString);
        SQLDBHelper db = new SQLDBHelper();
        DataTable dtBudget = new DataTable();

        private void FrmBudgetApprove_Load(object sender, EventArgs e)
        {
            LoadApproveGrid();
        }

        protected void LoadApproveGrid()
        {
            try
            {
                DataTable dataTable = db.GetDataWithoutParam(CommandType.StoredProcedure, "Proc_GetBudgetApprove", connection);
                SfDataGridPreBudjet.DataSource = null;
                SfDataGridPreBudjet.DataSource = dataTable;
                SfDataGridPreBudjet.Columns[0].Width = 60;
                SfDataGridPreBudjet.Columns[1].Visible = false;
                SfDataGridPreBudjet.Columns[2].Visible = false;
                SfDataGridPreBudjet.Columns[3].Width = 90;
                SfDataGridPreBudjet.Columns[4].Width = 200;
                SfDataGridPreBudjet.Columns[5].Width = 90;
                SfDataGridPreBudjet.Columns[6].Width = 90;
                SfDataGridPreBudjet.Columns[7].Width = 80;
                SfDataGridPreBudjet.Columns[8].Width = 100;
                SfDataGridPreBudjet.Columns[9].Width = 90;
                SfDataGridPreBudjet.Columns[10].Width = 90;
                SfDataGridPreBudjet.Columns[11].Width = 90;
                SfDataGridPreBudjet.Columns[12].Width = 90;
                SfDataGridPreBudjet.Columns[13].Visible = false;
                dtBudget = dataTable;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void SfDataGridPreBudjet_CellDoubleClick(object sender, Syncfusion.WinForms.DataGrid.Events.CellClickEventArgs e)
        {
            try
            {
                if (e.DataColumn.Index == 12)
                {
                    var selectedItem = SfDataGridPreBudjet.SelectedItems[0];
                    var dataRow = (selectedItem as DataRowView).Row;
                    decimal OrdeMuid = Convert.ToDecimal(dataRow["OrdermUid"].ToString());
                    decimal BudgetUid = Convert.ToDecimal(dataRow["Uid"].ToString());
                    lblOrderNo.Text = dataRow["DocNo"].ToString();
                    lblStyle.Text = dataRow["StyleName"].ToString();
                    lblSeason.Text = dataRow["Sason"].ToString();
                    lblOrderQty.Text = dataRow["ORDERQTY"].ToString();
                    lblReqQty.Text = dataRow["PLANQTY"].ToString();
                    lblIncome.Text = dataRow["INCOME"].ToString();
                    lblExpense.Text = dataRow["EXPENCES"].ToString();
                    lblPLValue.Text = dataRow["ProfitLossValue"].ToString();
                    lblPLPer.Text = dataRow["Percentage"].ToString();
                    CmbStatus.Text = dataRow["STATUS"].ToString();
                    GrApprove.Visible = true;

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void SplitSave_DropDowItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            try
            {
                if (e.ClickedItem.Text == "Back")
                {
                    GrApprove.Visible = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void SplitSave_Click(object sender, EventArgs e)
        {
            try
            {
                var selectedItem = SfDataGridPreBudjet.SelectedItems[0];
                var dataRow = (selectedItem as DataRowView).Row;
                decimal OrdeMuid = Convert.ToDecimal(dataRow["OrdermUid"].ToString());
                decimal BudgetUid = Convert.ToDecimal(dataRow["Uid"].ToString());
                SqlParameter[] sqlParameters = {
                    new SqlParameter("@OrderMUid",OrdeMuid),
                    new SqlParameter("@OrderMBudgetUid",BudgetUid),
                    new SqlParameter("@DocNo",lblOrderNo.Text),
                    new SqlParameter("@StyleName",lblStyle.Text),
                    new SqlParameter("@Season",lblSeason.Text),
                    new SqlParameter("@OrderQty",lblOrderQty.Text),
                    new SqlParameter("@PlanQty",lblReqQty.Text),
                    new SqlParameter("@Income",lblIncome.Text),
                    new SqlParameter("@Expenses",lblExpense.Text),
                    new SqlParameter("@PLValue",lblPLValue.Text),
                    new SqlParameter("@PLPer",lblPLPer.Text),
                    new SqlParameter("@Sts",CmbStatus.Text),
                    new SqlParameter("@Remarks",txtRemarks.Text),
                    new SqlParameter("@CreateDate",Convert.ToDateTime(DateTime.Now)),
                    new SqlParameter("@UserId",GeneralParameters.UserdId),
                };
                db.ExecuteNonQuery(CommandType.StoredProcedure, "Proc_OrderMBudgetApprove", sqlParameters, connection);
                GrApprove.Visible = false;
                lblOrderNo.Text = string.Empty;
                lblStyle.Text = string.Empty;
                lblSeason.Text = string.Empty;
                lblOrderQty.Text = string.Empty;
                lblReqQty.Text = string.Empty;
                lblIncome.Text = string.Empty;
                lblExpense.Text = string.Empty;
                lblPLValue.Text = string.Empty;
                lblPLPer.Text = string.Empty;
                CmbStatus.Text = string.Empty;
                LoadApproveGrid();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void BtnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BtnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                if (SfDataGridPreBudjet.SelectedIndex != -1)
                {
                    var selectedItem = SfDataGridPreBudjet.SelectedItems[0];
                    var dataRow = (selectedItem as DataRowView).Row;
                    decimal CUid = Convert.ToDecimal(dataRow["OrderMUid"].ToString());
                    GeneralParameters.ReportUid = CUid;
                    GeneralParameters.ReportName = "Budget";
                    FrmReportviwer frmReportviwer = new FrmReportviwer
                    {
                        MdiParent = this.MdiParent,
                        StartPosition = FormStartPosition.Manual,
                        Location = new Point(200, 80)
                    };
                    frmReportviwer.Show();
                }
                else
                {
                    MessageBox.Show("Select a row", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void SfDataGridPreBudjet_QueryCellStyle(object sender, Syncfusion.WinForms.DataGrid.Events.QueryCellStyleEventArgs e)
        {
            try
            {
                if (e.Column.MappingName == "STATUS")
                {
                    if (e.DisplayText == "Pending")
                    {
                        e.Style.BackColor = Color.Orange;
                        e.Style.TextColor = Color.Black;
                    }
                    else
                    {
                        e.Style.BackColor = Color.LightGreen;
                        e.Style.TextColor = Color.DarkSlateBlue;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void BtnDownloadtoExcel_Click(object sender, EventArgs e)
        {
            try
            {
                Microsoft.Office.Interop.Excel._Application app = new Microsoft.Office.Interop.Excel.Application();
                Microsoft.Office.Interop.Excel._Workbook workbook = app.Workbooks.Add(Type.Missing);
                Microsoft.Office.Interop.Excel._Worksheet worksheet = null;
                app.Visible = true;
                worksheet = workbook.Sheets["Sheet1"];
                worksheet = workbook.ActiveSheet;
                worksheet.Name = "Orders";
                for (int i = 0; i < dtBudget.Columns.Count; i++)
                {
                    if (i == 0 || i == 1)
                    {
                    }
                    else
                    {
                        worksheet.Cells[1, i - 1] = dtBudget.Columns[i].ColumnName;
                    }
                }
                for (int i = 0; i < dtBudget.Rows.Count; i++)
                {
                    for (int j = 0; j < dtBudget.Columns.Count; j++)
                    {
                        if (j == 0 || j == 1)
                        {
                        }
                        else
                        {
                            worksheet.Cells[i + 2, j - 1] = dtBudget.Rows[i][j].ToString();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }

        private void SfDataGridPreBudjet_Click(object sender, EventArgs e)
        {

        }
    }
}
