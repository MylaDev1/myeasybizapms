﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Drawing.Imaging;
using System.Configuration;
using System.Data.SqlClient;


namespace MyEasyBizAPMS
{
    public partial class FrmGRN : Form
    {
        public FrmGRN()
        {
            this.BackColor = Color.White; ;
            InitializeComponent();
        }
        int uid = 0;
        int mode = 0;
        string tpuid = "";
        int Fillid;
        int sum;
        double sum1;
        int Dtype = 0;
        int SP;
        int i; int m;
        BindingSource bsParty = new BindingSource();
        BindingSource bsitem = new BindingSource();
        BindingSource bs = new BindingSource();
        string str1key;
        public int SelectId = 0;
        SQLDBHelper db = new SQLDBHelper();
        SqlConnection conn = new SqlConnection(GeneralParameters.ConnectionString);

        //SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connstr"].ConnectionString);
        SqlCommand qur = new SqlCommand();
        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void FrmGRN_Load(object sender, EventArgs e)
        {
            qur.Connection = conn;

            Genclass.Dtype = 9;
            Genclass.buttonstyleform(this);
            Genclass.buttonstylepanel(panadd);
            panadd.Visible = true;

            this.HFGP.DefaultCellStyle.Font = new Font("calibri", 10);
            this.HFGP.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 10, FontStyle.Bold);
            this.HFIT.DefaultCellStyle.Font = new Font("calibri", 10);
            this.HFIT.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 10, FontStyle.Bold);
            this.RQGR.DefaultCellStyle.Font = new Font("calibri", 10);
            this.RQGR.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 10, FontStyle.Bold);

            this.DataGridCommon.DefaultCellStyle.Font = new Font("calibri", 10);
            this.DataGridCommon.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 10, FontStyle.Bold);
            DataGridCommon.RowHeadersVisible = false;
            HFIT.RowHeadersVisible = false;

            HFGP.RowHeadersVisible = false;
            RQGR.RowHeadersVisible = false;
            chkgrd.RowHeadersVisible = false;

            Genpan.Visible = true;
            Editpan.Visible = false;

            if (Text == "Yarn GRN")
            {
                Dtype = 9;

            }
            else if (Text == "Fabric GRN")
            {
                Dtype = 9;

            }
            else if (Text == "Trims GRN")
            {
                Dtype = 9;

            }

            DataGridCommon.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            DataGridCommon.EnableHeadersVisualStyles = false;
            DataGridCommon.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.GradientActiveCaption;
            chkact.Checked = true;
            LoadGetJobCard(1);

            Chkedtact.Checked = true;
            Titlep();
            HFGP.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            HFGP.EnableHeadersVisualStyles = false;
            HFGP.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.GradientActiveCaption;
            HFGP.Focus();
            dtpgrndt.Format = DateTimePickerFormat.Custom;
            dtpgrndt.CustomFormat = "dd/MM/yyyy";
            Dtpdt.Format = DateTimePickerFormat.Custom;
            Dtpdt.CustomFormat = "dd/MM/yyyy";

            serialno.Visible = false;

        }
        private void Titlep()
        {
            HFIT.AutoGenerateColumns = false;
            HFIT.Refresh();
            HFIT.DataSource = null;
            HFIT.Rows.Clear();
            HFIT.ColumnCount = 11;
            HFIT.Columns[0].Name = "Itemname";
            HFIT.Columns[1].Name = "DcQty";
            HFIT.Columns[2].Name = "AccQty";
            HFIT.Columns[3].Name = "RejQty";
            HFIT.Columns[4].Name = "Itid";
            HFIT.Columns[5].Name = "uid";
            HFIT.Columns[6].Name = "refuid";
            HFIT.Columns[7].Name = "NoofBags";
            HFIT.Columns[8].Name = "Addnotes";
            HFIT.Columns[9].Name = "Type";
            HFIT.Columns[10].Name = "refno";
            HFIT.EditMode = DataGridViewEditMode.EditOnKeystroke;
            HFIT.Columns[1].ReadOnly = false;
            HFIT.Columns[2].ReadOnly = false;

            HFIT.Columns[0].Width = 450;
            HFIT.Columns[1].Width = 75;
            HFIT.Columns[2].Width = 75;
            HFIT.Columns[3].Width = 75;
            HFIT.Columns[4].Visible = false;
            HFIT.Columns[5].Visible = false;
            HFIT.Columns[6].Visible = false;
            HFIT.Columns[7].Width = 75;
            HFIT.Columns[8].Width = 150;
            HFIT.Columns[9].Visible = false;
            HFIT.Columns[10].Visible = false;
        }
        private void button1_Click(object sender, EventArgs e)
        {
            mode = 1;
            Genpan.Visible = false;
            Editpan.Visible = true;
            Genclass.ClearTextBox(this, Editpan);
            HFIT.Refresh();
            HFIT.DataSource = null;
            HFIT.Rows.Clear();

            Titlep1();
            Titlep2();
            dtpgrndt.Format = DateTimePickerFormat.Custom;
            dtpgrndt.CustomFormat = "dd MMM yyyy";
            Dtpdt.Format = DateTimePickerFormat.Custom;
            Dtpdt.CustomFormat = "dd MMM yyyy";
            Genclass.Gendocno();
            txtgrn.Text = Genclass.ST;
            txtdcno.Focus();
        }

        private void btnexit_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void btnaddrcan_Click(object sender, EventArgs e)
        {

            Genpan.Visible = true;
            LoadGetJobCard(1);
        }

        protected DataTable LoadGetJobCard(int tag)
        {


            chkact.Checked = true;
            if (chkact.Checked == true)
            {
                SP = 1;

            }
            else
            {
                SP = 0;

            }


            DataTable dt = new DataTable();
            try
            {

                SqlParameter[] para = {
                    new SqlParameter("@active",SP),
                      //new SqlParameter("@COMPANYID","1"),
                      new SqlParameter("@type",Text),


                };

                dt = db.GetData(CommandType.StoredProcedure, "SP_GETGRNLOAD", para);


                LoadDataTable(dt);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        protected void LoadDataTable(DataTable dt)
        {
            try
            {
                HFGP.DataSource = null;
                HFGP.AutoGenerateColumns = false;
                HFGP.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
                HFGP.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
                HFGP.ColumnCount = 10;
                HFGP.Columns[0].Name = "Uid";
                HFGP.Columns[0].HeaderText = "Uid";
                HFGP.Columns[0].DataPropertyName = "Uid";

                HFGP.Columns[1].Name = "GRNNO";
                HFGP.Columns[1].HeaderText = "GRNNO";
                HFGP.Columns[1].DataPropertyName = "GRNNO";

                HFGP.Columns[2].Name = "Date";
                HFGP.Columns[2].HeaderText = "Date";
                HFGP.Columns[2].DataPropertyName = "Date";

                HFGP.Columns[3].Name = "InvNo";
                HFGP.Columns[3].HeaderText = "InvNo";
                HFGP.Columns[3].DataPropertyName = "InvNo";

                HFGP.Columns[4].Name = "InvDate";
                HFGP.Columns[4].HeaderText = "InvDate";
                HFGP.Columns[4].DataPropertyName = "InvDate";

                HFGP.Columns[5].Name = "jjno";
                HFGP.Columns[5].HeaderText = "jjno";
                HFGP.Columns[5].DataPropertyName = "jjno";

                HFGP.Columns[6].Name = "Party";
                HFGP.Columns[6].HeaderText = "Party";
                HFGP.Columns[6].DataPropertyName = "Party";

                HFGP.Columns[7].Name = "puid";
                HFGP.Columns[7].HeaderText = "puid";
                HFGP.Columns[7].DataPropertyName = "puid";

                HFGP.Columns[8].Name = "narration";
                HFGP.Columns[8].HeaderText = "narration";
                HFGP.Columns[8].DataPropertyName = "narration";

                HFGP.Columns[9].Name = "active";
                HFGP.Columns[9].HeaderText = "active";
                HFGP.Columns[9].DataPropertyName = "active";




                bs.DataSource = dt;

                HFGP.DataSource = bs;


                HFGP.Columns[0].Visible = false;
                HFGP.Columns[7].Visible = false;
                HFGP.Columns[8].Visible = false;
                HFGP.Columns[9].Visible = false;
                HFGP.Columns[1].Width = 88;
                HFGP.Columns[2].Width = 89;
                HFGP.Columns[3].Width = 180;
                HFGP.Columns[4].Width = 89;
                HFGP.Columns[5].Visible = false;

                HFGP.Columns[6].Width = 420;

                HFGP.Columns[2].DefaultCellStyle.Format = "yyyy'/'MM'/'dd";
                HFGP.Columns[4].DefaultCellStyle.Format = "yyyy'/'MM'/'dd";


            }
            catch (Exception ex)
            {
                throw ex;
            }
        }




        private void loadput()


        {

        }

        private void loadput1()
        {

        }

        private void Titlep1()
        {
            RQGR.ColumnCount = 5;
            RQGR.Columns[0].Name = "Itemname";
            RQGR.Columns[1].Name = "Reqdate";
            RQGR.Columns[2].Name = "Qty";
            RQGR.Columns[3].Name = "Uid";
            //HFIT.Columns[4].Name = "Itid";
            RQGR.Columns[0].Width = 350;
            RQGR.Columns[1].Width = 100;
            RQGR.Columns[2].Width = 50;
            RQGR.Columns[3].Visible = false;
            RQGR.Columns[4].Visible = false;
        }
        private void Titlep2()
        {
            chkgrd.ColumnCount = 2;
            chkgrd.Columns[0].Name = "Itemname";

            chkgrd.Columns[1].Name = "Uid";


        }


        private void btnadd_Click(object sender, EventArgs e)
        {
            if (txtitem.Text == "")
            {
                MessageBox.Show("Select the Item");
                txtitem.Focus();
                return;
            }
            if (txtqty.Text == "")
            {
                MessageBox.Show("Enter the Quantity");
                txtqty.Focus();
                return;
            }
            if (txtdcqty.Text == "")
            {
                MessageBox.Show("Enter the Dc Quantity");
                txtdcqty.Focus();
                return;
            }
            if (txtrejqty.Text == "")
            {

                txtrejqty.Text = "0";
            }

            if (txtbags.Text == "")
            {

                txtbags.Text = "0";
            }
            double pp = Convert.ToDouble(txtpono.Text);
            double pp1 = Convert.ToDouble(txtdcqty.Text);
            double pp2 = Convert.ToDouble(txtrejqty.Text);
            double pp3 = pp1 + pp2;
            if (pp < pp1)
            {

                MessageBox.Show("Qty Exceed");
                return;
            }



            HFIT.AutoGenerateColumns = false;

            var index = HFIT.Rows.Add();
            HFIT.Rows[index].Cells[0].Value = txtitem.Text;
            HFIT.Rows[index].Cells[1].Value = txtqty.Text;
            HFIT.Rows[index].Cells[2].Value = txtdcqty.Text;
            HFIT.Rows[index].Cells[3].Value = txtrejqty.Text;
            HFIT.Rows[index].Cells[4].Value = txtititd.Text;
            HFIT.Rows[index].Cells[5].Value = 0;
            HFIT.Rows[index].Cells[6].Value = txtpoid.Text;
            HFIT.Rows[index].Cells[7].Value = txtbags.Text;
            HFIT.Rows[index].Cells[8].Value = txtaddnotes.Text;
            HFIT.Rows[index].Cells[9].Value = txtjjno.Text;
            HFIT.Rows[index].Cells[10].Value = txtrecqty.Text;

            conn.Close();
            conn.Open();

            qur.CommandText = "insert into  GRNTemp values (" + txtpoid.Text + "," + txtdcqty.Text + ")";
            qur.ExecuteNonQuery();

            txtjjno.Text = "";
            txtitem.Text = "";
            txtqty.Text = "";
            txtdcqty.Text = "";
            txtaddnotes.Text = "";
            txtititd.Text = "";

        }

        private void buttrqok_Click_1(object sender, EventArgs e)
        {
            if (txtreqqty.Text == "")
            {
                MessageBox.Show("Enter the Quantity");
                return;
            }

            int rowscount = HFIT.Rows.Count - 1;
            int rowscount1 = RQGR.Rows.Count - 1;

            txtqty2.Text = "";
            txtqty1.Text = "";

            for (int i = 0; i < rowscount; i++)
            {
                if (Cboit.Text == HFIT.Rows[i].Cells[0].Value)
                {
                    txtqty2.Text = HFIT.Rows[i].Cells[1].Value.ToString();

                    Genclass.sum = Convert.ToInt16(txtreqqty.Text);

                    for (int j = 0; j < rowscount1; j++)
                    {
                        if (Cboit.Text == RQGR.Rows[j].Cells[0].Value)
                        {
                            Genclass.sum = Genclass.sum + Convert.ToInt16(RQGR.Rows[j].Cells[2].Value);

                            txtqty1.Text = Genclass.sum.ToString();
                            if (Convert.ToInt16(txtqty1.Text) > Convert.ToInt16(txtqty2.Text))
                            {
                                MessageBox.Show("Qty Exceeds");
                                txtqty.Text = "";
                                txtqty.Focus();
                                return;
                            }
                        }
                    }

                }
            }

            Boolean strboo = false;

            if (chkgrd.RowCount - 1 == 0)
            {
                var index1 = chkgrd.Rows.Add();
                chkgrd.Rows[index1].Cells[0].Value = Cboit.Text;

                chkgrd.Rows[index1].Cells[1].Value = Cboit.SelectedValue;

            }
            else
            {
                for (i = 0; i < chkgrd.RowCount - 1; i++)
                {
                    if (chkgrd.Rows[i].Cells[0].Value == Cboit.Text)
                    {
                        strboo = true;
                    }
                }
                if (strboo == false)
                {
                    Titlep2();
                    var index1 = chkgrd.Rows.Add();
                    chkgrd.Rows[index1].Cells[0].Value = Cboit.Text;

                    chkgrd.Rows[index1].Cells[1].Value = Cboit.SelectedValue;

                }

            }

            Titlep1();
            var index = RQGR.Rows.Add();
            RQGR.Rows[index].Cells[0].Value = Cboit.Text;
            RQGR.Rows[index].Cells[1].Value = Dtpreq.Value;
            RQGR.Columns[1].DefaultCellStyle.Format = "yyyy'/'MM'/'dd";
            RQGR.Rows[index].Cells[2].Value = txtreqqty.Text;
            RQGR.Rows[index].Cells[3].Value = Cboit.SelectedValue;
            RQGR.Rows[index].Cells[4].Value = index;


            txtreqqty.Text = "";
        }

        private void Reqbk_Click_1(object sender, EventArgs e)
        {

            if (HFIT.RowCount != chkgrd.RowCount)
            {
                MessageBox.Show("Item not matched");
                return;
            }

            for (i = 0; i < HFIT.RowCount - 1; i++)
            {
                sum = 0;
                for (int j = 0; j < RQGR.RowCount - 1; j++)
                {
                    if (Convert.ToInt16(RQGR.Rows[j].Cells[3].Value) == Convert.ToInt16(HFIT.Rows[i].Cells[4].Value))

                    {
                        sum = sum + Convert.ToInt16(RQGR.Rows[j].Cells[2].Value);
                    }
                }

                if (sum != Convert.ToInt16(HFIT.Rows[i].Cells[1].Value))
                {
                    MessageBox.Show("Qty not matched");
                    return;
                }
            }
            Reqpan.Visible = false;

        }

        private void btnsave_Click_1(object sender, EventArgs e)
        {
            if (mode == 1)
            {
                Genclass.Dtype = 9;
                Genclass.TYPENAME = Text;
                Genclass.Gendocnolist();
                txtgrn.Text = Genclass.ST;
            }

            if (txtname.Text == "")
            {
                MessageBox.Show("Enter the Party");

                txtname.Focus();
                return;
            }

            if (txtdcno.Text == "")
            {
                MessageBox.Show("Enter the txtdcno");

                txtdcno.Focus();
                return;
            }

            if (txtnar.Text == "")
            {

                txtnar.Text = "0";
            }

            if (txtnar.Text == "")
            {

                txtnar.Text = "0";
            }
            conn.Close();
            conn.Open();




            if (mode == 1)
            {

                SqlParameter[] para ={

                    new SqlParameter("@DOCTYPEID",Dtype),
                    new SqlParameter("@DocNo",txtgrn.Text),
                    new SqlParameter("@DocDate",Convert.ToDateTime(dtpgrndt.Text)),
                    new SqlParameter("@RefDocNo",txtdcno.Text),
                    new SqlParameter("@RefDocDate",Convert.ToDateTime(Dtpdt.Text)),
                    new SqlParameter("@PartyUid", txtpuid.Text),
                    new SqlParameter("@Narration",txtnar.Text),
                    new SqlParameter("@BasicTotal","0"),
                    new SqlParameter("@NetValue","0"),
                    new SqlParameter("@Active",1),
                    new SqlParameter("@Yearid",Genclass.Yearid),
                    new SqlParameter("@invno", Convert.ToString(txtdcno.Text)),
                    new SqlParameter("@invdate",Convert.ToDateTime(Dtpdt.Text))

                };
                db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_GRNHEAD", para, conn);

                string quy6 = "select * from grnm where docno='" + txtgrn.Text + "' and doctypeid=" + Dtype + "";
                Genclass.cmd = new SqlCommand(quy6, conn);
                SqlDataAdapter aptr3 = new SqlDataAdapter(Genclass.cmd);
                DataTable tap5 = new DataTable();
                aptr3.Fill(tap5);
                if (tap5.Rows.Count > 0)
                {
                    txtgrnid.Text = tap5.Rows[0]["uid"].ToString();

                }

                for (int j = 0; j < HFIT.RowCount - 1; j++)
                {


                    if (HFIT.Rows[j].Cells[2].Value == "")
                    {

                        HFIT.Rows[j].Cells[2].Value = "0";
                    }
                    if (HFIT.Rows[j].Cells[3].Value == "")
                    {

                        HFIT.Rows[j].Cells[3].Value = "0";
                    }
                    if (HFIT.Rows[j].Cells[4].Value == "")
                    {

                        HFIT.Rows[j].Cells[4].Value = "0";
                    }
                    if (HFIT.Rows[j].Cells[5].Value == "")
                    {

                        HFIT.Rows[j].Cells[5].Value = "0";
                    }
                    if (HFIT.Rows[j].Cells[6].Value == "")
                    {

                        HFIT.Rows[j].Cells[6].Value = "0";
                    }
                    if (HFIT.Rows[j].Cells[9].Value == "")
                    {

                        HFIT.Rows[j].Cells[9].Value = "0";
                    }
                    conn.Close();
                    conn.Open();

                    SqlParameter[] para1 ={
                            new SqlParameter("@uid", "0"),
                            new SqlParameter("@DOCTYPEID",Dtype),
                            new SqlParameter("@guid", txtgrnid.Text),
                            new SqlParameter("@ItemUId", HFIT.Rows[j].Cells[4].Value),
                            new SqlParameter("@PQty", Convert.ToDecimal(HFIT.Rows[j].Cells[2].Value)),
                            new SqlParameter("@BasicValue", Convert.ToDecimal(HFIT.Rows[j].Cells[4].Value)),
                            new SqlParameter("@AddNotes",Convert.ToString(HFIT.Rows[j].Cells[8].Value)),
                            new SqlParameter("@DCQty", Convert.ToDecimal(HFIT.Rows[j].Cells[3].Value)),
                            new SqlParameter("@Refuid",HFIT.Rows[j].Cells[6].Value),
                            new SqlParameter("@dis",HFIT.Rows[j].Cells[7].Value),
                            new SqlParameter("@disval","0"),
                            new SqlParameter("@mode", Convert.ToString(HFIT.Rows[j].Cells[10].Value)),
                            new SqlParameter("@type", Convert.ToString(HFIT.Rows[j].Cells[9].Value)),
                            new SqlParameter("@itemname", Convert.ToString(HFIT.Rows[j].Cells[0].Value)),


                    };


                    db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_GRNDET", para1, conn);
                }

            }


            else
            {
                SqlParameter[] para ={

                   new SqlParameter("@DOCTYPEID",Dtype),
                    new SqlParameter("@DocNo",txtgrn.Text),
                    new SqlParameter("@DocDate",Convert.ToDateTime(dtpgrndt.Text)),
                    new SqlParameter("@RefDocNo",txtdcno.Text),
                    new SqlParameter("@RefDocDate",Convert.ToDateTime(Dtpdt.Text)),
                    new SqlParameter("@PartyUid", txtpuid.Text),
                    new SqlParameter("@Narration",txtnar.Text),
                    new SqlParameter("@BasicTotal","0"),
                    new SqlParameter("@NetValue","0"),
                    new SqlParameter("@Active",1),
                    new SqlParameter("@Yearid",Genclass.Yearid),
                    new SqlParameter("@invno", Convert.ToString(txtdcno.Text)),
                    new SqlParameter("@invdate",Convert.ToDateTime(Dtpdt.Text)),
                    new SqlParameter("@uid",txtgrnid.Text)

                };
                db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_GRNHEADUPDATE", para, conn);

                qur.CommandText = "delete from grnd where guid=" + txtgrnid.Text + "";
                qur.ExecuteNonQuery();

                string quy6 = "select * from grnm where docno='" + txtgrn.Text + "' and doctypeid=9 and uid=" + txtgrnid.Text + "";
                Genclass.cmd = new SqlCommand(quy6, conn);
                SqlDataAdapter aptr3 = new SqlDataAdapter(Genclass.cmd);
                DataTable tap5 = new DataTable();
                aptr3.Fill(tap5);
                if (tap5.Rows.Count > 0)
                {
                    txtgrnid.Text = tap5.Rows[0]["uid"].ToString();

                }


                for (int j = 0; j < HFIT.RowCount - 1; j++)
                {


                    if (HFIT.Rows[j].Cells[2].Value == "")
                    {

                        HFIT.Rows[j].Cells[2].Value = "0";
                    }
                    if (HFIT.Rows[j].Cells[3].Value == "")
                    {

                        HFIT.Rows[j].Cells[3].Value = "0";
                    }
                    if (HFIT.Rows[j].Cells[4].Value == "")
                    {

                        HFIT.Rows[j].Cells[4].Value = "0";
                    }
                    if (HFIT.Rows[j].Cells[5].Value == "")
                    {

                        HFIT.Rows[j].Cells[5].Value = "0";
                    }
                    if (HFIT.Rows[j].Cells[6].Value == "")
                    {

                        HFIT.Rows[j].Cells[6].Value = "0";
                    }
                    if (HFIT.Rows[j].Cells[9].Value == "")
                    {

                        HFIT.Rows[j].Cells[9].Value = "0";
                    }
                    conn.Close();
                    conn.Open();

                    SqlParameter[] para1 ={
                            new SqlParameter("@uid", txtgrnid.Text),
                           new SqlParameter("@DOCTYPEID",Dtype),
                            new SqlParameter("@guid", txtgrnid.Text),
                            new SqlParameter("@ItemUId", HFIT.Rows[j].Cells[4].Value),
                            new SqlParameter("@PQty", Convert.ToDecimal(HFIT.Rows[j].Cells[2].Value)),
                            new SqlParameter("@BasicValue", Convert.ToDecimal(HFIT.Rows[j].Cells[4].Value)),
                            new SqlParameter("@AddNotes",Convert.ToString(HFIT.Rows[j].Cells[8].Value)),
                            new SqlParameter("@DCQty", Convert.ToDecimal(HFIT.Rows[j].Cells[3].Value)),
                            new SqlParameter("@Refuid",HFIT.Rows[j].Cells[6].Value),
                            new SqlParameter("@dis",HFIT.Rows[j].Cells[7].Value),
                            new SqlParameter("@disval","0"),
                            new SqlParameter("@mode", Convert.ToString(HFIT.Rows[j].Cells[10].Value)),
                            new SqlParameter("@type", Convert.ToString(HFIT.Rows[j].Cells[9].Value)),
                            new SqlParameter("@itemname", Convert.ToString(HFIT.Rows[j].Cells[0].Value)),


                };


                    db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_GRNDET", para1, conn);
                }


            }



            if (mode == 1)
            {
                conn.Close();
                conn.Open();
                qur.CommandText = "update doctypemlist set lastno= lastno + 1 where doctypeid=" + Genclass.Dtype + " and finyear='19-20'  and prfix='" + Genclass.prfix + "'";
                qur.ExecuteNonQuery();
            }


            string quyq1 = "select distinct  a.uid from grnm a inner join  grnd b on a.uid=b.guid and a.Doctypeid=b.DocTypeID and b.DocTypeID=9 where  a.docno='" + txtgrn.Text + "' and docdate='" + Convert.ToDateTime(dtpgrndt.Text).ToString("yyyy-MM-dd") + "'";

            Genclass.cmd = new SqlCommand(quyq1, conn);
            SqlDataAdapter aptrq1 = new SqlDataAdapter(Genclass.cmd);
            DataTable tapq1 = new DataTable();
            aptrq1.Fill(tapq1);
            if (tapq1.Rows.Count > 0)
            {
                txtgrnid.Text = tapq1.Rows[0]["uid"].ToString();
                if (mode == 1)
                {
                    qur.CommandText = "exec SP_stockleg " + txtgrnid.Text + "," + GeneralParameters.UserdId + ",1";
                    qur.ExecuteNonQuery();
                }
                else

                {
                    qur.CommandText = "exec SP_stockleg " + txtgrnid.Text + "," + GeneralParameters.UserdId + ",2";
                    qur.ExecuteNonQuery();
                }

            }


            MessageBox.Show("Record has been Updated", "Save", MessageBoxButtons.OK);

            conn.Close();
            LoadGetJobCard(1);
            Genpan.Visible = true;
            Editpan.Visible = false;
            panadd.Visible = true;
            serialno.Visible = false;
        }

        private void txtname_KeyDown_1(object sender, KeyEventArgs e)
        {

            try
            {
                if (e.KeyCode == Keys.F10)
                {
                    grSearch.Visible = false;
                }
                else if (e.KeyCode == Keys.F2)
                {
                    SelectId = 1;
                    int Index = DataGridCommon.SelectedCells[0].RowIndex;

                    txtname.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtpuid.Text = DataGridCommon.Rows[Index].Cells[0].Value.ToString();


                    grSearch.Visible = false;
                    SelectId = 0;
                }
                else if (e.KeyCode == Keys.Escape)
                {
                    grSearch.Visible = false;
                }
                else if (e.KeyValue == 40)
                {
                    DataGridCommon.Select();
                }
                else if (e.KeyCode == Keys.Enter)
                {
                    txtname_Click(sender, e);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

        }


        private void txtitem_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.F10)
                {
                    grSearch.Visible = false;
                }
                else if (e.KeyCode == Keys.F2)
                {
                    SelectId = 1;
                    int Index = DataGridCommon.SelectedCells[0].RowIndex;

                    txtititd.Text = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    txtitem.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtrecqty.Text = DataGridCommon.Rows[Index].Cells[2].Value.ToString();
                    txtpoid.Text = DataGridCommon.Rows[Index].Cells[3].Value.ToString();
                    txtpono.Text = DataGridCommon.Rows[Index].Cells[4].Value.ToString();
                    txtqty.Text = DataGridCommon.Rows[Index].Cells[4].Value.ToString();
                    txtjjno.Text = DataGridCommon.Rows[Index].Cells[5].Value.ToString();
                    txtaddnotes.Text = DataGridCommon.Rows[Index].Cells[6].Value.ToString();


                    grSearch.Visible = false;
                    SelectId = 0;
                }
                else if (e.KeyCode == Keys.Escape)
                {
                    grSearch.Visible = false;
                }
                else if (e.KeyValue == 40)
                {
                    DataGridCommon.Select();
                }
                else if (e.KeyCode == Keys.Enter)
                {
                    txtitem_Click(sender, e);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }


        }

        private void label8_Click_1(object sender, EventArgs e)
        {

            if (HFIT.Rows[0].Cells[0].Value == "" || HFIT.Rows[0].Cells[0].Value == null)
            {
                MessageBox.Show("No Item For Required Date");
                return;
            }
            else
            {
                //Titlep1();
                //Titlep2();

                DataTable dt = new DataTable();
                DataRow dr;
                dt.Columns.Add("Uid");
                dt.Columns.Add("Item");

                int rowscount = HFIT.Rows.Count - 1;

                for (int i = 0; i < rowscount; i++)
                {
                    dr = dt.NewRow();

                    dr["Uid"] = HFIT.Rows[i].Cells[4].Value;
                    dr["Item"] = HFIT.Rows[i].Cells[0].Value;

                    dt.Rows.Add(dr);
                }


                Cboit.DataSource = null;
                Cboit.DataSource = dt;
                Cboit.DisplayMember = "Item";
                Cboit.ValueMember = "uid";
                //Cboit.Text = dt.Rows[0]["Item"].ToString();
                //Cboit.SelectedIndex = -1;
                Reqpan.Visible = true;


            }
        }



        private void btnaddrcan_Click_1(object sender, EventArgs e)
        {
            Editpan.Visible = false;
            Genpan.Visible = true;
            chkact.Checked = true;
            panadd.Visible = true;
        }

        private void btnedit_Click(object sender, EventArgs e)
        {



            try
            {

                conn.Open();
                HFIT.Refresh();
                HFIT.DataSource = null;
                HFIT.Rows.Clear();

                mode = 2;
                Genpan.Visible = false;
                Editpan.Visible = true;
                int i = HFGP.SelectedCells[0].RowIndex;
                txtgrnid.Text = HFGP.Rows[i].Cells[0].Value.ToString();
                txtgrn.Text = HFGP.Rows[i].Cells[1].Value.ToString();
                dtpgrndt.Text = HFGP.Rows[i].Cells[2].Value.ToString();
                txtdcno.Text = HFGP.Rows[i].Cells[3].Value.ToString();
                Dtpdt.Text = HFGP.Rows[i].Cells[4].Value.ToString();
                txtjjno.Text = HFGP.Rows[i].Cells[5].Value.ToString();
                txtname.Text = HFGP.Rows[i].Cells[6].Value.ToString();
                txtpuid.Text = HFGP.Rows[i].Cells[7].Value.ToString();
                txtnar.Text = HFGP.Rows[i].Cells[8].Value.ToString();
                if (HFGP.Rows[i].Cells[9].Value.ToString() == "True")
                {
                    Chkedtact.Checked = true;
                }
                else
                {
                    Chkedtact.Checked = false;
                }
                string quy3 = "select * from TransactionsP a inner join transactionsplist b on a.uid=b.transactionspuid and a.doctypeid=10 inner join  transactionsplist c on b.uid=c.refuid and  c.doctypeid=20   where a.uid=" + txtgrnid.Text + "";
                Genclass.cmd = new SqlCommand(quy3, conn);

                SqlDataAdapter aptr3 = new SqlDataAdapter(Genclass.cmd);
                DataTable tap3 = new DataTable();
                aptr3.Fill(tap3);

                if (tap3.Rows.Count > 0)
                {
                    MessageBox.Show("This GRN No already converted to DCwithout Process");
                    Editpan.Visible = false;
                    Genpan.Visible = true;

                }
                else
                {

                    string quy = "select itemname as Item,Pqty,Dcqty,Addnotes,itemuid,a.uid from TransactionsPlist a inner join itemm b on a.itemuid=b.uid where a.transactionspuid=" + txtgrnid.Text + "";
                    Genclass.cmd = new SqlCommand(quy, conn);

                    SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
                    DataTable tap = new DataTable();
                    aptr.Fill(tap);

                    this.HFIT.DefaultCellStyle.Font = new Font("Calibri", 10);
                    //dgv.ColumnHeadersDefaultCellStyle.Font = new Font(dgv.ColumnHeadersDefaultCellStyle.Font, FontStyle.Bold);
                    this.HFIT.ColumnHeadersDefaultCellStyle.Font = new Font("Calibri", 10, FontStyle.Bold);
                    HFIT.AutoGenerateColumns = false;
                    HFIT.Refresh();
                    HFIT.DataSource = null;
                    HFIT.Rows.Clear();


                    HFIT.ColumnCount = tap.Columns.Count;
                    i = 0;
                    foreach (DataColumn column in tap.Columns)
                    {
                        HFIT.Columns[i].Name = column.ColumnName;
                        HFIT.Columns[i].HeaderText = column.ColumnName;
                        HFIT.Columns[i].DataPropertyName = column.ColumnName;
                        i = i + 1;
                    }




                    HFIT.Columns[0].Width = 450;
                    HFIT.Columns[1].Width = 50;
                    HFIT.Columns[2].Width = 50;
                    HFIT.Columns[3].Width = 250;
                    HFIT.Columns[4].Visible = false;
                    HFIT.Columns[5].Visible = false;
                    HFIT.DataSource = tap;




                    string quy1 = "select itemname as Item,Reqdate,Qty,itemid from grnreqdate a inner join itemm b on a.itemid=b.uid where a.headid=" + txtgrnid.Text + "";
                    Genclass.cmd = new SqlCommand(quy1, conn);

                    SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
                    DataTable tap1 = new DataTable();
                    aptr1.Fill(tap1);


                    RQGR.AutoGenerateColumns = false;
                    RQGR.Refresh();
                    RQGR.DataSource = null;
                    RQGR.Rows.Clear();


                    RQGR.ColumnCount = tap1.Columns.Count;

                    i = 0;
                    foreach (DataColumn column in tap1.Columns)
                    {
                        RQGR.Columns[i].Name = column.ColumnName;
                        RQGR.Columns[i].HeaderText = column.ColumnName;
                        RQGR.Columns[i].DataPropertyName = column.ColumnName;
                        i = i + 1;
                    }



                    RQGR.Columns[0].Width = 350;
                    RQGR.Columns[1].Width = 100;
                    RQGR.Columns[2].Width = 50;
                    RQGR.Columns[3].Visible = false;

                    RQGR.Columns[1].DefaultCellStyle.Format = "yyyy'/'MM'/'dd";


                    RQGR.DataSource = tap1;

                    string quy2 = "select Distinct itemname as Item,itemid from grnreqdate a inner join itemm b on a.itemid=b.uid where a.headid=" + txtgrnid.Text + "";
                    Genclass.cmd = new SqlCommand(quy1, conn);

                    SqlDataAdapter aptr2 = new SqlDataAdapter(Genclass.cmd);
                    DataTable tap2 = new DataTable();
                    aptr2.Fill(tap2);


                    chkgrd.AutoGenerateColumns = false;
                    chkgrd.Refresh();
                    chkgrd.DataSource = null;
                    chkgrd.Rows.Clear();


                    chkgrd.ColumnCount = tap2.Columns.Count;

                    i = 0;
                    foreach (DataColumn column in tap2.Columns)
                    {
                        chkgrd.Columns[i].Name = column.ColumnName;
                        chkgrd.Columns[i].HeaderText = column.ColumnName;
                        chkgrd.Columns[i].DataPropertyName = column.ColumnName;
                        i = i + 1;
                    }



                    chkgrd.Columns[0].Width = 350;
                    chkgrd.Columns[1].Width = 100;

                    chkgrd.DataSource = tap2;

                }
            }


            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }
            finally
            {
                conn.Close();
                Genclass.cmd.Dispose();
            }



        }

        private void txtscr1_TextChanged(object sender, EventArgs e)
        {
            bs.Filter = string.Format("GRNNO Like '%{0}%'  or  Date Like '%{1}%' or  InvNo Like '%{1}%' or  InvDate Like '%{1}%' or  jjno Like '%{1}%'  or  Party Like '%{1}%'  ", txtscr1.Text, txtscr1.Text, txtscr1.Text, txtscr1.Text, txtscr1.Text, txtscr1.Text);

        }

        private void Txtscr2_TextChanged(object sender, EventArgs e)
        {

        }

        private void Txtscr3_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtscr4_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtscr5_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtscr6_TextChanged(object sender, EventArgs e)
        {

        }

        private void HFGP_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {

        }

        private void RQGR_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {

        }

        private void HFIT_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {

        }

        private void HFIT_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

            if (HFIT.CurrentRow.Cells[1].Value != null)
            {
                HFIT.CurrentRow.Cells[1].ReadOnly = false;
                HFIT.Update();
            }
            if (HFIT.CurrentRow.Cells[2].Value != null)
            {
                HFIT.CurrentRow.Cells[2].ReadOnly = false;
                HFIT.Update();
            }
            if (HFIT.CurrentRow.Cells[3].Value != null)
            {
                HFIT.CurrentRow.Cells[3].ReadOnly = false;
                HFIT.Update();
            }

        }


        private void cmdprt_Click(object sender, EventArgs e)
        {
            conn.Open();

            qur.CommandText = "delete from  Printtable";
            qur.ExecuteNonQuery();

            int i = HFGP.SelectedCells[0].RowIndex;

            qur.CommandText = "insert into Printtable select * from vw_TransactionsP where uid=" + HFGP.Rows[i].Cells[0].Value.ToString() + "";
            qur.ExecuteNonQuery();



            Crviewer crv = new Crviewer();
            crv.Show();
            conn.Close();
        }

        private void btndelete_Click(object sender, EventArgs e)
        {
            mode = 3;
            qur.CommandText = "";
            conn.Open();
            int i = HFGP.SelectedCells[0].RowIndex;
            qur.CommandText = "Exec Sp_Transactions " + Dtype + ",'" + txtgrn.Text + "','" + dtpgrndt.Value + "','" + txtdcno.Text + "','" + Dtpdt.Value + "',0,'" + txtnar.Text + "',0,0,0,0,0,0,'" + txtjjno.Text + "'," + GeneralParameters.UserdId + "," + Genclass.Yearid + ",0,0,0,0,'0',0,0,0,0,0,0,0,0,0," + HFGP.Rows[i].Cells[0].Value + ",0," + i + "," + mode + "";
            qur.ExecuteNonQuery();


            string quy = "select * from Msgdes";
            Genclass.cmd = new SqlCommand(quy, conn);

            SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
            DataTable tap = new DataTable();
            aptr.Fill(tap);


            MessageBox.Show(tap.Rows[0]["Msg"].ToString(), "Save", MessageBoxButtons.OK);
            conn.Close();
            LoadGetJobCard(1);
        }

        private void chkact_CheckedChanged(object sender, EventArgs e)
        {
            LoadGetJobCard(1);
        }

        private void txtname_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bsParty.Filter = string.Format("Name LIKE '%{0}%' ", txtname.Text);



                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            mode = 1;
            Genpan.Visible = false;
            Editpan.Visible = true;
            panadd.Visible = false;
            Genclass.ClearTextBox(this, Editpan);
            HFIT.Refresh();
            HFIT.DataSource = null;
            HFIT.Rows.Clear();

            Titlep1();
            Titlep2();
            dtpgrndt.Format = DateTimePickerFormat.Custom;
            dtpgrndt.CustomFormat = "dd MMM yyyy";
            Dtpdt.Format = DateTimePickerFormat.Custom;
            Dtpdt.CustomFormat = "dd MMM yyyy";
            //Genclass.Gendocno();
            //txtgrn.Text = Genclass.ST;
            txtdcno.Focus();

            conn.Close();
            conn.Open();

            qur.CommandText = "truncate table GRNTemp";
            qur.ExecuteNonQuery();
        }

        private void butedit_Click(object sender, EventArgs e)
        {

            try
            {
                conn.Close();
                conn.Open();
                HFIT.Refresh();
                HFIT.DataSource = null;
                HFIT.Rows.Clear();

                mode = 2;
                Genpan.Visible = false;
                Editpan.Visible = true;
                panadd.Visible = false;
                int i = HFGP.SelectedCells[0].RowIndex;
                txtgrnid.Text = HFGP.Rows[i].Cells[0].Value.ToString();
                txtgrn.Text = HFGP.Rows[i].Cells[1].Value.ToString();
                dtpgrndt.Text = HFGP.Rows[i].Cells[2].Value.ToString();
                txtdcno.Text = HFGP.Rows[i].Cells[3].Value.ToString();
                Dtpdt.Text = HFGP.Rows[i].Cells[4].Value.ToString();
                txtjjno.Text = HFGP.Rows[i].Cells[5].Value.ToString();
                txtname.Text = HFGP.Rows[i].Cells[6].Value.ToString();

                txtpuid.Text = HFGP.Rows[i].Cells[7].Value.ToString();
                txtnar.Text = HFGP.Rows[i].Cells[8].Value.ToString();
                if (HFGP.Rows[i].Cells[9].Value.ToString() == "True")
                {
                    Chkedtact.Checked = true;
                }
                else
                {
                    Chkedtact.Checked = false;
                }
                conn.Close();
                conn.Open();

                qur.CommandText = "truncate table GRNTemp";
                qur.ExecuteNonQuery();

                string quy = "SP_GETGRNEDIT " + txtgrnid.Text + "";
                Genclass.cmd = new SqlCommand(quy, conn);

                SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
                DataTable tap = new DataTable();
                aptr.Fill(tap);

                this.HFIT.DefaultCellStyle.Font = new Font("Calibri", 10);
                //dgv.ColumnHeadersDefaultCellStyle.Font = new Font(dgv.ColumnHeadersDefaultCellStyle.Font, FontStyle.Bold);
                this.HFIT.ColumnHeadersDefaultCellStyle.Font = new Font("Calibri", 10, FontStyle.Bold);
                HFIT.AutoGenerateColumns = false;
                HFIT.Refresh();
                HFIT.DataSource = null;
                HFIT.Rows.Clear();


                HFIT.ColumnCount = tap.Columns.Count;
                i = 0;
                foreach (DataColumn column in tap.Columns)
                {
                    HFIT.Columns[i].Name = column.ColumnName;
                    HFIT.Columns[i].HeaderText = column.ColumnName;
                    HFIT.Columns[i].DataPropertyName = column.ColumnName;
                    i = i + 1;
                }




                HFIT.Columns[0].Width = 450;
                HFIT.Columns[1].Width = 80;
                HFIT.Columns[2].Width = 80;
                HFIT.Columns[3].Width = 80;
                HFIT.Columns[4].Visible = false;
                HFIT.Columns[5].Visible = false;
                HFIT.Columns[6].Visible = false;
                HFIT.Columns[7].Width = 80;
                HFIT.Columns[8].Width = 80;
                HFIT.Columns[9].Visible = false;
                HFIT.Columns[10].Visible = false;
                HFIT.DataSource = tap;





            }


            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }
            finally
            {
                conn.Close();
                Genclass.cmd.Dispose();
            }


        }

        private void butexit_Click(object sender, EventArgs e)
        {
            conn.Close();
            conn.Open();



            int i = HFGP.SelectedCells[0].RowIndex;


            qur.CommandText = "delete from   grnd  where guid=" + HFGP.Rows[i].Cells[0].Value.ToString() + "";
            qur.ExecuteNonQuery();

            qur.CommandText = "delete from   grnm  where uid=" + HFGP.Rows[i].Cells[0].Value.ToString() + "";
            qur.ExecuteNonQuery();

            qur.CommandText = "delete from   MFG_STOCK_LEDGER  where HEADTABLE_UID=" + HFGP.Rows[i].Cells[0].Value.ToString() + "";
            qur.ExecuteNonQuery();

            LoadGetJobCard(1);
        }

        private void buttnext1_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void chkact_CheckedChanged_1(object sender, EventArgs e)
        {
            LoadGetJobCard(1);
        }

        private void buttnnvfst_Click(object sender, EventArgs e)
        {
            int index = HFGP.SelectedRows[0].Index;
            int index1 = 0;
            int index2 = HFGP.Rows.Count - 1;

            HFGP.Rows[index].Selected = false;
            HFGP.Rows[index1].Selected = true;
            index1 = index1 + 1;
            lblno1.Text = index1.ToString();
            lblno2.Text = "of " + index2.ToString();
        }

        private void buttnnxtlft_Click(object sender, EventArgs e)
        {

            int index = HFGP.SelectedRows[0].Index;
            int index1 = HFGP.Rows.Count - 1; ;
            if (index > 0)
            {
                HFGP.Rows[index].Selected = false;
                HFGP.Rows[index - 1].Selected = true;
                lblno1.Text = index.ToString();
                lblno2.Text = "of " + index1.ToString();
            }
        }

        private void buttrnxt_Click(object sender, EventArgs e)
        {
            int index = HFGP.SelectedRows[0].Index;
            int index2 = HFGP.SelectedRows[0].Index + 2;
            int index1 = HFGP.Rows.Count - 1; ;
            if (index < HFGP.Rows.Count - 2)
            {
                HFGP.Rows[index].Selected = false;
                HFGP.Rows[index + 1].Selected = true;
                lblno1.Text = index2.ToString();
                lblno2.Text = "of " + index1.ToString();

            }
        }

        private void btnfinnxt_Click(object sender, EventArgs e)
        {
            int index = HFGP.SelectedRows[0].Index;
            int index1 = HFGP.Rows.Count - 1;


            HFGP.Rows[index].Selected = false;

            HFGP.Rows[index1 - 1].Selected = true;
            lblno1.Text = index1.ToString();
            lblno2.Text = "of " + index1.ToString();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            conn.Open();

            qur.CommandText = "delete from  Printtable";
            qur.ExecuteNonQuery();

            int i = HFGP.SelectedCells[0].RowIndex;

            qur.CommandText = "insert into Printtable select * from vw_TransactionsP where uid=" + HFGP.Rows[i].Cells[0].Value.ToString() + "";
            qur.ExecuteNonQuery();



            Crviewer crv = new Crviewer();
            crv.Show();
            conn.Close();
        }

        private void HFGP_CellMouseUp(object sender, DataGridViewCellMouseEventArgs e)
        {

        }

        private void HFGP_KeyUp(object sender, KeyEventArgs e)
        {

        }

        private void txtitem_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bsitem.Filter = string.Format("itemname LIKE '%{0}%' ", txtitem.Text);



                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtname_Click(object sender, EventArgs e)
        {
            Genclass.type = 1;
            DataTable dt = getParty();
            bsParty.DataSource = dt;
            FillGrid(dt, 1);
            Point loc = FindLocation(txtname);
            grSearch.Location = new Point(loc.X, loc.Y + 20);
            grSearch.Visible = true;
            grSearch.Text = "Party Search";

        }

        protected void FillGrid(DataTable dt, int FillId)
        {
            try
            {
                DataGridCommon.DataSource = null;
                DataGridCommon.AutoGenerateColumns = false;
                DataGridCommon.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
                DataGridCommon.DefaultCellStyle.WrapMode = DataGridViewTriState.True;

                DataGridCommon.ColumnCount = 2;
                DataGridCommon.Columns[0].Name = "Uid";
                DataGridCommon.Columns[0].HeaderText = "Uid";
                DataGridCommon.Columns[0].DataPropertyName = "Uid";
                DataGridCommon.Columns[1].Name = "Name";
                DataGridCommon.Columns[1].HeaderText = "Name";
                DataGridCommon.Columns[1].DataPropertyName = "Name";


                DataGridCommon.Columns[1].Width = 350;
                DataGridCommon.DataSource = bsParty;
                DataGridCommon.Columns[0].Visible = false;


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        protected void FillGrid2(DataTable dt, int FillId)
        {
            try
            {
                DataGridCommon.DataSource = null;
                DataGridCommon.AutoGenerateColumns = false;
                DataGridCommon.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
                DataGridCommon.DefaultCellStyle.WrapMode = DataGridViewTriState.True;

                DataGridCommon.ColumnCount = 1;
                DataGridCommon.Columns[0].Name = "Docno";
                DataGridCommon.Columns[0].HeaderText = "PoNo";
                DataGridCommon.Columns[0].DataPropertyName = "Docno";



                DataGridCommon.Columns[0].Width = 350;
                DataGridCommon.DataSource = bs;



            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        protected void FillGrid1(DataTable dt, int FillId)
        {
            try
            {
                DataGridCommon.DataSource = null;
                DataGridCommon.AutoGenerateColumns = false;
                DataGridCommon.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
                DataGridCommon.DefaultCellStyle.WrapMode = DataGridViewTriState.True;

                DataGridCommon.ColumnCount = 8;
                DataGridCommon.Columns[0].Name = "Uid";
                DataGridCommon.Columns[0].HeaderText = "Uid";
                DataGridCommon.Columns[0].DataPropertyName = "Uid";
                DataGridCommon.Columns[1].Name = "Itemname";
                DataGridCommon.Columns[1].HeaderText = "Itemname";
                DataGridCommon.Columns[1].DataPropertyName = "Itemname";
                DataGridCommon.Columns[2].Name = "docno";
                DataGridCommon.Columns[2].HeaderText = "docno";
                DataGridCommon.Columns[2].DataPropertyName = "docno";
                DataGridCommon.Columns[3].Name = "listuid";
                DataGridCommon.Columns[3].HeaderText = "listuid";
                DataGridCommon.Columns[3].DataPropertyName = "listuid";
                DataGridCommon.Columns[4].Name = "qty";
                DataGridCommon.Columns[4].HeaderText = "qty";
                DataGridCommon.Columns[4].DataPropertyName = "qty";

                DataGridCommon.Columns[5].Name = "type";
                DataGridCommon.Columns[5].HeaderText = "type";
                DataGridCommon.Columns[5].DataPropertyName = "type";

                DataGridCommon.Columns[6].Name = "addnotes";
                DataGridCommon.Columns[6].HeaderText = "Socno";
                DataGridCommon.Columns[6].DataPropertyName = "addnotes";

                DataGridCommon.Columns[7].Name = "Style";
                DataGridCommon.Columns[7].HeaderText = "Style";
                DataGridCommon.Columns[7].DataPropertyName = "Style";



                DataGridCommon.Columns[1].Width = 250;
                DataGridCommon.Columns[2].Width = 100;
                DataGridCommon.DataSource = bsitem;
                DataGridCommon.Columns[0].Visible = false;
                DataGridCommon.Columns[3].Visible = false;
                DataGridCommon.Columns[4].Visible = false;
                DataGridCommon.Columns[5].Visible = false;
                DataGridCommon.Columns[6].Width = 100;
                DataGridCommon.Columns[7].Width = 100;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        private Point FindLocation(Control ctrl)
        {
            if (ctrl.Parent is Form)
                return ctrl.Location;
            else
            {
                Point p = FindLocation(ctrl.Parent);
                p.X += ctrl.Location.X;
                p.Y += ctrl.Location.Y;
                return p;
            }
        }

        protected DataTable getParty()
        {
            DataTable dt = new DataTable();
            try
            {
                if (Genclass.type == 1 && Text == "Yarn GRN" || Genclass.type == 1 && Text == "Fabric GRN" || Genclass.type == 1 && Text == "Trims GRN")
                {
                    SqlParameter[] para = { new SqlParameter("@type", Text) };

                    dt = db.getDataWithParameterCmdType(CommandType.StoredProcedure, "SP_GETGRNNAME", para);
                    bsParty.DataSource = dt;

                }

                else if (Genclass.type == 2 && Text == "Yarn GRN" || Genclass.type == 2 && Text == "Fabric GRN" || Genclass.type == 2 && Text == "Trims GRN")
                {
                    SqlParameter[] para = { new SqlParameter("@suppid", Convert.ToInt16(txtpuid.Text)), new SqlParameter("@type", Text), new SqlParameter("@docno", pono.Text) };

                    dt = db.getDataWithParameterCmdType(CommandType.StoredProcedure, "SP_GETGRNITEMNAME", para);
                    bsitem.DataSource = dt;

                }
                else if (Genclass.type == 3 && Text == "Yarn GRN" || Genclass.type == 3 && Text == "Fabric GRN" || Genclass.type == 3 && Text == "Trims GRN")
                {
                    SqlParameter[] para = { new SqlParameter("@suppid", Convert.ToInt16(txtpuid.Text)), new SqlParameter("@type", Text) };

                    dt = db.getDataWithParameterCmdType(CommandType.StoredProcedure, "SP_GETGRNITEMNAMEpono", para);
                    bs.DataSource = dt;

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            return dt;
        }

        private void txtitem_Click(object sender, EventArgs e)
        {

            Genclass.type = 2;
            DataTable dt = getParty();
            bsitem.DataSource = dt;
            FillGrid1(dt, 1);
            Point loc = FindLocation(txtitem);
            grSearch.Location = new Point(loc.X, loc.Y + 20);
            button22.Visible = false;
            grSearch.Visible = true;
            grSearch.Text = "Items Search";
        }

        private void txtitem_MouseClick(object sender, MouseEventArgs e)
        {
            Genclass.type = 2;
            DataTable dt = getParty();
            bsitem.DataSource = dt;
            FillGrid1(dt, 1);
            Point loc = FindLocation(txtitem);
            grSearch.Location = new Point(loc.X, loc.Y + 20);
            button22.Visible = false;
            grSearch.Visible = true;
            grSearch.Text = "Items Search";
        }

        private void dtpgrndt_ValueChanged(object sender, EventArgs e)
        {

        }

        private void dtpgrndt_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                e.Handled = true;
                SendKeys.Send("{TAB}");
            }
        }

        private void txtdcno_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                e.Handled = true;
                SendKeys.Send("{TAB}");
            }
        }

        private void Dtpdt_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                e.Handled = true;
                SendKeys.Send("{TAB}");
            }
        }

        private void txtnar_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                e.Handled = true;
                SendKeys.Send("{TAB}");
            }
        }

        private void txtqty_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                e.Handled = true;
                SendKeys.Send("{TAB}");
            }
        }

        private void txtdcqty_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                e.Handled = true;
                SendKeys.Send("{TAB}");
            }
        }

        private void txtrejqty_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                e.Handled = true;
                SendKeys.Send("{TAB}");
            }
        }

        private void txtaddnotes_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                e.Handled = true;
                SendKeys.Send("{TAB}");
            }
        }

        private void btnHide_Click(object sender, EventArgs e)
        {
            grSearch.Visible = false;
        }

        private void button18_Click(object sender, EventArgs e)
        {
            try
            {
                SelectId = 1;
                int Index = DataGridCommon.SelectedCells[0].RowIndex;
                if (Genclass.type == 1)
                {
                    txtpuid.Text = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    txtname.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();

                }
                else if (Genclass.type == 2)
                {
                    txtititd.Text = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    txtitem.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtrecqty.Text = DataGridCommon.Rows[Index].Cells[2].Value.ToString();
                    txtpoid.Text = DataGridCommon.Rows[Index].Cells[3].Value.ToString();
                    txtqty.Text = DataGridCommon.Rows[Index].Cells[4].Value.ToString();
                    txtpono.Text = DataGridCommon.Rows[Index].Cells[4].Value.ToString();
                    txtjjno.Text = DataGridCommon.Rows[Index].Cells[5].Value.ToString();
                    txtaddnotes.Text = DataGridCommon.Rows[Index].Cells[6].Value.ToString();

                }
                else if (Genclass.type == 3)
                {

                    pono.Text = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                }

                grSearch.Visible = false;
                SelectId = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void button22_Click(object sender, EventArgs e)
        {
            GeneralParametrs.MenyKey = 7;
            Frmpar contc = new Frmpar();
            contc.Show();
            contc.FormBorderStyle = FormBorderStyle.None;
            contc.StartPosition = FormStartPosition.Manual;
            contc.Location = new System.Drawing.Point(200, 80);
        }

        private void DataGridCommon_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SelectId = 1;
                int Index = DataGridCommon.SelectedCells[0].RowIndex;
                if (Genclass.type == 1)
                {
                    txtname.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtpuid.Text = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                }
                else
                {
                    txtititd.Text = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    txtitem.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtrecqty.Text = DataGridCommon.Rows[Index].Cells[2].Value.ToString();
                    txtpoid.Text = DataGridCommon.Rows[Index].Cells[3].Value.ToString();
                    txtpono.Text = DataGridCommon.Rows[Index].Cells[4].Value.ToString();
                    txtqty.Text = DataGridCommon.Rows[Index].Cells[4].Value.ToString();
                    txtjjno.Text = DataGridCommon.Rows[Index].Cells[5].Value.ToString();
                    txtaddnotes.Text = DataGridCommon.Rows[Index].Cells[6].Value.ToString();

                }


                grSearch.Visible = false;
                SelectId = 0;
            }
        }

        private void DataGridCommon_DoubleClick(object sender, EventArgs e)
        {

            SelectId = 1;
            int Index = DataGridCommon.SelectedCells[0].RowIndex;
            if (Genclass.type == 1)
            {
                txtname.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                txtpuid.Text = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
            }
            else if (Genclass.type == 2)
            {
                txtititd.Text = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                txtitem.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                txtrecqty.Text = DataGridCommon.Rows[Index].Cells[2].Value.ToString();
                txtpoid.Text = DataGridCommon.Rows[Index].Cells[3].Value.ToString();
                txtqty.Text = DataGridCommon.Rows[Index].Cells[4].Value.ToString();
                txtpono.Text = DataGridCommon.Rows[Index].Cells[4].Value.ToString();
                txtjjno.Text = DataGridCommon.Rows[Index].Cells[5].Value.ToString();
                txtaddnotes.Text = DataGridCommon.Rows[Index].Cells[6].Value.ToString();

            }
            else if (Genclass.type == 3)
            {

                pono.Text = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
            }


            grSearch.Visible = false;
            SelectId = 0;
        }

        private void DataGridCommon_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void txtrejqty_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtbags_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                e.Handled = true;
                SendKeys.Send("{TAB}");
            }
        }

        protected void Loadser()
        {

            Dataserial.DataSource = null;
            Dataserial.AutoGenerateColumns = false;
            Dataserial.ColumnCount = 6;
            Dataserial.Columns[0].Name = "itemuid";
            Dataserial.Columns[0].HeaderText = "itemuid";
            Dataserial.Columns[0].Visible = false;
            Dataserial.Columns[1].Name = "Qty";
            Dataserial.Columns[1].HeaderText = "Qty";
            Dataserial.Columns[1].Visible = false;
            Dataserial.Columns[2].Name = "Serialno";
            Dataserial.Columns[2].HeaderText = "Serialno";
            Dataserial.Columns[2].Width = 230;

            Dataserial.Columns[3].Name = "LossWt";
            Dataserial.Columns[3].HeaderText = "LossWt";
            Dataserial.Columns[3].Width = 100;
            Dataserial.Columns[4].Name = "TareWt";
            Dataserial.Columns[4].HeaderText = "TareWt";
            Dataserial.Columns[4].Width = 100;
            Dataserial.Columns[5].Name = "Netwt";
            Dataserial.Columns[5].HeaderText = "Netwt";
            Dataserial.Columns[5].Width = 100;
        }
        private void loadserial()
        {
            conn.Close();
            conn.Open();


            string qur = "select  b.UId,b.itemname from  transactionsp a inner join transactionsplist f on a.uid=f.transactionspuid    inner join itemm b  on f.itemuid=b.UId  where  a.Active=1   and a.docno='" + txtgrn.Text + "' and a.uid=" + txtgrnid.Text + "";

            SqlCommand cmd = new SqlCommand(qur, conn);
            SqlDataAdapter apt = new SqlDataAdapter(cmd);
            DataTable tab = new DataTable();
            apt.Fill(tab);
            cboserial.DataSource = null;
            cboserial.DataSource = tab;
            cboserial.DisplayMember = "itemname";
            cboserial.ValueMember = "uid";
            //cboserial.SelectedIndex = -1;
            conn.Close();






        }
        private void button10_Click(object sender, EventArgs e)
        {
            serialno.Visible = true;
            Genclass.ClearTextBox(this, serialno);
            Dataserial.RowHeadersVisible = false;
            Dataserial.Refresh();
            Dataserial.DataSource = null;
            Dataserial.Rows.Clear();
            Loadser();
            loadserial();
            if (mode == 2)
            {
                dataload();
            }

        }
        private void dataload()
        {

            if (serialno.Visible == true && cboserial.ValueMember != string.Empty && cboserial.SelectedValue != null)
            {

                Dataserial.Refresh();
                Dataserial.DataSource = null;
                Dataserial.Rows.Clear();
                Loadser();

                Genclass.strsql = "select  b.itemuid,c.weight as qty,serialno,losswt,tarewt,netwt from transactionsp  a inner join  transactionsplist b on a.uid=b.transactionspuid  inner join GRNSERIALNOGEN c on a.uid=c.listid and b.itemuid=c.itemuid  where b.itemuid=" + cboserial.SelectedValue + "  and a.docno='" + txtgrn.Text + "' and a.uid=" + txtgrnid.Text + "  ";
                Genclass.cmd = new SqlCommand(Genclass.strsql, conn);


                SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
                DataTable tap1 = new DataTable();
                aptr1.Fill(tap1);

                sum1 = 0;



                for (m = 0; m < tap1.Rows.Count; m++)
                {
                    var index = Dataserial.Rows.Add();

                    Dataserial.Rows[index].Cells[0].Value = tap1.Rows[m]["itemuid"].ToString();

                    Dataserial.Rows[index].Cells[1].Value = tap1.Rows[m]["QTY"].ToString();

                    Dataserial.Rows[index].Cells[2].Value = tap1.Rows[m]["serialno"].ToString();
                    Dataserial.Rows[index].Cells[3].Value = tap1.Rows[m]["losswt"].ToString();
                    Dataserial.Rows[index].Cells[4].Value = tap1.Rows[m]["tarewt"].ToString();
                    Dataserial.Rows[index].Cells[5].Value = tap1.Rows[m]["netwt"].ToString();


                }
                Loadser();
            }
        }
        private void txtnar_TextChanged(object sender, EventArgs e)
        {

        }

        private void Genpan_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button20_Click(object sender, EventArgs e)
        {
            serialno.Visible = false;
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bs.Filter = string.Format("docno LIKE '%{0}%' ", pono.Text);



                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void label19_Click(object sender, EventArgs e)
        {

        }

        private void pono_Click(object sender, EventArgs e)
        {
            Genclass.type = 3;
            DataTable dt = getParty();
            bs.DataSource = dt;
            FillGrid2(dt, 1);
            Point loc = FindLocation(pono);
            grSearch.Location = new Point(loc.X, loc.Y + 20);
            button22.Visible = false;
            grSearch.Visible = true;
            grSearch.Text = "Items Search";
        }

        private void pono_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.F10)
                {
                    grSearch.Visible = false;
                }
                else if (e.KeyCode == Keys.F2)
                {
                    SelectId = 1;
                    int Index = DataGridCommon.SelectedCells[0].RowIndex;

                    pono.Text = DataGridCommon.Rows[Index].Cells[0].Value.ToString();


                    grSearch.Visible = false;
                    SelectId = 0;
                }
                else if (e.KeyCode == Keys.Escape)
                {
                    grSearch.Visible = false;
                }
                else if (e.KeyValue == 40)
                {
                    DataGridCommon.Select();
                }
                else if (e.KeyCode == Keys.Enter)
                {
                    pono_Click(sender, e);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

        }

        private void HFIT_KeyDown(object sender, KeyEventArgs e)
        {
            int index = HFIT.SelectedCells[0].RowIndex;

            conn.Close();
            conn.Open();
            {

                qur.CommandText = "delete from GRNTemp where refuid=" + HFIT.CurrentRow.Cells[6].Value.ToString() + "";
                qur.ExecuteNonQuery();



                HFIT.Rows.RemoveAt(index);
                HFIT.ClearSelection();

            }
        }
    }
}