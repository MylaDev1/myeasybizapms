﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Drawing.Imaging;
using System.Configuration;
using System.Data.SqlClient;
using System.Diagnostics;
using excel = Microsoft.Office.Interop.Excel;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;

namespace MyEasyBizAPMS
{
    public partial class FrmRateApprove : Form
    {
        private CrystalDecisions.Windows.Forms.CrystalReportViewer Cryview;

        ReportDocument doc = new ReportDocument();
        public FrmRateApprove()
        {
            this.BackColor = Color.White; ;
            InitializeComponent();
        }
        string uid = "";
        int mode = 0;
        string tpuid = "";
        SqlConnection conn = new SqlConnection(GeneralParameters.ConnectionString);
        SqlCommand qur = new SqlCommand();
        private DataRow doc1;
        DataTable Docno = new DataTable();
        BindingSource bs = new BindingSource();

        DataTable Docno1 = new DataTable();
        BindingSource bsc = new BindingSource();
        BindingSource bsp = new BindingSource();
        BindingSource bsserial = new BindingSource();
        BindingSource IN = new BindingSource();
        BindingSource OUT = new BindingSource();
        BindingSource bsFabric = new BindingSource();
        SQLDBHelper db = new SQLDBHelper();
   
        int Fillid;
        BindingSource bsParty = new BindingSource();
        string str1key;
        public int SelectId = 0;
        private void txtdcno_TextChanged(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            mode = 1;
            Genpan.Visible = false;

            panadd.Visible = false;
            Genclass.h = 0;
            Genclass.ClearTextBox(this, Editpnl);
            Genclass.STR = "";
            Genclass.Gendocno();
            txtgrn.Text = Genclass.ST;
            Editpnl.Visible = true;
            lkppnl.Visible = false;
            Genclass.sum1 = 0;
            Genclass.sum2 = 0;
            HFIT.Refresh();
            HFIT.DataSource = null;
            HFIT.Rows.Clear();

            HFGP1.Refresh();
            HFGP1.DataSource = null;
            HFGP1.Rows.Clear();

            conn.Close();
            conn.Open();
            Docno.Clear();

            button13.Visible = false;
            if (Docno.Columns.Count == 0)
            {
                Docno.Columns.Add("uid");
                Docno.Columns.Add("Docno");
            }
            conn.Close();
            conn.Open();


        }

        private void butedit_Click(object sender, EventArgs e)
        {



            mode = 2;
            panadd.Visible = false;
            Genpan.Visible = false;
            Editpnl.Visible = true;
            lkppnl.Visible = false;
            conn.Close();
            conn.Open();

            int i = HFGP.SelectedCells[0].RowIndex;
            uid = HFGP.Rows[i].Cells[0].Value.ToString();
            txtgrnid.Text = HFGP.Rows[i].Cells[0].Value.ToString();
            txtgrn.Text = HFGP.Rows[i].Cells[1].Value.ToString();
            dtpgrndt.Text = HFGP.Rows[i].Cells[2].Value.ToString();
            txtwo.Text = HFGP.Rows[i].Cells[3].Value.ToString();
            textBox2.Text = HFGP.Rows[i].Cells[5].Value.ToString();
            txtnar.Text = HFGP.Rows[i].Cells[4].Value.ToString();


            //Chkedtact.Checked = true;
            HFIT.Refresh();
            HFIT.DataSource = null;
            HFIT.Rows.Clear();

 

            Titlep();
            load();

        }
        private void load()
        {
            Genclass.strsql = "SP_GETRATEAPPEDIT " + txtgrnid.Text + " ";
            Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
            SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
            DataTable tap1 = new DataTable();
            aptr1.Fill(tap1);

            Genclass.sum1 = 0;


            for (int k = 0; k < tap1.Rows.Count; k++)
            {
                var index = HFIT.Rows.Add();
       
                HFIT.Rows[index].Cells[0].Value = tap1.Rows[k]["HEADID"].ToString();

                HFIT.Rows[index].Cells[1].Value = tap1.Rows[k]["WORKID"].ToString();


                HFIT.Rows[index].Cells[2].Value = tap1.Rows[k]["ITEM"].ToString();

                HFIT.Rows[index].Cells[3].Value = tap1.Rows[k]["ITMID"].ToString();

                HFIT.Rows[index].Cells[4].Value = tap1.Rows[k]["COLUR"].ToString();


                HFIT.Rows[index].Cells[5].Value = tap1.Rows[k]["PROCESS"].ToString();
                HFIT.Rows[index].Cells[6].Value = tap1.Rows[k]["RATE"].ToString();
                HFIT.Rows[index].Cells[7].Value = tap1.Rows[k]["UID"].ToString();
                HFIT.Rows[index].Cells[8].Value = tap1.Rows[k]["name"].ToString();
                HFIT.Rows[index].Cells[9].Value = tap1.Rows[k]["suppid"].ToString();
                HFIT.Rows[index].Cells[10].Value = tap1.Rows[k]["qty"].ToString();
                HFIT.Rows[index].Cells[11].Value = tap1.Rows[k]["refid"].ToString();
                HFIT.Rows[index].Cells[12].Value = tap1.Rows[k]["UOm"].ToString();
            }
        }

        private void FrmRateApprove_Load(object sender, EventArgs e)
        {
            qur.Connection = conn;
            //Left = (MdiParent.ClientRectangle.Width - Width) / 3;
            //Top = (MdiParent.ClientRectangle.Height - Height) / 3;
            Genclass.buttonstyleform(this);
            Genclass.buttonstylepanel(panadd);
            panadd.Visible = true;
            Genpan.Visible = true;
            Editpnl.Visible = false;

            this.HFGP.DefaultCellStyle.Font = new Font("calibri", 10);
            this.HFGP.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 10, FontStyle.Bold);
            this.HFIT.DefaultCellStyle.Font = new Font("calibri", 10);
            this.HFIT.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 10, FontStyle.Bold);

            this.HFGP1.DefaultCellStyle.Font = new Font("calibri", 10);
            this.HFGP1.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 10, FontStyle.Bold);

            this.HFGP2.DefaultCellStyle.Font = new Font("calibri", 10);
            this.HFGP2.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 10, FontStyle.Bold);

            HFIT.RowHeadersVisible = false;
            HFGP.RowHeadersVisible = false;
            HFGP2.RowHeadersVisible = false;
            HFGP1.RowHeadersVisible = false;




            HFGP2.EnableHeadersVisualStyles = false;
            HFGP2.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.GradientActiveCaption;

            HFIT.EnableHeadersVisualStyles = false;
            HFIT.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.GradientActiveCaption;
            HFGP1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            HFGP1.EnableHeadersVisualStyles = false;
            HFGP1.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.GradientActiveCaption;

            HFGP.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            HFGP.EnableHeadersVisualStyles = false;
            HFGP.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.GradientActiveCaption;

            Genclass.Dtype = 10;

            dtpfnt.Format = DateTimePickerFormat.Custom;
            dtpfnt.CustomFormat = "dd/MM/yyyy";
            dtpgrndt.Format = DateTimePickerFormat.Custom;
            dtpgrndt.CustomFormat = "dd/MM/yyyy";
            //////dtpsupp.Format = DateTimePickerFormat.Custom;
            //////dtpsupp.CustomFormat = "dd/MM/yyyy";
            chkact.Checked = true;


            LoadGetJobCard(1);
            Titlep();
            //type1();
        }
        public void type1()
        {
            if (txtwo.Text != "")
            {


                Genclass.strsql = "select * from orderm where docno ='" + txtwo.Text + "'  ";
                Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
                DataTable tap1 = new DataTable();
                aptr1.Fill(tap1);
                if (tap1.Rows.Count > 0)
                {

                    string Query = "Select Distinct Colour from OrderMFabricBomColour where ordermuid = '" + tap1.Rows[0]["uid"].ToString() + "'";
                    DataTable data = db.GetDataWithoutParam(CommandType.Text, Query, conn);
                    DataRow dataRow = data.NewRow();
                    dataRow["Colour"] = "Blank";
                    data.Rows.Add(dataRow);
                    txtcolor.DisplayMember = "Colour";
                    txtcolor.ValueMember = "Colour";
                    txtcolor.DataSource = data;
                }
            }

        }
        private void Titlep()
        {
            HFIT.AutoGenerateColumns = false;
            HFIT.Refresh();
            HFIT.DataSource = null;
            HFIT.Rows.Clear();
            HFIT.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            HFIT.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            HFIT.ColumnCount = 13;
            HFIT.Columns[0].Name = "headid";
            HFIT.Columns[1].Name = "workid";
            HFIT.Columns[2].Name = "Item";
            HFIT.Columns[3].Name = "itmid";
            HFIT.Columns[4].Name = "Colour";
            HFIT.Columns[5].Name = "Process";
            HFIT.Columns[6].Name = "Rate";
            HFIT.Columns[7].Name = "uid";
            HFIT.Columns[8].Name = "SupplierName";
            HFIT.Columns[9].Name = "suppid";
            HFIT.Columns[10].Name = "Qty";
            HFIT.Columns[11].Name = "refid";
            HFIT.Columns[12].Name = "uom";
            //HFIT.EditMode = DataGridViewEditMode.EditOnKeystroke;




            HFIT.Columns[2].Width = 300;

            HFIT.Columns[4].Width = 70;
            HFIT.Columns[5].Width = 100;
            HFIT.Columns[6].Width = 70;
            HFIT.Columns[8].Width = 150;
            HFIT.Columns[9].Visible = false;
            HFIT.Columns[10].Width = 70;
            HFIT.Columns[1].Visible = false;
            HFIT.Columns[0].Visible = false;
            HFIT.Columns[3].Visible = false;
            HFIT.Columns[7].Visible = false;
            HFIT.Columns[11].Visible = false;
            HFIT.Columns[12].Visible = false;
        }
        protected DataTable LoadGetJobCard(int tag)
        {

            int SP;
            chkact.Checked = true;
            if (chkact.Checked == true)
            {
                SP = 1;

            }
            else
            {
                SP = 0;

            }


            DataTable dt = new DataTable();
            try
            {

                SqlParameter[] para = {
                    new SqlParameter("@active",SP),



                };

                dt = db.GetData(CommandType.StoredProcedure, "SP_GETRATEAPPROVAL", para);


                LoadDataTable(dt);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        protected void LoadDataTable(DataTable dt)
        {
            try
            {
                HFGP.DataSource = null;
                HFGP.AutoGenerateColumns = false;
                HFGP.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
                HFGP.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
                HFGP.ColumnCount = 6;
                HFGP.Columns[0].Name = "Uid";
                HFGP.Columns[0].HeaderText = "Uid";
                HFGP.Columns[0].DataPropertyName = "Uid";

                HFGP.Columns[1].Name = "Docno";
                HFGP.Columns[1].HeaderText = "Docno";
                HFGP.Columns[1].DataPropertyName = "Docno";

                HFGP.Columns[2].Name = "Docdate";
                HFGP.Columns[2].HeaderText = "Docdate";
                HFGP.Columns[2].DataPropertyName = "Docdate";

                HFGP.Columns[3].Name = "Socno";
                HFGP.Columns[3].HeaderText = "Socno";
                HFGP.Columns[3].DataPropertyName = "Socno";

                HFGP.Columns[4].Name = "Narration";
                HFGP.Columns[4].HeaderText = "Narration";
                HFGP.Columns[4].DataPropertyName = "Narration";

                HFGP.Columns[5].Name = "WorkOrderNo";
                HFGP.Columns[5].HeaderText = "WorkOrderNo";
                HFGP.Columns[5].DataPropertyName = "WorkOrderNo";

              




                bs.DataSource = dt;

                HFGP.DataSource = bs;


                HFGP.Columns[0].Visible = false;
       
                HFGP.Columns[1].Width = 100;
                HFGP.Columns[2].Width = 100;
                HFGP.Columns[3].Width = 280;
                HFGP.Columns[4].Width = 100;
                HFGP.Columns[5].Width = 180;




            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void txtwo_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bsFabric.Filter = string.Format("Socno LIKE '%{0}%' ", txtwo.Text);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtwo_Click(object sender, EventArgs e)
        {
            Genclass.type = 1;

            DataTable dt = getParty();
            bsFabric.DataSource = dt;
            FillGrid1(dt, 1);
            Point loc = FindLocation(txtwo);
            grNewSearch.Location = new Point(loc.X, loc.Y + 20);
            grNewSearch.Visible = true;
        }
        private Point FindLocation(Control ctrl)
        {
            if (ctrl.Parent is Form)
                return ctrl.Location;
            else
            {
                Point p = FindLocation(ctrl.Parent);
                p.X += ctrl.Location.X;
                p.Y += ctrl.Location.Y;
                return p;
            }
        }

        protected void FillGrid1(DataTable dt, int Fillid)
        {
            try
            {
                DataGridCommonNew.DataSource = null;
                DataGridCommonNew.AutoGenerateColumns = false;
                DataGridCommonNew.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
                DataGridCommonNew.DefaultCellStyle.WrapMode = DataGridViewTriState.True;

                DataGridCommonNew.ColumnCount = 3;
                DataGridCommonNew.Columns[0].Name = "Uid";
                DataGridCommonNew.Columns[0].HeaderText = "Uid";
                DataGridCommonNew.Columns[0].DataPropertyName = "Uid";
                DataGridCommonNew.Columns[1].Name = "Socno";
                DataGridCommonNew.Columns[1].HeaderText = "Socno";
                DataGridCommonNew.Columns[1].DataPropertyName = "Socno";
                DataGridCommonNew.Columns[1].Width = 100;
                DataGridCommonNew.Columns[2].Name = "WORKORDER";
                DataGridCommonNew.Columns[2].HeaderText = "WORKORDER";
                DataGridCommonNew.Columns[2].DataPropertyName = "WORKORDER";
                DataGridCommonNew.Columns[2].Width = 200;
                DataGridCommonNew.DataSource = bsFabric;
                DataGridCommonNew.Columns[0].Visible = false;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        protected void FillGrid3(DataTable dt, int Fillid)
        {
            try
            {
                DataGridCommonNew.DataSource = null;
                DataGridCommonNew.AutoGenerateColumns = false;
                DataGridCommonNew.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
                DataGridCommonNew.DefaultCellStyle.WrapMode = DataGridViewTriState.True;

                DataGridCommonNew.ColumnCount = 2;
                DataGridCommonNew.Columns[0].Name = "Uid";
                DataGridCommonNew.Columns[0].HeaderText = "Uid";
                DataGridCommonNew.Columns[0].DataPropertyName = "Uid";
                DataGridCommonNew.Columns[1].Name = "Name";
                DataGridCommonNew.Columns[1].HeaderText = "Name";
                DataGridCommonNew.Columns[1].DataPropertyName = "Name";
                DataGridCommonNew.Columns[1].Width = 200;
               
                DataGridCommonNew.DataSource = bs;
                DataGridCommonNew.Columns[0].Visible = false;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        protected DataTable getParty()
        {
            DataTable dt = new DataTable();
            try
            {


                if (Genclass.type == 1)
                {
                    dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "SP_GETRATEAPPREFNO", conn);
                    bsFabric.DataSource = dt;
                }
                else if (Genclass.type == 2)
                {

                    SqlParameter[] para = { new SqlParameter("@TYPE", Convert.ToString(cbopono.Text)), new SqlParameter("@socno", Convert.ToString(txtwo.Text)) };

             
                    dt = db.getDataWithParameterCmdType(CommandType.StoredProcedure, "SP_GETRATEAPPREFNOITEM", para);
                    bsc.DataSource = dt;

                }
                else if (Genclass.type == 5)
                {

                    SqlParameter[] para = { new SqlParameter("@TYPE", Convert.ToString(cbopono.Text)), new SqlParameter("@socno", Convert.ToString(txtwo.Text)), new SqlParameter("@process", Convert.ToString(cbopro.Text)) };


                    dt = db.getDataWithParameterCmdType(CommandType.StoredProcedure, "SP_GETRATEAPPREFNOITEMProcessselect", para);
                    bsc.DataSource = dt;

                }
                else if (Genclass.type == 3)
                {
                    dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "SP_GETPARTYSUPPLIERProcessor", conn);
                    bs.DataSource = dt;

                }
                else if (Genclass.type == 4)
                {


                    SqlParameter[] para = { new SqlParameter("@socno", Convert.ToString(txtwo.Text)) };
                    
             
                    
                    dt = db.getDataWithParameterCmdType(CommandType.StoredProcedure, "SP_GETRATEAPPREFNOprocess", para);
                    bsp.DataSource = dt;

                }


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            return dt;
        }

        private void btnSelect_Click(object sender, EventArgs e)
        {
            try
            {
                SelectId = 1;
                if (Genclass.type == 1)
                {
                    int Index = DataGridCommonNew.SelectedCells[0].RowIndex;
                    txtwo.Text = DataGridCommonNew.Rows[Index].Cells[1].Value.ToString();
                    txtwo.Tag = DataGridCommonNew.Rows[Index].Cells[0].Value.ToString();
                    textBox2.Text = DataGridCommonNew.Rows[Index].Cells[2].Value.ToString();
                    
                }
                else if (Genclass.type == 3)
                {
                    int Index = DataGridCommonNew.SelectedCells[0].RowIndex;
                    textBox3.Text = DataGridCommonNew.Rows[Index].Cells[1].Value.ToString();
                    textBox3.Tag = DataGridCommonNew.Rows[Index].Cells[0].Value.ToString();

                }

                SelectId = 0;
                grNewSearch.Visible = false;
                SelectId = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }

        private void DataGridCommonNew_KeyDown(object sender, KeyEventArgs e)
        {

        }

        private void DataGridCommonNew_Click(object sender, EventArgs e)
        {
            SelectId = 1;
            if (Genclass.type == 1)
            {
                int Index = DataGridCommonNew.SelectedCells[0].RowIndex;
                txtwo.Text = DataGridCommonNew.Rows[Index].Cells[1].Value.ToString();
                txtwo.Tag = DataGridCommonNew.Rows[Index].Cells[0].Value.ToString();
                textBox2.Text = DataGridCommonNew.Rows[Index].Cells[2].Value.ToString();
               
            }
            else if (Genclass.type == 3)
            {
                int Index = DataGridCommonNew.SelectedCells[0].RowIndex;
                textBox3.Text = DataGridCommonNew.Rows[Index].Cells[1].Value.ToString();
                textBox3.Tag = DataGridCommonNew.Rows[Index].Cells[0].Value.ToString();

            }

            SelectId = 0;
            grNewSearch.Visible = false;
            SelectId = 0;

            SelectId = 0;
            grNewSearch.Visible = false;
            SelectId = 0;
        }

        private void txtdcqty_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bsc.Filter = string.Format("Itemname LIKE '%{0}%' ", txtdcqty.Text);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtdcqty_Click(object sender, EventArgs e)
        {
            Genclass.type = 2;

            DataTable dt = getParty();
            bsc.DataSource = dt;
            FillGrid2(dt, 1);
            Point loc = FindLocation(txtdcqty);
            lkppnl.Location = new Point(loc.X, loc.Y + 20);
            lkppnl.Visible = true;
        }
        protected void FillGrid2(DataTable dt, int FillId)
        {
            try
            {
                HFGP2.DataSource = null;
                HFGP2.AutoGenerateColumns = false;
                HFGP2.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
                HFGP2.DefaultCellStyle.WrapMode = DataGridViewTriState.True;

                HFGP2.ColumnCount = 8;
                HFGP2.Columns[0].Name = "ITEMID";
                HFGP2.Columns[0].HeaderText = "ITEMID";
                HFGP2.Columns[0].DataPropertyName = "ITEMID";

                HFGP2.Columns[1].Name = "Itemname";
                HFGP2.Columns[1].HeaderText = "ItemName";
                HFGP2.Columns[1].DataPropertyName = "Itemname";
                HFGP2.Columns[1].Width = 300;


                HFGP2.Columns[2].Name = "COLOUR";
                HFGP2.Columns[2].HeaderText = "COLOUR";
                HFGP2.Columns[2].DataPropertyName = "COLOUR";
                HFGP2.Columns[2].Width = 100;


                HFGP2.Columns[3].Name = "PROCESS";
                HFGP2.Columns[3].HeaderText = "PROCESS";
                HFGP2.Columns[3].DataPropertyName = "PROCESS";
                HFGP2.Columns[3].Visible = false;


                HFGP2.Columns[4].Name = "UID";
                HFGP2.Columns[4].HeaderText = "UID";
                HFGP2.Columns[4].DataPropertyName = "UID";


                HFGP2.Columns[5].Name = "qty";
                HFGP2.Columns[5].HeaderText = "qty";
                HFGP2.Columns[5].DataPropertyName = "qty";
                HFGP2.Columns[5].Width = 100;
                HFGP2.Columns[6].Name = "UOM";
                HFGP2.Columns[6].HeaderText = "UOM";
                HFGP2.Columns[6].DataPropertyName = "UOM";


                HFGP2.Columns[7].Name = "seqno";
                HFGP2.Columns[7].HeaderText = "seqno";
                HFGP2.Columns[7].DataPropertyName = "seqno";

                HFGP2.DataSource = bsc;
                HFGP2.Columns[0].Visible = false;
                HFGP2.Columns[4].Visible = false;
                HFGP2.Columns[6].Visible = false;
                HFGP2.Columns[7].Visible = false;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        protected void FillGrid4(DataTable dt, int FillId)
        {
            try
            {
                HFGP2.DataSource = null;
                HFGP2.AutoGenerateColumns = false;


                HFGP2.ColumnCount = 8;


                HFGP2.Columns[0].Name = "ITEMID";
                HFGP2.Columns[0].HeaderText = "ITEMID";
                HFGP2.Columns[0].DataPropertyName = "ITEMID";
                HFGP2.Columns[1].Name = "PROCESS";
                HFGP2.Columns[1].HeaderText = "PROCESS";
                HFGP2.Columns[1].DataPropertyName = "PROCESS";
                HFGP2.Columns[1].Width = 100;
           

                HFGP2.Columns[2].Name = "Itemname";
                HFGP2.Columns[2].HeaderText = "ItemName";
                HFGP2.Columns[2].DataPropertyName = "Itemname";
                HFGP2.Columns[2].Width = 300;


                HFGP2.Columns[3].Name = "COLOUR";
                HFGP2.Columns[3].HeaderText = "COLOUR";
                HFGP2.Columns[3].DataPropertyName = "COLOUR";
                HFGP2.Columns[3].Width = 100;


                HFGP2.Columns[4].Name = "UID";
                HFGP2.Columns[4].HeaderText = "UID";
                HFGP2.Columns[4].DataPropertyName = "UID";

                HFGP2.Columns[5].Name = "qty";
                HFGP2.Columns[5].HeaderText = "qty";
                HFGP2.Columns[5].DataPropertyName = "qty";
                HFGP2.Columns[5].Width = 100;

                HFGP2.Columns[6].Name = "UOM";
                HFGP2.Columns[6].HeaderText = "UOM";
                HFGP2.Columns[6].DataPropertyName = "UOM";

                HFGP2.Columns[7].Name = "seqno";
                HFGP2.Columns[7].HeaderText = "seqno";
                HFGP2.Columns[7].DataPropertyName = "seqno";

                HFGP2.DataSource = bsp;
                HFGP2.Columns[0].Visible = false;
                HFGP2.Columns[4].Visible = false;
                HFGP2.Columns[6].Visible = false;
                HFGP2.Columns[7].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        private void txtdcqty_KeyDown(object sender, KeyEventArgs e)
        {
           
        }

        private void button10_Click(object sender, EventArgs e)
        {
            try
            {
                SelectId = 1;

                if (Genclass.type == 2)
                {
                    int Index = HFGP2.SelectedCells[0].RowIndex;

                    txtdcqty.Text = HFGP2.Rows[Index].Cells[1].Value.ToString();
                    txtdcqty.Tag = HFGP2.Rows[Index].Cells[0].Value.ToString();
                    txtprocess.Text = HFGP2.Rows[Index].Cells[3].Value.ToString();
                    txtcolor.Text = HFGP2.Rows[Index].Cells[2].Value.ToString();
                    txtqty.Tag = HFGP2.Rows[Index].Cells[4].Value.ToString();
                    txtqty.Text = HFGP2.Rows[Index].Cells[5].Value.ToString();
                    txtitem.Text = HFGP2.Rows[Index].Cells[6].Value.ToString();
                    Genclass.sum1 = Convert.ToDouble(HFGP2.Rows[Index].Cells[7].Value.ToString());
                    txtcolor.Items.Add(HFGP2.Rows[Index].Cells[2].Value.ToString());
                    txtcolor.Text = HFGP2.Rows[Index].Cells[2].Value.ToString();

                }
                else if (Genclass.type == 5)

                {

                    int Index = HFGP2.SelectedCells[0].RowIndex;

                    txtdcqty.Text = HFGP2.Rows[Index].Cells[1].Value.ToString();
                    txtdcqty.Tag = HFGP2.Rows[Index].Cells[0].Value.ToString();
                    txtprocess.Text = HFGP2.Rows[Index].Cells[3].Value.ToString();
                    txtcolor.Text = HFGP2.Rows[Index].Cells[2].Value.ToString();
                    txtqty.Tag = HFGP2.Rows[Index].Cells[4].Value.ToString();

                    txtqty.Text = HFGP2.Rows[Index].Cells[5].Value.ToString();
                    txtitem.Text = HFGP2.Rows[Index].Cells[6].Value.ToString();
                    Genclass.sum1 = Convert.ToDouble(HFGP2.Rows[Index].Cells[7].Value.ToString());
                    txtcolor.Items.Add(HFGP2.Rows[Index].Cells[2].Value.ToString());
                    txtcolor.Text = HFGP2.Rows[Index].Cells[2].Value.ToString();

                }
                else
                {
                    int Index = HFGP2.SelectedCells[0].RowIndex;
                    txtdcqty.Text = HFGP2.Rows[Index].Cells[2].Value.ToString();
                    txtdcqty.Tag = HFGP2.Rows[Index].Cells[0].Value.ToString();
                    txtprocess.Text = HFGP2.Rows[Index].Cells[1].Value.ToString();
                    txtcolor.Text = HFGP2.Rows[Index].Cells[3].Value.ToString();
                    txtqty.Tag = HFGP2.Rows[Index].Cells[4].Value.ToString();

                    txtqty.Text = HFGP2.Rows[Index].Cells[5].Value.ToString();
                    txtitem.Text = HFGP2.Rows[Index].Cells[6].Value.ToString();
                    Genclass.sum1 = Convert.ToDouble(HFGP2.Rows[Index].Cells[7].Value.ToString());
                    txtcolor.Items.Add(HFGP2.Rows[Index].Cells[3].Value.ToString() );
                    txtcolor.Text = HFGP2.Rows[Index].Cells[3].Value.ToString();
                }
               


                lkppnl.Visible = false;
                SelectId = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void HFGP2_Click(object sender, EventArgs e)
        {
            SelectId = 1;
            if (Genclass.type == 2)
            {
                int Index = HFGP2.SelectedCells[0].RowIndex;

                txtdcqty.Text = HFGP2.Rows[Index].Cells[1].Value.ToString();
                txtdcqty.Tag = HFGP2.Rows[Index].Cells[0].Value.ToString();
                txtprocess.Text = HFGP2.Rows[Index].Cells[3].Value.ToString();
                txtcolor.Text = HFGP2.Rows[Index].Cells[2].Value.ToString();
                txtqty.Tag = HFGP2.Rows[Index].Cells[4].Value.ToString();
               
                txtqty.Text = HFGP2.Rows[Index].Cells[5].Value.ToString();
                txtitem.Text = HFGP2.Rows[Index].Cells[6].Value.ToString();
                Genclass.sum1 = Convert.ToDouble(HFGP2.Rows[Index].Cells[7].Value.ToString());
                txtcolor.Items.Add(HFGP2.Rows[Index].Cells[2].Value.ToString());
                txtcolor.Text = HFGP2.Rows[Index].Cells[2].Value.ToString();
            }
            else if (Genclass.type == 5)

            {
               
                    int Index = HFGP2.SelectedCells[0].RowIndex;

                    txtdcqty.Text = HFGP2.Rows[Index].Cells[1].Value.ToString();
                    txtdcqty.Tag = HFGP2.Rows[Index].Cells[0].Value.ToString();
                    txtprocess.Text = HFGP2.Rows[Index].Cells[3].Value.ToString();
                    txtcolor.Text = HFGP2.Rows[Index].Cells[2].Value.ToString();
                    txtqty.Tag = HFGP2.Rows[Index].Cells[4].Value.ToString();

                    txtqty.Text = HFGP2.Rows[Index].Cells[5].Value.ToString();
                    txtitem.Text = HFGP2.Rows[Index].Cells[6].Value.ToString();
                    Genclass.sum1 = Convert.ToDouble(HFGP2.Rows[Index].Cells[7].Value.ToString());
                txtcolor.Items.Add(HFGP2.Rows[Index].Cells[2].Value.ToString());
                txtcolor.Text = HFGP2.Rows[Index].Cells[2].Value.ToString();


            }
            else
            {
                int Index = HFGP2.SelectedCells[0].RowIndex;
                txtdcqty.Text = HFGP2.Rows[Index].Cells[2].Value.ToString();
                txtdcqty.Tag = HFGP2.Rows[Index].Cells[0].Value.ToString();
                txtprocess.Text = HFGP2.Rows[Index].Cells[1].Value.ToString();
                txtcolor.Text = HFGP2.Rows[Index].Cells[3].Value.ToString();
                txtqty.Tag = HFGP2.Rows[Index].Cells[4].Value.ToString();
                txtqty.Text = HFGP2.Rows[Index].Cells[5].Value.ToString();
                txtitem.Text = HFGP2.Rows[Index].Cells[6].Value.ToString();
                Genclass.sum1 = Convert.ToDouble(HFGP2.Rows[Index].Cells[7].Value.ToString());
                txtcolor.Items.Add(HFGP2.Rows[Index].Cells[3].Value.ToString());
                txtcolor.Text = HFGP2.Rows[Index].Cells[3].Value.ToString();

            }




            lkppnl.Visible = false;
            SelectId = 0;
        }

        private void button11_Click(object sender, EventArgs e)
        {
            lkppnl.Visible = false;
        }

        private void button9_Click(object sender, EventArgs e)
        {
            grNewSearch.Visible = false;
        }

        private void btnsave_Click(object sender, EventArgs e)
        {
            conn.Close();
            conn.Open();
            Genclass.Dtype = 10;

            if (HFIT.RowCount <= 1)
            {
                MessageBox.Show("Enter the details");
                return;

            }

            //}
            if (mode == 1)
            {


                SqlParameter[] para ={
                       new SqlParameter("@UID","0"),
                    new SqlParameter("@DOCNO",txtgrn.Text),

                    new SqlParameter("@DOCDATE",Convert.ToDateTime(dtpgrndt.Text)),
                    new SqlParameter("@socno",txtwo.Text),
                    new SqlParameter("@workorderno",textBox2.Text),
                    new SqlParameter("@ref",txtnar.Text),
                     new SqlParameter("@active","1"),
                     new SqlParameter("@statud","Pending"),
                    new SqlParameter("@Returnid",SqlDbType.Int),
                    

                };

                para[8].Direction = ParameterDirection.Output;
                int ReturnId = db.ExecuteNonQuery(CommandType.StoredProcedure, "sp_RateAppM", para, conn, 8);
                Genclass.h = ReturnId;
                for (int i = 0; i < HFIT.RowCount - 1; i++)
                {

                    conn.Close();
                    conn.Open();

                    if (HFIT.Rows[i].Cells[3].Value.ToString() == "")

                    {
                        HFIT.Rows[i].Cells[3].Value = "0";

                    }
                    if (HFIT.Rows[i].Cells[6].Value.ToString() == "")

                    {
                        HFIT.Rows[i].Cells[6].Value = "0";

                    }

                    SqlParameter[] para1 ={

                    new SqlParameter("@headid",Genclass.h),
                    new SqlParameter("@workid",HFIT.Rows[i].Cells[1].Value),
                      new SqlParameter("@Item", HFIT.Rows[i].Cells[2].Value),
                    new SqlParameter("@itmid", HFIT.Rows[i].Cells[3].Value),
                    new SqlParameter("@colur", HFIT.Rows[i].Cells[4].Value ),
                    new SqlParameter("@Process",HFIT.Rows[i].Cells[5].Value ),
                    new SqlParameter("@Rate",HFIT.Rows[i].Cells[6].Value ),
                    new SqlParameter("@qty",HFIT.Rows[i].Cells[10].Value ),
                    new SqlParameter("@suppid",HFIT.Rows[i].Cells[9].Value ),
                   new SqlParameter("@refid",HFIT.Rows[i].Cells[11].Value ),
                             new SqlParameter("@status","Pending" ),
                                       new SqlParameter("@uom",HFIT.Rows[i].Cells[12].Value ),

                };

                    db.ExecuteNonQuery(CommandType.StoredProcedure, "sp_RateAppD", para1, conn);
                }



            }
            else

            {

                if (mode == 2)
                {
                    qur.CommandText = "delete from RateAppd where headid=" + Genclass.h + "";
                    qur.ExecuteNonQuery();
              

                }


                SqlParameter[] para ={

                    new SqlParameter("@DOCNO",txtgrn.Text),

                    new SqlParameter("@DOCDATE",Convert.ToDateTime(dtpgrndt.Text)),
                    new SqlParameter("@socno",txtwo.Text),
                    new SqlParameter("@workorderno",textBox2.Text),
                    new SqlParameter("@ref",txtnar.Text),
                        new SqlParameter("@UID",Genclass.h),
                              new SqlParameter("@active","1"),

                };
                db.ExecuteNonQuery(CommandType.StoredProcedure, "sp_RateAppMUpdate", para, conn);

                for (int i = 0; i < HFIT.RowCount - 1; i++)
                {

                    conn.Close();
                    conn.Open();

                    if (HFIT.Rows[i].Cells[3].Value.ToString() == "")

                    {
                        HFIT.Rows[i].Cells[3].Value = "0";

                    }
                    if (HFIT.Rows[i].Cells[6].Value.ToString() == "")

                    {
                        HFIT.Rows[i].Cells[6].Value = "0";

                    }

                    SqlParameter[] para1 ={

                    new SqlParameter("@headid",Genclass.h),
                    new SqlParameter("@workid",HFIT.Rows[i].Cells[1].Value),
                    new SqlParameter("@Item", HFIT.Rows[i].Cells[2].Value),
                    new SqlParameter("@itmid", HFIT.Rows[i].Cells[3].Value),
                    new SqlParameter("@colur", HFIT.Rows[i].Cells[4].Value ),
                    new SqlParameter("@Process",HFIT.Rows[i].Cells[5].Value ),
                    new SqlParameter("@Rate",HFIT.Rows[i].Cells[6].Value ),
                    new SqlParameter("@qty",HFIT.Rows[i].Cells[10].Value ),
                    new SqlParameter("@suppid",HFIT.Rows[i].Cells[9].Value ),
                    new SqlParameter("@refid",HFIT.Rows[i].Cells[11].Value ),
                    new SqlParameter("@status","Pending" ),
                    new SqlParameter("@uom",HFIT.Rows[i].Cells[12].Value ),


                };

                    db.ExecuteNonQuery(CommandType.StoredProcedure, "sp_RateAppD", para1, conn);
                }
            }


                if (mode == 1)
            {
                qur.CommandText = "update doctypem set lastno= lastno + 1 where doctypeid=" + Genclass.Dtype + "  and finyear='19-20'";
                qur.ExecuteNonQuery();


            }
            MessageBox.Show("Records Saved");
            Editpnl.Visible = false;
            Genpan.Visible = true;
            panadd.Visible = true;
            LoadGetJobCard(1);
        }

        private void btnaddrcan_Click(object sender, EventArgs e)
        {
            Editpnl.Visible = false;
            Genpan.Visible = true;
            panadd.Visible = true;
            LoadGetJobCard(1);
        }

        private void button1_Click(object sender, EventArgs e)
        {

            //Genclass.strsql = "select distinct seqno from processdet a inner join processdetlist  b  on a.uid=b.headid  inner join processm c on b.processid=c.uid WHERE  a.socno='" + txtwo.Text+"' and b.itemid="+ txtdcqty.Tag +"  and c.processname='"+ txtprocess.Text + "'";
            //Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
            //SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
            //DataTable tap1 = new DataTable();
            //aptr1.Fill(tap1);
            //if (tap1.Rows.Count>0)
            //{
            //    Genclass.sum1 = Convert.ToDouble(tap1.Rows[0]["Seqno"].ToString());

            //    if (Genclass.sum1 > 20)
            //    {
            //        if (txtcolor.Text == "Blank")
            //        {
            //            MessageBox.Show("Check the color");
            //            return;
            //            txtcolor.Focus();


            //        }


            //    }

            //}

            var index = HFIT.Rows.Add();

            HFIT.Rows[index].Cells[0].Value ="0";
            HFIT.Rows[index].Cells[1].Value = txtwo.Tag;
            HFIT.Rows[index].Cells[2].Value = txtdcqty.Text + " " + txtcolor.Text;
            HFIT.Rows[index].Cells[3].Value = txtdcqty.Tag;
            HFIT.Rows[index].Cells[4].Value = txtcolor.Text;
            HFIT.Rows[index].Cells[5].Value = txtprocess.Text;
            HFIT.Rows[index].Cells[6].Value = txtrate.Text;
            HFIT.Rows[index].Cells[7].Value = 0;
            HFIT.Rows[index].Cells[8].Value = textBox3.Text;
            HFIT.Rows[index].Cells[9].Value = textBox3.Tag;
            HFIT.Rows[index].Cells[10].Value = txtqty.Text;
            HFIT.Rows[index].Cells[11].Value = txtqty.Tag;
            HFIT.Rows[index].Cells[12].Value = txtitem.Text;

      
            txtcolor.Text = string.Empty;
           
            txtdcqty.Text = string.Empty;
            txtprocess.Text = string.Empty;
            txtrate.Text = string.Empty;


        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    if (textBox3.Text != "")
                    {
                        bs.Filter = string.Format("Name LIKE '%{0}%' ", textBox3.Text);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void textBox3_Click(object sender, EventArgs e)
        {
            Genclass.type = 3;

            DataTable dt = getParty();
            bs.DataSource = dt;
            FillGrid3(dt, 1);
            Point loc = FindLocation(textBox3);
            grNewSearch.Location = new Point(loc.X, loc.Y + 20);
            grNewSearch.Visible = true;
        }

        private void DataGridCommonNew_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void txtprocess_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bsp.Filter = string.Format("process LIKE '%{0}%' ", txtprocess.Text);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void cbopono_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbopono.Text != "")
            {
                Genclass.type = 2;
                DataTable dt = getParty();
                bsc.DataSource = dt;
                FillGrid2(dt, 1);
                Point loc = FindLocation(txtprocess);
                lkppnl.Location = new Point(loc.X, loc.Y + 20);
                lkppnl.Visible = true;
                if (cbopono.Text == "Fabric")
                {
                    conn.Close();
                    conn.Open();
                    string qur = "SP_GETRATEAPPREFNOprocess '" + txtwo.Text + "', '" + cbopono.Text + "' ";
                    SqlCommand cmd = new SqlCommand(qur, conn);
                    SqlDataAdapter apt = new SqlDataAdapter(cmd);
                    DataTable tab = new DataTable();
                    apt.Fill(tab);
                    cbopro.DataSource = null;
                    cbopro.DataSource = tab;
                    cbopro.DisplayMember = "PROCESS";
                    cbopro.ValueMember = "PROCESS";
                    //cbostate.SelectedIndex = -1;


                }
                else if (cbopono.Text == "Yarn")
                {
                    conn.Close();
                    conn.Open();
                    string qur = "SP_GETRATEAPPREFNOprocess '" + txtwo.Text + "', '" + cbopono.Text + "' ";
                    SqlCommand cmd = new SqlCommand(qur, conn);
                    SqlDataAdapter apt = new SqlDataAdapter(cmd);
                    DataTable tab = new DataTable();
                    apt.Fill(tab);
                    cbopro.DataSource = null;
                    cbopro.DataSource = tab;
                    cbopro.DisplayMember = "PROCESS";
                    cbopro.ValueMember = "PROCESS";
                    //cbostate.SelectedIndex = -1;



                }

            }
        }

        private void txtprocess_Click(object sender, EventArgs e)
        {
            //Genclass.type = 4;
            //DataTable dt = getParty();
            //bsp.DataSource = dt;
            //FillGrid4(dt, 1);
            Point loc = FindLocation(txtprocess);
            lkppnl.Location = new Point(loc.X, loc.Y + 20);
            lkppnl.Visible = true;
        }

        private void button22_Click(object sender, EventArgs e)
        {

        }

        private void buttnext1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void label9_Click(object sender, EventArgs e)
        {

        }

        private void HFGP2_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void txtqty_TextChanged(object sender, EventArgs e)
        {

        }

        private void button12_Click(object sender, EventArgs e)
        {
            string message = "Are you sure to cancel this PO ?";
            string caption = "Dilama";
            MessageBoxButtons buttons = MessageBoxButtons.YesNo;
            DialogResult result;

            result = MessageBox.Show(message, caption, buttons);

            if (result == System.Windows.Forms.DialogResult.Yes)
            {

                int i = HFGP.SelectedCells[0].RowIndex;
                uid = HFGP.Rows[i].Cells[0].Value.ToString();

                conn.Close();
                conn.Open();


                Genclass.strsql = "select  distinct a.uid from rateappd a inner join rateappm b on a.headid=b.uid where b.uid=" + uid + "  ";
                Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
                DataTable tap1 = new DataTable();
                aptr1.Fill(tap1);
                for (int l = 0; l < HFIT.RowCount - 1; l++)
                {
      
                    Genclass.strsql1 = "select  from JOKNITTINGITEMS  where refid=" + uid + " ";
                    Genclass.cmd = new SqlCommand(Genclass.strsql1, conn);
                    SqlDataAdapter aptr2 = new SqlDataAdapter(Genclass.cmd);
                    DataTable tap2 = new DataTable();
                    aptr2.Fill(tap2);


                    if(tap2.Rows.Count==0)
                    {

                        qur.CommandText = "delete  from rateappd  where headid=" + uid + " ";
                        qur.ExecuteNonQuery();

                        qur.CommandText = "delete from   rateappm    where uid=" + uid + "  ";
                        qur.ExecuteNonQuery();


                        MessageBox.Show("Deleted");
                    }
                    else


                    {


                        MessageBox.Show("Already Issued ");
                        return;
                    }
                }

             
            }
            LoadGetJobCard(1);
        }

        private void HFIT_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (HFIT.CurrentRow.Cells[0].Value != null && HFIT.CurrentCell.ColumnIndex == 4)
            {
                if (HFIT.CurrentCell.ColumnIndex == 4)

                {
                    DataGridViewCell cell = HFIT.CurrentRow.Cells[4];
                    HFIT.CurrentCell = cell;
                    HFIT.BeginEdit(true);
                }

            }
        }

        private void HFIT_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void txtscr1_TextChanged(object sender, EventArgs e)
        {
            bs.Filter = string.Format("docno Like '%{0}%' or  socno Like '%{1}%' or  narration Like '%{1}%' or  workorderno Like '%{1}%' ", txtscr1.Text, txtscr1.Text, txtscr1.Text, txtscr1.Text);
        }

        private void cbopro_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbopro.Text != "" && cbopro.ValueMember != "" && cbopro.DisplayMember != "")
            {
                Genclass.type = 5;
                DataTable dt = getParty();
                bsc.DataSource = dt;
                FillGrid2(dt, 1);
                Point loc = FindLocation(txtprocess);
                lkppnl.Location = new Point(loc.X, loc.Y + 20);
                lkppnl.Visible = true;
            }

        }

        private void cbopro_DropDown(object sender, EventArgs e)
        {
           
        }
    }
}

