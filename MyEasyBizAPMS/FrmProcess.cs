﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MyEasyBizAPMS
{
    public partial class FrmProcess : Form
    {
        public FrmProcess()
        {
            InitializeComponent();
            this.SfDataGridProcess.Style.HeaderStyle.Font.FontStyle = FontStyle.Bold;
            this.SfDataGridProcess.Style.HeaderStyle.BackColor = Color.DeepSkyBlue;
        }
        SQLDBHelper db = new SQLDBHelper();
        SqlConnection conn = new SqlConnection(GeneralParameters.ConnectionString);

        private void SplitAdd_Click(object sender, EventArgs e)
        {
            try
            {
                ClearControl();
                txtProcessName.Tag = "0";
                grBack.Visible = true;
                grFront.Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void SplitSave_DropDowItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            try
            {
                if (e.ClickedItem.Text == "Back")
                {
                    LoadGrid();
                    grBack.Visible = false;
                    grFront.Visible = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void SplitAdd_DropDowItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            try
            {
                if (e.ClickedItem.Text == "Close")
                {
                    this.Close();
                }
                else if (e.ClickedItem.Text == "Delete")
                {
                    var selectedItem = SfDataGridProcess.SelectedItems[0];
                    var dataRow = (selectedItem as DataRowView).Row;
                    string uid = dataRow["UID"].ToString();
                    string Query = "Update ProcessM Set Active =0 Where Uid =" + uid + "";
                    db.ExecuteNonQuery(CommandType.Text, Query, conn);
                    MessageBox.Show("Deleted Successfully", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    LoadGrid();
                }
                else if(e.ClickedItem.Text =="Edit")
                {
                    if(SfDataGridProcess.SelectedIndex == -1)
                    {
                        MessageBox.Show("Select a Row", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                    var selectedItem = SfDataGridProcess.SelectedItems[0];
                    var dataRow = (selectedItem as DataRowView).Row;
                    CmbTax.SelectedValue = dataRow["TaxUID"].ToString();
                    txtShortName.Text = dataRow["ProcessSName"].ToString();
                    txtProcessName.Text = dataRow["ProcessName"].ToString();
                    txtProcessName.Tag = dataRow["UID"].ToString();
                    CmbProcessGroup.SelectedValue = dataRow["ProcessGroupId"].ToString();
                    bool Active = Convert.ToBoolean(dataRow["Active"].ToString());
                    bool Yarn = Convert.ToBoolean(dataRow["Yarn"].ToString());
                    bool Fabric = Convert.ToBoolean(dataRow["Fabric"].ToString());
                    bool Components = Convert.ToBoolean(dataRow["Components"].ToString());
                    bool Gaments = Convert.ToBoolean(dataRow["Garments"].ToString());
                    bool Trims = Convert.ToBoolean(dataRow["Trims"].ToString());
                    if (Active == true)
                    {
                        chckActive.Checked = true;
                    }
                    else
                    {
                        chckActive.Checked = false;
                    }
                    if (Yarn == true)
                    {
                        chckYarn.Checked = true;
                    }
                    else
                    {
                        chckYarn.Checked = false;
                    }
                    if (Fabric == true)
                    {
                        chckFabric.Checked = true;
                    }
                    else
                    {
                        chckFabric.Checked = false;
                    }
                    if (Components == true)
                    {
                        chckComponents.Checked = true;
                    }
                    else
                    {
                        chckComponents.Checked = false;
                    }
                    if (Gaments == true)
                    {
                        chckGarments.Checked = true;
                    }
                    else
                    {
                        chckGarments.Checked = false;
                    }
                    if (Trims == true)
                    {
                        chckTrims.Checked = true;
                    }
                    else
                    {
                        chckTrims.Checked = false;
                    }
                    grFront.Visible = false;
                    grBack.Visible = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void FrmProcess_Load(object sender, EventArgs e)
        {
            LoadTax();
            LoadGrid();
            GetTProcessType();
            CmbProcessGroup.SelectedIndex = 0;
        }

        protected void LoadTax()
        {
            try
            {
                DataTable dt = GetTax();
                CmbTax.DataSource = null;
                CmbTax.DisplayMember = "GeneralName";
                CmbTax.ValueMember = "GUid";
                CmbTax.DataSource = dt;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected DataTable GetTax()
        {
            DataTable dt = new DataTable();
            try
            {
                dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "SP_GetTax", conn);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            return dt;
        }

        protected void GetTProcessType()
        {
            DataTable dt = new DataTable();
            try
            {
                SqlParameter[] sqlParameters = { new SqlParameter("@TypeMUid", 39), new SqlParameter("@Active", 1) };
                dt = db.GetDataWithParam(CommandType.StoredProcedure, "SP_GetGeneralM", sqlParameters, conn);
                CmbProcessGroup.DataSource = null;
                CmbProcessGroup.DisplayMember = "GeneralName";
                CmbProcessGroup.ValueMember = "Guid";
                CmbProcessGroup.DataSource = dt;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }


        private void SplitSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtProcessName.Text != string.Empty || txtShortName.Text != string.Empty || CmbTax.SelectedIndex != -1 || CmbProcessGroup.SelectedIndex != -1)
                {
                    bool Active = false;
                    bool Yarn = false;
                    bool Fabric = false;
                    bool Components = false;
                    bool Gaments = false;
                    bool Trims = false;
                    if(chckActive.Checked == true)
                    {
                        Active = true;
                    }
                    else
                    {
                        Active = false;
                    }
                    if(chckYarn.Checked == true)
                    {
                        Yarn = true;
                    }
                    else
                    {
                        Yarn = false;
                    }
                    if(chckFabric.Checked == true)
                    {
                        Fabric = true;
                    }
                    else
                    {
                        Fabric = false;
                    }
                    if(chckComponents.Checked == true)
                    {
                        Components = true;
                    }
                    else
                    {
                        Components = false;
                    }
                    if (chckGarments.Checked == true)
                    {
                        Gaments = true;
                    }
                    else
                    {
                        Gaments = false;
                    }
                    if (chckTrims.Checked == true)
                    {
                        Trims = true;
                    }
                    else
                    {
                        Trims = false;
                    }
                    SqlParameter[] parameters = {
                        new SqlParameter("@UID",txtProcessName.Tag),
                        new SqlParameter("@ProcessName",txtProcessName.Text),
                        new SqlParameter("@ProcessSName",txtShortName.Text),
                        new SqlParameter("@TaxUID",CmbTax.SelectedValue),
                        new SqlParameter("@Yarn",Yarn),
                        new SqlParameter("@Fabric",Fabric),
                        new SqlParameter("@Components",Components),
                        new SqlParameter("@Garments",Gaments),
                        new SqlParameter("@Trims",Trims),
                        new SqlParameter("@Active",Active),
                        new SqlParameter("@CreatedDate",DateTime.Now),
                        new SqlParameter("@CreatedBy",GeneralParameters.UserdId),
                        new SqlParameter("@ProcessGroupId",CmbProcessGroup.SelectedValue)
                    };
                    db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_ProcessM", parameters, conn);
                    MessageBox.Show("Record Saved Successfully", "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    ClearControl();
                }
                else
                {
                    MessageBox.Show("Fill Required Fields !", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected void ClearControl()
        {
            txtProcessName.Text = string.Empty;
            txtShortName.Text = string.Empty;
            CmbTax.SelectedIndex = -1;
            chckActive.Checked = false;
            chckYarn.Checked = false;
            chckFabric.Checked = false;
            chckComponents.Checked = false;
            chckGarments.Checked = false;
            chckTrims.Checked = false;
        }

        protected void LoadGrid()
        {
            try
            {
                DataTable dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "SP_GetProcessM", conn);
                SfDataGridProcess.DataSource = null;
                SfDataGridProcess.DataSource = dt;
                SfDataGridProcess.Columns[0].Visible = false;
                SfDataGridProcess.Columns[3].Visible = false;
                SfDataGridProcess.Columns[4].Visible = false;
                SfDataGridProcess.Columns[5].Visible = false;
                SfDataGridProcess.Columns[6].Visible = false;
                SfDataGridProcess.Columns[7].Visible = false;
                SfDataGridProcess.Columns[8].Visible = false;
                SfDataGridProcess.Columns[9].Visible = false;
                SfDataGridProcess.Columns[10].Visible = false;
                SfDataGridProcess.Columns[11].Visible = false;
                SfDataGridProcess.Columns[12].Visible = false;
                SfDataGridProcess.Columns[1].Width = 350;
                SfDataGridProcess.Columns[2].Width = 300;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

        }
    }
}
