﻿namespace MyEasyBizAPMS
{
    partial class FrmYarnMaster
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmYarnMaster));
            this.grBack = new System.Windows.Forms.GroupBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtOpeningStk = new System.Windows.Forms.TextBox();
            this.CmbPurity = new System.Windows.Forms.ComboBox();
            this.CmbCount = new System.Windows.Forms.ComboBox();
            this.CmbYarnType = new System.Windows.Forms.ComboBox();
            this.CmbCategory = new System.Windows.Forms.ComboBox();
            this.CmbItemType = new System.Windows.Forms.ComboBox();
            this.BtnMixingOk = new System.Windows.Forms.Button();
            this.txtRatio = new System.Windows.Forms.TextBox();
            this.CmbBlendMixing = new System.Windows.Forms.ComboBox();
            this.DataGridMixing = new System.Windows.Forms.DataGridView();
            this.btnReset = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.chckActive = new System.Windows.Forms.CheckBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.CmbTax = new System.Windows.Forms.ComboBox();
            this.CmbUOM = new System.Windows.Forms.ComboBox();
            this.TxtHsnCode = new System.Windows.Forms.TextBox();
            this.txtItemSpec = new System.Windows.Forms.RichTextBox();
            this.TxtShade = new System.Windows.Forms.TextBox();
            this.txtBlend = new System.Windows.Forms.TextBox();
            this.panadd = new System.Windows.Forms.Panel();
            this.chckAc = new System.Windows.Forms.CheckBox();
            this.btnAdd = new System.Windows.Forms.Button();
            this.PanelgridNos = new System.Windows.Forms.Panel();
            this.lblFrom = new System.Windows.Forms.Label();
            this.lblCount = new System.Windows.Forms.Label();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.btnFirst = new System.Windows.Forms.Button();
            this.btnBack = new System.Windows.Forms.Button();
            this.btnLast = new System.Windows.Forms.Button();
            this.btnNxt = new System.Windows.Forms.Button();
            this.btnEdit = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.btnAddCancel = new System.Windows.Forms.Button();
            this.grFront = new System.Windows.Forms.GroupBox();
            this.SfDataGridYarn = new Syncfusion.WinForms.DataGrid.SfDataGrid();
            this.grBack.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridMixing)).BeginInit();
            this.panadd.SuspendLayout();
            this.PanelgridNos.SuspendLayout();
            this.grFront.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SfDataGridYarn)).BeginInit();
            this.SuspendLayout();
            // 
            // grBack
            // 
            this.grBack.Controls.Add(this.label12);
            this.grBack.Controls.Add(this.txtOpeningStk);
            this.grBack.Controls.Add(this.CmbPurity);
            this.grBack.Controls.Add(this.CmbCount);
            this.grBack.Controls.Add(this.CmbYarnType);
            this.grBack.Controls.Add(this.CmbCategory);
            this.grBack.Controls.Add(this.CmbItemType);
            this.grBack.Controls.Add(this.BtnMixingOk);
            this.grBack.Controls.Add(this.txtRatio);
            this.grBack.Controls.Add(this.CmbBlendMixing);
            this.grBack.Controls.Add(this.DataGridMixing);
            this.grBack.Controls.Add(this.btnReset);
            this.grBack.Controls.Add(this.label1);
            this.grBack.Controls.Add(this.label7);
            this.grBack.Controls.Add(this.label5);
            this.grBack.Controls.Add(this.label4);
            this.grBack.Controls.Add(this.label3);
            this.grBack.Controls.Add(this.label2);
            this.grBack.Controls.Add(this.chckActive);
            this.grBack.Controls.Add(this.label11);
            this.grBack.Controls.Add(this.label10);
            this.grBack.Controls.Add(this.label9);
            this.grBack.Controls.Add(this.label8);
            this.grBack.Controls.Add(this.label6);
            this.grBack.Controls.Add(this.CmbTax);
            this.grBack.Controls.Add(this.CmbUOM);
            this.grBack.Controls.Add(this.TxtHsnCode);
            this.grBack.Controls.Add(this.txtItemSpec);
            this.grBack.Controls.Add(this.TxtShade);
            this.grBack.Controls.Add(this.txtBlend);
            this.grBack.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grBack.Location = new System.Drawing.Point(4, 0);
            this.grBack.Name = "grBack";
            this.grBack.Size = new System.Drawing.Size(690, 471);
            this.grBack.TabIndex = 0;
            this.grBack.TabStop = false;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(327, 393);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(97, 18);
            this.label12.TabIndex = 44;
            this.label12.Text = "Opening Stock";
            // 
            // txtOpeningStk
            // 
            this.txtOpeningStk.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtOpeningStk.Location = new System.Drawing.Point(425, 389);
            this.txtOpeningStk.Name = "txtOpeningStk";
            this.txtOpeningStk.Size = new System.Drawing.Size(173, 26);
            this.txtOpeningStk.TabIndex = 43;
            // 
            // CmbPurity
            // 
            this.CmbPurity.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CmbPurity.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CmbPurity.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CmbPurity.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CmbPurity.FormattingEnabled = true;
            this.CmbPurity.Location = new System.Drawing.Point(146, 157);
            this.CmbPurity.Name = "CmbPurity";
            this.CmbPurity.Size = new System.Drawing.Size(250, 26);
            this.CmbPurity.TabIndex = 42;
            this.CmbPurity.SelectedIndexChanged += new System.EventHandler(this.CmbPurity_SelectedIndexChanged);
            // 
            // CmbCount
            // 
            this.CmbCount.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CmbCount.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CmbCount.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CmbCount.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CmbCount.FormattingEnabled = true;
            this.CmbCount.Location = new System.Drawing.Point(146, 125);
            this.CmbCount.Name = "CmbCount";
            this.CmbCount.Size = new System.Drawing.Size(250, 26);
            this.CmbCount.TabIndex = 41;
            this.CmbCount.SelectedIndexChanged += new System.EventHandler(this.CmbCount_SelectedIndexChanged);
            // 
            // CmbYarnType
            // 
            this.CmbYarnType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CmbYarnType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CmbYarnType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CmbYarnType.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CmbYarnType.FormattingEnabled = true;
            this.CmbYarnType.Location = new System.Drawing.Point(146, 92);
            this.CmbYarnType.Name = "CmbYarnType";
            this.CmbYarnType.Size = new System.Drawing.Size(250, 26);
            this.CmbYarnType.TabIndex = 40;
            this.CmbYarnType.SelectedIndexChanged += new System.EventHandler(this.CmbYarnType_SelectedIndexChanged);
            // 
            // CmbCategory
            // 
            this.CmbCategory.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CmbCategory.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CmbCategory.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CmbCategory.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CmbCategory.FormattingEnabled = true;
            this.CmbCategory.Location = new System.Drawing.Point(146, 58);
            this.CmbCategory.Name = "CmbCategory";
            this.CmbCategory.Size = new System.Drawing.Size(250, 26);
            this.CmbCategory.TabIndex = 39;
            // 
            // CmbItemType
            // 
            this.CmbItemType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CmbItemType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CmbItemType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CmbItemType.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CmbItemType.FormattingEnabled = true;
            this.CmbItemType.Location = new System.Drawing.Point(146, 22);
            this.CmbItemType.Name = "CmbItemType";
            this.CmbItemType.Size = new System.Drawing.Size(250, 26);
            this.CmbItemType.TabIndex = 38;
            // 
            // BtnMixingOk
            // 
            this.BtnMixingOk.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnMixingOk.Location = new System.Drawing.Point(645, 26);
            this.BtnMixingOk.Name = "BtnMixingOk";
            this.BtnMixingOk.Size = new System.Drawing.Size(36, 27);
            this.BtnMixingOk.TabIndex = 36;
            this.BtnMixingOk.Text = "Ok";
            this.BtnMixingOk.UseVisualStyleBackColor = true;
            this.BtnMixingOk.Visible = false;
            this.BtnMixingOk.Click += new System.EventHandler(this.BtnMixingOk_Click);
            // 
            // txtRatio
            // 
            this.txtRatio.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRatio.Location = new System.Drawing.Point(580, 26);
            this.txtRatio.Name = "txtRatio";
            this.txtRatio.Size = new System.Drawing.Size(65, 26);
            this.txtRatio.TabIndex = 35;
            this.txtRatio.Visible = false;
            // 
            // CmbBlendMixing
            // 
            this.CmbBlendMixing.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CmbBlendMixing.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CmbBlendMixing.FormattingEnabled = true;
            this.CmbBlendMixing.Location = new System.Drawing.Point(402, 25);
            this.CmbBlendMixing.Name = "CmbBlendMixing";
            this.CmbBlendMixing.Size = new System.Drawing.Size(176, 26);
            this.CmbBlendMixing.TabIndex = 34;
            this.CmbBlendMixing.Visible = false;
            // 
            // DataGridMixing
            // 
            this.DataGridMixing.AllowUserToAddRows = false;
            this.DataGridMixing.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DataGridMixing.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.DataGridMixing.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DataGridMixing.DefaultCellStyle = dataGridViewCellStyle5;
            this.DataGridMixing.Location = new System.Drawing.Point(402, 53);
            this.DataGridMixing.Name = "DataGridMixing";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DataGridMixing.RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.DataGridMixing.RowHeadersVisible = false;
            this.DataGridMixing.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridMixing.Size = new System.Drawing.Size(277, 164);
            this.DataGridMixing.TabIndex = 33;
            this.DataGridMixing.Visible = false;
            // 
            // btnReset
            // 
            this.btnReset.Location = new System.Drawing.Point(474, 259);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(67, 31);
            this.btnReset.TabIndex = 16;
            this.btnReset.Text = "Reset";
            this.btnReset.UseVisualStyleBackColor = true;
            this.btnReset.Click += new System.EventHandler(this.BtnReset_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(74, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(69, 18);
            this.label1.TabIndex = 0;
            this.label1.Text = "Item Type";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(98, 161);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(45, 18);
            this.label7.TabIndex = 32;
            this.label7.Text = "Purity";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(99, 194);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(44, 18);
            this.label5.TabIndex = 4;
            this.label5.Text = "Blend";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(80, 62);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(63, 18);
            this.label4.TabIndex = 3;
            this.label4.Text = "Category";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(77, 96);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(66, 18);
            this.label3.TabIndex = 2;
            this.label3.Text = "Yarn Type";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(98, 129);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(45, 18);
            this.label2.TabIndex = 1;
            this.label2.Text = "Count";
            // 
            // chckActive
            // 
            this.chckActive.AutoSize = true;
            this.chckActive.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chckActive.Location = new System.Drawing.Point(146, 430);
            this.chckActive.Name = "chckActive";
            this.chckActive.Size = new System.Drawing.Size(65, 22);
            this.chckActive.TabIndex = 11;
            this.chckActive.Text = "Active";
            this.chckActive.UseVisualStyleBackColor = true;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(115, 357);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(28, 18);
            this.label11.TabIndex = 22;
            this.label11.Text = "Tax";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(74, 393);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(69, 18);
            this.label10.TabIndex = 20;
            this.label10.Text = "HSN Code";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(104, 319);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(39, 18);
            this.label9.TabIndex = 18;
            this.label9.Text = "UOM";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(43, 277);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(100, 18);
            this.label8.TabIndex = 14;
            this.label8.Text = "SPECIFICATION";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(97, 229);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(46, 18);
            this.label6.TabIndex = 5;
            this.label6.Text = "Shade";
            // 
            // CmbTax
            // 
            this.CmbTax.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CmbTax.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CmbTax.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CmbTax.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CmbTax.FormattingEnabled = true;
            this.CmbTax.Location = new System.Drawing.Point(146, 353);
            this.CmbTax.Name = "CmbTax";
            this.CmbTax.Size = new System.Drawing.Size(248, 26);
            this.CmbTax.TabIndex = 9;
            // 
            // CmbUOM
            // 
            this.CmbUOM.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CmbUOM.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CmbUOM.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CmbUOM.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CmbUOM.FormattingEnabled = true;
            this.CmbUOM.Location = new System.Drawing.Point(146, 315);
            this.CmbUOM.Name = "CmbUOM";
            this.CmbUOM.Size = new System.Drawing.Size(248, 26);
            this.CmbUOM.TabIndex = 8;
            // 
            // TxtHsnCode
            // 
            this.TxtHsnCode.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtHsnCode.Location = new System.Drawing.Point(146, 389);
            this.TxtHsnCode.Name = "TxtHsnCode";
            this.TxtHsnCode.Size = new System.Drawing.Size(173, 26);
            this.TxtHsnCode.TabIndex = 10;
            // 
            // txtItemSpec
            // 
            this.txtItemSpec.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtItemSpec.Location = new System.Drawing.Point(146, 259);
            this.txtItemSpec.Name = "txtItemSpec";
            this.txtItemSpec.ReadOnly = true;
            this.txtItemSpec.Size = new System.Drawing.Size(328, 50);
            this.txtItemSpec.TabIndex = 7;
            this.txtItemSpec.Text = "";
            // 
            // TxtShade
            // 
            this.TxtShade.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtShade.Location = new System.Drawing.Point(146, 225);
            this.TxtShade.Name = "TxtShade";
            this.TxtShade.Size = new System.Drawing.Size(328, 26);
            this.TxtShade.TabIndex = 6;
            this.TxtShade.TextChanged += new System.EventHandler(this.TxtShade_TextChanged);
            // 
            // txtBlend
            // 
            this.txtBlend.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBlend.Location = new System.Drawing.Point(146, 190);
            this.txtBlend.Name = "txtBlend";
            this.txtBlend.Size = new System.Drawing.Size(251, 26);
            this.txtBlend.TabIndex = 37;
            // 
            // panadd
            // 
            this.panadd.BackColor = System.Drawing.Color.White;
            this.panadd.Controls.Add(this.chckAc);
            this.panadd.Controls.Add(this.btnAdd);
            this.panadd.Controls.Add(this.PanelgridNos);
            this.panadd.Controls.Add(this.btnFirst);
            this.panadd.Controls.Add(this.btnBack);
            this.panadd.Controls.Add(this.btnLast);
            this.panadd.Controls.Add(this.btnNxt);
            this.panadd.Controls.Add(this.btnEdit);
            this.panadd.Controls.Add(this.btnSave);
            this.panadd.Controls.Add(this.btnExit);
            this.panadd.Controls.Add(this.btnAddCancel);
            this.panadd.Location = new System.Drawing.Point(6, 475);
            this.panadd.Name = "panadd";
            this.panadd.Size = new System.Drawing.Size(688, 36);
            this.panadd.TabIndex = 238;
            // 
            // chckAc
            // 
            this.chckAc.AutoSize = true;
            this.chckAc.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chckAc.Location = new System.Drawing.Point(212, 8);
            this.chckAc.Name = "chckAc";
            this.chckAc.Size = new System.Drawing.Size(65, 22);
            this.chckAc.TabIndex = 16;
            this.chckAc.Text = "Active";
            this.chckAc.UseVisualStyleBackColor = true;
            this.chckAc.CheckedChanged += new System.EventHandler(this.ChckAc_CheckedChanged);
            // 
            // btnAdd
            // 
            this.btnAdd.BackColor = System.Drawing.Color.White;
            this.btnAdd.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnAdd.FlatAppearance.BorderSize = 0;
            this.btnAdd.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAdd.Image = ((System.Drawing.Image)(resources.GetObject("btnAdd.Image")));
            this.btnAdd.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAdd.Location = new System.Drawing.Point(482, 3);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(88, 31);
            this.btnAdd.TabIndex = 184;
            this.btnAdd.Text = "Add new";
            this.btnAdd.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAdd.UseVisualStyleBackColor = false;
            this.btnAdd.Click += new System.EventHandler(this.BtnAdd_Click);
            // 
            // PanelgridNos
            // 
            this.PanelgridNos.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.PanelgridNos.Controls.Add(this.lblFrom);
            this.PanelgridNos.Controls.Add(this.lblCount);
            this.PanelgridNos.Controls.Add(this.flowLayoutPanel3);
            this.PanelgridNos.Controls.Add(this.flowLayoutPanel2);
            this.PanelgridNos.Controls.Add(this.flowLayoutPanel1);
            this.PanelgridNos.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PanelgridNos.Location = new System.Drawing.Point(52, 2);
            this.PanelgridNos.Name = "PanelgridNos";
            this.PanelgridNos.Size = new System.Drawing.Size(70, 30);
            this.PanelgridNos.TabIndex = 214;
            this.PanelgridNos.Visible = false;
            // 
            // lblFrom
            // 
            this.lblFrom.AutoSize = true;
            this.lblFrom.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFrom.ForeColor = System.Drawing.Color.Black;
            this.lblFrom.Location = new System.Drawing.Point(4, 4);
            this.lblFrom.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblFrom.Name = "lblFrom";
            this.lblFrom.Size = new System.Drawing.Size(15, 18);
            this.lblFrom.TabIndex = 163;
            this.lblFrom.Text = "1";
            // 
            // lblCount
            // 
            this.lblCount.AutoSize = true;
            this.lblCount.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCount.ForeColor = System.Drawing.Color.Black;
            this.lblCount.Location = new System.Drawing.Point(27, 4);
            this.lblCount.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCount.Name = "lblCount";
            this.lblCount.Size = new System.Drawing.Size(31, 18);
            this.lblCount.TabIndex = 162;
            this.lblCount.Text = "of 1";
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.Location = new System.Drawing.Point(61, 30);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(200, 100);
            this.flowLayoutPanel3.TabIndex = 2;
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.Location = new System.Drawing.Point(38, 35);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(200, 100);
            this.flowLayoutPanel2.TabIndex = 1;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Location = new System.Drawing.Point(62, -129);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(200, 100);
            this.flowLayoutPanel1.TabIndex = 0;
            // 
            // btnFirst
            // 
            this.btnFirst.BackColor = System.Drawing.Color.White;
            this.btnFirst.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnFirst.FlatAppearance.BorderSize = 0;
            this.btnFirst.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFirst.Image = ((System.Drawing.Image)(resources.GetObject("btnFirst.Image")));
            this.btnFirst.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnFirst.Location = new System.Drawing.Point(6, 2);
            this.btnFirst.Name = "btnFirst";
            this.btnFirst.Size = new System.Drawing.Size(19, 31);
            this.btnFirst.TabIndex = 213;
            this.btnFirst.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnFirst.UseVisualStyleBackColor = false;
            this.btnFirst.Visible = false;
            // 
            // btnBack
            // 
            this.btnBack.BackColor = System.Drawing.Color.White;
            this.btnBack.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnBack.FlatAppearance.BorderSize = 0;
            this.btnBack.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBack.Image = ((System.Drawing.Image)(resources.GetObject("btnBack.Image")));
            this.btnBack.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnBack.Location = new System.Drawing.Point(29, 2);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(18, 31);
            this.btnBack.TabIndex = 212;
            this.btnBack.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnBack.UseVisualStyleBackColor = false;
            this.btnBack.Visible = false;
            // 
            // btnLast
            // 
            this.btnLast.BackColor = System.Drawing.Color.White;
            this.btnLast.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnLast.FlatAppearance.BorderSize = 0;
            this.btnLast.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLast.Image = ((System.Drawing.Image)(resources.GetObject("btnLast.Image")));
            this.btnLast.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnLast.Location = new System.Drawing.Point(153, 2);
            this.btnLast.Name = "btnLast";
            this.btnLast.Size = new System.Drawing.Size(19, 31);
            this.btnLast.TabIndex = 211;
            this.btnLast.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnLast.UseVisualStyleBackColor = false;
            this.btnLast.Visible = false;
            // 
            // btnNxt
            // 
            this.btnNxt.BackColor = System.Drawing.Color.White;
            this.btnNxt.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnNxt.FlatAppearance.BorderSize = 0;
            this.btnNxt.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNxt.Image = ((System.Drawing.Image)(resources.GetObject("btnNxt.Image")));
            this.btnNxt.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnNxt.Location = new System.Drawing.Point(131, 2);
            this.btnNxt.Name = "btnNxt";
            this.btnNxt.Size = new System.Drawing.Size(18, 31);
            this.btnNxt.TabIndex = 210;
            this.btnNxt.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnNxt.UseVisualStyleBackColor = false;
            this.btnNxt.Visible = false;
            // 
            // btnEdit
            // 
            this.btnEdit.BackColor = System.Drawing.Color.White;
            this.btnEdit.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEdit.Image = ((System.Drawing.Image)(resources.GetObject("btnEdit.Image")));
            this.btnEdit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnEdit.Location = new System.Drawing.Point(568, 3);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(60, 31);
            this.btnEdit.TabIndex = 185;
            this.btnEdit.Text = "Edit";
            this.btnEdit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnEdit.UseVisualStyleBackColor = false;
            this.btnEdit.Click += new System.EventHandler(this.BtnEdit_Click);
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnSave.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.Image = ((System.Drawing.Image)(resources.GetObject("btnSave.Image")));
            this.btnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSave.Location = new System.Drawing.Point(533, 3);
            this.btnSave.Margin = new System.Windows.Forms.Padding(4);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(81, 31);
            this.btnSave.TabIndex = 8;
            this.btnSave.Text = "Save";
            this.btnSave.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.BtnSave_Click);
            // 
            // btnExit
            // 
            this.btnExit.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnExit.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.Image = ((System.Drawing.Image)(resources.GetObject("btnExit.Image")));
            this.btnExit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExit.Location = new System.Drawing.Point(626, 3);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(57, 31);
            this.btnExit.TabIndex = 208;
            this.btnExit.Text = "Exit";
            this.btnExit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnExit.UseVisualStyleBackColor = false;
            this.btnExit.Click += new System.EventHandler(this.BtnExit_Click);
            // 
            // btnAddCancel
            // 
            this.btnAddCancel.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnAddCancel.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddCancel.Image = ((System.Drawing.Image)(resources.GetObject("btnAddCancel.Image")));
            this.btnAddCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAddCancel.Location = new System.Drawing.Point(612, 3);
            this.btnAddCancel.Margin = new System.Windows.Forms.Padding(4);
            this.btnAddCancel.Name = "btnAddCancel";
            this.btnAddCancel.Size = new System.Drawing.Size(60, 31);
            this.btnAddCancel.TabIndex = 9;
            this.btnAddCancel.Text = "Back";
            this.btnAddCancel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAddCancel.UseVisualStyleBackColor = false;
            this.btnAddCancel.Click += new System.EventHandler(this.BtnAddCancel_Click);
            // 
            // grFront
            // 
            this.grFront.Controls.Add(this.SfDataGridYarn);
            this.grFront.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grFront.Location = new System.Drawing.Point(3, 0);
            this.grFront.Name = "grFront";
            this.grFront.Size = new System.Drawing.Size(691, 472);
            this.grFront.TabIndex = 16;
            this.grFront.TabStop = false;
            // 
            // SfDataGridYarn
            // 
            this.SfDataGridYarn.AccessibleName = "Table";
            this.SfDataGridYarn.AllowFiltering = true;
            this.SfDataGridYarn.AllowResizingColumns = true;
            this.SfDataGridYarn.Location = new System.Drawing.Point(3, 12);
            this.SfDataGridYarn.Name = "SfDataGridYarn";
            this.SfDataGridYarn.Size = new System.Drawing.Size(683, 457);
            this.SfDataGridYarn.TabIndex = 0;
            this.SfDataGridYarn.Text = "sfDataGrid1";
            this.SfDataGridYarn.DrawCell += new Syncfusion.WinForms.DataGrid.Events.DrawCellEventHandler(this.SfDataGridYarn_DrawCell);
            // 
            // FrmYarnMaster
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(698, 513);
            this.Controls.Add(this.panadd);
            this.Controls.Add(this.grFront);
            this.Controls.Add(this.grBack);
            this.Name = "FrmYarnMaster";
            this.Text = "Yarn Master";
            this.Load += new System.EventHandler(this.FrmYarnMaster_Load);
            this.grBack.ResumeLayout(false);
            this.grBack.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridMixing)).EndInit();
            this.panadd.ResumeLayout(false);
            this.panadd.PerformLayout();
            this.PanelgridNos.ResumeLayout(false);
            this.PanelgridNos.PerformLayout();
            this.grFront.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.SfDataGridYarn)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grBack;
        private System.Windows.Forms.Panel panadd;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnEdit;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnAddCancel;
        private System.Windows.Forms.Panel PanelgridNos;
        private System.Windows.Forms.Label lblFrom;
        private System.Windows.Forms.Label lblCount;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Button btnFirst;
        private System.Windows.Forms.Button btnBack;
        private System.Windows.Forms.Button btnLast;
        private System.Windows.Forms.Button btnNxt;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RichTextBox txtItemSpec;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.CheckBox chckActive;
        private System.Windows.Forms.GroupBox grFront;
        private System.Windows.Forms.CheckBox chckAc;
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox TxtHsnCode;
        private System.Windows.Forms.Label label11;
        private Syncfusion.WinForms.DataGrid.SfDataGrid SfDataGridYarn;
        private System.Windows.Forms.TextBox TxtShade;
        private System.Windows.Forms.Label label7;
        //private MyEasyBizAPMS.CBox cBoxItemType;
        //private MyEasyBizAPMS.CBox cBoxPurity;
        //private MyEasyBizAPMS.CBox cBoxCount;
        //private MyEasyBizAPMS.CBox cBoxBlend;
        //private MyEasyBizAPMS.CBox cBoxYarnType;
        //private MyEasyBizAPMS.CBox cBoxCategory;
        private System.Windows.Forms.ComboBox CmbTax;
        private System.Windows.Forms.ComboBox CmbUOM;
        private System.Windows.Forms.Button BtnMixingOk;
        private System.Windows.Forms.TextBox txtRatio;
        private System.Windows.Forms.ComboBox CmbBlendMixing;
        private System.Windows.Forms.DataGridView DataGridMixing;
        private System.Windows.Forms.TextBox txtBlend;
        private System.Windows.Forms.ComboBox CmbItemType;
        private System.Windows.Forms.ComboBox CmbPurity;
        private System.Windows.Forms.ComboBox CmbCount;
        private System.Windows.Forms.ComboBox CmbYarnType;
        private System.Windows.Forms.ComboBox CmbCategory;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtOpeningStk;
    }
}