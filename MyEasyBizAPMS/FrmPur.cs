﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Drawing.Imaging;
using System.Configuration;
using System.Data.SqlClient;
using System.Diagnostics;
using excel = Microsoft.Office.Interop.Excel;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;

//using Bytescout.Spreadsheet;
namespace MyEasyBizAPMS
{
    public partial class FrmPur : Form
    {

        public string fo
        {
            get { return txtitemname.Text; }
            set { txtitemname.Text = value; }

        }
        public FrmPur()
        {
            CenterToParent();
            this.BackColor = Color.White;
            InitializeComponent();
        }
         private CrystalDecisions.Windows.Forms.CrystalReportViewer Cryview;



        ReportDocument doc = new ReportDocument();
        string uid = "";
        int mode = 0;
        double sum1;
        double sum2;
        int sum;
        string address;
        int ty = 0;
        int type = 0;
        double dis3 = 0;
        double dis4 = 0;
        double dd8 = 0;
        double dd1 = 0;
        double dd2 = 0;
        double hg = 0;
        int i;
        String TYPENAME;
        int Dtype = 0;
        double sum5 =0;
        string strfin;
        int totamt;
        double dd9;
         double val2;
        double sumb;
        double uy;
        double uy1;
        double dis6;
         double dis5;
        int m; Double net1; double someInt; double rof; Double rof1; Double rof2; Double net; double bval; double dis;
        double hh; double dd; double sump; double sumq; double add; double yt7; double df = 0; double dg = 0; double dg1 = 0; double str2 = 0;
        int p = 0; double sun; Int64 NumVal; string Nw;
        int j = -1;
        BindingSource bsc = new BindingSource();
        BindingSource bs = new BindingSource();
        SQLDBHelper db = new SQLDBHelper();
        int Fillid;
        BindingSource bsParty = new BindingSource();
        string str1key;
        public int SelectId = 0;
        private static Microsoft.Office.Interop.Excel.Workbook mWorkBook;
        private static Microsoft.Office.Interop.Excel.Sheets mWorkSheets;
        private static Microsoft.Office.Interop.Excel.Worksheet mWSheet1;
        private static Microsoft.Office.Interop.Excel.Application oXL;
        //SqlCommand cmd;
        SqlConnection conn = new SqlConnection(GeneralParameters.ConnectionString);

        //SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connstr"].ConnectionString);
        SqlCommand qur = new SqlCommand();
        private void FrmPur_Load(object sender, EventArgs e)
        {
            chkact.Checked = true;
            addpnl.Visible = false;
            qur.Connection = conn;
            if (Text == "Yarn Purchase")
            {
                Dtype = 7;

            }
            else if (Text == "Fabric Purchase")
            {
                Dtype = 7;

            }
            else if (Text == "Trims Purchase")
            {
                Dtype = 7;

            }
            Genclass.Dtype = 7;
            HFGP.RowHeadersVisible = false;
            HFIT.RowHeadersVisible = false;
            HFGA.RowHeadersVisible = false;
            HFGT.RowHeadersVisible = false;
            HFGTAX.RowHeadersVisible = false;
            HFGST.RowHeadersVisible = false;
            HFGP2.RowHeadersVisible = false;
            Genpan.Visible = true;
            Taxpan.Visible = false;
            Editpan.Visible = false;
            TitleAdd();
            Titleterm();
            Titlep();
            Titlegst();

            this.HFGP.DefaultCellStyle.Font = new Font("calibri", 10);
            this.HFGP.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 10, FontStyle.Bold);
            this.HFIT.DefaultCellStyle.Font = new Font("calibri", 9);
            this.HFIT.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 9, FontStyle.Bold);
            this.HFGA.DefaultCellStyle.Font = new Font("calibri", 10);
            this.HFGA.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 10, FontStyle.Bold);
            this.HFGT.DefaultCellStyle.Font = new Font("calibri", 10);
            this.HFGT.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 10, FontStyle.Bold);
            this.HFGTAX.DefaultCellStyle.Font = new Font("calibri", 10);
            this.HFGTAX.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 10, FontStyle.Bold);
            this.HFGST.DefaultCellStyle.Font = new Font("calibri", 9);
            this.HFGST.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 9, FontStyle.Bold);
            this.HFGP2.DefaultCellStyle.Font = new Font("calibri", 10);
            this.HFGP2.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 10, FontStyle.Bold);
  
            txtplace.ReadOnly = true;
            txtitemname.ReadOnly = true;


            txttbval.ReadOnly = true;
            txttprdval.ReadOnly = true;
            txtexcise.ReadOnly = true;
            txtigval.ReadOnly = true;
            txtttot.ReadOnly = true;
            TxtNetAmt.ReadOnly = true;
            //loadtax();

            txtexcise.Text = "";
           
           
            chkact.Checked = true;
            Genclass.buttonstyleform(this);
            Genclass.buttonstylepanel(panadd);
            panadd.Visible = true;
            HFGP.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            HFGP.EnableHeadersVisualStyles = false;
            HFGP.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.GradientActiveCaption;
            HFIT.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            HFIT.EnableHeadersVisualStyles = false;
            HFIT.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.GradientActiveCaption;
            HFGA.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            HFGA.EnableHeadersVisualStyles = false;
            HFGA.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.GradientActiveCaption;
            //HFGT.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            HFGT.EnableHeadersVisualStyles = false;
            HFGT.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.GradientActiveCaption;
            HFGTAX.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            HFGTAX.EnableHeadersVisualStyles = false;
            HFGTAX.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.GradientActiveCaption;
            HFGST.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            HFGST.EnableHeadersVisualStyles = false;
            HFGST.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.GradientActiveCaption;
            HFGP2.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            HFGP2.EnableHeadersVisualStyles = false;
            HFGP2.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.GradientActiveCaption;
            HFGP.Focus();
            lkppnl.Visible = false;
            Genpan.Width = 1298;
            Genpan.Height = 618;

            DTPDOCDT.Format = DateTimePickerFormat.Custom;
            DTPDOCDT.CustomFormat = "dd/MM/yyyy";

            Dtpdt.Format = DateTimePickerFormat.Custom;
            Dtpdt.CustomFormat = "dd/MM/yyyy";
            Dtppre.Format = DateTimePickerFormat.Custom;
            Dtppre.CustomFormat = "dd/MM/yyyy";
            Dtprem.Format = DateTimePickerFormat.Custom;
            Dtprem.CustomFormat = "dd/MM/yyyy";

            LoadGetJobCard(1);
            tax();



        }


        public void tax()
        {
            string qur = "select Generalname as tax,Cast( F1 as int) uid from generalm where   Typemuid in (8) and active=1 ";
            DataTable tab = db.GetData2(CommandType.Text, qur);
            txtTaxPercentage.DataSource = null;
            txtTaxPercentage.DisplayMember = "tax";
            txtTaxPercentage.ValueMember = "uid";
            txtTaxPercentage.DataSource = tab;
        }
        private void loadtax()
        {

            conn.Open();
            string qur = "select a.UId,a.GeneralName from  GENERALM a inner join typem b  on a.TypeM_Uid=b.UId where a.TypeM_Uid in (4,5,6,7,8,9) and Active=1 ";
            SqlCommand cmd = new SqlCommand(qur, conn);
            SqlDataAdapter apt = new SqlDataAdapter(cmd);
            DataTable tab = new DataTable();
            apt.Fill(tab);
            conn.Close();



        }
      
        protected DataTable LoadGetJobCard(int tag)
        {

            int SP;
            //chkact.Checked = true;
            if (chkact.Checked == true)
            {
                SP = 1;

            }
            else
            {
                SP = 0;

            }


            DataTable dt = new DataTable();
            try
            {

                    SqlParameter[] para = {
                    new SqlParameter("@active",SP),
                    new SqlParameter("@type",Text),
                    new SqlParameter("@datetype",Dtype),

                };

                dt = db.GetData(CommandType.StoredProcedure, "SP_GETPURLOAD", para);


                LoadDataTable(dt);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        protected void LoadDataTable(DataTable dt)
        {
            try
            {
                HFGP.DataSource = null;
                HFGP.AutoGenerateColumns = false;
                HFGP.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
                HFGP.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
                HFGP.ColumnCount = 17;

                HFGP.Columns[0].Name = "Uid";
                HFGP.Columns[0].HeaderText = "Uid";
                HFGP.Columns[0].DataPropertyName = "Uid";

                HFGP.Columns[1].Name = "Docno";
                HFGP.Columns[1].HeaderText = "Docno";
                HFGP.Columns[1].DataPropertyName = "Docno";

                HFGP.Columns[2].Name = "Docdate";
                HFGP.Columns[2].HeaderText = "Docdate";
                HFGP.Columns[2].DataPropertyName = "Docdate";

                HFGP.Columns[3].Name = "Dcno";
                HFGP.Columns[3].HeaderText = "Dcno";
                HFGP.Columns[3].DataPropertyName = "Dcno";

                HFGP.Columns[4].Name = "Dcdate";
                HFGP.Columns[4].HeaderText = "Dcdate";
                HFGP.Columns[4].DataPropertyName = "Dcdate";

                HFGP.Columns[5].Name = "Name";
                HFGP.Columns[5].HeaderText = "Name";
                HFGP.Columns[5].DataPropertyName = "Name";

                HFGP.Columns[6].Name = "Netvalue";
                HFGP.Columns[6].HeaderText = "Netvalue";
                HFGP.Columns[6].DataPropertyName = "Netvalue";

                HFGP.Columns[7].Name = "partyuid";
                HFGP.Columns[7].HeaderText = "partyuid";
                HFGP.Columns[7].DataPropertyName = "partyuid";

                HFGP.Columns[8].Name = "placeuid";
                HFGP.Columns[8].HeaderText = "placeuid";
                HFGP.Columns[8].DataPropertyName = "placeuid";

                HFGP.Columns[9].Name = "Placename";
                HFGP.Columns[9].HeaderText = "Placename";
                HFGP.Columns[9].DataPropertyName = "Placename";

                HFGP.Columns[10].Name = "dtpre";
                HFGP.Columns[10].HeaderText = "dtpre";
                HFGP.Columns[10].DataPropertyName = "dtpre";
                HFGP.Columns[11].Name = "dtrem";
                HFGP.Columns[11].HeaderText = "dtrem";
                HFGP.Columns[11].DataPropertyName = "dtrem";
                HFGP.Columns[12].Name = "transp";
                HFGP.Columns[12].HeaderText = "transp";
                HFGP.Columns[12].DataPropertyName = "transp";
                HFGP.Columns[13].Name = "vehno";
                HFGP.Columns[13].HeaderText = "vehno";
                HFGP.Columns[13].DataPropertyName = "vehno";
                HFGP.Columns[14].Name = "remarks";
                HFGP.Columns[14].HeaderText = "remarks";
                HFGP.Columns[14].DataPropertyName = "remarks";
                HFGP.Columns[15].Name = "Status";
                HFGP.Columns[15].HeaderText = "Status";
                HFGP.Columns[15].DataPropertyName = "Status";
                HFGP.Columns[16].Name = "reqdt";
                HFGP.Columns[16].HeaderText = "reqdt";
                HFGP.Columns[16].DataPropertyName = "reqdt";

                
                bs.DataSource = dt;

                HFGP.DataSource = bs;
                HFGP.Columns[0].Visible = false;
                HFGP.Columns[1].Width = 87;
                HFGP.Columns[2].Width = 89;
                HFGP.Columns[3].Visible = false;
                HFGP.Columns[4].Visible = false;
                HFGP.Columns[5].Width = 560;
                HFGP.Columns[6].Width = 100;
                HFGP.Columns[7].Visible = false;
                HFGP.Columns[8].Visible = false;
                HFGP.Columns[9].Visible = false;
                HFGP.Columns[10].Visible = false;
                HFGP.Columns[11].Visible = false;
                HFGP.Columns[12].Visible = false;
                HFGP.Columns[13].Visible = false;
                HFGP.Columns[14].Visible = false;
                HFGP.Columns[15].Width = 100;
                HFGP.Columns[16].Visible = false;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void Titlep()
        {
            HFIT.ColumnCount = 22;
            HFIT.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            HFIT.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            HFIT.Columns[0].Name = "Itemname";
            HFIT.Columns[1].Name = "UoM";
            //HFIT.Columns[2].Name = "Addnotes";
            HFIT.Columns[2].Name = "Price";
            HFIT.Columns[3].Name = "Qty";
            HFIT.Columns[4].Name = "Value";
            HFIT.Columns[5].Name = "Itemuid";
            HFIT.Columns[6].Name = "Refuid";
            HFIT.Columns[7].Name = "Dis%";
            HFIT.Columns[8].Name = "Disval";
            HFIT.Columns[9].Name = "Taxableval";
            HFIT.Columns[10].Name = "GST%";
            HFIT.Columns[11].Name = "GSTVAL";
            HFIT.Columns[12].Name = "VAT%";
            HFIT.Columns[13].Name = "VATval";
            HFIT.Columns[14].Name = "Total";
            HFIT.Columns[15].Name = "Hsnuid";
            HFIT.Columns[16].Name = "SocNo";
            HFIT.Columns[17].Name = "Itemname";
            if(Text=="Yarn Purchase")
            {

                HFIT.Columns[18].Name = "No of Bags";

            }
            else if (Text == "Fabric Purchase")
            {
                HFIT.Columns[18].Name = "No of Rolls";

            }
            else if (Text == "Trims Purchase")
            {
                HFIT.Columns[18].Name = "No of Bags";

            }


            HFIT.Columns[19].Name = "Style";
            HFIT.Columns[20].Name = "Addnotes";
            HFIT.Columns[21].Name = "referid";
            if (Text == "Trims Purchase")
            {
                HFIT.Columns[0].Width = 320;

            }

             else
            {
                HFIT.Columns[0].Width = 250;

            }
            //HFIT.Columns[0].Width = 250;
            HFIT.Columns[1].Width = 40;
            HFIT.Columns[2].Width = 68;
            HFIT.Columns[3].Width = 60;
            HFIT.Columns[4].Width = 50;

            HFIT.Columns[5].Visible = false;
            HFIT.Columns[6].Visible = false;

            HFIT.Columns[7].Visible = false;

            HFIT.Columns[8].Visible = false;
            HFIT.Columns[9].Visible = false;
            HFIT.Columns[10].Width = 40;
            HFIT.Columns[11].Visible = false;
            HFIT.Columns[12].Visible = false;
            HFIT.Columns[13].Visible = false;
            HFIT.Columns[14].Visible = false;
            HFIT.Columns[15].Visible = false;
            HFIT.Columns[16].Width = 70;
            HFIT.Columns[17].Visible = false;
            if (Text == "Yarn Purchase" && Text == "Fabric Purchase")
            {
                HFIT.Columns[20].Visible = false;
               

            }
            else if (Text == "Trims Purchase")

            {

                HFIT.Columns[20].Width = 150;
            }
            
            HFIT.Columns[21].Visible = false;
            if (Text == "Yarn Purchase")
            {

                HFIT.Columns[18].Width = 70;

            }
            else if (Text == "Fabric Purchase")
            {
                HFIT.Columns[18].Width = 70;

            }
            else if (Text == "Trims Purchase")
            {
                HFIT.Columns[18].Visible = false;

            }
     
            HFIT.Columns[19].Width = 70;
        }

        private void Titletax()
        {
            HFGTAX.ColumnCount = 6;
            HFGTAX.Columns[0].Name = "Taxable Value";
            HFGTAX.Columns[1].Name = "SGST%";
            HFGTAX.Columns[2].Name = "SGST Value";
            HFGTAX.Columns[3].Name = "CGST%";
            HFGTAX.Columns[4].Name = "CGST Value";
            HFGTAX.Columns[0].Width = 100;
            HFGTAX.Columns[1].Width = 60;
            HFGTAX.Columns[2].Width = 100;
            HFGTAX.Columns[3].Width = 60;
            HFGTAX.Columns[4].Width = 100;
            HFGTAX.Columns[5].Name = "hsnid";
            HFGTAX.Columns[5].Visible = false;
        }

        private void Loadgrid1()
        {
            try
            {
                conn.Open();

                string quy = "select c.itemname,b.pqty,b.itemuid,refuid from transactionsp a inner join transactionsplist b on a.uid=b.transactionspuid and a.companyid=" + GeneralParameters.UserdId + " inner join itemm c on b.itemuid=c.uid where a.uid=" + uid + "";
                SqlCommand cmd = new SqlCommand(quy, conn);
                SqlDataAdapter aptr = new SqlDataAdapter(cmd);
                DataTable tap = new DataTable();
                aptr.Fill(tap);
                HFIT.AutoGenerateColumns = false;
                HFIT.Refresh();
                HFIT.DataSource = null;
                HFIT.Rows.Clear();
                i = 0;

                HFIT.ColumnCount = tap.Columns.Count;
           
                foreach (DataColumn column in tap.Columns)
                {
                    HFIT.Columns[i].Name = column.ColumnName;
                    HFIT.Columns[i].HeaderText = column.ColumnName;
                    HFIT.Columns[i].DataPropertyName = column.ColumnName;
                    i = i + 1;
                }



                HFIT.Columns[0].Width = 450;

                HFIT.Columns[1].Width = 75;
                HFIT.Columns[2].Visible = false;
                HFIT.Columns[3].Visible = false;
                HFIT.DataSource = tap;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }
            finally
            {
                conn.Close();
                Genclass.cmd.Dispose();
            }
        }
        private void button1_Click(object sender, System.EventArgs e)
        {
            mode = 1;
            Genpan.Visible = false;

            Genclass.ClearTextBox(this, Editpan);
            if (Dtype == 40)
            {
                Genclass.Gendocno();
                txtgrn.Text = Genclass.ST;
                label15.Visible = false;
                txtitemname.Visible = false;
                label14.Visible = false;
                txtprice.Visible = false;
                label31.Visible = false;
                txtqty.Visible = false;
                label32.Visible = false;
                txtbval.Visible = false;

            }
            else if (Dtype == 80)
            {
                Genclass.Gendocno();
                txtgrn.Text = Genclass.ST;
            }
            Editpan.Visible = true;
            DTPDOCDT.Focus();
            HFIT.Refresh();
            HFIT.DataSource = null;
            HFIT.Rows.Clear();

        }

        private void loadput()
        {
            conn.Close();
            conn.Open();



        }

        private Size New(int p1, int p2)
        {
            throw new NotImplementedException();
        }


        private void btnexit_Click(object sender, System.EventArgs e)
        {
            this.Dispose();
        }

        private void txtper_TextChanged(object sender, System.EventArgs e)
        {
            int val1;
            int val2;
            int val3;
            val1 = Convert.ToInt16(txttaxable.Text);
            val2 = Convert.ToInt16(txtper.Text);
            val3 = (val1 * val2) / 100;
            //txtdis.Text = Convert.ToString(val3);
            CalcNetAmt();

        }

        private void CalcNetAmt()
        {
            totamt = Convert.ToInt16(txttaxable.Text);
            strfin = TxtNetAmt.Text;
        }
                private void Taxduty()
        {
        }

        private void btnsave_Click(object sender, System.EventArgs e)
        {
            if (txtname.Text == "")
            {
                MessageBox.Show("Enter the Party");
                txtname.Focus();
            }

            if (conn.State != ConnectionState.Open)
            {
                conn.Open();
            }

            Genclass.TYPENAME = Text;
            Genclass.Dtype = 7;
            if (mode == 1)
            {
                Genclass.Gendocnolist();
                txtgrn.Text = Genclass.ST;
            }



            //Genclass.Dtype = 90;
            if (txttrans.Text == "")
            {
                txttrans.Text = "0";
            }
            if (txtveh.Text == "")
            {
                txtveh.Text = "0";
            }
            if (txttdis.Text == "")
            {
                txttdis.Text = "0";
            }
            if (txttdisc.Text == "")
            {
                txttdisc.Text = "0";
            }
            if (txtcharges.Text == "")
            {
                txtcharges.Text = "0";
            }
            if (txtdcno.Text == "")
            {
                txtdcno.Text = "0";
            }
            if (txtrem.Text == "")
            {
                txtrem.Text = "0";
            }
            if (mode == 2)
            {
                qur.CommandText = "delete from purchased where transactionspuid=" + uid + "";
                qur.ExecuteNonQuery();
            }
            for (int i = 0; i < HFIT.RowCount - 1; i++)
            {
                Txttot.Text = Txttot.Text.Replace(",", "");
                txtexcise.Text = txtexcise.Text.Replace(",", "");
                if (mode == 1)
                {

                    if (i == 0)
                    {
                        
                        SqlParameter[] para ={
                        new SqlParameter("@UID","0"),
                        new SqlParameter("@DOCTYPEID", Dtype),
                        new SqlParameter("@DOCNO",txtgrn.Text),
                        new SqlParameter("@DOCDATE", Convert.ToDateTime(DTPDOCDT.Text).ToString("yyyy-MM-dd")),
                        new SqlParameter("@DCNO", txtdcno.Text),
                        new SqlParameter("@DCDATE", Convert.ToDateTime(DTPDOCDT.Text).ToString("yyyy-MM-dd")),
                        new SqlParameter("@Partyuid",txtpuid.Text),
                        new SqlParameter("@Remarks", txtrem.Text),
                        new SqlParameter("@ACTIVE", "1"),
                        new SqlParameter("@companyid", GeneralParameters.UserdId),
                        new SqlParameter("@Netvalue", TxtNetAmt.Text),
                        new SqlParameter("@YEARID",Genclass.Yearid),
                        new SqlParameter("@Roff", TxtRoff.Text),
                        new SqlParameter("@dtpre", Convert.ToDateTime(DTPDOCDT.Text).ToString("yyyy-MM-dd")),
                        new SqlParameter("@dtrem", Convert.ToDateTime(DTPDOCDT.Text).ToString("yyyy-MM-dd")),
                        new SqlParameter("@Transp", txttrans.Text),
                        new SqlParameter("@Vehno", txtveh.Text),
                        new SqlParameter("@placeuid", txtpluid.Text),
                        new SqlParameter("@orderdt",'0'),
                        new SqlParameter("@TYPE",Text),
                        new SqlParameter("@Returnid",SqlDbType.Int),
                        new SqlParameter("@approved","Pending"),
                        new SqlParameter("@appdt", Convert.ToDateTime(reqdt.Text).ToString("yyyy-MM-dd"))



                };
                        para[20].Direction = ParameterDirection.Output;
                       int   uid = (db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_PurchaseM", para, conn, 20));
                        txtgrnid.Text = Convert.ToString(uid);

                    }
               


                    string quyt = "select isnull(f1,0) as f1 from   generalm where f1='" + HFIT.Rows[i].Cells[10].Value + "'";
                    SqlCommand cmd = new SqlCommand(quyt, conn);
                    SqlDataAdapter aptrt = new SqlDataAdapter(cmd);
                    DataTable tapt = new DataTable();
                    aptrt.Fill(tapt);
                   
                    if (tapt.Rows.Count > 0)
                    {
                        if (txtstid.Text == "196")
                        {
                            dd1 = Convert.ToDouble(tapt.Rows[0]["f1"].ToString()) / 2;

                            txttcgstp.Text = dd1.ToString("0.00");
                            if (txttdis.Text == "0")
                            {
                                dd2 = ((Convert.ToDouble(HFIT.Rows[i].Cells[4].Value) / 100) * Convert.ToDouble(tapt.Rows[0]["f1"].ToString())) / 2;
                                dd8 = 0;
                            }
                            else
                            {
                                dd8 = ((Convert.ToDouble(HFIT.Rows[i].Cells[4].Value) / 100) * Convert.ToDouble(txttdis.Text));

                                dd2 = (((Convert.ToDouble(HFIT.Rows[i].Cells[4].Value) - dd8) / 100) * Convert.ToDouble(tapt.Rows[0]["f1"].ToString())/2);
                              }
                                   txttcgval.Text = dd2.ToString("0.00");
                                   dd9 = Convert.ToDouble(HFIT.Rows[i].Cells[4].Value) - dd8;
                                   txtbval.Text = dd9.ToString("0.00");

      
                    if(HFIT.Rows[i].Cells[16].Value==null)
                            {
                                HFIT.Rows[i].Cells[16].Value = "0";

                            }

                            SqlParameter[] para1 ={

                        new SqlParameter("@DOCTYPEID", Dtype),
                        new SqlParameter("@TRANSACTIONSPUID",txtgrnid.Text),
                        new SqlParameter("@ITEMUID",HFIT.Rows[i].Cells[5].Value),
                        new SqlParameter("@PQTY", HFIT.Rows[i].Cells[3].Value),
                        new SqlParameter("@PRATE", HFIT.Rows[i].Cells[2].Value),
                        new SqlParameter("@BASICVALUE",HFIT.Rows[i].Cells[4].Value),
                        new SqlParameter("@TAXABLEVAL", txtbval.Text),
                        new SqlParameter("@REFUID",HFIT.Rows[i].Cells[6].Value),
                        new SqlParameter("@DISP", txttdis.Text),
                        new SqlParameter("@DISVAL",dd8),
                        new SqlParameter("@CGSTID",txttcgstp.Text),
                        new SqlParameter("@CGSTVAL",txttcgval.Text),
                        new SqlParameter("@SGSTID", txttcgstp.Text),
                        new SqlParameter("@SGSTVAL",txttcgval.Text),
                        new SqlParameter("@IGSTID", '0'),
                        new SqlParameter("@IGSTVAL",'0'),
                        new SqlParameter("@MODE", mode),
                        new SqlParameter("@TOTVALUE",TxtNetAmt.Text ),
                        new SqlParameter("@ADDNOTES",HFIT.Rows[i].Cells[16].Value),
                         new SqlParameter("@itemname",HFIT.Rows[i].Cells[0].Value),
                         new SqlParameter("@style",HFIT.Rows[i].Cells[19].Value),
                         new SqlParameter("@bags",HFIT.Rows[i].Cells[18].Value),
                            new SqlParameter("@socno",HFIT.Rows[i].Cells[20].Value),
                                       new SqlParameter("@billrate","0"),
                                                new SqlParameter("@referid",HFIT.Rows[i].Cells[21].Value),
                                                             new SqlParameter("@status","Open"),
                };

                            db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_purchased", para1, conn);
                            txtbval.Text = "";
                          }
                       else

                          {

                            if (HFIT.Rows[i].Cells[16].Value == null)
                            {
                                HFIT.Rows[i].Cells[16].Value = "0";

                            }


                            dd8 = ((Convert.ToDouble(HFIT.Rows[i].Cells[4].Value) / 100) * Convert.ToDouble(txttdis.Text));

                                        dd2 = (((Convert.ToDouble(HFIT.Rows[i].Cells[4].Value) - dd8) / 100) * Convert.ToDouble(tapt.Rows[0]["f1"].ToString()));
                                        txttcgstp.Text = tapt.Rows[0]["f1"].ToString();
                                        txttcgval.Text = dd2.ToString("0.00");
                                        dd9 = Convert.ToDouble(HFIT.Rows[i].Cells[4].Value) - dd8;
                                        txtbval.Text = dd9.ToString("0.00");
                                      


                            SqlParameter[] para1 ={



                        new SqlParameter("@DOCTYPEID", Dtype),
                        new SqlParameter("@TRANSACTIONSPUID",txtgrnid.Text),
                        new SqlParameter("@ITEMUID",HFIT.Rows[i].Cells[5].Value),
                        new SqlParameter("@PQTY", HFIT.Rows[i].Cells[3].Value),
                        new SqlParameter("@PRATE", HFIT.Rows[i].Cells[2].Value),
                        new SqlParameter("@BASICVALUE",HFIT.Rows[i].Cells[4].Value),
                        new SqlParameter("@TAXABLEVAL", txtbval.Text),
                        new SqlParameter("@REFUID",HFIT.Rows[i].Cells[6].Value),
                        new SqlParameter("@DISP", txttdis.Text),
                        new SqlParameter("@DISVAL",dd8),
                        new SqlParameter("@CGSTID",'0'),
                        new SqlParameter("@CGSTVAL",'0'),
                        new SqlParameter("@SGSTID",'0' ),
                        new SqlParameter("@SGSTVAL",'0'),
                        new SqlParameter("@IGSTID", txttcgstp.Text),
                        new SqlParameter("@IGSTVAL",txttcgval.Text),
                        new SqlParameter("@MODE", mode),
                        new SqlParameter("@TOTVALUE",TxtNetAmt.Text ),
                        new SqlParameter("@ADDNOTES",HFIT.Rows[i].Cells[16].Value),
                        new SqlParameter("@itemname",HFIT.Rows[i].Cells[0].Value),
                          new SqlParameter("@style",HFIT.Rows[i].Cells[19].Value),
                         new SqlParameter("@bags",HFIT.Rows[i].Cells[18].Value),
                               new SqlParameter("@socno",HFIT.Rows[i].Cells[20].Value),
                                 new SqlParameter("@billrate","0"),
                                          new SqlParameter("@referid",HFIT.Rows[i].Cells[21].Value),
                                                new SqlParameter("@status","Open"),

                };

                            db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_purchased", para1, conn);
                        }



                        }

                }



                if (mode == 2)
                {





                    SqlParameter[] para ={
                   new SqlParameter("@DOCTYPEID", Dtype),
                        new SqlParameter("@DOCNO",txtgrn.Text),
                        new SqlParameter("@DOCDATE", Convert.ToDateTime(DTPDOCDT.Text).ToString("yyyy-MM-dd")),
                        new SqlParameter("@DCNO", txtdcno.Text),
                        new SqlParameter("@DCDATE", Convert.ToDateTime(DTPDOCDT.Text).ToString("yyyy-MM-dd")),
                        new SqlParameter("@Partyuid",txtpuid.Text),
                        new SqlParameter("@Remarks", txtrem.Text),
                        new SqlParameter("@ACTIVE", "1"),
                        new SqlParameter("@companyid", GeneralParameters.UserdId),
                        new SqlParameter("@Netvalue", TxtNetAmt.Text),
                        new SqlParameter("@YEARID",Genclass.Yearid),
                        new SqlParameter("@Roff", TxtRoff.Text),
                        new SqlParameter("@dtpre", Convert.ToDateTime(DTPDOCDT.Text).ToString("yyyy-MM-dd")),
                        new SqlParameter("@dtrem", Convert.ToDateTime(DTPDOCDT.Text).ToString("yyyy-MM-dd")),
                        new SqlParameter("@Transp", txttrans.Text),
                        new SqlParameter("@Vehno", txtveh.Text),
                        new SqlParameter("@placeuid", txtpluid.Text),
                        new SqlParameter("@orderdt",'0'),
                        new SqlParameter("@TYPE",Text),
                        new SqlParameter("@UID", txtgrnid.Text),
                       new SqlParameter("@approved","Pending"),
                        new SqlParameter("@appdt", Convert.ToDateTime(reqdt.Text).ToString("yyyy-MM-dd"))




                };

                db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_PurchaseUpdate", para, conn);


                    string quyq = "select isnull(f1,0) as f1 from   generalm where f1='" + HFIT.Rows[i].Cells[10].Value + "'";
                    SqlCommand cmd = new SqlCommand(quyq, conn);
                    SqlDataAdapter aptrq = new SqlDataAdapter(cmd);
                    DataTable tapq = new DataTable();
                    aptrq.Fill(tapq);

                    if (tapq.Rows.Count > 0)
                    {
                        if (txtstid.Text == "196")
                        {
                            dd1 = Convert.ToDouble(tapq.Rows[0]["f1"].ToString()) / 2;

                            txttcgstp.Text = dd1.ToString("0.00");
                            if (txttdis.Text == "0")
                            {
                                dd2 = ((Convert.ToDouble(HFIT.Rows[i].Cells[4].Value) / 100) * Convert.ToDouble(tapq.Rows[0]["f1"].ToString())) / 2;
                                dd8 = 0;
                            }
                            else
                            {
                                dd8 = ((Convert.ToDouble(HFIT.Rows[i].Cells[4].Value) / 100) * Convert.ToDouble(txttdis.Text));

                                dd2 = (((Convert.ToDouble(HFIT.Rows[i].Cells[4].Value) - dd8) / 100) * Convert.ToDouble(tapq.Rows[0]["f1"].ToString()) / 2);
                            }
                            txttcgval.Text = dd2.ToString("0.00");
                            dd9 = Convert.ToDouble(HFIT.Rows[i].Cells[4].Value) - dd8;
                            txtbval.Text = dd9.ToString("0.00");
                            txtexcise.Text = txtexcise.Text.Replace(",", "");


                            if (HFIT.Rows[i].Cells[16].Value == null)
                            {
                                HFIT.Rows[i].Cells[16].Value = "0";

                            }

                            SqlParameter[] para1 ={

                        new SqlParameter("@DOCTYPEID", Dtype),
                        new SqlParameter("@TRANSACTIONSPUID",txtgrnid.Text),
                        new SqlParameter("@ITEMUID",HFIT.Rows[i].Cells[5].Value),
                        new SqlParameter("@PQTY", HFIT.Rows[i].Cells[3].Value),
                        new SqlParameter("@PRATE", HFIT.Rows[i].Cells[2].Value),
                        new SqlParameter("@BASICVALUE",HFIT.Rows[i].Cells[4].Value),
                        new SqlParameter("@TAXABLEVAL", txtbval.Text),
                        new SqlParameter("@REFUID",HFIT.Rows[i].Cells[6].Value),
                        new SqlParameter("@DISP", txttdis.Text),
                        new SqlParameter("@DISVAL",dd8),
                        new SqlParameter("@CGSTID",txttcgstp.Text),
                        new SqlParameter("@CGSTVAL",txttcgval.Text),
                        new SqlParameter("@SGSTID", txttcgstp.Text),
                        new SqlParameter("@SGSTVAL",txttcgval.Text),
                        new SqlParameter("@IGSTID", '0'),
                        new SqlParameter("@IGSTVAL",'0'),
                        new SqlParameter("@MODE", mode),
                        new SqlParameter("@TOTVALUE",TxtNetAmt.Text ),
                        new SqlParameter("@ADDNOTES",HFIT.Rows[i].Cells[16].Value),
                        new SqlParameter("@itemname",HFIT.Rows[i].Cells[0].Value),
                      new SqlParameter("@style",HFIT.Rows[i].Cells[19].Value),
                         new SqlParameter("@bags",HFIT.Rows[i].Cells[18].Value),
                               new SqlParameter("@socno",HFIT.Rows[i].Cells[20].Value),
                                 new SqlParameter("@billrate","0"),
                                          new SqlParameter("@referid",HFIT.Rows[i].Cells[21].Value),
                                                new SqlParameter("@status","Open"),

                };

                            db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_purchased", para1, conn);
                            txtbval.Text = "";
                        }
                        else
                        {
                            dd1 = Convert.ToDouble(tapq.Rows[0]["f1"].ToString());

                            txttcgstp.Text = dd1.ToString("0.00");

                            dd8 = ((Convert.ToDouble(HFIT.Rows[i].Cells[4].Value) / 100) * Convert.ToDouble(txttdis.Text));

                            dd2 = (((Convert.ToDouble(HFIT.Rows[i].Cells[4].Value) - dd8) / 100) * Convert.ToDouble(tapq.Rows[0]["f1"].ToString()));
                            txttcgval.Text = dd2.ToString("0.00");
                            dd9 = Convert.ToDouble(HFIT.Rows[i].Cells[4].Value) - dd8;
                            txtbval.Text = dd9.ToString("0.00");
                            txtexcise.Text = txtexcise.Text.Replace(",", "");

                            if (HFIT.Rows[i].Cells[16].Value == null)
                            {
                                HFIT.Rows[i].Cells[16].Value = "0";

                            }

                            SqlParameter[] para1 ={



                        new SqlParameter("@DOCTYPEID", Dtype),
                        new SqlParameter("@TRANSACTIONSPUID",txtgrnid.Text),
                        new SqlParameter("@ITEMUID",HFIT.Rows[i].Cells[5].Value),
                        new SqlParameter("@PQTY", HFIT.Rows[i].Cells[3].Value),
                        new SqlParameter("@PRATE", HFIT.Rows[i].Cells[2].Value),
                        new SqlParameter("@BASICVALUE",HFIT.Rows[i].Cells[4].Value),
                        new SqlParameter("@TAXABLEVAL", txtbval.Text),
                        new SqlParameter("@REFUID",HFIT.Rows[i].Cells[6].Value),
                        new SqlParameter("@DISP", txttdis.Text),
                        new SqlParameter("@DISVAL",dd8),
                        new SqlParameter("@CGSTID",'0'),
                        new SqlParameter("@CGSTVAL",'0'),
                        new SqlParameter("@SGSTID",'0' ),
                        new SqlParameter("@SGSTVAL",'0'),
                        new SqlParameter("@IGSTID", txttcgstp.Text),
                        new SqlParameter("@IGSTVAL",txttcgval.Text),
                        new SqlParameter("@MODE", mode),
                        new SqlParameter("@TOTVALUE",TxtNetAmt.Text ),
                        new SqlParameter("@ADDNOTES",HFIT.Rows[i].Cells[16].Value),
                        new SqlParameter("@itemname",HFIT.Rows[i].Cells[0].Value),
                      new SqlParameter("@style",HFIT.Rows[i].Cells[19].Value),
                         new SqlParameter("@bags",HFIT.Rows[i].Cells[18].Value),
                               new SqlParameter("@socno",HFIT.Rows[i].Cells[20].Value),
                                 new SqlParameter("@billrate","0"),
                                          new SqlParameter("@referid",HFIT.Rows[i].Cells[21].Value),
                                                new SqlParameter("@status","Open"),

                };

                            db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_purchased", para1, conn);
                            txtbval.Text = ""; 


                        }
                    }
                }
            }

            conn.Close();
            conn.Open();
                    qur.CommandText = "delete from  purchasecharges where puid=" + txtgrnid.Text + "";
                    qur.ExecuteNonQuery();

                    if (txttdis.Text == "" || txttdis.Text == null)
                    {
                        txttdis.Text = "0";
                        txttdisc.Text = "0";
                    }

                
                    if (txtcharges.Text != "")
                    {
           

                    SqlParameter[] para1 ={
                 

                    new SqlParameter("@puid",  txtgrnid.Text),
                    new SqlParameter("@DOCTYPEID",Dtype),
                    new SqlParameter("@CHARGESUID", 13),

                    new SqlParameter("@CHARGEAMOUNT",txtcharges.Text),
                    new SqlParameter("@TOTALCHARGES", txtcharges.Text),


                     new SqlParameter("@ADDITIONAL", '0'),
                      new SqlParameter("@YEARID",  Genclass.Yearid),


                };

                db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_purchasecharges", para1, conn);
            }

                    if (mode == 1)
                    {
                conn.Close();
                conn.Open();
                        qur.CommandText = "update doctypemlist set lastno= lastno + 1 where doctypeid=" + Dtype + " and finyear='19-20'  and prfix='" + Genclass.prfix + "'"; 
                        qur.ExecuteNonQuery();
                    }



                    MessageBox.Show("Record has been saved", "Save", MessageBoxButtons.OK);

                
            

             
                addipan.Visible = false;
                Editpan.Visible = false;
                Taxpan.Visible = false;

                panadd.Visible = true;
                Genpan.Visible = true;
                sum5 = 0;
                j = -1;

            buttnfinbk_Click_1(sender, e);

        }


        private void cbotax_Click(object sender, System.EventArgs e)
        {
           
            CalcNetAmt();
            
        }


        private void txtdcno_TextChanged(object sender, EventArgs e)
        {


           
        }



        private void button4_Click(object sender, EventArgs e)
        {
            Editpan.Visible = false;
            Genpan.Visible = true;
        }

        private void HFIT_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            //int i = HFIT.SelectedCells[0].RowIndex;
            //HFIT.Rows[i].Cells[4].Value = Convert.ToDouble(HFIT.Rows[i].Cells[2].Value) * Convert.ToDouble(HFIT.Rows[i].Cells[3].Value);
        }

        private void button6_Click(object sender, EventArgs e)
        {

        }

        private void button8_Click(object sender, EventArgs e)
        {
            Editpan.Visible = false;
            Taxpan.Visible = true;
            txtexcise.Text = Txttot.Text;
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button9_Click(object sender, EventArgs e)
        {
            //termspan.Visible = false;
            addipan.Visible = true;
            button9.Visible = false;
            button10.Visible = true;

        }

        private void button10_Click(object sender, EventArgs e)
        {

        }

        private void txtexcise_TextChanged(object sender, EventArgs e)
        {

        }

        private void Taxpan_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button10_Click_1(object sender, EventArgs e)
        {

        }

        //private void panel1_Paint(object sender, PaintEventArgs e)
        //{

        //}
        private void TitleAdd()
        {
            HFGA.ColumnCount = 3;
            HFGA.Columns[0].Name = "Additional Charge";
            HFGA.Columns[1].Name = "Amount";
            HFGA.Columns[2].Name = "uid";

            HFGA.Columns[0].Width = 334;
            HFGA.Columns[1].Width = 90;
            HFGA.Columns[2].Visible = false;
        }

        private void Titleterm()
        {
            HFGT.ColumnCount = 3;
            HFGT.Columns[0].Name = "Terms";
            HFGT.Columns[1].Name = "Term Description";
            HFGT.Columns[2].Name = "uid";

            HFGT.Columns[0].Width = 300;
            HFGT.Columns[1].Width = 310;
            HFGT.Columns[2].Visible = false;



        }

        private void txtterms_KeyDown(object sender, KeyEventArgs e)
        {
          
        }

        private void txtterms_TextChanged(object sender, EventArgs e)
        {

        }

        private void button11_Click(object sender, EventArgs e)
        {

            HFGT.AllowUserToAddRows = true;
            HFGT.AutoGenerateColumns = true;
            var index = HFGT.Rows.Add();
            HFGT.Rows[index].Cells[0].Value = txtterms.Text;
            HFGT.Rows[index].Cells[1].Value = txtremde.Text;
            HFGT.Rows[index].Cells[2].Value = txttermid.Text;

        }

        private void button12_Click(object sender, EventArgs e)
        {

        }

        private void button10_Click_2(object sender, EventArgs e)
        {
            addipan.Visible = false;
            termspan.Visible = true;
            button9.Visible = true;
            button10.Visible = false;
        }

        private void txtaddcharge_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtaddcharge_KeyDown(object sender, KeyEventArgs e)
        {
           
        }

        private void txttotaddd_TextChanged(object sender, EventArgs e)
        {
            if (txttotaddd.Text == "0" || txttotaddd.Text == "")
            {
                TxtNetAmt.Text = txtexcise.Text;
            }
            else
            {
                TxtNetAmt.Text = "0";
                val2 = Convert.ToDouble(txtexcise.Text) + Convert.ToDouble(txttotaddd.Text);
                TxtNetAmt.Text = val2.ToString();

            }
        }

        private void btnedit_Click(object sender, EventArgs e)
        {
            buttnfinbk.Visible = true;
            HFIT.AutoGenerateColumns = false;
            HFIT.Refresh();
            HFIT.DataSource = null;
            HFIT.Rows.Clear();
            HFGA.AutoGenerateColumns = false;
            HFGA.Refresh();
            HFGA.DataSource = null;
            HFGA.Rows.Clear();
            HFGT.AutoGenerateColumns = false;
            HFGT.Refresh();
            HFGT.DataSource = null;
            HFGT.Rows.Clear();
            HFGST.Refresh();
            HFGST.DataSource = null;
            HFGST.Rows.Clear();
            mode = 2;
            dis3 = 0;
            dis4 = 0;
            str1key = "F2";
            txttdis.Text = "0";
            txtexcise.Text = "0";
            txtigval.Text = "0";
            HFIT.Columns[12].ReadOnly = true;
            HFIT.Columns[13].ReadOnly = true;
            Genpan.Visible = false;
            Editpan.Visible = true;
            lkppnl.Visible = false;
            int i = HFGP.SelectedCells[0].RowIndex;
            uid = HFGP.Rows[i].Cells[0].Value.ToString();
            txtgrnid.Text = HFGP.Rows[i].Cells[0].Value.ToString();
            txtgrn.Text = HFGP.Rows[i].Cells[1].Value.ToString();
            DTPDOCDT.Text = HFGP.Rows[i].Cells[2].Value.ToString();
            reqdt.Text = HFGP.Rows[i].Cells[16].Value.ToString();
            txtdcno.Text = HFGP.Rows[i].Cells[3].Value.ToString();
            Dtpdt.Text = HFGP.Rows[i].Cells[4].Value.ToString();
            txtname.Text = HFGP.Rows[i].Cells[5].Value.ToString();
            txtpuid.Text = HFGP.Rows[i].Cells[7].Value.ToString();
            txtveh.Text = HFGP.Rows[i].Cells[13].Value.ToString();
            txtrem.Text = HFGP.Rows[i].Cells[14].Value.ToString();

            if (HFGP.Rows[i].Cells[15].Value.ToString() != "Approved")
            {
                conn.Close();
                conn.Open();
                {

                    if (Text == "Yarn Purchase")
                    {
                        Genclass.strsql = " SP_GETPUREDIT  " + txtgrnid.Text + " ";
                        Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
             
                    }
                    else if (Text == "Trims Purchase")
                    {
                        Genclass.strsql = " SP_GETPUREDITTRIMS  " + txtgrnid.Text + " ";
                        Genclass.cmd = new SqlCommand(Genclass.strsql, conn);

                    }
                    else if (Text == "Fabric Purchase")
                    {
                        Genclass.strsql = " SP_GETPUREDITFABRIC  " + txtgrnid.Text + " ";
                        Genclass.cmd = new SqlCommand(Genclass.strsql, conn);

                    }
                    SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
                    DataTable tap1 = new DataTable();
                    aptr1.Fill(tap1);
                    sum1 = 0;
                    sum5 = 0;
                    sumb = 0; 
                    uy = 0;
                    uy1 = 0;

                    Txttot.Text = "";
                    for (int k = 0; k < tap1.Rows.Count; k++)
                    {
                        var index = HFIT.Rows.Add();
                        HFIT.Rows[index].Cells[0].Value = tap1.Rows[k]["itemname"].ToString();
                        HFIT.Rows[index].Cells[1].Value = tap1.Rows[k]["UOM"].ToString();
                        HFIT.Rows[index].Cells[2].Value = tap1.Rows[k]["PRate"].ToString();
                        HFIT.Rows[index].Cells[3].Value = tap1.Rows[k]["pqty"].ToString();
                        sumb = Convert.ToDouble(tap1.Rows[k]["BasicValue"].ToString());
                        HFIT.Rows[index].Cells[4].Value = sumb.ToString("0.00");
                        txtbval.Text = sumb.ToString("0.00");
                        HFIT.Rows[index].Cells[5].Value = tap1.Rows[k]["itemuid"].ToString();
                        HFIT.Rows[index].Cells[6].Value = tap1.Rows[k]["refuid"].ToString();
                        HFIT.Rows[index].Cells[7].Value = tap1.Rows[k]["disp"].ToString();
                        HFIT.Rows[index].Cells[8].Value = tap1.Rows[k]["disval"].ToString();
                        HFIT.Rows[index].Cells[9].Value = tap1.Rows[k]["Taxableval"].ToString();
                        if (txtstid.Text == "196")
                        {
                             uy = Convert.ToDouble(tap1.Rows[k]["Sgstid"].ToString()) * 2;
                            HFIT.Rows[index].Cells[10].Value = uy.ToString();
                             uy1 = Convert.ToDouble(tap1.Rows[k]["sgstval"].ToString()) * 2;
                            HFIT.Rows[index].Cells[11].Value = uy1.ToString();
                        }
                        else
                        {
                            HFIT.Rows[index].Cells[10].Value = tap1.Rows[k]["igstid"].ToString();
                            HFIT.Rows[index].Cells[11].Value = tap1.Rows[k]["igstval"].ToString();
                        }


                        HFIT.Rows[index].Cells[12].Value = tap1.Rows[k]["Sgstid"].ToString();
                        HFIT.Rows[index].Cells[13].Value = tap1.Rows[k]["Sgstval"].ToString();
                        HFIT.Rows[index].Cells[14].Value = tap1.Rows[k]["totvalue"].ToString();
                        HFIT.Rows[index].Cells[15].Value = tap1.Rows[k]["uid"].ToString();
                        HFIT.Rows[index].Cells[16].Value = tap1.Rows[k]["addnotes"].ToString();
                        HFIT.Rows[index].Cells[18].Value = tap1.Rows[k]["bags"].ToString();
                        HFIT.Rows[index].Cells[19].Value = tap1.Rows[k]["style"].ToString();
                        HFIT.Rows[index].Cells[20].Value = tap1.Rows[k]["socno"].ToString();
                        HFIT.Rows[index].Cells[21].Value = tap1.Rows[k]["referid"].ToString();
                        if (txtstid.Text == "196")
                        {
                            dis4 = Convert.ToDouble(tap1.Rows[k]["Sgstid"].ToString()) * 2;
                            dis3 = Convert.ToDouble(tap1.Rows[k]["Sgstval"].ToString()) * 2;
                        }
                        else
                        {
                            dis4 = Convert.ToDouble(tap1.Rows[k]["igstid"].ToString());
                            dis3 = Convert.ToDouble(tap1.Rows[k]["igstval"].ToString());
                        }

                        sum1 = sum1 + Convert.ToDouble(HFIT.Rows[index].Cells[4].Value.ToString());

                    }

                    //Txttot.Text = Genclass.sum1.ToString();

                    Titlep();

                    btnaddrcan.Visible = false;


                    string strsql = "SP_GETPUREDITCHARGE  " + txtgrnid.Text + " ";
                    SqlCommand cmd = new SqlCommand(strsql, conn);
                    SqlDataAdapter aptr2 = new SqlDataAdapter(cmd);
                    DataTable tap2 = new DataTable();
                    aptr2.Fill(tap2);
                    if (tap2.Rows.Count > 0)
                    {
                        txtcharges.Text = tap2.Rows[0]["chargeamount"].ToString();
                    }

                    if (tap1.Rows.Count > 0)
                    {
                        txttbval.Text = Txttot.Text;
                        txttdis.Text = tap1.Rows[0]["disp"].ToString();
                   

                    }
                    else
                    {
                        txttdis_TextChanged_1(sender, e);
                    }
                }
            }
            else
            {


                MessageBox.Show("Already this Purchase Order Approved  Cont Edit");
                return;


            }

            if (HFGP.Rows[i].Cells[15].Value.ToString() == "Approved")

            {

                Editpan.Visible = false;
                Genpan.Visible = true;
                panadd.Visible = true;
                LoadGetJobCard(1);
            }
         
        }




        private void txtscr1_TextChanged(object sender, EventArgs e)
        {
            bs.Filter = string.Format("Docno Like '%{0}%' or  DocDate Like '%{1}%' or  Dcno Like '%{1}%'  or  Dcdate Like '%{1}%'     or  Name Like '%{1}%'   or  Status Like '%{1}%'    ",  txtscr1.Text, txtscr1.Text, txtscr1.Text, txtscr1.Text, txtscr1.Text, txtscr1.Text);
        }

      
               
        private void Txtscr2_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void Txtscr3_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void txtscr4_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void txtscr5_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void HFGP_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void chkact_CheckedChanged(object sender, EventArgs e)
        {
            LoadGetJobCard(1);
        }

        private void button6_Click_1(object sender, EventArgs e)
        {
            mode = 1;
            Genpan.Visible = false;
          
            Genclass.ClearTextBox(this, Editpan);
            cboprocess.Enabled = true;
            label15.Visible = true;
            txtitemname.Visible = true;
            label14.Visible = true;
            txtprice.Visible = true;
            label31.Visible = true;
            txtqty.Visible = true;
            label32.Visible = true;
            txtbval.Visible = true;
            buttcusok.Visible = true;
            //}
            Editpan.Visible = true;
            btnsave.Visible = true;
            btnaddrcan.Visible = false;
            buttnnxt.Visible = false;
            buttnfinbk.Visible = true;
            button11.Visible = true;
            button12.Visible = true;
            DTPDOCDT.Focus();
            HFIT.Refresh();
            HFIT.DataSource = null;
            HFIT.Rows.Clear();
            HFGST.Refresh();
            HFGST.DataSource = null;
            HFGST.Rows.Clear();
            sum = 1;
            lkppnl.Visible = false;
          
            panadd.Visible = false;
            Titlep();
            str1key = "F2";
            Editpan.Width = 1280;
            Editpan.Height = 618;

            if(Text=="Trims Purchase")
            {
                label64.Visible = false;
                txtbags.Visible = false;
            }
            

        }

        private void butedit_Click(object sender, EventArgs e)
        {

            mode = 2;
            panadd.Visible = false;
            Genpan.Visible = false;
            Taxpan.Visible = false;
            Editpan.Visible = true;
     
            label15.Visible = true;
            txtitemname.Visible = true;
            label14.Visible = true;
            txtprice.Visible = true;
            label31.Visible = true;
            txtqty.Visible = true;
            label32.Visible = true;
            txtbval.Visible = true;
            //buttcusok.Visible = true;
            buttnfinbk.Visible = true;
            btnsave.Visible = true;

            int i = HFGP.SelectedCells[0].RowIndex;
            uid = HFGP.Rows[i].Cells[0].Value.ToString();
            txtgrnid.Text = HFGP.Rows[i].Cells[0].Value.ToString();
            txtgrn.Text = HFGP.Rows[i].Cells[1].Value.ToString();
            DTPDOCDT.Text = HFGP.Rows[i].Cells[2].Value.ToString();
            txtdcno.Text = HFGP.Rows[i].Cells[3].Value.ToString();
            Dtpdt.Text = HFGP.Rows[i].Cells[4].Value.ToString();
            txtname.Text = HFGP.Rows[i].Cells[5].Value.ToString();
            txtpuid.Text = HFGP.Rows[i].Cells[7].Value.ToString();
            txtpluid.Text = HFGP.Rows[i].Cells[8].Value.ToString();
            txtplace.Text = HFGP.Rows[i].Cells[9].Value.ToString();
            Dtppre.Text = HFGP.Rows[i].Cells[10].Value.ToString();
            Dtprem.Text = HFGP.Rows[i].Cells[11].Value.ToString();
            txttrans.Text = HFGP.Rows[i].Cells[12].Value.ToString();
            txtveh.Text = HFGP.Rows[i].Cells[13].Value.ToString();
            txtrem.Text = HFGP.Rows[i].Cells[14].Value.ToString();

            conn.Open();
            //HFIT.AutoGenerateColumns = false;

            HFIT.Refresh();
            HFIT.DataSource = null;
            HFIT.Rows.Clear();

            //HFGA.AutoGenerateColumns = false;
            HFGA.Refresh();
            HFGA.DataSource = null;
            HFGA.Rows.Clear();
            //HFGT.AutoGenerateColumns = false;
            HFGT.Refresh();
            HFGT.DataSource = null;
            HFGT.Rows.Clear();


            {
                string strsql = " select distinct b.uid,c.ItemName,d.GeneralName as uom,PRate,pqty,BasicValue,ItemUId,b.refuid,disp,disval,Taxableval,igstid,igstval,totvalue,addnotes,isnull(chargeamount,0) as chargeamount from stransactionsp a inner join Stransactionsplist b on a.uid=b.transactionspuid  left join itemm c  on b.itemuid=c.uid left join generalm d on c.UOM_UId=d.Uid left join sTransactionsPcharges e on a.uid=e.stransactionspuid where b.transactionspuid=" + uid + " and b.doctypeid=90 ";
                SqlCommand cmd = new SqlCommand(strsql, conn);
                SqlDataAdapter aptr1 = new SqlDataAdapter(cmd);
                 DataTable tap1 = new DataTable();
                aptr1.Fill(tap1);
           
                sum1 = 0;
                Txttot.Text = "";
                for (int k = 0; k < tap1.Rows.Count; k++)
                {
                    var index = HFIT.Rows.Add();
                    HFIT.Rows[index].Cells[0].Value = tap1.Rows[k]["itemname"].ToString();
                    HFIT.Rows[index].Cells[1].Value = tap1.Rows[k]["UOM"].ToString();
                    HFIT.Rows[index].Cells[2].Value = tap1.Rows[k]["PRate"].ToString();
                    HFIT.Rows[index].Cells[3].Value = tap1.Rows[k]["pqty"].ToString();
                    HFIT.Rows[index].Cells[4].Value = tap1.Rows[k]["BasicValue"].ToString();
                    HFIT.Rows[index].Cells[5].Value = tap1.Rows[k]["itemuid"].ToString();
                    HFIT.Rows[index].Cells[6].Value = tap1.Rows[k]["refuid"].ToString();
                    //HFIT.Rows[index].Cells[7].Value = tap1.Rows[k]["disp"].ToString();
                    //HFIT.Rows[index].Cells[8].Value = tap1.Rows[k]["disval"].ToString();
                    //HFIT.Rows[index].Cells[9].Value = tap1.Rows[k]["Taxableval"].ToString();
                    HFIT.Rows[index].Cells[10].Value = tap1.Rows[k]["igstid"].ToString();
                    HFIT.Rows[index].Cells[11].Value = tap1.Rows[k]["igstval"].ToString();
                    //HFIT.Rows[index].Cells[12].Value = tap1.Rows[k]["Sgstid"].ToString();
                    //HFIT.Rows[index].Cells[13].Value = tap1.Rows[k]["Sgstval"].ToString();
                    HFIT.Rows[index].Cells[14].Value = tap1.Rows[k]["totvalue"].ToString();
                    HFIT.Rows[index].Cells[15].Value = tap1.Rows[k]["uid"].ToString();
                    HFIT.Rows[index].Cells[16].Value = tap1.Rows[k]["addnotes"].ToString();

                    sum1 = sum1 + Convert.ToDouble(HFIT.Rows[k].Cells[4].Value);
                    Txttot.Text = sum1.ToString();
                    //txtexcise.Text = Genclass.sum1.ToString();
                }

                txttbval.Text = Txttot.Text;
                if (tap1.Rows.Count > 0)
                {
                    txttdis.Text = tap1.Rows[0]["disp"].ToString();
                    txttdisc.Text = tap1.Rows[0]["disval"].ToString();
                    txtcharges.Text = tap1.Rows[0]["chargeamount"].ToString();
                    //splittax();
                }
                Titlep();
                //btnsave.Visible = false;
                btnaddrcan.Visible = false;

                conn.Close();
            }
            conn.Open();
            {
                string strsql = "select * from Stransactionspcharges a left join generalm b on a.chargesuid=b.uid  where a.stransactionspuid=" + txtgrnid.Text + " ";
                SqlCommand cmd = new SqlCommand(strsql, conn);
                SqlDataAdapter aptr2 = new SqlDataAdapter(cmd);
                DataTable tap2 = new DataTable();
                aptr2.Fill(tap2);
                if (tap2.Rows.Count > 0)
                {
                    txtcharges.Text = tap2.Rows[0]["chargeamount"].ToString();
                }
               
                conn.Close();
            }
            conn.Open();
            {
                string strsql = "select * from STransactionsPterms   where stransactionspuid=" + txtgrnid.Text + " ";
                SqlCommand cmd = new SqlCommand(strsql, conn);
                SqlDataAdapter aptr3 = new SqlDataAdapter(cmd);
                DataTable tap3 = new DataTable();
                aptr3.Fill(tap3);
                val2 = 0;
                for (int g = 0; g < tap3.Rows.Count; g++)
                {
                    var index = HFGT.Rows.Add();
                    HFGT.Rows[index].Cells[0].Value = tap3.Rows[g]["termsuid"].ToString();
                    HFGT.Rows[index].Cells[1].Value = tap3.Rows[g]["termsdesc"].ToString();
        


                }
                conn.Close();
                val2 = Convert.ToDouble(txtexcise.Text) + Convert.ToDouble(txtcharges.Text);
                TxtNetAmt.Text = val2.ToString();
            }
        }

        private void buttnext1_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void HFGP_CellMouseUp(object sender, DataGridViewCellMouseEventArgs e)
        {
            
        }

        private void HFGP_KeyUp(object sender, KeyEventArgs e)
        {
            
        }

        private void buttnnvfst_Click(object sender, EventArgs e)
        {
        }

        private void buttnnxtlft_Click(object sender, EventArgs e)
        {
            
        }

        private void buttrnxt_Click(object sender, EventArgs e)
        {
           
        }

        private void btnfinnxt_Click(object sender, EventArgs e)
        {
           

        }

        private void txtscr6_TextChanged(object sender, EventArgs e)
        {
           
        }

        private void btnadd_Click(object sender, EventArgs e)
        {

            for (int i = 0; i < HFGA.RowCount - 1; i++)
            {
                if (HFGA.Rows[i].Cells[2].Value.ToString() == txtaddid.Text)
                {
                    MessageBox.Show("Charge Already Added");
                    txtaddcharge.Text = "";
                    txtcharges.Text = "";
                    txtaddcharge.Focus();
                    return;
                }
            }

            HFGA.AllowUserToAddRows = true;
            HFGA.AutoGenerateColumns = true;
            var index = HFGA.Rows.Add();
            HFGA.Rows[index].Cells[0].Value = txtaddcharge.Text;
            HFGA.Rows[index].Cells[1].Value = txtcharges.Text;
            HFGA.Rows[index].Cells[2].Value = txtaddid.Text;




            sum1 = sum1 + Convert.ToDouble(txtcharges.Text);
            txttotaddd.Text = sum1.ToString();
            txttotc.Text = sum1.ToString();
            splittax();
            txtaddcharge.Text = "";
            txtcharges.Text = "";
            txtaddcharge.Focus();
        }



        private void button11_Click_2(object sender, EventArgs e)
        {

            if (Txttot.Text == "" || Txttot.Text == null)
            {
                MessageBox.Show("Enter the item to proceed");
                return;
            }
            Taxpan.Visible = true;
            btnsave.Visible = false;
            btnaddrcan.Visible = false;

            txtexcise.Text = Txttot.Text;
            //button9.Visible = false;
            addipan.Visible = false;
            //termspan.Visible = false;
            button11.Visible = false;
            button12.Visible = false;
            btnsave.Visible = true;
            buttnnxt.Visible = false;
            buttnfinbk.Visible = true;
            HFGT.Visible = false;
            label34.Visible = false;
            Editpan.Visible = true;
            txtexcise.Text = Txttot.Text;
            //Titletax();
            sum1 = 0;
            conn.Open();
           
            txttbval.Text = Txttot.Text;

            splittax();


            conn.Close();
        }

        private void splittax()
        {
            if (txttdis.Text == "" || txttdis.Text == null || txttdis.Text == "0")
            {
                txttprdval.Text = txttbval.Text;

            }
            else
            {
                dis6 = Convert.ToDouble(txttbval.Text) / 100 * Convert.ToDouble(txttdis.Text);
                txttdisc.Text = dis6.ToString("0.00");

                dis4 = Convert.ToDouble(txttbval.Text) - Convert.ToDouble(txttdisc.Text);
                txttprdval.Text = dis4.ToString("#,0.00");
            }

            if (txtcharges.Text == "" || txtcharges.Text == null || txtcharges.Text == "0")
            {
                txtexcise.Text = txttprdval.Text;
            }
            else
            {
                dis4 = Convert.ToDouble(txttprdval.Text) + Convert.ToDouble(txtcharges.Text);
                txtexcise.Text = dis4.ToString("#,0.00");
            }




            Genclass.strsql = "select generalname from partym a inner join generalm b on a.stateuid=b.uid where a.companyid=" + GeneralParameters.UserdId + " and a.uid=" + txtpuid.Text + "";

            Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
            SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
            DataTable tap1 = new DataTable();
            aptr1.Fill(tap1);

            //MessageBox.Show(tap1.Rows[0]["generalname"].ToString());
            if (tap1.Rows[0]["generalname"].ToString() == "Tamil Nadu 33")
            {
                txttgstp.Text = HFIT.Rows[0].Cells[10].Value.ToString();
                txttgstval.Text = HFIT.Rows[0].Cells[11].Value.ToString();
                dis4 = Convert.ToDouble(HFIT.Rows[0].Cells[10].Value.ToString()) / 2;
                txttcgstp.Text = dis4.ToString("#,0.00");
                txtsgstp.Text = dis4.ToString("#,0.00");
                dis3 = (Convert.ToDouble(txtexcise.Text) / 100) * (Convert.ToDouble(HFIT.Rows[0].Cells[10].Value.ToString()));
                dis3 = dis3 / 2;
                txttcgval.Text = dis3.ToString("#,0.00");
                txttsgval.Text = dis3.ToString("#,0.00");
                dis5 = Convert.ToDouble(txtexcise.Text) + dis3 + dis3;
                txtttot.Text = dis5.ToString("#,0.00");


            }
            else
            {
                txttgstp.Text = HFIT.Rows[0].Cells[10].Value.ToString();
                txttgstval.Text = HFIT.Rows[0].Cells[11].Value.ToString();
                txtigstp.Text = HFIT.Rows[0].Cells[10].Value.ToString();
                dis3 = (Convert.ToDouble(txtexcise.Text) / 100) * (Convert.ToDouble(HFIT.Rows[0].Cells[10].Value.ToString()));
                txtigval.Text = dis3.ToString("#,0.00");
                dis5 = Convert.ToDouble(txtexcise.Text) + dis3;
                txtttot.Text = dis5.ToString("#,0.00");
            }

            sum2 = 0;
            if (txtstid.Text == "24")
            {
                for (m = 0; m < HFGST.RowCount - 1; m++)
                {
                    sum2 = sum2 + Convert.ToDouble(HFGST.Rows[m].Cells[4].Value);
                    txttaxtot.Text = sum2.ToString();
                    txtigval.Text = sum2.ToString();
                }
            }
            else
            {

                for (m = 0; m < HFGST.RowCount - 1; m++)
                {
                    sum2 = sum2 + Convert.ToDouble(HFGST.Rows[m].Cells[1].Value);
                    txttaxtot.Text = sum2.ToString();
                    txtigval.Text = sum2.ToString();
                }
            }


            net1 = Convert.ToDouble(txtttot.Text);
            TxtNetAmt.Text = net1.ToString("#,0.00");
            someInt = (int)net1;

            rof = Math.Round(net1 - someInt, 2);
            TxtRoff.Text = rof.ToString("#,0.00");

            if (Convert.ToDouble(TxtRoff.Text) < 0.49)
            {
                rof1 = -1 * Convert.ToDouble(TxtRoff.Text);
                TxtRoff.Text = rof1.ToString("#,0.00");
            }
            else
            {
                rof2 = 1 - Convert.ToDouble(TxtRoff.Text);
                TxtRoff.Text = rof2.ToString("#,0.00");
            }

            net = Convert.ToDouble(TxtNetAmt.Text) + Convert.ToDouble(TxtRoff.Text);
            //int ne=Convert.ToInt16(net);
            TxtNetAmt.Text = net.ToString("#,0.00");

            TxtNetAmt.Text = TxtNetAmt.Text.Replace(",", "");
            txttcgval.Text = txttcgval.Text.Replace(",", "");
            txttsgval.Text = txttsgval.Text.Replace(",", "");
            txtigval.Text = txtigval.Text.Replace(",", "");
            txtttot.Text = txtttot.Text.Replace(",", "");
            txttprdval.Text = txttprdval.Text.Replace(",", "");
            txtexcise.Text = txtexcise.Text.Replace(",", "");
            txttdisc.Text = txttdisc.Text.Replace(",", "");

        }

        private void button12_Click_2(object sender, EventArgs e)
        {
            Editpan.Visible = false;
            Genpan.Visible = true;
            panadd.Visible = true;
            LoadGetJobCard(1);
        }

        private void button7_Click(object sender, EventArgs e)
        {

            for (int i = 0; i < HFGT.RowCount - 1; i++)
            {
                if (HFGT.Rows[i].Cells[2].Value.ToString() == txttermid.Text)
                {
                    MessageBox.Show("Terms Already Added");
                    txtterms.Text = "";
                    txtremde.Text = "";
                    txtterms.Focus();
                    return;
                }
            }

            HFGT.AllowUserToAddRows = true;
            HFGT.AutoGenerateColumns = true;
            var index = HFGT.Rows.Add();
            HFGT.Rows[index].Cells[0].Value = txtterms.Text;
            HFGT.Rows[index].Cells[1].Value = txtremde.Text;
            HFGT.Rows[index].Cells[2].Value = txttermid.Text;

            txtterms.Text = "";
            txtremde.Text = "";
            txtterms.Focus();
        }

        private void btnaddrcan_Click_1(object sender, EventArgs e)
        {
            addipan.Visible = true;
            //termspan.Visible = false;
            Editpan.Visible = false;
            //button1.Visible = false;
            btnaddrcan.Visible = false;
            btnsave.Visible = false;
            buttnfinbk.Visible = true;
            buttnnxt.Visible = true;
        }


        private void txtqty_TextChanged(object sender, EventArgs e)
        {
            if (txtqty.Text != "")
            {
                bval = Convert.ToDouble(txtqty.Text) * Convert.ToDouble(txtprice.Text);
                txtbval.Text = bval.ToString();
            }

        }



        private void buttnnxt_Click(object sender, EventArgs e)
        {
            addipan.Visible = false;
            termspan.Visible = true;
            buttnfinbk.Visible = false;
            buttnnxt.Visible = false;
            btnaddrcan.Visible = true;
            btnsave.Visible = true;

            //HFGT.
            if (HFGT.Rows[0].Cells[0].Value == "" || HFGT.Rows[0].Cells[0].Value == null)
            {
                var index = HFGT.Rows.Add();
                HFGT.Rows[index].Cells[0].Value = "No.of Cases";

                var index1 = HFGT.Rows.Add();
                HFGT.Rows[index1].Cells[0].Value = "Box No/Size";
                var index2 = HFGT.Rows.Add();
                HFGT.Rows[index2].Cells[0].Value = "Gross Weight";
                var index3 = HFGT.Rows.Add();
                HFGT.Rows[index3].Cells[0].Value = "Carrier";
                var index4 = HFGT.Rows.Add();
                HFGT.Rows[index4].Cells[0].Value = "Destination";
                var index5 = HFGT.Rows.Add();
                HFGT.Rows[index5].Cells[0].Value = "LR/RR/RPPNO./DT";
            }
            HFGT.Columns[0].ReadOnly = true;
        }

        private void buttnfinbk_Click(object sender, EventArgs e)
        {
            addipan.Visible = false;
            Taxpan.Visible = false;
            //termspan.Visible = false;
            buttnfinbk.Visible = false;
            buttnnxt.Visible = false;
            //button11.Visible = true;
            //button12.Visible = true;
            panadd.Visible = true;
            Editpan.Visible = false;
            Genpan.Visible = true;

            TxtNetAmt.Text = Txttot.Text;

        }

        private void button1_Click_1(object sender, EventArgs e)
        {

                 Genclass.Dtype = 7;

            Genclass.address = Text;

            if (HFGP.CurrentRow.Cells[15].Value.ToString() == "Approved")
            {
                conn.Close();
                conn.Open();

                qur.CommandText = "delete from  NotoWords";
                qur.ExecuteNonQuery();

                Int64 NumVal = Convert.ToInt64(HFGP.CurrentRow.Cells[6].Value);
                string Nw = Rupees(NumVal);

                qur.CommandText = "Insert into NotoWords values('" + Nw + "',1)";
                qur.ExecuteNonQuery();

                Genclass.Prtid = Convert.ToInt16(HFGP.CurrentRow.Cells[0].Value.ToString());


                for (int i = 1; i < 4; i++)
                {
                    Genclass.slno = i;
                    Crviewer crv = new Crviewer();
                    if (Text == "Yarn Purchase")
                    {
                        string strsql = "select * from Vw_salprtexlfin   where muid=" + Genclass.Prtid + "  and doctypeid=7";
                        SqlCommand cmd = new SqlCommand(strsql, conn);
                        SqlDataAdapter aptr = new SqlDataAdapter(cmd);
                         DataTable tap = new DataTable();
                        aptr.Fill(tap);
                        if (tap.Rows.Count > 0)
                        {
                            if (tap.Rows[0]["bstate"].ToString() == "Tamilnadu")
                            {


                                SqlDataAdapter da = new SqlDataAdapter("sp_pdf", conn);
                                da.SelectCommand.CommandType = CommandType.StoredProcedure;
                                da.SelectCommand.Parameters.Add("@Muid", SqlDbType.Int).Value = Genclass.Prtid;
                                da.SelectCommand.Parameters.Add("@slno", SqlDbType.Int).Value = Genclass.slno;
                                da.SelectCommand.Parameters.Add("@companyid", SqlDbType.Int).Value = GeneralParameters.UserdId;
                                da.SelectCommand.Parameters.Add("@doctypeid", SqlDbType.Int).Value = 7;
                                DataSet ds = new DataSet();
                                da.Fill(ds, "salesinvoice");

                          
                                    doc.Load(Application.StartupPath + "\\PurchaseOrdertemp.rpt");
                               

                                SqlDataAdapter da2 = new SqlDataAdapter("SP_SGSTTAX", conn);
                                da2.SelectCommand.CommandType = CommandType.StoredProcedure;
                                da2.SelectCommand.Parameters.Add("@uid", SqlDbType.Int).Value = Genclass.Prtid;

                                da2.SelectCommand.Parameters.Add("@doctypeid", SqlDbType.Int).Value = 7;
                                DataTable ds2 = new DataTable("SGSTTAX");
                                da2.Fill(ds2);
                                ds.Tables.Add(ds2);

                                doc.Subreports["CrySgst.rpt"].SetDataSource(ds2);
                                doc.SetDataSource(ds);


                                SqlDataAdapter da1 = new SqlDataAdapter("terms", conn);
                                da1.SelectCommand.CommandType = CommandType.StoredProcedure;
                                da1.SelectCommand.Parameters.Add("@uid", SqlDbType.Int).Value = Genclass.Prtid;
                                DataTable ds1 = new DataTable("Terms");
                                da1.Fill(ds1);
                                ds.Tables.Add(ds1);





                                doc.Subreports["Termrpt1.rpt"].SetDataSource(ds1);
                                doc.SetDataSource(ds);


                            }

                            else
                            {
                                SqlDataAdapter da = new SqlDataAdapter("sp_pdfOTHERSTATE", conn);
                                da.SelectCommand.CommandType = CommandType.StoredProcedure;
                                da.SelectCommand.Parameters.Add("@Muid", SqlDbType.Int).Value = Genclass.Prtid;
                                //da.SelectCommand.Parameters.Add("@type", SqlDbType.Int).Value = 2;
                                da.SelectCommand.Parameters.Add("@slno", SqlDbType.Int).Value = Genclass.slno;
                                da.SelectCommand.Parameters.Add("@companyid", SqlDbType.Int).Value = GeneralParameters.UserdId;
                                da.SelectCommand.Parameters.Add("@doctypeid", SqlDbType.Int).Value = 7;
                                DataSet ds = new DataSet();
                                da.Fill(ds, "salesinvoice");
                               
                                    doc.Load(Application.StartupPath + "\\PurchaseOrdertemp.rpt");
                            
                               

                                SqlDataAdapter da2 = new SqlDataAdapter("SP_IGSTTAX", conn);
                                da2.SelectCommand.CommandType = CommandType.StoredProcedure;
                                da2.SelectCommand.Parameters.Add("@uid", SqlDbType.Int).Value = Genclass.Prtid;
                                da2.SelectCommand.Parameters.Add("@doctypeid", SqlDbType.Int).Value = 7;
                                DataTable ds2 = new DataTable("IGSTTAX");
                                da2.Fill(ds2);
                                ds.Tables.Add(ds2);

                                doc.Subreports["Cryigst.rpt"].SetDataSource(ds2);
                                doc.SetDataSource(ds);


                                SqlDataAdapter da1 = new SqlDataAdapter("terms", conn);
                                da1.SelectCommand.CommandType = CommandType.StoredProcedure;
                                da1.SelectCommand.Parameters.Add("@uid", SqlDbType.Int).Value = Genclass.Prtid;
                                DataTable ds1 = new DataTable("Terms");
                                da1.Fill(ds1);
                                ds.Tables.Add(ds1);




                                doc.Subreports["Termrpt1.rpt"].SetDataSource(ds1);
                                doc.SetDataSource(ds);

                            }
                        }
                    }
                    else if (Text == "Fabric Purchase")
                    {

                        string strsql = "select * from Vw_salprtexlfintrims   where muid=" + Genclass.Prtid + "  and doctypeid=7";
                        SqlCommand cmd = new SqlCommand(strsql, conn);
                        SqlDataAdapter aptr = new SqlDataAdapter(cmd);
                        DataTable tap = new DataTable();
                        aptr.Fill(tap);
                        if (tap.Rows.Count > 0)
                        {
                            if (tap.Rows[0]["bstate"].ToString() == "Tamilnadu")
                            {


                                SqlDataAdapter da = new SqlDataAdapter("sp_pdfFabric", conn);
                                da.SelectCommand.CommandType = CommandType.StoredProcedure;
                                da.SelectCommand.Parameters.Add("@Muid", SqlDbType.Int).Value = Genclass.Prtid;
                                da.SelectCommand.Parameters.Add("@slno", SqlDbType.Int).Value = Genclass.slno;
                                da.SelectCommand.Parameters.Add("@companyid", SqlDbType.Int).Value = GeneralParameters.UserdId;
                                da.SelectCommand.Parameters.Add("@doctypeid", SqlDbType.Int).Value = 7;
                                DataSet ds = new DataSet();
                                da.Fill(ds, "salesinvoice");

                              
                                    doc.Load(Application.StartupPath + "\\PurchaseOrdertemp.rpt");
                               


                                SqlDataAdapter da2 = new SqlDataAdapter("SP_SGSTTAX", conn);
                                da2.SelectCommand.CommandType = CommandType.StoredProcedure;
                                da2.SelectCommand.Parameters.Add("@uid", SqlDbType.Int).Value = Genclass.Prtid;

                                da2.SelectCommand.Parameters.Add("@doctypeid", SqlDbType.Int).Value = 7;
                                DataTable ds2 = new DataTable("SGSTTAX");
                                da2.Fill(ds2);
                                ds.Tables.Add(ds2);

                                doc.Subreports["CrySgst.rpt"].SetDataSource(ds2);
                                doc.SetDataSource(ds);



                                SqlDataAdapter da1 = new SqlDataAdapter("terms", conn);
                                da1.SelectCommand.CommandType = CommandType.StoredProcedure;
                                da1.SelectCommand.Parameters.Add("@uid", SqlDbType.Int).Value = Genclass.Prtid;
                                DataTable ds1 = new DataTable("Terms");
                                da1.Fill(ds1);
                                ds.Tables.Add(ds1);





                                doc.Subreports["Termrpt1.rpt"].SetDataSource(ds1);
                                doc.SetDataSource(ds);


                            }

                            else
                            {
                                SqlDataAdapter da = new SqlDataAdapter("sp_pdfOTHERSTATEFabric", conn);
                                da.SelectCommand.Connection = conn;
                                da.SelectCommand.CommandType = CommandType.StoredProcedure;
                                da.SelectCommand.Parameters.Add("@Muid", SqlDbType.Int).Value = Genclass.Prtid;
                                //da.SelectCommand.Parameters.Add("@type", SqlDbType.Int).Value = 2;
                                da.SelectCommand.Parameters.Add("@slno", SqlDbType.Int).Value = Genclass.slno;
                                da.SelectCommand.Parameters.Add("@companyid", SqlDbType.Int).Value = GeneralParameters.UserdId;
                                da.SelectCommand.Parameters.Add("@doctypeid", SqlDbType.Int).Value = 7;
                                DataSet ds = new DataSet();
                                da.Fill(ds, "salesinvoice");
                               
                                    doc.Load(Application.StartupPath + "\\PurchaseOrdertemp.rpt");
                                
                                
                                SqlDataAdapter da2 = new SqlDataAdapter("SP_IGSTTAX", conn);
                                da2.SelectCommand.CommandType = CommandType.StoredProcedure;
                                da2.SelectCommand.Parameters.Add("@uid", SqlDbType.Int).Value = Genclass.Prtid;
                                da2.SelectCommand.Parameters.Add("@doctypeid", SqlDbType.Int).Value = 7;
                                DataTable ds2 = new DataTable("IGSTTAX");
                                da2.Fill(ds2);
                                ds.Tables.Add(ds2);
                                doc.Subreports["Cryigst.rpt"].SetDataSource(ds2);
                                doc.SetDataSource(ds);


                                SqlDataAdapter da1 = new SqlDataAdapter("terms", conn);
                                da1.SelectCommand.CommandType = CommandType.StoredProcedure;
                                da1.SelectCommand.Parameters.Add("@uid", SqlDbType.Int).Value = Genclass.Prtid;
                                DataTable ds1 = new DataTable("Terms");
                                da1.Fill(ds1);
                                ds.Tables.Add(ds1);




                                doc.Subreports["Termrpt1.rpt"].SetDataSource(ds1);
                                doc.SetDataSource(ds);

                            }
                        }


                    }
                    else if (Text == "Trims Purchase")
                    {

                        string strsql = "select * from Vw_salprtexlfintrims   where muid=" + Genclass.Prtid + "  and doctypeid=7";
                        SqlCommand cmd = new SqlCommand(strsql, conn);
                        SqlDataAdapter aptr = new SqlDataAdapter(cmd);
                        DataTable tap = new DataTable();
                        aptr.Fill(tap);
                        if (tap.Rows.Count > 0)
                        {
                            if (tap.Rows[0]["bstate"].ToString() == "Tamilnadu")
                            {


                                SqlDataAdapter da = new SqlDataAdapter("sp_PurchaseTrims", conn);
                                da.SelectCommand.CommandType = CommandType.StoredProcedure;
                                da.SelectCommand.Parameters.Add("@Muid", SqlDbType.Int).Value = Genclass.Prtid;
                                da.SelectCommand.Parameters.Add("@slno", SqlDbType.Int).Value = Genclass.slno;
                                da.SelectCommand.Parameters.Add("@companyid", SqlDbType.Int).Value = GeneralParameters.UserdId;
                                da.SelectCommand.Parameters.Add("@doctypeid", SqlDbType.Int).Value = 7;
                                DataSet ds = new DataSet();
                                da.Fill(ds, "salesinvoice");


                                doc.Load(Application.StartupPath + "\\PurchaseOrdertemp.rpt");



                                SqlDataAdapter da2 = new SqlDataAdapter("SP_SGSTTAX", conn);
                                da2.SelectCommand.CommandType = CommandType.StoredProcedure;
                                da2.SelectCommand.Parameters.Add("@uid", SqlDbType.Int).Value = Genclass.Prtid;

                                da2.SelectCommand.Parameters.Add("@doctypeid", SqlDbType.Int).Value = 7;
                                DataTable ds2 = new DataTable("SGSTTAX");
                                da2.Fill(ds2);
                                ds.Tables.Add(ds2);

                                doc.Subreports["CrySgst.rpt"].SetDataSource(ds2);
                                doc.SetDataSource(ds);



                                SqlDataAdapter da1 = new SqlDataAdapter("terms", conn);
                                da1.SelectCommand.CommandType = CommandType.StoredProcedure;
                                da1.SelectCommand.Parameters.Add("@uid", SqlDbType.Int).Value = Genclass.Prtid;
                                DataTable ds1 = new DataTable("Terms");
                                da1.Fill(ds1);
                                ds.Tables.Add(ds1);





                                doc.Subreports["Termrpt1.rpt"].SetDataSource(ds1);
                                doc.SetDataSource(ds);


                            }

                            else
                            {
                                SqlDataAdapter da = new SqlDataAdapter("sp_otherstateTrims", conn);
                                da.SelectCommand.Connection = conn;
                                da.SelectCommand.CommandType = CommandType.StoredProcedure;
                                da.SelectCommand.Parameters.Add("@Muid", SqlDbType.Int).Value = Genclass.Prtid;
                                //da.SelectCommand.Parameters.Add("@type", SqlDbType.Int).Value = 2;
                                da.SelectCommand.Parameters.Add("@slno", SqlDbType.Int).Value = Genclass.slno;
                                da.SelectCommand.Parameters.Add("@companyid", SqlDbType.Int).Value = GeneralParameters.UserdId;
                                da.SelectCommand.Parameters.Add("@doctypeid", SqlDbType.Int).Value = 7;
                                DataSet ds = new DataSet();
                                da.Fill(ds, "salesinvoice");

                                doc.Load(Application.StartupPath + "\\PurchaseOrdertemp.rpt");


                                SqlDataAdapter da2 = new SqlDataAdapter("SP_IGSTTAX", conn);
                                da2.SelectCommand.CommandType = CommandType.StoredProcedure;
                                da2.SelectCommand.Parameters.Add("@uid", SqlDbType.Int).Value = Genclass.Prtid;
                                da2.SelectCommand.Parameters.Add("@doctypeid", SqlDbType.Int).Value = 7;
                                DataTable ds2 = new DataTable("IGSTTAX");
                                da2.Fill(ds2);
                                ds.Tables.Add(ds2);
                                doc.Subreports["Cryigst.rpt"].SetDataSource(ds2);
                                doc.SetDataSource(ds);


                                SqlDataAdapter da1 = new SqlDataAdapter("terms", conn);
                                da1.SelectCommand.CommandType = CommandType.StoredProcedure;
                                da1.SelectCommand.Parameters.Add("@uid", SqlDbType.Int).Value = Genclass.Prtid;
                                DataTable ds1 = new DataTable("Terms");
                                da1.Fill(ds1);
                                ds.Tables.Add(ds1);




                                doc.Subreports["Termrpt1.rpt"].SetDataSource(ds1);
                                doc.SetDataSource(ds);

                            }
                        }


                    }
                    doc.PrintToPrinter(1, false, 0, 0);
                }
            }
            else

            {

                MessageBox.Show("Purchase Order Pending");
                return;
            }

        }


        public string Rupees(Int64 rup)
        {
            string result = "";
            Int64 res;
            if ((rup / 10000000) > 0)
            {
                res = rup / 10000000;
                rup = rup % 10000000;
                result = result + ' ' + RupeesToWords(res) + " Crore";
            }
            if ((rup / 100000) > 0)
            {
                res = rup / 100000;
                rup = rup % 100000;
                result = result + ' ' + RupeesToWords(res) + " Lakh";
            }
            if ((rup / 1000) > 0)
            {
                res = rup / 1000;
                rup = rup % 1000;
                result = result + ' ' + RupeesToWords(res) + " Thousand";
            }


            if ((rup / 100) > 0)
            {
                res = rup / 100;
                rup = rup % 100;
                if (rup > 0)
                {
                    result = result + ' ' + RupeesToWords(res) + " Hundred and";
                }
                else
                {
                    result = result + ' ' + RupeesToWords(res) + " Hundred";
                }
            }
            if ((rup % 10) >= 0)
            {
                res = rup % 100;
                result = result + " " + RupeesToWords(res);
            }
            result = "Rupees " + result + ' ' + "only";
            return result;
        }

        public string RupeesToWords(Int64 rup)
        {
            string result = "";
            if ((rup >= 1) && (rup <= 10))
            {
                if ((rup % 10) == 1) result = "One";
                if ((rup % 10) == 2) result = "Two";
                if ((rup % 10) == 3) result = "Three";
                if ((rup % 10) == 4) result = "Four";
                if ((rup % 10) == 5) result = "Five";
                if ((rup % 10) == 6) result = "Six";
                if ((rup % 10) == 7) result = "Seven";
                if ((rup % 10) == 8) result = "Eight";
                if ((rup % 10) == 9) result = "Nine";
                if ((rup % 10) == 0) result = "Ten";
            }
            if (rup > 9 && rup < 20)
            {
                if (rup == 11) result = "Eleven";
                if (rup == 12) result = "Twelve";
                if (rup == 13) result = "Thirteen";
                if (rup == 14) result = "Forteen";
                if (rup == 15) result = "Fifteen";
                if (rup == 16) result = "Sixteen";
                if (rup == 17) result = "Seventeen";
                if (rup == 18) result = "Eighteen";
                if (rup == 19) result = "Nineteen";
            }

            if (rup >= 20 && (rup / 10) == 2 && (rup % 10) == 0) result = "Twenty";
            if (rup > 20 && (rup / 10) == 3 && (rup % 10) == 0) result = "Thirty";
            if (rup > 20 && (rup / 10) == 4 && (rup % 10) == 0) result = "Forty";
            if (rup > 20 && (rup / 10) == 5 && (rup % 10) == 0) result = "Fifty";
            if (rup > 20 && (rup / 10) == 6 && (rup % 10) == 0) result = "Sixty";
            if (rup > 20 && (rup / 10) == 7 && (rup % 10) == 0) result = "Seventy";
            if (rup > 20 && (rup / 10) == 8 && (rup % 10) == 0) result = "Eighty";
            if (rup > 20 && (rup / 10) == 9 && (rup % 10) == 0) result = "Ninty";

            if (rup > 20 && (rup / 10) == 2 && (rup % 10) != 0)
            {
                if ((rup % 10) == 1) result = "Twenty One";
                if ((rup % 10) == 2) result = "Twenty Two";
                if ((rup % 10) == 3) result = "Twenty Three";
                if ((rup % 10) == 4) result = "Twenty Four";
                if ((rup % 10) == 5) result = "Twenty Five";
                if ((rup % 10) == 6) result = "Twenty Six";
                if ((rup % 10) == 7) result = "Twenty Seven";
                if ((rup % 10) == 8) result = "Twenty Eight";
                if ((rup % 10) == 9) result = "Twenty Nine";
            }
            if (rup > 20 && (rup / 10) == 3 && (rup % 10) != 0)
            {
                if ((rup % 10) == 1) result = "Thirty One";
                if ((rup % 10) == 2) result = "Thirty Two";
                if ((rup % 10) == 3) result = "Thirty Three";
                if ((rup % 10) == 4) result = "Thirty Four";
                if ((rup % 10) == 5) result = "Thirty Five";
                if ((rup % 10) == 6) result = "Thirty Six";
                if ((rup % 10) == 7) result = "Thirty Seven";
                if ((rup % 10) == 8) result = "Thirty Eight";
                if ((rup % 10) == 9) result = "Thirty Nine";
            }
            if (rup > 20 && (rup / 10) == 4 && (rup % 10) != 0)
            {
                if ((rup % 10) == 1) result = "Forty One";
                if ((rup % 10) == 2) result = "Forty Two";
                if ((rup % 10) == 3) result = "Forty Three";
                if ((rup % 10) == 4) result = "Forty Four";
                if ((rup % 10) == 5) result = "Forty Five";
                if ((rup % 10) == 6) result = "Forty Six";
                if ((rup % 10) == 7) result = "Forty Seven";
                if ((rup % 10) == 8) result = "Forty Eight";
                if ((rup % 10) == 9) result = "Forty Nine";
            }
            if (rup > 20 && (rup / 10) == 5 && (rup % 10) != 0)
            {
                if ((rup % 10) == 1) result = "Fifty One";
                if ((rup % 10) == 2) result = "Fifty Two";
                if ((rup % 10) == 3) result = "Fifty Three";
                if ((rup % 10) == 4) result = "Fifty Four";
                if ((rup % 10) == 5) result = "Fifty Five";
                if ((rup % 10) == 6) result = "Fifty Six";
                if ((rup % 10) == 7) result = "Fifty Seven";
                if ((rup % 10) == 8) result = "Fifty Eight";
                if ((rup % 10) == 9) result = "Fifty Nine";
            }
            if (rup > 20 && (rup / 10) == 6 && (rup % 10) != 0)
            {
                if ((rup % 10) == 1) result = "Sixty One";
                if ((rup % 10) == 2) result = "Sixty Two";
                if ((rup % 10) == 3) result = "Sixty Three";
                if ((rup % 10) == 4) result = "Sixty Four";
                if ((rup % 10) == 5) result = "Sixty Five";
                if ((rup % 10) == 6) result = "Sixty Six";
                if ((rup % 10) == 7) result = "Sixty Seven";
                if ((rup % 10) == 8) result = "Sixty Eight";
                if ((rup % 10) == 9) result = "Sixty Nine";
            }
            if (rup > 20 && (rup / 10) == 7 && (rup % 10) != 0)
            {
                if ((rup % 10) == 1) result = "Seventy One";
                if ((rup % 10) == 2) result = "Seventy Two";
                if ((rup % 10) == 3) result = "Seventy Three";
                if ((rup % 10) == 4) result = "Seventy Four";
                if ((rup % 10) == 5) result = "Seventy Five";
                if ((rup % 10) == 6) result = "Seventy Six";
                if ((rup % 10) == 7) result = "Seventy Seven";
                if ((rup % 10) == 8) result = "Seventy Eight";
                if ((rup % 10) == 9) result = "Seventy Nine";
            }
            if (rup > 20 && (rup / 10) == 8 && (rup % 10) != 0)
            {
                if ((rup % 10) == 1) result = "Eighty One";
                if ((rup % 10) == 2) result = "Eighty Two";
                if ((rup % 10) == 3) result = "Eighty Three";
                if ((rup % 10) == 4) result = "Eighty Four";
                if ((rup % 10) == 5) result = "Eighty Five";
                if ((rup % 10) == 6) result = "Eighty Six";
                if ((rup % 10) == 7) result = "Eighty Seven";
                if ((rup % 10) == 8) result = "Eighty Eight";
                if ((rup % 10) == 9) result = "Eighty Nine";
            }
            if (rup > 20 && (rup / 10) == 9 && (rup % 10) != 0)
            {
                if ((rup % 10) == 1) result = "Ninty One";
                if ((rup % 10) == 2) result = "Ninty Two";
                if ((rup % 10) == 3) result = "Ninty Three";
                if ((rup % 10) == 4) result = "Ninty Four";
                if ((rup % 10) == 5) result = "Ninty Five";
                if ((rup % 10) == 6) result = "Ninty Six";
                if ((rup % 10) == 7) result = "Ninty Seven";
                if ((rup % 10) == 8) result = "Ninty Eight";
                if ((rup % 10) == 9) result = "Ninty Nine";
            }
            return result;
        }


        private void txtname_KeyDown_1(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.F10)
                {
                    grSearch.Visible = false;
                }
                else if (e.KeyCode == Keys.F2)
                {
                    SelectId = 1;
                    int Index = DataGridCommon.SelectedCells[0].RowIndex;
                  
                        txtname.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                        txtpuid.Text = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    

                    grSearch.Visible = false;
                    SelectId = 0;
                }
                else if (e.KeyCode == Keys.Escape)
                {
                    grSearch.Visible = false;
                }
                else if (e.KeyValue == 40)
                {
                    DataGridCommon.Select();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtdcno_KeyDown_1(object sender, KeyEventArgs e)
        {
           

        }

        private void txtdcid_TextChanged_1(object sender, EventArgs e)
        {

            if (Dtype == 90)
            {

                if (txtdcno.Text == "")
                {
                    return;

                }
                else
                {
                    string strsql = "select Itemname,UoM,itemuid,refid,qty,PRICE,convert(decimal(18,2),BasicValue,105) AS BasicValue,disper,convert(decimal(18,2),Disvalue,105) AS Disvalue,convert(decimal(18,2),Taxablevalue,105) AS Taxablevalue,convert(decimal(18,2),igstval,105) AS igstval,ig,0 as gid,0 as gt,convert(decimal(18,2),(BasicValue-(isnull(Disvalue,0))+ (isnull(igstval,0))),105) as total,hsnid from (select distinct   c.itemname,d.generalname as uom,b.itemuid,b.uid as refid,b.pqty-isnull(sum(e.pqty),0) as qty,f.Price ,(b.pqty-isnull(sum(e.pqty),0)) * f.Price as BasicValue,disper,((b.pqty-isnull(sum(e.pqty),0)) * f.Price/100)* disper as Disvalue,((b.pqty-isnull(sum(e.pqty),0)) * f.Price)-(((b.pqty-isnull(sum(e.pqty),0)) * f.Price/100)* disper) as Taxablevalue,i.f1 as ig,(((b.pqty-isnull(sum(e.pqty),0)) * f.Price)-(((b.pqty-isnull(sum(e.pqty),0)) * f.Price/100)* disper))/100 * i.f1 as igstval,c.hsnid from transactionsp a inner join transactionsplist b on  a.uid=b.transactionspuid and a.doctypeid=20 and a.companyid=1 inner join itemm c on b.itemuid=c.uid left join generalm d on c.uom_uid=d.uid  left join transactionsplist e on  b.uid=e.refuid and e.doctypeid=40 inner join pur_price_list f on c.uid=f.itemuid and c.partyuid=f.suppuid left join generalm i on f.igstid=i.uid where a.docno='" + txtdcno.Text + "' group by c.itemname,d.generalname,b.itemuid,b.uid,b.pqty,f.price,disper,i.f1,c.hsnid having b.pqty-isnull(sum(e.pqty),0) >0) tab";
                    SqlCommand cmd = new SqlCommand(strsql, conn);
                    SqlDataAdapter aptr1 = new SqlDataAdapter(cmd);
                     DataTable tap1 = new DataTable();
                    aptr1.Fill(tap1);
                    //txtname.Text = tap1.Rows[0]["Name"].ToString();
                    //txtpuid.Text = tap1.Rows[0]["Partyuid"].ToString();
                    for (int i = 0; i < tap1.Rows.Count; i++)
                    {
                        var index = HFIT.Rows.Add();



                        HFIT.Rows[index].Cells[0].Value = tap1.Rows[i]["itemname"].ToString();
                        HFIT.Rows[index].Cells[1].Value = tap1.Rows[i]["UOM"].ToString();
                        HFIT.Rows[index].Cells[2].Value = tap1.Rows[i]["Price"].ToString();
                        HFIT.Rows[index].Cells[3].Value = tap1.Rows[i]["qty"].ToString();
                        HFIT.Rows[index].Cells[4].Value = tap1.Rows[i]["BasicValue"].ToString();
                        HFIT.Rows[index].Cells[5].Value = tap1.Rows[i]["itemuid"].ToString();
                        HFIT.Rows[index].Cells[6].Value = tap1.Rows[i]["refid"].ToString();
                        HFIT.Rows[index].Cells[7].Value = tap1.Rows[i]["disper"].ToString();
                        HFIT.Rows[index].Cells[8].Value = tap1.Rows[i]["Disvalue"].ToString();
                        HFIT.Rows[index].Cells[9].Value = tap1.Rows[i]["Taxablevalue"].ToString();
                        HFIT.Rows[index].Cells[10].Value = tap1.Rows[i]["ig"].ToString();
                        HFIT.Rows[index].Cells[11].Value = tap1.Rows[i]["igstval"].ToString();
                        HFIT.Rows[index].Cells[12].Value = tap1.Rows[i]["gid"].ToString();
                        HFIT.Rows[index].Cells[13].Value = tap1.Rows[i]["gt"].ToString();
                        HFIT.Rows[index].Cells[14].Value = tap1.Rows[i]["Total"].ToString();
                        HFIT.Rows[index].Cells[15].Value = tap1.Rows[i]["hsnid"].ToString();

                        sum1 = sum1 + Convert.ToDouble(HFIT.Rows[i].Cells[14].Value);
                        Txttot.Text = sum1.ToString();




                        //Titlep();



                    }

                }


            }
            else
            {
                txtitemname.Focus();

            }
        }

        private void TxtNetAmt_TextChanged(object sender, EventArgs e)
        {

        }

        private void buttcusok_Click_1(object sender, EventArgs e)
        {


        }

        private void txtitemname_KeyDown_1(object sender, KeyEventArgs e)
        {
          


        }

        private void txtqty_TextChanged_1(object sender, EventArgs e)
        {

        }

        private void txtplace_KeyDown(object sender, KeyEventArgs e)
        {

        }

        private void txtcharges_TextChanged(object sender, EventArgs e)
        {
            splittax();
        }

        private void label16_Click(object sender, EventArgs e)
        {

        }

        private void TxtRoff_TextChanged(object sender, EventArgs e)
        {

        }

        private void label17_Click(object sender, EventArgs e)
        {

        }

        private void txttbval_TextChanged(object sender, EventArgs e)
        {

        }

        private void txttdis_TextChanged(object sender, EventArgs e)
        {
            if (txttdis.Text != "")
            {
                if (Convert.ToDouble(txttdis.Text) > 0)
                {
                    dis = (Convert.ToDouble(txttbval.Text) / 100) * (Convert.ToDouble(txttdis.Text));
                    txttdisc.Text = dis.ToString();
                }
                else
                {
                    txttdisc.Text = "0";
                }
                splittax();
            }
        }

        private void txtprice_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtname_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.F10)
                {
                    grSearch.Visible = false;
                }
                else if (e.KeyCode == Keys.F2)
                {
                    SelectId = 1;
                    int Index = DataGridCommon.SelectedCells[0].RowIndex;
                   
                        txtname.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                        txtpuid.Text = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    

                    grSearch.Visible = false;
                    SelectId = 0;
                }
                else if (e.KeyCode == Keys.Escape)
                {
                    grSearch.Visible = false;
                }
                else if (e.KeyValue == 40)
                {
                    DataGridCommon.Select();
                }
                else if (e.KeyCode == Keys.Enter)
                {
                    txtname_Click(sender, e);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

        }

        private void txtplace_KeyDown_1(object sender, KeyEventArgs e)
        {
            
        }

        private void txtitemname_KeyDown(object sender, KeyEventArgs e)
        {

            try
            {
                if (e.KeyCode == Keys.F3)
                {
                    str1key = "F3";
                    txtitemname_Click(sender, e);
                }
                else if (e.KeyCode == Keys.F2)
                {
                    str1key = "F2";
                    txtitemname_Click(sender, e);

                }
          
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

        }
        private void loadput1()
        {
            conn.Close();
            conn.Open();
           
        }

        private void buttcusok_Click(object sender, EventArgs e)
        {
            if(Text == "Trims Purchase")

            {
                hh = Convert.ToDouble(txtcheckqty.Text);
                dd= Convert.ToDouble(txtqty.Text);

                if(hh<dd)
                {

                    MessageBox.Show("Qty Exceeds");
                    return;
                }

            }

            if (Text == "Yarn Purchase")

            {
                hh = Convert.ToDouble(txtcheckqty.Text)+60;
                dd = Convert.ToDouble(txtqty.Text);

                if (hh < dd)
                {

                    MessageBox.Show("Qty Exceeds");
                    return;
                }

            }
            if (Text == "Fabric Purchase")

            {
                hh = Convert.ToDouble(txtcheckqty.Text) + 60;
                dd = Convert.ToDouble(txtqty.Text);

                if (hh < dd)
                {

                    MessageBox.Show("Qty Exceeds");
                    return;
                }

            }





            if (txtqty.Text == "" || txtqty.Text == "0" || txtprice.Text == "" || txtprice.Text == "0" || txtitemname.Text == "" || txtitemname.Text == "0" || txtname.Text == "" || txtname.Text == "0")
            {
                MessageBox.Show("Enter the Party or Item or Rate or Qty to Proceed");
                return;
            }

            if (txtbval.Text != "" || txtbval.Text != "0")
            {
               
                    j = -1;

                if (Text=="Yarn Purchase")
                {
                 
                    Genclass.strsql = "select distinct uid as itemuid,Itemname,UoM,0 as hsnid," + txtqty.Tag + " as refid,qty,PRICE,convert(decimal(18,2),BasicValue,105) AS  BasicValue,disper,convert(decimal(18,2),Disvalue,105) AS Disvalue,convert(decimal(18,2),Taxablevalue,105) AS   Taxablevalue,gstper,gstval,convert(decimal(18,2),BasicValue-(isnull(Disvalue,0))+ (isnull(gstval,0)),105) as total   from   (select distinct a.Yarnname itemname,UOM.generalname as uom,y.yarnid as Uid," + txtqty.Text + " as qty," + txtprice.Text + "  as Price," + txtbval.Text + " as BasicValue,  0 as disper,0 as Disvalue," + txtbval.Text + " as   Taxablevalue,ISNULL(TAX.F1,0) as gstper,convert(decimal(18,2),(" + txtbval.Text + "/100 * TAX.f1),105) as gstval   from  OrderMBudjetYarn a INNER JOIN ORDERM O ON O.UID =A.ORDERMUID INNER JOIN YARNMASTER Y ON Y.YARNID =A.YARNUID INNER JOIN GENERALM UOM ON UOM.GUID =Y.UOM LEFT JOIN PURCHASED D ON A.ORDERMUID=D.REFUID INNER JOIN GENERALM TAX ON TAX.GUID =Y.tax where y.yarnid=" + txttitemid.Text + " and O.docno='" + cbowo.Text + "' GROUP BY O.UID,O.DOCNO ,a.Yarnname,UOM.GENERALNAME,a.Qty,a.Rate,a.Total,y.tax,y.yarnid,TAX.F1)tab ";
                    Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                }
                else if (Text == "Trims Purchase")
                {

                    Genclass.strsql = "select distinct uid as itemuid,Itemname,UoM,0 as hsnid," + txtqty.Tag + " as refid,qty,PRICE,convert(decimal(18,2),BasicValue,105) AS  BasicValue,disper,convert(decimal(18,2),Disvalue,105) AS Disvalue,convert(decimal(18,2),Taxablevalue,105) AS   Taxablevalue,gstper,gstval,convert(decimal(18,2),BasicValue-(isnull(Disvalue,0))+ (isnull(gstval,0)),105) as total   from    (select distinct (Case When a.OrderType = 'Combo Wise' Then a.ItemSpec + ' / ' + a.Colour  When a.OrderType = 'Size Wise' Then a.ItemSpec When a.OrderType = 'Style Wise' Then a.ItemSpec When a.OrderType = 'Order Wise' Then a.ItemSpec  End ) itemname, a.uom,c.Uid," + txtqty.Text + " as qty," + txtprice.Text + "  as Price," + txtbval.Text + " as BasicValue,  0 as disper,0 as Disvalue," + txtbval.Text + " as   Taxablevalue, ISNULL(c.tax,0) as gstper,convert(decimal(18,2),(" + txtbval.Text + "/100 * c.tax),105) as gstval  from OrderMMaterialBom a INNER JOIN ORDERM O ON O.UID =A.ORDERMUID inner join GeneralM b on a.CtgyId = b.Guid   INNER JOIN(SELECT T.*,I.TAXUID,I.BASEUOMUID,X.F1 TAX  FROM TRIMSM T INNER JOIN itemsm I ON I.ITEMCategoryUid = T.CATEGORYUID AND I.ACTIVE = 1 INNER JOIN  GENERALM X ON X.GUID = I.TAXUID) c on a.ItemUid = c.Uid  Left join StyleSize e on a.OrderMuid = e.OrderMuid and a.SizeUid = e.Uid   LEFT JOIN PURCHASED Dd ON A.ORDERMUID = Dd.REFUID and c.uid = dd.itemuid and c.uid=dd.itemuid  where c.uid=" + txttitemid.Text + " and O.docno='" + cbowo.Text + "')tab  ";
                    Genclass.cmd = new SqlCommand(Genclass.strsql, conn);

                }
                else if (Text == "Fabric Purchase")
                {

                    Genclass.strsql = "select distinct uid as itemuid,Itemname,UoM,0 as hsnid," + txtqty.Tag + " as refid,qty,PRICE,convert(decimal(18,2),BasicValue,105) AS  BasicValue,disper,convert(decimal(18,2),Disvalue,105) AS Disvalue,convert(decimal(18,2),Taxablevalue,105) AS   Taxablevalue,gstper,gstval,convert(decimal(18,2),BasicValue-(isnull(Disvalue,0))+ (isnull(gstval,0)),105) as total   from   (select distinct a.fabricname itemname,UOM.generalname as uom,y.uid as Uid," + txtqty.Text + " as qty," + txtprice.Text + "  as Price," + txtbval.Text + " as BasicValue,  0 as disper,0 as Disvalue," + txtbval.Text + " as   Taxablevalue,ISNULL(5,0) as gstper,convert(decimal(18,2),(" + txtbval.Text + "/100 * 5),105) as gstval   from  OrderMBudjetFabric a INNER JOIN ORDERM O ON O.UID =A.ORDERMUID INNER JOIN FabricM Y ON Y.uid =A.fabricuid INNER JOIN GENERALM UOM ON UOM.GUID =Y.purUOM LEFT JOIN PURCHASED D ON A.ORDERMUID=D.REFUID and y.uid=d.itemuid  and a.uid=d.referid  where y.uid=" + txttitemid.Text + " and O.docno='" + cbowo.Text + "'  and fabtag='Purchase'  GROUP BY O.UID,O.DOCNO ,a.fabricname,UOM.GENERALNAME,a.Qty,a.Rate,a.Total,y.uid)tab ";
                    Genclass.cmd = new SqlCommand(Genclass.strsql, conn);

                }
                SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
                DataTable tap1 = new DataTable();
                aptr1.Fill(tap1);
               
                    var index = HFIT.Rows.Add();
                    HFIT.Rows[index].Cells[0].Value = txtitemname.Text;
                    HFIT.Rows[index].Cells[1].Value = txtuom.Text;

                    sump = Convert.ToDouble(tap1.Rows[0]["PRICE"].ToString());
                    HFIT.Rows[index].Cells[2].Value = sump.ToString("0.000");
                    sumq = Convert.ToDouble(tap1.Rows[0]["qty"].ToString());
                    HFIT.Rows[index].Cells[3].Value = sumq.ToString("0.00");
                    sumb = Convert.ToDouble(tap1.Rows[0]["BasicValue"].ToString());
                    HFIT.Rows[index].Cells[4].Value = sumb.ToString("0.00");

                    //HFIT.Rows[index].Cells[2].Value = Txttot.Text.Replace(",", "");
                    //Txttot.Text = Txttot.Text.Replace(",", "");
                    //Txttot.Text = Txttot.Text.Replace(",", "");
                    //HFIT.Rows[index].Cells[2].Value = tap1.Rows[i]["Price"].ToString();
                    //HFIT.Rows[index].Cells[3].Value = tap1.Rows[i]["qty"].ToString();
                    //HFIT.Rows[index].Cells[4].Value = tap1.Rows[i]["BasicValue"].ToString();
                    HFIT.Rows[index].Cells[5].Value = tap1.Rows[0]["itemuid"].ToString();
                    HFIT.Rows[index].Cells[6].Value = tap1.Rows[0]["refid"].ToString();
                    HFIT.Rows[index].Cells[7].Value = tap1.Rows[0]["disper"].ToString();
                    HFIT.Rows[index].Cells[8].Value = tap1.Rows[0]["Disvalue"].ToString();
                    HFIT.Rows[index].Cells[9].Value = tap1.Rows[0]["Taxablevalue"].ToString();


                string qur1 = "select Generalname as tax,F1 uid from generalm where   Typemuid in (8) and Generalname='"+ txtTaxPercentage.Text +"' and active=1 ";
                DataTable tab1 = db.GetData2(CommandType.Text, qur1);
                if(tab1.Rows.Count>0)

                {

                    HFIT.Rows[index].Cells[10].Value = tab1.Rows[0]["uid"].ToString();
                    double tax,total;
                    tax = (Convert.ToDouble(txtbval.Text) * (Convert.ToDouble(tab1.Rows[0]["uid"].ToString()))) / 100;
                    HFIT.Rows[index].Cells[11].Value = tax.ToString();
                    total = (Convert.ToDouble(txtbval.Text))+ tax;

                    HFIT.Rows[index].Cells[14].Value = total.ToString();

                }


                HFIT.Rows[index].Cells[17].Value = tap1.Rows[0]["itemname"].ToString();
                //HFIT.Rows[index].Cells[12].Value = tap1.Rows[i]["VAT"].ToString();
                //HFIT.Rows[index].Cells[13].Value = tap1.Rows[i]["VATVAL"].ToString();

                HFIT.Rows[index].Cells[15].Value = tap1.Rows[0]["hsnid"].ToString();
                if(txtnotes.Text=="")
                {
                    txtnotes.Text = "0";

                }
                if (txtbags.Text == "")
                {
                    txtbags.Text = "0";

                }
                if (txtadd.Text == "")
                {
                    txtadd.Text = "0";

                }
                HFIT.Rows[index].Cells[16].Value = txtnotes.Text;
                HFIT.Rows[index].Cells[18].Value = txtbags.Text;
                HFIT.Rows[index].Cells[19].Value = txtstyle.Text;
                HFIT.Rows[index].Cells[20].Value = txtadd.Text;
                dis4 = Convert.ToDouble(HFIT.Rows[index].Cells[10].Value);
                    dis3 = Convert.ToDouble(HFIT.Rows[index].Cells[11].Value);
                    sum1 = sum1 + Convert.ToDouble(HFIT.Rows[index].Cells[4].Value);
                    Txttot.Text = sum1.ToString("#,0.00");
                    txttbval.Text = sum1.ToString("#,0.00");

                    Txttot.Text = Txttot.Text.Replace(",", "");
                    Titlegst();


                    if (txtstid.Text == "196")
                    {

                        dis4 = Convert.ToDouble(HFIT.Rows[index].Cells[10].Value) / 2;
                        dis3 = Convert.ToDouble(HFIT.Rows[index].Cells[11].Value) / 2;
                    }
                    else
                    {
                        dis4 = Convert.ToDouble(HFIT.Rows[index].Cells[10].Value) ;
                        dis3 = Convert.ToDouble(HFIT.Rows[index].Cells[11].Value);
                    }
                

                     HFIT.Rows[index].Cells[14].Value = tap1.Rows[0]["Total"].ToString();
                     HFIT.Rows[index].Cells[15].Value = tap1.Rows[0]["hsnid"].ToString();
                HFIT.Rows[index].Cells[21].Value = txtadd.Tag;
                if (cbowo.Text != "")
                {
                    HFIT.Rows[index].Cells[16].Value =cbowo.Text;
                }
                else
                {
                    HFIT.Rows[index].Cells[16].Value = "0";

                }
                    txttdis_TextChanged_1(sender, e);
               
                }

           
                

                Titlep();
            txtstyle.Text = "";
            txtbags.Text = "";
            txtitemname.Text = "";
                txtprice.Text = "";
                 cbowo.Text = "";
            txtqty.Text = "";
                txtnotes.Text = "";
                txtbval.Text = "";
                txtscr11.Focus();
            txtadd.Text = "";




            if (HFIT.RowCount > 1)
            {
                cboprocess.Enabled = false;

            }
            else

            {
                cboprocess.Enabled = true;

            }
        }
        

        private void txtprice_TextChanged_1(object sender, EventArgs e)
        {
            if (txtprice.Text != "" && txtqty.Text != "")
            {
                 add = Convert.ToDouble(txtprice.Text) * Convert.ToDouble(txtqty.Text);
                txtbval.Text = add.ToString("#,0.000");

                txtbval.Text = txtbval.Text.Replace(",", "");
            }

        }
        private void Titlegst()
        {
            if (txtstid.Text != "")
            {
                if (txtstid.Text == "196")
                {
                    HFGST.ColumnCount = 5;

                    HFGST.Columns[0].Name = "CGST%";
                    HFGST.Columns[1].Name = "CGST";

                    HFGST.Columns[2].Name = "SGST%";
                    HFGST.Columns[3].Name = "SGST";
                    HFGST.Columns[4].Name = "Total";


                    HFGST.Columns[0].Width = 70;
                    HFGST.Columns[1].Width = 70;
                    HFGST.Columns[2].Width = 70;
                    HFGST.Columns[3].Width = 70;
                    HFGST.Columns[4].Width = 70;

                }


                else
                {
                    HFGST.ColumnCount = 2;

                    HFGST.Columns[0].Name = "IGST%";
                    HFGST.Columns[1].Name = "IGST";
                    HFGST.Columns[0].Width = 180;
                    HFGST.Columns[1].Width = 170;

                }
            }
        }

        private void txtqty_TextChanged_2(object sender, EventArgs e)
        {
            if (txtqty.Text != ""&& txtprice.Text!="")
            {
                add = Convert.ToDouble(txtprice.Text) * Convert.ToDouble(txtqty.Text);
                txtbval.Text = add.ToString("#,0.00");
                txtbval.Text = txtbval.Text.Replace(",", "");
            }
        }

        private void txtname_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bsParty.Filter = string.Format("Name LIKE '%{0}%' ", txtname.Text);



                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void butcan_Click(object sender, EventArgs e)
        {
            string message = "Are you sure to cancel this PO ?";
            string caption = "Dilama";
            MessageBoxButtons buttons = MessageBoxButtons.YesNo;
            DialogResult result;

            result = MessageBox.Show(message, caption, buttons);

            if (result == System.Windows.Forms.DialogResult.Yes)
            {

                int i = HFGP.SelectedCells[0].RowIndex;
                uid = HFGP.Rows[i].Cells[0].Value.ToString();

                conn.Close();
                conn.Open();

                qur.CommandText = "delete  from purchased  where transactionspuid=" + uid + " ";
                qur.ExecuteNonQuery();

                qur.CommandText = "update  purchasem  set Approved='Cancelled',active=0 where uid=" + uid + "  ";
                qur.ExecuteNonQuery();
                MessageBox.Show("PO Cancelled");
            }
            LoadGetJobCard(1);
        }

        private void HFIT_KeyDown(object sender, KeyEventArgs e)
        {
            
        }

        private void txtpuid_TextChanged(object sender, EventArgs e)
        {
            if (txtpuid.Text != "")
            {
                string strsql = "Select address1, address2 ,city,stateuid  from supplierm where uid=" + txtpuid.Text + " ";
                SqlCommand cmd = new SqlCommand(strsql, conn);
                SqlDataAdapter aptr1 = new SqlDataAdapter(cmd);
                DataTable tap1 = new DataTable();
                aptr1.Fill(tap1);

                if (tap1.Rows.Count > 0)
                {
                    txtpadd1.Text = tap1.Rows[0]["address1"].ToString() + ' ' + tap1.Rows[0]["address2"].ToString() + ' ' + tap1.Rows[0]["city"].ToString();
                    txtplace.Text = txtname.Text;
                    txtpluid.Text = txtpuid.Text;
                    txtpadd2.Text = txtpadd1.Text;
                    txtstid.Text = tap1.Rows[0]["stateuid"].ToString();
                }
            }
        }

        private void txtpluid_TextChanged(object sender, EventArgs e)
        {
            if (txtpluid.Text != "")
            {

                string strsql = "Select address1, address2 ,city,stateuid  from supplierm where uid=" + txtpluid.Text + " ";
                SqlCommand cmd = new SqlCommand(strsql, conn);
                SqlDataAdapter aptr1 = new SqlDataAdapter(cmd);
                DataTable tap1 = new DataTable();
                aptr1.Fill(tap1);

                if (tap1.Rows.Count > 0)
                {
                    txtpadd2.Text = tap1.Rows[0]["address1"].ToString() + ' ' + tap1.Rows[0]["address2"].ToString() + ' ' + tap1.Rows[0]["city"].ToString();
                    //txtplace.Text = txtname.Text;
                    //txtpluid.Text = txtpuid.Text;
                    //txtpadd2.Text = txtpadd1.Text;
                    txtstid.Text = tap1.Rows[0]["stateuid"].ToString();
                }
            }
        }

        private void txtitemname_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtdcid_TextChanged(object sender, EventArgs e)
        {




        }

        private void txttrans_TextChanged(object sender, EventArgs e)
        {

        }

        ////private void Editpan_Paint(object sender, PaintEventArgs e)
        ////{

        ////}

        private void txtname_Click(object sender, EventArgs e)
        {
            ty = 1;
            DataTable dt = getParty();
            bsParty.DataSource = dt;
            FillGrid(dt, 1);
            Point loc = FindLocation(txtname);
            grSearch.Location = new Point(loc.X, loc.Y + 20);
            grSearch.Visible = true;
            grSearch.Text = "Party Search";

        }

        protected void FillGrid(DataTable dt, int FillId)
        {
            try
            {
                DataGridCommon.DataSource = null;
                DataGridCommon.AutoGenerateColumns = false;
                DataGridCommon.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
                DataGridCommon.DefaultCellStyle.WrapMode = DataGridViewTriState.True;

                DataGridCommon.ColumnCount = 2;
                DataGridCommon.Columns[0].Name = "Uid";
                DataGridCommon.Columns[0].HeaderText = "Uid";
                DataGridCommon.Columns[0].DataPropertyName = "Uid";
                DataGridCommon.Columns[1].Name = "Name";
                DataGridCommon.Columns[1].HeaderText = "Name";
                DataGridCommon.Columns[1].DataPropertyName = "Name";


                DataGridCommon.Columns[1].Width = 350;
                DataGridCommon.DataSource = bsParty;
                DataGridCommon.Columns[0].Visible = false;


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        protected void FillGrid1(DataTable dt, int FillId)
        {
            try
            {
                DataGridCommon.DataSource = null;
                DataGridCommon.AutoGenerateColumns = false;
                DataGridCommon.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
                DataGridCommon.DefaultCellStyle.WrapMode = DataGridViewTriState.True;

                DataGridCommon.ColumnCount = 3;
                DataGridCommon.Columns[0].Name = "Uid";
                DataGridCommon.Columns[0].HeaderText = "Uid";
                DataGridCommon.Columns[0].DataPropertyName = "Uid";
                DataGridCommon.Columns[1].Name = "Socno";
                DataGridCommon.Columns[1].HeaderText = "Socno";
                DataGridCommon.Columns[1].DataPropertyName = "Socno";
                DataGridCommon.Columns[2].Name = "Style";
                DataGridCommon.Columns[2].HeaderText = "Style";
                DataGridCommon.Columns[2].DataPropertyName = "Style";

                DataGridCommon.Columns[1].Width = 100;
                DataGridCommon.Columns[2].Width = 250;
                DataGridCommon.DataSource = bs;
                DataGridCommon.Columns[0].Visible = false;


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        private Point FindLocation(Control ctrl)
        {
            if (ctrl.Parent is Form)
                return ctrl.Location;
            else
            {
                Point p = FindLocation(ctrl.Parent);
                p.X += ctrl.Location.X;
                p.Y += ctrl.Location.Y;
                return p;
            }
        }
        protected DataTable getParty()
        {
            DataTable dt = new DataTable();
            try
            {

                if (ty == 1)
                {
                    dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "SP_GETPARTYSUPPLIER1", conn);
                    bsParty.DataSource = dt;
                }
               else if (ty == 2)
                {
                    dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "sp_getyarnsocno", conn);
                    bs.DataSource = dt;
                }
                else if (ty == 3)
                {
                    dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "sp_getTrimssocno", conn);
                    bs.DataSource = dt;
                }
                else if (ty == 4)
                {
                    dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "sp_getFabricsocno", conn);
                    bs.DataSource = dt;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            return dt;
        }
        private void txtitemname_Click(object sender, EventArgs e)
        {
          
            if (cbowo.Text == "")
            {
                MessageBox.Show("Select the socno");
                return;
            }

                lkppnl.Visible = true;

                if (Text=="Yarn Purchase")
                {
                    Genclass.strsql = "EXEC sp_getyarnpurchase '"+ cbowo.Text +"'";
                Genclass.FSSQLSortStr = "item";
            }
            else if (Text == "Fabric Purchase")
            {
                Genclass.strsql = "exec sp_getFabricpurchase   '" + cbowo.Text + "' ";
                }
            else if (Text == "Trims Purchase")
            {
                Genclass.strsql = "exec sp_getTrimspurchase  '" + cbowo.Text + "'";
                Genclass.FSSQLSortStr = "item";
            }

            Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
            SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
            DataTable tap = new DataTable();
            aptr.Fill(tap);
            bsc.DataSource = tap;



            HFGP2.AutoGenerateColumns = false;
            HFGP2.Refresh();
            HFGP2.DataSource = null;
            HFGP2.Rows.Clear();
            HFGP2.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            HFGP2.DefaultCellStyle.WrapMode = DataGridViewTriState.True;

            HFGP2.ColumnCount = tap.Columns.Count;
      i = 0;

            foreach (DataColumn column in tap.Columns)
            {
                HFGP2.Columns[i].Name = column.ColumnName;
                HFGP2.Columns[i].HeaderText = column.ColumnName;
                HFGP2.Columns[i].DataPropertyName = column.ColumnName;
                i = i + 1;
            }
            HFGP2.DataSource = tap;

            HFGP2.Columns[0].Visible = false;
            
                HFGP2.Columns[1].Width = 70;
                HFGP2.Columns[2].Width = 350;
                HFGP2.Columns[3].Width = 70;
                HFGP2.Columns[4].Width = 100;
                HFGP2.Columns[5].Visible = false;
                HFGP2.Columns[6].Visible = false;
                HFGP2.Columns[7].Visible = false;
                HFGP2.Columns[8].Visible = false;
                HFGP2.Columns[9].Visible = false;
            HFGP2.Columns[10].Visible = false;



            //str1key = "F2";

            conn.Close();


        }
        private void txttdis_TextChanged_1(object sender, EventArgs e)
        {
            if (txttdis.Text == "")
            {
                txttdis.Text = "0";
            }
            if (txtcharges.Text == "")
            {
                txttdis.Text = "0";
            }


            if (txttbval.Text != "")
            {


                if (txttdis.Text == "" || txttdis.Text == "0")
                {
                    txttdisc.Text = "0";
                }

                if (txtcharges.Text == "")
                {
                    txtcharges.Text = "0";
                }


                //if (Convert.ToDouble(txttdis.Text) > 0)
                //{
                    dis = (Convert.ToDouble(txttbval.Text) / 100) * (Convert.ToDouble(txttdis.Text));
                    txttdisc.Text = dis.ToString();
                    yt7 = Convert.ToDouble(txttbval.Text) - Convert.ToDouble(txttdisc.Text);
                    txttprdval.Text = yt7.ToString();
                    if (txtcharges.Text == "" || txtcharges.Text == null)
                    {
                        txtexcise.Text = txttprdval.Text;
                    }
                    else
                    {
                        dis4 = Convert.ToDouble(txttprdval.Text) + Convert.ToDouble(txtcharges.Text);
                        txtexcise.Text = dis4.ToString("0.00");
                    }
                    sum5 = 0;
                   df = 0;
                    hg = 0;

                    HFGST.Refresh();
                    HFGST.DataSource = null;
                    HFGST.Rows.Clear();
                    txtigval.Text = "0";
                    txttaxtot.Text = "0";

                    Titlegst();
                    for (int l = 0; l < HFIT.RowCount - 1; l++)
                    {

                        if (j != l)
                        {

                            dis4 = Convert.ToDouble(HFIT.Rows[l].Cells[10].Value);


                            dis3 = Convert.ToDouble(HFIT.Rows[l].Cells[4].Value) - (Convert.ToDouble(HFIT.Rows[l].Cells[4].Value) / 100) * (Convert.ToDouble(txttdis.Text));
                            dis3 = (dis3 / 100) * dis4;




                            if (txtstid.Text == "196")
                            {

                                if (mode == 2)
                                {
                                    dis4 = Convert.ToDouble(HFIT.Rows[l].Cells[10].Value) ;
                                    //dis3 = Convert.ToDouble(HFIT.Rows[l].Cells[11].Value);
                                }
                                dis4 = dis4 / 2;
                                dis3 = dis3 / 2;

                                int boo = 1;
                                if (HFGST.Rows.Count - 1 == 0)
                                {

                                    var index1 = HFGST.Rows.Add();
                                    HFGST.Rows[index1].Cells[0].Value = dis4.ToString("0.00");
                                    HFGST.Rows[index1].Cells[2].Value = dis4.ToString("0.00");
                                    HFGST.Rows[index1].Cells[1].Value = dis3.ToString("0.00");
                                    HFGST.Rows[index1].Cells[3].Value = dis3.ToString("0.00");
                                    HFGST.Rows[index1].Cells[4].Value = Convert.ToDouble(dis3.ToString("0.00")) * 2;
                                }
                                else
                                {
                                    for (int k = 0; k < HFGST.Rows.Count - 1; k++)
                                    {
                                        if (Convert.ToDouble(HFGST.Rows[k].Cells[0].Value) == dis4)
                                        {

                                            dg = Convert.ToDouble(HFGST.Rows[k].Cells[1].Value) + dis3;
                                            HFGST.Rows[k].Cells[1].Value = dg.ToString("0.00");

                                            HFGST.Rows[k].Cells[3].Value = dg.ToString("0.00");
                                            dg1 = Convert.ToDouble(HFGST.Rows[k].Cells[4].Value) + (Convert.ToDouble(dis3.ToString("0.00")) * 2);
                                            HFGST.Rows[k].Cells[4].Value = dg1.ToString("0.00");
                                            boo = 1;
                                            break;
                                        }
                                        else
                                        {
                                            boo = 2;
                                        }
                                    }
                                }

                                if (boo == 2)
                                {

                                    var index1 = HFGST.Rows.Add();
                                    HFGST.Rows[index1].Cells[0].Value = dis4.ToString("0.00");
                                    HFGST.Rows[index1].Cells[2].Value = dis4.ToString("0.00");
                                    HFGST.Rows[index1].Cells[1].Value = dis3.ToString("0.00");
                                    HFGST.Rows[index1].Cells[3].Value = dis3.ToString("0.00");
                                    HFGST.Rows[index1].Cells[4].Value = Convert.ToDouble(dis3.ToString("0.00")) * 2;
                                }

                                if (txtigval.Text == "")
                                {
                                    txtigval.Text = "0";
                                }
                                df = Convert.ToDouble(txtigval.Text) + (Convert.ToDouble(dis3.ToString("0.00")) * 2);
                                txtigval.Text = df.ToString("0.00");
                                txttaxtot.Text = df.ToString("0.00");
                            }
                            else
                            {
                                int boo = 1;
                                if (HFGST.Rows.Count - 1 == 0)
                                {
                                    var index1 = HFGST.Rows.Add();
                                    HFGST.Rows[index1].Cells[0].Value = dis4.ToString("0.00");
                                    HFGST.Rows[index1].Cells[1].Value = dis3.ToString("0.00");
                                }
                                else
                                {
                                    for (int k = 0; k < HFGST.Rows.Count - 1; k++)
                                    {
                                        if (Convert.ToDouble(HFGST.Rows[k].Cells[0].Value) == dis4)
                                        {

                                            dg = Convert.ToDouble(HFGST.Rows[k].Cells[1].Value) + dis3;
                                            HFGST.Rows[k].Cells[1].Value = dg.ToString("0.00");


                                            boo = 1;
                                            break;
                                        }
                                        else
                                        {
                                            boo = 2;
                                        }
                                    }
                                }

                                if (boo == 2)
                                {

                                    var index1 = HFGST.Rows.Add();
                                    HFGST.Rows[index1].Cells[0].Value = dis4.ToString("0.00");

                                    HFGST.Rows[index1].Cells[1].Value = dis3.ToString("0.00");


                                }
                                if (txtigval.Text == "")
                                {
                                    txtigval.Text = "0";
                                }
                                df = df + Convert.ToDouble(dis3.ToString("0.00"));
                                txtigval.Text = df.ToString("0.00");
                                txttaxtot.Text = df.ToString("0.00");
                            }
                        }
                    

                    //txtigval.Text = hg.ToString("0.00");
                    //txttaxtot.Text = hg.ToString("0.00");
                    //double tot = Convert.ToDouble(txtexcise.Text) + Convert.ToDouble(txtigval.Text);
                    //txtttot.Text = tot.ToString("0.00");
                    net1 = Convert.ToDouble(txtexcise.Text) + Convert.ToDouble(txtigval.Text) + Convert.ToDouble(txtcharges.Text);
                    txtttot.Text = net1.ToString("0.00");
                    TxtNetAmt.Text = net1.ToString("0.00");
                    someInt = (int)net1;

                    rof = Math.Round(net1 - someInt, 2);
                    TxtRoff.Text = rof.ToString("0.00");

                    if (Convert.ToDouble(TxtRoff.Text) < 0.49)
                    {
                         rof1 = -1 * Convert.ToDouble(TxtRoff.Text);
                        TxtRoff.Text = rof1.ToString("0.00");
                    }
                    else
                    {
                         rof2 = 1 - Convert.ToDouble(TxtRoff.Text);
                        TxtRoff.Text = rof2.ToString("0.00");
                    }

                     net = Convert.ToDouble(TxtNetAmt.Text) + Convert.ToDouble(TxtRoff.Text);
                    //int ne=Convert.ToInt16(net);
                    TxtNetAmt.Text = net.ToString("0.00");
                }
            }
        }

        private void txtcharges_TextChanged_1(object sender, EventArgs e)
        {
            if (txtcharges.Text != "")
            {
                if (txtcharges.Text == "" || txtcharges.Text == "0")
                {
                    txtcharges.Text = "0";
                }
                if (txttdisc.Text == "" || txttdisc.Text == "0")
                {
                    txttdisc.Text = "0";
                }
            

            }
        }

        private void txtname_MouseClick(object sender, MouseEventArgs e)
        {

        }

        private void button13_Click(object sender, EventArgs e)
        {
            Genclass.type = 7;
            Genclass.cat = 3;
            Genclass.address = Text;
            Genclass.Dtype = 7;
            if (HFGP.CurrentRow.Cells[15].Value.ToString() == "Approved")
            {
                conn.Close();
                conn.Open();
                qur.CommandText = "delete from  NotoWords";
                qur.ExecuteNonQuery();

                Int64 NumVal = Convert.ToInt64(HFGP.CurrentRow.Cells[6].Value);
                string Nw = Rupees(NumVal);

                qur.CommandText = "Insert into NotoWords values('" + Nw + "',1)";
                qur.ExecuteNonQuery();

                Genclass.Prtid = Convert.ToInt16(HFGP.CurrentRow.Cells[0].Value.ToString());


                Genclass.slno = 1;
                Crviewer crv = new Crviewer();
                crv.Show();



                conn.Close();
            }
            //else

            //{

            //    MessageBox.Show("Purchase Order Pending");
            //    return;
            //}
        }

        private void butedit_Click_1(object sender, EventArgs e)
        {
            addpnl.Visible = false;
            if (Text == "Trims Purchase")
            {
                label64.Visible = false;
                txtbags.Visible = false;
                
            }
            

            btnedit_Click(sender, e);
            panadd.Visible = false;
            txtbval.Text = "";
            cboprocess.Enabled = false;
        }

        private void txtscr11_TextChanged(object sender, EventArgs e)
        {
            if (Text=="Yarn Purchase")
            {
                bsc.Filter = string.Format("item LIKE '%{0}%'  and style LIKE '%{1}%'", txtscr11.Text, txtscr12.Text);
            }
            else
            {
                bsc.Filter = string.Format("item LIKE '%{0}%' and style LIKE '%{1}%'", txtscr11.Text, txtscr12.Text);
            }
        }

        private void txtscr12_TextChanged(object sender, EventArgs e)
        {
            if (Text == "Yarn Purchase")
            {
                bsc.Filter = string.Format("style LIKE '%{0}%' and item LIKE '%{1}%'", txtscr12.Text, txtscr11.Text);
            }
            else
            {
                bsc.Filter = string.Format("style LIKE '%{0}%'  and item LIKE '%{1}%'", txtscr12.Text, txtscr11.Text);
            }
        }

        private void HFGP2_Click(object sender, EventArgs e)
        {
            txtprice.Text = "";
            
                SelectId = 1;
                int Index = HFGP2.SelectedCells[0].RowIndex;

                txtqty.Tag = HFGP2.CurrentRow.Cells[0].Value.ToString();
                cbowo.Text = HFGP2.CurrentRow.Cells[1].Value.ToString();
                txttitemid.Text = HFGP2.CurrentRow.Cells[8].Value.ToString();
                txtitemname.Text = HFGP2.CurrentRow.Cells[2].Value.ToString();
                txtuom.Text = HFGP2.CurrentRow.Cells[3].Value.ToString();
                txtqty.Text = HFGP2.CurrentRow.Cells[4].Value.ToString();
                txtcheckqty.Text = HFGP2.CurrentRow.Cells[4].Value.ToString();
                txtbval.Text = HFGP2.CurrentRow.Cells[6].Value.ToString();

                txtstyle.Text = HFGP2.CurrentRow.Cells[9].Value.ToString();
            txtadd.Tag = HFGP2.CurrentRow.Cells[10].Value.ToString();
           

                txtprice.Focus();
            
            lkppnl.Visible = false;
        }

        private void txtscr11_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SelectId = 1;
                int Index = HFGP2.SelectedCells[0].RowIndex;

                txtqty.Tag = HFGP2.CurrentRow.Cells[0].Value.ToString();
                cbowo.Text = HFGP2.CurrentRow.Cells[1].Value.ToString();
                txttitemid.Text = HFGP2.CurrentRow.Cells[8].Value.ToString();
                txtitemname.Text = HFGP2.CurrentRow.Cells[2].Value.ToString();
                txtuom.Text = HFGP2.CurrentRow.Cells[3].Value.ToString();
                txtqty.Text = HFGP2.CurrentRow.Cells[4].Value.ToString();
                txtprice.Text = HFGP2.CurrentRow.Cells[5].Value.ToString();
                txtbval.Text = HFGP2.CurrentRow.Cells[6].Value.ToString();
                txtstyle.Text = HFGP2.CurrentRow.Cells[9].Value.ToString();
                lkppnl.Visible = false;

            }
            else if (e.KeyCode == Keys.Escape)
            {
                lkppnl.Visible = false;
                return;

            }
            txtitemname_Click(sender, e);
        }

        private void HFGP2_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void HFIT_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void label55_Click(object sender, EventArgs e)
        {

        }

        private void Txttot_TextChanged(object sender, EventArgs e)
        {

        }

        private void buttnfinbk_Click_1(object sender, EventArgs e)
        {
            Editpan.Visible = false;
            Genpan.Visible = true;
            LoadGetJobCard(1);
            panadd.Visible = true;
        }

        private void btnHide_Click(object sender, EventArgs e)
        {
            grSearch.Visible = false;
        }

        private void button18_Click(object sender, EventArgs e)
        {
            try
            {
                SelectId = 1;
                int Index = DataGridCommon.SelectedCells[0].RowIndex;
                if (ty == 1)
                {
                    txtname.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtpuid.Text = DataGridCommon.Rows[Index].Cells[0].Value.ToString();

                    txtplace.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtveh.Focus();
                }
                else
                {

                    cbowo.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    cbowo.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();

                }

                grSearch.Visible = false;
                SelectId = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void DataGridCommon_Click(object sender, EventArgs e)
        {
            
        }

        private void DataGridCommon_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void DataGridCommon_DoubleClick(object sender, EventArgs e)
        {
            SelectId = 1;
            int Index = DataGridCommon.SelectedCells[0].RowIndex;
            if (ty == 1)
            {
                txtname.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                txtpuid.Text = DataGridCommon.Rows[Index].Cells[0].Value.ToString();

                txtplace.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                txtveh.Focus();
            }
            else
            {

                cbowo.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                cbowo.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();

            }

            grSearch.Visible = false;
            SelectId = 0;
        }

        private void DataGridCommon_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SelectId = 1;
                int Index = DataGridCommon.SelectedCells[0].RowIndex;
                if (ty == 1)
                {
                    txtname.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtpuid.Text = DataGridCommon.Rows[Index].Cells[0].Value.ToString();

                    txtplace.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtveh.Focus();
                }
                else
                {

                    cbowo.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    cbowo.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();

                }

                grSearch.Visible = false;
                SelectId = 0;
            }
        }

        private void btnsave_Click_1(object sender, EventArgs e)
        {
            btnsave_Click(sender, e);
        }

        private void DTPDOCDT_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void txtveh_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                e.Handled = true;
                SendKeys.Send("{TAB}");
            }
        }

        private void txtrem_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                e.Handled = true;
                SendKeys.Send("{TAB}");
            }
        }

        private void txtprice_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                e.Handled = true;
                SendKeys.Send("{TAB}");
            }
        }

        private void txtqty_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                e.Handled = true;
                SendKeys.Send("{TAB}");
            }
        }

        private void cboprocess_SelectedIndexChanged(object sender, EventArgs e)
        {
           
        }

        private void HFIT_CellValueChanged_1(object sender, DataGridViewCellEventArgs e)
        {
           

        }

        private void HFIT_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (HFIT.CurrentRow.Cells[0].Value != null && HFIT.CurrentCell.ColumnIndex == 2)
            {
                if (HFIT.CurrentCell.ColumnIndex == 2)

                {
                    DataGridViewCell cell = HFIT.CurrentRow.Cells[2];
                    HFIT.CurrentCell = cell;
                    HFIT.BeginEdit(true);
                }

            }
            else if (HFIT.CurrentRow.Cells[0].Value != null && HFIT.CurrentCell.ColumnIndex == 3)
            {
                if (HFIT.CurrentCell.ColumnIndex == 3)

                {
                    DataGridViewCell cell = HFIT.CurrentRow.Cells[3];
                    HFIT.CurrentCell = cell;
                    HFIT.BeginEdit(true);
                }

            }
            else if (HFIT.CurrentRow.Cells[0].Value != null && HFIT.CurrentCell.ColumnIndex == 20)
            {
                if (HFIT.CurrentCell.ColumnIndex == 20)

                {
                    addpnl.Visible = true;
                     txtadd.Text= HFIT.CurrentRow.Cells[20].Value.ToString() ;
                }

            }

        }

        private void HFIT_CellClick_1(object sender, DataGridViewCellEventArgs e)
        {
            if (HFIT.CurrentRow.Cells[0].Value != null && HFIT.CurrentCell.ColumnIndex == 2)
            {
                if (HFIT.CurrentCell.ColumnIndex == 2)

                {
                    DataGridViewCell cell = HFIT.CurrentRow.Cells[2];
                    HFIT.CurrentCell = cell;
                    HFIT.BeginEdit(true);
                }

            }
            else if (HFIT.CurrentRow.Cells[0].Value != null && HFIT.CurrentCell.ColumnIndex == 3)
            {
                if (HFIT.CurrentCell.ColumnIndex == 3)

                {
                    DataGridViewCell cell = HFIT.CurrentRow.Cells[3];
                    HFIT.CurrentCell = cell;
                    HFIT.BeginEdit(true);
                }

            }

            else if (HFIT.CurrentRow.Cells[0].Value != null && HFIT.CurrentCell.ColumnIndex == 20)
            {
                if (HFIT.CurrentCell.ColumnIndex == 20)

                {

                    addpnl.Visible = true;
                    txtadd.Text = HFIT.CurrentRow.Cells[20].Value.ToString();

                }

            }
        }

        private void HFIT_CellValueChanged_2(object sender, DataGridViewCellEventArgs e)
        {
        
            for (int k = 0; k < HFIT.Rows.Count - 1; k++)
            {



                if (HFIT.Rows[k].Cells[2].Value == null || HFIT.Rows[k].Cells[3].Value == null )
                {
                    return;

                }
                else
                {

                    if (HFIT.Rows[k].Cells[2].Value.ToString() != "0.000" || HFIT.Rows[k].Cells[3].Value.ToString() != "0.000")
                    {
                        txtprice.Text = HFIT.Rows[k].Cells[2].Value.ToString();
                        str2 = Convert.ToDouble(HFIT.Rows[k].Cells[2].Value.ToString()) * Convert.ToDouble(HFIT.Rows[k].Cells[3].Value.ToString());
                        HFIT.Rows[k].Cells[4].Value = str2.ToString();

                       
                    }

                }




            }
            cal();
            

            txttdis_TextChanged_1(sender, e);

        }


        private void cal()

        {
            sum1 = 0;
            for (p = 0; p < HFIT.Rows.Count - 1; p++)
            {

                sum1 = sum1 + Convert.ToDouble(HFIT.Rows[p].Cells[4].Value.ToString());

            
            }
            Txttot.Text = sum1.ToString("0.00");
            txttbval.Text = sum1.ToString("0.00");

        }
        private void HFIT_KeyDown_1(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                j = HFIT.SelectedCells[0].RowIndex;

                sun = Convert.ToDouble(HFIT.Rows[j].Cells[4].Value.ToString());

                sum1 = Convert.ToDouble(Txttot.Text);


                sum1 = sum1 - sun;

                Txttot.Text = sum1.ToString("0.00");
                txttbval.Text = sum1.ToString("0.00");
                txttdis_TextChanged_1(sender, e);
            }
        }

        private void button22_Click(object sender, EventArgs e)
        {
            GeneralParametrs.MenyKey = 7;
            Frmpar contc = new Frmpar();
            contc.Show();
            contc.FormBorderStyle = FormBorderStyle.None;
            contc.StartPosition = FormStartPosition.Manual;
            contc.Location = new System.Drawing.Point(200, 80);
        }

        private void button14_Click(object sender, EventArgs e)
        {

            FrmYarnMaster contc = new FrmYarnMaster();
            contc.Show();
            contc.FormBorderStyle = FormBorderStyle.None;
            contc.StartPosition = FormStartPosition.Manual;
            contc.Location = new System.Drawing.Point(200, 80);
        }

        private void HFIT_CellContentClick_1(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void button15_Click(object sender, EventArgs e)
        {
            Genclass.Dtype = 7;

            Genclass.address = Text;

            if (HFGP.CurrentRow.Cells[15].Value.ToString() == "Approved")
            {
                conn.Close();
                conn.Open();

                qur.CommandText = "delete from  NotoWords";
                qur.ExecuteNonQuery();

                Int64 NumVal = Convert.ToInt64(HFGP.CurrentRow.Cells[6].Value);
                string Nw = Rupees(NumVal);

                qur.CommandText = "Insert into NotoWords values('" + Nw + "',1)";
                qur.ExecuteNonQuery();

                Genclass.Prtid = Convert.ToInt16(HFGP.CurrentRow.Cells[0].Value.ToString());


                for (int i = 1; i < 4; i++)
                {
                    Genclass.slno = i;
                    Crviewer crv = new Crviewer();
                    if (Text == "Yarn Purchase")
                    {
                        string strsql = "select * from Vw_salprtexlfin   where muid=" + Genclass.Prtid + "  and doctypeid=7";
                        SqlCommand cmd = new SqlCommand(strsql, conn);
                        SqlDataAdapter aptr = new SqlDataAdapter(cmd);
                        DataTable tap = new DataTable();
                        aptr.Fill(tap);
                        if (tap.Rows.Count > 0)
                        {
                            if (tap.Rows[0]["bstate"].ToString() == "Tamilnadu")
                            {


                                SqlDataAdapter da = new SqlDataAdapter("sp_pdf", conn);
                                da.SelectCommand.CommandType = CommandType.StoredProcedure;
                                da.SelectCommand.Parameters.Add("@Muid", SqlDbType.Int).Value = Genclass.Prtid;
                                da.SelectCommand.Parameters.Add("@slno", SqlDbType.Int).Value = Genclass.slno;
                                da.SelectCommand.Parameters.Add("@companyid", SqlDbType.Int).Value = GeneralParameters.UserdId;
                                da.SelectCommand.Parameters.Add("@doctypeid", SqlDbType.Int).Value = 7;
                                DataSet ds = new DataSet();
                                da.Fill(ds, "salesinvoice");


                                doc.Load(Application.StartupPath + "\\PurchaseOrderWithoutRat.rpt");


                                //SqlDataAdapter da2 = new SqlDataAdapter("SP_SGSTTAX", conn);
                                //da2.SelectCommand.CommandType = CommandType.StoredProcedure;
                                //da2.SelectCommand.Parameters.Add("@uid", SqlDbType.Int).Value = Genclass.Prtid;

                                //da2.SelectCommand.Parameters.Add("@doctypeid", SqlDbType.Int).Value = 7;
                                //DataTable ds2 = new DataTable("SGSTTAX");
                                //da2.Fill(ds2);
                                //ds.Tables.Add(ds2);

                                //doc.Subreports["CrySgst.rpt"].SetDataSource(ds2);
                                //doc.SetDataSource(ds);


                                SqlDataAdapter da1 = new SqlDataAdapter("terms", conn);
                                da1.SelectCommand.CommandType = CommandType.StoredProcedure;
                                da1.SelectCommand.Parameters.Add("@uid", SqlDbType.Int).Value = Genclass.Prtid;
                                DataTable ds1 = new DataTable("Terms");
                                da1.Fill(ds1);
                                ds.Tables.Add(ds1);





                                doc.Subreports["Termrpt1.rpt"].SetDataSource(ds1);
                                doc.SetDataSource(ds);


                            }

                            else
                            {
                                SqlDataAdapter da = new SqlDataAdapter("sp_pdfOTHERSTATE", conn);
                                da.SelectCommand.CommandType = CommandType.StoredProcedure;
                                da.SelectCommand.Parameters.Add("@Muid", SqlDbType.Int).Value = Genclass.Prtid;
                                //da.SelectCommand.Parameters.Add("@type", SqlDbType.Int).Value = 2;
                                da.SelectCommand.Parameters.Add("@slno", SqlDbType.Int).Value = Genclass.slno;
                                da.SelectCommand.Parameters.Add("@companyid", SqlDbType.Int).Value = GeneralParameters.UserdId;
                                da.SelectCommand.Parameters.Add("@doctypeid", SqlDbType.Int).Value = 7;
                                DataSet ds = new DataSet();
                                da.Fill(ds, "salesinvoice");

                                doc.Load(Application.StartupPath + "\\PurchaseOrderWithoutRat.rpt");



                                //SqlDataAdapter da2 = new SqlDataAdapter("SP_IGSTTAX", conn);
                                //da2.SelectCommand.CommandType = CommandType.StoredProcedure;
                                //da2.SelectCommand.Parameters.Add("@uid", SqlDbType.Int).Value = Genclass.Prtid;
                                //da2.SelectCommand.Parameters.Add("@doctypeid", SqlDbType.Int).Value = 7;
                                //DataTable ds2 = new DataTable("IGSTTAX");
                                //da2.Fill(ds2);
                                //ds.Tables.Add(ds2);

                                //doc.Subreports["Cryigst.rpt"].SetDataSource(ds2);
                                //doc.SetDataSource(ds);


                                SqlDataAdapter da1 = new SqlDataAdapter("terms", conn);
                                da1.SelectCommand.CommandType = CommandType.StoredProcedure;
                                da1.SelectCommand.Parameters.Add("@uid", SqlDbType.Int).Value = Genclass.Prtid;
                                DataTable ds1 = new DataTable("Terms");
                                da1.Fill(ds1);
                                ds.Tables.Add(ds1);




                                doc.Subreports["Termrpt1.rpt"].SetDataSource(ds1);
                                doc.SetDataSource(ds);

                            }
                        }
                    }
                    else if (Text == "Trims Purchase")
                    {

                        string strsql = "select * from Vw_salprtexlfintrims   where muid=" + Genclass.Prtid + "  and doctypeid=7";
                        SqlCommand cmd = new SqlCommand(strsql, conn);
                        SqlDataAdapter aptr = new SqlDataAdapter(cmd);
                        DataTable tap = new DataTable();
                        aptr.Fill(tap);
                        if (tap.Rows.Count > 0)
                        {
                            if (tap.Rows[0]["bstate"].ToString() == "Tamilnadu")
                            {


                                SqlDataAdapter da = new SqlDataAdapter("sp_PurchaseTrims", conn);
                                da.SelectCommand.CommandType = CommandType.StoredProcedure;
                                da.SelectCommand.Parameters.Add("@Muid", SqlDbType.Int).Value = Genclass.Prtid;
                                da.SelectCommand.Parameters.Add("@slno", SqlDbType.Int).Value = Genclass.slno;
                                da.SelectCommand.Parameters.Add("@companyid", SqlDbType.Int).Value = GeneralParameters.UserdId;
                                da.SelectCommand.Parameters.Add("@doctypeid", SqlDbType.Int).Value = 7;
                                DataSet ds = new DataSet();
                                da.Fill(ds, "salesinvoice");


                                doc.Load(Application.StartupPath + "\\PurchaseOrderWithoutRat.rpt");



                                //SqlDataAdapter da2 = new SqlDataAdapter("SP_SGSTTAX", conn);
                                //da2.SelectCommand.CommandType = CommandType.StoredProcedure;
                                //da2.SelectCommand.Parameters.Add("@uid", SqlDbType.Int).Value = Genclass.Prtid;

                                //da2.SelectCommand.Parameters.Add("@doctypeid", SqlDbType.Int).Value = 7;
                                //DataTable ds2 = new DataTable("SGSTTAX");
                                //da2.Fill(ds2);
                                //ds.Tables.Add(ds2);

                                //doc.Subreports["CrySgst.rpt"].SetDataSource(ds2);
                                //doc.SetDataSource(ds);



                                SqlDataAdapter da1 = new SqlDataAdapter("terms", conn);
                                da1.SelectCommand.CommandType = CommandType.StoredProcedure;
                                da1.SelectCommand.Parameters.Add("@uid", SqlDbType.Int).Value = Genclass.Prtid;
                                DataTable ds1 = new DataTable("Terms");
                                da1.Fill(ds1);
                                ds.Tables.Add(ds1);





                                doc.Subreports["Termrpt1.rpt"].SetDataSource(ds1);
                                doc.SetDataSource(ds);


                            }

                            else
                            {
                                SqlDataAdapter da = new SqlDataAdapter("sp_otherstateTrims", conn);
                                da.SelectCommand.Connection = conn;
                                da.SelectCommand.CommandType = CommandType.StoredProcedure;
                                da.SelectCommand.Parameters.Add("@Muid", SqlDbType.Int).Value = Genclass.Prtid;
                                //da.SelectCommand.Parameters.Add("@type", SqlDbType.Int).Value = 2;
                                da.SelectCommand.Parameters.Add("@slno", SqlDbType.Int).Value = Genclass.slno;
                                da.SelectCommand.Parameters.Add("@companyid", SqlDbType.Int).Value = GeneralParameters.UserdId;
                                da.SelectCommand.Parameters.Add("@doctypeid", SqlDbType.Int).Value = 7;
                                DataSet ds = new DataSet();
                                da.Fill(ds, "salesinvoice");

                                doc.Load(Application.StartupPath + "\\PurchaseOrderWithoutRat.rpt");


                                //SqlDataAdapter da2 = new SqlDataAdapter("SP_IGSTTAX", conn);
                                //da2.SelectCommand.CommandType = CommandType.StoredProcedure;
                                //da2.SelectCommand.Parameters.Add("@uid", SqlDbType.Int).Value = Genclass.Prtid;
                                //da2.SelectCommand.Parameters.Add("@doctypeid", SqlDbType.Int).Value = 7;
                                //DataTable ds2 = new DataTable("IGSTTAX");
                                //da2.Fill(ds2);
                                //ds.Tables.Add(ds2);
                                //doc.Subreports["Cryigst.rpt"].SetDataSource(ds2);
                                //doc.SetDataSource(ds);


                                SqlDataAdapter da1 = new SqlDataAdapter("terms", conn);
                                da1.SelectCommand.CommandType = CommandType.StoredProcedure;
                                da1.SelectCommand.Parameters.Add("@uid", SqlDbType.Int).Value = Genclass.Prtid;
                                DataTable ds1 = new DataTable("Terms");
                                da1.Fill(ds1);
                                ds.Tables.Add(ds1);




                                doc.Subreports["Termrpt1.rpt"].SetDataSource(ds1);
                                doc.SetDataSource(ds);

                            }
                        }


                    }
                    doc.PrintToPrinter(1, false, 0, 0);
                }
            }
            else

            {

                MessageBox.Show("Purchase Order Pending");
                return;
            }
        }

        private void button16_Click(object sender, EventArgs e)
        {
            Genclass.Dtype = 7;
            Genclass.type = 11;
            Genclass.cat = 3;
            Genclass.address = Text;
            conn.Close();
            conn.Open();
            qur.CommandText = "delete from  NotoWords";
            qur.ExecuteNonQuery();

            NumVal = Convert.ToInt64(HFGP.CurrentRow.Cells[6].Value);
            Nw = Rupees(NumVal);

            qur.CommandText = "Insert into NotoWords values('" + Nw + "',1)";
            qur.ExecuteNonQuery();

            Genclass.Prtid = Convert.ToInt16(HFGP.CurrentRow.Cells[0].Value.ToString());


            Genclass.slno = 1;
            Crviewer crv = new Crviewer();
            crv.Show();



            conn.Close();
        }

        private void DTPDOCDT_ValueChanged(object sender, EventArgs e)
        {

        }

        private void DTPDOCDT_KeyPress_1(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                e.Handled = true;
                SendKeys.Send("{TAB}");
            }
        }

        private void cbowo_SelectedIndexChanged(object sender, EventArgs e)
        {
            //if (cbowo.Text != "" && cbowo.SelectedValue != null && cbowo.ValueMember != "")
            //{
            //    loadoutputitem();
            //}
        }
        private void loadoutputitem()
        {
            if (cbowo.Text == "")
            {

                MessageBox.Show("Enter the workorderNo");
                return;
            }


            if (str1key == "F3")
            {
                lkppnl.Visible = true;
                Genclass.strsql = "sp_getworderno '" + cbowo.Text + "'";
                Genclass.FSSQLSortStr = "itemspec1";
            }
          
            Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
            SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
            DataTable tap = new DataTable();
            aptr.Fill(tap);
           
            bsParty.DataSource = tap;

            HFGP2.DataSource = null;
            HFGP2.AutoGenerateColumns = false;
            HFGP2.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            HFGP2.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            HFGP2.ColumnCount = 4;
            HFGP2.Columns[0].Name = "uid";
            HFGP2.Columns[0].HeaderText = "uid";
            HFGP2.Columns[0].DataPropertyName = "uid";
            HFGP2.Columns[1].Name = "itemspec1";
            HFGP2.Columns[1].HeaderText = "Itemname";
            HFGP2.Columns[1].DataPropertyName = "itemspec1";
            HFGP2.Columns[1].Width = 300;
            HFGP2.Columns[2].Name = "itemcode";
            HFGP2.Columns[2].HeaderText = "itemcode";
            HFGP2.Columns[2].DataPropertyName = "itemcode";
            HFGP2.Columns[2].Width = 140;
            HFGP2.Columns[3].Name = "qty";
            HFGP2.Columns[3].HeaderText = "qty";
            HFGP2.Columns[3].DataPropertyName = "qty";
            HFGP2.Columns[3].Width = 140;

            HFGP2.DataSource = bsParty;
            HFGP2.Columns[0].Visible = false;






        }

        private void txtstyle_TextChanged(object sender, EventArgs e)
        {

        }

        private void button17_Click(object sender, EventArgs e)
        {


         
                addpnl.Visible = true;
            
        }

        private void button19_Click(object sender, EventArgs e)
        {
            if (HFIT.Rows.Count > 1)
            {
                if (HFIT.CurrentCell.ColumnIndex == 20)

                {
                    HFIT.CurrentRow.Cells[20].Value = txtadd.Text;
                }

            }
           
            addpnl.Visible = false;
        
        }

        private void cbowo_TextChanged(object sender, EventArgs e)
        {
            try
            {

                if (cbowo.Text != "")
                {
                    if (SelectId == 0)
                    {
                        bs.Filter = string.Format("socno LIKE '%{0}%' ", cbowo.Text);



                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void cbowo_Click(object sender, EventArgs e)
        {

            if(Text=="Yarn Purchase")
            {
                ty = 2;

            }
            else if (Text == "Trims Purchase")
            {
                ty = 3;
            }
            else if (Text == "Fabric Purchase")
            {
                ty = 4;
            }
            DataTable dt = getParty();
            bs.DataSource = dt;
            FillGrid1(dt, 1);
            Point loc = FindLocation(cbowo);
            grSearch.Location = new Point(loc.X, loc.Y + 20);
            grSearch.Visible = true;
            grSearch.Text = "Socno Search";
        }

        private void cbowo_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.F10)
                {
                    grSearch.Visible = false;
                }
                else if (e.KeyCode == Keys.F2)
                {
                    SelectId = 1;
                    int Index = DataGridCommon.SelectedCells[0].RowIndex;

                    cbowo.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    cbowo.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();


                    grSearch.Visible = false;
                    SelectId = 0;
                }
                else if (e.KeyCode == Keys.Escape)
                {
                    grSearch.Visible = false;
                }
                else if (e.KeyValue == 40)
                {
                    DataGridCommon.Select();
                }
                else if (e.KeyCode == Keys.Enter)
                {
                    cbowo_Click(sender, e);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

        }

        private void button20_Click(object sender, EventArgs e)
        {
            lkppnl.Visible = false; 
        }

        private void lkppnl_Paint(object sender, PaintEventArgs e)
        {

        }

        private void HFGST_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void Genpan_Paint(object sender, PaintEventArgs e)
        {

        }

        private void txtpadd1_TextChanged(object sender, EventArgs e)
        {

        }
    }
}

