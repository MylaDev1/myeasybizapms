﻿using System;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data;
using System.Reflection;
using System.Drawing;

namespace MyEasyBizAPMS
{
    public partial class FrmMain : Form
    {
        public FrmMain()
        {
            InitializeComponent();
            this.FormClosing += FrmMain_FormClosing;
        }
        ContextMenuStrip context;
        SQLDBHelper db = new SQLDBHelper();
        int Userid;
        TreeNode parentNode = null;
        TreeNode childNode = null;
        SqlConnection conn = new SqlConnection(GeneralParameters.ConnectionString);
        private void FrmMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                DialogResult result = MessageBox.Show("Do you really want to exit?", "Closing", MessageBoxButtons.YesNo, MessageBoxIcon.Stop);
                if (result == DialogResult.Yes)
                {
                    FrmLogin frmLogin = new FrmLogin();
                    frmLogin.Show();
                }
                else
                {
                    e.Cancel = true;
                }
            }
            else
            {
                e.Cancel = true;
            }
        }

        private void FrmMain_Load(object sender, EventArgs e)
        {
            LblLoginUser.Text = GeneralParameters.LoginUserName;
            MdiClient ctlMDI;
            foreach (Control ctl in this.Controls)
            {
                try
                {
                    ctlMDI = (MdiClient)ctl;

                    ctlMDI.BackColor = this.BackColor;
                }
                catch
                {
                }
            }
            Userid = GeneralParameters.UserdId;
            LeadParentMenu();
            cmbMenu.Width = this.Width - 1187;
            treeViewMenu.Top = this.Top + 35;
            treeViewMenu.Height = this.Height - 90;
            PanelMenu.Left = this.Left + 200;
            PanelMenu.Width = this.Width - 210;
            lblMenu.Width = PanelMenu.Width;
        }
        protected void LeadParentMenu()
        {
            try
            {
                SqlParameter[] para = { new SqlParameter("@UserId", Userid) };
                DataTable dt = db.GetDataWithParam(CommandType.StoredProcedure, "SP_GetParentMenu", para, conn);
                cmbMenu.DataSource = null;
                cmbMenu.DisplayMember = "MAINMNU";
                cmbMenu.ValueMember = "MENUPARVAL";
                cmbMenu.DataSource = dt;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information");
                return;
            }
        }

        private void Show_msg(object sender2, EventArgs e2, string v, int MenuId)
        {
            string Seqchildc = string.Empty;
            if (Userid != 1)
            {
                Seqchildc = "SELECT MENUPARVAL,FRM_NAME,MNUSUBMENU,STATUS,IsSubMnu FROM MNU_SUBMENU WHERE USERID =" + v.ToString() + " and  MENUPARVAL = '" + MenuId + "' and STATUS='Y' order by MNUSUBMENU";
            }
            else
            {
                Seqchildc = "SELECT MENUPARVAL,FRM_NAME,MNUSUBMENU,STATUS,IsSubMnu FROM MNU_SUBMENU WHERE USERID =" + v.ToString() + " and  MENUPARVAL = '" + MenuId + "' and STATUS='Y' order by MNUSUBMENU";
            }
            DataTable dtchildc = db.GetDataWithoutParam(CommandType.Text, Seqchildc, conn);
            treeViewMenu.Nodes.Clear();
            foreach (DataRow dr in dtchildc.Rows)
            {
                if (parentNode == null)
                {
                    string drn = dr["FRM_NAME"].ToString();
                    childNode = treeViewMenu.Nodes.Add(dr["FRM_NAME"].ToString());
                    childNode.Tag = dr["IsSubMnu"].ToString();
                    //foreach (string img in treeviewImgList.Images.Keys)
                    //{
                    //    if (drn == img)
                    //    {
                    //        childNode = treeViewMenu.Nodes.Add(img, dr["FRM_NAME"].ToString(), img);
                    //    }
                    //}
                }
                else
                {
                    string drn = dr["FRM_NAME"].ToString();
                    childNode = treeViewMenu.Nodes.Add(dr["FRM_NAME"].ToString());
                    childNode.Tag = dr["IsSubMnu"].ToString();
                    //foreach (string img in treeviewImgList.Images.Keys)
                    //{
                    //    if (drn == img)
                    //    {
                    //        childNode = treeViewMenu.Nodes.Add(img, dr["FRM_NAME"].ToString(), img);
                    //    }
                    //}
                }
            }
        }
     
        private void TreeViewMenu_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            try
            {
                string name = e.Node.Text;
                string SubMwnu = e.Node.Tag.ToString();
                if(SubMwnu == "1")
                {
                    context = new ContextMenuStrip();
                    ContextMenuStrip contexMenu = new ContextMenuStrip();
                    SqlParameter[] parameters = { new SqlParameter("@MenuName", e.Node.Text), new SqlParameter("@UserId", GeneralParameters.UserdId) };
                    DataTable dt = db.GetDataWithParam(CommandType.StoredProcedure, "Sp_GetSubmenuofMenu", parameters, conn);
                    ToolStripMenuItem[] subitems = new ToolStripMenuItem[dt.Rows.Count];
                    if (dt.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            subitems[i] = new ToolStripMenuItem
                            {
                                Name = dt.Rows[i]["FRM_NAME"].ToString(),
                                Tag = dt.Rows[i]["FRM_CODE"].ToString(),
                                Text = dt.Rows[i]["FRM_NAME"].ToString()
                            };
                        }
                    }
                    contexMenu.Items.AddRange(subitems);
                    contexMenu.Show();
                    contexMenu.ItemClicked += new ToolStripItemClickedEventHandler(ContexSubmenuMenu_ItemClicked);
                    Point point = new Point(treeViewMenu.Location.X + 185, e.Y);
                    context = contexMenu;
                    context.Location = point;
                }
                else
                {
                    SqlParameter[] para = { new SqlParameter("@Frm_Name", name), new SqlParameter("@userid", GeneralParameters.UserdId) };
                    DataTable dtransaction = db.GetDataWithParam(CommandType.StoredProcedure, "SP_getFromNew", para, conn);
                    Assembly frmAssembly = Assembly.LoadFile(Application.ExecutablePath);
                    foreach (Type type in frmAssembly.GetTypes())
                    {
                        lblMenu.Text = name;
                        if (type.BaseType == typeof(Form))
                        {
                            if (type.Name == dtransaction.Rows[0][0].ToString())
                            {
                                Form frmShow = (Form)frmAssembly.CreateInstance(type.ToString());
                                foreach (Form form in this.MdiChildren)
                                {
                                    form.Close();
                                }
                                frmShow.Text = dtransaction.Rows[0][1].ToString();
                                //if(frmShow.Text == "Order Entry" || frmShow.Text == "Order Amendment")
                                //{
                                //    frmShow.StartPosition = FormStartPosition.Manual;
                                //    frmShow.Location = new Point(200, 80);
                                //}
                                //else
                                //{
                                //    frmShow.StartPosition = FormStartPosition.CenterScreen;
                                //}
                                frmShow.StartPosition = FormStartPosition.Manual;
                                frmShow.Location = new Point(200, 50);
                                frmShow.FormBorderStyle = FormBorderStyle.None;
                                frmShow.MdiParent = this;
                                frmShow.Text = dtransaction.Rows[0][1].ToString();
                                frmShow.Show();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }


        }

        private void ContexSubmenuMenu_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            try
            {
                string name = e.ClickedItem.Text.ToString();
                SqlParameter[] para = { new SqlParameter("@Frm_Name", name) };
                DataTable dtransaction = db.GetDataWithParam(CommandType.StoredProcedure, "SP_getFrom", para, conn);
                Assembly frmAssembly = Assembly.LoadFile(Application.ExecutablePath);
                foreach (Type type in frmAssembly.GetTypes())
                {
                    lblMenu.Text = e.ClickedItem.Text;
                    if (type.BaseType == typeof(Form))
                    {
                        if (type.Name == dtransaction.Rows[0]["FRM_CODE"].ToString())
                        {
                            Form frmShow = (Form)frmAssembly.CreateInstance(type.ToString());
                            foreach (Form form in this.MdiChildren)
                            {
                                form.Close();
                            }
                            frmShow.StartPosition = FormStartPosition.Manual;
                            frmShow.Location = new Point(200, 50);
                            frmShow.FormBorderStyle = FormBorderStyle.None;
                            frmShow.MdiParent = this;
                            frmShow.Text = dtransaction.Rows[0][1].ToString();
                            frmShow.Show();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void CmbMenu_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Show_msg(sender, e, Userid.ToString(), Convert.ToInt32(cmbMenu.SelectedValue));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
        
        private void ContexMenu_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            ToolStripItem item = e.ClickedItem;
            if(item.Text == "User Profile")
            {
                MessageBox.Show(GeneralParameters.DbName, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            if (item.Text == "Sign Out")
            {
                this.Dispose();
                FrmLogin Logi = new FrmLogin
                {
                    StartPosition = FormStartPosition.CenterScreen
                };
                Logi.Show();
            }
        }

        private void LblLoginUser_Click(object sender, EventArgs e)
        {
            context = new ContextMenuStrip();
            ContextMenuStrip contexMenu = new ContextMenuStrip();
            contexMenu.Items.Add("User Profile");
            contexMenu.Items.Add("Sign Out");
            contexMenu.Show();
            contexMenu.ItemClicked += new ToolStripItemClickedEventHandler(ContexMenu_ItemClicked);
            Point loc = FindLocation(LblLoginUser);
            context = contexMenu;
            context.Location = new Point(loc.X, loc.Y + 50);
        }

        private void LblLoginUser_MouseHover(object sender, EventArgs e)
        {
            context = new ContextMenuStrip();
            ContextMenuStrip contexMenu = new ContextMenuStrip();
            contexMenu.Items.Add("User Profile");
            contexMenu.Items.Add("Sign Out");
            contexMenu.Show();
            contexMenu.ItemClicked += new ToolStripItemClickedEventHandler(ContexMenu_ItemClicked);
            Point loc = FindLocation(LblLoginUser);
            context = contexMenu;
            context.Location = new Point(loc.X, loc.Y + 50);
        }

        private Point FindLocation(Control ctrl)
        {
            if (ctrl.Parent is Form)
                return ctrl.Location;
            else
            {
                Point p = FindLocation(ctrl.Parent);
                p.X += ctrl.Location.X;
                p.Y += ctrl.Location.Y;
                return p;
            }
        }

        private void LblLoginUser_MouseLeave(object sender, EventArgs e)
        {
            
        }

        private void LblLoginUser_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {

        }
    }
}
