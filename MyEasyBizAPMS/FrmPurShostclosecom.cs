﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Excel = Microsoft.Office.Interop.Excel;
using ExcelAutoFormat = Microsoft.Office.Interop.Excel.XlRangeAutoFormat;
namespace MyEasyBizAPMS
{
    public partial class FrmPurShostclosecom : Form
    {
        private CrystalDecisions.Windows.Forms.CrystalReportViewer Cryview;
        public FrmPurShostclosecom()
        {
            InitializeComponent();
        }
        string uid = "";
        int mode = 0;
        string tpuid = "";
        SqlConnection conn = new SqlConnection(GeneralParameters.ConnectionString);
        SqlCommand qur = new SqlCommand();
        private DataRow doc1;
        DataTable Docno = new DataTable();
        private static Microsoft.Office.Interop.Excel.Workbook mWorkBook;
        private static Microsoft.Office.Interop.Excel.Sheets mWorkSheets;
        private static Microsoft.Office.Interop.Excel.Worksheet mWSheet1;
        BindingSource bs = new BindingSource();
        BindingSource bsc = new BindingSource();
        BindingSource bsp = new BindingSource();
        BindingSource bsserial = new BindingSource();
        BindingSource bsc1 = new BindingSource();
        BindingSource bsc2 = new BindingSource();

        SQLDBHelper db = new SQLDBHelper();
        int Fillid;
        BindingSource bsParty = new BindingSource();
        string str1key;
        public int SelectId = 0;
        DataGridViewComboBoxCell cbxComboRow6;
        ComboBox cbxRow5;
        private void FrmPurShostclosecom_Load(object sender, EventArgs e)
        {

            dtpfrom.Format = DateTimePickerFormat.Custom;
            dtpfrom.CustomFormat = "dd/MM/yyyy";
            dtpto.Format = DateTimePickerFormat.Custom;
            dtpto.CustomFormat = "dd/MM/yyyy";
            grSearch.Visible = false;
            GrApprove.Visible = false;


        }

        private void btnHide_Click(object sender, EventArgs e)
        {
            grSearch.Visible = false;
        }

        private void button13_Click(object sender, EventArgs e)
        {
            LoadGetJobCard(1);
        }
        protected DataTable LoadGetJobCard1(int tag)
        {



            DataTable dt = new DataTable();
            try
            {

                SqlParameter[] para = {
                        new SqlParameter("@poid",txtname.Tag),




                };

                dt = db.GetData(CommandType.StoredProcedure, "sp_getpassponoshortNew", para);


                LoadDataTable(dt);



            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        protected void LoadDataTable1(DataTable dt)
        {
            try
            {

                this.RQGR.DefaultCellStyle.Font = new Font("calibri", 10);
                this.RQGR.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 10, FontStyle.Bold);
                RQGR.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
                RQGR.EnableHeadersVisualStyles = false;
                RQGR.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
                RQGR.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
                RQGR.RowHeadersVisible = false;
                RQGR.DataSource = null;

                bs.DataSource = dt;

                RQGR.DataSource = bs;
                RQGR.Columns[0].Width = 100;
                RQGR.Columns[1].Width = 300;
                RQGR.Columns[2].Width = 150;
                RQGR.Columns[3].Width = 150;
                RQGR.Columns[4].Width = 150;
                RQGR.Columns[5].Width = 100;
                RQGR.Columns[6].Visible = false;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected DataTable LoadGetJobCard(int tag)
        {



            DataTable dt = new DataTable();
            try
            {

                SqlParameter[] para = {
                        new SqlParameter("@poid",txtname.Tag),




                };

                dt = db.GetData(CommandType.StoredProcedure, "sp_getpassponoshortNew", para);


                LoadDataTable(dt);



            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        protected void LoadDataTable(DataTable dt)
        {
            try
            {

                this.RQGR.DefaultCellStyle.Font = new Font("calibri", 10);
                this.RQGR.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 10, FontStyle.Bold);
                RQGR.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
                RQGR.EnableHeadersVisualStyles = false;
                RQGR.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
                RQGR.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
                RQGR.RowHeadersVisible = false;
                RQGR.DataSource = null;

                bs.DataSource = dt;

                RQGR.DataSource = bs;
                RQGR.Columns[0].Width = 100;
                RQGR.Columns[1].Width = 300;
                RQGR.Columns[2].Width = 150;
                RQGR.Columns[3].Width = 150;
                RQGR.Columns[4].Width = 150;
                RQGR.Columns[5].Width = 100;
                RQGR.Columns[6].Visible = false;



            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void LoadData1(DataTable dt)
        {
            try
            {

                this.RQGR.DefaultCellStyle.Font = new Font("calibri", 10);
                this.RQGR.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 10, FontStyle.Bold);
                RQGR.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
                RQGR.EnableHeadersVisualStyles = false;
                RQGR.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
                RQGR.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
                RQGR.RowHeadersVisible = false;
                RQGR.DataSource = null;

                bs.DataSource = dt;

                RQGR.DataSource = bs;
                //RQGR.Columns[0].Width = 80;
                //RQGR.Columns[1].Width = 80;
                //RQGR.Columns[2].Width = 200;
                //RQGR.Columns[3].Width = 150;
                //RQGR.Columns[4].Width = 80;
                //RQGR.Columns[5].Width = 80;
                //RQGR.Columns[6].Width = 80;
                //RQGR.Columns[7].Width = 80;
                RQGR.Columns[8].Visible = false;
                //RQGR.Columns[9].Width = 80;
                //RQGR.Columns[10].Width = 80;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private void button1_Click(object sender, EventArgs e)
        {

            Microsoft.Office.Interop.Excel._Application app = new Microsoft.Office.Interop.Excel.Application();

            Microsoft.Office.Interop.Excel._Workbook workbook = app.Workbooks.Add(Type.Missing);

            Microsoft.Office.Interop.Excel._Worksheet worksheet = null;

            Genclass.sum1 = 0; Genclass.sum2 = 0;
            app.Visible = true;
            worksheet = workbook.Sheets["Sheet1"];
            if (this.Text == "GRN Register")
            {
                worksheet.Name = "GRN REGISTER";


                worksheet.Cells[1, 11] = "SV KNITS";
                worksheet.Cells[2, 11] = "GRN REGISTER";



                for (int x = 1; x < RQGR.Columns.Count + 1; x++)
                {
                    worksheet.Cells[3, x] = RQGR.Columns[x - 1].HeaderText;
                }
                Microsoft.Office.Interop.Excel.Range range2 = worksheet.Cells[1, 1] as Microsoft.Office.Interop.Excel.Range;
                range2.EntireRow.Font.Name = "Calibri";
                range2.EntireRow.Font.Bold = true;
                range2.EntireRow.Font.Size = 16;
                range2 = worksheet.get_Range("A1", "M1");
                range2.BorderAround(Excel.XlLineStyle.xlContinuous, Excel.XlBorderWeight.xlMedium, Excel.XlColorIndex.xlColorIndexAutomatic, Excel.XlColorIndex.xlColorIndexAutomatic);
                range2.EntireRow.Font.Name = "Calibri";
                range2.EntireRow.Font.Bold = true;
                range2.EntireRow.Font.Size = 16;
                range2 = worksheet.get_Range("A2", "M2");
                range2.BorderAround(Excel.XlLineStyle.xlContinuous, Excel.XlBorderWeight.xlMedium, Excel.XlColorIndex.xlColorIndexAutomatic, Excel.XlColorIndex.xlColorIndexAutomatic);
                range2.EntireRow.Font.Name = "Calibri";
                range2.EntireRow.Font.Bold = true;
                range2.EntireRow.Font.Size = 16;
                range2 = worksheet.get_Range("A3", "M3");
                range2.BorderAround(Excel.XlLineStyle.xlContinuous, Excel.XlBorderWeight.xlMedium, Excel.XlColorIndex.xlColorIndexAutomatic, Excel.XlColorIndex.xlColorIndexAutomatic);


                range2.EntireRow.Font.Name = "Calibri";
                range2.EntireRow.Font.Bold = true;
                range2.EntireRow.Font.Size = 12;




                worksheet.get_Range("A1", "D1").Style.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
                worksheet.get_Range("A2", "D2").Style.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
                worksheet.Range["A3"].ColumnWidth = 15;
                worksheet.Range["B3"].ColumnWidth = 15;
                worksheet.Range["C3"].ColumnWidth = 30;
                worksheet.Range["D3"].ColumnWidth = 30;
                worksheet.Range["E3"].ColumnWidth = 15;
                worksheet.Range["F3"].ColumnWidth = 15;
                worksheet.Range["G3"].ColumnWidth = 15;
                worksheet.Range["H3"].ColumnWidth = 15;
                worksheet.Range["I3"].ColumnWidth = 15;
                worksheet.Range["J3"].ColumnWidth = 15;
                worksheet.Range["K3"].ColumnWidth = 15;
                worksheet.Range["A1:M1"].MergeCells = true;
                worksheet.Range["A2:M2"].MergeCells = true;
                Genclass.sum = RQGR.Rows.Count;
                for (int i = 0; i < RQGR.Rows.Count - 1; i++)
                {
                    for (int j = 0; j < RQGR.Columns.Count; j++)
                    {

                        worksheet.Cells[i + 5, j + 1] = RQGR.Rows[i].Cells[j].Value.ToString();


                    }
                }
            }
            else
            {

                worksheet.Name = "PURCHASE ORDER REGISTER";


                worksheet.Cells[1, 8] = "SV KNITS";
                worksheet.Cells[2, 8] = "PURCHASE ORDER REGISTER";



                for (int x = 1; x < RQGR.Columns.Count + 1; x++)
                {
                    worksheet.Cells[3, x] = RQGR.Columns[x - 1].HeaderText;
                }
                Microsoft.Office.Interop.Excel.Range range2 = worksheet.Cells[1, 1] as Microsoft.Office.Interop.Excel.Range;
                range2.EntireRow.Font.Name = "Calibri";
                range2.EntireRow.Font.Bold = true;
                range2.EntireRow.Font.Size = 16;
                range2 = worksheet.get_Range("A1", "l1");
                range2.BorderAround(Excel.XlLineStyle.xlContinuous, Excel.XlBorderWeight.xlMedium, Excel.XlColorIndex.xlColorIndexAutomatic, Excel.XlColorIndex.xlColorIndexAutomatic);
                range2.EntireRow.Font.Name = "Calibri";
                range2.EntireRow.Font.Bold = true;
                range2.EntireRow.Font.Size = 16;
                range2 = worksheet.get_Range("A2", "l2");
                range2.BorderAround(Excel.XlLineStyle.xlContinuous, Excel.XlBorderWeight.xlMedium, Excel.XlColorIndex.xlColorIndexAutomatic, Excel.XlColorIndex.xlColorIndexAutomatic);
                range2.EntireRow.Font.Name = "Calibri";
                range2.EntireRow.Font.Bold = true;
                range2.EntireRow.Font.Size = 16;
                range2 = worksheet.get_Range("A3", "l3");
                range2.BorderAround(Excel.XlLineStyle.xlContinuous, Excel.XlBorderWeight.xlMedium, Excel.XlColorIndex.xlColorIndexAutomatic, Excel.XlColorIndex.xlColorIndexAutomatic);


                range2.EntireRow.Font.Name = "Calibri";
                range2.EntireRow.Font.Bold = true;
                range2.EntireRow.Font.Size = 12;




                worksheet.get_Range("A1", "l1").Style.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
                worksheet.get_Range("A2", "l2").Style.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
                worksheet.Range["A3"].ColumnWidth = 30;
                worksheet.Range["B3"].ColumnWidth = 30;
                worksheet.Range["C3"].ColumnWidth = 30;
                worksheet.Range["D3"].ColumnWidth = 30;
                worksheet.Range["E3"].ColumnWidth = 30;
                worksheet.Range["F3"].ColumnWidth = 30;
                worksheet.Range["G3"].ColumnWidth = 30;
                worksheet.Range["H3"].ColumnWidth = 30;

                worksheet.Range["A1:l1"].MergeCells = true;
                worksheet.Range["A2:l2"].MergeCells = true;
                Genclass.sum = RQGR.Rows.Count;
                for (int i = 0; i < RQGR.Rows.Count - 1; i++)
                {
                    for (int j = 0; j < RQGR.Columns.Count; j++)
                    {

                        worksheet.Cells[i + 5, j + 1] = RQGR.Rows[i].Cells[j].Value.ToString();
                        //if (j == 5 && RQGR.Rows[i].Cells[j].Value.ToString() != "")
                        //{
                        //    Genclass.sum1 = Genclass.sum1 + Convert.ToDouble(RQGR.Rows[i].Cells[j].Value.ToString());

                        //}
                        //else if (j == 7 && RQGR.Rows[i].Cells[j].Value.ToString() != "")
                        //{
                        //    Genclass.sum2 = Genclass.sum2 + Convert.ToDouble(RQGR.Rows[i].Cells[j].Value.ToString());

                        //}

                    }
                }
                //Genclass.sum = Genclass.sum + 4;
                //worksheet.Cells[Genclass.sum, 3] = "TOTAL";
                //worksheet.Cells[Genclass.sum, 6] = Genclass.sum1;
                //worksheet.Cells[Genclass.sum, 8] = Genclass.sum2;
            }
        }

        private void txtname_Click(object sender, EventArgs e)
        {
            DataTable dt = getParty();
            bsp.DataSource = dt;
            FillGrid2(dt, 1);
            Point loc = FindLocation(txtname);
            //grSearch.Location = new Point(500, 32);
            grSearch.Visible = true;
            grSearch.Text = "Name Search";
        }

        private void txtname_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.F10)
                {
                    grSearch.Visible = false;
                }
                else if (e.KeyCode == Keys.F2)
                {
                    SelectId = 1;
                    int Index = DataGridCommon.SelectedCells[0].RowIndex;

                    txtname.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtname.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();


                    grSearch.Visible = false;
                    SelectId = 0;
                }
                else if (e.KeyCode == Keys.Escape)
                {
                    grSearch.Visible = false;
                }
                else if (e.KeyValue == 40)
                {
                    DataGridCommon.Select();
                }
                else if (e.KeyCode == Keys.Enter)
                {
                    txtname_Click(sender, e);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
        private Point FindLocation(Control ctrl)
        {
            if (ctrl.Parent is Form)
                return ctrl.Location;
            else
            {
                Point p = FindLocation(ctrl.Parent);
                p.X += ctrl.Location.X;
                p.Y += ctrl.Location.Y;
                return p;
            }
        }
        protected void FillGrid2(DataTable dt, int FillId)
        {
            try
            {
                this.DataGridCommon.DefaultCellStyle.Font = new Font("calibri", 10);
                this.DataGridCommon.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 10, FontStyle.Bold);
                DataGridCommon.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
                DataGridCommon.EnableHeadersVisualStyles = false;
                DataGridCommon.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
                DataGridCommon.DefaultCellStyle.WrapMode = DataGridViewTriState.True;



                DataGridCommon.ColumnCount = 2;
                DataGridCommon.Columns[0].Name = "Uid";
                DataGridCommon.Columns[0].HeaderText = "Uid";
                DataGridCommon.Columns[0].DataPropertyName = "Uid";

                DataGridCommon.Columns[1].Name = "PoNo";
                DataGridCommon.Columns[1].HeaderText = "PoNo";
                DataGridCommon.Columns[1].DataPropertyName = "PoNo";
                DataGridCommon.Columns[1].Width = 300;





                DataGridCommon.DataSource = bsp;
                DataGridCommon.Columns[0].Visible = false;
                DataGridCommon.Columns[2].Visible = false;
                DataGridCommon.Columns[3].Visible = false;
                DataGridCommon.Columns[4].Visible = false;
                DataGridCommon.Columns[5].Visible = false;
                DataGridCommon.Columns[6].Visible = false;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        protected DataTable getParty()
        {
            DataTable dt = new DataTable();
            try
            {

                dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "sp_getPoNOShortclosed", conn);
                bsp.DataSource = dt;


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            return dt;
        }
        private void button18_Click(object sender, EventArgs e)
        {
            try
            {
                SelectId = 1;
                int Index = DataGridCommon.SelectedCells[0].RowIndex;

                txtname.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                txtname.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();



                grSearch.Visible = false;
                SelectId = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void DataGridCommon_DoubleClick(object sender, EventArgs e)
        {
            SelectId = 1;
            int Index = DataGridCommon.SelectedCells[0].RowIndex;

            txtname.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
            txtname.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();



            grSearch.Visible = false;
            SelectId = 0;
        }

        private void DataGridCommon_KeyDown(object sender, KeyEventArgs e)
        {
            SelectId = 1;
            int Index = DataGridCommon.SelectedCells[0].RowIndex;
            txtname.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
            txtname.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
        }

        private void DataGridCommon_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void txtgen1_TextChanged(object sender, EventArgs e)
        {
            bs.Filter = string.Format("itemname Like '%{0}%'   ", txtgen1.Text);
        }

        private void buttnext1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtname_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bsp.Filter = string.Format("pono LIKE '%{0}%' ", txtname.Text);



                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            bs.Filter = string.Format("socno Like '%{0}%'   ", textBox1.Text);
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            bs.Filter = string.Format("style Like '%{0}%'   ", textBox2.Text);
        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {

            if (checkBox1.Checked == true && checkBox2.Checked == false)
            {
                Genclass.itemgrid = 0;

            }
            else if (checkBox2.Checked == true && checkBox1.Checked == false)

            {
                Genclass.itemgrid = 1;

            }
            else if (checkBox3.Checked == true && checkBox1.Checked == false && checkBox2.Checked == false)

            {
                Genclass.itemgrid = 2;


            }

            DataTable dt = new DataTable();
           
                SqlParameter[] para = {
                new SqlParameter("@poid",txtname.Tag),
                   new SqlParameter("@type",Genclass.itemgrid),



                };

                dt = db.GetData(CommandType.StoredProcedure, "sp_getpassponoshort", para);


                LoadDataTable1(dt);
            }
            
    

        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked == true && checkBox2.Checked == false)
            {
                Genclass.itemgrid = 0;

            }
            else if (checkBox2.Checked == true && checkBox1.Checked == false)

            {
                Genclass.itemgrid = 1;

            }
            else if (checkBox3.Checked == true && checkBox1.Checked == false && checkBox2.Checked == false)

            {
                Genclass.itemgrid = 2;

            }
            DataTable dt = new DataTable();

            SqlParameter[] para = {
                new SqlParameter("@poid",txtname.Tag),
                   new SqlParameter("@type",Genclass.itemgrid),



                };

            dt = db.GetData(CommandType.StoredProcedure, "sp_getpassponoshort", para);


            LoadDataTable1(dt);
        }

        private void checkBox3_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked == true && checkBox2.Checked == false)
            {
                Genclass.itemgrid = 0;

            }
            else if (checkBox2.Checked == true && checkBox1.Checked == false)

            {
                Genclass.itemgrid = 1;

            }
            else if (checkBox3.Checked == true && checkBox1.Checked == false && checkBox2.Checked == false)

            {
                Genclass.itemgrid = 2;

            }
            DataTable dt = new DataTable();

            SqlParameter[] para = {
                new SqlParameter("@poid",txtname.Tag),
                   new SqlParameter("@type",Genclass.itemgrid),



                };

            dt = db.GetData(CommandType.StoredProcedure, "sp_getpassponoshort", para);


            LoadDataTable1(dt);
        }
        private DataTable Ctype()
        {
            DataTable tab = new DataTable();

            string qur = "select * from  statusM";
            SqlCommand cmd = new SqlCommand(qur, conn);
            SqlDataAdapter apt = new SqlDataAdapter(cmd);
            apt.Fill(tab);

            return tab;


        }
       
        private void RQGR_CellClick(object sender, DataGridViewCellEventArgs e)
        {
           
        }

        private void RQGR_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            
        }

        private void RQGR_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void RQGR_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex== 0 || e.ColumnIndex == 2 || e.ColumnIndex == 3|| e.ColumnIndex == 4 || e.ColumnIndex == 5)
                {
                    GrApprove.Visible = true;
                    lblOrderNo.Text = RQGR.CurrentRow.Cells[0].Value.ToString();
                    lblStyle.Text = RQGR.CurrentRow.Cells[1].Value.ToString();
                    lblSeason.Text = RQGR.CurrentRow.Cells[2].Value.ToString();

                    label12.Text = RQGR.CurrentRow.Cells[3].Value.ToString();
             

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void splitButton1_Click(object sender, EventArgs e)
        {
           
            SqlParameter[] sqlParameters = {

                    new SqlParameter("@status",CmbStatus.Text),
                    new SqlParameter("@socno",txtRemarks.Text),
                    new SqlParameter("@uid", RQGR.CurrentRow.Cells[6].Value.ToString()),
                };
            db.ExecuteNonQuery(CommandType.StoredProcedure, "sp_getshorclosedpurchase", sqlParameters, conn);

            GrApprove.Visible = false;
            lblOrderNo.Text = string.Empty;
            lblStyle.Text = string.Empty;
            lblSeason.Text = string.Empty;
            lblOrderQty.Text = string.Empty;
            lblReqQty.Text = string.Empty;
            lblIncome.Text = string.Empty;
            lblExpense.Text = string.Empty;
            lblPLValue.Text = string.Empty;
            lblPLPer.Text = string.Empty;
            CmbStatus.Text = string.Empty;
            LoadGetJobCard(1);
        }
    }
}
