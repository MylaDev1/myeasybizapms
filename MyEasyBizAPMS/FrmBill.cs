using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Drawing.Imaging;
using System.Configuration;
using System.Data.SqlClient;
using System.Diagnostics;
using excel = Microsoft.Office.Interop.Excel;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;

//using Bytescout.Spreadsheet;
namespace MyEasyBizAPMS
{
    public partial class FrmBill : Form
    {
        public string fo
        {
            get { return txtitemname.Text; }
            set { txtitemname.Text = value; }

        }
        public FrmBill()
        {

            CenterToParent();
            this.BackColor = Color.White;
            InitializeComponent();
        }
        private CrystalDecisions.Windows.Forms.CrystalReportViewer Cryview;


        //private System.ComponentModel.Container components = null;
        private DataRow doc1;
        DataTable Docno = new DataTable();
        ReportDocument doc = new ReportDocument();
        string uid = "";
        int mode = 0;
        double dis9 = 0;
        double dis3 = 0;
        double dis4 = 0;
        double dd8 = 0;
        double dd1 = 0;
        double dd2 = 0;
        double dd3 = 0;
        int cell9 = 0;
        double hg = 0;
        double df = 0;
        int j = -1;

        SQLDBHelper db = new SQLDBHelper();
        int Fillid;
        BindingSource bsParty = new BindingSource();
        BindingSource bs = new BindingSource();
        string str1key;
        public int SelectId = 0;
        private static Microsoft.Office.Interop.Excel.Workbook mWorkBook;
        private static Microsoft.Office.Interop.Excel.Sheets mWorkSheets;
        private static Microsoft.Office.Interop.Excel.Worksheet mWSheet1;
        private static Microsoft.Office.Interop.Excel.Application oXL;
        public event EventHandler ButtonFirstFormClicked;
        double loadsum = 0;

        //SqlCommand cmd;
        SqlConnection conn = new SqlConnection(GeneralParameters.ConnectionString);

        //SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connstr"].ConnectionString);
        SqlCommand qur = new SqlCommand();
        private void FrmBill_Load(object sender, EventArgs e)
        {
            qur.Connection = conn;
            Genclass.Dtype = 8;
            //Left = (MdiParent.ClientRectangle.Width - Width) / 3;
            //Top = (MdiParent.ClientRectangle.Height - Height) / 3;
            HFGP.RowHeadersVisible = false;
            HFGPT.RowHeadersVisible = false;
            HFIT.RowHeadersVisible = false;
            HFGST.RowHeadersVisible = false;
            HFGT.RowHeadersVisible = false;
            dtpfnt.Value = DateTime.Now;
            Genpan.Visible = true;
            Taxpan.Visible = false;
            Editpan.Visible = false;
            //DataGridViewCell cell1 = HFIT.CurrentRow.Cells[0];
            Titleterm();
            Titlep();
            Titlegst();
            this.HFGPT.DefaultCellStyle.Font = new Font("calibri", 10);
            this.HFGPT.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 10, FontStyle.Bold);
            this.HFGP.DefaultCellStyle.Font = new Font("calibri", 10);
            this.HFGP.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 10, FontStyle.Bold);
            this.HFIT.DefaultCellStyle.Font = new Font("calibri", 9);
            this.HFIT.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 9, FontStyle.Bold);

            this.HFGST.DefaultCellStyle.Font = new Font("calibri", 9);
            this.HFGST.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 9, FontStyle.Bold);


            txtplace.ReadOnly = true;
            txtitemname.ReadOnly = true;



            txttbval.ReadOnly = true;
            txtexcise.ReadOnly = true;
            txtigval.ReadOnly = true;
            txtttot.ReadOnly = true;
            TxtNetAmt.ReadOnly = true;
            loadtax();



            Genclass.buttonstyleform(this);
            Genclass.buttonstylepanel(panadd);

            panadd.Visible = true;
            HFGPT.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            HFGPT.EnableHeadersVisualStyles = false;
            HFGPT.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.GradientActiveCaption;
            HFGP.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            HFGP.EnableHeadersVisualStyles = false;
            HFGP.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.GradientActiveCaption;
            HFIT.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            HFIT.EnableHeadersVisualStyles = false;
            HFIT.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.GradientActiveCaption;



            HFGP.Focus();
            Genclass.sum1 = 0;
            chkact.Checked = true;
         
            serialno.Visible = false;

            mappnl.Visible = false;

            conn.Close();
            conn.Open();

            dtpfnt.Format = DateTimePickerFormat.Custom;
            dtpfnt.CustomFormat = "dd/MM/yyyy";
            dcdate.Format = DateTimePickerFormat.Custom;
            dcdate.CustomFormat = "dd/MM/yyyy";
            DTPDOCDT.Format = DateTimePickerFormat.Custom;
            DTPDOCDT.CustomFormat = "dd/MM/yyyy";
            Genpan.Width = 1298;
            Genpan.Height = 618;

        }
        protected DataTable LoadGetJobCard(int tag)
        {

            int SP;
            chkact.Checked = true;
            if (chkact.Checked == true)
            {
                SP = 1;

            }
            else
            {
                SP = 0;

            }

            DateTime str9 = Convert.ToDateTime(dtpfnt.Text);
            DataTable dt = new DataTable();
            try
            {

                SqlParameter[] para = {
                    new SqlParameter("@active",SP),
                      new SqlParameter("@COMPANYID","1"),
                       new SqlParameter("@MONTH",str9.Month),
                      new SqlParameter("@YEAR",str9.Year),


                };

                dt = db.GetData(CommandType.StoredProcedure, "SP_GETBILLLOAD", para);


                LoadDataTable(dt);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        protected void LoadDataTable(DataTable dt)
        {
            try
            {
           
                    

                HFGP.DataSource = null;
                HFGP.AutoGenerateColumns = false;
                HFGP.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
                HFGP.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
                HFGP.ColumnCount = 15;
                HFGP.Columns[0].Name = "Uid";
                HFGP.Columns[0].HeaderText = "Uid";
                HFGP.Columns[0].DataPropertyName = "Uid";

                HFGP.Columns[1].Name = "Docno";
                HFGP.Columns[1].HeaderText = "Docno";
                HFGP.Columns[1].DataPropertyName = "Docno";

                HFGP.Columns[2].Name = "Docdate";
                HFGP.Columns[2].HeaderText = "Docdate";
                HFGP.Columns[2].DataPropertyName = "Docdate";

                HFGP.Columns[3].Name = "InvNo";
                HFGP.Columns[3].HeaderText = "InvNo";
                HFGP.Columns[3].DataPropertyName = "InvNo";

                HFGP.Columns[4].Name = "InvDate";
                HFGP.Columns[4].HeaderText = "InvDate";
                HFGP.Columns[4].DataPropertyName = "InvDate";

                HFGP.Columns[5].Name = "Name";
                HFGP.Columns[5].HeaderText = "Name";
                HFGP.Columns[5].DataPropertyName = "Name";

                HFGP.Columns[6].Name = "Basicvalue";
                HFGP.Columns[6].HeaderText = "Basicvalue";
                HFGP.Columns[6].DataPropertyName = "Basicvalue";

                HFGP.Columns[7].Name = "Charges";
                HFGP.Columns[7].HeaderText = "Charges";
                HFGP.Columns[7].DataPropertyName = "Charges";

                HFGP.Columns[8].Name = "Taxvalue";
                HFGP.Columns[8].HeaderText = "Taxvalue";
                HFGP.Columns[8].DataPropertyName = "Taxvalue";

                HFGP.Columns[9].Name = "Netvalue";
                HFGP.Columns[9].HeaderText = "Netvalue";
                HFGP.Columns[9].DataPropertyName = "Netvalue";
                HFGP.Columns[10].Name = "Partyuid";
                HFGP.Columns[10].HeaderText = "Partyuid";
                HFGP.Columns[10].DataPropertyName = "Partyuid";

                HFGP.Columns[11].Name = "Placeuid";
                HFGP.Columns[11].HeaderText = "Placeuid";
                HFGP.Columns[11].DataPropertyName = "Placeuid";

                HFGP.Columns[12].Name = "remarks";
                HFGP.Columns[12].HeaderText = "remarks";
                HFGP.Columns[12].DataPropertyName = "remarks";

                HFGP.Columns[13].Name = "roff";
                HFGP.Columns[13].HeaderText = "roff";
                HFGP.Columns[13].DataPropertyName = "roff";
                HFGP.Columns[14].Name = "type";
                HFGP.Columns[14].HeaderText = "type";
                HFGP.Columns[14].DataPropertyName = "type";


                bs.DataSource = dt;

                HFGP.DataSource = bs;

                HFGP.Columns[0].Visible = false;

                HFGP.Columns[1].Width = 60;
                HFGP.Columns[2].Width = 80;

                HFGP.Columns[3].Width = 100;
         
                HFGP.Columns[4].Width = 70;
                HFGP.Columns[5].Width = 300;
                HFGP.Columns[6].Width = 98;
                HFGP.Columns[7].Width = 60;
                HFGP.Columns[8].Width = 100;
                HFGP.Columns[9].Width = 100;

                HFGP.Columns[10].Visible = false;
                HFGP.Columns[11].Visible = false;
                HFGP.Columns[12].Visible = false;
                HFGP.Columns[13].Visible = false;
                HFGP.Columns[14].Visible = false;
                //HFGP.Columns[6].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                //HFGP.Columns[7].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                //HFGP.Columns[8].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                //HFGP.Columns[9].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;


            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void loadput1()
        {


           

        }

        private void loadtax()
        {

            //conn.Open();
            //string qur = "select a.gUId,a.GeneralName from  GENERALM a inner join typem b  on a.TypeMUid=b.UId where a.TypeM_Uid in (4,5,6,7,8,9) and Active=1 ";
            //SqlCommand cmd = new SqlCommand(qur, conn);
            //SqlDataAdapter apt = new SqlDataAdapter(cmd);
            //DataTable tab = new DataTable();
            //apt.Fill(tab);
            ////cbotax.DataSource = null;
            ////cbotax.DataSource = tab;
            ////cbotax.DisplayMember = "GeneralName";
            ////cbotax.ValueMember = "uid";
            ////cbotax.SelectedIndex = -1;
            //conn.Close();



        }


        private void loaduomcon()
        {

            conn.Open();
            string qur = "select b.UId,b.itemname from  Batemp a inner join itemm b  on a.itid=b.UId where  Active=1 ";
            SqlCommand cmd = new SqlCommand(qur, conn);
            SqlDataAdapter apt = new SqlDataAdapter(cmd);
            DataTable tab = new DataTable();
            apt.Fill(tab);
            cbosGReturnItem.DataSource = null;
            cbosGReturnItem.DataSource = tab;
            cbosGReturnItem.DisplayMember = "itemname";
            cbosGReturnItem.ValueMember = "uid";
            //cbosGReturnItem.SelectedIndex = -1;
            conn.Close();



        }
        private void comboload()
        {



            for (int l = 0; l < HFIT.Rows.Count - 1; l++)
            {
                string qur = "select UId,itemname from  itemm   where  Active=1 and  uid=" + HFIT.Rows[l].Cells[6].Value + "  and uom_uid<>puomid ";

                SqlCommand cmd = new SqlCommand(qur, conn);
                SqlDataAdapter apt = new SqlDataAdapter(cmd);
                DataTable tab = new DataTable();
                apt.Fill(tab);
                if (tab.Rows.Count > 0)
                {

                    doc1 = Docno.NewRow();
                    doc1["uid"] = HFIT.Rows[l].Cells[6].Value;
                    doc1["docno"] = HFIT.Rows[l].Cells[0].Value;
                    Docno.Rows.Add(doc1);
                    cbosGReturnItem.DataSource = Docno;
                    cbosGReturnItem.DisplayMember = "docno";
                    cbosGReturnItem.ValueMember = "uid";

                }
            }

        }
        private void loadserial()
        {
            conn.Close();
            conn.Open();
            if (mode == 1)
            {
                string qur = "select distinct  b.UId,b.itemname from  Batemp a inner join itemm b  on a.itid=b.UId  where  Active=1 and serilze=1 ";

                SqlCommand cmd = new SqlCommand(qur, conn);
                SqlDataAdapter apt = new SqlDataAdapter(cmd);
                DataTable tab = new DataTable();
                apt.Fill(tab);
                cboserial.DataSource = null;
                cboserial.DataSource = tab;
                cboserial.DisplayMember = "itemname";
                cboserial.ValueMember = "uid";
                //cboserial.SelectedIndex = -1;
                conn.Close();
            }
            else

            {

                string qur = "select  b.UId,b.itemname from  StransactionspListStk a inner join stransactionsplist f on a.stlrefuid=f.uid inner join  stransactionsp g on f.transactionspuid=g.uid   inner join itemm b  on a.itemuid=b.UId  where  b.Active=1 and serilze=1  and g.docno='" + txtgrn.Text + "'";

                SqlCommand cmd = new SqlCommand(qur, conn);
                SqlDataAdapter apt = new SqlDataAdapter(cmd);
                DataTable tab = new DataTable();
                apt.Fill(tab);
                cboserial.DataSource = null;
                cboserial.DataSource = tab;
                cboserial.DisplayMember = "itemname";
                cboserial.ValueMember = "uid";
                //cboserial.SelectedIndex = -1;
                conn.Close();
            }





        }
       
        private void Titlegst()
        {
            if (txtpluid.Text != "")
            {
                if (txtpluid.Text == "196")
                {

                    HFGST.AutoGenerateColumns = false;
                    HFGST.Refresh();
                    HFGST.DataSource = null;
                    HFGST.Rows.Clear();
                    HFGST.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
                    HFGST.DefaultCellStyle.WrapMode = DataGridViewTriState.True;

                    HFGST.ColumnCount = 5;

                    HFGST.Columns[0].Name = "CGST%";
                    HFGST.Columns[1].Name = "CGST";

                    HFGST.Columns[2].Name = "SGST%";
                    HFGST.Columns[3].Name = "SGST";
                    HFGST.Columns[4].Name = "Total";


                    HFGST.Columns[0].Width = 60;
                    HFGST.Columns[1].Width = 70;
                    HFGST.Columns[2].Width = 60;
                    HFGST.Columns[3].Width = 70;
                    HFGST.Columns[4].Width = 60;

                }


                else
                {
                    HFGST.ColumnCount = 2;

                    HFGST.Columns[0].Name = "IGST%";
                    HFGST.Columns[1].Name = "IGST";
                    HFGST.Columns[0].Width = 100;
                    HFGST.Columns[1].Width = 150;

                }
            }
        }
        private void Titlep()
        {
            HFIT.AutoGenerateColumns = false;
            HFIT.Refresh();
            HFIT.DataSource = null;
            HFIT.Rows.Clear();
            HFIT.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            HFIT.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            HFIT.ColumnCount = 19;

            HFIT.Columns[0].Name = "Itemname";
            HFIT.Columns[1].Name = "UoM";

            HFIT.Columns[2].Name = "Price";
            HFIT.Columns[3].Name = "Qty";
            HFIT.Columns[4].Name = "Value";
            HFIT.Columns[5].Name = "Pono";
            HFIT.Columns[6].Name = "Itemuid";
            if (txtpluid.Text == "" || txtpluid.Text == "196")
            {
                HFIT.Columns[7].Name = "CGST%";
                HFIT.Columns[8].Name = "CGST";
                HFIT.Columns[13].Name = "SGST%";
                HFIT.Columns[14].Name = "SGST";
            }
            else
            {
                HFIT.Columns[7].Name = "IGST%";
                HFIT.Columns[8].Name = "IGST";
            }

            HFIT.Columns[9].Name = "Invno";
            HFIT.Columns[10].Name = "Refuid";
            HFIT.Columns[11].Name = "Hsnuid";
            HFIT.Columns[12].Name = "Total";
            HFIT.Columns[15].Name = "Item";
            HFIT.Columns[16].Name = "No Of bags";
            HFIT.Columns[17].Name = "Style";

            HFIT.Columns[18].Name = "Notes";

            HFIT.Columns[0].Width = 250;
            HFIT.Columns[1].Width = 50;
            HFIT.Columns[2].Width = 80;
            HFIT.Columns[3].Width = 80;

            HFIT.Columns[4].Width = 100;

            HFIT.Columns[6].Visible = false;
            HFIT.Columns[5].Width = 80;

            HFIT.Columns[7].Width = 70;
            HFIT.Columns[8].Visible = false;

            HFIT.Columns[9].Width = 100;
            HFIT.Columns[10].Visible = false;
            HFIT.Columns[11].Visible = false;
            HFIT.Columns[12].Visible = false;
            HFIT.Columns[13].Visible = false;
            HFIT.Columns[14].Visible = false;
            HFIT.Columns[15].Visible = false;
            HFIT.Columns[16].Visible = false;
            HFIT.Columns[17].Visible = false;
            HFIT.Columns[18].Visible = false;
        }

        private void Titlep2()
        {
            DataGridWeight.ColumnCount = 15;

            DataGridWeight.Columns[0].Name = "Itemname";
            DataGridWeight.Columns[1].Name = "UoM";

            DataGridWeight.Columns[2].Name = "Price";
            DataGridWeight.Columns[3].Name = "Qty";
            DataGridWeight.Columns[4].Name = "Value";
            DataGridWeight.Columns[5].Name = "Pono";
            DataGridWeight.Columns[6].Name = "Itemuid";


        }


        private void Loadgrid1()
        {
            try
            {
                conn.Open();

                string quy = "select c.itemname,b.pqty,b.itemuid,refuid from transactionsp a inner join transactionsplist b on a.uid=b.transactionspuid and a.companyid=" + GeneralParameters.UserdId + " inner join itemm c on b.itemuid=c.uid where a.uid=" + uid + "";


                Genclass.cmd = new SqlCommand(quy, conn);



                SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
                DataTable tap = new DataTable();
                aptr.Fill(tap);

                this.HFIT.DefaultCellStyle.Font = new Font("Calibri", 10);
                //dgv.ColumnHeadersDefaultCellStyle.Font = new Font(dgv.ColumnHeadersDefaultCellStyle.Font, FontStyle.Bold);
                this.HFIT.ColumnHeadersDefaultCellStyle.Font = new Font("Calibri", 10, FontStyle.Bold);
                HFIT.AutoGenerateColumns = false;
                HFIT.Refresh();
                HFIT.DataSource = null;
                HFIT.Rows.Clear();


                HFIT.ColumnCount = tap.Columns.Count;
                Genclass.i = 0;
                foreach (DataColumn column in tap.Columns)
                {
                    HFIT.Columns[Genclass.i].Name = column.ColumnName;
                    HFIT.Columns[Genclass.i].HeaderText = column.ColumnName;
                    HFIT.Columns[Genclass.i].DataPropertyName = column.ColumnName;
                    Genclass.i = Genclass.i + 1;
                }



                HFIT.Columns[0].Width = 450;

                HFIT.Columns[1].Width = 75;
                HFIT.Columns[2].Visible = false;
                HFIT.Columns[3].Visible = false;

                //HFIT.Columns[5].Width = 400;




                HFIT.DataSource = tap;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }
            finally
            {
                conn.Close();
                Genclass.cmd.Dispose();
            }
        }
        private void button1_Click(object sender, System.EventArgs e)
        {
            mode = 1;
            Genpan.Visible = false;

            Genclass.ClearTextBox(this, Editpan);
            if (Genclass.Dtype == 40)
            {
                Genclass.Gendocno();
                txtgrn.Text = Genclass.ST;
                label15.Visible = false;
                txtitemname.Visible = false;
                label14.Visible = false;
                txtprice.Visible = false;
                label31.Visible = false;
                txtqty.Visible = false;
                label32.Visible = false;
                txtbval.Visible = false;

            }
            else if (Genclass.Dtype == 80)
            {
                Genclass.Gendocno();
                txtgrn.Text = Genclass.ST;
            }
            Editpan.Visible = true;
            DTPDOCDT.Focus();
            HFIT.Refresh();
            HFIT.DataSource = null;
            HFIT.Rows.Clear();

        }

        private void loadput()
        {
            //conn.Open();

            if (Genclass.type == 1)
            {
                //Genclass.Module.Partylistviewcont3("uid", "Name", "Address1", Genclass.strsql, this, txtpuid, txtname, txttempadd1, Editpan);
                //Genclass.strsql = "select top 25 uid,Name as Party,Address1  from Partym where active=1 and ptype<>1  and  companyid=" + GeneralParameters.UserdId + " and type='Supplier' order by name";
                //Genclass.strsql = "select distinct Puid,name,address1 from  (select m.uid as Puid,m.name,m.address1,Docno, convert(varchar,Docdate,105) as Docdate,c.hsnid,c.itemname,c.uid,a.uid as Tuid, b.uid as listuid,b.pqty-isnull(sum(z.pqty),0) as qty,b.PRate as Price,b.BasicValue,i.f1  as gstper,convert(decimal(18,2),(b.BasicValue /100 * i.f1),105) as gstval from   TransactionsP a inner join TransactionsPList b on a.UId=b.TransactionsPUId and a.doctypeid=10 LEFT join stransactionsplist z on b.Uid=z.Refuid inner join itemm c on b.ItemUId=c.uid  left join   ItemGroup j on c.itemgroup_Uid=j.UId left join Hsndet f on j.hsnid=f.uid left join generalm i   on f.sgid=i.uid inner join partym m on a.partyuid=m.uid group by m.uid,m.name,m.address1,Docno,  Docdate,c.hsnid,c.itemname,c.uid,i.f1,b.pqty,b.PRate,b.BasicValue,i.f1,  a.uid,b.uid  having b.pqty-isnull(sum(z.pqty),0)>0) tab ";

                //Genclass.Partylistviewcont3("uid", "Name", "Vendorcode", Genclass.strsql, this, txtpuid, txtname, txttempadd1, Editpan);
                Genclass.strsql = "select top 25 uid,Name as Party,isnull(vendor,'') as Vendorcode  from Partym where active=1 and ptype<>1  and  companyid=" + GeneralParameters.UserdId + " and type='Supplier' order by name";
                Genclass.FSSQLSortStr = "Name";
            }

            else if (Genclass.type == 2)
            {
                //Genclass.Partylistviewcont("uid", "Docno", Genclass.strsql, this, txtdcid, txtdcno, Editpan);
                Genclass.strsql = "select distinct  a.uid,a.docno as Dcno from transactionsp a inner join transactionsplist b on  a.uid=b.transactionspuid and a.doctypeid=20 and a.companyid=" + GeneralParameters.UserdId + " inner join partym c on a.partyuid=c.uid   left join stransactionsplist e on  b.uid=e.refuid and e.doctypeid=40 group by a.uid,a.docno,b.pqty  having b.pqty-isnull(sum(e.pqty),0) >0";
                Genclass.FSSQLSortStr = "docno";
            }



            else if (Genclass.type == 5)
            {
                //Genclass.Partylistviewcont("uid", "Item", Genclass.strsql, this, txtgrossgen, txtBagNo, mappnl);
                //Genclass.strsql = "select distinct b.uid,itemname,price from pur_price_list a inner join itemm b on a.itemuid=b.uid where a.suppuid=" + txtpuid.Text + " and b.active=1 and a.companyid=1 and a.Eff_to is null";
                if (GeneralParameters.UserdId == 1)
                {
                    Genclass.strsql = "select distinct uid,itemname FROM itemm  where  active=1 and companyid=" + GeneralParameters.UserdId + "";
                }
                else
                {
                    Genclass.strsql = "select distinct uid,itemname FROM itemm  where  active=1 and companyid=" + GeneralParameters.UserdId + " and partyuid=" + txtpuid.Text + "";
                }
                Genclass.FSSQLSortStr = "itemname";
            }
            else if (Genclass.type == 6)
            {
                //Genclass.Partylistviewcont("uid", "Name", Genclass.strsql, this, txtpuid, txtname, Editpan);
                Genclass.strsql = "select top 50 uid,Name as Party from Partym where active=1 and ptype<>1 and  companyid=" + GeneralParameters.UserdId + "";
                //Genclass.strsql = "    select distinct b.uid,name from pur_price_list a inner join partym b on a.Suppuid=b.uid where  active=1 and a.companyid=1 and a.Eff_to is null";

                Genclass.FSSQLSortStr = "Name";
            }

            else if (Genclass.type == 7)
            {
                //Genclass.Partylistviewcont3("uid", "Name", "Address1", Genclass.strsql, this, txtpluid, txtplace, txttempadd2, Editpan);
                Genclass.strsql = "select top 25 uid,Name as Party,Address1  from Partym where active=1 and ptype<>1  and  companyid=" + GeneralParameters.UserdId + "";

                Genclass.FSSQLSortStr = "Name";
            }

            else if (Genclass.type == 8)
            {
                //Genclass.Partylistviewcont3("uid", "docno", "docdate", Genclass.strsql, this, txtdcid, txttrans, txtitemname, Editpan);
                Genclass.strsql = "select distinct Tuid,docno,docdate  from ( select m.uid as Puid,m.name,m.address1,Docno, convert(varchar,Docdate,105) as Docdate,c.hsnid,c.itemname,c.uid,a.uid as Tuid, b.uid as listuid,b.pqty-isnull(sum(z.pqty),0) as qty,b.PRate as Price,b.BasicValue,i.f1  as gstper,convert(decimal(18,2),(b.BasicValue /100 * i.f1),105) as gstval from   TransactionsP a inner join TransactionsPList b on a.UId=b.TransactionsPUId and a.doctypeid=10 LEFT join stransactionsplist z on b.Uid=z.Refuid inner join itemm c on b.ItemUId=c.uid  left join   ItemGroup j on c.itemgroup_Uid=j.UId left join Hsndet f on j.hsnid=f.uid left join generalm i   on f.sgid=i.uid inner join partym m on a.partyuid=m.uid where m.uid=" + txtpuid.Text + " group by m.uid,m.name,m.address1,Docno,  Docdate,c.hsnid,c.itemname,c.uid,i.f1,b.pqty,b.PRate,b.BasicValue,i.f1,  a.uid,b.uid  having b.pqty-isnull(sum(z.pqty),0)>0)tab";
                //Dtprem.Text = txtitemname.Text;
                Genclass.FSSQLSortStr = "docno";
            }
            Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
            SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
            DataTable tap = new DataTable();
            aptr.Fill(tap);

            if (tap.Rows.Count == 0)
            {
                MessageBox.Show("No records");
                return;
            }

            Frmlookup contc = new Frmlookup();
            DataGridView dt = (DataGridView)contc.Controls["HFGP"];
            dt.Refresh();
            dt.ColumnCount = tap.Columns.Count;
            dt.Columns[0].Visible = false;
            dt.Columns[1].Width = 400;

            if (Genclass.type == 1 || Genclass.type == 7)
            {
                dt.Columns[1].Width = 280;
                dt.Columns[2].Width = 280;
                //dt.Columns[2].Visible = false;
            }



            dt.DefaultCellStyle.Font = new Font("Calibri", 10);

            dt.ColumnHeadersDefaultCellStyle.Font = new Font("Calibri", 10, FontStyle.Bold);
            dt.AutoGenerateColumns = false;

            Genclass.i = 0;
            foreach (DataColumn column in tap.Columns)
            {
                dt.Columns[Genclass.i].Name = column.ColumnName;
                dt.Columns[Genclass.i].HeaderText = column.ColumnName;
                dt.Columns[Genclass.i].DataPropertyName = column.ColumnName;
                Genclass.i = Genclass.i + 1;
            }

            dt.DataSource = tap;
            contc.Show();




        }


        //private void HFIT_CellEnter(object sender, DataGridViewCellEventArgs e)
        //{
        //    int i = HFIT.SelectedCells[0].RowIndex;
        //    HFIT.Rows[i].Cells[4].Value = Convert.ToDouble(HFIT.Rows[i].Cells[2].Value) * Convert.ToDouble(HFIT.Rows[i].Cells[3].Value);
        //}

        //private void HFIT_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        //{
        //    if (HFIT.RowCount - 2 > 0)
        //    {
        //        Genclass.sum = 0;
        //        txtamt.Text = null;
        //        txtamttot.Text = null;
        //        if (HFIT.RowCount > 2)
        //        {
        //            for (int i = 0; i < HFIT.RowCount - 1; i++)
        //            {
        //                if (HFIT.Rows[i].Cells[4].Value.ToString() == "" || HFIT.Rows[i].Cells[4].Value.ToString() == null)
        //                {
        //                    return;
        //                }

        //                else
        //                {
        //                    //int sum1 = (int.TryParse(HFIT.Rows[i].Cells[2].Value.ToString(), out quantity) && int.TryParse(HFIT.Rows[i].Cells[4].Value.ToString(), out rate));
        //                    Genclass.sum = Genclass.sum + Convert.ToInt16(HFIT.Rows[i].Cells[4].Value.ToString());

        //                    txtamttot.Text = Genclass.sum.ToString();
        //                    txtamt.Text = txtamttot.Text;
        //                }
        //            }
        //        }
        //    }
        //}


        private void btnexit_Click(object sender, System.EventArgs e)
        {
            this.Dispose();
        }

        private void txtper_TextChanged(object sender, System.EventArgs e)
        {
            int val1;
            int val2;
            int val3;
            val1 = Convert.ToInt16(txttaxable.Text);
            val2 = Convert.ToInt16(txtper.Text);
            val3 = (val1 * val2) / 100;
            //txtdis.Text = Convert.ToString(val3);
            CalcNetAmt();

        }

        //private void textBox2_TextChanged(object sender, System.EventArgs e)
        //{

        //}
        private void CalcNetAmt()
        {

            int val4;
            int val5;
            int roff;
            int totamt;

            totamt = Convert.ToInt16(txttaxable.Text);

            //int dis = Convert.ToInt16(txtdis.Text);
            //if (txtper.Text == "")
            //{
            //    TxtNetAmt.Text = txttaxable.Text + txtexcise.Text + txttax.Text + txtpf.Text;
            //    val4 = Convert.ToInt16(TxtNetAmt.Text);
            //    val5 = val4 - Convert.ToInt16(txtdis.Text);
            //    // Genclass.strfin = totamt + Convert.ToInt16(txtpf.Text) - dis;

            //    TxtNetAmt.Text = Convert.ToString(val5);
            //}
            //else
            //{
            //    TxtNetAmt.Text = "0.00";


            //    TxtNetAmt.Text = txttaxable.Text + txtexcise.Text + txttax.Text + txtpf.Text;
            //    int cal5 = Convert.ToInt16(TxtNetAmt.Text);
            //    val4 = Convert.ToInt16(TxtNetAmt.Text) - Convert.ToInt16(txtdis.Text);
            //    //val5 = val4 - Convert.ToInt16(txtdis.Text);
            //    //txtpf.Text = "0.00";
            //    string cal1 = txtpf.Text;


            //TxtNetAmt.Text = Convert.ToString(val4);
            Genclass.strfin = TxtNetAmt.Text;
        }

        //TxtRoff.Text =val4 -Convert.ToString(TxtNetAmt.Text);

        //}

        //private void cboexcise_SelectedIndexChanged(object sender, System.EventArgs e)
        //{

        //    }
        //private void exciseduty()
        //{

        // if(cboexcise.SelectedValue==null)
        //    {
        //    return;

        //    }
        //    else
        //    {
        //    conn.Open();
        //        {
        //         Genclass.strsql = "Select * from GeneralM where TypeM_Uid=5 and Uid= "+ cboexcise.SelectedValue +" ";
        //        Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
        //        SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
        //        DataTable tap1 = new DataTable();
        //        aptr1.Fill(tap1);
        //        LblED1.Text = tap1.Rows[0]["F1"].ToString();
        //        LblCess.Text = tap1.Rows[0]["F2"].ToString();
        //        LblHECess.Text = tap1.Rows[0]["F3"].ToString();

        //        }
        //}



        //        }


        private void Taxduty()
        {

            //if (cbotax.SelectedValue == null)
            //{
            //    return;

            //}
            //else
            //{
            //    conn.Open();
            //    {
            //        Genclass.strsql = "Select * from GeneralM where  Uid= " + cbotax.SelectedValue + " ";
            //        Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
            //        SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
            //        DataTable tap1 = new DataTable();
            //        aptr1.Fill(tap1);
            //        lbltax.Text = tap1.Rows[0]["F1"].ToString();


            //    }
            //    conn.Close();
            //}



        }

        //private void cboexcise_Click(object sender, System.EventArgs e)
        //{
        //    if (cboexcise.SelectedValue != null)
        //    {
        //        exciseduty();
        //        txtted.Text = "";
        //        CalcNetAmt();
        //        int ed;
        //        int Cess;
        //        int HECess;
        //        int excise;
        //        int lbled;

        //        //val3 = (val1 * val2) / 100;
        //        //txtdis.Text = Convert.ToString(val3);
        //        lbled = Convert.ToInt16(LblED1.Text);
        //        ed = (Convert.ToInt16(Genclass.strfin) * lbled) / 100;
        //        txtted.Text = Convert.ToString(ed);
        //        Cess = ed * Convert.ToInt16(LblCess.Text) / 100;
        //        txtcess.Text = Convert.ToString(Cess);
        //        HECess = ed * Convert.ToInt16(LblHECess.Text) / 100;
        //        txthecess.Text = Convert.ToString(HECess);
        //        excise = ed + Cess + HECess;
        //        txtexcise.Text = Convert.ToString(excise);

        //    }   
        //    else
        //    {
        //        txtted.Text = "0.00";
        //        txtcess.Text = "0.00";
        //        txthecess.Text = "0.00";
        //        txtexcise.Text = "";
        //        LblED1.Text = "";
        //        LblCess.Text = "";
        //        LblHECess.Text = "";



        //    }

        //}

        private void btnsave_Click(object sender, System.EventArgs e)
        {
            if (txtname.Text == "")
            {
                MessageBox.Show("Enter the Party");
                txtname.Focus();
                return;
            }

            if (Genclass.ty != 1)
            {
                if (txtgen1.Text == "")
                {
                    MessageBox.Show("Enter the Invoiceno");
                    txtname.Focus();
                    return;
                }
            }

            conn.Close();
            conn.Open();





            Genclass.Dtype = 8;

            if (txttdis.Text == "")
            {
                txttdis.Text = "0";
                txttdisc.Text = "0";
            }

            if (mode == 2)
            {
                qur.CommandText = "delete from purchasecharges where puid=" + uid + "";
                qur.ExecuteNonQuery();
                qur.CommandText = "delete from purchased where transactionspuid=" + uid + "";
                qur.ExecuteNonQuery();
                //qur.CommandText = "delete from billpomap where headid='" + uid +     "'";
                //qur.ExecuteNonQuery();

                //qur.CommandText = "delete from StransactionspListStkSerial where REFNO='" + txtgrn.Text + "'";
                //qur.ExecuteNonQuery();
            }


            for (int i = 0; i < HFIT.RowCount - 1; i++)
            {
                if (mode == 1)
                {

                    //qur.CommandText = "Exec Sp_SalesINvoice " + Genclass.Dtype + ",'" + txtgrn.Text + "','" + DTPDOCDT.Value + "','" + txtdcno.Text + "','" + Dtpdt.Value + "'," + txtpuid.Text + ",'" + txtrem.Text + "',1," + GeneralParameters.UserdId + "," + HFIT.Rows[i].Cells[5].Value + " ," + HFIT.Rows[i].Cells[3].Value + " ," + HFIT.Rows[i].Cells[2].Value + "   ,   " + HFIT.Rows[i].Cells[4].Value + " , 0," + HFIT.Rows[i].Cells[7].Value + "   ,    " + HFIT.Rows[i].Cells[8].Value + "    ,0, 0 ,0, 0,  " + HFIT.Rows[i].Cells[10].Value + ", " + HFIT.Rows[i].Cells[11].Value + ",0," + i + "," + mode + "," + Txttot.Text + "," + TxtNetAmt.Text + "," + Genclass.Yearid + "," + HFIT.Rows[i].Cells[9].Value + "," + TxtRoff.Text + ",'" + Dtppre.Value + "','" + Dtprem.Value + "','" + txttrans.Text + "','" + txtveh.Text + "'," + txtpluid.Text + "";
                        if (i == 0)
                        {

                            SqlParameter[] para ={


                        new SqlParameter("@UID","0"),
                        new SqlParameter("@DOCTYPEID", Genclass.Dtype),
                        new SqlParameter("@DOCNO",txtgrn.Text),
                        new SqlParameter("@DOCDATE", Convert.ToDateTime(DTPDOCDT.Text).ToString("yyyy-MM-dd")),
                        new SqlParameter("@DCNO", txtgen1.Text),
                        new SqlParameter("@DCDATE", Convert.ToDateTime(dcdate.Text).ToString("yyyy-MM-dd")),
                        new SqlParameter("@Partyuid",txtpuid.Text),
                        new SqlParameter("@Remarks", txtrem.Text),
                        new SqlParameter("@ACTIVE", "1"),
                        new SqlParameter("@companyid", GeneralParameters.UserdId),
                        new SqlParameter("@Netvalue", TxtNetAmt.Text),
                        new SqlParameter("@YEARID",Genclass.Yearid),
                        new SqlParameter("@Roff", TxtRoff.Text),
                        new SqlParameter("@dtpre", Convert.ToDateTime(Dtppre.Text).ToString("yyyy-MM-dd")),
                        new SqlParameter("@dtrem", Convert.ToDateTime(Dtprem.Text).ToString("yyyy-MM-dd")),
                        new SqlParameter("@Transp", txttrans.Text),
                        new SqlParameter("@Vehno", txtlisid.Text),
                        new SqlParameter("@placeuid", txtpluid.Text),
                        new SqlParameter("@orderdt",'0'),
                        new SqlParameter("@TYPE",Genclass.title ),
                        new SqlParameter("@Returnid",SqlDbType.Int),
                          new SqlParameter("@approved","0"),
                        new SqlParameter("@appdt", Convert.ToDateTime(DTPDOCDT.Text).ToString("yyyy-MM-dd"))




                };
                        para[20].Direction = ParameterDirection.Output;
                        int uid = (db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_PurchaseM", para, conn, 20));
                        txtgrnid.Text = Convert.ToString(uid);
                     


                    }
               

                    string quyt = "select isnull(f1,0) as f1 from   generalm where f1='" + HFIT.Rows[i].Cells[10].Value + "'";

                    Genclass.cmd = new SqlCommand(quyt, conn);
                    SqlDataAdapter aptrt = new SqlDataAdapter(Genclass.cmd);
                    DataTable tapt = new DataTable();
                    aptrt.Fill(tapt);

                    if (tapt.Rows.Count > 0)
                    {
                        if (txtpluid.Text == "196")
                        {
                            dd1 = Convert.ToDouble(tapt.Rows[0]["f1"].ToString()) / 2;

                            txttcgstp.Text = dd1.ToString("0.00");
                            if (txttdis.Text == "0")
                            {
                                dd2 = ((Convert.ToDouble(HFIT.Rows[i].Cells[4].Value) / 100) * Convert.ToDouble(tapt.Rows[0]["f1"].ToString())) / 2;
                                dd8 = 0;
                            }
                            else
                            {
                                dd8 = ((Convert.ToDouble(HFIT.Rows[i].Cells[4].Value) / 100) * Convert.ToDouble(txttdis.Text));

                                dd2 = (((Convert.ToDouble(HFIT.Rows[i].Cells[4].Value) - dd8) / 100) * Convert.ToDouble(tapt.Rows[0]["f1"].ToString()) / 2);
                            }
                            txttcgval.Text = dd2.ToString("0.00");
                            double dd9 = Convert.ToDouble(HFIT.Rows[i].Cells[4].Value) - dd8;
                            txtbval.Text = dd9.ToString("0.00");


                            SqlParameter[] para1 ={


                       
                        new SqlParameter("@DOCTYPEID", Genclass.Dtype),
                        new SqlParameter("@TRANSACTIONSPUID",txtgrnid.Text),
                        new SqlParameter("@ITEMUID",HFIT.Rows[i].Cells[6].Value),
                        new SqlParameter("@PQTY", HFIT.Rows[i].Cells[3].Value),
                        new SqlParameter("@PRATE", HFIT.Rows[i].Cells[2].Value),
                        new SqlParameter("@BASICVALUE",HFIT.Rows[i].Cells[4].Value),
                        new SqlParameter("@TAXABLEVAL", txtbval.Text),
                        new SqlParameter("@REFUID", HFIT.Rows[i].Cells[10].Value),
                        new SqlParameter("@DISP", txttdis.Text),
                        new SqlParameter("@DISVAL",dd8),
                        new SqlParameter("@CGSTID",txttcgstp.Text),
                        new SqlParameter("@CGSTVAL",txttcgval.Text),
                        new SqlParameter("@SGSTID", txttcgstp.Text),
                        new SqlParameter("@SGSTVAL",txttcgval.Text),
                        new SqlParameter("@IGSTID", '0'),
                        new SqlParameter("@IGSTVAL",'0'),
                        new SqlParameter("@MODE", mode),
                        new SqlParameter("@TOTVALUE",TxtNetAmt.Text ),
                        new SqlParameter("@ADDNOTES",HFIT.Rows[i].Cells[9].Value),
                             new SqlParameter("@itemname",HFIT.Rows[i].Cells[0].Value),
                                new SqlParameter("@bags",HFIT.Rows[i].Cells[16].Value),
                                 new SqlParameter("@style",HFIT.Rows[i].Cells[17].Value),
                      
                               new SqlParameter("@socno",HFIT.Rows[i].Cells[18].Value),
                };

                            db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_Purchased", para1, conn);
                           
                            txtbval.Text = "";
                        }
                        else
                        {
                            dd1 = Convert.ToDouble(tapt.Rows[0]["f1"].ToString());

                            txttcgstp.Text = dd1.ToString("0.00");

                            dd8 = ((Convert.ToDouble(HFIT.Rows[i].Cells[4].Value) / 100) * Convert.ToDouble(txttdis.Text));

                            dd2 = (((Convert.ToDouble(HFIT.Rows[i].Cells[4].Value) - dd8) / 100) * Convert.ToDouble(tapt.Rows[0]["f1"].ToString()));
                            txttcgval.Text = dd2.ToString("0.00");
                            double dd9 = Convert.ToDouble(HFIT.Rows[i].Cells[4].Value) - dd8;
                            txtbval.Text = dd9.ToString("0.00");

                            SqlParameter[] para ={


                            new SqlParameter("@DOCTYPEID", Genclass.Dtype),
                        new SqlParameter("@TRANSACTIONSPUID", txtgrnid.Text),
                        new SqlParameter("@ITEMUID", HFIT.Rows[i].Cells[6].Value),
                        new SqlParameter("@PQTY", HFIT.Rows[i].Cells[3].Value),
                        new SqlParameter("@PRATE", HFIT.Rows[i].Cells[2].Value),
                        new SqlParameter("@BASICVALUE", HFIT.Rows[i].Cells[4].Value),
                        new SqlParameter("@TAXABLEVAL", txtbval.Text),
                        new SqlParameter("@REFUID", HFIT.Rows[i].Cells[10].Value),
                        new SqlParameter("@DISP", txttdis.Text),
                        new SqlParameter("@DISVAL", dd8),
                        new SqlParameter("@CGSTID",'0'),
                        new SqlParameter("@CGSTVAL", '0'),
                        new SqlParameter("@SGSTID", '0'),
                        new SqlParameter("@SGSTVAL",'0'),
                        new SqlParameter("@IGSTID", txttcgstp.Text),
                        new SqlParameter("@IGSTVAL", txttcgval.Text),
                        new SqlParameter("@MODE", mode),
                        new SqlParameter("@TOTVALUE", TxtNetAmt.Text),
                        new SqlParameter("@ADDNOTES", HFIT.Rows[i].Cells[9].Value),
                             new SqlParameter("@itemname",HFIT.Rows[i].Cells[0].Value),
                              new SqlParameter("@bags",HFIT.Rows[i].Cells[16].Value),
                                 new SqlParameter("@style",HFIT.Rows[i].Cells[17].Value),
                             new SqlParameter("@socno",HFIT.Rows[i].Cells[18].Value),

                };

                        db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_purchased", para, conn);


                        txtbval.Text = "";
                        }

                    }
                  
                }


                if (mode == 2)
                {
                    SqlParameter[] para ={


                 
                        new SqlParameter("@DOCTYPEID", Genclass.Dtype),
                        new SqlParameter("@DOCNO",txtgrn.Text),
                        new SqlParameter("@DOCDATE", Convert.ToDateTime(DTPDOCDT.Text).ToString("yyyy-MM-dd")),
                        new SqlParameter("@DCNO", txtgen1.Text),
                        new SqlParameter("@DCDATE", Convert.ToDateTime(dcdate.Text).ToString("yyyy-MM-dd")),
                        new SqlParameter("@Partyuid",txtpuid.Text),
                        new SqlParameter("@Remarks", txtrem.Text),
                        new SqlParameter("@ACTIVE", "1"),
                        new SqlParameter("@companyid", GeneralParameters.UserdId),
                        new SqlParameter("@Netvalue", TxtNetAmt.Text),
                        new SqlParameter("@YEARID",Genclass.Yearid),
                        new SqlParameter("@Roff", TxtRoff.Text),
                        new SqlParameter("@dtpre", Convert.ToDateTime(Dtppre.Text).ToString("yyyy-MM-dd")),
                        new SqlParameter("@dtrem", Convert.ToDateTime(Dtprem.Text).ToString("yyyy-MM-dd")),
                        new SqlParameter("@Transp", txttrans.Text),
                        new SqlParameter("@Vehno", txtlisid.Text),
                        new SqlParameter("@placeuid", txtpluid.Text),
                        new SqlParameter("@orderdt",'0'),
                        new SqlParameter("@TYPE",Genclass.title ),
                        new SqlParameter("@UID",txtgrnid.Text),
                                       new SqlParameter("@approved","0"),
                        new SqlParameter("@appdt", Convert.ToDateTime(DTPDOCDT.Text).ToString("yyyy-MM-dd"))




                };

                    db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_PurchaseUpdate", para, conn);

                    string quyq = "select isnull(f1,0) as f1 from   generalm where f1='" + HFIT.Rows[i].Cells[7].Value + "'";
                    Genclass.cmd = new SqlCommand(quyq, conn);
                    SqlDataAdapter aptrq = new SqlDataAdapter(Genclass.cmd);
                    DataTable tapq = new DataTable();
                    aptrq.Fill(tapq);

                    if (tapq.Rows.Count > 0)
                    {
                        if (txtpluid.Text == "196")
                        {

                            dd1 = Convert.ToDouble(tapq.Rows[0]["f1"].ToString()) / 2;

                            txttcgstp.Text = dd1.ToString("0.00");
                            if (txttdis.Text == "0")
                            {
                                dd2 = ((Convert.ToDouble(HFIT.Rows[i].Cells[4].Value) / 100) * Convert.ToDouble(tapq.Rows[0]["f1"].ToString())) / 2;
                                dd8 = 0;
                            }
                            else
                            {
                                dd8 = ((Convert.ToDouble(HFIT.Rows[i].Cells[4].Value) / 100) * Convert.ToDouble(txttdis.Text));

                                dd2 = (((Convert.ToDouble(HFIT.Rows[i].Cells[4].Value) - dd8) / 100) * Convert.ToDouble(tapq.Rows[0]["f1"].ToString()) / 2);
                            }

                            txttcgval.Text = dd2.ToString("0.00");
                            double dd9 = Convert.ToDouble(HFIT.Rows[i].Cells[4].Value) - dd8;
                            txtbval.Text = dd9.ToString("0.00");

                            if(HFIT.Rows[i].Cells[9].Value==null)
                            {

                                HFIT.Rows[i].Cells[9].Value = "0";
                            }

                            SqlParameter[] para1 ={



                        new SqlParameter("@DOCTYPEID", Genclass.Dtype),
                        new SqlParameter("@TRANSACTIONSPUID",txtgrnid.Text),
                        new SqlParameter("@ITEMUID",HFIT.Rows[i].Cells[6].Value),
                        new SqlParameter("@PQTY", HFIT.Rows[i].Cells[3].Value),
                        new SqlParameter("@PRATE", HFIT.Rows[i].Cells[2].Value),
                        new SqlParameter("@BASICVALUE",HFIT.Rows[i].Cells[4].Value),
                        new SqlParameter("@TAXABLEVAL", txtbval.Text),
                        new SqlParameter("@REFUID", HFIT.Rows[i].Cells[10].Value),
                        new SqlParameter("@DISP", txttdis.Text),
                        new SqlParameter("@DISVAL",dd8),
                        new SqlParameter("@CGSTID",txttcgstp.Text),
                        new SqlParameter("@CGSTVAL",txttcgval.Text),
                        new SqlParameter("@SGSTID", txttcgstp.Text),
                        new SqlParameter("@SGSTVAL",txttcgval.Text),
                        new SqlParameter("@IGSTID", '0'),
                        new SqlParameter("@IGSTVAL",'0'),
                        new SqlParameter("@MODE", mode),
                        new SqlParameter("@TOTVALUE",TxtNetAmt.Text ),
                        new SqlParameter("@ADDNOTES",Convert.ToString(HFIT.Rows[i].Cells[9].Value)),
                             new SqlParameter("@itemname",HFIT.Rows[i].Cells[0].Value),
                         new SqlParameter("@bags",HFIT.Rows[i].Cells[16].Value),
                                 new SqlParameter("@style",HFIT.Rows[i].Cells[17].Value),
                             new SqlParameter("@socno",HFIT.Rows[i].Cells[18].Value),


                };

                            db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_Purchased", para1, conn);
                            txtbval.Text = "";

                        }
                        else
                        {
                            dd1 = Convert.ToDouble(tapq.Rows[0]["f1"].ToString());

                            txttcgstp.Text = dd1.ToString("0.00");

                            dd8 = ((Convert.ToDouble(HFIT.Rows[i].Cells[4].Value) / 100) * Convert.ToDouble(txttdis.Text));

                            dd2 = (((Convert.ToDouble(HFIT.Rows[i].Cells[4].Value) - dd8) / 100) * Convert.ToDouble(tapq.Rows[0]["f1"].ToString()));
                            txttcgval.Text = dd2.ToString("0.00");
                            double dd9 = Convert.ToDouble(HFIT.Rows[i].Cells[4].Value) - dd8;
                            txtbval.Text = dd9.ToString("0.00");
                            SqlParameter[] para1 ={


                            new SqlParameter("@DOCTYPEID", Genclass.Dtype),
                        new SqlParameter("@TRANSACTIONSPUID", txtgrnid.Text),
                        new SqlParameter("@ITEMUID", HFIT.Rows[i].Cells[6].Value),
                        new SqlParameter("@PQTY", HFIT.Rows[i].Cells[3].Value),
                        new SqlParameter("@PRATE", HFIT.Rows[i].Cells[2].Value),
                        new SqlParameter("@BASICVALUE", HFIT.Rows[i].Cells[4].Value),
                        new SqlParameter("@TAXABLEVAL", txtbval.Text),
                        new SqlParameter("@REFUID", HFIT.Rows[i].Cells[10].Value),
                        new SqlParameter("@DISP", txttdis.Text),
                        new SqlParameter("@DISVAL", dd8),
                        new SqlParameter("@CGSTID",'0'),
                        new SqlParameter("@CGSTVAL", '0'),
                        new SqlParameter("@SGSTID", '0'),
                        new SqlParameter("@SGSTVAL",'0'),
                        new SqlParameter("@IGSTID", txttcgstp.Text),
                        new SqlParameter("@IGSTVAL", txttcgval.Text),
                        new SqlParameter("@MODE", mode),
                        new SqlParameter("@TOTVALUE", TxtNetAmt.Text),
                        new SqlParameter("@ADDNOTES", HFIT.Rows[i].Cells[9].Value),
                             new SqlParameter("@itemname",HFIT.Rows[i].Cells[0].Value),
                  new SqlParameter("@bags",HFIT.Rows[i].Cells[16].Value),
                                 new SqlParameter("@style",HFIT.Rows[i].Cells[17].Value),
                             new SqlParameter("@socno",HFIT.Rows[i].Cells[18].Value),


                };

                            db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_Purchased", para1, conn);
                            txtbval.Text = "";
                        }

                    }


                }
            }
            
            for (int k = 0; k < HFGPT.RowCount - 1; k++)
            {
                conn.Close();
                conn.Open();
                SqlParameter[] para1 ={
                    new SqlParameter("@grnno", HFGPT.Rows[k].Cells[1].Value),
                       new SqlParameter("@headid", txtgrnid.Text),
                    
                    new SqlParameter("@dcno",HFGPT.Rows[k].Cells[2].Value),
                    new SqlParameter("@dcdate", HFGPT.Rows[k].Cells[3].Value),
                    new SqlParameter("@pono", HFGPT.Rows[k].Cells[4].Value),
                 
                };

                db.ExecuteNonQuery(CommandType.StoredProcedure, "sp_billpomap", para1, conn);
            }


           
            if (txttdis.Text == "" || txttdis.Text == null)
            {
                txttdis.Text = "0";
                txttdisc.Text = "0";
            }




            if (txtcharges.Text != "")
            {
              

                SqlParameter[] para1 ={


                    new SqlParameter("@puid",  txtgrnid.Text),
                    new SqlParameter("@DOCTYPEID",Genclass.Dtype),
                    new SqlParameter("@CHARGESUID", 13),

                    new SqlParameter("@CHARGEAMOUNT",txtcharges.Text),
                    new SqlParameter("@TOTALCHARGES", txtcharges.Text),


                     new SqlParameter("@ADDITIONAL", '0'),
                      new SqlParameter("@YEARID",  Genclass.Yearid),


                };

                db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_purchasecharges", para1, conn);
            }

            if (mode == 1)
            {
                conn.Close();
                conn.Open();
                qur.CommandText = "update doctypem set lastno= lastno + 1 where doctypeid=" + Genclass.Dtype + "  and finyear='19-20'";
                qur.ExecuteNonQuery();
            }


          




            MessageBox.Show("Record has been saved", "Save", MessageBoxButtons.OK);


            conn.Close();
            LoadGetJobCard(1);
            TotalAmount();
            Editpan.Visible = false;
            addipan.Visible = false;
            Taxpan.Visible = false;
            panadd.Visible = true;
            Genpan.Visible = true;
            Genclass.sum5 = 0;
            j = -1;

        }



        private void cbotax_Click(object sender, System.EventArgs e)
        {
            //if (cbotax.SelectedValue != null)
            //{
            //    int ed;
            //    int Cess;
            //    int HECess;
            //    int excise;
            //    int lbled;
            //    int tax;
            //    int vat;

            //    lbled = Convert.ToInt16(LblED1.Text);
            //    ed = (Convert.ToInt16(Genclass.strfin) * lbled) / 100;
            //    txtted.Text = Convert.ToString(ed);
            //    Cess = ed * Convert.ToInt16(LblCess.Text) / 100;
            //    txtcess.Text = Convert.ToString(Cess);
            //    HECess = ed * Convert.ToInt16(LblHECess.Text) / 100;
            //    txthecess.Text = Convert.ToString(HECess);
            //    excise = ed + Cess + HECess;
            //    txtexcise.Text = Convert.ToString(excise);
            //    CalcNetAmt();
            //    Taxduty();
            //    tax = Convert.ToInt16(lbltax.Text);
            //    vat = Convert.ToInt16(TxtNetAmt.Text) * tax / 100;
            //    txttax.Text = Convert.ToString(vat);
            CalcNetAmt();
            //}
        }


        private void txtdcno_TextChanged(object sender, EventArgs e)
        {


            //if (Genclass.Dtype == 40)
            //{

            //    if (txtdcno.Text == "")
            //    {
            //        return;

            //    }
            //    else
            //    {
            //        Genclass.strsql = "select Itemname,UoM,itemuid,refid,qty,PRICE,convert(decimal(18,2),BasicValue,105) AS BasicValue,disper,convert(decimal(18,2),Disvalue,105) AS Disvalue,convert(decimal(18,2),Taxablevalue,105) AS Taxablevalue,convert(decimal(18,2),Cgstval,105) AS EDVAL,convert(decimal(18,2),sgstval,105) AS VATVAL,convert(decimal(18,2),igstval,105) AS igstval,CG as ED,sg as VAT,ig, convert(decimal(18,2),(BasicValue-(isnull(Disvalue,0))+ (isnull(Cgstval,0)+isnull(sgstval,0))),105) as total from (select distinct   c.itemname,d.generalname as uom,b.itemuid,b.uid as refid,b.pqty-isnull(sum(e.pqty),0) as qty,f.Price ,(b.pqty-isnull(sum(e.pqty),0)) * f.Price as BasicValue,disper,((b.pqty-isnull(sum(e.pqty),0)) * f.Price/100)* disper as Disvalue,((b.pqty-isnull(sum(e.pqty),0)) * f.Price)-(((b.pqty-isnull(sum(e.pqty),0)) * f.Price/100)* disper) as Taxablevalue,g.f1 as CG,(((b.pqty-isnull(sum(e.pqty),0)) * f.Price)-(((b.pqty-isnull(sum(e.pqty),0)) * f.Price/100)* disper))/100 * g.f1 as Cgstval,h.f1 as SG,(((b.pqty-isnull(sum(e.pqty),0)) * f.Price)-(((b.pqty-isnull(sum(e.pqty),0)) * f.Price/100)* disper))/100 * h.f1 as sgstval,i.f1 as ig,(((b.pqty-isnull(sum(e.pqty),0)) * f.Price)-(((b.pqty-isnull(sum(e.pqty),0)) * f.Price/100)* disper))/100 * i.f1 as igstval from transactionsp a inner join transactionsplist b on  a.uid=b.transactionspuid and a.doctypeid=30 and a.companyid=1 inner join itemm c on b.itemuid=c.uid left join generalm d on c.uom_uid=d.uid  left join transactionsplist e on  b.uid=e.refuid and e.doctypeid=40 inner join pur_price_list f on c.uid=f.itemuid and c.partyuid=f.suppuid inner join generalm g on f.cgstid=g.uid inner join generalm h on f.sgstid=h.uid inner join generalm i on f.igstid=i.uid where a.docno='0001/17-18' group by c.itemname,d.generalname,b.itemuid,b.uid,b.pqty,f.price,disper,g.f1,h.f1,i.f1 having b.pqty-isnull(sum(e.pqty),0) >0) tab";

            //        Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
            //        SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
            //        DataTable tap1 = new DataTable();
            //        aptr1.Fill(tap1);
            //        //txtname.Text = tap1.Rows[0]["Name"].ToString();
            //        //txtpuid.Text = tap1.Rows[0]["Partyuid"].ToString();
            //        for (int i = 0; i < tap1.Rows.Count; i++)
            //        {
            //            var index = HFIT.Rows.Add();
            //            HFIT.Rows[index].Cells[0].Value = tap1.Rows[i]["itemname"].ToString();
            //            HFIT.Rows[index].Cells[1].Value = tap1.Rows[i]["UOM"].ToString();
            //            HFIT.Rows[index].Cells[2].Value = tap1.Rows[i]["Price"].ToString();
            //            HFIT.Rows[index].Cells[3].Value = tap1.Rows[i]["qty"].ToString();
            //            HFIT.Rows[index].Cells[4].Value = tap1.Rows[i]["BasicValue"].ToString();
            //            HFIT.Rows[index].Cells[5].Value = tap1.Rows[i]["itemuid"].ToString();
            //            HFIT.Rows[index].Cells[6].Value = tap1.Rows[i]["refid"].ToString();
            //            HFIT.Rows[index].Cells[7].Value = tap1.Rows[i]["disper"].ToString();
            //            HFIT.Rows[index].Cells[8].Value = tap1.Rows[i]["Disvalue"].ToString();
            //            HFIT.Rows[index].Cells[9].Value = tap1.Rows[i]["Taxablevalue"].ToString();
            //            HFIT.Rows[index].Cells[10].Value = tap1.Rows[i]["ED"].ToString();
            //            HFIT.Rows[index].Cells[11].Value = tap1.Rows[i]["EDVAL"].ToString();
            //            HFIT.Rows[index].Cells[12].Value = tap1.Rows[i]["VAT"].ToString();
            //            HFIT.Rows[index].Cells[13].Value = tap1.Rows[i]["VATVAL"].ToString();
            //            HFIT.Rows[index].Cells[14].Value = tap1.Rows[i]["Total"].ToString();
            //            HFIT.Rows[index].Cells[15].Value = index;

            //            Genclass.sum1 = Genclass.sum1 + Convert.ToDouble(HFIT.Rows[i].Cells[14].Value);
            //            Txttot.Text = Genclass.sum1.ToString();




            //            //Titlep();



            //        }

            //    }


            //}
            //else
            //{
            //    txtitemname.Focus();

            //}
        }



        private void button4_Click(object sender, EventArgs e)
        {
            Editpan.Visible = false;
            Genpan.Visible = true;
        }


        private void button6_Click(object sender, EventArgs e)
        {

        }

        private void button8_Click(object sender, EventArgs e)
        {
            Editpan.Visible = false;
            Taxpan.Visible = true;
            txtexcise.Text = Txttot.Text;
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button9_Click(object sender, EventArgs e)
        {

            addipan.Visible = true;


        }

        private void button10_Click(object sender, EventArgs e)
        {

        }

        private void txtexcise_TextChanged(object sender, EventArgs e)
        {

        }

        private void Taxpan_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button10_Click_1(object sender, EventArgs e)
        {

        }




        private void Titleterm()
        {
            //HFGT.ColumnCount = 3;
            //HFGT.Columns[0].Name = "Terms";
            //HFGT.Columns[1].Name = "Term Description";
            //HFGT.Columns[2].Name = "uid";

            //HFGT.Columns[0].Width = 300;
            //HFGT.Columns[1].Width = 310;
            //HFGT.Columns[2].Visible = false;



        }

        private void txtterms_KeyDown(object sender, KeyEventArgs e)
        {
            //if (e.KeyCode == Keys.Enter)
            //{
            //    Genclass.fieldone = "";
            //    Genclass.fieldtwo = "";
            //    Genclass.fieldthree = "";
            //    Genclass.fieldFour = "";
            //    Genclass.fieldFive = "";
            //    Genclass.type = 4;
            //    loadput();
            //}
        }

        private void txtterms_TextChanged(object sender, EventArgs e)
        {

        }

        //private void button11_Click(object sender, EventArgs e)
        //{

        //    HFGT.AllowUserToAddRows = true;
        //    HFGT.AutoGenerateColumns = true;
        //    var index = HFGT.Rows.Add();
        //    HFGT.Rows[index].Cells[0].Value = txtterms.Text;
        //    HFGT.Rows[index].Cells[1].Value = txtremde.Text;
        //    HFGT.Rows[index].Cells[2].Value = txttermid.Text;

        //}

        private void button12_Click(object sender, EventArgs e)
        {

        }

        private void button10_Click_2(object sender, EventArgs e)
        {
            addipan.Visible = false;

        }

        private void txtaddcharge_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtaddcharge_KeyDown(object sender, KeyEventArgs e)
        {
            //if (e.KeyCode == Keys.Enter)
            //{
            //    Genclass.fieldone = "";
            //    Genclass.fieldtwo = "";
            //    Genclass.fieldthree = "";
            //    Genclass.fieldFour = "";
            //    Genclass.fieldFive = "";
            //    Genclass.type = 3;
            //    loadput();
            //}
        }

        private void txttotaddd_TextChanged(object sender, EventArgs e)
        {
            if (txttotaddd.Text == "0" || txttotaddd.Text == "")
            {
                TxtNetAmt.Text = txtexcise.Text;
            }
            else
            {
                TxtNetAmt.Text = "0";
                double val2 = Convert.ToDouble(txtexcise.Text) + Convert.ToDouble(txttotaddd.Text);
                TxtNetAmt.Text = val2.ToString();

            }
        }

        private void btnedit_Click(object sender, EventArgs e)
        {
            HFIT.AutoGenerateColumns = false;
            HFIT.Refresh();
            HFIT.DataSource = null;
            HFIT.Rows.Clear();

            HFGT.AutoGenerateColumns = false;
            HFGT.Refresh();
            HFGT.DataSource = null;
            HFGT.Rows.Clear();
            mode = 2;

            Genpan.Visible = false;
            Editpan.Visible = true;
            int i = HFGP.SelectedCells[0].RowIndex;
            txtgrnid.Text = HFGP.Rows[i].Cells[0].Value.ToString();
            txtgrn.Text = HFGP.Rows[i].Cells[1].Value.ToString();
            DTPDOCDT.Text = HFGP.Rows[i].Cells[2].Value.ToString();
            txtdcno.Text = HFGP.Rows[i].Cells[3].Value.ToString();
            Dtpdt.Text = HFGP.Rows[i].Cells[4].Value.ToString();
            txtname.Text = HFGP.Rows[i].Cells[5].Value.ToString();
            txtpuid.Text = HFGP.Rows[i].Cells[10].Value.ToString();
            //txtrem.Text = HFGP.Rows[i].Cells[8].Value.ToString();
            conn.Open();
            {
                Genclass.strsql = " select b.uid,c.ItemName,d.GeneralName as uom,PRate,pqty,BasicValue,ItemUId,b.refuid,disp,disval,Taxableval,Cgstid,Cgstval,Sgstid,Sgstval,totvalue from stransactionsp a inner join Stransactionsplist b on a.uid=b.transactionspuid  left join itemm c  on b.itemuid=c.uid left join generalm d on c.UOM_UId=d.Uid  where b.transactionspuid=" + txtgrnid.Text + " ";

                Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
                DataTable tap1 = new DataTable();
                aptr1.Fill(tap1);
                //txtname.Text = tap1.Rows[0]["Name"].ToString();
                //txtpuid.Text = tap1.Rows[0]["Partyuid"].ToString();
                Genclass.sum1 = 0;

                for (int k = 0; k < tap1.Rows.Count; k++)
                {
                    var index = HFIT.Rows.Add();
                    HFIT.Rows[index].Cells[0].Value = tap1.Rows[k]["itemname"].ToString();
                    HFIT.Rows[index].Cells[1].Value = tap1.Rows[k]["UOM"].ToString();
                    HFIT.Rows[index].Cells[2].Value = tap1.Rows[k]["PRate"].ToString();
                    HFIT.Rows[index].Cells[3].Value = tap1.Rows[k]["pqty"].ToString();
                    HFIT.Rows[index].Cells[4].Value = tap1.Rows[k]["BasicValue"].ToString();
                    HFIT.Rows[index].Cells[5].Value = tap1.Rows[k]["itemuid"].ToString();
                    HFIT.Rows[index].Cells[6].Value = tap1.Rows[k]["refuid"].ToString();
                    HFIT.Rows[index].Cells[7].Value = tap1.Rows[k]["disp"].ToString();
                    HFIT.Rows[index].Cells[8].Value = tap1.Rows[k]["disval"].ToString();
                    HFIT.Rows[index].Cells[9].Value = tap1.Rows[k]["Taxableval"].ToString();
                    HFIT.Rows[index].Cells[10].Value = tap1.Rows[k]["Cgstid"].ToString();
                    HFIT.Rows[index].Cells[11].Value = tap1.Rows[k]["Cgstval"].ToString();
                    HFIT.Rows[index].Cells[12].Value = tap1.Rows[k]["Sgstid"].ToString();
                    HFIT.Rows[index].Cells[13].Value = tap1.Rows[k]["Sgstval"].ToString();
                    HFIT.Rows[index].Cells[14].Value = tap1.Rows[k]["totvalue"].ToString();
                    HFIT.Rows[index].Cells[15].Value = tap1.Rows[i]["uid"].ToString();

                    Genclass.sum1 = Genclass.sum1 + Convert.ToDouble(HFIT.Rows[k].Cells[14].Value);
                    Txttot.Text = Genclass.sum1.ToString();
                    //txtexcise.Text = Genclass.sum1.ToString();



                }
                HFIT.Columns[0].Width = 300;
                HFIT.Columns[1].Visible = false;
                HFIT.Columns[2].Width = 60;
                HFIT.Columns[3].Width = 60;
                HFIT.Columns[4].Width = 100;

                HFIT.Columns[5].Visible = false;
                HFIT.Columns[6].Visible = false;
                HFIT.Columns[9].Visible = false;

                HFIT.Columns[7].Width = 50;
                HFIT.Columns[8].Width = 100;

                HFIT.Columns[10].Width = 50;
                HFIT.Columns[11].Width = 50;
                HFIT.Columns[12].Width = 50;
                HFIT.Columns[13].Width = 50;
                HFIT.Columns[14].Width = 100;
                HFIT.Columns[15].Visible = false;
                conn.Close();
            }
            conn.Open();
            {
                Genclass.strsql = "select * from Stransactionspcharges a left join generalm b on a.chargesuid=b.uid  where a.stransactionspuid=" + txtgrnid.Text + " ";

                Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                SqlDataAdapter aptr2 = new SqlDataAdapter(Genclass.cmd);
                DataTable tap2 = new DataTable();
                aptr2.Fill(tap2);
                Genclass.sum1 = 0;
                for (int j = 0; j < tap2.Rows.Count; j++)
                {
                    //var index = HFGA.Rows.Add();
                    //HFGA.Rows[index].Cells[0].Value = tap2.Rows[j]["generalname"].ToString();
                    //HFGA.Rows[index].Cells[1].Value = tap2.Rows[j]["chargeamount"].ToString();
                    //HFGA.Rows[index].Cells[2].Value = tap2.Rows[j]["chargesuid"].ToString();


                    //Genclass.sum1 = Genclass.sum1 + Convert.ToDouble(HFGA.Rows[j].Cells[1].Value);
                    txttotaddd.Text = Genclass.sum1.ToString();
                }
                conn.Close();
            }
            conn.Open();
            {
                Genclass.strsql = "select * from STransactionsPTerms a left join generalm b on a.termsuid=b.uid  where a.stransactionspuid=" + txtgrnid.Text + " ";

                Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                SqlDataAdapter aptr3 = new SqlDataAdapter(Genclass.cmd);
                DataTable tap3 = new DataTable();
                aptr3.Fill(tap3);

                for (int g = 0; g < tap3.Rows.Count; g++)
                {
                    var index = HFGT.Rows.Add();
                    HFGT.Rows[index].Cells[0].Value = tap3.Rows[g]["generalname"].ToString();
                    HFGT.Rows[index].Cells[1].Value = tap3.Rows[g]["chargesamount"].ToString();
                    HFGT.Rows[index].Cells[2].Value = tap3.Rows[g]["chargesuid"].ToString();


                }
                conn.Close();

                double val2 = Convert.ToDouble(txtexcise.Text) + Convert.ToDouble(txttotaddd.Text);
                TxtNetAmt.Text = val2.ToString();
            }

        }



        private void txtscr1_TextChanged(object sender, EventArgs e)
        {
            bs.Filter = string.Format("Docno Like '%{0}%'  or  Docdate Like '%{1}%' or  InvNo Like '%{1}%' or  InvDate Like '%{1}%' or  Name Like '%{1}%'  ", txtscr1.Text, txtscr1.Text, txtscr1.Text, txtscr1.Text, txtscr1.Text);
        }

        private void Txtscr2_TextChanged(object sender, EventArgs e)
        {
        
        }

        private void Txtscr3_TextChanged(object sender, EventArgs e)
        {
           
        }

        private void txtscr4_TextChanged(object sender, EventArgs e)
        {
           
        }

        private void txtscr5_TextChanged(object sender, EventArgs e)
        {
           
        }

        private void HFGP_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void chkact_CheckedChanged(object sender, EventArgs e)
        {
            LoadGetJobCard(1);
        }

        private void button6_Click_1(object sender, EventArgs e)
        {
            mode = 1;
            Genpan.Visible = false;
            panadd.Visible = false;
            Genclass.ClearTextBox(this, Editpan);
            //Genclass.Module.ClearTextBox(this, addipan);
            cell9 = 0;
            Genclass.Gendocno();
            txtgrn.Text = Genclass.ST;
            label15.Visible = true;
            txtitemname.Visible = true;
            label14.Visible = true;
            txtprice.Visible = true;
            label31.Visible = true;
            txtqty.Visible = true;
            label32.Visible = true;
            txtbval.Visible = true;
            buttcusok.Visible = true;

            Editpan.Visible = true;
            btnsave.Visible = true;
            //btnaddrcan.Visible = false;
            //buttnnxt.Visible = false;
            buttnfinbk.Visible = true;
            //button11.Visible = true;
            //button12.Visible = true;
            DTPDOCDT.Focus();
            HFIT.Refresh();
            HFIT.DataSource = null;
            HFIT.Rows.Clear();

            HFGST.Refresh();
            HFGST.DataSource = null;
            HFGST.Rows.Clear();
            conn.Close();
            conn.Open();
            Docno.Clear();
            if (Docno.Columns.Count == 0)
            {
                Docno.Columns.Add("uid");
                Docno.Columns.Add("Docno");
            }
            qur.CommandText = "truncate table PgBatmp";
            qur.ExecuteNonQuery();

            Genclass.sum = 1;
            Genclass.sum1 = 0;
            Genclass.sum2 = 0;
            Genclass.sum3 = 0;
            Genclass.sum4 = 0;
            Genclass.sum5 = 0;

            Titlep();

      

            Editpan.Width = 1280;
            Editpan.Height = 618;


        }

        private void butedit_Click(object sender, EventArgs e)
        {
            if (HFGP.Rows.Count == 1)
            {
                MessageBox.Show("No Record");
                return;
            }
            mode = 2;
            dis3 = 0;
            dis3 = 0;

            txtexcise.Text = "0";
            txtigval.Text = "0";

            HFIT.Columns[12].ReadOnly = true;
            HFIT.Columns[13].ReadOnly = true;

            panadd.Visible = false;
            Genpan.Visible = false;
            Taxpan.Visible = false;
            Editpan.Visible = true;

            Genclass.sum = 1;
            Genclass.sum1 = 0;
            Genclass.sum2 = 0;
            Genclass.sum3 = 0;
            Genclass.sum4 = 0;
            Genclass.sum5 = 0;
            label15.Visible = true;
            txtitemname.Visible = true;
            label14.Visible = true;
            txtprice.Visible = true;
            label31.Visible = true;
            txtqty.Visible = true;
            label32.Visible = true;
            txtbval.Visible = true;
            buttcusok.Visible = true;
            btnsave.Visible = true;
            buttnfinbk.Visible = true;

            int i = HFGP.SelectedCells[0].RowIndex;
            uid = HFGP.Rows[i].Cells[0].Value.ToString();
            txtgrnid.Text = HFGP.Rows[i].Cells[0].Value.ToString();
            txtgrn.Text = HFGP.Rows[i].Cells[1].Value.ToString();
            DTPDOCDT.Text = HFGP.Rows[i].Cells[2].Value.ToString();

            dcdate.Text = HFGP.Rows[i].Cells[4].Value.ToString();
            txtname.Text = HFGP.Rows[i].Cells[5].Value.ToString();
            txtpuid.Text = HFGP.Rows[i].Cells[10].Value.ToString();
            txtgen1.Text = HFGP.Rows[i].Cells[3].Value.ToString();

            txtrem.Text = HFGP.Rows[i].Cells[12].Value.ToString();
            conn.Close();
            conn.Open();

            qur.CommandText = "truncate table PgBatmp";
            qur.ExecuteNonQuery();

           
            Docno.Clear();
            if (Docno.Columns.Count == 0)
            {
                Docno.Columns.Add("uid");
                Docno.Columns.Add("Docno");
            }
            HFIT.Refresh();
            HFIT.DataSource = null;
            HFIT.Rows.Clear();

            HFGT.Refresh();
            HFGT.DataSource = null;
            HFGT.Rows.Clear();

            HFGST.Refresh();
            HFGST.DataSource = null;
            HFGST.Rows.Clear();

            txttdis.Text = "0";
            Titlep();
            if (HFGP.Rows[i].Cells[14].Value.ToString() == "Yarn Purchase")
            {
                Genclass.strsql = "SP_GETBILLEDIT  " + txtgrnid.Text + " ";
                Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
            }
            else if (HFGP.Rows[i].Cells[14].Value.ToString() == "Trims Purchase")

            {
                Genclass.strsql = "SP_GETBILLEDITTRIMS  " + txtgrnid.Text + " ";
                Genclass.cmd = new SqlCommand(Genclass.strsql, conn);

            }
          
            SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
            DataTable tap1 = new DataTable();
            aptr1.Fill(tap1);

            Genclass.sum1 = 0;

            Txttot.Text = "";
            for (int k = 0; k < tap1.Rows.Count; k++)
            {
                var index = HFIT.Rows.Add();
                txttgstp.Text = tap1.Rows[k]["igstid"].ToString();
                HFIT.Rows[index].Cells[0].Value = tap1.Rows[k]["itemname"].ToString();

                HFIT.Rows[index].Cells[1].Value = tap1.Rows[k]["UOM"].ToString();

                double sump = Convert.ToDouble(tap1.Rows[k]["PRate"].ToString());
                HFIT.Rows[index].Cells[2].Value = sump.ToString("0.000");
                double sumq = Convert.ToDouble(tap1.Rows[k]["pqty"].ToString());
                HFIT.Rows[index].Cells[3].Value = sumq.ToString("0.00");
                double sumb = Convert.ToDouble(tap1.Rows[k]["BasicValue"].ToString());
                HFIT.Rows[index].Cells[4].Value = sumb.ToString("0.00");
                txtbval.Text = sumb.ToString("0.00");

                //HFIT.Rows[index].Cells[5].Value = tap1.Rows[k]["disval"].ToString();
                HFIT.Rows[index].Cells[10].Value = tap1.Rows[k]["refuid"].ToString();
                HFIT.Rows[index].Cells[6].Value = tap1.Rows[k]["ItemUId"].ToString();
                HFIT.Rows[index].Cells[16].Value = tap1.Rows[k]["bags"].ToString();
                HFIT.Rows[index].Cells[17].Value = tap1.Rows[k]["style"].ToString();
                HFIT.Rows[index].Cells[18].Value = tap1.Rows[k]["socno"].ToString();


                if (txtpluid.Text == "196")
                {
                    double yu = Convert.ToDouble(tap1.Rows[k]["cgstid"].ToString()) * 2;
                    HFIT.Rows[index].Cells[7].Value = yu;

                    double yu1 = Convert.ToDouble(tap1.Rows[k]["cgstval"].ToString()) * 2;
                    HFIT.Rows[index].Cells[8].Value = yu1;
               
                }


                else
                {
                    HFIT.Rows[index].Cells[7].Value = tap1.Rows[k]["igstid"].ToString();
                    HFIT.Rows[index].Cells[8].Value = tap1.Rows[k]["igstval"].ToString();
                }


                if (tap1.Rows[k]["cgstid"].ToString() != null)
                {
                    HFIT.Rows[index].Cells[13].Value = tap1.Rows[k]["cgstid"].ToString();
                    HFIT.Rows[index].Cells[14].Value = tap1.Rows[k]["cgstval"].ToString();
                }

                if (txtpluid.Text == "196")
                {
                    dis4 = Convert.ToDouble(tap1.Rows[k]["cgstid"].ToString()) * 2;
                    dis3 = Convert.ToDouble(tap1.Rows[k]["cgstval"].ToString()) * 2;
                }
                else
                {
                    dis4 = Convert.ToDouble(tap1.Rows[k]["igstid"].ToString());
                    dis3 = Convert.ToDouble(tap1.Rows[k]["igstval"].ToString());
                }



                Genclass.sum1 = Genclass.sum1 + Convert.ToDouble(HFIT.Rows[index].Cells[4].Value.ToString());

            }

            Txttot.Text = Genclass.sum1.ToString();

        

            btnaddrcan.Visible = false;


            Genclass.strsql = "select * from purchasecharges a left join generalm b on a.chargesuid=b.guid  where a.puid=" + txtgrnid.Text + " ";

            Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
            SqlDataAdapter aptr2 = new SqlDataAdapter(Genclass.cmd);
            DataTable tap2 = new DataTable();
            aptr2.Fill(tap2);
            if (tap2.Rows.Count > 0)
            {
                txtcharges.Text = tap2.Rows[0]["chargeamount"].ToString();
            }

            if (tap1.Rows.Count > 0)
            {
                txttbval.Text = Txttot.Text;
                txttdis.Text = tap1.Rows[0]["disp"].ToString();
                //txttdisc.Text = tap1.Rows[0]["disval"].ToString();

            }
            txtbval.Text = "";
            serialno.Visible = false;

            mappnl.Visible = false;
            TxtNetAmt.Text = HFGP.Rows[i].Cells[9].Value.ToString();
            TxtRoff.Text = HFGP.Rows[i].Cells[13].Value.ToString();

            loadgrndetedit();
        }

        private void buttnext1_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void HFGP_CellMouseUp(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (HFGP.Rows[0].Cells[0].Value != "")
            {
                int index = HFGP.SelectedRows[0].Index;
                if (index < HFGP.Rows.Count - 1)
                {
                    index = index + 1;
                    int index1 = HFGP.Rows.Count - 1;
                    lblno1.Text = index.ToString();
                    lblno2.Text = "of " + index1.ToString();
                }
            }
        }

        private void HFGP_KeyUp(object sender, KeyEventArgs e)
        {
            if (HFGP.Rows[0].Cells[0].Value != "")
            {
                int index = HFGP.SelectedRows[0].Index;
                if (index < HFGP.Rows.Count - 1)
                {
                    index = index + 1;
                    int index1 = HFGP.Rows.Count - 1;
                    lblno1.Text = index.ToString();
                    lblno2.Text = "of " + index1.ToString();
                }
            }
        }

        private void buttnnvfst_Click(object sender, EventArgs e)
        {
            int index = HFGP.SelectedRows[0].Index;
            int index1 = 0;
            int index2 = HFGP.Rows.Count - 1;

            HFGP.Rows[index].Selected = false;
            HFGP.Rows[index1].Selected = true;
            index1 = index1 + 1;
            lblno1.Text = index1.ToString();
            lblno2.Text = "of " + index2.ToString();
        }

        private void buttnnxtlft_Click(object sender, EventArgs e)
        {
            int index = HFGP.SelectedRows[0].Index;
            int index1 = HFGP.Rows.Count - 1; ;
            if (index > 0)
            {
                HFGP.Rows[index].Selected = false;
                HFGP.Rows[index - 1].Selected = true;
                lblno1.Text = index.ToString();
                lblno2.Text = "of " + index1.ToString();
            }
        }

        private void buttrnxt_Click(object sender, EventArgs e)
        {
            int index = HFGP.SelectedRows[0].Index;
            int index2 = HFGP.SelectedRows[0].Index + 2;
            int index1 = HFGP.Rows.Count - 1; ;
            if (index < HFGP.Rows.Count - 2)
            {
                HFGP.Rows[index].Selected = false;
                HFGP.Rows[index + 1].Selected = true;
                lblno1.Text = index2.ToString();
                lblno2.Text = "of " + index1.ToString();

            }
        }

        private void btnfinnxt_Click(object sender, EventArgs e)
        {
            int index = HFGP.SelectedRows[0].Index;
            int index1 = HFGP.Rows.Count - 1;


            HFGP.Rows[index].Selected = false;

            HFGP.Rows[index1 - 1].Selected = true;
            lblno1.Text = index1.ToString();
            lblno2.Text = "of " + index1.ToString();

        }

        private void txtscr6_TextChanged(object sender, EventArgs e)
        {
          
        }

        private void btnadd_Click(object sender, EventArgs e)
        {

            
        }



        private void button11_Click_2(object sender, EventArgs e)
        {


        }

        private void splittax()
        {


            if (txttdis.Text == "" || txttdis.Text == null || txttdis.Text == "0" || txtpuid.Text == "")
            {
                txttprdval.Text = txttbval.Text;
            }
            else
            {
                if (txttbval.Text != "")
                {
                    double dis6 = Convert.ToDouble(txttbval.Text) / 100 * Convert.ToDouble(txttdis.Text);
                    txttdisc.Text = dis6.ToString("0.00");

                    dis4 = Convert.ToDouble(txttbval.Text) - Convert.ToDouble(txttdisc.Text);
                    txttprdval.Text = dis4.ToString("#,0.00");
                }
            }

            if (txtcharges.Text == "" || txtcharges.Text == null)
            {
                txtexcise.Text = txttprdval.Text;
            }
            else
            {
                dis4 = Convert.ToDouble(txttprdval.Text) + Convert.ToDouble(txtcharges.Text);
                txtexcise.Text = dis4.ToString("#,0.00");
            }

            if (txtpuid.Text == "")
            {
                return;
            }
            Genclass.strsql = "select generalname from partym a inner join generalm b on a.stateuid=b.uid where a.companyid=" + GeneralParameters.UserdId + " and a.uid=" + txtpuid.Text + "";

            Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
            SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
            DataTable tap1 = new DataTable();
            aptr1.Fill(tap1);
            if (txtexcise.Text != "")
            {
                //MessageBox.Show(tap1.Rows[0]["generalname"].ToString());
                if (tap1.Rows[0]["generalname"].ToString() == "Tamil Nadu 33")
                {


                    //txttgstp.Text = HFIT.Rows[0].Cells[7].Value.ToString();
                    //txttgstval.Text = HFIT.Rows[0].Cells[8].Value.ToString();

                    dis4 = Convert.ToDouble(txttgstp.Text) / 2;

                    txttcgstp.Text = dis4.ToString("0.00");
                    txtsgstp.Text = dis4.ToString("0.00");
                    if (mode == 2)
                    {
                        dis3 = (Convert.ToDouble(txtbval.Text) / 100) * (Convert.ToDouble(txttgstp.Text));

                        txttgstval.Text = dis3.ToString("0.00");
                        dis3 = dis3 / 2;

                        txttcgval.Text = dis3.ToString("0.00");
                        txttsgval.Text = dis3.ToString("0.00");

                        if (txtigval.Text != "")
                        {
                            double dis5 = Convert.ToDouble(txtigval.Text) + Convert.ToDouble(txtexcise.Text) + dis3 + dis3;
                            txtttot.Text = dis5.ToString("0.00");
                        }
                        else
                        {
                            double dis5 = Convert.ToDouble(txtexcise.Text) + dis3 + dis3;
                            txtttot.Text = dis5.ToString("0.00");

                        }
                    }
                    else
                    {
                        if (Genclass.ty == 1)
                        {
                            dis3 = (Convert.ToDouble(txtbval.Text) / 100) * (Convert.ToDouble(txttgstp.Text));

                            txttgstval.Text = dis3.ToString("0.00");
                            dis3 = dis3 / 2;

                            txttcgval.Text = dis3.ToString("0.00");
                            txttsgval.Text = dis3.ToString("0.00");

                            if (txtigval.Text != "")
                            {
                                double dis5 = Convert.ToDouble(txtigval.Text) + Convert.ToDouble(txtexcise.Text) + dis3 + dis3;
                                txtttot.Text = dis5.ToString("0.00");
                            }
                            else
                            {
                                double dis5 = Convert.ToDouble(txtexcise.Text) + dis3 + dis3;
                                txtttot.Text = dis5.ToString("0.00");

                            }
                        }
                        else
                        {
                            if (HFIT.CurrentRow.Cells[13].Value == "" || HFIT.CurrentRow.Cells[13].Value == null)
                            {


                                dis3 = (Convert.ToDouble(txtbval.Text) / 100) * (Convert.ToDouble(txttgstp.Text));

                                txttgstval.Text = dis3.ToString("0.00");
                                dis3 = dis3 / 2;

                                txttcgval.Text = dis3.ToString("0.00");
                                txttsgval.Text = dis3.ToString("0.00");

                                if (txtigval.Text != "")
                                {
                                    double dis5 = Convert.ToDouble(txtigval.Text) + Convert.ToDouble(txtexcise.Text) + dis3 + dis3;
                                    txtttot.Text = dis5.ToString("0.00");
                                }
                                else
                                {
                                    double dis5 = Convert.ToDouble(txtexcise.Text) + dis3 + dis3;
                                    txtttot.Text = dis5.ToString("0.00");

                                }
                            }

                        }
                    }
                }


                else
                {
                    if (txtexcise.Text != "")
                    {
                        txttgstp.Text = HFIT.Rows[0].Cells[7].Value.ToString();
                        dis4 = Convert.ToDouble(HFIT.Rows[0].Cells[7].Value.ToString());
                        txttgstval.Text = HFIT.Rows[0].Cells[8].Value.ToString();
                        txtigstp.Text = HFIT.Rows[0].Cells[7].Value.ToString();


                        dis3 = (Convert.ToDouble(txtbval.Text) / 100) * (Convert.ToDouble(txttgstp.Text));
                        txttgstval.Text = dis3.ToString("0.00");
                        txtigval.Text = dis3.ToString("0.00");

                        if (txtigval.Text != "")
                        {
                            double dis5 = Convert.ToDouble(txtigval.Text) + Convert.ToDouble(txtexcise.Text) + dis3;
                            txtttot.Text = dis5.ToString("0.00");
                        }
                        else
                        {
                            double dis5 = Convert.ToDouble(txtexcise.Text) + dis3;
                            txtttot.Text = dis5.ToString("0.00");
                        }

                    }
                }




                Double net1 = Convert.ToDouble(txtttot.Text);
                TxtNetAmt.Text = net1.ToString("0.00");
                double someInt = (int)net1;

                double rof = Math.Round(net1 - someInt, 2);
                TxtRoff.Text = rof.ToString("0.00");

                if (Convert.ToDouble(TxtRoff.Text) < 0.49)
                {
                    Double rof1 = -1 * Convert.ToDouble(TxtRoff.Text);
                    TxtRoff.Text = rof1.ToString("0.00");
                }
                else
                {
                    Double rof2 = 1 - Convert.ToDouble(TxtRoff.Text);
                    TxtRoff.Text = rof2.ToString("0.00");
                }

                Double net = Convert.ToDouble(TxtNetAmt.Text) + Convert.ToDouble(TxtRoff.Text);
                //int ne=Convert.ToInt16(net);
                TxtNetAmt.Text = net.ToString("0.00");
            }

            //TxtNetAmt.Text = TxtNetAmt.Text.Replace(",", "");
            //txttcgval.Text = txttcgval.Text.Replace(",", "");
            //txttsgval.Text = txttsgval.Text.Replace(",", "");
            //txtigval.Text = txtigval.Text.Replace(",", "");
            //txtttot.Text = txtttot.Text.Replace(",", "");
            //txttprdval.Text = txttprdval.Text.Replace(",", "");
            //txtexcise.Text = txtexcise.Text.Replace(",", "");
            //txttdisc.Text = txttdisc.Text.Replace(",", "");


        }

        private void button12_Click_2(object sender, EventArgs e)
        {
            addipan.Visible = false;
            Editpan.Visible = false;
            Genpan.Visible = true;
            panadd.Visible = true;
            LoadGetJobCard(1);
        }



        private void btnaddrcan_Click_1(object sender, EventArgs e)
        {
            addipan.Visible = true;

            Editpan.Visible = false;
            //button1.Visible = false;
            btnaddrcan.Visible = false;
            btnsave.Visible = false;
            buttnfinbk.Visible = true;
            buttnnxt.Visible = true;
        }





        private void buttnnxt_Click(object sender, EventArgs e)
        {
            addipan.Visible = false;

            buttnfinbk.Visible = false;
            buttnnxt.Visible = false;
            btnaddrcan.Visible = true;
            btnsave.Visible = true;

            //HFGT.
            if (HFGT.Rows[0].Cells[0].Value == "" || HFGT.Rows[0].Cells[0].Value == null)
            {
                var index = HFGT.Rows.Add();
                HFGT.Rows[index].Cells[0].Value = "No.of Cases";

                var index1 = HFGT.Rows.Add();
                HFGT.Rows[index1].Cells[0].Value = "Box No/Size";
                var index2 = HFGT.Rows.Add();
                HFGT.Rows[index2].Cells[0].Value = "Gross Weight";
                var index3 = HFGT.Rows.Add();
                HFGT.Rows[index3].Cells[0].Value = "Carrier";
                var index4 = HFGT.Rows.Add();
                HFGT.Rows[index4].Cells[0].Value = "Destination";
                var index5 = HFGT.Rows.Add();
                HFGT.Rows[index5].Cells[0].Value = "LR/RR/RPPNO./DT";
            }
            HFGT.Columns[0].ReadOnly = true;
        }

        private void buttnfinbk_Click(object sender, EventArgs e)
        {
            addipan.Visible = false;
            Taxpan.Visible = false;
            //termspan.Visible = false;
            //buttnfinbk.Visible = false;
            buttnnxt.Visible = false;
            //button11.Visible = true;
            //button12.Visible = true;
            panadd.Visible = true;
            Editpan.Visible = false;
            Genpan.Visible = true;
            Genclass.sum5 = 0;
            TxtNetAmt.Text = Txttot.Text;
            Docno.Dispose();
            j = -1;
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            if (HFGP.Rows.Count == 1)
            {
                MessageBox.Show("No Record");
                return;
            }

            Genclass.strsql = "SELECT A.UID,D.DOCNO,F.DOCNO as grnno FROM STRANSACTIONSP A INNER JOIN STRANSACTIONSPLIST B ON A.UID=B.TRANSACTIONSPUID AND A.DOCTYPEID=100 AND   B.DOCTYPEID=100 INNER JOIN STRANSACTIONSPLIST C  ON B.REFUID = C.UID  AND C.DOCTYPEID = 90INNER JOIN STRANSACTIONSP D  ON C.TRANSACTIONSPUID = D.UID  AND D.DOCTYPEID = 90 INNER JOIN TRANSACTIONSPLIST E  ON C.UID = E.REFUID  AND E.DOCTYPEID = 10 INNER JOIN TRANSACTIONSP F  ON E.TRANSACTIONSPUID = F.UID  AND F.DOCTYPEID = 10 WHERE A.UID = " + HFGP.CurrentRow.Cells[0].Value.ToString() + "";

            Genclass.FSSQLSortStr = "itemname";
            Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
            SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
            DataTable tap = new DataTable();
            aptr.Fill(tap);

            if (tap.Rows.Count > 0)

            {
                for (int mm = 0; mm < tap.Rows.Count; mm++)
                {
                    if (mm == 0)
                    {
                        Genclass.address = tap.Rows[mm]["grnno"].ToString();
                    }
                    else
                    {

                        Genclass.address = Genclass.address + " , " + tap.Rows[mm]["grnno"].ToString();

                    }


                }

            }
            conn.Close();
            conn.Open();
            qur.CommandText = "delete from  getpogrnno";
            qur.ExecuteNonQuery();
            qur.CommandText = "insert into getpogrnno values(" + HFGP.CurrentRow.Cells[0].Value.ToString() + ",'" + tap.Rows[0]["docno"].ToString() + "',('" + Genclass.address + "'))";
            qur.ExecuteNonQuery();


            conn.Close();
            conn.Open();

            qur.CommandText = "delete from  NotoWords";
            qur.ExecuteNonQuery();

            Int64 NumVal = Convert.ToInt64(HFGP.CurrentRow.Cells[9].Value);
            string Nw = Rupees(NumVal);

            qur.CommandText = "Insert into NotoWords values('" + Nw + "',1)";
            qur.ExecuteNonQuery();

            Genclass.Prtid = Convert.ToInt16(HFGP.CurrentRow.Cells[0].Value.ToString());


            for (int i = 1; i < 4; i++)
            {
                Genclass.slno = i;
                Crviewer crv = new Crviewer();

                Genclass.strsql = "select * from Vw_salprtexlfin   where muid=" + Genclass.Prtid + " and Companyid=" + GeneralParameters.UserdId + " and doctypeid=100";

                Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                SqlDataAdapter aptr3 = new SqlDataAdapter(Genclass.cmd);
                DataTable tap3 = new DataTable();
                aptr3.Fill(tap3);
                if (tap3.Rows.Count > 0)
                {
                    if (tap3.Rows[0]["bstate"].ToString() == "Tamil Nadu 33")
                    {
                      

                            SqlDataAdapter da = new SqlDataAdapter("[sp_pdfNew1Bill]", conn);
                            da.SelectCommand.CommandType = CommandType.StoredProcedure;
                            da.SelectCommand.Parameters.Add("@Muid", SqlDbType.Int).Value = Genclass.Prtid;
                            da.SelectCommand.Parameters.Add("@slno", SqlDbType.Int).Value = Genclass.slno;
                            da.SelectCommand.Parameters.Add("@companyid", SqlDbType.Int).Value = GeneralParameters.UserdId;
                            da.SelectCommand.Parameters.Add("@doctypeid", SqlDbType.Int).Value = 100;
                            DataSet ds = new DataSet();
                            da.Fill(ds, "salesinvoice");

                            if (GeneralParameters.UserdId == 1)
                            {
                                doc.Load(Application.StartupPath + "\\BillAccounting.rpt");
                            }
                            else
                            {
                                doc.Load(Application.StartupPath + "\\salesinvoiceNewVen.rpt");
                            }



                            SqlDataAdapter da1 = new SqlDataAdapter("terms", conn);
                            da1.SelectCommand.CommandType = CommandType.StoredProcedure;
                            da1.SelectCommand.Parameters.Add("@uid", SqlDbType.Int).Value = Genclass.Prtid;
                            DataTable ds1 = new DataTable("Terms");
                            da1.Fill(ds1);
                            ds.Tables.Add(ds1);

                            doc.Subreports["Termrpt1.rpt"].SetDataSource(ds1);

                            SqlDataAdapter da2 = new SqlDataAdapter("SP_taxcal", conn);
                            da2.SelectCommand.CommandType = CommandType.StoredProcedure;
                            da2.SelectCommand.Parameters.Add("@muid", SqlDbType.Int).Value = Genclass.Prtid;
                            da2.SelectCommand.Parameters.Add("@statuid", SqlDbType.Int).Value = tap3.Rows[0]["stateuid"].ToString();
                            da2.SelectCommand.Parameters.Add("@companyid", SqlDbType.Int).Value = GeneralParameters.UserdId;
                            DataTable ds2 = new DataTable("taxcal");
                            da2.Fill(ds2);
                            ds.Tables.Add(ds2);

                            doc.Subreports["Taxrpt.rpt"].SetDataSource(ds2);
                            doc.SetDataSource(ds);



                        }

                        else
                        {
                            SqlDataAdapter da = new SqlDataAdapter("[sp_pdfNew1Bill]", conn);
                            da.SelectCommand.CommandType = CommandType.StoredProcedure;
                            da.SelectCommand.Parameters.Add("@Muid", SqlDbType.Int).Value = Genclass.Prtid;
                            da.SelectCommand.Parameters.Add("@slno", SqlDbType.Int).Value = Genclass.slno;
                            da.SelectCommand.Parameters.Add("@companyid", SqlDbType.Int).Value = GeneralParameters.UserdId;
                            da.SelectCommand.Parameters.Add("@doctypeid", SqlDbType.Int).Value = 100;
                            DataSet ds = new DataSet();
                            da.Fill(ds, "salesinvoice");

                            if (GeneralParameters.UserdId == 1)
                            {
                                doc.Load(Application.StartupPath + "\\BillAccounting.rpt");
                            }
                            else
                            {
                                doc.Load(Application.StartupPath + "\\salesinvoiceNewVen.rpt");
                            }



                            SqlDataAdapter da1 = new SqlDataAdapter("terms", conn);
                            da1.SelectCommand.CommandType = CommandType.StoredProcedure;
                            da1.SelectCommand.Parameters.Add("@uid", SqlDbType.Int).Value = Genclass.Prtid;
                            DataTable ds1 = new DataTable("Terms");
                            da1.Fill(ds1);
                            ds.Tables.Add(ds1);

                            doc.Subreports["Termrpt1.rpt"].SetDataSource(ds1);

                            SqlDataAdapter da2 = new SqlDataAdapter("SP_taxcal", conn);
                            da2.SelectCommand.CommandType = CommandType.StoredProcedure;
                            da2.SelectCommand.Parameters.Add("@muid", SqlDbType.Int).Value = Genclass.Prtid;
                            da2.SelectCommand.Parameters.Add("@statuid", SqlDbType.Int).Value = tap3.Rows[0]["stateuid"].ToString();
                            da2.SelectCommand.Parameters.Add("@companyid", SqlDbType.Int).Value = GeneralParameters.UserdId;
                            DataTable ds2 = new DataTable("taxcal");
                            da2.Fill(ds2);
                            ds.Tables.Add(ds2);

                            doc.Subreports["Taxrpt.rpt"].SetDataSource(ds2);
                            doc.SetDataSource(ds);

                        
                    }




                    doc.PrintToPrinter(1, false, 0, 0);
                }
            }
        }


        public string Rupees(Int64 rup)
        {
            string result = "";
            Int64 res;
            if ((rup / 10000000) > 0)
            {
                res = rup / 10000000;
                rup = rup % 10000000;
                result = result + ' ' + RupeesToWords(res) + " Crore";
            }
            if ((rup / 100000) > 0)
            {
                res = rup / 100000;
                rup = rup % 100000;
                result = result + ' ' + RupeesToWords(res) + " Lakh";
            }
            if ((rup / 1000) > 0)
            {
                res = rup / 1000;
                rup = rup % 1000;
                result = result + ' ' + RupeesToWords(res) + " Thousand";
            }


            if ((rup / 100) > 0)
            {
                res = rup / 100;
                rup = rup % 100;
                if (rup > 0)
                {
                    result = result + ' ' + RupeesToWords(res) + " Hundred and";
                }
                else
                {
                    result = result + ' ' + RupeesToWords(res) + " Hundred";
                }
            }
            if ((rup % 10) >= 0)
            {
                res = rup % 100;
                result = result + " " + RupeesToWords(res);
            }
            result = "Rupees " + result + ' ' + "only";
            return result;
        }

        public string RupeesToWords(Int64 rup)
        {
            string result = "";
            if ((rup >= 1) && (rup <= 10))
            {
                if ((rup % 10) == 1) result = "One";
                if ((rup % 10) == 2) result = "Two";
                if ((rup % 10) == 3) result = "Three";
                if ((rup % 10) == 4) result = "Four";
                if ((rup % 10) == 5) result = "Five";
                if ((rup % 10) == 6) result = "Six";
                if ((rup % 10) == 7) result = "Seven";
                if ((rup % 10) == 8) result = "Eight";
                if ((rup % 10) == 9) result = "Nine";
                if ((rup % 10) == 0) result = "Ten";
            }
            if (rup > 9 && rup < 20)
            {
                if (rup == 11) result = "Eleven";
                if (rup == 12) result = "Twelve";
                if (rup == 13) result = "Thirteen";
                if (rup == 14) result = "Forteen";
                if (rup == 15) result = "Fifteen";
                if (rup == 16) result = "Sixteen";
                if (rup == 17) result = "Seventeen";
                if (rup == 18) result = "Eighteen";
                if (rup == 19) result = "Nineteen";
            }

            if (rup >= 20 && (rup / 10) == 2 && (rup % 10) == 0) result = "Twenty";
            if (rup > 20 && (rup / 10) == 3 && (rup % 10) == 0) result = "Thirty";
            if (rup > 20 && (rup / 10) == 4 && (rup % 10) == 0) result = "Forty";
            if (rup > 20 && (rup / 10) == 5 && (rup % 10) == 0) result = "Fifty";
            if (rup > 20 && (rup / 10) == 6 && (rup % 10) == 0) result = "Sixty";
            if (rup > 20 && (rup / 10) == 7 && (rup % 10) == 0) result = "Seventy";
            if (rup > 20 && (rup / 10) == 8 && (rup % 10) == 0) result = "Eighty";
            if (rup > 20 && (rup / 10) == 9 && (rup % 10) == 0) result = "Ninty";

            if (rup > 20 && (rup / 10) == 2 && (rup % 10) != 0)
            {
                if ((rup % 10) == 1) result = "Twenty One";
                if ((rup % 10) == 2) result = "Twenty Two";
                if ((rup % 10) == 3) result = "Twenty Three";
                if ((rup % 10) == 4) result = "Twenty Four";
                if ((rup % 10) == 5) result = "Twenty Five";
                if ((rup % 10) == 6) result = "Twenty Six";
                if ((rup % 10) == 7) result = "Twenty Seven";
                if ((rup % 10) == 8) result = "Twenty Eight";
                if ((rup % 10) == 9) result = "Twenty Nine";
            }
            if (rup > 20 && (rup / 10) == 3 && (rup % 10) != 0)
            {
                if ((rup % 10) == 1) result = "Thirty One";
                if ((rup % 10) == 2) result = "Thirty Two";
                if ((rup % 10) == 3) result = "Thirty Three";
                if ((rup % 10) == 4) result = "Thirty Four";
                if ((rup % 10) == 5) result = "Thirty Five";
                if ((rup % 10) == 6) result = "Thirty Six";
                if ((rup % 10) == 7) result = "Thirty Seven";
                if ((rup % 10) == 8) result = "Thirty Eight";
                if ((rup % 10) == 9) result = "Thirty Nine";
            }
            if (rup > 20 && (rup / 10) == 4 && (rup % 10) != 0)
            {
                if ((rup % 10) == 1) result = "Forty One";
                if ((rup % 10) == 2) result = "Forty Two";
                if ((rup % 10) == 3) result = "Forty Three";
                if ((rup % 10) == 4) result = "Forty Four";
                if ((rup % 10) == 5) result = "Forty Five";
                if ((rup % 10) == 6) result = "Forty Six";
                if ((rup % 10) == 7) result = "Forty Seven";
                if ((rup % 10) == 8) result = "Forty Eight";
                if ((rup % 10) == 9) result = "Forty Nine";
            }
            if (rup > 20 && (rup / 10) == 5 && (rup % 10) != 0)
            {
                if ((rup % 10) == 1) result = "Fifty One";
                if ((rup % 10) == 2) result = "Fifty Two";
                if ((rup % 10) == 3) result = "Fifty Three";
                if ((rup % 10) == 4) result = "Fifty Four";
                if ((rup % 10) == 5) result = "Fifty Five";
                if ((rup % 10) == 6) result = "Fifty Six";
                if ((rup % 10) == 7) result = "Fifty Seven";
                if ((rup % 10) == 8) result = "Fifty Eight";
                if ((rup % 10) == 9) result = "Fifty Nine";
            }
            if (rup > 20 && (rup / 10) == 6 && (rup % 10) != 0)
            {
                if ((rup % 10) == 1) result = "Sixty One";
                if ((rup % 10) == 2) result = "Sixty Two";
                if ((rup % 10) == 3) result = "Sixty Three";
                if ((rup % 10) == 4) result = "Sixty Four";
                if ((rup % 10) == 5) result = "Sixty Five";
                if ((rup % 10) == 6) result = "Sixty Six";
                if ((rup % 10) == 7) result = "Sixty Seven";
                if ((rup % 10) == 8) result = "Sixty Eight";
                if ((rup % 10) == 9) result = "Sixty Nine";
            }
            if (rup > 20 && (rup / 10) == 7 && (rup % 10) != 0)
            {
                if ((rup % 10) == 1) result = "Seventy One";
                if ((rup % 10) == 2) result = "Seventy Two";
                if ((rup % 10) == 3) result = "Seventy Three";
                if ((rup % 10) == 4) result = "Seventy Four";
                if ((rup % 10) == 5) result = "Seventy Five";
                if ((rup % 10) == 6) result = "Seventy Six";
                if ((rup % 10) == 7) result = "Seventy Seven";
                if ((rup % 10) == 8) result = "Seventy Eight";
                if ((rup % 10) == 9) result = "Seventy Nine";
            }
            if (rup > 20 && (rup / 10) == 8 && (rup % 10) != 0)
            {
                if ((rup % 10) == 1) result = "Eighty One";
                if ((rup % 10) == 2) result = "Eighty Two";
                if ((rup % 10) == 3) result = "Eighty Three";
                if ((rup % 10) == 4) result = "Eighty Four";
                if ((rup % 10) == 5) result = "Eighty Five";
                if ((rup % 10) == 6) result = "Eighty Six";
                if ((rup % 10) == 7) result = "Eighty Seven";
                if ((rup % 10) == 8) result = "Eighty Eight";
                if ((rup % 10) == 9) result = "Eighty Nine";
            }
            if (rup > 20 && (rup / 10) == 9 && (rup % 10) != 0)
            {
                if ((rup % 10) == 1) result = "Ninty One";
                if ((rup % 10) == 2) result = "Ninty Two";
                if ((rup % 10) == 3) result = "Ninty Three";
                if ((rup % 10) == 4) result = "Ninty Four";
                if ((rup % 10) == 5) result = "Ninty Five";
                if ((rup % 10) == 6) result = "Ninty Six";
                if ((rup % 10) == 7) result = "Ninty Seven";
                if ((rup % 10) == 8) result = "Ninty Eight";
                if ((rup % 10) == 9) result = "Ninty Nine";
            }
            return result;
        }




        private void txtname_KeyDown_1(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.F10)
                {
                    grSearch.Visible = false;
                }
                else if (e.KeyCode == Keys.F2)
                {
                    SelectId = 1;
                    int Index = DataGridCommon.SelectedCells[0].RowIndex;

                    txtname.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtpuid.Text = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    txtgen1.Focus();
                    loadpono();
                    grSearch.Visible = false;
                    SelectId = 0;
                }
                else if (e.KeyCode == Keys.Escape)
                {
                    grSearch.Visible = false;
                }
                else
                {

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

        }

        private void txtdcno_KeyDown_1(object sender, KeyEventArgs e)
        {


        }



        private void TxtNetAmt_TextChanged(object sender, EventArgs e)
        {

        }

        private void buttcusok_Click_1(object sender, EventArgs e)
        {


        }

        private void txtitemname_KeyDown_1(object sender, KeyEventArgs e)
        {

        }



        private void txtplace_KeyDown(object sender, KeyEventArgs e)
        {

        }

        private void txtcharges_TextChanged(object sender, EventArgs e)
        {
            //splittax();
        }

        private void label16_Click(object sender, EventArgs e)
        {

        }

        private void TxtRoff_TextChanged(object sender, EventArgs e)
        {

        }

        private void label17_Click(object sender, EventArgs e)
        {

        }

        private void txttbval_TextChanged(object sender, EventArgs e)
        {

        }

        private void txttdis_TextChanged(object sender, EventArgs e)
        {
            if (txttdis.Text != "")
            {
                if (Convert.ToDouble(txttdis.Text) > 0)
                {
                    double dis = (Convert.ToDouble(txttbval.Text) / 100) * (Convert.ToDouble(txttdis.Text));
                    txttdisc.Text = dis.ToString();
                }
                else
                {
                    txttdisc.Text = "0";
                }
                splittax();
            }
        }

        private void txtprice_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtname_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.F10)
                {
                    grSearch.Visible = false;
                }
                else if (e.KeyCode == Keys.F2)
                {
                    SelectId = 1;
                    int Index = DataGridCommon.SelectedCells[0].RowIndex;

                    txtname.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtpuid.Text = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    txtgen1.Focus();

                    grSearch.Visible = false;
                    SelectId = 0;
                }
                else if (e.KeyCode == Keys.Escape)
                {
                    grSearch.Visible = false;
                }
                else if (e.KeyValue == 40)
                {
                    DataGridCommon.Select();
                }
                else if (e.KeyCode == Keys.Enter)
                {
                    txtname_Click(sender,e);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }


        }

        private void txtplace_KeyDown_1(object sender, KeyEventArgs e)
        {
            //if (e.KeyCode == Keys.Enter)
            //{
            //    Genclass.fieldone = "";
            //    Genclass.fieldtwo = "";
            //    Genclass.fieldthree = "";
            //    Genclass.fieldFour = "";
            //    Genclass.fieldFive = "";

            //    Genclass.type = 7;
            //    loadput();


            //}
            //else if (e.KeyCode == Keys.Escape)
            //{
            //    txtplace.Text = "";
            //    txtplace.Focus();
            //}
        }

        private void txtitemname_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                //Genclass.fieldone = "";
                //Genclass.fieldtwo = "";
                //Genclass.fieldthree = "";
                //Genclass.fieldFour = "";
                //Genclass.fieldFive = "";
                //Genclass.type = 5;
                //loadput();
                txtprice.Focus();
            }
        }

        private void buttcusok_Click(object sender, EventArgs e)
        {

            if (Genclass.ty1 == 2)
            {
                if (GeneralParameters.UserdId == 1)
                {
                    Genclass.strsql = "select * from pgbatmp";
                }
                else
                {
                    Genclass.strsql = "select * from batmp";

                }

            }
            else
            {
                if (txtqty.Text == "" || txtqty.Text == "0" || txtprice.Text == "" || txtprice.Text == "0" || txtitemname.Text == "" || txtitemname.Text == "0" || txtname.Text == "" || txtname.Text == "0")
                {
                    MessageBox.Show("Enter the Party or Item or Rate or Qty to Proceed");
                    return;
                }


                //Genclass.strsql = "select distinct Itemname,UoM,itemuid,0 as refid,qty,PRICE,convert(decimal(18,2),BasicValue,105) AS BasicValue,disper,convert(decimal(18,2),Disvalue,105) AS Disvalue,convert(decimal(18,2),Taxablevalue,105) AS Taxablevalue,convert(decimal(18,2),Cgstval,105) AS EDVAL,convert(decimal(18,2),sgstval,105) AS VATVAL,0 AS igstval,CG as ED,sg as VAT,0 as ig,convert(decimal(18,2),BasicValue-(isnull(Disvalue,0))+ (isnull(Cgstval,0)+isnull(sgstval,0)),105) as total from (select distinct   c.itemname,d.generalname as uom,b.itemuid,b.uid as refid," + txtqty.Text + " as qty," + txtprice.Text + " as Price," + txtbval.Text + " as BasicValue,disper,(" + txtbval.Text + " * disper)/100 as Disvalue," + txtbval.Text + "-((" + txtbval.Text + " * disper)/100) as Taxablevalue,g.f1 as CG,((" + txtbval.Text + "-((" + txtbval.Text + " * disper)/100)) * g.f1)/100 as Cgstval,h.f1 as SG,((" + txtbval.Text + "-((" + txtbval.Text + " * disper)/100)+ ((" + txtbval.Text + "-((" + txtbval.Text + " * disper)/100)) * g.f1)/100) * h.f1)/100 as sgstval from transactionsp a inner join transactionsplist b on  a.uid=b.transactionspuid and a.doctypeid=30 and a.companyid=1 inner join itemm c on b.itemuid=c.uid left join generalm d on c.uom_uid=d.uid   inner join pur_price_list f on c.uid=f.itemuid and c.partyuid=f.suppuid inner join generalm g on f.cgstid=g.uid inner join generalm h on f.sgstid=h.uid inner join generalm i on f.igstid=i.uid where c.uid=" + txttitemid.Text + " group by c.itemname,d.generalname,b.itemuid,b.uid,b.pqty,f.price,disper,g.f1,h.f1,i.f1) tab";
                //Genclass.strsql = "select distinct uid as itemuid,hsnid,Itemname,UoM,0 as refid," + txtqty.Text + " as qty,PRICE,convert(decimal(18,2),BasicValue,105) AS BasicValue,disper,convert(decimal(18,2),Disvalue,105) AS Disvalue,convert(decimal(18,2),Taxablevalue,105) AS Taxablevalue,gstper,gstval,convert(decimal(18,2),BasicValue-(isnull(Disvalue,0))+ (isnull(gstval,0)),105) as total from (select distinct   c.hsnid,c.itemname,d.generalname as uom,c.uid,10 as qty,42.00 as Price," + txtbval.Text + " as BasicValue,disper,(" + txtbval.Text + " * disper)/100 as Disvalue," + txtbval.Text + "-((" + txtbval.Text + " * disper)/100) as Taxablevalue,i.f1 as gstper,((" + txtqty.Text + " * f.Price)-(((" + txtqty.Text + " * f.Price)/100)* disper))/100 * i.f1 as gstval from  itemm c left join generalm d on c.uom_uid=d.uid   inner join pur_price_list f on c.uid=f.itemuid and c.partyuid=f.suppuid  left join generalm i on f.igstid=i.uid where c.uid=" + txttitemid.Text + " group by c.hsnid,c.itemname,d.generalname,c.uid,f.price,disper,i.f1) tab";
                Genclass.strsql = "select distinct uid as itemuid,hsnid,Itemname,UoM,0 as refid,qty,PRICE,convert(decimal(18,2),BasicValue,105) AS BasicValue,disper,convert(decimal(18,2),Disvalue,105) AS Disvalue,convert(decimal(18,2),Taxablevalue,105) AS Taxablevalue,gstper,gstval,convert(decimal(18,2),BasicValue-(isnull(Disvalue,0))+ (isnull(gstval,0)),105) as total from (select distinct c.hsnid,c.itemname,d.generalname as uom,c.uid," + txtqty.Text + " as qty," + txtprice.Text + " as Price," + txtbval.Text + " as BasicValue,0 as disper,0 as Disvalue," + txtbval.Text + " as Taxablevalue,i.f1 as gstper,convert(decimal(18,2),(" + txtbval.Text + " /100 * i.f1),105) as gstval from  itemm c left join generalm d on c.uom_uid=d.uid left join ItemGroup j on c.itemgroup_Uid=j.UId left join Hsndet f on j.hsnid=f.uid left join generalm i on f.sgid=i.uid  where c.Uid=" + txttitemid.Text + " group by c.hsnid,c.itemname,d.generalname,c.uid,i.f1) tab";
            }



            Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
            SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
            DataTable tap1 = new DataTable();
            aptr1.Fill(tap1);
            //txtname.Text = tap1.Rows[0]["Name"].ToString();
            //txtpuid.Text = tap1.Rows[0]["Partyuid"].ToString();
            for (int i = 0; i < tap1.Rows.Count; i++)
            {
                var index = HFIT.Rows.Add();
                HFIT.Rows[index].Cells[0].Value = tap1.Rows[i]["itemname"].ToString();
                HFIT.Rows[index].Cells[1].Value = tap1.Rows[i]["UOM"].ToString();

                double sump = Convert.ToDouble(tap1.Rows[i]["Price"].ToString());
                HFIT.Rows[index].Cells[2].Value = sump.ToString("0.000");
                double sumq = Convert.ToDouble(tap1.Rows[i]["qty"].ToString());
                HFIT.Rows[index].Cells[3].Value = sumq.ToString("0.00");
                double sumb = Convert.ToDouble(tap1.Rows[i]["BasicValue"].ToString());
                HFIT.Rows[index].Cells[4].Value = sumb.ToString("0.00");

                //HFIT.Rows[index].Cells[2].Value = Txttot.Text.Replace(",", "");
                //Txttot.Text = Txttot.Text.Replace(",", "");
                //Txttot.Text = Txttot.Text.Replace(",", "");
                //HFIT.Rows[index].Cells[2].Value = tap1.Rows[i]["Price"].ToString();
                //HFIT.Rows[index].Cells[3].Value = tap1.Rows[i]["qty"].ToString();
                //HFIT.Rows[index].Cells[4].Value = tap1.Rows[i]["BasicValue"].ToString();
                HFIT.Rows[index].Cells[5].Value = tap1.Rows[i]["itemuid"].ToString();
                HFIT.Rows[index].Cells[6].Value = tap1.Rows[i]["refid"].ToString();
                HFIT.Rows[index].Cells[7].Value = tap1.Rows[i]["disper"].ToString();
                HFIT.Rows[index].Cells[8].Value = tap1.Rows[i]["Disvalue"].ToString();
                HFIT.Rows[index].Cells[9].Value = tap1.Rows[i]["Taxablevalue"].ToString();
                HFIT.Rows[index].Cells[10].Value = tap1.Rows[i]["GSTper"].ToString();
                HFIT.Rows[index].Cells[11].Value = tap1.Rows[i]["GSTVAL"].ToString();
                //HFIT.Rows[index].Cells[12].Value = tap1.Rows[i]["VAT"].ToString();
                //HFIT.Rows[index].Cells[13].Value = tap1.Rows[i]["VATVAL"].ToString();
                HFIT.Rows[index].Cells[14].Value = tap1.Rows[i]["Total"].ToString();
                HFIT.Rows[index].Cells[15].Value = tap1.Rows[i]["hsnid"].ToString();
                HFIT.Rows[index].Cells[16].Value = txtnotes.Text;

                Genclass.sum1 = Genclass.sum1 + Convert.ToDouble(HFIT.Rows[index].Cells[4].Value);
                Txttot.Text = Genclass.sum1.ToString("#,0.00");

                Txttot.Text = Txttot.Text.Replace(",", "");
            }



            Titlep();
            txtitemname.Text = "";
            txtprice.Text = "";
            txtqty.Text = "";
            txtnotes.Text = "";
            txtbval.Text = "";
            txtitemname.Focus();

        }


        private void txtname_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bsParty.Filter = string.Format("Name LIKE '%{0}%' ", txtname.Text);

                 }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void butcan_Click(object sender, EventArgs e)
        {
            string message = "Are you sure to cancel this Invoice ?";
            string caption = "Dilama";
            MessageBoxButtons buttons = MessageBoxButtons.YesNo;
            DialogResult result;

            result = MessageBox.Show(message, caption, buttons);

            if (result == System.Windows.Forms.DialogResult.Yes)
            {

                int i = HFGP.SelectedCells[0].RowIndex;
                uid = HFGP.Rows[i].Cells[0].Value.ToString();
                qur.CommandText = "Update stransactionsp set active=0 where uid=" + uid + "";
                qur.ExecuteNonQuery();
                MessageBox.Show("Invoice Cancelled");
            }
            LoadGetJobCard(1);
        }

        private void txtpuid_TextChanged(object sender, EventArgs e)

        {
            if (txtpuid.Text != "")
            {
                Genclass.strsql = "Select address1, address2 ,city,stateuid  from supplierm where uid=" + txtpuid.Text + " and companyid=" + GeneralParameters.UserdId + "";



                Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
                DataTable tap1 = new DataTable();
                aptr1.Fill(tap1);

                if (tap1.Rows.Count > 0)
                {
                    txtpadd1.Text = tap1.Rows[0]["address1"].ToString() + ' ' + tap1.Rows[0]["address2"].ToString() + ' ' + tap1.Rows[0]["city"].ToString();
                    txtplace.Text = txtname.Text;
                    txtpluid.Text = txtpuid.Text;
                    txtpadd2.Text = txtpadd1.Text;
                    txtpluid.Text = tap1.Rows[0]["stateuid"].ToString();
                    Genclass.cat = Convert.ToInt32(tap1.Rows[0]["stateuid"].ToString());
                }
            }
        }

        private void txtpluid_TextChanged(object sender, EventArgs e)
        {
            if (txtpluid.Text != "")
            {

                Genclass.strsql = "Select address1, address2 ,city  from supplierm where uid=" + txtpluid.Text + " and companyid=" + GeneralParameters.UserdId + "";



                Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
                DataTable tap1 = new DataTable();
                aptr1.Fill(tap1);

                if (tap1.Rows.Count > 0)
                {
                    txtpadd2.Text = tap1.Rows[0]["address1"].ToString() + ' ' + tap1.Rows[0]["address2"].ToString() + ' ' + tap1.Rows[0]["city"].ToString();
                    //txtplace.Text = txtname.Text;
                    //txtpluid.Text = txtpuid.Text;
                    //txtpadd2.Text = txtpadd1.Text;
                }
            }
        }

        private void txtplace_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtdcno_KeyDown(object sender, KeyEventArgs e)
        {

            //if (e.KeyCode == Keys.Enter)
            //{
            //    if (txtname.Text == "")
            //    {
            //        MessageBox.Show("Select the Party");
            //        return;
            //    }
            //    Genclass.fieldone = "";
            //    Genclass.fieldtwo = "";
            //    Genclass.fieldthree = "";
            //    Genclass.fieldFour = "";
            //    Genclass.fieldFive = "";

            //    Genclass.type = 8;
            //    loadput();



            //}
            //else if (e.KeyCode == Keys.Escape)
            //{
            //    txtdcno.Text = "";
            //    txtdcid.Text = "";
            //    txtdcno.Focus();
            //}
        }

        private void txtdcid_TextChanged(object sender, EventArgs e)
        {
            if (txtdcid.Text == "" || txtdcid.Text == "0")

            {
                return;
            }
            if (Genclass.ty == 1)
            {

                Genclass.strsql = "select a.docdate,pqty,prate,basicvalue,isnull(e.f1,0) as f1,convert(decimal(18,2),((basicvalue+isnull(f.ChargeAmount,0))/100)*isnull(e.f1,0),102) as taxvalue,z.generalname as uom from stransactionsplist a inner join itemm b on a.itemuid=b.uid inner join generalm z on b.uom_uid=z.uid inner join itemgroup c on b.itemgroup_uid=c.uid inner join hsndet d on c.hsnid=d.uid inner join generalm e on d.sgid=e.uid left join stransactionspcharges f on a.transactionspuid=f.STransactionsPUid left join stransactions  g on a.transactionspuid=g.uid where a.uid=" + txtdcid.Text + "";


                Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
                DataTable tap1 = new DataTable();
                aptr1.Fill(tap1);

                if (tap1.Rows.Count > 0)
                {
                    txtprice.Text = tap1.Rows[0]["prate"].ToString();
                    txtqty.Text = tap1.Rows[0]["pqty"].ToString();
                    txtbval.Text = tap1.Rows[0]["basicvalue"].ToString();
                    txttgstp.Text = tap1.Rows[0]["f1"].ToString();
                    txttgstval.Text = tap1.Rows[0]["taxvalue"].ToString();
                    txtuom.Text = tap1.Rows[0]["uom"].ToString();

                }



            }
            if (Genclass.ty == 2)
            {
                if (txtdcid.Text != "")
                {


                    //Genclass.strsql = "select Docno,convert(varchar,Docdate,105) as Docdate,a.transp,c.hsnid,c.itemname,uom,c.uid,a.uid as Tuid,b.uid as Listuid,b.pqty-isnull(sum(z.pqty),0) as qty,b.PRate as Price,b.BasicValue,i.f1 as gstper,convert(decimal(18,2),(b.BasicValue /100 * i.f1),105) as gstval,b.width,b.length,b.nos from orderp a inner join orderplist b on a.uid=b.TransactionsPUId left join stransactionsplist z on b.Uid=z.Refuid inner join itemm c on b.ItemUId=c.uid  left join ItemGroup j on c.itemgroup_Uid=j.UId left join Hsndet f on j.hsnid=f.uid left join generalm i on f.sgid=i.uid where a.uid=" + txtdcid.Text + " group by Docno,Docdate,c.hsnid,c.itemname,c.uid,i.f1,b.pqty,b.PRate,b.BasicValue,i.f1,b.width,b.length,b.nos,uom,a.uid,a.transp,b.uid having b.pqty-isnull(sum(z.pqty),0)>0";
                    Genclass.strsql = " select Docno,convert(varchar,Docdate,105) as Docdate,a.dcno,a.dcdate,a.narration,c.hsnid,c.itemname,x.generalname as uom,c.uid ,  a.uid as Tuid,  b.Uid as listuid,b.pqty-isnull(sum(z.pqty),0) as qty,isnull(y.prate,m.price) as price, (b.pqty-isnull(sum(z.pqty),0))* isnull(y.prate,m.price) as BasicValue ,i.f1  as gstper, convert(decimal(18,2), ((((b.pqty-isnull(sum(z.pqty),0)))*(isnull(y.prate,m.price))) *  isnull(i.f1,0)/100))   as gstval,  (b.pqty-isnull(sum(z.pqty),0))* isnull(y.prate,m.price)+ ((((b.pqty-isnull(sum(z.pqty),0)))*(isnull(y.prate,m.price))) *  isnull(i.f1,0)/100) as total from   TransactionsP a inner join TransactionsPList b  on a.UId=b.TransactionsPUId LEFT join stransactionsplist z on b.Uid=z.Refuid   and z.doctypeid=100 LEFT join stransactionsplist y on y.Uid=b.Refuid inner join itemm c  on b.ItemUId=c.uid left join  generalm x on c.UOM_UId=x.uid left join   ItemGroup j on c.itemgroup_Uid=j.UId left join  Hsndet f on j.hsnid=f.uid left join generalm i   on f.sgid=i.uid left join pur_price_list m on a.PartyUid=m.Suppuid and b.ItemUId=m.itemuid  where a.uid=" + txtdcid.Text + "   group by Docno,   Docdate,a.dcno,a.dcdate,c.hsnid,c.itemname,c.uid,i.f1,b.pqty,b.PRate,b.BasicValue,i.f1,Price,  a.uid,b.uid,Narration,x.GeneralName,y.prate having b.pqty-isnull(sum(z.pqty),0)>0";


                    Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                    SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
                    DataTable tap1 = new DataTable();
                    aptr1.Fill(tap1);

                    //if (tap1.Rows.Count > 0)
                    //{
                    //    txtdcno.Text = tap1.Rows[0]["dcno"].ToString();
                    //    dtpdc.Text = tap1.Rows[0]["dcdate"].ToString();
                    //}
                    Genclass.sum5 = 0;
                    for (int i = 0; i < tap1.Rows.Count; i++)
                    {


                        var index = HFIT.Rows.Add();
                        HFIT.Rows[index].Cells[0].Value = tap1.Rows[i]["itemname"].ToString();

                        HFIT.Rows[index].Cells[1].Value = tap1.Rows[i]["UOM"].ToString();

                        double sump = Convert.ToDouble(tap1.Rows[i]["Price"].ToString());
                        HFIT.Rows[index].Cells[2].Value = sump.ToString("0.000");
                        double sumq = Convert.ToDouble(tap1.Rows[i]["qty"].ToString());
                        HFIT.Rows[index].Cells[3].Value = sumq.ToString("0.00");
                        double sumb = Convert.ToDouble(tap1.Rows[i]["BasicValue"].ToString());
                        HFIT.Rows[index].Cells[4].Value = sumb.ToString("0.00");

                        Genclass.sum5 = Genclass.sum5 + Convert.ToDouble(HFIT.Rows[i].Cells[4].Value);
                        Txttot.Text = Genclass.sum5.ToString("0.00");

                        HFIT.Rows[index].Cells[5].Value = txtnotes.Text;

                        HFIT.Rows[index].Cells[6].Value = tap1.Rows[i]["uid"].ToString();
                        HFIT.Rows[index].Cells[7].Value = tap1.Rows[i]["gstper"].ToString();

                        HFIT.Rows[index].Cells[8].Value = tap1.Rows[i]["gstval"].ToString();
                        HFIT.Rows[index].Cells[9].Value = tap1.Rows[i]["docno"].ToString();

                        HFIT.Rows[index].Cells[10].Value = tap1.Rows[i]["listuid"].ToString();
                        HFIT.Rows[index].Cells[11].Value = tap1.Rows[i]["hsnid"].ToString();
                        HFIT.Rows[index].Cells[12].Value = tap1.Rows[i]["total"].ToString();



                    }



                }
                //fun();
            }

        }

        private void fun()
        {
            Genclass.sum1 = 0;
            Genclass.sum2 = 0;
            Genclass.sum4 = 0;
            for (int i = 0; i < HFIT.Rows.Count - 1; i++)
            {

                Genclass.sum1 = Genclass.sum1 + Convert.ToDouble(HFIT.Rows[i].Cells[4].Value);
                Txttot.Text = Genclass.sum1.ToString("0.00");

                Genclass.sum2 = Genclass.sum2 + Convert.ToDouble(HFIT.Rows[i].Cells[2].Value);
                txtprice.Text = Genclass.sum2.ToString("0.00");

                // Genclass.sum3 = Genclass.sum3 + Convert.ToDouble(HFIT.Rows[i].Cells[3].Value);
                //txtqty.Text = Genclass.sum3.ToString();

                Genclass.sum4 = Genclass.sum4 + Convert.ToDouble(HFIT.Rows[i].Cells[4].Value);
                txtbval.Text = Genclass.sum4.ToString("0.00");
            }
        }


        //private void HFIT_CellValueChanged_1(object sender, DataGridViewCellEventArgs e)
        //{


        ////    for (int k = 0; k < HFIT.Rows.Count - 1; k++)
        ////    {



        ////        if (HFIT.Rows[k].Cells[2].Value == "" || HFIT.Rows[k].Cells[2].Value == null || HFIT.Rows[k].Cells[3].Value == null || HFIT.Rows[k].Cells[3].Value == "" || HFIT.Rows[k].Cells[7].Value == "" || HFIT.Rows[k].Cells[7].Value == null)
        ////        {
        ////            return;

        ////        }
        ////        else
        ////        {
        ////            //Genclass.sum1 = 0;
        ////            //Genclass.sum4 = 0;
        ////            if (HFIT.Rows[k].Cells[2].Value.ToString() != "0.000" || HFIT.Rows[k].Cells[3].Value.ToString() != "0.000" || HFIT.Rows[k].Cells[7].Value.ToString() != "0.000")
        ////            {
        ////                //if (HFIT.Rows[k].Cells[2].Value.ToString() != "0.000")

        ////                txtprice.Text = HFIT.Rows[k].Cells[2].Value.ToString();
        ////                double str2 = Convert.ToDouble(HFIT.Rows[k].Cells[2].Value.ToString()) * Convert.ToDouble(HFIT.Rows[k].Cells[3].Value.ToString());
        ////                HFIT.Rows[k].Cells[4].Value = str2.ToString();
        ////                double str3 = Convert.ToDouble(HFIT.Rows[k].Cells[4].Value.ToString()) * (Convert.ToDouble(HFIT.Rows[k].Cells[7].Value.ToString()) / 100);
        ////                HFIT.Rows[k].Cells[8].Value = str3.ToString();
        ////                double str4 = Convert.ToDouble(HFIT.Rows[k].Cells[4].Value.ToString()) + Convert.ToDouble(HFIT.Rows[k].Cells[8].Value.ToString());
        ////                HFIT.Rows[k].Cells[12].Value = str4.ToString();

        ////                //Genclass.sum1 = Genclass.sum1 + Convert.ToDouble(HFIT.Rows[k].Cells[4].Value);
        ////                //Txttot.Text = Genclass.sum1.ToString("0.00");

        //    button11_Click_2(sender, e); 



        ////            }

        ////        }

        //        fun2();



        ////    }



        //}

        private void txttrans_TextChanged(object sender, EventArgs e)
        {

        }

        private void txttrans_KeyDown(object sender, KeyEventArgs e)
        {
            //if (txtname.Text == "")
            //{
            //    MessageBox.Show("Select the Party");
            //    return;
            //}
            //Genclass.fieldone = "";
            //Genclass.fieldtwo = "";
            //Genclass.fieldthree = "";
            //Genclass.fieldFour = "";
            //Genclass.fieldFive = "";

            //Genclass.type = 8;
            //loadput();



        }



        private void txttrans_TextChanged_1(object sender, EventArgs e)
        {

        }

        private void txttrans_KeyDown_1(object sender, KeyEventArgs e)
        {
            //if (txtname.Text == "")
            //{
            //    MessageBox.Show("Select the Party");
            //    return;
            //}
            //Genclass.fieldone = "";
            //Genclass.fieldtwo = "";
            //Genclass.fieldthree = "";
            //Genclass.fieldFour = "";
            //Genclass.fieldFive = "";

            //Genclass.type = 8;
            //loadput();
        }


        private void fun2()
        {
            if (mode == 1)
            {
                txtbval.Text = "0.00";
                Txttot.Text = "0.00";
                Genclass.sum1 = 0;
                Genclass.sum4 = 0;

                for (int i = 0; i < HFIT.Rows.Count - 1; i++)
                {

                    if (HFIT.Rows[i].Cells[12].Value != "" || HFIT.Rows[i].Cells[12].Value != "0.00" || HFIT.Rows[i].Cells[4].Value != "" || HFIT.Rows[i].Cells[4].Value != "0.00")
                    {
                        Genclass.sum1 = Genclass.sum1 + Convert.ToDouble(HFIT.Rows[i].Cells[4].Value);
                        Txttot.Text = Genclass.sum1.ToString();


                        //Genclass.sum4 = Genclass.sum4 + Convert.ToDouble(HFIT.Rows[i].Cells[4].Value);
                        //txtbval.Text = Genclass.sum4.ToString();
                    }

                }
            }
        }

        private void fun3()
        {
            txtbval.Text = "0.00";
            Txttot.Text = "0.00";
            Genclass.sum1 = 0;
            Genclass.sum4 = 0;

            for (int i = 0; i < HFIT.Rows.Count - 1; i++)
            {

                if (HFIT.Rows[i].Cells[12].Value != "" || HFIT.Rows[i].Cells[12].Value != "0.00" || HFIT.Rows[i].Cells[4].Value != "" || HFIT.Rows[i].Cells[4].Value != "0.00")
                {
                    Genclass.sum1 = Genclass.sum1 + Convert.ToDouble(HFIT.Rows[i].Cells[4].Value);
                    Txttot.Text = Genclass.sum1.ToString();



                }

            }


        }

        private void Txttot_TextChanged(object sender, EventArgs e)
        {

            //if (Txttot.Text != "")
            //{
            //    if (Txttot.Text != "0.00")
            //    {
            //        Genclass.sum1 = 0;
            //        Genclass.sum4 = 0;
            //        Txttot.Text = "0";

            //        for (int i = 0; i < HFIT.Rows.Count - 1; i++)
            //        {
            //            if (HFIT.Rows[i].Cells[12].Value != "" || HFIT.Rows[i].Cells[12].Value != "0.00" || HFIT.Rows[i].Cells[4].Value != "" || HFIT.Rows[i].Cells[4].Value != "0.00")
            //            {
            //                Genclass.sum1 = Genclass.sum1 + Convert.ToDouble(HFIT.Rows[i].Cells[12].Value);
            //                Txttot.Text = Genclass.sum1.ToString();


            //                Genclass.sum4 = Genclass.sum4 + Convert.ToDouble(HFIT.Rows[i].Cells[4].Value);
            //                txtbval.Text = Genclass.sum4.ToString();
            //            }
            //        }
            //    }
            //}
        }

        private void txtdcno_TextChanged_1(object sender, EventArgs e)
        {

        }

        private void txtname_Click(object sender, EventArgs e)
        {

            DataTable dt = getParty();
            bsParty.DataSource = dt;
            FillGrid(dt, 1);
            Point loc = FindLocation(txtname);
            grSearch.Location = new Point(loc.X, loc.Y + 20);
            grSearch.Visible = true;
            grSearch.Text = "Party Search";
        }
        protected DataTable getParty()
        {
            DataTable dt = new DataTable();
            try
            {
                dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "SP_getsupplier11", conn);
                bsParty.DataSource = dt;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            return dt;
        }
        private void txttrans_Click(object sender, EventArgs e)
        {
            if (txtname.Text == "")
            {
                MessageBox.Show("Select the Party");
                return;
            }
            //Genclass.fieldone = "";
            //Genclass.fieldtwo = "";
            //Genclass.fieldthree = "";
            //Genclass.fieldFour = "";
            //Genclass.fieldFive = "";

            Genclass.type = 8;
            loadput();

        }
        protected void FillGrid(DataTable dt, int FillId)
        {
            try
            {
                DataGridCommon.DataSource = null;
                DataGridCommon.AutoGenerateColumns = false;
                DataGridCommon.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
                DataGridCommon.DefaultCellStyle.WrapMode = DataGridViewTriState.True;


                DataGridCommon.ColumnCount = 2;
                DataGridCommon.Columns[0].Name = "Uid";
                DataGridCommon.Columns[0].HeaderText = "Uid";
                DataGridCommon.Columns[0].DataPropertyName = "Uid";
                DataGridCommon.Columns[1].Name = "Name";
                DataGridCommon.Columns[1].HeaderText = "Name";
                DataGridCommon.Columns[1].DataPropertyName = "Name";


                DataGridCommon.Columns[1].Width = 350;
                DataGridCommon.DataSource = bsParty;
                DataGridCommon.Columns[0].Visible = false;


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private Point FindLocation(Control ctrl)
        {
            if (ctrl.Parent is Form)
                return ctrl.Location;
            else
            {
                Point p = FindLocation(ctrl.Parent);
                p.X += ctrl.Location.X;
                p.Y += ctrl.Location.Y;
                return p;
            }
        }
        private void txttdis_TextChanged_1(object sender, EventArgs e)
        {


            if (txttdis.Text == "")
            {
                txttdis.Text = "0";
            }
            if (txtcharges.Text == "")
            {
                txttdis.Text = "0";
            }


            if (txttbval.Text != "")
            {


                if (txttdis.Text == "" || txttdis.Text == "0")
                {
                    txttdisc.Text = "0";
                }

                if (txtcharges.Text == "")
                {
                    txtcharges.Text = "0";
                }


                double dis = (Convert.ToDouble(txttbval.Text) / 100) * (Convert.ToDouble(txttdis.Text));
                txttdisc.Text = dis.ToString();

                double yt7 = Convert.ToDouble(txttbval.Text) - Convert.ToDouble(txttdisc.Text);
                txttprdval.Text = yt7.ToString();

                if (txtcharges.Text == "" || txtcharges.Text == null)
                {
                    txtexcise.Text = txttprdval.Text;
                }
                else
                {
                    dis4 = Convert.ToDouble(txttprdval.Text);
                    txtexcise.Text = dis4.ToString("0.00");
                }

                Genclass.sum5 = 0;
                df = 0;
                hg = 0;

                HFGST.Refresh();
                HFGST.DataSource = null;
                HFGST.Rows.Clear();

                txtigval.Text = "0";
                txttaxtot.Text = "0";

                Titlegst();

                for (int l = 0; l < HFIT.RowCount - 1; l++)
                {

                    if (j != l)
                    {

                        dis4 = Convert.ToDouble(HFIT.Rows[l].Cells[7].Value);


                        dis3 = Convert.ToDouble(HFIT.Rows[l].Cells[4].Value) - (Convert.ToDouble(HFIT.Rows[l].Cells[4].Value) / 100) * (Convert.ToDouble(txttdis.Text));
                        dis3 = (dis3 / 100) * dis4;




                        if (txtpluid.Text == "196")
                        {
                            if (mode == 2)
                            {
                                dis4 = Convert.ToDouble(HFIT.Rows[l].Cells[7].Value);
                                dis3 = Convert.ToDouble(HFIT.Rows[l].Cells[8].Value);
                            }
                            dis4 = dis4 / 2;
                            dis3 = dis3 / 2;

                            int boo = 1;
                            if (HFGST.Rows.Count - 1 == 0)
                            {

                                var index1 = HFGST.Rows.Add();
                                HFGST.Rows[index1].Cells[0].Value = dis4.ToString("0.00");
                                HFGST.Rows[index1].Cells[2].Value = dis4.ToString("0.00");
                                HFGST.Rows[index1].Cells[1].Value = dis3.ToString("0.00");
                                HFGST.Rows[index1].Cells[3].Value = dis3.ToString("0.00");
                                HFGST.Rows[index1].Cells[4].Value = Convert.ToDouble(dis3.ToString("0.00")) * 2;
                            }
                            else
                            {
                                for (int k = 0; k < HFGST.Rows.Count - 1; k++)
                                {
                                    if (Convert.ToDouble(HFGST.Rows[k].Cells[0].Value) == dis4)
                                    {

                                        double dg = Convert.ToDouble(HFGST.Rows[k].Cells[1].Value) + dis3;
                                        HFGST.Rows[k].Cells[1].Value = dg.ToString("0.00");

                                        HFGST.Rows[k].Cells[3].Value = dg.ToString("0.00");
                                        double dg1 = Convert.ToDouble(HFGST.Rows[k].Cells[4].Value) + (Convert.ToDouble(dis3.ToString("0.00")) * 2);
                                        HFGST.Rows[k].Cells[4].Value = dg1.ToString("0.00");
                                        boo = 1;
                                        break;
                                    }
                                    else
                                    {
                                        boo = 2;
                                    }
                                }
                            }

                            if (boo == 2)
                            {

                                var index1 = HFGST.Rows.Add();
                                HFGST.Rows[index1].Cells[0].Value = dis4.ToString("0.00");
                                HFGST.Rows[index1].Cells[2].Value = dis4.ToString("0.00");
                                HFGST.Rows[index1].Cells[1].Value = dis3.ToString("0.00");
                                HFGST.Rows[index1].Cells[3].Value = dis3.ToString("0.00");
                                HFGST.Rows[index1].Cells[4].Value = Convert.ToDouble(dis3.ToString("0.00")) * 2;
                            }

                            if (txtigval.Text == "")
                            {
                                txtigval.Text = "0";
                            }
                            df = Convert.ToDouble(txtigval.Text) + (Convert.ToDouble(dis3.ToString("0.00")) * 2);
                            txtigval.Text = df.ToString("0.00");
                            txttaxtot.Text = df.ToString("0.00");
                        }
                        else
                        {
                            if (mode == 2 && dis3 == 0)
                            {
                                dis4 = Convert.ToDouble(HFIT.Rows[l].Cells[7].Value);
                                dis3 = Convert.ToDouble(HFIT.Rows[l].Cells[8].Value);
                            }
                            int boo = 1;
                            if (HFGST.Rows.Count - 1 == 0)
                            {
                                var index1 = HFGST.Rows.Add();
                                HFGST.Rows[index1].Cells[0].Value = dis4.ToString("0.00");
                                HFGST.Rows[index1].Cells[1].Value = dis3.ToString("0.00");
                            }
                            else
                            {
                                for (int k = 0; k < HFGST.Rows.Count - 1; k++)
                                {
                                    if (Convert.ToDouble(HFGST.Rows[k].Cells[0].Value) == dis4)
                                    {

                                        double dg = Convert.ToDouble(HFGST.Rows[k].Cells[1].Value) + dis3;
                                        HFGST.Rows[k].Cells[1].Value = dg.ToString("0.00");


                                        boo = 1;
                                        break;
                                    }
                                    else
                                    {
                                        boo = 2;
                                    }
                                }
                            }

                            if (boo == 2)
                            {

                                var index1 = HFGST.Rows.Add();
                                HFGST.Rows[index1].Cells[0].Value = dis4.ToString("0.00");

                                HFGST.Rows[index1].Cells[1].Value = dis3.ToString("0.00");


                            }
                            if (txtigval.Text == "")
                            {
                                txtigval.Text = "0";
                            }
                            df = df + Convert.ToDouble(dis3.ToString("0.00"));
                            txtigval.Text = df.ToString("0.00");
                            txttaxtot.Text = df.ToString("0.00");
                        }
                    }
                }



                double net1 = Convert.ToDouble(txtexcise.Text) + Convert.ToDouble(txtigval.Text) + Convert.ToDouble(txtcharges.Text);

                txtttot.Text = net1.ToString("0.00");
                TxtNetAmt.Text = net1.ToString("0.00");
                double someInt = (int)net1;

                double rof = Math.Round(net1 - someInt, 2);
                TxtRoff.Text = rof.ToString("0.00");

                if (Convert.ToDouble(TxtRoff.Text) < 0.49)
                {
                    Double rof1 = -1 * Convert.ToDouble(TxtRoff.Text);
                    TxtRoff.Text = rof1.ToString("0.00");
                }
                else
                {
                    Double rof2 = 1 - Convert.ToDouble(TxtRoff.Text);
                    TxtRoff.Text = rof2.ToString("0.00");
                }

                Double net = Convert.ToDouble(TxtNetAmt.Text) + Convert.ToDouble(TxtRoff.Text);
                //int ne=Convert.ToInt16(net);
                TxtNetAmt.Text = net.ToString("0.00");

            }
        }

        private void button13_Click(object sender, EventArgs e)
        {
            if (HFGP.Rows.Count == 1)
            {
                MessageBox.Show("No Record");
                return;
            }

            Genclass.strsql = "SELECT A.UID,D.DOCNO,F.DOCNO as grnno FROM STRANSACTIONSP A INNER JOIN STRANSACTIONSPLIST B ON A.UID=B.TRANSACTIONSPUID AND A.DOCTYPEID=100 AND   B.DOCTYPEID=100 INNER JOIN STRANSACTIONSPLIST C  ON B.REFUID = C.UID  AND C.DOCTYPEID = 90INNER JOIN STRANSACTIONSP D  ON C.TRANSACTIONSPUID = D.UID  AND D.DOCTYPEID = 90 INNER JOIN TRANSACTIONSPLIST E  ON C.UID = E.REFUID  AND E.DOCTYPEID = 10 INNER JOIN TRANSACTIONSP F  ON E.TRANSACTIONSPUID = F.UID  AND F.DOCTYPEID = 10 WHERE A.UID = "+ HFGP.CurrentRow.Cells[0].Value.ToString() + "";              

            Genclass.FSSQLSortStr = "itemname";
        Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
        SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
        DataTable tap = new DataTable();
        aptr.Fill(tap);

            if(tap.Rows.Count  >0)

            {
                for (int mm = 0; mm < tap.Rows.Count; mm++)
                {
                    if (mm == 0)
                    {
                        Genclass.address = tap.Rows[mm]["grnno"].ToString();
                    }
                    else
                    {

                        Genclass.address = Genclass.address + " , " + tap.Rows[mm]["grnno"].ToString();

                    }


                }

            }
            conn.Close();
            conn.Open();
            qur.CommandText = "delete from  getpogrnno";
            qur.ExecuteNonQuery();
            qur.CommandText = "insert into getpogrnno values("+ HFGP.CurrentRow.Cells[0].Value.ToString() + ",'" + tap.Rows[0]["docno"].ToString() + "',('"+ Genclass.address  + "'))";
            qur.ExecuteNonQuery();


            Genclass.cat = 4;
            conn.Close();
            conn.Open();
            qur.CommandText = "delete from  NotoWords";
            qur.ExecuteNonQuery();

            Int64 NumVal = Convert.ToInt64(HFGP.CurrentRow.Cells[9].Value);
            string Nw = Rupees(NumVal);

            qur.CommandText = "Insert into NotoWords values('" + Nw + "',1)";
            qur.ExecuteNonQuery();

            Genclass.Prtid = Convert.ToInt16(HFGP.CurrentRow.Cells[0].Value.ToString());


            Genclass.slno = 1;
            Crviewer crv = new Crviewer();
            crv.Show();



            conn.Close();
        }

        private void txtitemname_Click(object sender, EventArgs e)
        {
            //Genclass.fieldone = "";
            //Genclass.fieldtwo = "";
            //Genclass.fieldthree = "";
            //Genclass.fieldFour = "";
            //Genclass.fieldFive = "";
            //Genclass.fieldSix = "";

            Genclass.type = 5;
            conn.Close();
            conn.Open();
            if (GeneralParameters.UserdId == 1)
            {
                qur.CommandText = "delete from pgbatmp";
                qur.ExecuteNonQuery();
            }
            else
            {
                qur.CommandText = "delete from batmp";
                qur.ExecuteNonQuery();
            }
            loadput1();



        }



        private void txtqty_TextChanged(object sender, EventArgs e)
        {
            if (txtqty.Text != "" && txtprice.Text != "")
            {
                double bval = Convert.ToDouble(txtqty.Text) * Convert.ToDouble(txtprice.Text);
                txtbval.Text = bval.ToString();

                if (Genclass.ty == 3)
                {
                    double bval1 = (bval * Convert.ToDouble(txttgstp.Text)) / 100;
                    txttgstval.Text = bval.ToString();
                }
            }
        }

        private void txtprice_TextChanged_1(object sender, EventArgs e)
        {
            if (txtprice.Text != "" && txtqty.Text != "")
            {
                double add = Convert.ToDouble(txtprice.Text) * Convert.ToDouble(txtqty.Text);
                txtbval.Text = add.ToString("0.000");


            }

        }

        private void button7_Click(object sender, EventArgs e)
        {


            if (txtdcid.Text == "")
            {

                txtdcid.Text = "0";
            }

            Titlep();
            //if (Genclass.ty == 1)
            //{
            if (GeneralParameters.UserdId == 1)
            {
                Genclass.strsql = "select * from pgbatmp";
            }
            else
            {
                Genclass.strsql = "select * from batmp";

            }

            Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
            SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
            DataTable tap1 = new DataTable();
            aptr1.Fill(tap1);

            for (int i = 0; i < tap1.Rows.Count; i++)
            {
                j = -1;
                var index = HFIT.Rows.Add();
                HFIT.Rows[index].Cells[0].Value = tap1.Rows[i]["itemname"].ToString();

                HFIT.Rows[index].Cells[1].Value = tap1.Rows[i]["uom"].ToString();
                HFIT.Rows[index].Cells[2].Value = tap1.Rows[i]["price"].ToString();
                HFIT.Rows[index].Cells[3].Value = tap1.Rows[i]["qty"].ToString();
                HFIT.Rows[index].Cells[4].Value = tap1.Rows[i]["value"].ToString();
                txtbval.Text = tap1.Rows[i]["value"].ToString();
                Genclass.sum1 = Genclass.sum1 + Convert.ToDouble(tap1.Rows[i]["value"].ToString());
                Txttot.Text = Genclass.sum1.ToString();
                HFIT.Rows[index].Cells[5].Value = tap1.Rows[i]["pono"].ToString();
                HFIT.Rows[index].Cells[6].Value = tap1.Rows[i]["itid"].ToString();
                txttgstp.Text = tap1.Rows[i]["gstper"].ToString();
                HFIT.Rows[index].Cells[7].Value = tap1.Rows[i]["gstper"].ToString();
                HFIT.Rows[index].Cells[8].Value = tap1.Rows[i]["gstval"].ToString();
                HFIT.Rows[index].Cells[9].Value = tap1.Rows[i]["invno"].ToString();
                HFIT.Rows[index].Cells[10].Value = tap1.Rows[i]["refid"].ToString();
                dis4 = Convert.ToDouble(tap1.Rows[i]["gstper"].ToString()) / 2;
                dis3 = Convert.ToDouble(tap1.Rows[i]["gstval"].ToString()) / 2;

                Titlegst();
                if (txtpluid.Text == "193")
                {

                    dis4 = Convert.ToDouble(HFIT.Rows[index].Cells[7].Value) / 2;
                    dis3 = Convert.ToDouble(HFIT.Rows[index].Cells[8].Value) / 2;
                }
                else
                {
                    dis4 = Convert.ToDouble(HFIT.Rows[index].Cells[7].Value);
                    dis3 = Convert.ToDouble(HFIT.Rows[index].Cells[8].Value);
                }
                //    if (txtpluid.Text == "24")
                //    {
                //        int boo = 1;
                //        if (HFGST.Rows.Count - 1 == 0)
                //        {

                //            var index1 = HFGST.Rows.Add();
                //            HFGST.Rows[index1].Cells[0].Value = dis4.ToString("0.00");
                //            HFGST.Rows[index1].Cells[2].Value = dis4.ToString("0.00");
                //            HFGST.Rows[index1].Cells[1].Value = dis3.ToString("0.00");
                //            HFGST.Rows[index1].Cells[3].Value = dis3.ToString("0.00");
                //            double fin;
                //            fin = (Convert.ToDouble(dis3.ToString("0.00")) * 2);
                //            HFGST.Rows[index1].Cells[4].Value = fin.ToString("0.00");
                //        }
                //        else
                //        {
                //            for (int k = 0; k < HFGST.Rows.Count - 1; k++)
                //            {
                //                if (Convert.ToDouble(HFGST.Rows[k].Cells[0].Value) == dis4)
                //                {
                //                    //HFGST.Rows[k].Cells[0].Value = dis4.ToString("0.00");
                //                    double dg = Convert.ToDouble(HFGST.Rows[k].Cells[1].Value) + dis3;
                //                    HFGST.Rows[k].Cells[1].Value = dg.ToString("0.00");
                //                    //HFGST.Rows[k].Cells[1].Value = dis3.ToString("0.00");
                //                    HFGST.Rows[k].Cells[3].Value = dg.ToString("0.00");
                //                    double fin;
                //                    fin = (Convert.ToDouble(dis3.ToString("0.00")) * 2);
                //                    HFGST.Rows[k].Cells[4].Value = Convert.ToDouble(HFGST.Rows[k].Cells[4].Value) + Convert.ToDouble(fin.ToString("0.00"));
                //                    boo = 1;
                //                    break;
                //                }
                //                else
                //                {
                //                    boo = 2;
                //                }
                //            }
                //        }

                //        if (boo == 2)
                //        {

                //            var index1 = HFGST.Rows.Add();
                //            HFGST.Rows[index1].Cells[0].Value = dis4.ToString("0.00");
                //            HFGST.Rows[index1].Cells[2].Value = dis4.ToString("0.00");
                //            HFGST.Rows[index1].Cells[1].Value = dis3.ToString("0.00");
                //            HFGST.Rows[index1].Cells[3].Value = dis3.ToString("0.00");
                //            double fin;
                //            fin = (Convert.ToDouble(dis3.ToString("0.00")) * 2);
                //            HFGST.Rows[index1].Cells[4].Value = fin.ToString("0.00");
                //        }

                //        if (txtigval.Text == "")
                //        {
                //            txtigval.Text = "0";
                //        }

                //        double df = Convert.ToDouble(txtigval.Text) + (dis3 * 2);
                //        txtigval.Text = df.ToString("0.00");
                //        Genclass.sum2 = 0;
                //        for (int m = 0; m < HFGST.RowCount - 1; m++)
                //        {
                //            Genclass.sum2 = Genclass.sum2 + Convert.ToDouble(HFGST.Rows[m].Cells[4].Value);
                //            txttaxtot.Text = Genclass.sum2.ToString();
                //        }

                //    }
                //    else
                //    {
                //        int boo = 1;
                //        if (HFGST.Rows.Count - 1 == 0)
                //        {
                //            var index1 = HFGST.Rows.Add();
                //            HFGST.Rows[index1].Cells[0].Value = dis4.ToString("0.00");
                //            HFGST.Rows[index1].Cells[1].Value = dis3.ToString("0.00");
                //        }
                //        else
                //        {
                //            for (int k = 0; k < HFGST.Rows.Count - 1; k++)
                //            {
                //                if (Convert.ToDouble(HFGST.Rows[k].Cells[0].Value) == dis4)
                //                {

                //                    double dg = Convert.ToDouble(HFGST.Rows[k].Cells[1].Value) + dis3;
                //                    HFGST.Rows[k].Cells[1].Value = dg.ToString("0.00");


                //                    boo = 1;
                //                    break;
                //                }
                //                else
                //                {
                //                    boo = 2;
                //                }
                //            }
                //        }

                //        if (boo == 2)
                //        {

                //            var index1 = HFGST.Rows.Add();
                //            HFGST.Rows[index1].Cells[0].Value = dis4.ToString("0.00");

                //            HFGST.Rows[index1].Cells[1].Value = dis3.ToString("0.00");


                //        }
                //        if (txtigval.Text == "")
                //        {
                //            txtigval.Text = "0";
                //        }
                //        df = df + Convert.ToDouble(dis3.ToString("0.00"));
                //        txtigval.Text = df.ToString("0.00");
                //        txttaxtot.Text = df.ToString("0.00");
                //    }
           

            conn.Close();
                conn.Open();
                if (GeneralParameters.UserdId == 1)
                {
                    qur.CommandText = "truncate table  pgbatmp";
                    qur.ExecuteNonQuery();
                }
                else
                {
                    qur.CommandText = "truncate table   batmp";
                    qur.ExecuteNonQuery();

                }

                txttbval.Text = Txttot.Text;
            txttdis_TextChanged_1(sender, e);
            }

            txtitemname.Text = "";
            txtuom.Text = "";
            txtprice.Text = "";
            txtqty.Text = "";
            txtbval.Text = "";
            txtnotes.Text = "";
            txttitemid.Text = "";
            //txttgstp.Text = "";
            //txttgstval.Text = "";
            txtdcid.Text = "";
            //txtgen1.Text = "";
            //dcdate.Text = "";
            conn.Close();
            conn.Open();

            conn.Close();

        }

        private void HFIT_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {

        }



        private void txttitemid_TextChanged(object sender, EventArgs e)
        {
            if (txttitemid.Text != "")
            {
                if (Genclass.ty == 3 || Genclass.ty == 4)
                {
                    Genclass.strsql = "select isnull(c.sname,0) as f1,z.generalname as uom from itemm b  inner join generalm z on b.uom_uid=z.uid inner join itemgroup c on b.itemgroup_uid=c.uid where b.uid=" + txttitemid.Text + "";


                    Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                    SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
                    DataTable tap1 = new DataTable();
                    aptr1.Fill(tap1);

                    if (tap1.Rows.Count > 0)
                    {

                        txttgstp.Text = tap1.Rows[0]["f1"].ToString();
                        //txttgstval.Text = tap1.Rows[0]["taxvalue"].ToString();
                        txtuom.Text = tap1.Rows[0]["uom"].ToString();

                    }
                }

            }
        }

        private void txtgen2_TextChanged(object sender, EventArgs e)
        {

        }

        private void DTPDOCDT_ValueChanged(object sender, EventArgs e)
        {

        }

        private void dtpfnt_ValueChanged(object sender, EventArgs e)
        {
            LoadGetJobCard(1);
            TotalAmount();

        }
        private void TotalAmount()
        {
            conn.Close();
            conn.Open();
            DateTime str9 = Convert.ToDateTime(dtpfnt.Text);
            //if (Genclass.Dtype == 40)
            //{
            //if (chkact.Checked == true)
            //{
            txttotamt.Text = "0";
            txtbasicval.Text = "0";
            txttax.Text = "0";
            Genclass.strsql = "Select  isnull(sum(a.Netvalue),0) as totamt from purchasem a inner join  supplierm b on a.partyuid=b.uid left join  supplierm c on a.placeuid=c.uid where a.active=1 and a.doctypeid=8 and a.companyid=" + GeneralParameters.UserdId + "   and month(docdate)=" + str9.Month + "  and year(docdate)=" + str9.Year + " ";
            //Genclass.strsql = "Select  isnull(sum(a.Netvalue),0) as totamt from stransactionsp a inner join  partym b on a.partyuid=b.uid  left join  partym c on a.placeuid=c.uid left join stransactionsplist d on a.uid=d.transactionspuid where a.active=1 and a.doctypeid=80 and a.companyid=" + GeneralParameters.UserdId + " and  " + Genclass.StrSrch + " and month(docdate)=" + str9.Month + "  and year(docdate)=" + str9.Year + "  group by a.Uid,DocNo,docdate,Dcno,b.Name,Netvalue,igstval,partyuid,a.placeuid,c.name,dtpre,dtrem,transp,vehno,a.remarks";
            Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
            SqlDataAdapter aptr2 = new SqlDataAdapter(Genclass.cmd);
            DataTable tap2 = new DataTable();
            aptr2.Fill(tap2);
            txttotamt.Text = tap2.Rows[0]["totamt"].ToString();


            Genclass.strsql = " select SUM(Basicvalue) as Basicvalue from(select Basicvalue-sum(disval) as Basicvalue from (Select  isnull(SUM(d.basicvalue),0) as Basicvalue,transactionspuid from purchasem a  left join purchased d on a.uid=d.transactionspuid where a.active=1 and a.doctypeid=8 and a.companyid=" + GeneralParameters.UserdId + "  and month(docdate)=" + str9.Month + "  and year(docdate)=" + str9.Year + " group by transactionspuid) aa inner join (select distinct disval,transactionspuid from  purchased where  doctypeid=8)bb on  aa.transactionspuid=bb.transactionspuid group by Basicvalue) tab ";
            Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
            SqlDataAdapter aptr3 = new SqlDataAdapter(Genclass.cmd);
            DataTable tap3 = new DataTable();
            aptr3.Fill(tap3);

            txtbasicval.Text = tap3.Rows[0]["Basicvalue"].ToString();


            //Genclass.strsql = "Select  isnull(d.igstval,0) as Taxval from stransactionsp a inner join  partym b on a.partyuid=b.uid  left join  partym c on a.placeuid=c.uid left join stransactionsplist d on a.uid=d.transactionspuid where a.active=1 and a.doctypeid=80 and a.companyid=" + GeneralParameters.UserdId + " and  " + Genclass.StrSrch + " and month(docdate)=" + str9.Month + "  and year(docdate)=" + str9.Year + "  group by a.Uid,DocNo,docdate,Dcno,b.Name,Netvalue,igstval,partyuid,a.placeuid,c.name,dtpre,dtrem,transp,vehno,a.remarks";
            Genclass.strsql = "select sum(igstval) as Taxval from (select distinct igstval from purchased a inner join purchasem b on a.transactionspuid=b.uid where b.active=1 and a.doctypeid=8 and companyid=" + GeneralParameters.UserdId + "  and month(docdate)=" + str9.Month + "  and year(docdate)=" + str9.Year + ") tab ";

            Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
            SqlDataAdapter aptr4 = new SqlDataAdapter(Genclass.cmd);
            DataTable tap4 = new DataTable();
            aptr4.Fill(tap4);

            txttax.Text = tap4.Rows[0]["Taxval"].ToString();

            Genclass.strsql = "Select  isnull(SUM(e.totalcharges),0) as charges from purchasem a   left join  purchaseCharges e on a.uid=e.puid   where a.active=1 and a.doctypeid=8 and a.companyid=" + GeneralParameters.UserdId + "  and month(docdate)=" + str9.Month + "  and year(docdate)=" + str9.Year + " ";

            Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
            SqlDataAdapter aptr5 = new SqlDataAdapter(Genclass.cmd);
            DataTable tap5 = new DataTable();
            aptr5.Fill(tap5);
            if (tap5.Rows.Count > 0)
            {
                txtchargessum.Text = tap5.Rows[0]["charges"].ToString();
            }

            else
            {
                txtchargessum.Text = "0.00";
            }

            conn.Close();
        }

        private void txtprice_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {

                txtqty.Focus();
            }
        }

        private void txtqty_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {

                txtnotes.Focus();
            }
        }

        private void txtnotes_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                button7.Focus();
            }

        }

        private void Editpan_Paint(object sender, PaintEventArgs e)
        {

        }

        //private void HFIT_CellClick(object sender, DataGridViewCellEventArgs e)
        //{


        //    //DataGridViewCell cell1 = HFIT.CurrentRow.Cells[9];
        //    //if (cell1.ColumnIndex == 9)
        //    //{
        //    //    cell9 = 1;
        //    //    HFIT.CurrentCell = cell1;
        //    //    HFIT.BeginEdit(true);
        //    //}

        //}

        private void txtitemname_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtcharges_TextChanged_1(object sender, EventArgs e)
        {
            if (txtcharges.Text == "")
            {
                txtcharges.Text = "0";
            }
            if (txtigval.Text == "")
            {
                txtigval.Text = "0";
            }
            if (txtexcise.Text != "")
            {
                double tot = 0;
                tot = Convert.ToDouble(txtexcise.Text) + Convert.ToDouble(txtigval.Text);
                txtttot.Text = tot.ToString();

                tot = 0;

                tot = Convert.ToDouble(txtttot.Text) + Convert.ToDouble(txtcharges.Text);
                txtttot.Text = tot.ToString("0.00");

                Double net1 = 0;
                net1 = Convert.ToDouble(txtttot.Text);
                TxtNetAmt.Text = net1.ToString("0.00");
                double someInt = (int)net1;
                double rof = 0;
                rof = Math.Round(net1 - someInt, 2);
                TxtRoff.Text = rof.ToString("0.00");

                if (Convert.ToDouble(TxtRoff.Text) < 0.49)
                {
                    Double rof1 = -1 * Convert.ToDouble(TxtRoff.Text);
                    TxtRoff.Text = rof1.ToString("0.00");
                }
                else
                {
                    Double rof2 = 1 - Convert.ToDouble(TxtRoff.Text);
                    TxtRoff.Text = rof2.ToString("0.00");
                }
                Double net = 0;
                net = Convert.ToDouble(TxtNetAmt.Text) + Convert.ToDouble(TxtRoff.Text);
                //int ne=Convert.ToInt16(net);
                TxtNetAmt.Text = net.ToString("0.00");
            }
        }



        private void HFIT_KeyDown_1(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                j = HFIT.SelectedCells[0].RowIndex;

                double sun = Convert.ToDouble(HFIT.Rows[j].Cells[4].Value.ToString());

                Genclass.sum1 = Convert.ToDouble(Txttot.Text);


                Genclass.sum1 = Genclass.sum1 - sun;

                Txttot.Text = Genclass.sum1.ToString("0.00");
                txttbval.Text = Genclass.sum1.ToString("0.00");
                txttdis_TextChanged_1(sender, e);
            }

        }

        private void HFIT_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void button18_Click(object sender, EventArgs e)
        {
            try
            {
                SelectId = 1;
                int Index = DataGridCommon.SelectedCells[0].RowIndex;

                txtname.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                txtpuid.Text = DataGridCommon.Rows[Index].Cells[0].Value.ToString();

                txtplace.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                //loadgrndet();
                txtgen1.Focus();
                
                loadpono();

                grSearch.Visible = false;
                SelectId = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }


        private void loadpono()
        {

            Genclass.strsql = "select  distinct a.docno as POno,a.uid from purchasem a inner join purchased b on a.UId = b.TransactionsPUId and b.DocTypeID = 7 left join purchased gg on b.uid = gg.refuid   and b.itemuid=gg.itemuid AND gg.doctypeid = 8 left join purchasem hh on gg.transactionspuid = hh.uid  left join itemsm f   on b.ItemUId = f.Uid left join supplierm h on a.partyuid = h.uid     where b.itemname is not null  and h.uid = 1 and a.companyid = 1 group by     a.uid,a.docno,b.pqty,a.DcNo,a.docdate, a.docdate,b.uid,hh.docno,hh.uid having isnull(b.PQty, 0) - isnull(sum(gg.PQty), 0) > 0";
            Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
            SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
            DataTable tap = new DataTable();
            aptr.Fill(tap);

            if (tap.Rows.Count > 0)
            {
                cbopono.DataSource = null;
                cbopono.DataSource = tap;
                cbopono.DisplayMember = "POno";
                cbopono.ValueMember = "uid";

            }
        }
        private void loadgrndet1()
        {

            if (cbopono.Text != ""  && cbopono.SelectedValue!=null && cbopono.ValueMember!="")
            {
                Genclass.strsql1 = "select distinct type from purchasem a inner join purchased b on a.uid=b.TransactionsPUId where a.doctypeid=7 and docno='" + cbopono.Text + "' ";
                Genclass.cmd = new SqlCommand(Genclass.strsql1, conn);
                SqlDataAdapter aptr2 = new SqlDataAdapter(Genclass.cmd);
                DataTable tap2 = new DataTable();
                aptr2.Fill(tap2);

                if (tap2.Rows[0]["type"].ToString() == "Yarn Purchase")
                {
                
                    Genclass.strsql = "insert into PgBatmp  select  distinct b.itemname,d.generalname,b.prate,sum(b.pqty) as qty,convert(decimal(18,2),b.prate*sum(b.pqty),102) as basicvalue,a.docno,b.itemuid as uid,tax.f1,(b.prate * sum(b.pqty)) / 100 * tax.f1 as gstval, '' as invno,b.uid ,isnull(b.bags,0) as bags,isnull(b.style,0) as style,b.socno from purchasem      a inner join purchased b on a.UId = b.TransactionsPUId   and b.DocTypeID = 7   left join purchased mm on b.uid = mm.refuid left join purchasem kk   on kk.UId = mm.TransactionsPUId and kk.DocTypeID = 8     left join OrderMBudjetYarn c  on b.itemuid=c.uid INNER JOIN ORDERM O ON O.UID =c.ORDERMUID and b.refuid=c.ORDERMUID INNER JOIN YARNMASTER Y ON Y.YARNID =c.YARNUID    left join generalm d on y.uom=d.gUid  left JOIN GENERALM TAX ON TAX.GUID =Y.tax    where a.docno = '" + cbopono.Text + "'  and  b.itemname is not null   group by  a.uid,b.pqty,b.itemuid,  b.uid,d.GeneralName,b.PRate,tax.f1,b.itemname,a.docno,b.bags,b.style,b.socno  ";
                    Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                }
                else if (tap2.Rows[0]["type"].ToString() == "Trims Purchase")
                {

                    Genclass.strsql = "insert into PgBatmp select distinct  b.ItemName,tt.GeneralName as uom,PRate,pqty pqty,convert(decimal(18,2),b.prate*b.pqty,102) BasicValue,a.docno,b.itemuid as uid,ff.f1,(b.prate * b.pqty) / 100 * ff.f1 as gstval, '' as invno,b.uid ,isnull(b.bags,0) as bags,isnull(b.style,0) as style,b.socno  from purchasem a inner join purchased b on a.uid=b.transactionspuid     INNER JOIN  OrderMMaterialBom aa on b.refuid=aa.ORDERMUID   INNER JOIN ORDERM O ON O.UID =b.refuid   and aa.ORDERMUID=O.uid inner join GeneralM bb on aa.CtgyId = bb.Guid inner join TrimsM cc on aa.ItemUid = cc.Uid inner join itemsm dd on aa.itemuid=dd.uid  INNER JOIN (SELECT DISTINCT U.GUID,U.GENERALNAME,I.ITEMNAME,I.ItemCategoryUid,I.taxuid  FROM GENERALM U INNER JOIN ITEMSM I ON I.BASEUOMUID = U.GUID) tt on aa.Ctgyid =tt.ItemCategoryUid inner join GeneralM ff on tt.taxuid = ff.Guid       where  a.type='Trims Purchase'  and a.docno='" + cbopono.Text + "' and a.doctypeid=7 group by       a.uid,b.pqty,b.itemuid,  b.uid,tt.GeneralName,b.PRate,ff.f1,b.itemname,a.docno,b.bags,b.style,b.socno    ";
                    Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                }
                SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
                DataTable tap = new DataTable();
                aptr.Fill(tap);



                if (txtdcid.Text == "")
                {

                    txtdcid.Text = "0";
                }

                Titlep();

                if (GeneralParameters.UserdId == 1)
                {
                    Genclass.strsql = "select * from pgbatmp";
                    Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                }
                else
                {
                    Genclass.strsql = "select * from batmp";
                    Genclass.cmd = new SqlCommand(Genclass.strsql, conn);

                }

       
                SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
                DataTable tap1 = new DataTable();
                aptr1.Fill(tap1);

                for (int i = 0; i < tap1.Rows.Count; i++)
                {
                    j = -1;
                    var index = HFIT.Rows.Add();
                    HFIT.Rows[index].Cells[0].Value = tap1.Rows[i]["itemname"].ToString();

                    HFIT.Rows[index].Cells[1].Value = tap1.Rows[i]["uom"].ToString();
                    HFIT.Rows[index].Cells[2].Value = tap1.Rows[i]["price"].ToString();
                    HFIT.Rows[index].Cells[3].Value = tap1.Rows[i]["qty"].ToString();
                    HFIT.Rows[index].Cells[4].Value = tap1.Rows[i]["value"].ToString();
                    txtbval.Text = tap1.Rows[i]["value"].ToString();
                    Genclass.sum1 = Genclass.sum1 + Convert.ToDouble(tap1.Rows[i]["value"].ToString());
                    Txttot.Text = Genclass.sum1.ToString();
                    HFIT.Rows[index].Cells[5].Value = tap1.Rows[i]["pono"].ToString();
                    HFIT.Rows[index].Cells[6].Value = tap1.Rows[i]["itid"].ToString();
                    txttgstp.Text = tap1.Rows[i]["gstper"].ToString();
                    HFIT.Rows[index].Cells[7].Value = tap1.Rows[i]["gstper"].ToString();
                    HFIT.Rows[index].Cells[8].Value = tap1.Rows[i]["gstval"].ToString();
                    HFIT.Rows[index].Cells[9].Value = tap1.Rows[i]["invno"].ToString();
                    HFIT.Rows[index].Cells[10].Value = tap1.Rows[i]["refid"].ToString();
                    HFIT.Rows[index].Cells[16].Value = tap1.Rows[i]["bag"].ToString();
                    HFIT.Rows[index].Cells[17].Value = tap1.Rows[i]["style"].ToString();
                    HFIT.Rows[index].Cells[18].Value = tap1.Rows[i]["addnotes"].ToString();
                    dis4 = Convert.ToDouble(tap1.Rows[i]["gstper"].ToString()) / 2;
                    dis3 = Convert.ToDouble(tap1.Rows[i]["gstval"].ToString()) / 2;

                    Titlegst();
                    if (txtpluid.Text == "196")
                    {

                        dis4 = Convert.ToDouble(HFIT.Rows[index].Cells[7].Value) / 2;
                        dis3 = Convert.ToDouble(HFIT.Rows[index].Cells[8].Value) / 2;
                    }
                    else
                    {
                        dis4 = Convert.ToDouble(HFIT.Rows[index].Cells[7].Value);
                        dis3 = Convert.ToDouble(HFIT.Rows[index].Cells[8].Value);
                    }

                    conn.Close();
                    conn.Open();
                    if (GeneralParameters.UserdId == 1)
                    {
                        qur.CommandText = "truncate table  pgbatmp";
                        qur.ExecuteNonQuery();
                    }



                    txttbval.Text = Txttot.Text;

                    if (txttdis.Text == "")
                    {
                        txttdis.Text = "0";
                    }
                    if (txtcharges.Text == "")
                    {
                        txttdis.Text = "0";
                    }


                    if (txttbval.Text != "")
                    {


                        if (txttdis.Text == "" || txttdis.Text == "0")
                        {
                            txttdisc.Text = "0";
                        }

                        if (txtcharges.Text == "")
                        {
                            txtcharges.Text = "0";
                        }


                        double dis = (Convert.ToDouble(txttbval.Text) / 100) * (Convert.ToDouble(txttdis.Text));
                        txttdisc.Text = dis.ToString();

                        double yt7 = Convert.ToDouble(txttbval.Text) - Convert.ToDouble(txttdisc.Text);
                        txttprdval.Text = yt7.ToString();

                        if (txtcharges.Text == "" || txtcharges.Text == null)
                        {
                            txtexcise.Text = txttprdval.Text;
                        }
                        else
                        {
                            dis4 = Convert.ToDouble(txttprdval.Text);
                            txtexcise.Text = dis4.ToString("0.00");
                        }

                        Genclass.sum5 = 0;
                        df = 0;
                        hg = 0;

                        HFGST.Refresh();
                        HFGST.DataSource = null;
                        HFGST.Rows.Clear();

                        txtigval.Text = "0";
                        txttaxtot.Text = "0";

                        Titlegst();

                        for (int l = 0; l < HFIT.RowCount - 1; l++)
                        {

                            if (j != l)
                            {

                                dis4 = Convert.ToDouble(HFIT.Rows[l].Cells[7].Value);


                                dis3 = Convert.ToDouble(HFIT.Rows[l].Cells[4].Value) - (Convert.ToDouble(HFIT.Rows[l].Cells[4].Value) / 100) * (Convert.ToDouble(txttdis.Text));
                                dis3 = (dis3 / 100) * dis4;




                                if (txtpluid.Text == "196")
                                {
                                    if (mode == 2)
                                    {
                                        dis4 = Convert.ToDouble(HFIT.Rows[l].Cells[7].Value);
                                        dis3 = Convert.ToDouble(HFIT.Rows[l].Cells[8].Value);
                                    }
                                    dis4 = dis4 / 2;
                                    dis3 = dis3 / 2;

                                    int boo = 1;
                                    if (HFGST.Rows.Count - 1 == 0)
                                    {

                                        var index1 = HFGST.Rows.Add();
                                        HFGST.Rows[index1].Cells[0].Value = dis4.ToString("0.00");
                                        HFGST.Rows[index1].Cells[2].Value = dis4.ToString("0.00");
                                        HFGST.Rows[index1].Cells[1].Value = dis3.ToString("0.00");
                                        HFGST.Rows[index1].Cells[3].Value = dis3.ToString("0.00");
                                        HFGST.Rows[index1].Cells[4].Value = Convert.ToDouble(dis3.ToString("0.00")) * 2;
                                    }
                                    else
                                    {
                                        for (int k = 0; k < HFGST.Rows.Count - 1; k++)
                                        {
                                            if (Convert.ToDouble(HFGST.Rows[k].Cells[0].Value) == dis4)
                                            {

                                                double dg = Convert.ToDouble(HFGST.Rows[k].Cells[1].Value) + dis3;
                                                HFGST.Rows[k].Cells[1].Value = dg.ToString("0.00");

                                                HFGST.Rows[k].Cells[3].Value = dg.ToString("0.00");
                                                double dg1 = Convert.ToDouble(HFGST.Rows[k].Cells[4].Value) + (Convert.ToDouble(dis3.ToString("0.00")) * 2);
                                                HFGST.Rows[k].Cells[4].Value = dg1.ToString("0.00");
                                                boo = 1;
                                                break;
                                            }
                                            else
                                            {
                                                boo = 2;
                                            }
                                        }
                                    }

                                    if (boo == 2)
                                    {

                                        var index1 = HFGST.Rows.Add();
                                        HFGST.Rows[index1].Cells[0].Value = dis4.ToString("0.00");
                                        HFGST.Rows[index1].Cells[2].Value = dis4.ToString("0.00");
                                        HFGST.Rows[index1].Cells[1].Value = dis3.ToString("0.00");
                                        HFGST.Rows[index1].Cells[3].Value = dis3.ToString("0.00");
                                        HFGST.Rows[index1].Cells[4].Value = Convert.ToDouble(dis3.ToString("0.00")) * 2;
                                    }

                                    if (txtigval.Text == "")
                                    {
                                        txtigval.Text = "0";
                                    }
                                    df = Convert.ToDouble(txtigval.Text) + (Convert.ToDouble(dis3.ToString("0.00")) * 2);
                                    txtigval.Text = df.ToString("0.00");
                                    txttaxtot.Text = df.ToString("0.00");
                                }
                                else
                                {
                                    if (mode == 2 && dis3 == 0)
                                    {
                                        dis4 = Convert.ToDouble(HFIT.Rows[l].Cells[7].Value);
                                        dis3 = Convert.ToDouble(HFIT.Rows[l].Cells[8].Value);
                                    }
                                    int boo = 1;
                                    if (HFGST.Rows.Count - 1 == 0)
                                    {
                                        var index1 = HFGST.Rows.Add();
                                        HFGST.Rows[index1].Cells[0].Value = dis4.ToString("0.00");
                                        HFGST.Rows[index1].Cells[1].Value = dis3.ToString("0.00");
                                    }
                                    else
                                    {
                                        for (int k = 0; k < HFGST.Rows.Count - 1; k++)
                                        {
                                            if (Convert.ToDouble(HFGST.Rows[k].Cells[0].Value) == dis4)
                                            {

                                                double dg = Convert.ToDouble(HFGST.Rows[k].Cells[1].Value) + dis3;
                                                HFGST.Rows[k].Cells[1].Value = dg.ToString("0.00");


                                                boo = 1;
                                                break;
                                            }
                                            else
                                            {
                                                boo = 2;
                                            }
                                        }
                                    }

                                    if (boo == 2)
                                    {

                                        var index1 = HFGST.Rows.Add();
                                        HFGST.Rows[index1].Cells[0].Value = dis4.ToString("0.00");

                                        HFGST.Rows[index1].Cells[1].Value = dis3.ToString("0.00");


                                    }
                                    if (txtigval.Text == "")
                                    {
                                        txtigval.Text = "0";
                                    }
                                    df = df + Convert.ToDouble(dis3.ToString("0.00"));
                                    txtigval.Text = df.ToString("0.00");
                                    txttaxtot.Text = df.ToString("0.00");
                                }
                            }
                        }



                        double net1 = Convert.ToDouble(txtexcise.Text) + Convert.ToDouble(txtigval.Text) + Convert.ToDouble(txtcharges.Text);

                        txtttot.Text = net1.ToString("0.00");
                        TxtNetAmt.Text = net1.ToString("0.00");
                        double someInt = (int)net1;

                        double rof = Math.Round(net1 - someInt, 2);
                        TxtRoff.Text = rof.ToString("0.00");

                        if (Convert.ToDouble(TxtRoff.Text) < 0.49)
                        {
                            Double rof1 = -1 * Convert.ToDouble(TxtRoff.Text);
                            TxtRoff.Text = rof1.ToString("0.00");
                        }
                        else
                        {
                            Double rof2 = 1 - Convert.ToDouble(TxtRoff.Text);
                            TxtRoff.Text = rof2.ToString("0.00");
                        }

                        Double net = Convert.ToDouble(TxtNetAmt.Text) + Convert.ToDouble(TxtRoff.Text);
                        //int ne=Convert.ToInt16(net);
                        TxtNetAmt.Text = net.ToString("0.00");

                    }
                }
            }

            


        }
        private void loadgrndet()
        {
            Genclass.strsql = "select a.UId,a.docno as PONo,a.DcNo as DcNo,convert(date, a.docdate, 102) as DcDate,a.type from purchasem a inner join purchased b on a.UId = b.TransactionsPUId and b.DocTypeID = 7 left join purchased gg on b.refuid = gg.uid  AND gg.doctypeid = 8 left join purchasem hh on gg.transactionspuid = hh.uid and b.itemuid = gg.itemuid   left join itemsm f on b.ItemUId = f.Uid left join supplierm h on a.partyuid = h.uid     where b.itemname is not null    and h.uid = " + txtpuid.Text + " and a.companyid = 1   and a.docno='" + cbopono.Text + "'    group by   a.uid,a.docno,b.pqty,a.DcNo,a.docdate,a.docdate,b.uid,  hh.docno,a.type having isnull(b.PQty, 0) - isnull(sum(gg.PQty), 0) > 0 ";
            Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
            SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
            DataTable tap = new DataTable();
            aptr.Fill(tap);

            if(tap.Rows.Count>0)
            {
                Genclass.title = tap.Rows[0]["type"].ToString();
                HFGPT.AutoGenerateColumns = false;
                HFGPT.Refresh();
                HFGPT.DataSource = null;
                HFGPT.Rows.Clear();
                HFGPT.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
                HFGPT.DefaultCellStyle.WrapMode = DataGridViewTriState.True;

                HFGPT.ColumnCount = tap.Columns.Count;
                Genclass.i = 0;

                foreach (DataColumn column in tap.Columns)
                {
                    HFGPT.Columns[Genclass.i].Name = column.ColumnName;
                    HFGPT.Columns[Genclass.i].HeaderText = column.ColumnName;
                    HFGPT.Columns[Genclass.i].DataPropertyName = column.ColumnName;
                    Genclass.i = Genclass.i + 1;
                }

                HFGPT.Columns[0].Visible = false;

                HFGPT.Columns[1].Width = 100;
                HFGPT.Columns[2].Width = 100;
          
                HFGPT.Columns[3].Width = 100;

                HFGPT.Columns[4].Width = 100;

                HFGPT.DataSource = tap;

            }


        }
        private void loadgrndetedit()
        {
            Genclass.strsql = "select uid,grnno as pono,dcno,dcdate,pono as Type from billpomap where headid="+ txtgrnid.Text +" ";
            Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
            SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
            DataTable tap = new DataTable();
            aptr.Fill(tap);

            if (tap.Rows.Count > 0)
            {

                HFGPT.AutoGenerateColumns = false;
                HFGPT.Refresh();
                HFGPT.DataSource = null;
                HFGPT.Rows.Clear();


                HFGPT.ColumnCount = tap.Columns.Count;
                Genclass.i = 0;

                foreach (DataColumn column in tap.Columns)
                {
                    HFGPT.Columns[Genclass.i].Name = column.ColumnName;
                    HFGPT.Columns[Genclass.i].HeaderText = column.ColumnName;
                    HFGPT.Columns[Genclass.i].DataPropertyName = column.ColumnName;
                    Genclass.i = Genclass.i + 1;
                }

                HFGPT.Columns[0].Visible = false;

                HFGPT.Columns[1].Width = 100;
                HFGPT.Columns[2].Width = 100;

                HFGPT.Columns[3].Width = 100;

                HFGPT.Columns[4].Width = 100;

                HFGPT.DataSource = tap;

            }


        }
        private void btnHide_Click(object sender, EventArgs e)
        {
            grSearch.Visible = false;
        }

        private void DataGridCommon_DoubleClick(object sender, EventArgs e)
        {
            SelectId = 1;
            int Index = DataGridCommon.SelectedCells[0].RowIndex;

            txtname.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
            txtpuid.Text = DataGridCommon.Rows[Index].Cells[0].Value.ToString();

            txtplace.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
            loadpono();
            txtgen1.Focus();
            grSearch.Visible = false;
            SelectId = 0;
        }

        private void DataGridCommon_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SelectId = 1;
                int Index = DataGridCommon.SelectedCells[0].RowIndex;

                txtname.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                txtpuid.Text = DataGridCommon.Rows[Index].Cells[0].Value.ToString();

                txtplace.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                txtgen1.Focus();
                loadpono();
                grSearch.Visible = false;
                SelectId = 0;
            }
        }

        private void DataGridCommon_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
        protected void LoadWeightrGrid()
        {

            DataGridWeight.DataSource = null;
            DataGridWeight.AutoGenerateColumns = false;
            DataGridWeight.ColumnCount = 10;
            DataGridWeight.Columns[0].Name = "Itemname";
            DataGridWeight.Columns[0].HeaderText = "Itemname";
            DataGridWeight.Columns[0].Width = 250;
            DataGridWeight.Columns[1].Name = "Skuom";
            DataGridWeight.Columns[1].HeaderText = "Skuom";
            DataGridWeight.Columns[1].Width = 80;
            DataGridWeight.Columns[2].Name = "Qty";
            DataGridWeight.Columns[2].HeaderText = "Qty";
            DataGridWeight.Columns[2].Width = 80;
            DataGridWeight.Columns[3].Name = "Length";
            DataGridWeight.Columns[3].HeaderText = "Length";
            DataGridWeight.Columns[3].Width = 80;
            DataGridWeight.Columns[4].Name = "Width";
            DataGridWeight.Columns[4].HeaderText = "Width";
            DataGridWeight.Columns[4].DefaultCellStyle.Format = "N4";
            DataGridWeight.Columns[4].Width = 80;
            DataGridWeight.Columns[5].Name = "POqty";
            DataGridWeight.Columns[5].HeaderText = "POqty";
            DataGridWeight.Columns[5].Width = 80;
            DataGridWeight.Columns[6].Name = "POuom";
            DataGridWeight.Columns[6].HeaderText = "POuom";
            DataGridWeight.Columns[6].Width = 80;

            DataGridWeight.Columns[7].Name = "Itemuid";
            DataGridWeight.Columns[7].HeaderText = "Itemuid";
            DataGridWeight.Columns[7].Visible = false;
            DataGridWeight.Columns[8].Name = "skuomid";
            DataGridWeight.Columns[8].HeaderText = "skuomid";
            DataGridWeight.Columns[8].Visible = false;
            DataGridWeight.Columns[9].Name = "puomid";
            DataGridWeight.Columns[9].HeaderText = "puomid";
            DataGridWeight.Columns[9].Visible = false;




        }
        private void button9_Click_1(object sender, EventArgs e)
        {
            mappnl.Visible = true;
            Genclass.ClearTextBox(this, mappnl);
            DataGridWeight.Refresh();
            DataGridWeight.DataSource = null;
            DataGridWeight.Rows.Clear();
            LoadWeightrGrid();
            Docno.Clear();
            comboload();


            if (mode == 2)
            {
                dataload1();



            }
            button9.Visible = false;
            button10.Visible = false;
            btnsave.Visible = false;
            buttnfinbk.Visible = false;
        }
        private void dataload1()
        {

            if (mappnl.Visible == true && cbosGReturnItem.ValueMember != string.Empty && cbosGReturnItem.SelectedValue != null)
            {

                DataGridWeight.Refresh();
                DataGridWeight.DataSource = null;
                DataGridWeight.Rows.Clear();
                LoadWeightrGrid();

                if (mode == 1)
                {
                    Genclass.strsql = "select a.itemname,c.generalname as SKUoM,qty,isnull(gstval,0) as  length,isnull(gstper,0)  width,isnull(price,0) as poqty,d.generalname as POUom,itid as Itemuid,c.uid as skuomid,d.uid as PUomid  from Batemp a inner  join itemm b on a.itid = b.uid  inner  join generalm c on b.uom_uid = c.uid  left  join generalm d on b.puomid = d.uid   where a.itid=" + cbosGReturnItem.SelectedValue + " ";
                    Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                }
                else

                {

                    Genclass.strsql = "select b.itemname,c.generalname as SKUoM,stkqty as qty,isnull(stklength,0) as  length,isnull(stkwidth,0) as  width,poqty  as poqty,d.generalname as POUom,a.Itemuid,c.uid as skuomid,d.uid as PUomid  from StransactionspListStk a  inner join stransactionsplist f on a.stlrefuid=f.uid inner join  stransactionsp g on f.transactionspuid=g.uid inner join itemm b on a.Itemid = b.uid  inner join generalm c on b.uom_uid = c.uid  left join generalm d on b.puomid = d.uid   where a.itemuid=" + cbosGReturnItem.SelectedValue + "  and g.docno='" + txtgrn.Text + "' union all select a.itemname,c.generalname as SKUoM,qty,isnull(gstval,0) as  length,isnull(gstper,0)  width,isnull(price,0) as poqty,d.generalname as POUom,itid as Itemuid,c.uid as skuomid,d.uid as PUomid  from Batemp a inner  join itemm b on a.itid = b.uid  inner  join generalm c on b.uom_uid = c.uid  left  join generalm d on b.puomid = d.uid   where a.itid=" + cbosGReturnItem.SelectedValue + "";
                    Genclass.cmd = new SqlCommand(Genclass.strsql, conn);

                }
                SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
                DataTable tap1 = new DataTable();
                aptr1.Fill(tap1);

                Genclass.sum1 = 0;

                Txttot.Text = "";

                for (int m = 0; m < tap1.Rows.Count; m++)
                {
                    var index = DataGridWeight.Rows.Add();

                    DataGridWeight.Rows[index].Cells[0].Value = tap1.Rows[m]["itemname"].ToString();

                    DataGridWeight.Rows[index].Cells[1].Value = tap1.Rows[m]["SKUoM"].ToString();

                    DataGridWeight.Rows[index].Cells[2].Value = tap1.Rows[m]["qty"].ToString();

                    DataGridWeight.Rows[index].Cells[3].Value = tap1.Rows[m]["length"].ToString();

                    DataGridWeight.Rows[index].Cells[4].Value = tap1.Rows[m]["width"].ToString();

                    DataGridWeight.Rows[index].Cells[5].Value = tap1.Rows[m]["poqty"].ToString();

                    DataGridWeight.Rows[index].Cells[6].Value = tap1.Rows[m]["POUom"].ToString();

                    DataGridWeight.Rows[index].Cells[7].Value = tap1.Rows[m]["Itemuid"].ToString();

                    DataGridWeight.Rows[index].Cells[8].Value = tap1.Rows[m]["skuomid"].ToString();

                    DataGridWeight.Rows[index].Cells[9].Value = tap1.Rows[m]["PUomid"].ToString();


                }
                LoadWeightrGrid();
            }
        }
        private void cbosGReturnItem_SelectedIndexChanged(object sender, EventArgs e)
        {

            for (int k = 0; k < HFIT.Rows.Count - 1; k++)
            {
                if (HFIT.Rows[k].Cells[0].Value.ToString() == cbosGReturnItem.Text)
                {
                    txtqty1.Text = Convert.ToString(HFIT.Rows[k].Cells[3].Value);

                    txtbags.Text = Convert.ToString(HFIT.Rows[k].Cells[1].Value);

                }

            }
            dataload1();
        }

        private void button19_Click(object sender, EventArgs e)
        {

            mappnl.Visible = false;
            button9.Visible = true;
            button10.Visible = true;
            btnsave.Visible = true;
            buttnfinbk.Visible = true;

        }

        private void button10_Click_3(object sender, EventArgs e)
        {
            serialno.Visible = true;
            Genclass.ClearTextBox(this, serialno);
            Dataserial.RowHeadersVisible = false;
            Dataserial.Refresh();
            Dataserial.DataSource = null;
            Dataserial.Rows.Clear();
            Loadser();
            loadserial();

           


            if (mode == 2)
            {
                dataload();
                



            }
            button9.Visible = false;
            button10.Visible = false;
            btnsave.Visible = false;
            buttnfinbk.Visible = false;
        }


        private void dataload()
        {

            if (serialno.Visible == true && cboserial.ValueMember != string.Empty && cboserial.SelectedValue != null)
            {

                Dataserial.Refresh();
                Dataserial.DataSource = null;
                Dataserial.Rows.Clear();
                Loadser();
                if (mode == 1)
                {
                    Genclass.strsql = "select  itemuid,QTY,serialno from serialtmp  where itemuid=" + cboserial.SelectedValue + " ";
                    Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                }
                else

                {
                    Genclass.strsql = "select  b.itemuid,QTY,serialno from StransactionspListStkSerial  a inner join  StransactionspListStk b on b.uid=a.stlstkrefuid inner join stransactionsplist c on b.stlrefuid=c.uid  inner join stransactionsp g on c.transactionspuid=g.uid  where b.itemuid=" + cboserial.SelectedValue + "  and g.docno='" + txtgrn.Text + "'  UNION ALL select   itemuid,QTY,serialno from serialtmp  where itemuid=" + cboserial.SelectedValue + "";
                    Genclass.cmd = new SqlCommand(Genclass.strsql, conn);

                }
                SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
                DataTable tap1 = new DataTable();
                aptr1.Fill(tap1);

                Genclass.sum1 = 0;

                Txttot.Text = "";

                for (int m = 0; m < tap1.Rows.Count; m++)
                {
                    var index = Dataserial.Rows.Add();

                    Dataserial.Rows[index].Cells[0].Value = tap1.Rows[m]["itemuid"].ToString();

                    Dataserial.Rows[index].Cells[1].Value = tap1.Rows[m]["QTY"].ToString();

                    Dataserial.Rows[index].Cells[2].Value = tap1.Rows[m]["serialno"].ToString();



                }
                Loadser();
            }
        }
        private void Loadseriallist()
        {


        }

        protected void Loadser()
        {

            Dataserial.DataSource = null;
            Dataserial.AutoGenerateColumns = false;
            Dataserial.ColumnCount = 3;
            Dataserial.Columns[0].Name = "itemuid";
            Dataserial.Columns[0].HeaderText = "itemuid";
            Dataserial.Columns[0].Visible = false;
            Dataserial.Columns[1].Name = "Qty";
            Dataserial.Columns[1].HeaderText = "Qty";
            Dataserial.Columns[1].Width = 100;
            Dataserial.Columns[2].Name = "batchno";
            Dataserial.Columns[2].HeaderText = "batchno";
            Dataserial.Columns[2].Width = 150;
        }
        private void textBox11_TextChanged(object sender, EventArgs e)
        {

        }

        private void button20_Click(object sender, EventArgs e)
        {

            for (int k = 0; k < HFIT.Rows.Count - 1; k++)
            {



                Genclass.strsql1 = "select * from itemm   where uid =" + HFIT.Rows[k].Cells[6].Value + "   and   serilze=1";
                Genclass.cmd = new SqlCommand(Genclass.strsql1, conn);
                SqlDataAdapter aptr2 = new SqlDataAdapter(Genclass.cmd);
                DataTable tap2 = new DataTable();
                aptr2.Fill(tap2);
                if (tap2.Rows.Count > 0)
                {

                    Genclass.strsql = "select serialno from serialtmp   where itemuid=" + HFIT.Rows[k].Cells[6].Value + "  union all  select serialno    from StransactionspListStkSerial  a inner join  StransactionspListStk b on b.uid=a.stlstkrefuid inner join stransactionsplist c on b.stlrefuid=c.uid  inner join stransactionsp g on c.transactionspuid=g.uid  where b.itemuid=" + HFIT.Rows[k].Cells[6].Value + "  and g.docno='" + txtgrn.Text + "' ";
                    Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                    SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
                    DataTable tap1 = new DataTable();
                    aptr1.Fill(tap1);
                    if (tap1.Rows.Count > 0)
                    {
                        double ui = Convert.ToDouble(tap1.Rows.Count);

                        if (Convert.ToDouble(HFIT.Rows[k].Cells[3].Value) != ui)
                        {

                            MessageBox.Show("Complete the mapping item : " + HFIT.Rows[k].Cells[0].Value + "");
                            return;

                        }

                    }
                }

            }

            serialno.Visible = false;
            button9.Visible = true;
            button10.Visible = true;
            btnsave.Visible = true;
            buttnfinbk.Visible = true;
        }

        private void button23_Click(object sender, EventArgs e)
        {
           
            conn.Close();
            conn.Open();

            Genclass.strsql = "select  b.itemuid,QTY,serialno from StransactionspListStkSerial  a inner join  StransactionspListStk b on b.uid=a.stlstkrefuid inner join stransactionsplist c on b.stlrefuid = c.uid  inner join stransactionsp g on c.transactionspuid = g.uid where b.itemid = "+ cboserial.SelectedValue + "  and g.yearid = "+ Genclass.Yearid +" group by  b.itemuid,QTY,serialno having count(serialno) > 1 ";
            Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
            SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
            DataTable tap1 = new DataTable();
            aptr1.Fill(tap1);
            if (tap1.Rows.Count > 0)
            {

                MessageBox.Show("serialNo  already Exist  this item");
                return;
                               

            }   

            txtserialqty.Text = "1";
            txtgrossgen.Text = Convert.ToString(cboserial.SelectedValue);
            for (int i = 0; i < Dataserial.Rows.Count - 1; i++)
            {
                if (txtgrossgen.Text == Dataserial.Rows[i].Cells[0].Value.ToString() && txtsrail.Text == Dataserial.Rows[i].Cells[2].Value.ToString())
                {

                    MessageBox.Show("serial for this item already Exist");
                    txtserialqty.Text = "";
                    txtsrail.Text = "";
                    return;
                }
            }

            loadsum = loadsum + 1;
            double sp = loadsum;
            double op = Convert.ToDouble(textBox5.Text);
            Genclass.sum3 = 0;
            for (int p = 0; p < Dataserial.RowCount - 1; p++)
            {
                Genclass.sum3 = Genclass.sum3 + Convert.ToDouble(Dataserial.Rows[p].Cells[1].Value.ToString());
                double IOP = Genclass.sum3;
                if (op == IOP)
                {

                    MessageBox.Show("serial no already mapped");
                    loadsum = 0;
                    return;

                }


            }
            if (op != sp || op < sp)
            {



                int d = 1;
                SqlParameter[] para ={
                    new SqlParameter("@serialno",txtsrail.Text),
                    new SqlParameter("@QTY",d),
                    new SqlParameter("@ITEMUID",cboserial.SelectedValue),


                };
                db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_SERIALTEMP", para, conn);
                //if (op ==sp)
                //{
                //    loadsum = 0;

                //}
            }
            else if (op == sp)
            {
                int d = 1;
                SqlParameter[] para ={
                    new SqlParameter("@serialno",txtsrail.Text),
                    new SqlParameter("@QTY",d),
                    new SqlParameter("@ITEMUID",cboserial.SelectedValue),


                };
                db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_SERIALTEMP", para, conn);
                loadsum = 0;

            }
            else

            {
                MessageBox.Show("serial no already mapped");
                loadsum = 0;
                return;

            }
            dataload();
            txtserialqty.Text = "";
            txtsrail.Text = "";
        }

        private void cboserial_SelectedIndexChanged(object sender, EventArgs e)
        {


            for (int k = 0; k < HFIT.Rows.Count - 1; k++)
            {
                if (HFIT.Rows[k].Cells[0].Value.ToString() == cboserial.Text)
                {
                    textBox5.Text = Convert.ToString(HFIT.Rows[k].Cells[3].Value);

                    textBox6.Text = Convert.ToString(HFIT.Rows[k].Cells[1].Value);

                }

            }
            dataload();
        }

        private void DataGridWeight_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void DataGridWeight_CellClick(object sender, DataGridViewCellEventArgs e)
        {
           
                if (DataGridWeight.CurrentRow.Cells[0].Value.ToString() != "" && DataGridWeight.CurrentRow.Cells[0].Value.ToString() != null)
                {
                    if (DataGridWeight.CurrentCell.ColumnIndex == 5)

                    {
                        DataGridViewCell cell = DataGridWeight.CurrentRow.Cells[5];
                        DataGridWeight.CurrentCell = cell;
                        DataGridWeight.BeginEdit(true);
                    }

                }

            
        }

        private void DataGridWeight_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            for (int k = 0; k < DataGridWeight.Rows.Count - 1; k++)
            {



                if (DataGridWeight.Rows[k].Cells[5].Value == "" || DataGridWeight.Rows[k].Cells[5].Value == null)
                {
                    return;

                }
                else
                {

                    if (DataGridWeight.Rows[k].Cells[5].Value.ToString() != "0.000" && DataGridWeight.CurrentCell.ColumnIndex == 5)
                    {
                        conn.Close();
                        conn.Open();
                        if (mode == 1)
                        {
                            qur.CommandText = "update Batemp  set qty=" + DataGridWeight.Rows[k].Cells[5].Value.ToString() + " where itid=" + DataGridWeight.Rows[k].Cells[7].Value.ToString() + "";
                            qur.ExecuteNonQuery();
                        }
                        else

                        {
                            qur.CommandText = "update StransactionspListStk  set poqty=" + DataGridWeight.Rows[k].Cells[5].Value.ToString() + " where itemuid=" + DataGridWeight.Rows[k].Cells[7].Value.ToString() + "";
                            qur.ExecuteNonQuery();

                        }

                        dataload1();
                    }

                }




            }
        }

        private void textBox16_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox14_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox15_TextChanged(object sender, EventArgs e)
        {

        }

        private void serialno_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (txtBagNo.Text == "")
            {
                MessageBox.Show("Enter the itemname");
                txtBagNo.Focus();
                return;
            }

            if (txtNoogBags.Text == "")
            {
                MessageBox.Show("Enter the po qty");
                txtNoogBags.Focus();
                return;
            }


            if (txtstart.Text == "")
            {
                MessageBox.Show("Enter the qty");
                txtstart.Focus();
                return;
            }


            if (txtWeight.Text == "")
            {
                MessageBox.Show("Enter the width");
                txtWeight.Focus();
                return;
            }


            if (txtGrossWght.Text == "")
            {
                MessageBox.Show("Enter the length");
                txtGrossWght.Focus();
                return;
            }

            conn.Close();
            conn.Open();


            SqlParameter[] para ={
                    new SqlParameter("@ITEMNAME",txtBagNo.Text),
                    new SqlParameter("@PRICE",txtNoogBags.Text),
                    new SqlParameter("@QTY",txtstart.Text),
                    new SqlParameter("@ITID",cbosGReturnItem.SelectedValue),
                    new SqlParameter("@width",txtWeight.Text),
                    new SqlParameter("@length",txtGrossWght.Text),
                    new SqlParameter("@refid",txtgrossgen.Text)
              };
            db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_BILLUOMCONTEMP", para, conn);

            dataload1();
            txtBagNo.Text = "";
            txtNoogBags.Text = "";
            txtstart.Text = "";
            txtgrossgen.Text = "";
            txtGrossWght.Text = "";
            txtWeight.Text = "";
        }

        private void txttarewt1_Click(object sender, EventArgs e)
        {
            Genclass.type = 5;
            loadput();
        }

        private void txtBagNo_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtBagNo_Click(object sender, EventArgs e)
        {
            //Genclass.fieldone = "";
            //Genclass.fieldtwo = "";
            //Genclass.fieldthree = "";
            //Genclass.fieldFour = "";
            //Genclass.fieldFive = "";
            //Genclass.fieldSix = "";
            Genclass.Dtype = 5;
            loadput();
        }

        private void txtsrail_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                button23_Click(sender, e);
            }
        }

        private void txtsrail_TextChanged(object sender, EventArgs e)
        {

        }

        private void mappnl_Paint(object sender, PaintEventArgs e)
        {

        }

        private void Dataserial_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void Dataserial_KeyDown(object sender, KeyEventArgs e)
        {
            conn.Close();
            conn.Open();

            if (mode == 2)
            {
                Genclass.strsql = "select * from StransactionspListStkSerial where serialno='" + Dataserial.CurrentRow.Cells[2].Value.ToString() + "'  and refno='" + txtgrn.Text + "'";
                Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                SqlDataAdapter aptr4 = new SqlDataAdapter(Genclass.cmd);
                DataTable tap4 = new DataTable();
                aptr4.Fill(tap4);
                if (tap4.Rows.Count > 0)
                {
                    qur.CommandText = "delete from StransactionspListStkSerial where uid=" + tap4.Rows[0]["uid"].ToString() + " ";
                    qur.ExecuteNonQuery();

                }
                else

                {

                    qur.CommandText = "delete from serialtmp where serial='" + Dataserial.CurrentRow.Cells[2].Value.ToString() + "'  and itemuid=" + Dataserial.CurrentRow.Cells[0].Value.ToString() + "";
                    qur.ExecuteNonQuery();

                }

            }
            else

            {

                Genclass.strsql = "select * from serialtmp where serial='" + Dataserial.CurrentRow.Cells[2].Value.ToString() + "'  and itemuid='" + Dataserial.CurrentRow.Cells[0].Value.ToString() + "'";
                Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                SqlDataAdapter aptr4 = new SqlDataAdapter(Genclass.cmd);
                DataTable tap4 = new DataTable();
                aptr4.Fill(tap4);
                if (tap4.Rows.Count > 0)
                {
                    qur.CommandText = "delete from serialtmp where serial='" + Dataserial.CurrentRow.Cells[2].Value.ToString() + "' ";
                    qur.ExecuteNonQuery();

                }

            }
            dataload();

        }

        ////private void Dataserial_CellContentClick_1(object sender, DataGridViewCellEventArgs e)
        ////{

        ////}

        private void Dataserial_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            
        }

        private void Dataserial_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {




            //if (Dataserial.CurrentRow.Cells[2].Value == "" || Dataserial.CurrentRow.Cells[2].Value == null)
            //{
            //    return;

            //}
            //else
            //{

            //    if (Dataserial.CurrentRow.Cells[0].Value.ToString() != "" && Dataserial.CurrentCell.ColumnIndex == 2)
            //    {

            //        conn.Close();
            //        conn.Open();
            //        if (mode == 1)
            //        {
            //            qur.CommandText = "update serialtmp  set serialno=" + Dataserial.CurrentRow.Cells[2].Value.ToString() + " where itemuid=" + Dataserial.CurrentRow.Cells[0].Value.ToString() + "";
            //            qur.ExecuteNonQuery();
            //        }
            //        else if (mode == 2)

            //        {
            //            Genclass.strsql = "select a.uid    from StransactionspListStkSerial a inner join StransactionspListStk b on b.uid = a.stlstkrefuid inner join stransactionsplist c on b.stlrefuid = c.uid  inner join stransactionsp g on c.transactionspuid = g.uid  where  g.docno = '" + txtgrn.Text + "' and b.itemuid=" + Dataserial.CurrentRow.Cells[0].Value.ToString() + "";
            //            Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
            //            SqlDataAdapter aptr4 = new SqlDataAdapter(Genclass.cmd);
            //            DataTable tap4 = new DataTable();
            //            aptr4.Fill(tap4);
            //            if (tap4.Rows.Count > 0)
            //            {


            //                qur.CommandText = "update StransactionspListStkSerial  set serialno=" + Dataserial.CurrentRow.Cells[2].Value.ToString() + " where uid=" + tap4.Rows[0]["uid"].ToString() + "";
            //                qur.ExecuteNonQuery();

            //            }

            //        }
            //        dataload();



            //    }


            //}

        }

        private void Dataserial_CellContentClick_1(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void Dataserial_KeyDown_1(object sender, KeyEventArgs e)
        {
            Dataserial_KeyDown(sender, e);
        }

        private void Dataserial_CellClick_1(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void Dataserial_CellValueChanged_1(object sender, DataGridViewCellEventArgs e)
        {
            Dataserial_CellValueChanged(sender, e);
        }

        private void Dataserial_CellContentClick_2(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void TxtRoff_TextChanged_1(object sender, EventArgs e)
        {
            if (TxtRoff.Text != "" && TxtRoff.Text != "0.00"  && TxtRoff.Text != "-" && TxtRoff.Text != "=" && TxtRoff.Text != "+")
            {
                Double net = Convert.ToDouble(txtttot.Text) + Convert.ToDouble(TxtRoff.Text);
                //int ne=Convert.ToInt16(net);
                TxtNetAmt.Text = net.ToString("0.00");
            }
        }

        private void panadd_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button21_Click(object sender, EventArgs e)
        {
            string message = "Are you sure to cancel this Bill ?";
            string caption = "Dilama";
            MessageBoxButtons buttons = MessageBoxButtons.YesNo;
            DialogResult result;

            result = MessageBox.Show(message, caption, buttons);

            if (result == System.Windows.Forms.DialogResult.Yes)
            {

                int i = HFGP.SelectedCells[0].RowIndex;
                uid = HFGP.Rows[i].Cells[0].Value.ToString();
                conn.Close();
                conn.Open();


                qur.CommandText = "delete  from stransactionsplist  where transactionspuid=" + uid + "";
                qur.ExecuteNonQuery();
                qur.CommandText = "delete  from stransactionsp  where uid=" + uid + "";
                qur.ExecuteNonQuery();

                MessageBox.Show("Bill Deleted");
            }
            LoadGetJobCard(1);
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void txtrem_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtpadd1_TextChanged(object sender, EventArgs e)
        {

        }

        private void cbopono_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbopono.ValueMember != "" && cbopono.DisplayMember !="")
            {
                loadgrndet();
                Titlep();
                loadgrndet1();
            }
        }

        private void DTPDOCDT_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                e.Handled = true;
                SendKeys.Send("{TAB}");
            }

        }

        private void txtgen1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                e.Handled = true;
                SendKeys.Send("{TAB}");
            }
        }

        private void dcdate_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                e.Handled = true;
                SendKeys.Send("{TAB}");
            }
        }

        private void txtrem_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                e.Handled = true;
                SendKeys.Send("{TAB}");
            }
        }
    }
}
 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    