﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace MyEasyBizAPMS
{
    public partial class FrmLogin : Form
    {
        public FrmLogin()
        {
            InitializeComponent();
        }
        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnStr"].ConnectionString);
        DataTable dtYear = new DataTable();
        private void BtnLogin_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtUserName.Text == string.Empty && txtPassword.Text == string.Empty)
                {
                    MessageBox.Show("Enter user name and Password", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtUserName.Focus();
                    return;
                }
                else if (txtUserName.Text == string.Empty)
                {
                    MessageBox.Show("Enter user name", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtUserName.Focus();
                    return;
                }
                else if (txtPassword.Text == string.Empty)
                {
                    MessageBox.Show("Enter Password", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtPassword.Focus();
                    return;
                }
                else
                {
                    DataTable dataTable = dtYear.Select("Yrid=" + cmbYear.SelectedValue + "", string.Empty).CopyToDataTable();
                    GeneralParameters.ServerName = dataTable.Rows[0]["DBServer"].ToString();
                    GeneralParameters.DbName = dataTable.Rows[0]["DBName"].ToString();
                    GeneralParameters.UserName = dataTable.Rows[0]["DBUserName"].ToString();
                    GeneralParameters.Password = dataTable.Rows[0]["DBPassword"].ToString();
                    string ConnectionString = "Data Source = '" + GeneralParameters.ServerName + "'; Initial Catalog = '" + GeneralParameters.DbName + "'; User ID = '" + GeneralParameters.UserName + "'; Password = '" + GeneralParameters.Password + "';";
                    GeneralParameters.ConnectionString = ConnectionString;
                    SqlConnection conn = new SqlConnection(ConnectionString);
                    SQLDBHelper db = new SQLDBHelper();
                    SqlParameter[] para = {
                        new SqlParameter("@UserName",txtUserName.Text),
                        new SqlParameter("@Password",txtPassword.Text)
                    };
                    DataTable dt = db.GetDataWithParam(CommandType.StoredProcedure, "SP_UserLogin", para, conn);
                    if (dt.Rows.Count == 0)
                    {
                        MessageBox.Show("Enter the correct user name and Password", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        txtUserName.Focus();
                        return;
                    }
                    else
                    {
                        GeneralParameters.UserdId = Convert.ToInt32(dt.Rows[0]["Uid"].ToString());
                        GeneralParameters.LoginUserName = dt.Rows[0]["UserName"].ToString();
                        GeneralParameters.dtCompany = db.GetDataWithoutParam(CommandType.StoredProcedure, "SP_GetCompany", conn);
                        this.Hide();
                        FrmMain main = new FrmMain();
                        main.Show();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void FrmLogin_Load(object sender, EventArgs e)
        {
            LoadYear();
        }

        protected void LoadYear()
        {
            GeneralParameters.CompanyId = ConfigurationManager.AppSettings["CompanyId"];
            SQLDBHelper db = new SQLDBHelper();
            SqlParameter[] sqlParameters = { new SqlParameter("@CompnayId", GeneralParameters.CompanyId) };
            dtYear = db.GetDataWithParam(CommandType.StoredProcedure, "Proc_GetYear", sqlParameters, conn);
            cmbYear.DataSource = null;
            cmbYear.DisplayMember = "YrName";
            cmbYear.ValueMember = "Yrid";
            cmbYear.DataSource = dtYear;
        }

        private void TxtUserName_TextChanged(object sender, EventArgs e)
        {

        }

        private void CmbYear_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
