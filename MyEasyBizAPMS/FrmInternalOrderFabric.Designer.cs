﻿namespace MyEasyBizAPMS
{
    partial class FrmInternalOrderFabric
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grBack = new System.Windows.Forms.GroupBox();
            this.tabControlAdv1 = new Syncfusion.Windows.Forms.Tools.TabControlAdv();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.tabFabric = new Syncfusion.Windows.Forms.Tools.TabPageAdv();
            this.tabYarn = new Syncfusion.Windows.Forms.Tools.TabPageAdv();
            this.tabYarnProcess = new Syncfusion.Windows.Forms.Tools.TabPageAdv();
            this.tabFabricProcess = new Syncfusion.Windows.Forms.Tools.TabPageAdv();
            this.grBack.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tabControlAdv1)).BeginInit();
            this.tabControlAdv1.SuspendLayout();
            this.SuspendLayout();
            // 
            // grBack
            // 
            this.grBack.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.grBack.Controls.Add(this.dateTimePicker1);
            this.grBack.Controls.Add(this.textBox1);
            this.grBack.Controls.Add(this.label2);
            this.grBack.Controls.Add(this.label1);
            this.grBack.Controls.Add(this.tabControlAdv1);
            this.grBack.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grBack.Location = new System.Drawing.Point(12, 6);
            this.grBack.Name = "grBack";
            this.grBack.Size = new System.Drawing.Size(776, 477);
            this.grBack.TabIndex = 0;
            this.grBack.TabStop = false;
            // 
            // tabControlAdv1
            // 
            this.tabControlAdv1.ActiveTabForeColor = System.Drawing.Color.Empty;
            this.tabControlAdv1.BackColor = System.Drawing.SystemColors.Control;
            this.tabControlAdv1.BeforeTouchSize = new System.Drawing.Size(761, 401);
            this.tabControlAdv1.CloseButtonForeColor = System.Drawing.Color.Empty;
            this.tabControlAdv1.CloseButtonHoverForeColor = System.Drawing.Color.Empty;
            this.tabControlAdv1.CloseButtonPressedForeColor = System.Drawing.Color.Empty;
            this.tabControlAdv1.Controls.Add(this.tabFabric);
            this.tabControlAdv1.Controls.Add(this.tabYarn);
            this.tabControlAdv1.Controls.Add(this.tabYarnProcess);
            this.tabControlAdv1.Controls.Add(this.tabFabricProcess);
            this.tabControlAdv1.InActiveTabForeColor = System.Drawing.Color.Empty;
            this.tabControlAdv1.Location = new System.Drawing.Point(9, 70);
            this.tabControlAdv1.Name = "tabControlAdv1";
            this.tabControlAdv1.SeparatorColor = System.Drawing.SystemColors.ControlDark;
            this.tabControlAdv1.ShowSeparator = false;
            this.tabControlAdv1.Size = new System.Drawing.Size(761, 401);
            this.tabControlAdv1.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(52, 18);
            this.label1.TabIndex = 1;
            this.label1.Text = "Doc No";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(202, 26);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(63, 18);
            this.label2.TabIndex = 2;
            this.label2.Text = "Doc Date";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(64, 22);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(132, 26);
            this.textBox1.TabIndex = 3;
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker1.Location = new System.Drawing.Point(271, 22);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(119, 26);
            this.dateTimePicker1.TabIndex = 4;
            // 
            // tabFabric
            // 
            this.tabFabric.Image = null;
            this.tabFabric.ImageSize = new System.Drawing.Size(16, 16);
            this.tabFabric.Location = new System.Drawing.Point(1, 30);
            this.tabFabric.Name = "tabFabric";
            this.tabFabric.ShowCloseButton = true;
            this.tabFabric.Size = new System.Drawing.Size(758, 369);
            this.tabFabric.TabIndex = 1;
            this.tabFabric.Text = "Fabric";
            this.tabFabric.ThemesEnabled = false;
            // 
            // tabYarn
            // 
            this.tabYarn.Image = null;
            this.tabYarn.ImageSize = new System.Drawing.Size(16, 16);
            this.tabYarn.Location = new System.Drawing.Point(1, 30);
            this.tabYarn.Name = "tabYarn";
            this.tabYarn.ShowCloseButton = true;
            this.tabYarn.Size = new System.Drawing.Size(758, 369);
            this.tabYarn.TabIndex = 2;
            this.tabYarn.Text = "Yarn";
            this.tabYarn.ThemesEnabled = false;
            // 
            // tabYarnProcess
            // 
            this.tabYarnProcess.Image = null;
            this.tabYarnProcess.ImageSize = new System.Drawing.Size(16, 16);
            this.tabYarnProcess.Location = new System.Drawing.Point(1, 30);
            this.tabYarnProcess.Name = "tabYarnProcess";
            this.tabYarnProcess.ShowCloseButton = true;
            this.tabYarnProcess.Size = new System.Drawing.Size(758, 369);
            this.tabYarnProcess.TabIndex = 3;
            this.tabYarnProcess.Text = "Yarn Process";
            this.tabYarnProcess.ThemesEnabled = false;
            // 
            // tabFabricProcess
            // 
            this.tabFabricProcess.Image = null;
            this.tabFabricProcess.ImageSize = new System.Drawing.Size(16, 16);
            this.tabFabricProcess.Location = new System.Drawing.Point(1, 30);
            this.tabFabricProcess.Name = "tabFabricProcess";
            this.tabFabricProcess.ShowCloseButton = true;
            this.tabFabricProcess.Size = new System.Drawing.Size(758, 369);
            this.tabFabricProcess.TabIndex = 4;
            this.tabFabricProcess.Text = "Fabric Process";
            this.tabFabricProcess.ThemesEnabled = false;
            // 
            // FrmInternalOrderFabric
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(800, 495);
            this.Controls.Add(this.grBack);
            this.Name = "FrmInternalOrderFabric";
            this.Text = "Internal Order Fabric";
            this.grBack.ResumeLayout(false);
            this.grBack.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tabControlAdv1)).EndInit();
            this.tabControlAdv1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grBack;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private Syncfusion.Windows.Forms.Tools.TabControlAdv tabControlAdv1;
        private Syncfusion.Windows.Forms.Tools.TabPageAdv tabFabric;
        private Syncfusion.Windows.Forms.Tools.TabPageAdv tabYarn;
        private Syncfusion.Windows.Forms.Tools.TabPageAdv tabYarnProcess;
        private Syncfusion.Windows.Forms.Tools.TabPageAdv tabFabricProcess;
    }
}