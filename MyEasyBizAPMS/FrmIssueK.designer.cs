﻿namespace MyEasyBizAPMS
{
    partial class FrmIssueK
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmIssueK));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.Genpan = new System.Windows.Forms.Panel();
            this.label11 = new System.Windows.Forms.Label();
            this.txtscr1 = new System.Windows.Forms.TextBox();
            this.HFGP = new System.Windows.Forms.DataGridView();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.flowLayoutPanel4 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel5 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel6 = new System.Windows.Forms.FlowLayoutPanel();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.label57 = new System.Windows.Forms.Label();
            this.dtpfnt = new System.Windows.Forms.DateTimePicker();
            this.txtscr8 = new System.Windows.Forms.TextBox();
            this.txtscr7 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.txtscr6 = new System.Windows.Forms.TextBox();
            this.txtscr5 = new System.Windows.Forms.TextBox();
            this.Txtscr2 = new System.Windows.Forms.TextBox();
            this.txtscr4 = new System.Windows.Forms.TextBox();
            this.Txtscr3 = new System.Windows.Forms.TextBox();
            this.Editpnl = new System.Windows.Forms.Panel();
            this.grSearch = new System.Windows.Forms.Panel();
            this.button18 = new System.Windows.Forms.Button();
            this.btnHide = new System.Windows.Forms.Button();
            this.DataGridCommon = new System.Windows.Forms.DataGridView();
            this.button22 = new System.Windows.Forms.Button();
            this.label28 = new System.Windows.Forms.Label();
            this.reqdt = new System.Windows.Forms.DateTimePicker();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.tabC = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.panel1 = new System.Windows.Forms.Panel();
            this.txtoutuom = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.lkppnl = new System.Windows.Forms.Panel();
            this.cbowono = new System.Windows.Forms.TextBox();
            this.label36 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.button14 = new System.Windows.Forms.Button();
            this.button15 = new System.Windows.Forms.Button();
            this.HFGP2 = new System.Windows.Forms.DataGridView();
            this.txtvehicle = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txtwref = new System.Windows.Forms.TextBox();
            this.RQGR = new System.Windows.Forms.DataGridView();
            this.txtoutqty = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.txtoutrolls = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.txtoutitem = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.button4 = new System.Windows.Forms.Button();
            this.label38 = new System.Windows.Forms.Label();
            this.txtworder = new System.Windows.Forms.TextBox();
            this.button13 = new System.Windows.Forms.Button();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label40 = new System.Windows.Forms.Label();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.label39 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.button16 = new System.Windows.Forms.Button();
            this.button17 = new System.Windows.Forms.Button();
            this.txtscr11 = new System.Windows.Forms.TextBox();
            this.HFG9 = new System.Windows.Forms.DataGridView();
            this.label69 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.cbotype = new System.Windows.Forms.ComboBox();
            this.label24 = new System.Windows.Forms.Label();
            this.cbotax = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtprice = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtoutoutqty = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtoutputitem = new System.Windows.Forms.TextBox();
            this.HFIT = new System.Windows.Forms.DataGridView();
            this.btnadd = new System.Windows.Forms.Button();
            this.txtdcqty = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtitem = new System.Windows.Forms.TextBox();
            this.txtbeamid = new System.Windows.Forms.TextBox();
            this.txtoutputid = new System.Windows.Forms.TextBox();
            this.txtitemid = new System.Windows.Forms.TextBox();
            this.txtmillid = new System.Windows.Forms.TextBox();
            this.txtpuid = new System.Windows.Forms.TextBox();
            this.txtgrnid = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.txtuom = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.HFGP1 = new System.Windows.Forms.DataGridView();
            this.label17 = new System.Windows.Forms.Label();
            this.txtrectot = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.txtisstot = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.txtmode = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.txtbags = new System.Windows.Forms.TextBox();
            this.button10 = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.txtaddnotes = new System.Windows.Forms.TextBox();
            this.txtbeam = new System.Windows.Forms.TextBox();
            this.serialno = new System.Windows.Forms.Panel();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.label80 = new System.Windows.Forms.Label();
            this.Dataserial = new System.Windows.Forms.DataGridView();
            this.label78 = new System.Windows.Forms.Label();
            this.button20 = new System.Windows.Forms.Button();
            this.label76 = new System.Windows.Forms.Label();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.label79 = new System.Windows.Forms.Label();
            this.txtsrail = new System.Windows.Forms.TextBox();
            this.cboserial = new System.Windows.Forms.ComboBox();
            this.label84 = new System.Windows.Forms.Label();
            this.txtserialqty = new System.Windows.Forms.TextBox();
            this.label85 = new System.Windows.Forms.Label();
            this.button23 = new System.Windows.Forms.Button();
            this.label88 = new System.Windows.Forms.Label();
            this.textBox14 = new System.Windows.Forms.TextBox();
            this.textBox15 = new System.Windows.Forms.TextBox();
            this.txtsqty = new System.Windows.Forms.TextBox();
            this.CBOWNO = new System.Windows.Forms.ComboBox();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.label35 = new System.Windows.Forms.Label();
            this.txtreqrty = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.stpreq = new System.Windows.Forms.DateTimePicker();
            this.HFTP = new System.Windows.Forms.DataGridView();
            this.txtreqty = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.cbosGReturnItem = new System.Windows.Forms.ComboBox();
            this.label67 = new System.Windows.Forms.Label();
            this.txtqty = new System.Windows.Forms.TextBox();
            this.button12 = new System.Windows.Forms.Button();
            this.cboprocess = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.Phone = new System.Windows.Forms.Label();
            this.txtgrn = new System.Windows.Forms.TextBox();
            this.dtpgrndt = new System.Windows.Forms.DateTimePicker();
            this.txtname = new System.Windows.Forms.TextBox();
            this.txtnobeams = new System.Windows.Forms.TextBox();
            this.txtseqno = new System.Windows.Forms.TextBox();
            this.label33 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.panadd = new System.Windows.Forms.Panel();
            this.button11 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.lblno1 = new System.Windows.Forms.Label();
            this.lblno2 = new System.Windows.Forms.Label();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.buttnnvfst = new System.Windows.Forms.Button();
            this.buttnnxtlft = new System.Windows.Forms.Button();
            this.btnfinnxt = new System.Windows.Forms.Button();
            this.buttrnxt = new System.Windows.Forms.Button();
            this.buttnext1 = new System.Windows.Forms.Button();
            this.chkact = new System.Windows.Forms.CheckBox();
            this.butexit = new System.Windows.Forms.Button();
            this.butedit = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.Chkedtact = new System.Windows.Forms.CheckBox();
            this.btnaddrcan = new System.Windows.Forms.Button();
            this.btnsave = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.Genpan.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HFGP)).BeginInit();
            this.panel2.SuspendLayout();
            this.Editpnl.SuspendLayout();
            this.grSearch.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridCommon)).BeginInit();
            this.tabC.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.lkppnl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HFGP2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RQGR)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HFG9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HFIT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HFGP1)).BeginInit();
            this.serialno.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Dataserial)).BeginInit();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HFTP)).BeginInit();
            this.panadd.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // Genpan
            // 
            this.Genpan.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.Genpan.Controls.Add(this.label11);
            this.Genpan.Controls.Add(this.txtscr1);
            this.Genpan.Controls.Add(this.HFGP);
            this.Genpan.Controls.Add(this.panel2);
            this.Genpan.Controls.Add(this.button5);
            this.Genpan.Controls.Add(this.button6);
            this.Genpan.Controls.Add(this.button7);
            this.Genpan.Controls.Add(this.button8);
            this.Genpan.Controls.Add(this.label57);
            this.Genpan.Controls.Add(this.dtpfnt);
            this.Genpan.Controls.Add(this.txtscr8);
            this.Genpan.Controls.Add(this.txtscr7);
            this.Genpan.Controls.Add(this.textBox2);
            this.Genpan.Controls.Add(this.textBox1);
            this.Genpan.Controls.Add(this.txtscr6);
            this.Genpan.Controls.Add(this.txtscr5);
            this.Genpan.Controls.Add(this.Txtscr2);
            this.Genpan.Controls.Add(this.txtscr4);
            this.Genpan.Controls.Add(this.Txtscr3);
            this.Genpan.Location = new System.Drawing.Point(-1, -1);
            this.Genpan.Margin = new System.Windows.Forms.Padding(4);
            this.Genpan.Name = "Genpan";
            this.Genpan.Size = new System.Drawing.Size(1096, 552);
            this.Genpan.TabIndex = 215;
            this.Genpan.Paint += new System.Windows.Forms.PaintEventHandler(this.Genpan_Paint);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(2, 14);
            this.label11.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(140, 23);
            this.label11.TabIndex = 201;
            this.label11.Text = " Knitting Delivery";
            // 
            // txtscr1
            // 
            this.txtscr1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtscr1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtscr1.Location = new System.Drawing.Point(1, 43);
            this.txtscr1.Margin = new System.Windows.Forms.Padding(5);
            this.txtscr1.Name = "txtscr1";
            this.txtscr1.Size = new System.Drawing.Size(1066, 26);
            this.txtscr1.TabIndex = 1;
            this.txtscr1.TextChanged += new System.EventHandler(this.txtscr1_TextChanged);
            // 
            // HFGP
            // 
            this.HFGP.BackgroundColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.HFGP.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.HFGP.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnF2;
            this.HFGP.Location = new System.Drawing.Point(1, 74);
            this.HFGP.Margin = new System.Windows.Forms.Padding(4);
            this.HFGP.Name = "HFGP";
            this.HFGP.ReadOnly = true;
            this.HFGP.Size = new System.Drawing.Size(1066, 471);
            this.HFGP.TabIndex = 3;
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel2.Controls.Add(this.label12);
            this.panel2.Controls.Add(this.label13);
            this.panel2.Controls.Add(this.flowLayoutPanel4);
            this.panel2.Controls.Add(this.flowLayoutPanel5);
            this.panel2.Controls.Add(this.flowLayoutPanel6);
            this.panel2.Location = new System.Drawing.Point(66, 390);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(60, 30);
            this.panel2.TabIndex = 214;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(4, 5);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(15, 16);
            this.label12.TabIndex = 163;
            this.label12.Text = "1";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Black;
            this.label13.Location = new System.Drawing.Point(27, 5);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(29, 16);
            this.label13.TabIndex = 162;
            this.label13.Text = "of 1";
            // 
            // flowLayoutPanel4
            // 
            this.flowLayoutPanel4.Location = new System.Drawing.Point(61, 30);
            this.flowLayoutPanel4.Name = "flowLayoutPanel4";
            this.flowLayoutPanel4.Size = new System.Drawing.Size(200, 100);
            this.flowLayoutPanel4.TabIndex = 2;
            // 
            // flowLayoutPanel5
            // 
            this.flowLayoutPanel5.Location = new System.Drawing.Point(38, 35);
            this.flowLayoutPanel5.Name = "flowLayoutPanel5";
            this.flowLayoutPanel5.Size = new System.Drawing.Size(200, 100);
            this.flowLayoutPanel5.TabIndex = 1;
            // 
            // flowLayoutPanel6
            // 
            this.flowLayoutPanel6.Location = new System.Drawing.Point(62, -129);
            this.flowLayoutPanel6.Name = "flowLayoutPanel6";
            this.flowLayoutPanel6.Size = new System.Drawing.Size(200, 100);
            this.flowLayoutPanel6.TabIndex = 0;
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.Color.White;
            this.button5.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button5.FlatAppearance.BorderSize = 0;
            this.button5.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button5.Image = ((System.Drawing.Image)(resources.GetObject("button5.Image")));
            this.button5.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button5.Location = new System.Drawing.Point(19, 390);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(19, 31);
            this.button5.TabIndex = 213;
            this.button5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button5.UseVisualStyleBackColor = false;
            // 
            // button6
            // 
            this.button6.BackColor = System.Drawing.Color.White;
            this.button6.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button6.FlatAppearance.BorderSize = 0;
            this.button6.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button6.Image = ((System.Drawing.Image)(resources.GetObject("button6.Image")));
            this.button6.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button6.Location = new System.Drawing.Point(44, 390);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(18, 31);
            this.button6.TabIndex = 212;
            this.button6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button6.UseVisualStyleBackColor = false;
            // 
            // button7
            // 
            this.button7.BackColor = System.Drawing.Color.White;
            this.button7.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button7.FlatAppearance.BorderSize = 0;
            this.button7.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button7.Image = ((System.Drawing.Image)(resources.GetObject("button7.Image")));
            this.button7.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button7.Location = new System.Drawing.Point(156, 390);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(19, 31);
            this.button7.TabIndex = 211;
            this.button7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button7.UseVisualStyleBackColor = false;
            // 
            // button8
            // 
            this.button8.BackColor = System.Drawing.Color.White;
            this.button8.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button8.FlatAppearance.BorderSize = 0;
            this.button8.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button8.Image = ((System.Drawing.Image)(resources.GetObject("button8.Image")));
            this.button8.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button8.Location = new System.Drawing.Point(132, 390);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(18, 31);
            this.button8.TabIndex = 210;
            this.button8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button8.UseVisualStyleBackColor = false;
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label57.Location = new System.Drawing.Point(411, 167);
            this.label57.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(42, 21);
            this.label57.TabIndex = 223;
            this.label57.Text = "Date";
            // 
            // dtpfnt
            // 
            this.dtpfnt.CustomFormat = "MMM/yyyy";
            this.dtpfnt.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpfnt.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpfnt.Location = new System.Drawing.Point(458, 164);
            this.dtpfnt.Margin = new System.Windows.Forms.Padding(4);
            this.dtpfnt.Name = "dtpfnt";
            this.dtpfnt.Size = new System.Drawing.Size(104, 26);
            this.dtpfnt.TabIndex = 222;
            this.dtpfnt.Value = new System.DateTime(2017, 7, 4, 0, 0, 0, 0);
            // 
            // txtscr8
            // 
            this.txtscr8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtscr8.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtscr8.Location = new System.Drawing.Point(651, 230);
            this.txtscr8.Margin = new System.Windows.Forms.Padding(5);
            this.txtscr8.Name = "txtscr8";
            this.txtscr8.Size = new System.Drawing.Size(100, 26);
            this.txtscr8.TabIndex = 225;
            // 
            // txtscr7
            // 
            this.txtscr7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtscr7.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtscr7.Location = new System.Drawing.Point(627, 256);
            this.txtscr7.Margin = new System.Windows.Forms.Padding(5);
            this.txtscr7.Name = "txtscr7";
            this.txtscr7.Size = new System.Drawing.Size(164, 26);
            this.txtscr7.TabIndex = 224;
            // 
            // textBox2
            // 
            this.textBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox2.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox2.Location = new System.Drawing.Point(857, 198);
            this.textBox2.Margin = new System.Windows.Forms.Padding(5);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(142, 26);
            this.textBox2.TabIndex = 227;
            // 
            // textBox1
            // 
            this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.Location = new System.Drawing.Point(611, 198);
            this.textBox1.Margin = new System.Windows.Forms.Padding(5);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(150, 26);
            this.textBox1.TabIndex = 226;
            // 
            // txtscr6
            // 
            this.txtscr6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtscr6.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtscr6.Location = new System.Drawing.Point(766, 198);
            this.txtscr6.Margin = new System.Windows.Forms.Padding(5);
            this.txtscr6.Name = "txtscr6";
            this.txtscr6.Size = new System.Drawing.Size(85, 26);
            this.txtscr6.TabIndex = 202;
            this.txtscr6.TextChanged += new System.EventHandler(this.txtscr6_TextChanged);
            // 
            // txtscr5
            // 
            this.txtscr5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtscr5.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtscr5.Location = new System.Drawing.Point(110, 199);
            this.txtscr5.Margin = new System.Windows.Forms.Padding(5);
            this.txtscr5.Name = "txtscr5";
            this.txtscr5.Size = new System.Drawing.Size(500, 26);
            this.txtscr5.TabIndex = 90;
            // 
            // Txtscr2
            // 
            this.Txtscr2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Txtscr2.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txtscr2.Location = new System.Drawing.Point(21, 199);
            this.Txtscr2.Margin = new System.Windows.Forms.Padding(5);
            this.Txtscr2.Name = "Txtscr2";
            this.Txtscr2.Size = new System.Drawing.Size(90, 26);
            this.Txtscr2.TabIndex = 87;
            // 
            // txtscr4
            // 
            this.txtscr4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtscr4.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtscr4.Location = new System.Drawing.Point(287, 157);
            this.txtscr4.Margin = new System.Windows.Forms.Padding(5);
            this.txtscr4.Name = "txtscr4";
            this.txtscr4.Size = new System.Drawing.Size(90, 22);
            this.txtscr4.TabIndex = 100;
            // 
            // Txtscr3
            // 
            this.Txtscr3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Txtscr3.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txtscr3.Location = new System.Drawing.Point(198, 157);
            this.Txtscr3.Margin = new System.Windows.Forms.Padding(5);
            this.Txtscr3.Name = "Txtscr3";
            this.Txtscr3.Size = new System.Drawing.Size(90, 22);
            this.Txtscr3.TabIndex = 88;
            // 
            // Editpnl
            // 
            this.Editpnl.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.Editpnl.Controls.Add(this.grSearch);
            this.Editpnl.Controls.Add(this.label28);
            this.Editpnl.Controls.Add(this.reqdt);
            this.Editpnl.Controls.Add(this.checkBox2);
            this.Editpnl.Controls.Add(this.tabC);
            this.Editpnl.Controls.Add(this.cboprocess);
            this.Editpnl.Controls.Add(this.label4);
            this.Editpnl.Controls.Add(this.label2);
            this.Editpnl.Controls.Add(this.label3);
            this.Editpnl.Controls.Add(this.Phone);
            this.Editpnl.Controls.Add(this.txtgrn);
            this.Editpnl.Controls.Add(this.dtpgrndt);
            this.Editpnl.Controls.Add(this.txtname);
            this.Editpnl.Controls.Add(this.txtnobeams);
            this.Editpnl.Controls.Add(this.txtseqno);
            this.Editpnl.Controls.Add(this.label33);
            this.Editpnl.Controls.Add(this.label29);
            this.Editpnl.Location = new System.Drawing.Point(0, 13);
            this.Editpnl.Name = "Editpnl";
            this.Editpnl.Size = new System.Drawing.Size(1101, 542);
            this.Editpnl.TabIndex = 216;
            this.Editpnl.Paint += new System.Windows.Forms.PaintEventHandler(this.Editpnl_Paint);
            // 
            // grSearch
            // 
            this.grSearch.BackColor = System.Drawing.Color.White;
            this.grSearch.Controls.Add(this.button18);
            this.grSearch.Controls.Add(this.btnHide);
            this.grSearch.Controls.Add(this.DataGridCommon);
            this.grSearch.Controls.Add(this.button22);
            this.grSearch.Location = new System.Drawing.Point(93, 242);
            this.grSearch.Name = "grSearch";
            this.grSearch.Size = new System.Drawing.Size(476, 252);
            this.grSearch.TabIndex = 399;
            this.grSearch.Visible = false;
            // 
            // button18
            // 
            this.button18.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button18.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button18.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button18.Location = new System.Drawing.Point(383, 215);
            this.button18.Name = "button18";
            this.button18.Size = new System.Drawing.Size(74, 28);
            this.button18.TabIndex = 394;
            this.button18.Text = "Select";
            this.button18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button18.UseVisualStyleBackColor = false;
            this.button18.Click += new System.EventHandler(this.button18_Click);
            // 
            // btnHide
            // 
            this.btnHide.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnHide.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHide.Image = ((System.Drawing.Image)(resources.GetObject("btnHide.Image")));
            this.btnHide.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnHide.Location = new System.Drawing.Point(6, 216);
            this.btnHide.Name = "btnHide";
            this.btnHide.Size = new System.Drawing.Size(71, 27);
            this.btnHide.TabIndex = 393;
            this.btnHide.Text = "Close";
            this.btnHide.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnHide.UseVisualStyleBackColor = false;
            this.btnHide.Click += new System.EventHandler(this.btnHide_Click);
            // 
            // DataGridCommon
            // 
            this.DataGridCommon.AllowUserToAddRows = false;
            this.DataGridCommon.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.DataGridCommon.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridCommon.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.DataGridCommon.Location = new System.Drawing.Point(7, 10);
            this.DataGridCommon.Name = "DataGridCommon";
            this.DataGridCommon.ReadOnly = true;
            this.DataGridCommon.RowHeadersVisible = false;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.DataGridCommon.RowsDefaultCellStyle = dataGridViewCellStyle3;
            this.DataGridCommon.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridCommon.Size = new System.Drawing.Size(450, 204);
            this.DataGridCommon.TabIndex = 0;
            this.DataGridCommon.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridCommon_CellContentClick);
            this.DataGridCommon.DoubleClick += new System.EventHandler(this.DataGridCommon_DoubleClick);
            this.DataGridCommon.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DataGridCommon_KeyDown);
            // 
            // button22
            // 
            this.button22.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button22.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button22.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button22.Location = new System.Drawing.Point(202, 123);
            this.button22.Name = "button22";
            this.button22.Size = new System.Drawing.Size(105, 28);
            this.button22.TabIndex = 396;
            this.button22.Text = "Add Supplier";
            this.button22.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button22.UseVisualStyleBackColor = false;
            this.button22.Click += new System.EventHandler(this.button22_Click);
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(945, 5);
            this.label28.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(102, 19);
            this.label28.TabIndex = 428;
            this.label28.Text = "Required Date";
            this.label28.Click += new System.EventHandler(this.label28_Click);
            // 
            // reqdt
            // 
            this.reqdt.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.reqdt.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.reqdt.Location = new System.Drawing.Point(949, 29);
            this.reqdt.Name = "reqdt";
            this.reqdt.Size = new System.Drawing.Size(122, 27);
            this.reqdt.TabIndex = 429;
            this.reqdt.ValueChanged += new System.EventHandler(this.reqdt_ValueChanged);
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.BackColor = System.Drawing.Color.Transparent;
            this.checkBox2.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox2.Location = new System.Drawing.Point(822, 30);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(121, 23);
            this.checkBox2.TabIndex = 427;
            this.checkBox2.Text = "Required Date";
            this.checkBox2.UseVisualStyleBackColor = false;
            this.checkBox2.CheckedChanged += new System.EventHandler(this.checkBox2_CheckedChanged);
            // 
            // tabC
            // 
            this.tabC.Controls.Add(this.tabPage1);
            this.tabC.Controls.Add(this.tabPage2);
            this.tabC.Controls.Add(this.tabPage3);
            this.tabC.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabC.Location = new System.Drawing.Point(20, 64);
            this.tabC.Name = "tabC";
            this.tabC.SelectedIndex = 0;
            this.tabC.Size = new System.Drawing.Size(1079, 471);
            this.tabC.TabIndex = 426;
            this.tabC.SelectedIndexChanged += new System.EventHandler(this.tabC_SelectedIndexChanged);
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.tabPage1.Controls.Add(this.panel1);
            this.tabPage1.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabPage1.ForeColor = System.Drawing.Color.Black;
            this.tabPage1.Location = new System.Drawing.Point(4, 28);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1071, 439);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Dc Items";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.panel1.Controls.Add(this.txtoutuom);
            this.panel1.Controls.Add(this.label27);
            this.panel1.Controls.Add(this.lkppnl);
            this.panel1.Controls.Add(this.RQGR);
            this.panel1.Controls.Add(this.txtoutqty);
            this.panel1.Controls.Add(this.label21);
            this.panel1.Controls.Add(this.txtoutrolls);
            this.panel1.Controls.Add(this.label22);
            this.panel1.Controls.Add(this.txtoutitem);
            this.panel1.Controls.Add(this.label20);
            this.panel1.Controls.Add(this.button4);
            this.panel1.Controls.Add(this.label38);
            this.panel1.Controls.Add(this.txtworder);
            this.panel1.Controls.Add(this.button13);
            this.panel1.Location = new System.Drawing.Point(-4, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1073, 420);
            this.panel1.TabIndex = 1;
            // 
            // txtoutuom
            // 
            this.txtoutuom.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtoutuom.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtoutuom.Location = new System.Drawing.Point(776, 28);
            this.txtoutuom.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtoutuom.Name = "txtoutuom";
            this.txtoutuom.Size = new System.Drawing.Size(84, 27);
            this.txtoutuom.TabIndex = 428;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(779, 7);
            this.label27.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(44, 21);
            this.label27.TabIndex = 437;
            this.label27.Text = "Uom";
            // 
            // lkppnl
            // 
            this.lkppnl.BackColor = System.Drawing.Color.White;
            this.lkppnl.Controls.Add(this.cbowono);
            this.lkppnl.Controls.Add(this.label36);
            this.lkppnl.Controls.Add(this.label34);
            this.lkppnl.Controls.Add(this.textBox4);
            this.lkppnl.Controls.Add(this.button14);
            this.lkppnl.Controls.Add(this.button15);
            this.lkppnl.Controls.Add(this.HFGP2);
            this.lkppnl.Controls.Add(this.txtvehicle);
            this.lkppnl.Controls.Add(this.label1);
            this.lkppnl.Controls.Add(this.label9);
            this.lkppnl.Controls.Add(this.txtwref);
            this.lkppnl.Location = new System.Drawing.Point(26, 60);
            this.lkppnl.Name = "lkppnl";
            this.lkppnl.Size = new System.Drawing.Size(853, 308);
            this.lkppnl.TabIndex = 436;
            this.lkppnl.Visible = false;
            // 
            // cbowono
            // 
            this.cbowono.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.cbowono.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbowono.Location = new System.Drawing.Point(283, 7);
            this.cbowono.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cbowono.Name = "cbowono";
            this.cbowono.Size = new System.Drawing.Size(148, 27);
            this.cbowono.TabIndex = 438;
            this.cbowono.TextChanged += new System.EventHandler(this.cbowono_TextChanged);
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.Location = new System.Drawing.Point(177, 11);
            this.label36.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(105, 19);
            this.label36.TabIndex = 429;
            this.label36.Text = "Work Order No";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.Location = new System.Drawing.Point(439, 11);
            this.label34.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(40, 19);
            this.label34.TabIndex = 437;
            this.label34.Text = "Style";
            // 
            // textBox4
            // 
            this.textBox4.AcceptsReturn = true;
            this.textBox4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox4.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox4.Location = new System.Drawing.Point(3, 40);
            this.textBox4.Margin = new System.Windows.Forms.Padding(4);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(837, 26);
            this.textBox4.TabIndex = 395;
            this.textBox4.TextChanged += new System.EventHandler(this.textBox4_TextChanged_1);
            // 
            // button14
            // 
            this.button14.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button14.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button14.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button14.Location = new System.Drawing.Point(778, 277);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(72, 28);
            this.button14.TabIndex = 394;
            this.button14.Text = "Select";
            this.button14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button14.UseVisualStyleBackColor = false;
            this.button14.Click += new System.EventHandler(this.button14_Click_1);
            // 
            // button15
            // 
            this.button15.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button15.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button15.Image = ((System.Drawing.Image)(resources.GetObject("button15.Image")));
            this.button15.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button15.Location = new System.Drawing.Point(6, 277);
            this.button15.Name = "button15";
            this.button15.Size = new System.Drawing.Size(71, 27);
            this.button15.TabIndex = 393;
            this.button15.Text = "Close";
            this.button15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button15.UseVisualStyleBackColor = false;
            this.button15.Click += new System.EventHandler(this.button15_Click_1);
            // 
            // HFGP2
            // 
            this.HFGP2.AllowUserToAddRows = false;
            this.HFGP2.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.HFGP2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.HFGP2.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.HFGP2.Location = new System.Drawing.Point(3, 73);
            this.HFGP2.Name = "HFGP2";
            this.HFGP2.ReadOnly = true;
            this.HFGP2.RowHeadersVisible = false;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.HFGP2.RowsDefaultCellStyle = dataGridViewCellStyle4;
            this.HFGP2.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.HFGP2.Size = new System.Drawing.Size(847, 203);
            this.HFGP2.TabIndex = 0;
            this.HFGP2.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.HFGP2_CellContentClick);
            this.HFGP2.DoubleClick += new System.EventHandler(this.HFGP2_DoubleClick_1);
            this.HFGP2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.HFGP2_KeyDown_1);
            // 
            // txtvehicle
            // 
            this.txtvehicle.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtvehicle.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvehicle.Location = new System.Drawing.Point(51, 6);
            this.txtvehicle.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtvehicle.Name = "txtvehicle";
            this.txtvehicle.Size = new System.Drawing.Size(123, 27);
            this.txtvehicle.TabIndex = 192;
            this.txtvehicle.Click += new System.EventHandler(this.txtvehicle_Click);
            this.txtvehicle.TextChanged += new System.EventHandler(this.txtvehicle_TextChanged);
            this.txtvehicle.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtvehicle_KeyDown);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(4, 8);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 19);
            this.label1.TabIndex = 9;
            this.label1.Text = "SocNo";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(246, 160);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(128, 21);
            this.label9.TabIndex = 427;
            this.label9.Text = "Press F3 ItemList";
            // 
            // txtwref
            // 
            this.txtwref.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtwref.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtwref.Location = new System.Drawing.Point(479, 7);
            this.txtwref.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtwref.Name = "txtwref";
            this.txtwref.Size = new System.Drawing.Size(361, 27);
            this.txtwref.TabIndex = 436;
            this.txtwref.TextChanged += new System.EventHandler(this.txtwref_TextChanged);
            // 
            // RQGR
            // 
            this.RQGR.BackgroundColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.RQGR.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.RQGR.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.RQGR.Location = new System.Drawing.Point(19, 60);
            this.RQGR.Name = "RQGR";
            this.RQGR.Size = new System.Drawing.Size(1027, 344);
            this.RQGR.TabIndex = 432;
            // 
            // txtoutqty
            // 
            this.txtoutqty.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtoutqty.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtoutqty.Location = new System.Drawing.Point(861, 28);
            this.txtoutqty.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtoutqty.Name = "txtoutqty";
            this.txtoutqty.Size = new System.Drawing.Size(71, 27);
            this.txtoutqty.TabIndex = 429;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(947, 7);
            this.label21.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(76, 21);
            this.label21.TabIndex = 435;
            this.label21.Text = "NoofBags";
            // 
            // txtoutrolls
            // 
            this.txtoutrolls.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtoutrolls.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtoutrolls.Location = new System.Drawing.Point(933, 28);
            this.txtoutrolls.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtoutrolls.Name = "txtoutrolls";
            this.txtoutrolls.Size = new System.Drawing.Size(74, 27);
            this.txtoutrolls.TabIndex = 430;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(871, 7);
            this.label22.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(35, 21);
            this.label22.TabIndex = 434;
            this.label22.Text = "Qty";
            // 
            // txtoutitem
            // 
            this.txtoutitem.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtoutitem.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtoutitem.Location = new System.Drawing.Point(18, 28);
            this.txtoutitem.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtoutitem.Name = "txtoutitem";
            this.txtoutitem.Size = new System.Drawing.Size(748, 27);
            this.txtoutitem.TabIndex = 427;
            this.txtoutitem.Click += new System.EventHandler(this.txtoutitem_Click_1);
            this.txtoutitem.TextChanged += new System.EventHandler(this.txtoutitem_TextChanged_1);
            this.txtoutitem.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtoutitem_KeyDown_1);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(14, 7);
            this.label20.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(83, 21);
            this.label20.TabIndex = 433;
            this.label20.Text = "ItemName";
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button4.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button4.Location = new System.Drawing.Point(1007, 28);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(39, 28);
            this.button4.TabIndex = 431;
            this.button4.Text = "OK";
            this.button4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.button4_Click_1);
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.Location = new System.Drawing.Point(706, 201);
            this.label38.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(105, 19);
            this.label38.TabIndex = 438;
            this.label38.Text = "Work Order No";
            // 
            // txtworder
            // 
            this.txtworder.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtworder.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtworder.Location = new System.Drawing.Point(743, 225);
            this.txtworder.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtworder.Name = "txtworder";
            this.txtworder.Size = new System.Drawing.Size(109, 27);
            this.txtworder.TabIndex = 439;
            // 
            // button13
            // 
            this.button13.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button13.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button13.Location = new System.Drawing.Point(980, 184);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(35, 27);
            this.button13.TabIndex = 430;
            this.button13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button13.UseVisualStyleBackColor = false;
            this.button13.Click += new System.EventHandler(this.button13_Click);
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.tabPage2.Controls.Add(this.panel4);
            this.tabPage2.Controls.Add(this.label31);
            this.tabPage2.Controls.Add(this.cbotype);
            this.tabPage2.Controls.Add(this.label24);
            this.tabPage2.Controls.Add(this.cbotax);
            this.tabPage2.Controls.Add(this.label8);
            this.tabPage2.Controls.Add(this.txtprice);
            this.tabPage2.Controls.Add(this.label14);
            this.tabPage2.Controls.Add(this.txtoutoutqty);
            this.tabPage2.Controls.Add(this.label5);
            this.tabPage2.Controls.Add(this.txtoutputitem);
            this.tabPage2.Controls.Add(this.HFIT);
            this.tabPage2.Controls.Add(this.btnadd);
            this.tabPage2.Controls.Add(this.txtdcqty);
            this.tabPage2.Controls.Add(this.label7);
            this.tabPage2.Controls.Add(this.txtitem);
            this.tabPage2.Controls.Add(this.txtbeamid);
            this.tabPage2.Controls.Add(this.txtoutputid);
            this.tabPage2.Controls.Add(this.txtitemid);
            this.tabPage2.Controls.Add(this.txtmillid);
            this.tabPage2.Controls.Add(this.txtpuid);
            this.tabPage2.Controls.Add(this.txtgrnid);
            this.tabPage2.Controls.Add(this.label30);
            this.tabPage2.Controls.Add(this.txtuom);
            this.tabPage2.Controls.Add(this.textBox3);
            this.tabPage2.Controls.Add(this.button2);
            this.tabPage2.Controls.Add(this.HFGP1);
            this.tabPage2.Controls.Add(this.label17);
            this.tabPage2.Controls.Add(this.txtrectot);
            this.tabPage2.Controls.Add(this.label16);
            this.tabPage2.Controls.Add(this.txtisstot);
            this.tabPage2.Controls.Add(this.label15);
            this.tabPage2.Controls.Add(this.label10);
            this.tabPage2.Controls.Add(this.txtmode);
            this.tabPage2.Controls.Add(this.label18);
            this.tabPage2.Controls.Add(this.txtbags);
            this.tabPage2.Controls.Add(this.button10);
            this.tabPage2.Controls.Add(this.label6);
            this.tabPage2.Controls.Add(this.label19);
            this.tabPage2.Controls.Add(this.txtaddnotes);
            this.tabPage2.Controls.Add(this.txtbeam);
            this.tabPage2.Controls.Add(this.serialno);
            this.tabPage2.Controls.Add(this.CBOWNO);
            this.tabPage2.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabPage2.Location = new System.Drawing.Point(4, 28);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1071, 439);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Plan Items";
            this.tabPage2.Click += new System.EventHandler(this.tabPage2_Click);
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.SystemColors.HighlightText;
            this.panel4.Controls.Add(this.label40);
            this.panel4.Controls.Add(this.textBox8);
            this.panel4.Controls.Add(this.textBox7);
            this.panel4.Controls.Add(this.textBox5);
            this.panel4.Controls.Add(this.label39);
            this.panel4.Controls.Add(this.label37);
            this.panel4.Controls.Add(this.button16);
            this.panel4.Controls.Add(this.button17);
            this.panel4.Controls.Add(this.txtscr11);
            this.panel4.Controls.Add(this.HFG9);
            this.panel4.Controls.Add(this.label69);
            this.panel4.Controls.Add(this.label32);
            this.panel4.Location = new System.Drawing.Point(25, 70);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(842, 352);
            this.panel4.TabIndex = 5636;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.Location = new System.Drawing.Point(480, 15);
            this.label40.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(40, 19);
            this.label40.TabIndex = 5641;
            this.label40.Text = "Style";
            // 
            // textBox8
            // 
            this.textBox8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox8.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox8.Location = new System.Drawing.Point(520, 11);
            this.textBox8.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new System.Drawing.Size(312, 27);
            this.textBox8.TabIndex = 5640;
            // 
            // textBox7
            // 
            this.textBox7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox7.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox7.Location = new System.Drawing.Point(56, 16);
            this.textBox7.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new System.Drawing.Size(132, 27);
            this.textBox7.TabIndex = 5639;
            this.textBox7.Click += new System.EventHandler(this.textBox7_Click);
            this.textBox7.TextChanged += new System.EventHandler(this.textBox7_TextChanged);
            // 
            // textBox5
            // 
            this.textBox5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox5.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox5.Location = new System.Drawing.Point(301, 14);
            this.textBox5.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(178, 27);
            this.textBox5.TabIndex = 5638;
            this.textBox5.TextChanged += new System.EventHandler(this.textBox5_TextChanged);
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.Location = new System.Drawing.Point(10, 16);
            this.label39.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(49, 19);
            this.label39.TabIndex = 5637;
            this.label39.Text = "SocNo";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.Location = new System.Drawing.Point(189, 19);
            this.label37.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(105, 19);
            this.label37.TabIndex = 431;
            this.label37.Text = "Work Order No";
            // 
            // button16
            // 
            this.button16.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button16.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button16.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button16.Location = new System.Drawing.Point(761, 317);
            this.button16.Name = "button16";
            this.button16.Size = new System.Drawing.Size(71, 30);
            this.button16.TabIndex = 396;
            this.button16.Text = "Select";
            this.button16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button16.UseVisualStyleBackColor = false;
            this.button16.Click += new System.EventHandler(this.button16_Click_1);
            // 
            // button17
            // 
            this.button17.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button17.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button17.Image = ((System.Drawing.Image)(resources.GetObject("button17.Image")));
            this.button17.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button17.Location = new System.Drawing.Point(8, 317);
            this.button17.Name = "button17";
            this.button17.Size = new System.Drawing.Size(73, 29);
            this.button17.TabIndex = 395;
            this.button17.Text = "Close";
            this.button17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button17.UseVisualStyleBackColor = false;
            this.button17.Click += new System.EventHandler(this.button17_Click_1);
            // 
            // txtscr11
            // 
            this.txtscr11.AcceptsReturn = true;
            this.txtscr11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtscr11.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtscr11.Location = new System.Drawing.Point(8, 50);
            this.txtscr11.Margin = new System.Windows.Forms.Padding(4);
            this.txtscr11.Name = "txtscr11";
            this.txtscr11.Size = new System.Drawing.Size(552, 26);
            this.txtscr11.TabIndex = 98;
            // 
            // HFG9
            // 
            this.HFG9.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.HFG9.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.HFG9.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.HFG9.Location = new System.Drawing.Point(8, 85);
            this.HFG9.Name = "HFG9";
            this.HFG9.Size = new System.Drawing.Size(824, 233);
            this.HFG9.TabIndex = 97;
            this.HFG9.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.HFG9_CellContentClick);
            this.HFG9.DoubleClick += new System.EventHandler(this.HFG9_DoubleClick_1);
            this.HFG9.KeyDown += new System.Windows.Forms.KeyEventHandler(this.HFG9_KeyDown_1);
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label69.ForeColor = System.Drawing.Color.White;
            this.label69.Location = new System.Drawing.Point(64, 85);
            this.label69.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(81, 21);
            this.label69.TabIndex = 196;
            this.label69.Text = "Itemname";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.ForeColor = System.Drawing.Color.White;
            this.label32.Location = new System.Drawing.Point(64, 184);
            this.label32.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(124, 21);
            this.label32.TabIndex = 198;
            this.label32.Text = "Press ESC to Exit";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(329, 17);
            this.label31.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(39, 19);
            this.label31.TabIndex = 5635;
            this.label31.Text = "Type";
            // 
            // cbotype
            // 
            this.cbotype.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbotype.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbotype.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbotype.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbotype.FormattingEnabled = true;
            this.cbotype.Location = new System.Drawing.Point(326, 42);
            this.cbotype.Name = "cbotype";
            this.cbotype.Size = new System.Drawing.Size(89, 27);
            this.cbotype.TabIndex = 5598;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(843, 17);
            this.label24.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(31, 19);
            this.label24.TabIndex = 5634;
            this.label24.Text = "Tax";
            // 
            // cbotax
            // 
            this.cbotax.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbotax.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbotax.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbotax.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbotax.FormattingEnabled = true;
            this.cbotax.Location = new System.Drawing.Point(843, 42);
            this.cbotax.Name = "cbotax";
            this.cbotax.Size = new System.Drawing.Size(97, 27);
            this.cbotax.TabIndex = 5610;
            this.cbotax.SelectedIndexChanged += new System.EventHandler(this.cbotax_SelectedIndexChanged_1);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(761, 17);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(39, 19);
            this.label8.TabIndex = 5630;
            this.label8.Text = "Rate";
            // 
            // txtprice
            // 
            this.txtprice.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtprice.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtprice.Location = new System.Drawing.Point(761, 42);
            this.txtprice.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtprice.Name = "txtprice";
            this.txtprice.Size = new System.Drawing.Size(81, 27);
            this.txtprice.TabIndex = 5608;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(574, 17);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(39, 19);
            this.label14.TabIndex = 5625;
            this.label14.Text = "Uom";
            // 
            // txtoutoutqty
            // 
            this.txtoutoutqty.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtoutoutqty.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtoutoutqty.Location = new System.Drawing.Point(574, 42);
            this.txtoutoutqty.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtoutoutqty.Name = "txtoutoutqty";
            this.txtoutoutqty.Size = new System.Drawing.Size(68, 27);
            this.txtoutoutqty.TabIndex = 5604;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(513, 17);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(60, 19);
            this.label5.TabIndex = 5615;
            this.label5.Text = "Knit Dia";
            // 
            // txtoutputitem
            // 
            this.txtoutputitem.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtoutputitem.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtoutputitem.Location = new System.Drawing.Point(513, 42);
            this.txtoutputitem.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtoutputitem.Name = "txtoutputitem";
            this.txtoutputitem.Size = new System.Drawing.Size(60, 27);
            this.txtoutputitem.TabIndex = 5602;
            // 
            // HFIT
            // 
            this.HFIT.BackgroundColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.HFIT.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.HFIT.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.HFIT.Location = new System.Drawing.Point(22, 75);
            this.HFIT.Name = "HFIT";
            this.HFIT.Size = new System.Drawing.Size(1041, 357);
            this.HFIT.TabIndex = 5603;
            // 
            // btnadd
            // 
            this.btnadd.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnadd.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnadd.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnadd.Location = new System.Drawing.Point(1020, 41);
            this.btnadd.Name = "btnadd";
            this.btnadd.Size = new System.Drawing.Size(36, 28);
            this.btnadd.TabIndex = 5612;
            this.btnadd.Text = "OK";
            this.btnadd.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnadd.UseVisualStyleBackColor = false;
            this.btnadd.Click += new System.EventHandler(this.btnadd_Click_1);
            // 
            // txtdcqty
            // 
            this.txtdcqty.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtdcqty.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtdcqty.Location = new System.Drawing.Point(25, 42);
            this.txtdcqty.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtdcqty.Name = "txtdcqty";
            this.txtdcqty.Size = new System.Drawing.Size(297, 27);
            this.txtdcqty.TabIndex = 5597;
            this.txtdcqty.Click += new System.EventHandler(this.txtdcqty_Click_1);
            this.txtdcqty.TextChanged += new System.EventHandler(this.txtdcqty_TextChanged_1);
            this.txtdcqty.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtdcqty_KeyDown_1);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(422, 2);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(53, 38);
            this.label7.TabIndex = 5609;
            this.label7.Text = "Loop \r\nLength";
            // 
            // txtitem
            // 
            this.txtitem.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtitem.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtitem.Location = new System.Drawing.Point(422, 41);
            this.txtitem.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtitem.Name = "txtitem";
            this.txtitem.Size = new System.Drawing.Size(85, 27);
            this.txtitem.TabIndex = 5600;
            // 
            // txtbeamid
            // 
            this.txtbeamid.Location = new System.Drawing.Point(683, 187);
            this.txtbeamid.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtbeamid.Name = "txtbeamid";
            this.txtbeamid.Size = new System.Drawing.Size(87, 27);
            this.txtbeamid.TabIndex = 5620;
            // 
            // txtoutputid
            // 
            this.txtoutputid.Location = new System.Drawing.Point(271, 187);
            this.txtoutputid.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtoutputid.Name = "txtoutputid";
            this.txtoutputid.Size = new System.Drawing.Size(87, 27);
            this.txtoutputid.TabIndex = 5619;
            // 
            // txtitemid
            // 
            this.txtitemid.Location = new System.Drawing.Point(804, 221);
            this.txtitemid.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtitemid.Name = "txtitemid";
            this.txtitemid.Size = new System.Drawing.Size(87, 27);
            this.txtitemid.TabIndex = 5618;
            // 
            // txtmillid
            // 
            this.txtmillid.Location = new System.Drawing.Point(420, 120);
            this.txtmillid.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtmillid.Name = "txtmillid";
            this.txtmillid.Size = new System.Drawing.Size(87, 27);
            this.txtmillid.TabIndex = 5617;
            // 
            // txtpuid
            // 
            this.txtpuid.Location = new System.Drawing.Point(405, 165);
            this.txtpuid.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtpuid.Name = "txtpuid";
            this.txtpuid.Size = new System.Drawing.Size(87, 27);
            this.txtpuid.TabIndex = 5616;
            // 
            // txtgrnid
            // 
            this.txtgrnid.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtgrnid.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtgrnid.Location = new System.Drawing.Point(372, 164);
            this.txtgrnid.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtgrnid.Name = "txtgrnid";
            this.txtgrnid.Size = new System.Drawing.Size(101, 22);
            this.txtgrnid.TabIndex = 5626;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(785, 174);
            this.label30.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(131, 19);
            this.label30.TabIndex = 433;
            this.label30.Text = "Mode Of Transport";
            // 
            // txtuom
            // 
            this.txtuom.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtuom.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtuom.Location = new System.Drawing.Point(644, 42);
            this.txtuom.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtuom.Name = "txtuom";
            this.txtuom.Size = new System.Drawing.Size(56, 27);
            this.txtuom.TabIndex = 5605;
            // 
            // textBox3
            // 
            this.textBox3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox3.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox3.Location = new System.Drawing.Point(779, 195);
            this.textBox3.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(142, 27);
            this.textBox3.TabIndex = 432;
            this.textBox3.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBox3_KeyDown);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button2.Location = new System.Drawing.Point(707, 102);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(34, 32);
            this.button2.TabIndex = 5607;
            this.button2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button2.UseVisualStyleBackColor = false;
            // 
            // HFGP1
            // 
            this.HFGP1.BackgroundColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.HFGP1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.HFGP1.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.HFGP1.Location = new System.Drawing.Point(550, 123);
            this.HFGP1.Name = "HFGP1";
            this.HFGP1.Size = new System.Drawing.Size(87, 18);
            this.HFGP1.TabIndex = 5614;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(494, 132);
            this.label17.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(98, 21);
            this.label17.TabIndex = 5624;
            this.label17.Text = "No of Beams";
            // 
            // txtrectot
            // 
            this.txtrectot.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtrectot.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtrectot.Location = new System.Drawing.Point(800, 102);
            this.txtrectot.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtrectot.Name = "txtrectot";
            this.txtrectot.Size = new System.Drawing.Size(90, 22);
            this.txtrectot.TabIndex = 5623;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(701, 17);
            this.label16.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(41, 19);
            this.label16.TabIndex = 5622;
            this.label16.Text = "Rolls";
            // 
            // txtisstot
            // 
            this.txtisstot.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtisstot.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtisstot.Location = new System.Drawing.Point(701, 42);
            this.txtisstot.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtisstot.Name = "txtisstot";
            this.txtisstot.Size = new System.Drawing.Size(59, 27);
            this.txtisstot.TabIndex = 5606;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(644, 17);
            this.label15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(32, 19);
            this.label15.TabIndex = 5621;
            this.label15.Text = "Qty";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(729, 163);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(39, 19);
            this.label10.TabIndex = 5628;
            this.label10.Text = "GSM";
            // 
            // txtmode
            // 
            this.txtmode.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtmode.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtmode.Location = new System.Drawing.Point(729, 188);
            this.txtmode.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtmode.Name = "txtmode";
            this.txtmode.Size = new System.Drawing.Size(58, 27);
            this.txtmode.TabIndex = 5601;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(942, 17);
            this.label18.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(41, 19);
            this.label18.TabIndex = 5627;
            this.label18.Text = "Total";
            // 
            // txtbags
            // 
            this.txtbags.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtbags.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtbags.Location = new System.Drawing.Point(942, 42);
            this.txtbags.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtbags.Name = "txtbags";
            this.txtbags.Size = new System.Drawing.Size(76, 27);
            this.txtbags.TabIndex = 5611;
            // 
            // button10
            // 
            this.button10.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button10.Location = new System.Drawing.Point(138, 336);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(266, 33);
            this.button10.TabIndex = 5633;
            this.button10.Text = "OUTPUT ITEM MAPPING";
            this.button10.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(24, 21);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(76, 19);
            this.label6.TabIndex = 5629;
            this.label6.Text = "ItemName";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(621, 107);
            this.label19.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(79, 21);
            this.label19.TabIndex = 5631;
            this.label19.Text = "AddNotes";
            // 
            // txtaddnotes
            // 
            this.txtaddnotes.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtaddnotes.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtaddnotes.Location = new System.Drawing.Point(632, 246);
            this.txtaddnotes.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtaddnotes.Name = "txtaddnotes";
            this.txtaddnotes.Size = new System.Drawing.Size(163, 22);
            this.txtaddnotes.TabIndex = 5613;
            // 
            // txtbeam
            // 
            this.txtbeam.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtbeam.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtbeam.Location = new System.Drawing.Point(925, 216);
            this.txtbeam.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtbeam.Name = "txtbeam";
            this.txtbeam.Size = new System.Drawing.Size(106, 22);
            this.txtbeam.TabIndex = 5599;
            // 
            // serialno
            // 
            this.serialno.Controls.Add(this.checkBox1);
            this.serialno.Controls.Add(this.label80);
            this.serialno.Controls.Add(this.Dataserial);
            this.serialno.Controls.Add(this.label78);
            this.serialno.Controls.Add(this.button20);
            this.serialno.Controls.Add(this.label76);
            this.serialno.Controls.Add(this.textBox6);
            this.serialno.Controls.Add(this.label79);
            this.serialno.Controls.Add(this.txtsrail);
            this.serialno.Controls.Add(this.cboserial);
            this.serialno.Controls.Add(this.label84);
            this.serialno.Controls.Add(this.txtserialqty);
            this.serialno.Controls.Add(this.label85);
            this.serialno.Controls.Add(this.button23);
            this.serialno.Controls.Add(this.label88);
            this.serialno.Controls.Add(this.textBox14);
            this.serialno.Controls.Add(this.textBox15);
            this.serialno.Controls.Add(this.txtsqty);
            this.serialno.Location = new System.Drawing.Point(481, 114);
            this.serialno.Name = "serialno";
            this.serialno.Size = new System.Drawing.Size(92, 40);
            this.serialno.TabIndex = 5632;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.BackColor = System.Drawing.Color.White;
            this.checkBox1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox1.Location = new System.Drawing.Point(696, 100);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(92, 22);
            this.checkBox1.TabIndex = 448;
            this.checkBox1.Text = "From BOM";
            this.checkBox1.UseVisualStyleBackColor = false;
            // 
            // label80
            // 
            this.label80.AutoSize = true;
            this.label80.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label80.Location = new System.Drawing.Point(200, 138);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(74, 18);
            this.label80.TabIndex = 447;
            this.label80.Text = "ItemName";
            // 
            // Dataserial
            // 
            this.Dataserial.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.Dataserial.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Dataserial.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.Dataserial.Location = new System.Drawing.Point(203, 201);
            this.Dataserial.Margin = new System.Windows.Forms.Padding(4);
            this.Dataserial.Name = "Dataserial";
            this.Dataserial.ReadOnly = true;
            this.Dataserial.Size = new System.Drawing.Size(471, 285);
            this.Dataserial.TabIndex = 446;
            // 
            // label78
            // 
            this.label78.AutoSize = true;
            this.label78.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label78.ForeColor = System.Drawing.Color.Red;
            this.label78.Location = new System.Drawing.Point(400, 53);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(202, 23);
            this.label78.TabIndex = 445;
            this.label78.Text = "OUTPUT ITEM MAPPING";
            // 
            // button20
            // 
            this.button20.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button20.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button20.Image = ((System.Drawing.Image)(resources.GetObject("button20.Image")));
            this.button20.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button20.Location = new System.Drawing.Point(701, 288);
            this.button20.Margin = new System.Windows.Forms.Padding(4);
            this.button20.Name = "button20";
            this.button20.Size = new System.Drawing.Size(60, 30);
            this.button20.TabIndex = 444;
            this.button20.Text = "Back";
            this.button20.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button20.UseVisualStyleBackColor = false;
            // 
            // label76
            // 
            this.label76.AutoSize = true;
            this.label76.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label76.Location = new System.Drawing.Point(590, 138);
            this.label76.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(30, 18);
            this.label76.TabIndex = 439;
            this.label76.Text = "Qty";
            // 
            // textBox6
            // 
            this.textBox6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox6.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox6.Location = new System.Drawing.Point(597, 161);
            this.textBox6.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(69, 26);
            this.textBox6.TabIndex = 430;
            this.textBox6.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label79
            // 
            this.label79.AutoSize = true;
            this.label79.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label79.Location = new System.Drawing.Point(394, 234);
            this.label79.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(41, 18);
            this.label79.TabIndex = 429;
            this.label79.Text = "UOM";
            // 
            // txtsrail
            // 
            this.txtsrail.Location = new System.Drawing.Point(203, 166);
            this.txtsrail.Name = "txtsrail";
            this.txtsrail.Size = new System.Drawing.Size(382, 27);
            this.txtsrail.TabIndex = 1;
            // 
            // cboserial
            // 
            this.cboserial.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboserial.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboserial.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboserial.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboserial.FormattingEnabled = true;
            this.cboserial.Location = new System.Drawing.Point(203, 98);
            this.cboserial.Name = "cboserial";
            this.cboserial.Size = new System.Drawing.Size(471, 26);
            this.cboserial.TabIndex = 416;
            // 
            // label84
            // 
            this.label84.AutoSize = true;
            this.label84.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label84.Location = new System.Drawing.Point(346, 203);
            this.label84.Name = "label84";
            this.label84.Size = new System.Drawing.Size(30, 18);
            this.label84.TabIndex = 402;
            this.label84.Text = "Qty";
            // 
            // txtserialqty
            // 
            this.txtserialqty.Location = new System.Drawing.Point(215, 236);
            this.txtserialqty.Name = "txtserialqty";
            this.txtserialqty.Size = new System.Drawing.Size(149, 27);
            this.txtserialqty.TabIndex = 403;
            // 
            // label85
            // 
            this.label85.AutoSize = true;
            this.label85.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label85.Location = new System.Drawing.Point(204, 74);
            this.label85.Name = "label85";
            this.label85.Size = new System.Drawing.Size(37, 18);
            this.label85.TabIndex = 400;
            this.label85.Text = "Item";
            // 
            // button23
            // 
            this.button23.Location = new System.Drawing.Point(690, 160);
            this.button23.Name = "button23";
            this.button23.Size = new System.Drawing.Size(40, 28);
            this.button23.TabIndex = 2;
            this.button23.Text = "OK";
            this.button23.UseVisualStyleBackColor = true;
            // 
            // label88
            // 
            this.label88.AutoSize = true;
            this.label88.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label88.Location = new System.Drawing.Point(237, -14);
            this.label88.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label88.Name = "label88";
            this.label88.Size = new System.Drawing.Size(27, 15);
            this.label88.TabIndex = 359;
            this.label88.Text = "Qty";
            // 
            // textBox14
            // 
            this.textBox14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox14.Enabled = false;
            this.textBox14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox14.Location = new System.Drawing.Point(339, 239);
            this.textBox14.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.textBox14.Name = "textBox14";
            this.textBox14.Size = new System.Drawing.Size(91, 22);
            this.textBox14.TabIndex = 435;
            this.textBox14.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBox15
            // 
            this.textBox15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox15.Enabled = false;
            this.textBox15.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox15.Location = new System.Drawing.Point(340, 267);
            this.textBox15.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.textBox15.Name = "textBox15";
            this.textBox15.Size = new System.Drawing.Size(90, 22);
            this.textBox15.TabIndex = 431;
            this.textBox15.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtsqty
            // 
            this.txtsqty.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtsqty.Enabled = false;
            this.txtsqty.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtsqty.Location = new System.Drawing.Point(444, 303);
            this.txtsqty.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtsqty.Name = "txtsqty";
            this.txtsqty.Size = new System.Drawing.Size(89, 22);
            this.txtsqty.TabIndex = 404;
            this.txtsqty.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // CBOWNO
            // 
            this.CBOWNO.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CBOWNO.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CBOWNO.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CBOWNO.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CBOWNO.FormattingEnabled = true;
            this.CBOWNO.Location = new System.Drawing.Point(885, 246);
            this.CBOWNO.Name = "CBOWNO";
            this.CBOWNO.Size = new System.Drawing.Size(167, 27);
            this.CBOWNO.TabIndex = 430;
            this.CBOWNO.SelectedIndexChanged += new System.EventHandler(this.CBOWNO_SelectedIndexChanged);
            // 
            // tabPage3
            // 
            this.tabPage3.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.tabPage3.Controls.Add(this.label35);
            this.tabPage3.Controls.Add(this.txtreqrty);
            this.tabPage3.Controls.Add(this.label26);
            this.tabPage3.Controls.Add(this.label25);
            this.tabPage3.Controls.Add(this.stpreq);
            this.tabPage3.Controls.Add(this.HFTP);
            this.tabPage3.Controls.Add(this.txtreqty);
            this.tabPage3.Controls.Add(this.label23);
            this.tabPage3.Controls.Add(this.cbosGReturnItem);
            this.tabPage3.Controls.Add(this.label67);
            this.tabPage3.Controls.Add(this.txtqty);
            this.tabPage3.Controls.Add(this.button12);
            this.tabPage3.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabPage3.Location = new System.Drawing.Point(4, 28);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(1071, 439);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Required date";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.Location = new System.Drawing.Point(616, 12);
            this.label35.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(70, 21);
            this.label35.TabIndex = 434;
            this.label35.Text = "Remarks";
            // 
            // txtreqrty
            // 
            this.txtreqrty.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtreqrty.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtreqrty.Location = new System.Drawing.Point(294, 104);
            this.txtreqrty.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtreqrty.Name = "txtreqrty";
            this.txtreqrty.Size = new System.Drawing.Size(147, 27);
            this.txtreqrty.TabIndex = 430;
            this.txtreqrty.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtreqrty_KeyDown);
            this.txtreqrty.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtreqrty_KeyPress);
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(290, 81);
            this.label26.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(32, 19);
            this.label26.TabIndex = 431;
            this.label26.Text = "Qty";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(166, 81);
            this.label25.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(102, 19);
            this.label25.TabIndex = 429;
            this.label25.Text = "Required Date";
            // 
            // stpreq
            // 
            this.stpreq.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.stpreq.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.stpreq.Location = new System.Drawing.Point(167, 105);
            this.stpreq.Name = "stpreq";
            this.stpreq.Size = new System.Drawing.Size(122, 27);
            this.stpreq.TabIndex = 428;
            this.stpreq.KeyDown += new System.Windows.Forms.KeyEventHandler(this.stpreq_KeyDown);
            // 
            // HFTP
            // 
            this.HFTP.BackgroundColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.HFTP.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.HFTP.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.HFTP.Location = new System.Drawing.Point(167, 133);
            this.HFTP.Name = "HFTP";
            this.HFTP.Size = new System.Drawing.Size(420, 204);
            this.HFTP.TabIndex = 427;
            // 
            // txtreqty
            // 
            this.txtreqty.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtreqty.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtreqty.Location = new System.Drawing.Point(532, 34);
            this.txtreqty.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtreqty.Name = "txtreqty";
            this.txtreqty.Size = new System.Drawing.Size(71, 27);
            this.txtreqty.TabIndex = 425;
            this.txtreqty.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtreqty_KeyPress);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(528, 12);
            this.label23.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(35, 21);
            this.label23.TabIndex = 426;
            this.label23.Text = "Qty";
            // 
            // cbosGReturnItem
            // 
            this.cbosGReturnItem.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbosGReturnItem.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbosGReturnItem.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbosGReturnItem.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbosGReturnItem.FormattingEnabled = true;
            this.cbosGReturnItem.Location = new System.Drawing.Point(167, 34);
            this.cbosGReturnItem.Name = "cbosGReturnItem";
            this.cbosGReturnItem.Size = new System.Drawing.Size(358, 27);
            this.cbosGReturnItem.TabIndex = 424;
            this.cbosGReturnItem.SelectedIndexChanged += new System.EventHandler(this.cbosGReturnItem_SelectedIndexChanged);
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label67.Location = new System.Drawing.Point(169, 13);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(37, 18);
            this.label67.TabIndex = 423;
            this.label67.Text = "Item";
            // 
            // txtqty
            // 
            this.txtqty.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtqty.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtqty.Location = new System.Drawing.Point(618, 34);
            this.txtqty.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtqty.Name = "txtqty";
            this.txtqty.Size = new System.Drawing.Size(445, 27);
            this.txtqty.TabIndex = 191;
            this.txtqty.TextChanged += new System.EventHandler(this.txtqty_TextChanged);
            this.txtqty.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtqty_KeyDown);
            // 
            // button12
            // 
            this.button12.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button12.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button12.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button12.Location = new System.Drawing.Point(448, 104);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(27, 26);
            this.button12.TabIndex = 432;
            this.button12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button12.UseVisualStyleBackColor = false;
            this.button12.Click += new System.EventHandler(this.button12_Click);
            // 
            // cboprocess
            // 
            this.cboprocess.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.cboprocess.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboprocess.Location = new System.Drawing.Point(396, 84);
            this.cboprocess.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cboprocess.Name = "cboprocess";
            this.cboprocess.Size = new System.Drawing.Size(221, 27);
            this.cboprocess.TabIndex = 425;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(401, 63);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(59, 19);
            this.label4.TabIndex = 9;
            this.label4.Text = "Prcoess";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(131, 6);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(69, 19);
            this.label2.TabIndex = 189;
            this.label2.Text = "Doc Date";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(263, 6);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(103, 19);
            this.label3.TabIndex = 186;
            this.label3.Text = "Supplier Name";
            // 
            // Phone
            // 
            this.Phone.AutoSize = true;
            this.Phone.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Phone.Location = new System.Drawing.Point(14, 6);
            this.Phone.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Phone.Name = "Phone";
            this.Phone.Size = new System.Drawing.Size(52, 19);
            this.Phone.TabIndex = 188;
            this.Phone.Text = "DocNo";
            // 
            // txtgrn
            // 
            this.txtgrn.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtgrn.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtgrn.Location = new System.Drawing.Point(19, 28);
            this.txtgrn.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtgrn.Name = "txtgrn";
            this.txtgrn.Size = new System.Drawing.Size(110, 27);
            this.txtgrn.TabIndex = 187;
            // 
            // dtpgrndt
            // 
            this.dtpgrndt.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpgrndt.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpgrndt.Location = new System.Drawing.Point(135, 28);
            this.dtpgrndt.Name = "dtpgrndt";
            this.dtpgrndt.Size = new System.Drawing.Size(122, 27);
            this.dtpgrndt.TabIndex = 189;
            this.dtpgrndt.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dtpgrndt_KeyDown);
            // 
            // txtname
            // 
            this.txtname.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtname.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtname.Location = new System.Drawing.Point(267, 28);
            this.txtname.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtname.Name = "txtname";
            this.txtname.Size = new System.Drawing.Size(400, 27);
            this.txtname.TabIndex = 190;
            this.txtname.Click += new System.EventHandler(this.Txtname_Click);
            this.txtname.TextChanged += new System.EventHandler(this.Txtname_TextChanged);
            this.txtname.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Txtname_KeyDown);
            // 
            // txtnobeams
            // 
            this.txtnobeams.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtnobeams.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtnobeams.Location = new System.Drawing.Point(675, 29);
            this.txtnobeams.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtnobeams.Name = "txtnobeams";
            this.txtnobeams.Size = new System.Drawing.Size(134, 27);
            this.txtnobeams.TabIndex = 228;
            this.txtnobeams.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtnobeams_KeyDown);
            // 
            // txtseqno
            // 
            this.txtseqno.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtseqno.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtseqno.Location = new System.Drawing.Point(630, 84);
            this.txtseqno.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtseqno.Name = "txtseqno";
            this.txtseqno.Size = new System.Drawing.Size(59, 27);
            this.txtseqno.TabIndex = 435;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(626, 63);
            this.label33.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(50, 19);
            this.label33.TabIndex = 434;
            this.label33.Text = "SeqNo";
            this.label33.Click += new System.EventHandler(this.label33_Click);
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(681, 8);
            this.label29.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(78, 19);
            this.label29.TabIndex = 431;
            this.label29.Text = "Vehicle No";
            // 
            // panadd
            // 
            this.panadd.BackColor = System.Drawing.Color.White;
            this.panadd.Controls.Add(this.button11);
            this.panadd.Controls.Add(this.button3);
            this.panadd.Controls.Add(this.panel3);
            this.panadd.Controls.Add(this.buttnnvfst);
            this.panadd.Controls.Add(this.buttnnxtlft);
            this.panadd.Controls.Add(this.btnfinnxt);
            this.panadd.Controls.Add(this.buttrnxt);
            this.panadd.Controls.Add(this.buttnext1);
            this.panadd.Controls.Add(this.chkact);
            this.panadd.Controls.Add(this.butexit);
            this.panadd.Controls.Add(this.butedit);
            this.panadd.Controls.Add(this.button1);
            this.panadd.Location = new System.Drawing.Point(-1, 554);
            this.panadd.Name = "panadd";
            this.panadd.Size = new System.Drawing.Size(1096, 31);
            this.panadd.TabIndex = 217;
            // 
            // button11
            // 
            this.button11.BackColor = System.Drawing.Color.White;
            this.button11.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button11.Image = ((System.Drawing.Image)(resources.GetObject("button11.Image")));
            this.button11.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button11.Location = new System.Drawing.Point(567, 1);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(121, 31);
            this.button11.TabIndex = 216;
            this.button11.Text = "Print Preview";
            this.button11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button11.UseVisualStyleBackColor = false;
            this.button11.Click += new System.EventHandler(this.button11_Click);
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.White;
            this.button3.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.Image = ((System.Drawing.Image)(resources.GetObject("button3.Image")));
            this.button3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button3.Location = new System.Drawing.Point(502, 1);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(64, 30);
            this.button3.TabIndex = 215;
            this.button3.Text = "Print";
            this.button3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel3.Controls.Add(this.lblno1);
            this.panel3.Controls.Add(this.lblno2);
            this.panel3.Controls.Add(this.flowLayoutPanel3);
            this.panel3.Controls.Add(this.flowLayoutPanel2);
            this.panel3.Controls.Add(this.flowLayoutPanel1);
            this.panel3.Location = new System.Drawing.Point(64, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(74, 30);
            this.panel3.TabIndex = 214;
            // 
            // lblno1
            // 
            this.lblno1.AutoSize = true;
            this.lblno1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblno1.ForeColor = System.Drawing.Color.Black;
            this.lblno1.Location = new System.Drawing.Point(4, 5);
            this.lblno1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblno1.Name = "lblno1";
            this.lblno1.Size = new System.Drawing.Size(15, 16);
            this.lblno1.TabIndex = 163;
            this.lblno1.Text = "1";
            // 
            // lblno2
            // 
            this.lblno2.AutoSize = true;
            this.lblno2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblno2.ForeColor = System.Drawing.Color.Black;
            this.lblno2.Location = new System.Drawing.Point(27, 5);
            this.lblno2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblno2.Name = "lblno2";
            this.lblno2.Size = new System.Drawing.Size(29, 16);
            this.lblno2.TabIndex = 162;
            this.lblno2.Text = "of 1";
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.Location = new System.Drawing.Point(61, 30);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(200, 100);
            this.flowLayoutPanel3.TabIndex = 2;
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.Location = new System.Drawing.Point(38, 35);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(200, 100);
            this.flowLayoutPanel2.TabIndex = 1;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Location = new System.Drawing.Point(62, -129);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(200, 100);
            this.flowLayoutPanel1.TabIndex = 0;
            // 
            // buttnnvfst
            // 
            this.buttnnvfst.BackColor = System.Drawing.Color.White;
            this.buttnnvfst.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.buttnnvfst.FlatAppearance.BorderSize = 0;
            this.buttnnvfst.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttnnvfst.Image = ((System.Drawing.Image)(resources.GetObject("buttnnvfst.Image")));
            this.buttnnvfst.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttnnvfst.Location = new System.Drawing.Point(6, 0);
            this.buttnnvfst.Name = "buttnnvfst";
            this.buttnnvfst.Size = new System.Drawing.Size(19, 31);
            this.buttnnvfst.TabIndex = 213;
            this.buttnnvfst.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttnnvfst.UseVisualStyleBackColor = false;
            // 
            // buttnnxtlft
            // 
            this.buttnnxtlft.BackColor = System.Drawing.Color.White;
            this.buttnnxtlft.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.buttnnxtlft.FlatAppearance.BorderSize = 0;
            this.buttnnxtlft.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttnnxtlft.Image = ((System.Drawing.Image)(resources.GetObject("buttnnxtlft.Image")));
            this.buttnnxtlft.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttnnxtlft.Location = new System.Drawing.Point(40, 0);
            this.buttnnxtlft.Name = "buttnnxtlft";
            this.buttnnxtlft.Size = new System.Drawing.Size(18, 31);
            this.buttnnxtlft.TabIndex = 212;
            this.buttnnxtlft.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttnnxtlft.UseVisualStyleBackColor = false;
            // 
            // btnfinnxt
            // 
            this.btnfinnxt.BackColor = System.Drawing.Color.White;
            this.btnfinnxt.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnfinnxt.FlatAppearance.BorderSize = 0;
            this.btnfinnxt.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnfinnxt.Image = ((System.Drawing.Image)(resources.GetObject("btnfinnxt.Image")));
            this.btnfinnxt.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnfinnxt.Location = new System.Drawing.Point(178, 0);
            this.btnfinnxt.Name = "btnfinnxt";
            this.btnfinnxt.Size = new System.Drawing.Size(19, 31);
            this.btnfinnxt.TabIndex = 211;
            this.btnfinnxt.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnfinnxt.UseVisualStyleBackColor = false;
            // 
            // buttrnxt
            // 
            this.buttrnxt.BackColor = System.Drawing.Color.White;
            this.buttrnxt.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.buttrnxt.FlatAppearance.BorderSize = 0;
            this.buttrnxt.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttrnxt.Image = ((System.Drawing.Image)(resources.GetObject("buttrnxt.Image")));
            this.buttrnxt.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttrnxt.Location = new System.Drawing.Point(144, 0);
            this.buttrnxt.Name = "buttrnxt";
            this.buttrnxt.Size = new System.Drawing.Size(18, 31);
            this.buttrnxt.TabIndex = 210;
            this.buttrnxt.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttrnxt.UseVisualStyleBackColor = false;
            // 
            // buttnext1
            // 
            this.buttnext1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttnext1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttnext1.Image = ((System.Drawing.Image)(resources.GetObject("buttnext1.Image")));
            this.buttnext1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttnext1.Location = new System.Drawing.Point(689, 2);
            this.buttnext1.Name = "buttnext1";
            this.buttnext1.Size = new System.Drawing.Size(57, 30);
            this.buttnext1.TabIndex = 208;
            this.buttnext1.Text = "Exit";
            this.buttnext1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttnext1.UseVisualStyleBackColor = false;
            this.buttnext1.Click += new System.EventHandler(this.Buttnext1_Click);
            // 
            // chkact
            // 
            this.chkact.AutoSize = true;
            this.chkact.BackColor = System.Drawing.Color.White;
            this.chkact.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkact.Location = new System.Drawing.Point(223, 7);
            this.chkact.Name = "chkact";
            this.chkact.Size = new System.Drawing.Size(65, 22);
            this.chkact.TabIndex = 187;
            this.chkact.Text = "Active";
            this.chkact.UseVisualStyleBackColor = false;
            this.chkact.CheckedChanged += new System.EventHandler(this.chkact_CheckedChanged);
            // 
            // butexit
            // 
            this.butexit.BackColor = System.Drawing.Color.White;
            this.butexit.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.butexit.Image = ((System.Drawing.Image)(resources.GetObject("butexit.Image")));
            this.butexit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.butexit.Location = new System.Drawing.Point(432, 1);
            this.butexit.Name = "butexit";
            this.butexit.Size = new System.Drawing.Size(69, 30);
            this.butexit.TabIndex = 186;
            this.butexit.Text = "Delete";
            this.butexit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.butexit.UseVisualStyleBackColor = false;
            this.butexit.Click += new System.EventHandler(this.butexit_Click);
            // 
            // butedit
            // 
            this.butedit.BackColor = System.Drawing.Color.White;
            this.butedit.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.butedit.Image = ((System.Drawing.Image)(resources.GetObject("butedit.Image")));
            this.butedit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.butedit.Location = new System.Drawing.Point(378, 1);
            this.butedit.Name = "butedit";
            this.butedit.Size = new System.Drawing.Size(55, 30);
            this.butedit.TabIndex = 185;
            this.butedit.Text = "Edit";
            this.butedit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.butedit.UseVisualStyleBackColor = false;
            this.butedit.Click += new System.EventHandler(this.butedit_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.White;
            this.button1.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Image = ((System.Drawing.Image)(resources.GetObject("button1.Image")));
            this.button1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button1.Location = new System.Drawing.Point(290, 1);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(87, 30);
            this.button1.TabIndex = 184;
            this.button1.Text = "Add New";
            this.button1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Chkedtact
            // 
            this.Chkedtact.AutoSize = true;
            this.Chkedtact.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Chkedtact.Location = new System.Drawing.Point(846, 558);
            this.Chkedtact.Name = "Chkedtact";
            this.Chkedtact.Size = new System.Drawing.Size(62, 20);
            this.Chkedtact.TabIndex = 220;
            this.Chkedtact.Text = "Active";
            this.Chkedtact.UseVisualStyleBackColor = true;
            // 
            // btnaddrcan
            // 
            this.btnaddrcan.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnaddrcan.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnaddrcan.Image = ((System.Drawing.Image)(resources.GetObject("btnaddrcan.Image")));
            this.btnaddrcan.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnaddrcan.Location = new System.Drawing.Point(975, 554);
            this.btnaddrcan.Name = "btnaddrcan";
            this.btnaddrcan.Size = new System.Drawing.Size(60, 29);
            this.btnaddrcan.TabIndex = 219;
            this.btnaddrcan.Text = "Back";
            this.btnaddrcan.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnaddrcan.UseVisualStyleBackColor = false;
            this.btnaddrcan.Click += new System.EventHandler(this.Btnaddrcan_Click);
            // 
            // btnsave
            // 
            this.btnsave.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnsave.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnsave.Image = ((System.Drawing.Image)(resources.GetObject("btnsave.Image")));
            this.btnsave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnsave.Location = new System.Drawing.Point(914, 553);
            this.btnsave.Name = "btnsave";
            this.btnsave.Size = new System.Drawing.Size(60, 30);
            this.btnsave.TabIndex = 218;
            this.btnsave.Text = "Save";
            this.btnsave.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnsave.UseVisualStyleBackColor = false;
            this.btnsave.Click += new System.EventHandler(this.btnsave_Click);
            // 
            // button9
            // 
            this.button9.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button9.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button9.Image = ((System.Drawing.Image)(resources.GetObject("button9.Image")));
            this.button9.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button9.Location = new System.Drawing.Point(1037, 554);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(54, 29);
            this.button9.TabIndex = 221;
            this.button9.Text = "Exit";
            this.button9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button9.UseVisualStyleBackColor = false;
            this.button9.Click += new System.EventHandler(this.Button9_Click);
            // 
            // FrmIssueK
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1095, 586);
            this.Controls.Add(this.panadd);
            this.Controls.Add(this.Chkedtact);
            this.Controls.Add(this.btnaddrcan);
            this.Controls.Add(this.btnsave);
            this.Controls.Add(this.button9);
            this.Controls.Add(this.Genpan);
            this.Controls.Add(this.Editpnl);
            this.Name = "FrmIssueK";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = " Knitting Delivery";
            this.Load += new System.EventHandler(this.FrmIssueK_Load);
            this.Genpan.ResumeLayout(false);
            this.Genpan.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HFGP)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.Editpnl.ResumeLayout(false);
            this.Editpnl.PerformLayout();
            this.grSearch.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DataGridCommon)).EndInit();
            this.tabC.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.lkppnl.ResumeLayout(false);
            this.lkppnl.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HFGP2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RQGR)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HFG9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HFIT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HFGP1)).EndInit();
            this.serialno.ResumeLayout(false);
            this.serialno.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Dataserial)).EndInit();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HFTP)).EndInit();
            this.panadd.ResumeLayout(false);
            this.panadd.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel Genpan;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox txtscr6;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtscr5;
        private System.Windows.Forms.TextBox txtscr4;
        private System.Windows.Forms.TextBox Txtscr3;
        private System.Windows.Forms.TextBox Txtscr2;
        private System.Windows.Forms.TextBox txtscr1;
        private System.Windows.Forms.DataGridView HFGP;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel4;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel5;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel6;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.DateTimePicker dtpfnt;
        private System.Windows.Forms.TextBox txtscr8;
        private System.Windows.Forms.TextBox txtscr7;
        private System.Windows.Forms.Panel Editpnl;
        private System.Windows.Forms.TextBox cboprocess;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtqty;
        private System.Windows.Forms.TextBox txtvehicle;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label Phone;
        private System.Windows.Forms.TextBox txtgrn;
        private System.Windows.Forms.DateTimePicker dtpgrndt;
        private System.Windows.Forms.TextBox txtname;
        private System.Windows.Forms.TabControl tabC;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel grSearch;
        private System.Windows.Forms.Button button18;
        private System.Windows.Forms.Button btnHide;
        private System.Windows.Forms.DataGridView DataGridCommon;
        private System.Windows.Forms.TextBox txtnobeams;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.TextBox txtreqrty;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.DateTimePicker stpreq;
        private System.Windows.Forms.DataGridView HFTP;
        private System.Windows.Forms.TextBox txtreqty;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.ComboBox cbosGReturnItem;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.Panel panadd;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label lblno1;
        private System.Windows.Forms.Label lblno2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Button buttnnvfst;
        private System.Windows.Forms.Button buttnnxtlft;
        private System.Windows.Forms.Button btnfinnxt;
        private System.Windows.Forms.Button buttrnxt;
        private System.Windows.Forms.Button buttnext1;
        private System.Windows.Forms.CheckBox chkact;
        private System.Windows.Forms.Button butexit;
        private System.Windows.Forms.Button butedit;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btnaddrcan;
        private System.Windows.Forms.Button btnsave;
        private System.Windows.Forms.CheckBox Chkedtact;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.DateTimePicker reqdt;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.TextBox txtseqno;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.TextBox txtwref;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Button button22;
        private System.Windows.Forms.TextBox txtoutuom;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Panel lkppnl;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.Button button14;
        private System.Windows.Forms.Button button15;
        private System.Windows.Forms.DataGridView HFGP2;
        private System.Windows.Forms.DataGridView RQGR;
        private System.Windows.Forms.TextBox txtoutqty;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox txtoutrolls;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox txtoutitem;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Button button16;
        private System.Windows.Forms.Button button17;
        private System.Windows.Forms.TextBox txtscr11;
        private System.Windows.Forms.DataGridView HFG9;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.ComboBox cbotype;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.ComboBox cbotax;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtprice;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtoutoutqty;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtoutputitem;
        private System.Windows.Forms.DataGridView HFIT;
        private System.Windows.Forms.Button btnadd;
        private System.Windows.Forms.TextBox txtdcqty;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtitem;
        private System.Windows.Forms.TextBox txtbeamid;
        private System.Windows.Forms.TextBox txtoutputid;
        private System.Windows.Forms.TextBox txtitemid;
        private System.Windows.Forms.TextBox txtmillid;
        private System.Windows.Forms.TextBox txtpuid;
        private System.Windows.Forms.TextBox txtgrnid;
        private System.Windows.Forms.TextBox txtuom;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.DataGridView HFGP1;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtrectot;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtisstot;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtmode;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txtbags;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txtaddnotes;
        private System.Windows.Forms.TextBox txtbeam;
        private System.Windows.Forms.Panel serialno;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.Label label80;
        private System.Windows.Forms.DataGridView Dataserial;
        private System.Windows.Forms.Label label78;
        private System.Windows.Forms.Button button20;
        private System.Windows.Forms.Label label76;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.Label label79;
        private System.Windows.Forms.TextBox txtsrail;
        private System.Windows.Forms.ComboBox cboserial;
        private System.Windows.Forms.Label label84;
        private System.Windows.Forms.TextBox txtserialqty;
        private System.Windows.Forms.Label label85;
        private System.Windows.Forms.Button button23;
        private System.Windows.Forms.Label label88;
        private System.Windows.Forms.TextBox textBox14;
        private System.Windows.Forms.TextBox textBox15;
        private System.Windows.Forms.TextBox txtsqty;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.ComboBox CBOWNO;
        private System.Windows.Forms.TextBox txtworder;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.TextBox cbowono;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.TextBox textBox7;
    }
}