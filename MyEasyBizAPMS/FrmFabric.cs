﻿using Syncfusion.WinForms.DataGrid;
using Syncfusion.WinForms.DataGrid.Events;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Windows.Forms;

namespace MyEasyBizAPMS
{
    public partial class FrmFabric : Form
    {
        public FrmFabric()
        {
            InitializeComponent();
            this.sfDataGridFabric.Style.HeaderStyle.Font.FontStyle = FontStyle.Bold;
            this.sfDataGridFabric.Style.HeaderStyle.BackColor = Color.DeepSkyBlue;
        }
        SQLDBHelper db = new SQLDBHelper();
        SqlConnection conn = new SqlConnection(GeneralParameters.ConnectionString);
        RowAutoFitOptions autoFitOptions = new RowAutoFitOptions();
        List<string> excludeColumns = new List<string>();

        private void FrmFabric_Load(object sender, EventArgs e)
        {
            LoadStructureType();
            LoadButton(0);
            LoadYarnGridView();
            LoadFabric();
            Genclass.buttonstyleform(this);
            Genclass.buttonstylepanel(panadd);
            LoadFabricType();
            LoadYarn();
        }

        protected void LoadStructureType()
        {
            try
            {
                SqlParameter[] parameters = { new SqlParameter("@TypeMUid", 10), new SqlParameter("@Active", 1) };
                DataTable dt = db.GetDataWithParam(CommandType.StoredProcedure, "SP_GetGeneralM", parameters, conn);
                CmbStrucType.DataSource = null;
                CmbStrucType.DisplayMember = "GeneralName";
                CmbStrucType.ValueMember = "GUid";
                CmbStrucType.DataSource = dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void LoadFabricType()
        {
            try
            {
                SqlParameter[] parameters = { new SqlParameter("@TypeMUid", 12), new SqlParameter("@Active", 1) };
                DataTable dt = db.GetDataWithParam(CommandType.StoredProcedure, "SP_GetGeneralM", parameters, conn);
                CmbFabricType.DataSource = null;
                CmbFabricType.DisplayMember = "GeneralName";
                CmbFabricType.ValueMember = "GUid";
                CmbFabricType.DataSource = dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void CmbStrucType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string Query = "Select GUid,GeneralName from GeneralM Where TypeMUid = 11";
                DataTable dt = db.GetDataWithoutParam(CommandType.Text, Query, conn);
                cBoxStructure.LoadGrid(dt);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected void BuildName()
        {
            try
            {
                TxtName.Text = string.Empty;
                string FabricType = string.Empty;
                if (CmbFabricType.Text == "YARN DYED")
                {
                    FabricType = "Y/D";
                }
                else
                {
                    FabricType = CmbFabricType.Text;
                }
                TxtName.Text = CmbStrucType.Text + " " + cBoxStructure.textBox1.Text + " " + FabricType;
                string t = string.Empty;
                if (CmbFabricType.Text == "SOLID" || CmbFabricType.Text == "MELANGE")
                {
                    for (int i = 0; i < DataGridYarn.Rows.Count; i++)
                    {
                        if (i == 0)
                        {
                            t = DataGridYarn.Rows[i].Cells[1].Value.ToString() + " " + DataGridYarn.Rows[i].Cells[2].Value.ToString() + " %"; ;
                        }
                        else
                        {
                            t = t + " " + DataGridYarn.Rows[i].Cells[1].Value.ToString() + " " + DataGridYarn.Rows[i].Cells[2].Value.ToString() + " %"; ;
                        }
                    }
                }
                else
                {
                    for (int i = 0; i < DataGridYarn.Rows.Count; i++)
                    {
                        if (i == 0)
                        {
                            t = DataGridYarn.Rows[i].Cells[1].Value.ToString() + " "; 
                        }
                        else
                        {
                            t = t + " " + DataGridYarn.Rows[i].Cells[1].Value.ToString() + " ";
                        }
                    }
                    //t = CmbYarn.Text;
                }
                TxtName.Text = TxtName.Text + " ( " + t + " )";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void CmbFabricType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BuildName();
                if (CmbFabricType.Text == "MELANGE")
                {
                    DataGridYarn.Visible = true;
                    txtPer.Visible = true;
                    CmbYarn.Visible = true;
                    button1.Visible = true;
                }
                else if (CmbFabricType.Text == "SOLID")
                {
                    DataGridYarn.Visible = true;
                    txtPer.Visible = true;
                    CmbYarn.Visible = true;
                    button1.Visible = true;
                }
                else
                {
                    DataGridYarn.Visible = true;
                    txtPer.Visible = false;
                    CmbYarn.Visible = true;
                    button1.Visible = true;
                    DataGridYarn.Columns[2].Visible = false;
                    //CmbYarn.Visible = true;
                    //txtPer.Visible = true;
                    //DataGridYarn.Visible = false;
                    //button1.Visible = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected void LoadButton(int id)
        {
            try
            {
                if (id == 0)
                {
                    GrBack.Visible = false;
                    GrFront.Visible = true;
                    btnAdd.Visible = true;
                    btnEdit.Visible = true;
                    btnExit.Visible = true;
                    btnFirst.Visible = true;
                    btnBack.Visible = true;
                    btnNxt.Visible = true;
                    btnLast.Visible = true;
                    PanelgridNos.Visible = true;
                    btnSave.Visible = false;
                    btnAddCancel.Visible = false;
                    btnSave.Text = "Save";
                    chckAc.Visible = true;
                }
                else if (id == 1)
                {
                    GrBack.Visible = true;
                    GrFront.Visible = false;
                    btnAdd.Visible = false;
                    btnEdit.Visible = false;
                    btnExit.Visible = false;
                    btnFirst.Visible = false;
                    btnBack.Visible = false;
                    btnNxt.Visible = false;
                    btnLast.Visible = false;
                    PanelgridNos.Visible = false;
                    btnSave.Visible = true;
                    btnAddCancel.Visible = true;
                    btnSave.Text = "Save";
                    chckAc.Visible = false;
                }

                else if (id == 2)
                {
                    GrBack.Visible = true;
                    GrFront.Visible = false;
                    btnAdd.Visible = false;
                    btnEdit.Visible = false;
                    btnExit.Visible = false;
                    btnFirst.Visible = false;
                    btnBack.Visible = false;
                    btnNxt.Visible = false;
                    btnLast.Visible = false;
                    PanelgridNos.Visible = false;
                    btnSave.Visible = true;
                    btnAddCancel.Visible = true;
                    btnSave.Text = "Update";
                    chckAc.Visible = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            ClearTextBoxes();
            DataGridYarn.Rows.Clear();
            LoadButton(1);
            TxtName.Tag = "0";
        }

        private void BtnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BtnAddCancel_Click(object sender, EventArgs e)
        {
            LoadFabric();
            LoadButton(0);
        }

        private void cBoxItemType_Load(object sender, EventArgs e)
        {

        }

        protected void LoadYarn()
        {
            DataTable dt = LoadDatatable();
            CmbYarn.DataSource = null;
            CmbYarn.DisplayMember = "YarnName";
            CmbYarn.ValueMember = "YarnId";
            CmbYarn.DataSource = dt;
        }

        protected void LoadYarnGridView()
        {
            try
            {
                DataGridYarn.AutoGenerateColumns = false;
                DataGridYarn.ColumnCount = 5;
                DataGridYarn.Columns[0].Name = "SlNo";
                DataGridYarn.Columns[0].HeaderText = "SlNo";
                DataGridYarn.Columns[0].Width = 50;

                DataGridYarn.Columns[1].Name = "YarnName";
                DataGridYarn.Columns[1].HeaderText = "YarnName";
                DataGridYarn.Columns[1].Width = 280;

                DataGridYarn.Columns[2].Name = "Composition";
                DataGridYarn.Columns[2].HeaderText = "Composition";
                DataGridYarn.Columns[2].Width = 80;

                DataGridYarn.Columns[3].Name = "YarnId";
                DataGridYarn.Columns[3].HeaderText = "YarnId";
                DataGridYarn.Columns[3].Visible = false;

                DataGridYarn.Columns[4].Name = "Uid";
                DataGridYarn.Columns[4].HeaderText = "Uid";
                DataGridYarn.Columns[4].Visible = false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void LoadFabric()
        {
            try
            {
                sfDataGridFabric.DataSource = null;
                DataTable dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "SP_GetFabric", conn);
                sfDataGridFabric.DataSource = dt;
                sfDataGridFabric.Columns[1].Width = 150;
                sfDataGridFabric.Columns[2].Width = 150;
                sfDataGridFabric.Columns[3].Width = 150;
                sfDataGridFabric.Columns[4].Width = 450;
                sfDataGridFabric.Columns[5].Visible = false;
                sfDataGridFabric.Columns[0].Visible = false;
                sfDataGridFabric.Columns[6].Visible = false;
                sfDataGridFabric.Columns[7].Visible = false;
                sfDataGridFabric.Columns[8].Visible = false;
                sfDataGridFabric.Columns[9].Visible = false;
                foreach (var column in this.sfDataGridFabric.Columns)
                {
                    if (!column.MappingName.Equals("Uid") && !column.MappingName.Equals("Active"))
                        excludeColumns.Add(column.MappingName);
                }

                (sfDataGridFabric.Columns["FabricName"] as GridTextColumn).AllowTextWrapping = true;
                (sfDataGridFabric.Columns["FabricSName"] as GridTextColumn).AllowTextWrapping = true;
                sfDataGridFabric.QueryRowHeight += SfDataGrid_QueryRowHeight;
                gridRowResizingOptions.ExcludeColumns = excludeColumns;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void cBoxStructure_Leave(object sender, EventArgs e)
        {
            BuildName();
        }
        protected DataTable LoadDatatable()
        {
            DataTable dt = new DataTable();
            try
            {
                int Active;
                if (chckAc.Checked == true)
                {
                    Active = 1;
                }
                else
                {
                    Active = 0;
                }
                SqlParameter[] para = { new SqlParameter("@Active", Active) };
                dt = db.GetDataWithParam(CommandType.StoredProcedure, "SP_GetYarnMaster", para, conn);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            return dt;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (TxtName.Text != string.Empty || CmbStrucType.SelectedIndex != -1 || cBoxStructure.textBox1.Text != string.Empty || CmbFabricType.SelectedIndex != -1)
                {
                    bool Active = false;
                    if (chckAc.Checked == true)
                    {
                        Active = true;
                    }
                    else
                    {
                        Active = false;
                    }
                    if (btnSave.Text == "Save")
                    {
                        SqlParameter[] sqlParametersd = { new SqlParameter("@FabricName", TxtName.Text) };
                        DataTable dt = db.GetDataWithParam(CommandType.StoredProcedure, "Proc_CheckDuplicateFabric", sqlParametersd, conn);
                        if (dt.Rows.Count != 0)
                        {
                            MessageBox.Show("Entry already found", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            return;
                        }
                    }
                    SqlParameter[] parameters = {
                        new SqlParameter("@UID",TxtName.Tag),
                        new SqlParameter("@StructureTypeUID",CmbStrucType.SelectedValue),
                        new SqlParameter("@StructureUid",cBoxStructure.textBox1.Tag),
                        new SqlParameter("@FabricTypeUid",CmbFabricType.SelectedValue),
                        new SqlParameter("@FabricName",TxtName.Text),
                        new SqlParameter("@FabricSName",txtShortName.Text),
                        new SqlParameter("@Active",Active),
                        new SqlParameter("@CreatedDate",DateTime.Now.Date),
                        new SqlParameter("@CreatedBy",GeneralParameters.UserdId),
                        new SqlParameter("@ReturnId",SqlDbType.Decimal)
                    };
                    parameters[9].Direction = ParameterDirection.Output;
                    decimal uid = db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_FabricM", parameters, conn, 9);
                    //if(CmbFabricType.Text == "YARN DYED")
                    //{
                    //    for (int i = 0; i < DataGridYarn.Rows.Count; i++)
                    //    {
                    //        string Query = "Select UID from FabricMYarn Where FabricMUID=" + uid + "";
                    //        DataTable dt = db.GetDataWithoutParam(CommandType.Text, Query, conn);
                    //        int id = 0;
                    //        if(dt.Rows.Count > 0)
                    //        {
                    //            id = Convert.ToInt32(dt.Rows[0]["UID"].ToString()); 
                    //        }
                    //        else
                    //        {
                    //            id = 0;
                    //        }
                    //        SqlParameter[] sqlParameters = {
                    //            new SqlParameter("@UID",id),
                    //            new SqlParameter("@FabricMUID",uid),
                    //            new SqlParameter("@YarnUid",CmbYarn.SelectedValue),
                    //            new SqlParameter("@YarnComposition","0")
                    //        };
                    //        db.ExecuteNonQuery(CommandType.StoredProcedure, "Proc_FabricMYarn", sqlParameters, conn);
                    //    }
                    //}
                    //else
                    //{
                    for (int i = 0; i < DataGridYarn.Rows.Count; i++)
                    {
                        SqlParameter[] sqlParameters = {
                                new SqlParameter("@UID",DataGridYarn.Rows[i].Cells[4].Value.ToString()),
                                new SqlParameter("@FabricMUID",uid),
                                new SqlParameter("@YarnUid",DataGridYarn.Rows[i].Cells[3].Value.ToString()),
                                new SqlParameter("@YarnComposition",DataGridYarn.Rows[i].Cells[2].Value.ToString())
                            };
                        db.ExecuteNonQuery(CommandType.StoredProcedure, "Proc_FabricMYarn", sqlParameters, conn);
                    }
                    //}

                    MessageBox.Show("Record Saved Successfully", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    ClearTextBoxes();
                    DataGridYarn.Rows.Clear();
                    LoadFabric();
                    LoadButton(0);
                }
                else
                {
                    MessageBox.Show("Fill the mandatory fields", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        public void ClearTextBoxes(bool searchRecursively = true)
        {
            void clearTextBoxes(Control.ControlCollection controls, bool searchChildren)
            {
                foreach (Control c in controls)
                {
                    TextBox txt = c as TextBox;
                    txt?.Clear();
                    if (searchChildren && c.HasChildren)
                        clearTextBoxes(c.Controls, true);
                }
            }
            clearTextBoxes(this.Controls, searchRecursively);
        }

        private void BtnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                var selectedItem = sfDataGridFabric.SelectedItems[0];
                var dataRow = (selectedItem as DataRowView).Row;
                decimal CUid = Convert.ToDecimal(dataRow["UID"].ToString());
                string StructureType = dataRow["StructureType"].ToString();
                string Structure = dataRow["Structure"].ToString();
                string FabricType = dataRow["FabricType"].ToString();
                string FabricName = dataRow["FabricName"].ToString();
                string FabricSName = dataRow["FabricSName"].ToString();
                bool Active = Convert.ToBoolean(dataRow["Active"].ToString());
                decimal StructureTypeUID = Convert.ToDecimal(dataRow["StructureTypeUID"].ToString());
                decimal StructureUid = Convert.ToDecimal(dataRow["StructureUid"].ToString());
                decimal FabricTypeUid = Convert.ToDecimal(dataRow["FabricTypeUid"].ToString());
                CmbFabricType.SelectedValue = StructureTypeUID;
                cBoxStructure.textBox1.Text = Structure;
                cBoxStructure.textBox1.Tag = StructureUid;
                CmbFabricType.SelectedValue = FabricTypeUid;
                txtShortName.Text = FabricSName;
                TxtName.Text = FabricName;
                TxtName.Tag = CUid;
                if (Active == true)
                {
                    chckAc.Checked = true;
                }
                else
                {
                    chckAc.Checked = false;
                }
                SqlParameter[] sqlParameters = { new SqlParameter("@FabricMuid", CUid) };
                DataTable dt = db.GetDataWithParam(CommandType.StoredProcedure, "GetYarnFabricComposition", sqlParameters, conn);
                DataGridYarn.Rows.Clear();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (CmbFabricType.Text == "YARN DYED")
                    {
                        int Index = DataGridYarn.Rows.Add();
                        DataGridViewRow row = DataGridYarn.Rows[Index];
                        row.Cells[0].Value = DataGridYarn.Rows.Count;
                        row.Cells[1].Value = dt.Rows[i]["YarnName"].ToString();
                        row.Cells[2].Value = dt.Rows[i]["YarnComposition"].ToString();
                        row.Cells[3].Value = dt.Rows[i]["YarnUid"].ToString();
                        row.Cells[4].Value = dt.Rows[i]["UID"].ToString();
                    }
                    else
                    {
                        int Index = DataGridYarn.Rows.Add();
                        DataGridViewRow row = DataGridYarn.Rows[Index];
                        row.Cells[0].Value = DataGridYarn.Rows.Count;
                        row.Cells[1].Value = dt.Rows[i]["YarnName"].ToString();
                        row.Cells[2].Value = dt.Rows[i]["YarnComposition"].ToString();
                        row.Cells[3].Value = dt.Rows[i]["YarnUid"].ToString();
                        row.Cells[4].Value = dt.Rows[i]["UID"].ToString();
                    }

                }
                BuildName();
                LoadButton(2);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
        int height;
        RowAutoFitOptions gridRowResizingOptions = new RowAutoFitOptions();
        void SfDataGrid_QueryRowHeight(object sender, QueryRowHeightEventArgs e)
        {
            if (this.sfDataGridFabric.TableControl.IsTableSummaryIndex(e.RowIndex))
            {
                e.Height = 40;
                e.Handled = true;
            }
            else if (this.sfDataGridFabric.AutoSizeController.GetAutoRowHeight(e.RowIndex, gridRowResizingOptions, out height))
            {
                if (height > this.sfDataGridFabric.RowHeight)
                {
                    e.Height = height;
                    e.Handled = true;
                }
            }
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            try
            {
                if (CmbFabricType.Text == "YARN DYED")
                {
                    if (CmbYarn.Text != string.Empty)
                    {
                        int Index = DataGridYarn.Rows.Add();
                        DataGridViewRow row = (DataGridViewRow)DataGridYarn.Rows[Index];
                        row.Cells[0].Value = DataGridYarn.Rows.Count;
                        row.Cells[1].Value = CmbYarn.Text;
                        row.Cells[2].Value = "0";
                        row.Cells[3].Value = CmbYarn.SelectedValue;
                        row.Cells[4].Value = "0";
                        BuildName();

                    }
                }
                else
                {
                    if (CmbYarn.Text != string.Empty && txtPer.Text != string.Empty)
                    {
                        decimal sum = 0;
                        for (int i = 0; i < DataGridYarn.Rows.Count; i++)
                        {
                            sum += Convert.ToDecimal(DataGridYarn.Rows[i].Cells[2].Value.ToString());
                        }
                        sum += Convert.ToDecimal(txtPer.Text);
                        if (sum <= 100)
                        {
                            int Index = DataGridYarn.Rows.Add();
                            DataGridViewRow row = (DataGridViewRow)DataGridYarn.Rows[Index];
                            row.Cells[0].Value = DataGridYarn.Rows.Count;
                            row.Cells[1].Value = CmbYarn.Text;
                            row.Cells[2].Value = txtPer.Text;
                            row.Cells[3].Value = CmbYarn.SelectedValue;
                            row.Cells[4].Value = "0";
                            BuildName();
                        }
                        else
                        {
                            MessageBox.Show("Composition Exeed 100%", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            return;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }

        private void CmbYarn_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (CmbFabricType.Text == "YARN DYDED")
                {
                    BuildName();
                }
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
