﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Drawing.Imaging;
using System.Configuration;
using System.Data.SqlClient;

namespace MyEasyBizAPMS
{
    public partial class FrmQcEntry : Form
    {
        public FrmQcEntry()
        {
            this.BackColor = Color.White; ;
            InitializeComponent();
        }
        string uid = "";
        int mode = 0;
        int Dtype = 0;
        string da;
        int i;
        int type = 0;
        DataGridViewComboBoxCell cbxComboRow6;
        ComboBox cbxRow5;
        string tpuid = "";
        SqlConnection conn = new SqlConnection(GeneralParameters.ConnectionString);
        SqlCommand qur = new SqlCommand();
        DataTable docno = new DataTable();
        DataRow doc1;

        BindingSource bs = new BindingSource();
        DataTable Docno = new DataTable();
        DataTable Docno1 = new DataTable();
        BindingSource bsc = new BindingSource();
        BindingSource bsp = new BindingSource();
        BindingSource bsserial = new BindingSource();
        BindingSource IN = new BindingSource();
        BindingSource OUT = new BindingSource();
        BindingSource bsFabric = new BindingSource();

        SQLDBHelper db = new SQLDBHelper();
        int Fillid;
        BindingSource bsParty = new BindingSource();
        string str1key;
        public int SelectId = 0;
        private void FrmQcEntry_Load(object sender, EventArgs e)
        {
            qur.Connection = conn;
    
            Genclass.buttonstyleform(this);
            Genclass.buttonstylepanel(panadd);
            panadd.Visible = true;
            Genpan.Visible = true;
            Editpnl.Visible = false;
            this.HFGP.DefaultCellStyle.Font = new Font("calibri", 10);
            this.HFGP.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 10, FontStyle.Bold);
            this.HFIT.DefaultCellStyle.Font = new Font("calibri", 10);
            this.HFIT.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 10, FontStyle.Bold);
            this.RQGR.DefaultCellStyle.Font = new Font("calibri", 10);
            this.RQGR.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 10, FontStyle.Bold);
            this.HFGP1.DefaultCellStyle.Font = new Font("calibri", 10);
            this.HFGP1.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 10, FontStyle.Bold);
            HFIT.RowHeadersVisible = false;
            HFGP.RowHeadersVisible = false;
            RQGR.RowHeadersVisible = false;
            HFGP1.RowHeadersVisible = false;
            HFGP2.RowHeadersVisible = false;
            HFGP2.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            HFGP2.EnableHeadersVisualStyles = false;
            HFGP2.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.GradientActiveCaption;
            HFIT.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            HFIT.EnableHeadersVisualStyles = false;
            HFIT.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.GradientActiveCaption;
            HFGP1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            HFGP1.EnableHeadersVisualStyles = false;
            HFGP1.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.GradientActiveCaption;
            RQGR.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            RQGR.EnableHeadersVisualStyles = false;
            RQGR.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.GradientActiveCaption;
            HFGP.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            HFGP.EnableHeadersVisualStyles = false;
            HFGP.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.GradientActiveCaption;
            HFGP.Focus();
            if (Text == "Knit QC")
            {
                Genclass.Dtype = 1190;
            }
            else if (Text == "Fabric Process GRN QC")
            {
                Genclass.Dtype = 1200;

            }
            else 
            {
                Genclass.Dtype = 1360;

            }
            da = DateTime.Today.ToString("MMM/yyyy");
            dtpfnt.Text = da;
            chkact.Checked = true;
            Loadgrid();
            lkppnl.Visible = false;
        }
        private void Loadgrid()
        {
            try
            {
                conn.Close();
                conn.Open();
                Genclass.StrSrch = "";
                Genclass.FSSQLSortStr = "Docno";
                Genclass.FSSQLSortStr1 = "Docdate";
                Genclass.FSSQLSortStr2 = "Name";
                Genclass.FSSQLSortStr3 = "Itemname";
                Genclass.FSSQLSortStr4 = "Remarks";
                Genclass.FSSQLSortStr5 = "Qty";
                if (txtscr1.Text != "")
                {
                    if (Genclass.StrSrch == "")
                    {
                        Genclass.StrSrch = Genclass.FSSQLSortStr + " like '%" + txtscr1.Text + "%'";
                    }
                    else
                    {
                        Genclass.StrSrch = Genclass.StrSrch + " and " + Genclass.FSSQLSortStr + " like '%" + txtscr1.Text + "%'";
                    }
                }

                if (Txtscr2.Text != "")
                {
                    if (Genclass.StrSrch == "")
                    {
                        Genclass.StrSrch = Genclass.FSSQLSortStr1 + " like '%" + Txtscr2.Text + "%'";
                    }
                    else
                    {
                        Genclass.StrSrch = Genclass.StrSrch + " and " + Genclass.FSSQLSortStr1 + " like '%" + Txtscr2.Text + "%'";
                    }
                }

                if (txtscr5.Text != "")
                {
                    if (Genclass.StrSrch == "")
                    {
                        Genclass.StrSrch = Genclass.FSSQLSortStr2 + " like '%" + txtscr5.Text + "%'";
                    }
                    else
                    {
                        Genclass.StrSrch = Genclass.StrSrch + " and " + Genclass.FSSQLSortStr2 + " like '%" + txtscr5.Text + "%'";
                    }
                }

                if (txtscr6.Text != "")
                {
                    if (Genclass.StrSrch == "")
                    {
                        Genclass.StrSrch = Genclass.FSSQLSortStr4 + " like '%" + txtscr6.Text + "%'";
                    }
                    else
                    {
                        Genclass.StrSrch = Genclass.StrSrch + " and " + Genclass.FSSQLSortStr4 + " like '%" + txtscr6.Text + "%'";
                    }
                }


                if (textBox1.Text != "")
                {
                    if (Genclass.StrSrch == "")
                    {
                        Genclass.StrSrch = Genclass.FSSQLSortStr3 + " like '%" + textBox1.Text + "%'";
                    }
                    else
                    {
                        Genclass.StrSrch = Genclass.StrSrch + " and " + Genclass.FSSQLSortStr3 + " like '%" + textBox1.Text + "%'";
                    }
                }

                if (textBox2.Text != "")
                {
                    if (Genclass.StrSrch == "")
                    {
                        Genclass.StrSrch = Genclass.FSSQLSortStr5 + " like '%" + textBox2.Text + "%'";
                    }
                    else
                    {
                        Genclass.StrSrch = Genclass.StrSrch + " and " + Genclass.FSSQLSortStr5 + " like '%" + textBox2.Text + "%'";
                    }
                }

                if (txtscr1.Text != "")
                {
                    Genclass.StrSrch = " " + Genclass.StrSrch;
                }
                else if (Txtscr2.Text != "")
                {
                    Genclass.StrSrch = " " + Genclass.StrSrch;
                }
                else if (txtscr5.Text != "")
                {
                    Genclass.StrSrch = " " + Genclass.StrSrch;
                }
                else if (txtscr6.Text != "")
                {
                    Genclass.StrSrch = " " + Genclass.StrSrch;
                }
                else if (textBox1.Text != "")
                {
                    Genclass.StrSrch = " " + Genclass.StrSrch;
                }
                else if (textBox2.Text != "")
                {
                    Genclass.StrSrch = " " + Genclass.StrSrch;
                }
                else
                {
                    Genclass.StrSrch = "a.uid <> 0";
                }
                DateTime str9 = Convert.ToDateTime(dtpfnt.Text);
                if (Genclass.Dtype == 1190)
                {
                    if (chkact.Checked == true)
                    {
                        string quy = "select distinct a.UId,DocNo,DocDate,DcNo,DcDate,b.Name as Party,a.partyuid,a.active from qcm a inner join supplierm b on a.PartyUid=b.uid where a.active=1 and a.doctypeid=" + Genclass.Dtype + " and  " + Genclass.StrSrch + "  order by a.docno desc ";
                        Genclass.cmd = new SqlCommand(quy, conn);
                    }
                    else
                    {
                        string quy = "select distinct a.UId,DocNo,DocDate,DcNo,DcDate,b.Name as Party,a.partyuid,a.active from qcm a inner join supplierm b on a.PartyUid=b.uid where a.active=0 and a.doctypeid=" + Genclass.Dtype + " and  " + Genclass.StrSrch + " order by a.docno desc ";
                        Genclass.cmd = new SqlCommand(quy, conn);
                    }
                }
                else if (Genclass.Dtype == 1200)
                {
                    if (chkact.Checked == true)
                    {
                        string quy = "select distinct a.UId,DocNo,DocDate,DcNo,DcDate,b.Name as Party,a.partyuid,a.active from qcm a inner join supplierm b on a.PartyUid=b.uid where a.active=1 and a.doctypeid=" + Genclass.Dtype + " and  " + Genclass.StrSrch + " order by a.docno desc ";
                        Genclass.cmd = new SqlCommand(quy, conn);
                    }
                    else
                    {
                        string quy = "select distinct a.UId,DocNo,DocDate,DcNo,DcDate,b.Name as Party,a.partyuid,a.active from qcm a inner join supplierm b on a.PartyUid=b.uid where a.active=0 and a.doctypeid=" + Genclass.Dtype + " and  " + Genclass.StrSrch + " order by a.docno desc ";
                        Genclass.cmd = new SqlCommand(quy, conn);
                    }
                }
                else if (Genclass.Dtype == 1360)
                {
                    if (chkact.Checked == true)
                    {
                        string quy = "select distinct a.UId,DocNo,DocDate,DcNo,DcDate,b.Name as Party,a.partyuid,a.active from qcm a inner join supplierm b on a.PartyUid=b.uid where a.active=1 and a.doctypeid=" + Genclass.Dtype + " and  " + Genclass.StrSrch + " order by a.docno desc ";
                        Genclass.cmd = new SqlCommand(quy, conn);
                    }
                    else
                    {
                        string quy = "select distinct a.UId,DocNo,DocDate,DcNo,DcDate,b.Name as Party,a.partyuid,a.active from qcm a inner join supplierm b on a.PartyUid=b.uid where a.active=0 and a.doctypeid=" + Genclass.Dtype + " and  " + Genclass.StrSrch + " order by a.docno desc ";
                        Genclass.cmd = new SqlCommand(quy, conn);
                    }
                }


                SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
                DataTable tap = new DataTable();
                aptr.Fill(tap);
                HFGP.AutoGenerateColumns = false;
                HFGP.Refresh();
                HFGP.DataSource = null;
                HFGP.Rows.Clear();
                HFGP.ColumnCount = tap.Columns.Count;
                i = 0;
                foreach (DataColumn column in tap.Columns)
                {
                    HFGP.Columns[i].Name = column.ColumnName;
                    HFGP.Columns[i].HeaderText = column.ColumnName;
                    HFGP.Columns[i].DataPropertyName = column.ColumnName;
                    i = i + 1;
                }
                HFGP.Columns[0].Visible = false;
                HFGP.Columns[1].Width = 90;
                HFGP.Columns[2].Width = 90;
                HFGP.Columns[3].Width = 100;
                //HFGP.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                HFGP.Columns[4].Width = 100;
                HFGP.Columns[5].Width = 600;
                HFGP.Columns[6].Visible = false;
                HFGP.Columns[7].Visible = false;
     



                HFGP.DataSource = tap;
                HFGP.Columns[2].DefaultCellStyle.Format = "yyyy'/'MM'/'dd";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }
            finally
            {
                conn.Close();
                Genclass.cmd.Dispose();
            }
        }

        private void txtname_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bsParty.Filter = string.Format("name LIKE '%{0}%' ", txtname.Text);



                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtname_Click(object sender, EventArgs e)
        {
            txtname.Text = "";
            type = 1;
            DataTable dt = getParty();
            bsParty.DataSource = dt;
            FillGrid(dt, 1);
            Point loc = FindLocation(txtname);
            grSearch.Location = new Point(loc.X, loc.Y + 20);
            grSearch.Visible = true;
            grSearch.Text = "Name Search";
        }
        private Point FindLocation(Control ctrl)
        {
            if (ctrl.Parent is Form)
                return ctrl.Location;
            else
            {
                Point p = FindLocation(ctrl.Parent);
                p.X += ctrl.Location.X;
                p.Y += ctrl.Location.Y;
                return p;
            }
        }
        protected void FillGrid(DataTable dt, int FillId)
        {
            try
            {
                DataGridCommon.DataSource = null;
                DataGridCommon.AutoGenerateColumns = false;


                DataGridCommon.ColumnCount = 2;
                DataGridCommon.Columns[0].Name = "uid";
                DataGridCommon.Columns[0].HeaderText = "uid";
                DataGridCommon.Columns[0].DataPropertyName = "uid";
                DataGridCommon.Columns[1].Name = "Name";
                DataGridCommon.Columns[1].HeaderText = "Name";
                DataGridCommon.Columns[1].DataPropertyName = "Name";
                DataGridCommon.Columns[1].Width = 250;
                

                DataGridCommon.DataSource = bsParty;
                DataGridCommon.Columns[0].Visible = false;
             



            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        protected DataTable getParty()
        {
            DataTable dt = new DataTable();
            try
            {
                if (type == 1 && Genclass.Dtype == 1190)
                {


                    SqlParameter[] para = { new SqlParameter("@docno", textBox3.Text) };
                    dt = db.getDataWithParameterCmdType(CommandType.StoredProcedure, "sp_GETKNITTINGRECEIPTSUPP", para);
          
                    bsParty.DataSource = dt;
                }
                else if (type == 2 && Genclass.Dtype == 1190)
                {
                   
                    //SqlParameter[] para = { new SqlParameter("@SUPPID", txtpuid.Text) };
                    dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "sp_getloaddcno", conn);

                    bsc.DataSource = dt;

                }
                else if (type == 1 && Genclass.Dtype == 1200)
                {


                    SqlParameter[] para = { new SqlParameter("@docno", textBox3.Text) };
                    dt = db.getDataWithParameterCmdType(CommandType.StoredProcedure, "sp_GETKNITTINGRECEIPTSUPPfabricqc", para);

                    bsParty.DataSource = dt;
                }
                else if (type == 1 && Genclass.Dtype == 1360)
                {


                    SqlParameter[] para = { new SqlParameter("@docno", textBox3.Text) };
                    dt = db.getDataWithParameterCmdType(CommandType.StoredProcedure, "sp_GETYARNRECNO", para);

                    bsParty.DataSource = dt;
                }
                else if (type == 2 && Genclass.Dtype == 1200)
                {

                    //SqlParameter[] para = { new SqlParameter("@SUPPID", txtpuid.Text) };
                    dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "sp_getloaddcnofabricqc", conn);

                    bsc.DataSource = dt;

                }
                else if (type == 2 && Genclass.Dtype == 1360)
                {

                    //SqlParameter[] para = { new SqlParameter("@SUPPID", txtpuid.Text) };
                    dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "sp_getloaddcnoyarn", conn);

                    bsc.DataSource = dt;

                }


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            return dt;
        }

        private void txtbeam_TextChanged(object sender, EventArgs e)
        {
            
        }
        protected void FillGrid1(DataTable dt, int FillId)
        {
            try
            {
                DataGridCommon.DataSource = null;
                DataGridCommon.AutoGenerateColumns = false;


                DataGridCommon.ColumnCount = 3;
                DataGridCommon.Columns[0].Name = "uid";
                DataGridCommon.Columns[0].HeaderText = "uid";
                DataGridCommon.Columns[0].DataPropertyName = "uid";
                DataGridCommon.Columns[1].Name = "Dcno";
                DataGridCommon.Columns[1].HeaderText = "Dcno";
                DataGridCommon.Columns[1].DataPropertyName = "Dcno";
                DataGridCommon.Columns[1].Width = 300;

                DataGridCommon.Columns[2].Name = "Dno";
                DataGridCommon.Columns[2].HeaderText = "Dno";
                DataGridCommon.Columns[2].DataPropertyName = "Dno";
                DataGridCommon.DataSource = bsc;
                DataGridCommon.Columns[0].Visible = false;
                DataGridCommon.Columns[2].Visible = false;




            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        private void txtbeam_Click(object sender, EventArgs e)
        {
           

           

            txtscr11.Text = "";
            Point loc = FindLocation(txtbeam);
            grSearch.Location = new Point(loc.X, loc.Y + 20);
            grSearch.Visible = true;

            type = 2;

            txtscr11.Focus();
      
            SqlParameter[] para = { new SqlParameter("@suppid", Convert.ToString(txtpuid.Text)) };

            DataTable dt = db.getDataWithParameterCmdType(CommandType.StoredProcedure, "SP_GETALLQCENTRY", para);
            bsc.DataSource = dt;
         



            DataGridCommon.DataSource = null;
            DataGridCommon.AutoGenerateColumns = false;

            DataGridCommon.ColumnCount = 8;




            DataGridCommon.Columns[0].Name = "docno";
            DataGridCommon.Columns[0].HeaderText = "docno";
            DataGridCommon.Columns[0].DataPropertyName = "docno";
            DataGridCommon.Columns[0].Width = 100;
            DataGridCommon.Columns[1].Name = "docdate";
            DataGridCommon.Columns[1].HeaderText = "docdate";
            DataGridCommon.Columns[1].DataPropertyName = "docdate";
            DataGridCommon.Columns[1].Width = 100;
            DataGridCommon.Columns[2].Name = "itemname";
            DataGridCommon.Columns[2].HeaderText = "itemname";
            DataGridCommon.Columns[2].DataPropertyName = "itemname";
            DataGridCommon.Columns[2].Width = 300;
            DataGridCommon.Columns[3].Name = "Recqty";
            DataGridCommon.Columns[3].HeaderText = "Recqty";
            DataGridCommon.Columns[3].DataPropertyName = "Recqty";
            DataGridCommon.Columns[3].Width = 100;
            DataGridCommon.Columns[4].Name = "Rejqty";
            DataGridCommon.Columns[4].HeaderText = "Rejqty";
            DataGridCommon.Columns[4].DataPropertyName = "Rejqty";
            DataGridCommon.Columns[4].Width = 100;
            DataGridCommon.Columns[5].Name = "ReworkQty";
            DataGridCommon.Columns[5].HeaderText = "ReworkQty";
            DataGridCommon.Columns[5].DataPropertyName = "ReworkQty";
            DataGridCommon.Columns[5].Width = 100;
            DataGridCommon.Columns[6].Name = "uid";
            DataGridCommon.Columns[6].HeaderText = "uid";
            DataGridCommon.Columns[6].DataPropertyName = "uid";
            DataGridCommon.Columns[6].Visible = false;
            DataGridCommon.Columns[7].Name = "itemid";
            DataGridCommon.Columns[7].HeaderText = "itemid";
            DataGridCommon.Columns[7].DataPropertyName = "itemid";
            DataGridCommon.Columns[7].Visible = false;
            DataGridCommon.DataSource = bsc;




            conn.Close();
        }


        private void button18_Click(object sender, EventArgs e)
        {
            try
            {
                SelectId = 1;
                int Index = DataGridCommon.SelectedCells[0].RowIndex;
                if (type == 1)
                {
                    txtname.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtpuid.Text = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                  
                    txtbeam.Focus();
                    loadgrisitem();
                }
                else 
                {

                    textBox3.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();

                    textBox3.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();


                    txtbags.Text = DataGridCommon.Rows[Index].Cells[2].Value.ToString();
                }


                grSearch.Visible = false;
                SelectId = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
        private void loadgrisitem()
        {
            DataTable tab1 = new DataTable();
            RQGR.AutoGenerateColumns = false;
            RQGR.Refresh();
            RQGR.DataSource = null;
            RQGR.Rows.Clear();
            RQGR.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            RQGR.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            if (Genclass.Dtype == 1190)
            {
                string qur1 = "exec sp_getloaditem " + txtpuid.Text + ",'" + textBox3.Text + "'";
                SqlCommand cmd1 = new SqlCommand(qur1, conn);
                SqlDataAdapter apt1 = new SqlDataAdapter(cmd1);
           
                apt1.Fill(tab1);
            }
            else if (Genclass.Dtype == 1200)
            {
                string qur1 = "exec sp_getloaditemfabricqc " + txtpuid.Text + ",'" + textBox3.Text + "'";
                SqlCommand cmd1 = new SqlCommand(qur1, conn);
                SqlDataAdapter apt1 = new SqlDataAdapter(cmd1);
         
                apt1.Fill(tab1);


            }
            else if (Genclass.Dtype == 1360)
            {
                string qur1 = "exec sp_getloaditemyarnqc " + txtpuid.Text + ",'" + textBox3.Text + "'";
                SqlCommand cmd1 = new SqlCommand(qur1, conn);
                SqlDataAdapter apt1 = new SqlDataAdapter(cmd1);

                apt1.Fill(tab1);


            }


            if (tab1.Rows.Count > 0)

            {
                for (int k = 0; k < tab1.Rows.Count; k++)
                {
                    var index1 = RQGR.Rows.Add();
                    RQGR.Rows[index1].Cells[0].Value = tab1.Rows[k]["ItemName"].ToString();
                    RQGR.Rows[index1].Cells[1].Value = tab1.Rows[k]["itemid"].ToString();
                    RQGR.Rows[index1].Cells[2].Value = tab1.Rows[k]["Dcno"].ToString();
                    RQGR.Rows[index1].Cells[3].Value = tab1.Rows[k]["UOM"].ToString();
                    RQGR.Rows[index1].Cells[4].Value = tab1.Rows[k]["DcQty"].ToString();
                    RQGR.Rows[index1].Cells[5].Value = tab1.Rows[k]["AccQTy"].ToString();
                    RQGR.Rows[index1].Cells[6].Value = tab1.Rows[k]["RejQTy"].ToString();
                    RQGR.Rows[index1].Cells[7].Value = tab1.Rows[k]["ReWQTy"].ToString();
                    RQGR.Rows[index1].Cells[8].Value = tab1.Rows[k]["refuid"].ToString();
                    RQGR.Rows[index1].Cells[9].Value = tab1.Rows[k]["Addnotes"].ToString();
                }
            }

        
    }
        private void DataGridCommon_DoubleClick(object sender, EventArgs e)
        {
            SelectId = 1;
            int Index = DataGridCommon.SelectedCells[0].RowIndex;
            if (type == 1)
            {
                txtname.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                txtpuid.Text = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                loadgrisitem();
                txtbeam.Focus();
            }
            else
            {

                textBox3.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();

                textBox3.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                txtbags.Text = DataGridCommon.Rows[Index].Cells[2].Value.ToString();


            }


            grSearch.Visible = false;
            SelectId = 0;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            lkppnl.Visible = false;
            mode = 1;
            Genpan.Visible = false;
            panadd.Visible = false;
            Genclass.ClearTextBox(this, Editpnl);
            Genclass.STR = "";
            Genclass.Gendocno();
            txtgrn.Text = Genclass.ST;
            Editpnl.Visible = true;
            Genclass.sum1 = 0;
            Genclass.sum2 = 0;

            Chkedtact.Checked = true;
            RQGR.Refresh();
            RQGR.DataSource = null;
            RQGR.Rows.Clear();
            HFIT.Refresh();
            HFIT.DataSource = null;
            HFIT.Rows.Clear();
            HFGP1.Refresh();
            HFGP1.DataSource = null;
            HFGP1.Rows.Clear();
            Titlep3();
           
            dtpgrndt.Focus();

        }

       

        private void Titlep3()
        {
            RQGR.ColumnCount = 11;

            RQGR.Columns[0].Name = "itemname";
            RQGR.Columns[1].Name = "itemuid";
            RQGR.Columns[2].Name = "Dcno";
            RQGR.Columns[3].Name = "UOM";
            RQGR.Columns[4].Name = "DC Qty";
            RQGR.Columns[5].Name = "Acc Qty";
            RQGR.Columns[6].Name = "Rej Qty";
            RQGR.Columns[7].Name = "Rework Qty";
            RQGR.Columns[8].Name = "Narration";
            RQGR.Columns[9].Name = "refuid";
            RQGR.Columns[10].Name = "uid";

            RQGR.Columns[0].Width = 300;
            RQGR.Columns[1].Visible = false;
            RQGR.Columns[2].Width = 80;
            RQGR.Columns[3].Width = 60;
            RQGR.Columns[4].Width = 80;
            RQGR.Columns[5].Width = 80;
            RQGR.Columns[6].Width = 80;
            RQGR.Columns[7].Width = 80;
            RQGR.Columns[8].Width = 120;
            RQGR.Columns[9].Visible = false;
            RQGR.Columns[10].Visible = false;


        }
        private void btnHide_Click(object sender, EventArgs e)
        {
            grSearch.Visible = false;
        }

        private void txtqqty_TextChanged(object sender, EventArgs e)
        {
            if (txtqqty.Text != "")
            {

                if (txtwaste.Text == "")
                {
                    txtwaste.Text = "0";

                }
                

               double val= Convert.ToDouble(Genclass.val1) - Convert.ToDouble(txtqqty.Text);
                txtwaste.Text = val.ToString();
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (txtbeam.Text == "")
            {
                MessageBox.Show("Enter the Item");
                txtbeam.Focus();
            }

            if (txtqqty.Text == "")
            {
                MessageBox.Show("Enter the Qty");
                txtqqty.Focus();
            }
      

            RQGR.AutoGenerateColumns = false;
            var index = RQGR.Rows.Add();
            RQGR.Rows[index].Cells[0].Value = txtbeam.Text;
            RQGR.Rows[index].Cells[1].Value = txtqqty.Tag;
            RQGR.Rows[index].Cells[2].Value = txtqqty.Text;
            RQGR.Rows[index].Cells[3].Value = txtrejqty.Text;
            RQGR.Rows[index].Cells[4].Value = txtrewqty.Text;
            RQGR.Rows[index].Cells[5].Value = txtnarration.Text;
            RQGR.Rows[index].Cells[6].Value = txtbeam.Tag;
            RQGR.Rows[index].Cells[7].Value = 0;
        

            lkppnl.Visible = false;
            txtbeam.Text = "";
            txtqqty.Text = "";
            txtrejqty.Text = "";
            txtrewqty.Text = "";
            txtrewqty.Text = "";
            txtnarration.Focus();


        }
        private void loadg()
        {


            RQGR.Refresh();
            RQGR.DataSource = null;
            RQGR.Rows.Clear();

            Genclass.strsql = "select  b.itemname,itemuid,a.recqty,isnull(accqty,0) as accqty,isnull(rejqty,0) as rejqty,isnull(rewqty,0) as rewqty,Rejreason as Rejreason,rewreason as rewreason,a.uid  from jolqclist  a  inner join joqc d on a.joluid = d.uid  and d.doctypeid = 250 inner join itemm b on a.itemuid = b.uid  where a.joluid=" + txtgrnid.Text + " ";
            Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
            SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
            DataTable tap1 = new DataTable();
            aptr1.Fill(tap1);

            for (int k = 0; k < tap1.Rows.Count; k++)
            {
                var index = RQGR.Rows.Add();

                RQGR.Rows[index].Cells[0].Value = tap1.Rows[k]["itemuid"].ToString();

                RQGR.Rows[index].Cells[1].Value = tap1.Rows[k]["itemname"].ToString();


                RQGR.Rows[index].Cells[2].Value = tap1.Rows[k]["recqty"].ToString();

                RQGR.Rows[index].Cells[3].Value = tap1.Rows[k]["accqty"].ToString();

                RQGR.Rows[index].Cells[4].Value = tap1.Rows[k]["rejqty"].ToString();
                RQGR.Rows[index].Cells[5].Value = tap1.Rows[k]["rewqty"].ToString();
                RQGR.Rows[index].Cells[6].Value = tap1.Rows[k]["Rejreason"].ToString();
                RQGR.Rows[index].Cells[7].Value = tap1.Rows[k]["rewreason"].ToString();
                RQGR.Rows[index].Cells[8].Value = tap1.Rows[k]["uid"].ToString();

            }
            Titlep3();

        }

        private void loadno()
        {

            for (int k = 0; k < RQGR.RowCount - 1; k++)
            {
                HFIT.AutoGenerateColumns = false;
                var index = HFIT.Rows.Add();

                HFIT.Rows[index].Cells[0].Value = RQGR.Rows[index].Cells[1].Value;

                HFIT.Rows[index].Cells[1].Value = k +1 ;


                HFIT.Rows[index].Cells[2].Value = RQGR.Rows[index].Cells[2].Value;

                HFIT.Rows[index].Cells[3].Value = RQGR.Rows[index].Cells[4].Value;

                HFIT.Rows[index].Cells[4].Value = RQGR.Rows[index].Cells[0].Value;


            }
        }
        private void button10_Click(object sender, EventArgs e)
        {
            int Index = HFGP2.SelectedCells[0].RowIndex;

            txtbeam.Tag = DataGridCommon.Rows[Index].Cells[6].Value.ToString();

            txtbeam.Text = DataGridCommon.Rows[Index].Cells[2].Value.ToString();
            txtqqty.Text = DataGridCommon.Rows[Index].Cells[3].Value.ToString();
            txtrejqty.Text = DataGridCommon.Rows[Index].Cells[4].Value.ToString();
            txtrewqty.Text = DataGridCommon.Rows[Index].Cells[5].Value.ToString();
            txtqqty.Focus();
            lkppnl.Visible = false;
        }

        private void button11_Click(object sender, EventArgs e)
        {
            lkppnl.Visible = false;
        }

        private void txtscr11_TextChanged(object sender, EventArgs e)
        {
            bsc.Filter = string.Format("itemname LIKE '%{0}%' ", txtscr11.Text);
        }

        private void btnsave_Click(object sender, EventArgs e)
        {
            conn.Close();
            conn.Open();
     
            
            int SP;
            if (Chkedtact.Checked == true)
            {

                SP = 1;
            }
            else
            {
                SP = 0;

            }

            if (mode == 1)
            {
                SqlParameter[] para ={
                    new SqlParameter("@DOCTYPEID", Genclass.Dtype),
                    new SqlParameter("@DOCNO",txtgrn.Text),
                    new SqlParameter("@DOCDATE",Convert.ToDateTime(dtpgrndt.Text)),
                    new SqlParameter("@DCNO",txtbags.Text),
                    new SqlParameter("@DCDATE",Convert.ToDateTime(dtpdcdate.Text)),
                    new SqlParameter("@PARTYUID",txtpuid.Text),
                    new SqlParameter("@companyid","1"),
                    new SqlParameter("@yearid",Genclass.Yearid),
                    new SqlParameter("@ACTIVE",  "1")
                };
                db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_GETQCM", para, conn);

                string quy6 = "select * from qcm where docno='" + txtgrn.Text + "' and doctypeid="+ Genclass.Dtype +"";
                Genclass.cmd = new SqlCommand(quy6, conn);
                SqlDataAdapter aptr3 = new SqlDataAdapter(Genclass.cmd);
                DataTable tap5 = new DataTable();
                aptr3.Fill(tap5);
                if (tap5.Rows.Count > 0)
                {
                    txtgrnid.Text = tap5.Rows[0]["uid"].ToString();
                }
                for (int i = 0; i < RQGR.RowCount - 1; i++)
                {
                    conn.Close();
                    conn.Open();
              
                    SqlParameter[] para1 ={
                        new SqlParameter("@DOCTYPEID",Genclass.Dtype),
                         new SqlParameter("@HEADID", txtgrnid.Text),
                        new SqlParameter("@itemid",RQGR.Rows[i].Cells[1].Value),
                           new SqlParameter("@dcno",RQGR.Rows[i].Cells[2].Value),
                              new SqlParameter("@uom",RQGR.Rows[i].Cells[3].Value),
                        new SqlParameter("@DCQTY", RQGR.Rows[i].Cells[4].Value),
                        new SqlParameter("@ACCQTY", RQGR.Rows[i].Cells[5].Value),
                        new SqlParameter("@REJQTY", RQGR.Rows[i].Cells[6].Value),
                        new SqlParameter("@REQQTY", RQGR.Rows[i].Cells[7].Value),
                        new SqlParameter("@refuid",RQGR.Rows[i].Cells[8].Value),
                        new SqlParameter("@addnotes",RQGR.Rows[i].Cells[9].Value),
                            new SqlParameter("@itemname",RQGR.Rows[i].Cells[0].Value),
                    };
                    db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_QCD", para1, conn);

                }
            }
            else
            {
                SqlParameter[] para ={
                    new SqlParameter("@uid", txtgrnid.Text),
                    new SqlParameter("@DOCTYPEID", Genclass.Dtype),
                    new SqlParameter("@DOCNO",txtgrn.Text),
                    new SqlParameter("@DOCDATE",Convert.ToDateTime(dtpgrndt.Text)),
                    new SqlParameter("@DCNO",txtbags.Text),
                    new SqlParameter("@DCDATE",Convert.ToDateTime(dtpdcdate.Text)),
                    new SqlParameter("@PARTYUID",txtpuid.Text),
                    new SqlParameter("@companyid","1"),
                    new SqlParameter("@yearid",Genclass.Yearid),
                    new SqlParameter("@ACTIVE", "1"),
              
                };
                db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_GETQCMUPDATE", para, conn);


                conn.Close();
                conn.Open();
                qur.CommandText = "delete from qcd where headid=" + txtgrnid.Text + "";
                qur.ExecuteNonQuery();

                for (int i = 0; i < RQGR.RowCount - 1; i++)
                {
                    conn.Close();
                    conn.Open();

                    SqlParameter[] para1 ={
                        new SqlParameter("@DOCTYPEID",Genclass.Dtype),
                         new SqlParameter("@HEADID", txtgrnid.Text),
                        new SqlParameter("@itemid",RQGR.Rows[i].Cells[1].Value),
                           new SqlParameter("@dcno",RQGR.Rows[i].Cells[2].Value),
                              new SqlParameter("@uom",RQGR.Rows[i].Cells[3].Value),
                        new SqlParameter("@DCQTY", RQGR.Rows[i].Cells[4].Value),
                        new SqlParameter("@ACCQTY", RQGR.Rows[i].Cells[5].Value),
                        new SqlParameter("@REJQTY", RQGR.Rows[i].Cells[6].Value),
                        new SqlParameter("@REQQTY", RQGR.Rows[i].Cells[7].Value),
                        new SqlParameter("@refuid",RQGR.Rows[i].Cells[8].Value),
                        new SqlParameter("@addnotes",RQGR.Rows[i].Cells[9].Value),
                            new SqlParameter("@itemname",RQGR.Rows[i].Cells[0].Value),
                    };
                    db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_QCD", para1, conn);

                }

            }

            conn.Close();
            conn.Open();
            if (txtgrnid.Text != "")
            {

                if (Genclass.Dtype == 1190)
                {
                    string quy = "select d.uid,b.accqty from qcm a inner join qcd b on a.uid=b.headid inner join JOKNITtingREC  c on b.refuid=c.uid inner join jo  d on c.headid=d.uid and d.doctypeid=550 where a.uid=" + txtgrnid.Text + "";
                    Genclass.cmd = new SqlCommand(quy, conn);
                    SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
                    DataTable tap = new DataTable();
                    aptr.Fill(tap);
                    for (int l = 0; l < tap.Rows.Count; l++)
                    {
                        //qur.CommandText = "exec sp_getlossstockknitting " + tap.Rows[l]["uid"].ToString() + "," + tap.Rows[l]["accqty"].ToString() + " ";
                        //qur.ExecuteNonQuery();
                        qur.CommandText = "exec SP_stocklegKnitReceipt " + tap.Rows[l]["uid"].ToString() + ",550 ,1 ,"+ tap.Rows[l]["accqty"].ToString() + "";
                        qur.ExecuteNonQuery();
                    }
                }
                else if (Genclass.Dtype == 1200)
                {
                    string quy = "select D.uid,b.accqty from qcm a inner join qcd b on a.uid=b.headid inner join joreceipt  c on b.refuid=c.uid inner join jo  d on c.headid=d.uid and d.doctypeid=570 where a.uid=" + txtgrnid.Text + "";
                    Genclass.cmd = new SqlCommand(quy, conn);
                    SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
                    DataTable tap = new DataTable();
                    aptr.Fill(tap);
                    for (int l = 0; l < tap.Rows.Count; l++)
                    {
                        qur.CommandText = "exec SP_stocklegJoiReceipt " + tap.Rows[l]["uid"].ToString() + ",570,1 ," + tap.Rows[l]["accqty"].ToString() + "";
                        qur.ExecuteNonQuery();
                    }
                }

                else if (Genclass.Dtype == 1350)
                {
                    string quy = "select D.uid,b.accqty from qcm a inner join qcd b on a.uid=b.headid inner join joreceipt  c on b.refuid=c.uid inner join jo  d on c.headid=d.uid and d.doctypeid=220 where a.uid=" + txtgrnid.Text + "";
                    Genclass.cmd = new SqlCommand(quy, conn);
                    SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
                    DataTable tap = new DataTable();
                    aptr.Fill(tap);
                    for (int l = 0; l < tap.Rows.Count; l++)
                    {
                        qur.CommandText = "exec SP_stocklegJoiReceipt " + tap.Rows[l]["uid"].ToString() + ",220,1 ," + tap.Rows[l]["accqty"].ToString() + "";
                        qur.ExecuteNonQuery();
                    }
                }



                if (mode == 1)
                {
                    qur.CommandText = "exec sp_getscstock " + txtgrnid.Text + "," + Genclass.Dtype + " ,1";
                    qur.ExecuteNonQuery();

                    qur.CommandText = "exec POST_SUPPLIERSTOCK_SP " + txtgrnid.Text + "," + Genclass.Dtype + " ";
                    qur.ExecuteNonQuery();
                }
                else
                {
                    qur.CommandText = "delete from SUPPLIER_STOCK_LEDGER where hdid= " + txtgrnid.Text + " and doctypeid=" + Genclass.Dtype + " ";
                    qur.ExecuteNonQuery();
                    qur.CommandText = "exec sp_getscstock " + txtgrnid.Text + "," + Genclass.Dtype + ",2";
                    qur.ExecuteNonQuery();


                    qur.CommandText = "exec POST_SUPPLIERSTOCK_SP " + txtgrnid.Text + "," + Genclass.Dtype + " ";
                    qur.ExecuteNonQuery();
                }

            }
            if (mode==1)
            {
                qur.CommandText = "update doctypem set lastno=lastno+1 where doctypeid=" + Genclass.Dtype + " and finyear='19-20'";
                qur.ExecuteNonQuery();

            }

            MessageBox.Show("Record Saved Successfully");
            chkact.Checked = true;
            panadd.Visible = true;
            Genpan.Visible = true;
            Editpnl.Visible = false;
            Loadgrid();




        }

        private void Titlep2()
        {
            HFIT.ColumnCount = 5;


            HFIT.Columns[0].Name = "Itemname";
            HFIT.Columns[1].Name = "noofrolls";

            HFIT.Columns[2].Name = "Qty";
            HFIT.Columns[3].Name = "Waste";
            HFIT.Columns[4].Name = "itemuid";
     




            HFIT.Columns[0].Width = 500;
            HFIT.Columns[1].Width = 100;


            HFIT.Columns[2].Width = 100;
            HFIT.Columns[3].Width = 100;

            HFIT.Columns[4].Visible = false;



        }
        private void load()
        {
            //Genclass.strsql = "select  b.itemname,count(itemuid) as noofrolls,a.qty,a.waqty,a.itemuid from  JolistReceipt  a  inner join jo d on a.joluid=d.uid  and d.doctypeid=1102 inner join itemm b on a.itemuid = b.uid  where a.joluid=" + uid + "  group by  b.itemname,a.qty,a.waqty,a.itemuid";

            //Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
            //SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
            //DataTable tap1 = new DataTable();
            //aptr1.Fill(tap1);

            //Genclass.sum1 = 0;


            //for (int k = 0; k < tap1.Rows.Count; k++)
            //{
            //    var index = HFIT.Rows.Add();

            //    HFIT.Rows[index].Cells[0].Value = tap1.Rows[k]["itemname"].ToString();

            //    HFIT.Rows[index].Cells[1].Value = tap1.Rows[k]["noofrolls"].ToString();


            //    HFIT.Rows[index].Cells[2].Value = tap1.Rows[k]["Qty"].ToString();

            //    HFIT.Rows[index].Cells[3].Value = tap1.Rows[k]["waqty"].ToString();

            //    HFIT.Rows[index].Cells[4].Value = tap1.Rows[k]["itemuid"].ToString();


            //}
            //Titlep2();
        }

        private void loadK()
        {
            HFIT.Refresh();
            HFIT.DataSource = null;
            HFIT.Rows.Clear();

            Genclass.strsql = "select  b.itemname,count(itemuid) as noofrolls,isnull(sum(a.qty),0) as Qty,isnull(sum(a.waqty),0) as waqty,a.itemuid from  JolistReceipt  a  inner join jo d on a.joluid=d.uid  and d.doctypeid=1102 inner join itemm b on a.itemuid = b.uid  where a.joluid=" + txtgrnid.Text + "  group by  b.itemname,a.itemuid";

            Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
            SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
            DataTable tap1 = new DataTable();
            aptr1.Fill(tap1);

            Genclass.sum1 = 0;


            for (int k = 0; k < tap1.Rows.Count; k++)
            {
                var index = HFIT.Rows.Add();

                HFIT.Rows[index].Cells[0].Value = tap1.Rows[k]["itemname"].ToString();

                HFIT.Rows[index].Cells[1].Value = tap1.Rows[k]["noofrolls"].ToString();


                HFIT.Rows[index].Cells[2].Value = tap1.Rows[k]["Qty"].ToString();
                HFIT.Rows[index].Cells[3].Value = tap1.Rows[k]["waqty"].ToString();

                HFIT.Rows[index].Cells[4].Value = tap1.Rows[k]["itemuid"].ToString();


            }
            Titlep2();
        }


        private void butedit_Click(object sender, EventArgs e)
        {
            mode = 2;
            panadd.Visible = false;
            Genpan.Visible = false;
            Editpnl.Visible = true;
            conn.Open();
            conn.Close();

         
            int i = HFGP.SelectedCells[0].RowIndex;
            uid = HFGP.Rows[i].Cells[0].Value.ToString();
            txtgrnid.Text = HFGP.Rows[i].Cells[0].Value.ToString();
            txtgrn.Text = HFGP.Rows[i].Cells[1].Value.ToString();
            dtpgrndt.Text = HFGP.Rows[i].Cells[2].Value.ToString();
            txtqqty.Text = HFGP.Rows[i].Cells[3].Value.ToString();
            dtpdcdate.Text = HFGP.Rows[i].Cells[4].Value.ToString();
            txtname.Text = HFGP.Rows[i].Cells[5].Value.ToString();
            txtpuid.Text = HFGP.Rows[i].Cells[6].Value.ToString();


            if(HFGP.Rows[i].Cells[6].Value.ToString()=="1")
            {
                Chkedtact.Checked = true;

            }
            else
            {
                Chkedtact.Checked = false;

            }
      
         

        
            HFIT.Refresh();
            HFIT.DataSource = null;
            HFIT.Rows.Clear();
            RQGR.Refresh();
            RQGR.DataSource = null;
            RQGR.Rows.Clear();
            HFGP1.Refresh();
            HFGP1.DataSource = null;
            HFGP1.Rows.Clear();


            Titlep3();
           
            load1();
            
     

        }
    

        private void load1()
        {
            Genclass.strsql = "sp_geteditknittingqc " + txtgrnid.Text + "";
            Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
            SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
            DataTable tap1 = new DataTable();
            aptr1.Fill(tap1);




            for (int k = 0; k < tap1.Rows.Count; k++)
            {
                var index = RQGR.Rows.Add();
    
                RQGR.Rows[index].Cells[0].Value = tap1.Rows[k]["itemname"].ToString();

                RQGR.Rows[index].Cells[1].Value = tap1.Rows[k]["itemid"].ToString();
                RQGR.Rows[index].Cells[2].Value = tap1.Rows[k]["dcno"].ToString();
                RQGR.Rows[index].Cells[3].Value = tap1.Rows[k]["uom"].ToString();
                RQGR.Rows[index].Cells[4].Value = tap1.Rows[k]["dcqty"].ToString();
                RQGR.Rows[index].Cells[5].Value = tap1.Rows[k]["accqty"].ToString();

                RQGR.Rows[index].Cells[6].Value = tap1.Rows[k]["rejqty"].ToString();

                RQGR.Rows[index].Cells[7].Value = tap1.Rows[k]["reqqty"].ToString();
                RQGR.Rows[index].Cells[8].Value = tap1.Rows[k]["refuid"].ToString();
                RQGR.Rows[index].Cells[9].Value = tap1.Rows[k]["addnotes"].ToString();

                RQGR.Rows[index].Cells[10].Value = tap1.Rows[k]["uid"].ToString();
         

            }
           
        }
        private void button9_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void btnaddrcan_Click(object sender, EventArgs e)
        {
            Editpnl.Visible = false;
            Genpan.Visible = true;
            panadd.Visible = true;
            Loadgrid();

        }

        private void buttnext1_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void DataGridCommon_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void txtname_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.F10)
                {
                    grSearch.Visible = false;
                }
                else if (e.KeyCode == Keys.F2)
                {
                    SelectId = 1;
                    int Index = DataGridCommon.SelectedCells[0].RowIndex;
                    txtname.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtpuid.Text = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                 


                    grSearch.Visible = false;
                    SelectId = 0;
                }
                else if (e.KeyCode == Keys.Escape)
                {
                    grSearch.Visible = false;
                }
                else if (e.KeyValue == 40)
                {
                    DataGridCommon.Select();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

        }

        private void txtscr11_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.F10)
                {
                    lkppnl.Visible = false;
                }
                else if (e.KeyCode == Keys.F2)
                {
                    SelectId = 1;


                    int Index = HFGP2.SelectedCells[0].RowIndex;
                    txtbeam.Tag = DataGridCommon.Rows[Index].Cells[6].Value.ToString();

                    txtbeam.Text = DataGridCommon.Rows[Index].Cells[2].Value.ToString();
                    txtqqty.Text = DataGridCommon.Rows[Index].Cells[3].Value.ToString();
                    txtrejqty.Text = DataGridCommon.Rows[Index].Cells[4].Value.ToString();
                    txtrewqty.Text = DataGridCommon.Rows[Index].Cells[5].Value.ToString();


                    //Genclass.val1 = Convert.ToDouble(HFGP2.Rows[Index].Cells[2].Value.ToString());
                    //double val2 = Convert.ToDouble(Genclass.val1) - Convert.ToDouble(txtqqty.Text);
                    //txtwaste.Text = val2.ToString();


                    lkppnl.Visible = false;
                    txtqqty.Focus();
                    SelectId = 0;
                }
                else if (e.KeyCode == Keys.Escape)
                {
                    lkppnl.Visible = false;
                }
                else if (e.KeyValue == 40)
                {
                    HFGP2.Select();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

        }

        private void HFGP2_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void HFGP2_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                int Index = HFGP2.SelectedCells[0].RowIndex;

                txtbeam.Text = HFGP2.Rows[Index].Cells[1].Value.ToString();
                txtbeam.Tag = HFGP2.Rows[Index].Cells[0].Value.ToString();
                txtqqty.Text = HFGP2.Rows[Index].Cells[2].Value.ToString();
                txtmillid.Text = HFGP2.Rows[Index].Cells[3].Value.ToString();
                txtnagno.Text = HFGP2.Rows[Index].Cells[4].Value.ToString();

                Genclass.val1 = Convert.ToDouble(HFGP2.Rows[Index].Cells[2].Value.ToString());
                double val2 = Convert.ToDouble(Genclass.val1) - Convert.ToDouble(txtqqty.Text);
                txtwaste.Text = val2.ToString();
                txtqqty.Focus();
                lkppnl.Visible = false;
            }
        }

        private void DataGridCommon_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                int Index = HFGP2.SelectedCells[0].RowIndex;
                if (type == 1)
                {
                    txtname.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtpuid.Text = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    loadgrisitem();
                }
                else
                {
                    textBox3.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    textBox3.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtbags.Text = DataGridCommon.Rows[Index].Cells[2].Value.ToString();


                }
            }
        }

        private void HFGP2_DoubleClick(object sender, EventArgs e)
        {
            int Index = HFGP2.SelectedCells[0].RowIndex;

            txtname.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
            txtpuid.Text = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
            txtqty.Text = DataGridCommon.Rows[Index].Cells[2].Value.ToString();
            txtbags.Text = DataGridCommon.Rows[Index].Cells[4].Value.ToString();
            txtoutputid.Text = DataGridCommon.Rows[Index].Cells[3].Value.ToString();
            txtqqty.Focus();
            lkppnl.Visible = false;
        }

        private void butexit_Click(object sender, EventArgs e)
        {
            string message = "Are You Sure to qc entry?";
            string caption = "Dilama";
            MessageBoxButtons buttons = MessageBoxButtons.YesNo;
            DialogResult result;

            result = MessageBox.Show(message, caption, buttons);

            if (result == System.Windows.Forms.DialogResult.Yes)
            {
                int i = HFGP.SelectedCells[0].RowIndex;
                int uid = HFGP.SelectedCells[0].RowIndex;

                if (Genclass.Dtype == 1190)
                {
                    Genclass.strsql = "SP_CHECKKNITTINGISSUE  " + HFGP.Rows[i].Cells[0].Value.ToString() + "";
                    Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                }
                else if (Genclass.Dtype == 1200)

                {
                    Genclass.strsql = "SP_CHECKFABRICQCGRN  " + HFGP.Rows[i].Cells[0].Value.ToString() + "";
                    Genclass.cmd = new SqlCommand(Genclass.strsql, conn);

                }
                SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
                DataTable tap1 = new DataTable();
                aptr1.Fill(tap1);
                if (tap1.Rows.Count == 0)
                {
                    conn.Close();
                    conn.Open();
                    qur.CommandText = "exec SP_CHECKDELETEQCGRN " + HFGP.CurrentRow.Cells[0].Value.ToString() + "," + Genclass.Dtype + "   ";
                    qur.ExecuteNonQuery();
                }
                else

                {
                    MessageBox.Show("Already Converted Fabric Issue");
                    return;

                }



            }


            Loadgrid();
            }

        private void HFIT_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void txtscr1_TextChanged(object sender, EventArgs e)
        {
            Loadgrid();
        }

        private void Txtscr2_TextChanged(object sender, EventArgs e)
        {
            Loadgrid();
        }

        private void txtscr5_TextChanged(object sender, EventArgs e)
        {
            Loadgrid();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            Loadgrid();
        }

        private void txtscr6_TextChanged(object sender, EventArgs e)
        {
            Loadgrid();
        }

        private void button12_Click(object sender, EventArgs e)
        {
            Editpnl.Visible = false;
            Genpan.Visible = true;
            panadd.Visible = true;
            Loadgrid();
        }

        private void RQGR_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                conn.Close();
                conn.Open();

                string quy = "select  * from jolqclist where uid='" + RQGR.CurrentRow.Cells[8].Value.ToString() + "'";
                Genclass.cmd = new SqlCommand(quy, conn);
                SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
                DataTable tap = new DataTable();
                aptr.Fill(tap);
                if (tap.Rows.Count > 0)
                {
                    qur.CommandText = "delete from jolqclist  where uid=" + tap.Rows[0]["uid"].ToString() + "";
                    qur.ExecuteNonQuery();

                }

               

            }
            loadg();
            loadK();
        }

        private void RQGR_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void button15_Click(object sender, EventArgs e)
        {
            conn.Close();
            conn.Open();

            if (txtqty.Text == "")
            {
                txtqty.Text = "0";

            }
            if (txtbags.Text == "")
            {
                txtbags.Text = "0";

            }
          

            if (txtgrnid.Text != "")
            {

                qur.CommandText = "update  jo set  docdate='" + dtpgrndt.Text + "',remarks='" + txtqty.Text + "' where uid=" + txtgrnid.Text + "  and doctypeid =" + Genclass.Dtype + "  and docno='" + txtgrn.Text + "'";
                qur.ExecuteNonQuery();
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {

        }

        private void label19_Click(object sender, EventArgs e)
        {

        }

        private void RQGR_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (RQGR.RowCount > 1)
            {
                if (RQGR.CurrentCell.ColumnIndex != 0)
                {
                    if (RQGR.CurrentCell.ColumnIndex == 5 && RQGR.CurrentRow.Cells[5].Value.ToString() != "0.0" || RQGR.CurrentCell.ColumnIndex == 6 && RQGR.CurrentRow.Cells[6].Value.ToString() != "0.0" || RQGR.CurrentCell.ColumnIndex == 7 && RQGR.CurrentRow.Cells[7].Value.ToString() != "0.0")
                    {



                        RQGR.CurrentRow.Cells[5].Value = RQGR.CurrentRow.Cells[5].Value.ToString();

                        RQGR.CurrentRow.Cells[6].Value = RQGR.CurrentRow.Cells[6].Value.ToString();

                        RQGR.CurrentRow.Cells[7].Value = RQGR.CurrentRow.Cells[7].Value.ToString();
                    }







                    double ii = Convert.ToDouble(RQGR.CurrentRow.Cells[4].Value);
                    double pp = Convert.ToDouble(RQGR.CurrentRow.Cells[5].Value) + Convert.ToDouble(RQGR.CurrentRow.Cells[6].Value) + Convert.ToDouble(RQGR.CurrentRow.Cells[7].Value);
                    if (ii > pp)
                    {

                        MessageBox.Show("not match the qty");
                        return;
                    }
                    else if (ii < pp)
                    {

                        MessageBox.Show("Qty exceed");
                        return;
                    }
                }
            }
        }
       
        private void RQGR_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            
        }
        private void cbxItemCode_SelectionChangeCommitted(object sender, EventArgs e)
        {
            if (RQGR.CurrentCell.ColumnIndex == 6)
            {

                this.RQGR.CurrentRow.Cells[6].Value = this.cbxRow5.Text;
            }
            else if (RQGR.CurrentCell.ColumnIndex == 7)
            {




                this.RQGR.CurrentRow.Cells[7].Value = this.cbxRow5.Text;
            }

            
         

        }
        private void RQGR_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            
        }

        private void button13_Click(object sender, EventArgs e)
        {

            btnsave_Click(sender, e);
               

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void label20_Click(object sender, EventArgs e)
        {

        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void label21_Click(object sender, EventArgs e)
        {

        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {

        }

        private void dtpgrndt_ValueChanged(object sender, EventArgs e)
        {

        }

        private void textBox3_TextChanged_1(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bsFabric.Filter = string.Format("Itemname LIKE '%{0}%' ", textBox2.Text);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void textBox3_Click(object sender, EventArgs e)
        {

            type = 2;
  
            DataTable dt = getParty();
            bsc.DataSource = dt;
            FillGrid1(dt, 1);
            Point loc = FindLocation(textBox3);
            grSearch.Location = new Point(loc.X, loc.Y + 20);
            grSearch.Visible = true;
        }

        private void chkact_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void Genpan_Paint(object sender, PaintEventArgs e)
        {

        }
    }
    }
    
    

