﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Drawing.Imaging;
using System.Configuration;
using System.Data.SqlClient;
using System.Diagnostics;
using excel = Microsoft.Office.Interop.Excel;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;

namespace MyEasyBizAPMS
{
    public partial class FrmFabOpstk : Form
    {
        public FrmFabOpstk()
        {

            this.BackColor = Color.White; ;
            InitializeComponent();
        }
        int uid = 0;
        int mode = 0;
        string tpuid = "";


        SqlConnection conn = new SqlConnection(GeneralParameters.ConnectionString);
        SqlCommand qur = new SqlCommand();
        SQLDBHelper db = new SQLDBHelper();
        int Fillid;
        BindingSource bs = new BindingSource();
        BindingSource bsParty = new BindingSource();
        BindingSource bsitem = new BindingSource();

        DataTable Docno = new DataTable();
        DataTable Docno1 = new DataTable();
        BindingSource bsc = new BindingSource();
        BindingSource bsp = new BindingSource();
        BindingSource bsserial = new BindingSource();
        BindingSource IN = new BindingSource();
        BindingSource OUT = new BindingSource();
        BindingSource bsFabric = new BindingSource();
        string str1key;
        public int SelectId = 0;
        private void FrmFabOpstk_Load(object sender, EventArgs e)
        {
            qur.Connection = conn;



            this.HFGP.DefaultCellStyle.Font = new Font("Calibri", 10);
            this.HFGP.ColumnHeadersDefaultCellStyle.Font = new Font("Calibri", 10, FontStyle.Bold);
            HFGP.RowHeadersVisible = false;
            HFGP.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            HFGP.EnableHeadersVisualStyles = false;
            HFGP.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.GradientActiveCaption;
            //Module.ClearTextBox(this, Genpan);

            txtitem.Visible = true;
            txtitcode.Visible = true;
            txtop.Visible = true;




            LoadGetJobCard(1);

        }

        private void loadprocess()

        {

            if (txtbags.Text != "" && txtprice.Text != "")
            {

                string qur = "exec sp_getsocnoFrmworkorderprocess " + txtprice.Tag + ",'" + txtprice.Text + "','" + txtbags.Text + "','" + txtitid.Text + "'";
                SqlCommand cmd = new SqlCommand(qur, conn);
                SqlDataAdapter apt = new SqlDataAdapter(cmd);
                DataTable tab = new DataTable();
                apt.Fill(tab);
                CNOTYPE.DataSource = null;
                CNOTYPE.DataSource = tab;
                CNOTYPE.DisplayMember = "process";
                CNOTYPE.ValueMember = "uid";
                CNOTYPE.SelectedIndex = -1;
            }


        }
        protected DataTable LoadGetJobCard(int tag)
        {


            DateTime str9 = Convert.ToDateTime(Dtpdt.Text);

            DataTable dt = new DataTable();
            try
            {

                SqlParameter[] para = {
                    new SqlParameter("@COMPANYID","1"),
                      new SqlParameter("@YEARID",str9.Year),


                };


              
                    dt = db.GetData(CommandType.StoredProcedure, "SP_GETfabricOPSTKLOAD", para);
               

                LoadDataTable(dt);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        protected void LoadDataTable(DataTable dt)
        {
            try
            {
                HFGP.DataSource = null;
                HFGP.AutoGenerateColumns = false;
                HFGP.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
                HFGP.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
                HFGP.ColumnCount = 9;
                HFGP.Columns[0].Name = "Uid";
                HFGP.Columns[0].HeaderText = "Uid";
                HFGP.Columns[0].DataPropertyName = "Uid";

                //HFGP.Columns[1].Name = "Code";
                //HFGP.Columns[1].HeaderText = "Code";
                //HFGP.Columns[1].DataPropertyName = "Code";

                HFGP.Columns[2].Name = "socno";
                HFGP.Columns[2].HeaderText = "socno";
                HFGP.Columns[2].DataPropertyName = "socno";

                HFGP.Columns[3].Name = "workorderno";
                HFGP.Columns[3].HeaderText = "workorderno";
                HFGP.Columns[3].DataPropertyName = "workorderno";

                HFGP.Columns[4].Name = "itemname";
                HFGP.Columns[4].HeaderText = "itemname";
                HFGP.Columns[4].DataPropertyName = "itemname";

                HFGP.Columns[5].Name = "process";
                HFGP.Columns[5].HeaderText = "process";
                HFGP.Columns[5].DataPropertyName = "process";

                HFGP.Columns[6].Name = "uom";
                HFGP.Columns[6].HeaderText = "uom";
                HFGP.Columns[6].DataPropertyName = "uom";

                HFGP.Columns[7].Name = "qty";
                HFGP.Columns[7].HeaderText = "qty";
                HFGP.Columns[7].DataPropertyName = "qty";

                HFGP.Columns[8].Name = "fabricid";
                HFGP.Columns[8].HeaderText = "fabricid";
                HFGP.Columns[8].DataPropertyName = "fabricid";



                bs.DataSource = dt;

                HFGP.DataSource = bs;


                HFGP.Columns[0].Visible = false;

                HFGP.Columns[1].Visible = false;
                HFGP.Columns[2].Width = 80;
                HFGP.Columns[3].Width = 80;
                HFGP.Columns[4].Width = 420;
                HFGP.Columns[5].Width = 90;
                HFGP.Columns[6].Width = 90;
                HFGP.Columns[7].Width = 50;
                HFGP.Columns[8].Visible = false;


            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void Loadgrid()
        {
            try
            {
                conn.Close();
                conn.Open();



                DateTime str9 = Convert.ToDateTime(Dtpdt.Text);



                string quy = "select b.uid,0as Code,B.Itemname,Generalname as Uom,a.Qty,a.Price, isnull(a.Qty,0)* isnull(a.price,0) as Value,a.Bags,a.uid as refuid from ItemOpening a inner join itemm b on a.itemuid=b.uid inner join generalm c on b.uom_uid=c.uid  where a.companyid=1 and yrid =" + str9.Year + "";
                Genclass.cmd = new SqlCommand(quy, conn);



                SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
                DataTable tap = new DataTable();
                aptr.Fill(tap);


                HFGP.AutoGenerateColumns = true;
                HFGP.Refresh();
                HFGP.DataSource = null;
                HFGP.Rows.Clear();


                HFGP.ColumnCount = tap.Columns.Count;
                Genclass.i = 0;
                foreach (DataColumn column in tap.Columns)
                {
                    HFGP.Columns[Genclass.i].Name = column.ColumnName;
                    HFGP.Columns[Genclass.i].HeaderText = column.ColumnName;
                    HFGP.Columns[Genclass.i].DataPropertyName = column.ColumnName;
                    Genclass.i = Genclass.i + 1;
                }




                HFGP.Columns[0].Visible = false;

                HFGP.Columns[1].Width = 130;
                HFGP.Columns[2].Width = 420;
                HFGP.Columns[3].Width = 80;
                HFGP.Columns[4].Width = 90;
                HFGP.Columns[5].Width = 90;
                HFGP.Columns[6].Width = 90;
                HFGP.Columns[7].Width = 90;
                HFGP.Columns[8].Visible = false;




                HFGP.DataSource = tap;



            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }
            finally
            {
                conn.Close();
                Genclass.cmd.Dispose();
            }
        }
        private void txtitcode_TextChanged(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void txtitem_TextChanged(object sender, EventArgs e)
        {

        }

        private void grSearch_Paint(object sender, PaintEventArgs e)
        {

        }

        private void HFGP_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void panadd_Paint(object sender, PaintEventArgs e)
        {

        }

        private void txtuom_TextChanged(object sender, EventArgs e)
        {

        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void txtop_TextChanged(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void txtprice_TextChanged(object sender, EventArgs e)
        {

        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void txtbags_TextChanged(object sender, EventArgs e)
        {

        }

        private void buttrqok_Click(object sender, EventArgs e)
        {

        }

        private void DataGridCommon_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void txtitem_TextChanged_1(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bsitem.Filter = string.Format("itemname LIKE '%{0}%' ", txtitem.Text);



                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
        protected DataTable get_item()
        {

            DataTable dt = new DataTable();
            try
            {

                string qur1 = "sp_getsocnoFrmworkorderItem "+ txtprice.Tag +",'"+ txtprice.Text + "','"+ txtbags.Text +"'";
                SqlCommand cmd1 = new SqlCommand(qur1, conn);
                SqlDataAdapter apt1 = new SqlDataAdapter(cmd1);
                //DataTable dt = new DataTable();
                apt1.Fill(dt);

                    bsitem.DataSource = dt;
                

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            return dt;
        }


        private void txtitem_Click(object sender, EventArgs e)
        {
            Genclass.type = 2;
            DataTable dt = get_item();
            bsitem.DataSource = dt;
            FillGrid(dt, 1);
            Point loc = FindLocation(txtitem);
            grSearch.Location = new Point(loc.X, loc.Y + 20);
            grSearch.Visible = true;
            grSearch.Text = "Itemname Search";
        }
        private Point FindLocation(Control ctrl)
        {
            if (ctrl.Parent is Form)
                return ctrl.Location;
            else
            {
                Point p = FindLocation(ctrl.Parent);
                p.X += ctrl.Location.X;
                p.Y += ctrl.Location.Y;
                return p;
            }
        }


        protected void FillGrid(DataTable dt, int FillId)
        {
            try
            {
                DataGridCommon.DataSource = null;
                DataGridCommon.AutoGenerateColumns = false;
                DataGridCommon.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
                DataGridCommon.DefaultCellStyle.WrapMode = DataGridViewTriState.True;

                DataGridCommon.ColumnCount = 3;
                DataGridCommon.Columns[0].Name = "Uid";
                DataGridCommon.Columns[0].HeaderText = "Uid";
                DataGridCommon.Columns[0].DataPropertyName = "Uid";

                DataGridCommon.Columns[1].Name = "Itemname";
                DataGridCommon.Columns[1].HeaderText = "Itemname";
                DataGridCommon.Columns[1].DataPropertyName = "Itemname";

                DataGridCommon.Columns[2].Name = "Uom";
                DataGridCommon.Columns[2].HeaderText = "Uom";
                DataGridCommon.Columns[2].DataPropertyName = "Uom";

                DataGridCommon.DataSource = bsitem;
                DataGridCommon.Columns[0].Visible = false;
                DataGridCommon.Columns[2].Visible = false;
                DataGridCommon.Columns[1].Width = 500;


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        protected DataTable getParty()
        {
            DataTable dt = new DataTable();
            try
            {
                dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "sp_getsocnoFrmworkorder", conn);
                bsc.DataSource = dt;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            return dt;
        }

        private void txtitem_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.F10)
                {
                    grSearch.Visible = false;
                }
                else if (e.KeyCode == Keys.F2)
                {
                    SelectId = 1;
                    int Index = DataGridCommon.SelectedCells[0].RowIndex;

                    txtitid.Text = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    txtitem.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();

                    txtop.Focus();

                    grSearch.Visible = false;
                    SelectId = 0;
                }
                else if (e.KeyCode == Keys.Escape)
                {
                    grSearch.Visible = false;
                }
                else if (e.KeyValue == 40)
                {
                    DataGridCommon.Select();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void button18_Click(object sender, EventArgs e)
        {
            try
            {
                SelectId = 1;
                int Index = DataGridCommon.SelectedCells[0].RowIndex;
                if (Genclass.type == 1)
                {

                    txtbags.Text = DataGridCommon.Rows[Index].Cells[2].Value.ToString();
                    txtprice.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtprice.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                 
                }
                else
                    {

                    txtitid.Text = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    txtitem.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtuom.Text = DataGridCommon.Rows[Index].Cells[2].Value.ToString();
                    loadprocess();
                }
            
                txtop.Focus();


                grSearch.Visible = false;
                SelectId = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnHide_Click(object sender, EventArgs e)
        {
            grSearch.Visible = false;
        }

        private void DataGridCommon_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SelectId = 1;
                int Index = DataGridCommon.SelectedCells[0].RowIndex;
                if (Genclass.type == 1)
                {

                    txtbags.Text = DataGridCommon.Rows[Index].Cells[2].Value.ToString();
                    txtprice.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtprice.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();

                }
                else
                {

                    txtitid.Text = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    txtitem.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtuom.Text = DataGridCommon.Rows[Index].Cells[2].Value.ToString();
                    loadprocess();
                }

                txtop.Focus();



                grSearch.Visible = false;
                SelectId = 0;
            }
        }

        private void buttnext1_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void buttrqok_Click_1(object sender, EventArgs e)
        {
            conn.Close();
            conn.Open();
           
            DateTime str9 = Convert.ToDateTime(Dtpdt.Text);
            Genclass.address = "";
            string quy = "sp_getsocnoFrmworkorderprocessAll " + txtprice.Tag + ",'" + txtprice.Text + "','" + txtbags.Text + "','" + txtitid.Text + "',"+ CNOTYPE.SelectedValue +"  ";
            Genclass.cmd = new SqlCommand(quy, conn);
            SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
            DataTable tap = new DataTable();
            aptr.Fill(tap);

            if(tap.Rows.Count>0)

            {
                for(int i=0; i < tap.Rows.Count; i++)
                {
                    if(i==0)
                    {

                        if (tap.Rows[0]["colour"].ToString() != "Blank")
                        {
                            Genclass.address = tap.Rows[0]["process"].ToString() + " / " + tap.Rows[0]["colour"].ToString();
                        }
                        else
                        {

                            Genclass.address = tap.Rows[0]["process"].ToString();
                        }

                    }
                    else
                    {
                        if (tap.Rows[i]["colour"].ToString() != "Blank")

                        {
                            Genclass.address = Genclass.address + "/ " + tap.Rows[i]["process"].ToString() + " / " + tap.Rows[i]["colour"].ToString();

                        }
                        else
                        {
                            Genclass.address = Genclass.address + "/ " + tap.Rows[i]["process"].ToString();


                        }
                    }

                }


            }

            Genclass.address=txtitem.Text + "/ " + Genclass.address;

            string quy1 = "select * from fabricopstk  where yrid=" + str9.Year + " and fabricid=" + txtitid.Text + "";
            Genclass.cmd = new SqlCommand(quy1, conn);
            SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
            DataTable tap1 = new DataTable();
            aptr1.Fill(tap1);
            if (tap1.Rows.Count == 0)
            {

       

                SqlParameter[] para ={
                    new SqlParameter("@socno", txtprice.Text),
                             new SqlParameter("@workorderno", txtbags.Text),
                                                 new SqlParameter("@itemname", Genclass.address ),
                                new SqlParameter("@process", CNOTYPE.Text),
                                  new SqlParameter("@uom", txtuom.Text),
                                     new SqlParameter("@QTY", Convert.ToDecimal(txtop.Text)),
                    new SqlParameter("@fabricid", txtitid.Text),
   
                     new SqlParameter("@companyid",1),

           
                    new SqlParameter("@YRID",Convert.ToInt32(str9.Year)),
       
        
                };
                db.ExecuteNonQuery(CommandType.StoredProcedure, "sp_fabricopstksave", para, conn);
            }
            else

            {
              
                SqlParameter[] para ={
                    new SqlParameter("@socno", txtprice.Text),
                             new SqlParameter("@workorderno", txtbags.Text),
                                                 new SqlParameter("@itemname", Genclass.address ),
                                new SqlParameter("@process", CNOTYPE.Text),
                                  new SqlParameter("@uom", txtuom.Text),
                                     new SqlParameter("@QTY", Convert.ToDecimal(txtop.Text)),
                    new SqlParameter("@fabricid", txtitid.Text),

                     new SqlParameter("@companyid",1),


                    new SqlParameter("@YRID",Convert.ToInt32(str9.Year)),
                         new SqlParameter("@uid",tap1.Rows[0]["uid"].ToString()),

                };
                db.ExecuteNonQuery(CommandType.StoredProcedure, "sp_fabricopstkupdate", para, conn);


            }
            //Genclass.Module.ClearTextBox(this, Genpan);

            string quy2 = "select max(uid) as uid from fabricopstk  ";
            Genclass.cmd = new SqlCommand(quy2, conn);
            SqlDataAdapter aptr2 = new SqlDataAdapter(Genclass.cmd);
            DataTable tap2 = new DataTable();
            aptr2.Fill(tap2);
            if (tap2.Rows.Count > 0)
            {
             
                    qur.CommandText = "sp_savefabopstock  " + tap2.Rows[0]["uid"].ToString() + "";
                    qur.ExecuteNonQuery();
              
            }


            txtitem.Text = String.Empty;
            txtop.Text = String.Empty;
            txtuom.Text = String.Empty;
            txtprice.Text = String.Empty;
            txtbags.Text = String.Empty;

            LoadGetJobCard(1);
        }

        private void HFGP_CellContentClick_1(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void HFGP_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                conn.Close();
                conn.Open();
                qur.CommandText = "delete from ItemOpening where uid=" + HFGP.CurrentRow.Cells[7].Value.ToString() + "";
                qur.ExecuteNonQuery();
                Loadgrid();
            }
        }

        private void DataGridCommon_CellContentClick_1(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void txtitid_TextChanged(object sender, EventArgs e)
        {
            if (txtitid.Text != "")
            {

                if (CNOTYPE.Text == "Yarn")
                {
                    Genclass.strsql = "select * from yarnmaster a inner join generalm b on a.uom=b.guid where a.active=1  and a.yarnid=" + txtitid.Text + " ";
                    Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                    SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
                    DataTable tap = new DataTable();
                    aptr.Fill(tap);
                    if (tap.Rows.Count > 0)
                    {

                        //txtitcode.Text = tap.Rows[0]["itemcode"].ToString();
                        txtuom.Text = tap.Rows[0]["generalname"].ToString();

                    }
                }
                else if (CNOTYPE.Text == "Trims")

                {
                    Genclass.strsql = "select * from trimsm a  INNER JOIN	itemsm I ON I.ITEMCategoryUid=a.CATEGORYUID inner join generalm b on i.baseuomuid=b.guid where  a.uid=" + txtitid.Text + " ";
                    Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                    SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
                    DataTable tap = new DataTable();
                    aptr.Fill(tap);
                    if (tap.Rows.Count > 0)
                    {

                        //txtitcode.Text = tap.Rows[0]["itemcode"].ToString();
                        txtuom.Text = tap.Rows[0]["generalname"].ToString();

                    }

                }
                else if (CNOTYPE.Text == "Fabric")

                {
                    Genclass.strsql = "select Generalname from Fabricm b   inner join generalm c on b.planuom = c.Guid   where  b.uid=" + txtitid.Text + "   ";
                    Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                    SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
                    DataTable tap = new DataTable();
                    aptr.Fill(tap);
                    if (tap.Rows.Count > 0)
                    {

                        //txtitcode.Text = tap.Rows[0]["itemcode"].ToString();
                        txtuom.Text = tap.Rows[0]["generalname"].ToString();

                    }

                }
            }
        }

        private void txtbags_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                buttrqok_Click_1(sender, e);
            }
        }

        private void txtbags_TextChanged_1(object sender, EventArgs e)
        {

        }

        private void HFGP_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (HFGP.CurrentRow.Cells[0].Value.ToString() != "" && HFGP.CurrentRow.Cells[0].Value.ToString() != null)
            {
             if (HFGP.CurrentCell.ColumnIndex == 7)

                {
                    DataGridViewCell cell = HFGP.CurrentRow.Cells[7];
                    HFGP.CurrentCell = cell;
                    HFGP.BeginEdit(true);
                }


            }

        }

        private void HFGP_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (HFGP.CurrentRow.Cells[7].Value.ToString() != "0.000" && HFGP.CurrentCell.ColumnIndex == 7)
            {
                conn.Close();
                conn.Open();


                if (HFGP.CurrentRow.Cells[7].Value.ToString() != "" && HFGP.CurrentRow.Cells[7].Value.ToString() != "")
                {
               


                        qur.CommandText = "update fabricopstk  set qty=" + HFGP.CurrentRow.Cells[7].Value.ToString() + "  where uid=" + HFGP.CurrentRow.Cells[0].Value.ToString() + "";
                        qur.ExecuteNonQuery();
                    


                }



            }
          

                LoadGetJobCard(1);
            }

        private void CNOTYPE_SelectedIndexChanged(object sender, EventArgs e)
        {
            

            
        }

        private void txtprice_TextChanged_1(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bsFabric.Filter = string.Format("Itemname LIKE '%{0}%' ", txtprice.Text);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtprice_Click(object sender, EventArgs e)
        {
            Genclass.type = 1;
           
            DataTable dt = getParty();
            bsc.DataSource = dt;
            FillGrid1(dt, 1);
            Point loc = FindLocation(txtprice);
            grSearch.Location = new Point(loc.X, loc.Y + 20);
            grSearch.Visible = true;
        }
        protected void FillGrid1(DataTable dt, int Fillid)
        {
            try
            {
                DataGridCommon.DataSource = null;
                DataGridCommon.AutoGenerateColumns = false;
                DataGridCommon.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
                DataGridCommon.DefaultCellStyle.WrapMode = DataGridViewTriState.True;

                DataGridCommon.ColumnCount = 3;
                DataGridCommon.Columns[0].Name = "Uid";
                DataGridCommon.Columns[0].HeaderText = "Uid";
                DataGridCommon.Columns[0].DataPropertyName = "Uid";
                DataGridCommon.Columns[1].Name = "Socno";
                DataGridCommon.Columns[1].HeaderText = "Socno";
                DataGridCommon.Columns[1].DataPropertyName = "Socno";
                DataGridCommon.Columns[1].Width = 120;
                DataGridCommon.Columns[2].Name = "Workoder";
                DataGridCommon.Columns[2].HeaderText = "Workoder";
                DataGridCommon.Columns[2].DataPropertyName = "Workoder";
                DataGridCommon.Columns[2].Width = 120;
                DataGridCommon.DataSource = bsc;
                DataGridCommon.Columns[0].Visible = false;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void DataGridCommon_Click(object sender, EventArgs e)
        {
            int Index = DataGridCommon.SelectedCells[0].RowIndex;
            if (Genclass.type == 1)
            {

                txtbags.Text = DataGridCommon.Rows[Index].Cells[2].Value.ToString();
                txtprice.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                txtprice.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();

            }
            else
            {

                txtitid.Text = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                txtitem.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                txtuom.Text = DataGridCommon.Rows[Index].Cells[2].Value.ToString();
                loadprocess();
            }
            grSearch.Visible = false;
        }
    }
    }





