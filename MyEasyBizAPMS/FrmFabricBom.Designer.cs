﻿namespace MyEasyBizAPMS
{
    partial class FrmFabricBom
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Syncfusion.Windows.Forms.Tools.SplitButtonRenderer splitButtonRenderer1 = new Syncfusion.Windows.Forms.Tools.SplitButtonRenderer();
            Syncfusion.Windows.Forms.Tools.SplitButtonRenderer splitButtonRenderer2 = new Syncfusion.Windows.Forms.Tools.SplitButtonRenderer();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            this.SplitSave = new Syncfusion.Windows.Forms.Tools.SplitButton();
            this.toolClose = new Syncfusion.Windows.Forms.Tools.toolstripitem();
            this.SplitAdd = new Syncfusion.Windows.Forms.Tools.SplitButton();
            this.toolEdit = new Syncfusion.Windows.Forms.Tools.toolstripitem();
            this.toolCls = new Syncfusion.Windows.Forms.Tools.toolstripitem();
            this.grBack = new System.Windows.Forms.GroupBox();
            this.grSearch = new System.Windows.Forms.GroupBox();
            this.btnSelect = new System.Windows.Forms.Button();
            this.DataGridCommon = new System.Windows.Forms.DataGridView();
            this.btnHide = new System.Windows.Forms.Button();
            this.txtOrderSts = new System.Windows.Forms.TextBox();
            this.txtYear = new System.Windows.Forms.TextBox();
            this.txtSeason = new System.Windows.Forms.TextBox();
            this.CmbStyle = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.cmbOrderType = new System.Windows.Forms.ComboBox();
            this.cmbSts = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.dtpDeliveryDate = new System.Windows.Forms.DateTimePicker();
            this.label8 = new System.Windows.Forms.Label();
            this.chckFabFinalProcess = new System.Windows.Forms.CheckBox();
            this.cmbYear = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.cmbSeason = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.dtpOrderDate = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.txtOrderNo = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.dtpDocDate = new System.Windows.Forms.DateTimePicker();
            this.txtDocNo = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtOrderType = new System.Windows.Forms.TextBox();
            this.txtMerchindiser = new System.Windows.Forms.TextBox();
            this.txtCustomer = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.tabControlAdv1 = new Syncfusion.Windows.Forms.Tools.TabControlAdv();
            this.tabPageFabricColour = new Syncfusion.Windows.Forms.Tools.TabPageAdv();
            this.CmbStructureType = new System.Windows.Forms.ComboBox();
            this.label42 = new System.Windows.Forms.Label();
            this.DataGridComponent = new System.Windows.Forms.DataGridView();
            this.chckFinalProcess = new System.Windows.Forms.CheckBox();
            this.ChckRollForm = new System.Windows.Forms.CheckBox();
            this.button1 = new System.Windows.Forms.Button();
            this.txtComboClr = new System.Windows.Forms.TextBox();
            this.DataGridComboColour = new System.Windows.Forms.DataGridView();
            this.label36 = new System.Windows.Forms.Label();
            this.BtnFabricColour = new System.Windows.Forms.Button();
            this.label35 = new System.Windows.Forms.Label();
            this.txtFabricColourComposition = new System.Windows.Forms.TextBox();
            this.label34 = new System.Windows.Forms.Label();
            this.CmbComponentColour = new System.Windows.Forms.ComboBox();
            this.label33 = new System.Windows.Forms.Label();
            this.CmbFabricColourType = new System.Windows.Forms.ComboBox();
            this.label32 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.DatGridColourCompostion = new System.Windows.Forms.DataGridView();
            this.DatagridFabricColour = new System.Windows.Forms.DataGridView();
            this.CmbFabricCombo = new System.Windows.Forms.ComboBox();
            this.tabFabricAllocation = new Syncfusion.Windows.Forms.Tools.TabPageAdv();
            this.CmbFabAll = new System.Windows.Forms.ComboBox();
            this.label37 = new System.Windows.Forms.Label();
            this.txtDiaApply = new System.Windows.Forms.TextBox();
            this.ChckApplyAll = new System.Windows.Forms.CheckBox();
            this.ChckDetailFabricClr = new System.Windows.Forms.CheckBox();
            this.label31 = new System.Windows.Forms.Label();
            this.DataGridSize = new System.Windows.Forms.DataGridView();
            this.cmbType = new System.Windows.Forms.ComboBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.btnOk = new System.Windows.Forms.Button();
            this.txttAvgWt = new System.Windows.Forms.TextBox();
            this.txtToterence = new System.Windows.Forms.TextBox();
            this.txtGSM = new System.Windows.Forms.TextBox();
            this.ChckSelectAll = new System.Windows.Forms.CheckBox();
            this.txtFabric = new System.Windows.Forms.TextBox();
            this.txtComponent = new System.Windows.Forms.TextBox();
            this.DataGridFabricColourSummary = new System.Windows.Forms.DataGridView();
            this.DataGridFabric = new System.Windows.Forms.DataGridView();
            this.tabFabric = new Syncfusion.Windows.Forms.Tools.TabPageAdv();
            this.CmbFabricProcess = new System.Windows.Forms.ComboBox();
            this.chckFabProcessFinal = new System.Windows.Forms.CheckBox();
            this.label29 = new System.Windows.Forms.Label();
            this.DataGridFabricProcess = new System.Windows.Forms.DataGridView();
            this.btnFabricProcess = new System.Windows.Forms.Button();
            this.txtFabricProcessLoss = new System.Windows.Forms.TextBox();
            this.txtFProcess = new System.Windows.Forms.TextBox();
            this.txtFabricSeqNo = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.CmbProcessColour = new System.Windows.Forms.ComboBox();
            this.txtFabricColour = new System.Windows.Forms.TextBox();
            this.txtFabricProcess = new System.Windows.Forms.TextBox();
            this.tabYarnProcess = new Syncfusion.Windows.Forms.Tools.TabPageAdv();
            this.label41 = new System.Windows.Forms.Label();
            this.CmbFabBOMYarn = new System.Windows.Forms.ComboBox();
            this.CmbYarnProcessCombo = new System.Windows.Forms.ComboBox();
            this.label40 = new System.Windows.Forms.Label();
            this.BtnYarnColourCompOk = new System.Windows.Forms.Button();
            this.txtYarnComposition = new System.Windows.Forms.TextBox();
            this.txtyarnColour = new System.Windows.Forms.TextBox();
            this.label39 = new System.Windows.Forms.Label();
            this.DataGridYarnColour = new System.Windows.Forms.DataGridView();
            this.label38 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.CmbStage = new System.Windows.Forms.ComboBox();
            this.chckFinalYarn = new System.Windows.Forms.CheckBox();
            this.CmboYarnProcessColor = new System.Windows.Forms.ComboBox();
            this.DataGridYarnProcess = new System.Windows.Forms.DataGridView();
            this.BtnYarnProcess = new System.Windows.Forms.Button();
            this.txtProcessLoss = new System.Windows.Forms.TextBox();
            this.txtProcess = new System.Windows.Forms.TextBox();
            this.txtSeqNo = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.CmbYarn = new System.Windows.Forms.ComboBox();
            this.txtYarn = new System.Windows.Forms.TextBox();
            this.DataGridYarnProcessCombo = new System.Windows.Forms.DataGridView();
            this.tabYarn = new Syncfusion.Windows.Forms.Tools.TabPageAdv();
            this.BtnYarnOk = new System.Windows.Forms.Button();
            this.txtMixing = new System.Windows.Forms.TextBox();
            this.txtClr = new System.Windows.Forms.TextBox();
            this.txtYarnAllec = new System.Windows.Forms.TextBox();
            this.DataGridYarnComponent = new System.Windows.Forms.DataGridView();
            this.CmbYarnComp = new System.Windows.Forms.ComboBox();
            this.label19 = new System.Windows.Forms.Label();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.grFront = new System.Windows.Forms.GroupBox();
            this.BtnPrint = new System.Windows.Forms.Button();
            this.SfdDataGridFabricBom = new Syncfusion.WinForms.DataGrid.SfDataGrid();
            this.grBack.SuspendLayout();
            this.grSearch.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridCommon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabControlAdv1)).BeginInit();
            this.tabControlAdv1.SuspendLayout();
            this.tabPageFabricColour.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridComponent)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridComboColour)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DatGridColourCompostion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DatagridFabricColour)).BeginInit();
            this.tabFabricAllocation.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridSize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridFabricColourSummary)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridFabric)).BeginInit();
            this.tabFabric.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridFabricProcess)).BeginInit();
            this.tabYarnProcess.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridYarnColour)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridYarnProcess)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridYarnProcessCombo)).BeginInit();
            this.tabYarn.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridYarnComponent)).BeginInit();
            this.grFront.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SfdDataGridFabricBom)).BeginInit();
            this.SuspendLayout();
            // 
            // SplitSave
            // 
            this.SplitSave.BackColor = System.Drawing.SystemColors.Control;
            this.SplitSave.BeforeTouchSize = new System.Drawing.Size(75, 29);
            this.SplitSave.DropDownItems.Add(this.toolClose);
            this.SplitSave.DropDownPosition = Syncfusion.Windows.Forms.Tools.Position.Bottom;
            this.SplitSave.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SplitSave.ForeColor = System.Drawing.Color.Black;
            this.SplitSave.Location = new System.Drawing.Point(1032, 562);
            this.SplitSave.MinimumSize = new System.Drawing.Size(75, 23);
            this.SplitSave.Name = "SplitSave";
            splitButtonRenderer1.SplitButton = this.SplitSave;
            this.SplitSave.Renderer = splitButtonRenderer1;
            this.SplitSave.ShowDropDownOnButtonClick = false;
            this.SplitSave.Size = new System.Drawing.Size(75, 29);
            this.SplitSave.TabIndex = 35;
            this.SplitSave.Text = "Save";
            this.SplitSave.DropDowItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.SplitSave_DropDowItemClicked);
            this.SplitSave.Click += new System.EventHandler(this.SplitSave_Click);
            // 
            // toolClose
            // 
            this.toolClose.Name = "toolClose";
            this.toolClose.Size = new System.Drawing.Size(23, 23);
            this.toolClose.Text = "Back";
            // 
            // SplitAdd
            // 
            this.SplitAdd.BackColor = System.Drawing.SystemColors.Control;
            this.SplitAdd.BeforeTouchSize = new System.Drawing.Size(88, 31);
            this.SplitAdd.DropDownItems.Add(this.toolEdit);
            this.SplitAdd.DropDownItems.Add(this.toolCls);
            this.SplitAdd.DropDownPosition = Syncfusion.Windows.Forms.Tools.Position.Bottom;
            this.SplitAdd.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SplitAdd.ForeColor = System.Drawing.Color.Black;
            this.SplitAdd.Location = new System.Drawing.Point(909, 562);
            this.SplitAdd.MinimumSize = new System.Drawing.Size(75, 23);
            this.SplitAdd.Name = "SplitAdd";
            splitButtonRenderer2.SplitButton = this.SplitAdd;
            this.SplitAdd.Renderer = splitButtonRenderer2;
            this.SplitAdd.ShowDropDownOnButtonClick = false;
            this.SplitAdd.Size = new System.Drawing.Size(88, 31);
            this.SplitAdd.TabIndex = 0;
            this.SplitAdd.Text = "Add";
            this.SplitAdd.DropDowItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.SplitAdd_DropDowItemClicked);
            this.SplitAdd.Click += new System.EventHandler(this.SplitAdd_Click);
            // 
            // toolEdit
            // 
            this.toolEdit.Name = "toolEdit";
            this.toolEdit.Size = new System.Drawing.Size(23, 23);
            this.toolEdit.Text = "Edit";
            // 
            // toolCls
            // 
            this.toolCls.Name = "toolCls";
            this.toolCls.Size = new System.Drawing.Size(23, 23);
            this.toolCls.Text = "Close";
            // 
            // grBack
            // 
            this.grBack.Controls.Add(this.grSearch);
            this.grBack.Controls.Add(this.txtOrderSts);
            this.grBack.Controls.Add(this.txtYear);
            this.grBack.Controls.Add(this.txtSeason);
            this.grBack.Controls.Add(this.CmbStyle);
            this.grBack.Controls.Add(this.SplitSave);
            this.grBack.Controls.Add(this.label12);
            this.grBack.Controls.Add(this.cmbOrderType);
            this.grBack.Controls.Add(this.cmbSts);
            this.grBack.Controls.Add(this.label11);
            this.grBack.Controls.Add(this.label10);
            this.grBack.Controls.Add(this.dtpDeliveryDate);
            this.grBack.Controls.Add(this.label8);
            this.grBack.Controls.Add(this.chckFabFinalProcess);
            this.grBack.Controls.Add(this.cmbYear);
            this.grBack.Controls.Add(this.label7);
            this.grBack.Controls.Add(this.cmbSeason);
            this.grBack.Controls.Add(this.label6);
            this.grBack.Controls.Add(this.dtpOrderDate);
            this.grBack.Controls.Add(this.label3);
            this.grBack.Controls.Add(this.txtOrderNo);
            this.grBack.Controls.Add(this.label4);
            this.grBack.Controls.Add(this.dtpDocDate);
            this.grBack.Controls.Add(this.txtDocNo);
            this.grBack.Controls.Add(this.label1);
            this.grBack.Controls.Add(this.txtOrderType);
            this.grBack.Controls.Add(this.txtMerchindiser);
            this.grBack.Controls.Add(this.txtCustomer);
            this.grBack.Controls.Add(this.label5);
            this.grBack.Controls.Add(this.label2);
            this.grBack.Controls.Add(this.label9);
            this.grBack.Controls.Add(this.checkBox1);
            this.grBack.Controls.Add(this.tabControlAdv1);
            this.grBack.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grBack.Location = new System.Drawing.Point(7, -1);
            this.grBack.Name = "grBack";
            this.grBack.Size = new System.Drawing.Size(1113, 599);
            this.grBack.TabIndex = 0;
            this.grBack.TabStop = false;
            // 
            // grSearch
            // 
            this.grSearch.Controls.Add(this.btnSelect);
            this.grSearch.Controls.Add(this.DataGridCommon);
            this.grSearch.Controls.Add(this.btnHide);
            this.grSearch.Location = new System.Drawing.Point(452, 21);
            this.grSearch.Name = "grSearch";
            this.grSearch.Size = new System.Drawing.Size(348, 225);
            this.grSearch.TabIndex = 36;
            this.grSearch.TabStop = false;
            this.grSearch.Visible = false;
            // 
            // btnSelect
            // 
            this.btnSelect.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnSelect.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSelect.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnSelect.Location = new System.Drawing.Point(281, 195);
            this.btnSelect.Name = "btnSelect";
            this.btnSelect.Size = new System.Drawing.Size(61, 28);
            this.btnSelect.TabIndex = 396;
            this.btnSelect.Text = "Select";
            this.btnSelect.UseVisualStyleBackColor = false;
            this.btnSelect.Click += new System.EventHandler(this.BtnSelect_Click);
            // 
            // DataGridCommon
            // 
            this.DataGridCommon.AllowUserToAddRows = false;
            this.DataGridCommon.BackgroundColor = System.Drawing.Color.White;
            this.DataGridCommon.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridCommon.Location = new System.Drawing.Point(4, 12);
            this.DataGridCommon.MultiSelect = false;
            this.DataGridCommon.Name = "DataGridCommon";
            this.DataGridCommon.ReadOnly = true;
            this.DataGridCommon.RowHeadersVisible = false;
            this.DataGridCommon.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridCommon.Size = new System.Drawing.Size(336, 183);
            this.DataGridCommon.TabIndex = 0;
            this.DataGridCommon.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.DataGridCommon_CellMouseDoubleClick);
            // 
            // btnHide
            // 
            this.btnHide.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnHide.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHide.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnHide.Location = new System.Drawing.Point(3, 195);
            this.btnHide.Name = "btnHide";
            this.btnHide.Size = new System.Drawing.Size(65, 28);
            this.btnHide.TabIndex = 395;
            this.btnHide.Text = "Close";
            this.btnHide.UseVisualStyleBackColor = false;
            this.btnHide.Click += new System.EventHandler(this.BtnHide_Click);
            // 
            // txtOrderSts
            // 
            this.txtOrderSts.Enabled = false;
            this.txtOrderSts.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtOrderSts.Location = new System.Drawing.Point(759, 48);
            this.txtOrderSts.Name = "txtOrderSts";
            this.txtOrderSts.Size = new System.Drawing.Size(150, 23);
            this.txtOrderSts.TabIndex = 40;
            this.txtOrderSts.Visible = false;
            // 
            // txtYear
            // 
            this.txtYear.Enabled = false;
            this.txtYear.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtYear.Location = new System.Drawing.Point(749, 47);
            this.txtYear.Name = "txtYear";
            this.txtYear.Size = new System.Drawing.Size(128, 23);
            this.txtYear.TabIndex = 39;
            this.txtYear.Visible = false;
            // 
            // txtSeason
            // 
            this.txtSeason.Enabled = false;
            this.txtSeason.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSeason.Location = new System.Drawing.Point(749, 46);
            this.txtSeason.Name = "txtSeason";
            this.txtSeason.Size = new System.Drawing.Size(181, 23);
            this.txtSeason.TabIndex = 38;
            this.txtSeason.Visible = false;
            // 
            // CmbStyle
            // 
            this.CmbStyle.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CmbStyle.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CmbStyle.FormattingEnabled = true;
            this.CmbStyle.Location = new System.Drawing.Point(760, 17);
            this.CmbStyle.Name = "CmbStyle";
            this.CmbStyle.Size = new System.Drawing.Size(205, 23);
            this.CmbStyle.TabIndex = 37;
            this.CmbStyle.SelectedIndexChanged += new System.EventHandler(this.CmbStyle_SelectedIndexChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(701, 52);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(51, 15);
            this.label12.TabIndex = 33;
            this.label12.Text = "Style No";
            this.label12.Visible = false;
            // 
            // cmbOrderType
            // 
            this.cmbOrderType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbOrderType.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbOrderType.FormattingEnabled = true;
            this.cmbOrderType.Location = new System.Drawing.Point(760, 47);
            this.cmbOrderType.Name = "cmbOrderType";
            this.cmbOrderType.Size = new System.Drawing.Size(150, 23);
            this.cmbOrderType.TabIndex = 29;
            this.cmbOrderType.Visible = false;
            // 
            // cmbSts
            // 
            this.cmbSts.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSts.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbSts.FormattingEnabled = true;
            this.cmbSts.Location = new System.Drawing.Point(760, 17);
            this.cmbSts.Name = "cmbSts";
            this.cmbSts.Size = new System.Drawing.Size(150, 23);
            this.cmbSts.TabIndex = 28;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(687, 51);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(66, 15);
            this.label11.TabIndex = 31;
            this.label11.Text = "Order Type";
            this.label11.Visible = false;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(712, 21);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(33, 15);
            this.label10.TabIndex = 30;
            this.label10.Text = "Style";
            // 
            // dtpDeliveryDate
            // 
            this.dtpDeliveryDate.Enabled = false;
            this.dtpDeliveryDate.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpDeliveryDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpDeliveryDate.Location = new System.Drawing.Point(749, 47);
            this.dtpDeliveryDate.Name = "dtpDeliveryDate";
            this.dtpDeliveryDate.Size = new System.Drawing.Size(136, 23);
            this.dtpDeliveryDate.TabIndex = 21;
            this.dtpDeliveryDate.Visible = false;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(662, 51);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(80, 15);
            this.label8.TabIndex = 26;
            this.label8.Text = "Delivery Date";
            this.label8.Visible = false;
            // 
            // chckFabFinalProcess
            // 
            this.chckFabFinalProcess.AutoSize = true;
            this.chckFabFinalProcess.Location = new System.Drawing.Point(971, 25);
            this.chckFabFinalProcess.Name = "chckFabFinalProcess";
            this.chckFabFinalProcess.Size = new System.Drawing.Size(110, 22);
            this.chckFabFinalProcess.TabIndex = 49;
            this.chckFabFinalProcess.Text = "Is Final Fabric";
            this.chckFabFinalProcess.UseVisualStyleBackColor = true;
            this.chckFabFinalProcess.Visible = false;
            // 
            // cmbYear
            // 
            this.cmbYear.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbYear.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbYear.FormattingEnabled = true;
            this.cmbYear.Items.AddRange(new object[] {
            "2010",
            "2011",
            "2012",
            "2013",
            "2014",
            "2015",
            "2016",
            "2017",
            "2018"});
            this.cmbYear.Location = new System.Drawing.Point(749, 47);
            this.cmbYear.Name = "cmbYear";
            this.cmbYear.Size = new System.Drawing.Size(128, 23);
            this.cmbYear.TabIndex = 20;
            this.cmbYear.Visible = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(715, 51);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(30, 15);
            this.label7.TabIndex = 25;
            this.label7.Text = "Year";
            this.label7.Visible = false;
            // 
            // cmbSeason
            // 
            this.cmbSeason.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSeason.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbSeason.FormattingEnabled = true;
            this.cmbSeason.Location = new System.Drawing.Point(750, 46);
            this.cmbSeason.Name = "cmbSeason";
            this.cmbSeason.Size = new System.Drawing.Size(180, 23);
            this.cmbSeason.TabIndex = 19;
            this.cmbSeason.Visible = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(697, 50);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(46, 15);
            this.label6.TabIndex = 24;
            this.label6.Text = "Season";
            this.label6.Visible = false;
            // 
            // dtpOrderDate
            // 
            this.dtpOrderDate.Enabled = false;
            this.dtpOrderDate.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpOrderDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpOrderDate.Location = new System.Drawing.Point(760, 46);
            this.dtpOrderDate.Name = "dtpOrderDate";
            this.dtpOrderDate.Size = new System.Drawing.Size(136, 23);
            this.dtpOrderDate.TabIndex = 12;
            this.dtpOrderDate.Visible = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(691, 50);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(67, 15);
            this.label3.TabIndex = 15;
            this.label3.Text = "Order Date";
            this.label3.Visible = false;
            // 
            // txtOrderNo
            // 
            this.txtOrderNo.Enabled = false;
            this.txtOrderNo.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtOrderNo.Location = new System.Drawing.Point(527, 17);
            this.txtOrderNo.Name = "txtOrderNo";
            this.txtOrderNo.Size = new System.Drawing.Size(136, 23);
            this.txtOrderNo.TabIndex = 11;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(463, 21);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(46, 15);
            this.label4.TabIndex = 14;
            this.label4.Text = "Doc No";
            // 
            // dtpDocDate
            // 
            this.dtpDocDate.Enabled = false;
            this.dtpDocDate.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpDocDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpDocDate.Location = new System.Drawing.Point(325, 17);
            this.dtpDocDate.Name = "dtpDocDate";
            this.dtpDocDate.Size = new System.Drawing.Size(136, 23);
            this.dtpDocDate.TabIndex = 9;
            // 
            // txtDocNo
            // 
            this.txtDocNo.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDocNo.Location = new System.Drawing.Point(76, 13);
            this.txtDocNo.Name = "txtDocNo";
            this.txtDocNo.Size = new System.Drawing.Size(136, 23);
            this.txtDocNo.TabIndex = 0;
            this.txtDocNo.Click += new System.EventHandler(this.TxtDocNo_Enter);
            this.txtDocNo.TextChanged += new System.EventHandler(this.TxtDocNo_TextChanged);
            this.txtDocNo.Enter += new System.EventHandler(this.TxtDocNo_Enter);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(18, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 15);
            this.label1.TabIndex = 10;
            this.label1.Text = "Order No";
            this.label1.Click += new System.EventHandler(this.Label1_Click);
            // 
            // txtOrderType
            // 
            this.txtOrderType.Enabled = false;
            this.txtOrderType.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtOrderType.Location = new System.Drawing.Point(759, 47);
            this.txtOrderType.Name = "txtOrderType";
            this.txtOrderType.Size = new System.Drawing.Size(150, 23);
            this.txtOrderType.TabIndex = 41;
            this.txtOrderType.Visible = false;
            // 
            // txtMerchindiser
            // 
            this.txtMerchindiser.Enabled = false;
            this.txtMerchindiser.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMerchindiser.Location = new System.Drawing.Point(326, 17);
            this.txtMerchindiser.Name = "txtMerchindiser";
            this.txtMerchindiser.Size = new System.Drawing.Size(348, 23);
            this.txtMerchindiser.TabIndex = 22;
            this.txtMerchindiser.Visible = false;
            // 
            // txtCustomer
            // 
            this.txtCustomer.Enabled = false;
            this.txtCustomer.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCustomer.Location = new System.Drawing.Point(326, 17);
            this.txtCustomer.Name = "txtCustomer";
            this.txtCustomer.Size = new System.Drawing.Size(348, 23);
            this.txtCustomer.TabIndex = 18;
            this.txtCustomer.Visible = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(116, 20);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(59, 15);
            this.label5.TabIndex = 23;
            this.label5.Text = "Customer";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(262, 21);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 15);
            this.label2.TabIndex = 13;
            this.label2.Text = "Doc Date";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(238, 21);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(81, 15);
            this.label9.TabIndex = 27;
            this.label9.Text = "Merchindiser";
            this.label9.Visible = false;
            // 
            // tabControlAdv1
            // 
            this.tabControlAdv1.ActiveTabForeColor = System.Drawing.Color.Empty;
            this.tabControlAdv1.BeforeTouchSize = new System.Drawing.Size(1096, 512);
            this.tabControlAdv1.CloseButtonForeColor = System.Drawing.Color.Empty;
            this.tabControlAdv1.CloseButtonHoverForeColor = System.Drawing.Color.Empty;
            this.tabControlAdv1.CloseButtonPressedForeColor = System.Drawing.Color.Empty;
            this.tabControlAdv1.Controls.Add(this.tabPageFabricColour);
            this.tabControlAdv1.Controls.Add(this.tabFabricAllocation);
            this.tabControlAdv1.Controls.Add(this.tabFabric);
            this.tabControlAdv1.Controls.Add(this.tabYarnProcess);
            this.tabControlAdv1.Controls.Add(this.tabYarn);
            this.tabControlAdv1.InActiveTabForeColor = System.Drawing.Color.Empty;
            this.tabControlAdv1.Location = new System.Drawing.Point(11, 46);
            this.tabControlAdv1.Name = "tabControlAdv1";
            this.tabControlAdv1.SeparatorColor = System.Drawing.SystemColors.ControlDark;
            this.tabControlAdv1.ShowSeparator = false;
            this.tabControlAdv1.Size = new System.Drawing.Size(1096, 512);
            this.tabControlAdv1.TabIndex = 34;
            this.tabControlAdv1.SelectedIndexChanged += new System.EventHandler(this.TabControlAdv1_SelectedIndexChanged);
            // 
            // tabPageFabricColour
            // 
            this.tabPageFabricColour.Controls.Add(this.CmbStructureType);
            this.tabPageFabricColour.Controls.Add(this.label42);
            this.tabPageFabricColour.Controls.Add(this.DataGridComponent);
            this.tabPageFabricColour.Controls.Add(this.chckFinalProcess);
            this.tabPageFabricColour.Controls.Add(this.ChckRollForm);
            this.tabPageFabricColour.Controls.Add(this.button1);
            this.tabPageFabricColour.Controls.Add(this.txtComboClr);
            this.tabPageFabricColour.Controls.Add(this.DataGridComboColour);
            this.tabPageFabricColour.Controls.Add(this.label36);
            this.tabPageFabricColour.Controls.Add(this.BtnFabricColour);
            this.tabPageFabricColour.Controls.Add(this.label35);
            this.tabPageFabricColour.Controls.Add(this.txtFabricColourComposition);
            this.tabPageFabricColour.Controls.Add(this.label34);
            this.tabPageFabricColour.Controls.Add(this.CmbComponentColour);
            this.tabPageFabricColour.Controls.Add(this.label33);
            this.tabPageFabricColour.Controls.Add(this.CmbFabricColourType);
            this.tabPageFabricColour.Controls.Add(this.label32);
            this.tabPageFabricColour.Controls.Add(this.label30);
            this.tabPageFabricColour.Controls.Add(this.DatGridColourCompostion);
            this.tabPageFabricColour.Controls.Add(this.DatagridFabricColour);
            this.tabPageFabricColour.Controls.Add(this.CmbFabricCombo);
            this.tabPageFabricColour.Image = null;
            this.tabPageFabricColour.ImageSize = new System.Drawing.Size(16, 16);
            this.tabPageFabricColour.Location = new System.Drawing.Point(1, 30);
            this.tabPageFabricColour.Name = "tabPageFabricColour";
            this.tabPageFabricColour.ShowCloseButton = true;
            this.tabPageFabricColour.Size = new System.Drawing.Size(1093, 480);
            this.tabPageFabricColour.TabIndex = 5;
            this.tabPageFabricColour.Text = "Fabric Colour";
            this.tabPageFabricColour.ThemesEnabled = false;
            this.tabPageFabricColour.Click += new System.EventHandler(this.TabPageFabricColour_Click);
            // 
            // CmbStructureType
            // 
            this.CmbStructureType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CmbStructureType.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CmbStructureType.FormattingEnabled = true;
            this.CmbStructureType.Location = new System.Drawing.Point(71, 16);
            this.CmbStructureType.Name = "CmbStructureType";
            this.CmbStructureType.Size = new System.Drawing.Size(167, 23);
            this.CmbStructureType.TabIndex = 64;
            this.CmbStructureType.SelectedIndexChanged += new System.EventHandler(this.CmbStructureType_SelectedIndexChanged);
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(6, 18);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(65, 18);
            this.label42.TabIndex = 65;
            this.label42.Text = "Structure";
            // 
            // DataGridComponent
            // 
            this.DataGridComponent.AllowUserToAddRows = false;
            this.DataGridComponent.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.DeepSkyBlue;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DataGridComponent.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.DataGridComponent.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridComponent.EnableHeadersVisualStyles = false;
            this.DataGridComponent.Location = new System.Drawing.Point(287, 97);
            this.DataGridComponent.Name = "DataGridComponent";
            this.DataGridComponent.RowHeadersVisible = false;
            this.DataGridComponent.Size = new System.Drawing.Size(613, 162);
            this.DataGridComponent.TabIndex = 62;
            // 
            // chckFinalProcess
            // 
            this.chckFinalProcess.AutoSize = true;
            this.chckFinalProcess.Location = new System.Drawing.Point(738, 26);
            this.chckFinalProcess.Name = "chckFinalProcess";
            this.chckFinalProcess.Size = new System.Drawing.Size(183, 22);
            this.chckFinalProcess.TabIndex = 42;
            this.chckFinalProcess.Text = "Fabric Color Final Process";
            this.chckFinalProcess.UseVisualStyleBackColor = true;
            this.chckFinalProcess.Visible = false;
            // 
            // ChckRollForm
            // 
            this.ChckRollForm.AutoSize = true;
            this.ChckRollForm.Location = new System.Drawing.Point(939, 11);
            this.ChckRollForm.Name = "ChckRollForm";
            this.ChckRollForm.Size = new System.Drawing.Size(137, 22);
            this.ChckRollForm.TabIndex = 63;
            this.ChckRollForm.Text = "Roll Form Printing";
            this.ChckRollForm.UseVisualStyleBackColor = true;
            this.ChckRollForm.CheckedChanged += new System.EventHandler(this.ChckRollForm_CheckedChanged);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(929, 69);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(48, 26);
            this.button1.TabIndex = 6;
            this.button1.Text = "Ok";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Visible = false;
            this.button1.Click += new System.EventHandler(this.Button1_Click);
            // 
            // txtComboClr
            // 
            this.txtComboClr.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtComboClr.Location = new System.Drawing.Point(677, 71);
            this.txtComboClr.Name = "txtComboClr";
            this.txtComboClr.Size = new System.Drawing.Size(166, 23);
            this.txtComboClr.TabIndex = 4;
            this.txtComboClr.Visible = false;
            // 
            // DataGridComboColour
            // 
            this.DataGridComboColour.AllowUserToAddRows = false;
            this.DataGridComboColour.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.DeepSkyBlue;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DataGridComboColour.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.DataGridComboColour.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridComboColour.EnableHeadersVisualStyles = false;
            this.DataGridComboColour.Location = new System.Drawing.Point(14, 98);
            this.DataGridComboColour.Name = "DataGridComboColour";
            this.DataGridComboColour.RowHeadersVisible = false;
            this.DataGridComboColour.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridComboColour.Size = new System.Drawing.Size(267, 161);
            this.DataGridComboColour.TabIndex = 61;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.Location = new System.Drawing.Point(289, 75);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(81, 18);
            this.label36.TabIndex = 60;
            this.label36.Text = "Component";
            // 
            // BtnFabricColour
            // 
            this.BtnFabricColour.Location = new System.Drawing.Point(1025, 233);
            this.BtnFabricColour.Name = "BtnFabricColour";
            this.BtnFabricColour.Size = new System.Drawing.Size(65, 26);
            this.BtnFabricColour.TabIndex = 3;
            this.BtnFabricColour.Text = "Add";
            this.BtnFabricColour.UseVisualStyleBackColor = true;
            this.BtnFabricColour.Click += new System.EventHandler(this.BtnFabricColour_Click);
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.Location = new System.Drawing.Point(842, 52);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(86, 18);
            this.label35.TabIndex = 57;
            this.label35.Text = "Composition";
            this.label35.Visible = false;
            // 
            // txtFabricColourComposition
            // 
            this.txtFabricColourComposition.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFabricColourComposition.Location = new System.Drawing.Point(843, 71);
            this.txtFabricColourComposition.Name = "txtFabricColourComposition";
            this.txtFabricColourComposition.Size = new System.Drawing.Size(85, 23);
            this.txtFabricColourComposition.TabIndex = 5;
            this.txtFabricColourComposition.Visible = false;
            this.txtFabricColourComposition.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TxtFabricColourComposition_KeyDown);
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.Location = new System.Drawing.Point(677, 52);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(49, 18);
            this.label34.TabIndex = 55;
            this.label34.Text = "Colour";
            this.label34.Visible = false;
            // 
            // CmbComponentColour
            // 
            this.CmbComponentColour.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CmbComponentColour.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CmbComponentColour.FormattingEnabled = true;
            this.CmbComponentColour.Location = new System.Drawing.Point(880, 33);
            this.CmbComponentColour.Name = "CmbComponentColour";
            this.CmbComponentColour.Size = new System.Drawing.Size(203, 23);
            this.CmbComponentColour.TabIndex = 2;
            this.CmbComponentColour.Visible = false;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(16, 75);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(52, 18);
            this.label33.TabIndex = 52;
            this.label33.Text = "Combo";
            // 
            // CmbFabricColourType
            // 
            this.CmbFabricColourType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CmbFabricColourType.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CmbFabricColourType.FormattingEnabled = true;
            this.CmbFabricColourType.Items.AddRange(new object[] {
            "SOLID",
            "YARN DYED",
            "MELANGE"});
            this.CmbFabricColourType.Location = new System.Drawing.Point(287, 16);
            this.CmbFabricColourType.Name = "CmbFabricColourType";
            this.CmbFabricColourType.Size = new System.Drawing.Size(116, 23);
            this.CmbFabricColourType.TabIndex = 1;
            this.CmbFabricColourType.SelectedIndexChanged += new System.EventHandler(this.CmbFabricColourType_SelectedIndexChanged);
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(244, 18);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(37, 18);
            this.label32.TabIndex = 50;
            this.label32.Text = "Type";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(11, 47);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(45, 18);
            this.label30.TabIndex = 46;
            this.label30.Text = "Fabric";
            // 
            // DatGridColourCompostion
            // 
            this.DatGridColourCompostion.AllowUserToAddRows = false;
            this.DatGridColourCompostion.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.DeepSkyBlue;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DatGridColourCompostion.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.DatGridColourCompostion.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DatGridColourCompostion.EnableHeadersVisualStyles = false;
            this.DatGridColourCompostion.Location = new System.Drawing.Point(677, 96);
            this.DatGridColourCompostion.Name = "DatGridColourCompostion";
            this.DatGridColourCompostion.RowHeadersVisible = false;
            this.DatGridColourCompostion.Size = new System.Drawing.Size(347, 162);
            this.DatGridColourCompostion.TabIndex = 45;
            // 
            // DatagridFabricColour
            // 
            this.DatagridFabricColour.AllowUserToAddRows = false;
            this.DatagridFabricColour.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.DeepSkyBlue;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DatagridFabricColour.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.DatagridFabricColour.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DatagridFabricColour.EnableHeadersVisualStyles = false;
            this.DatagridFabricColour.Location = new System.Drawing.Point(14, 264);
            this.DatagridFabricColour.Name = "DatagridFabricColour";
            this.DatagridFabricColour.RowHeadersVisible = false;
            this.DatagridFabricColour.Size = new System.Drawing.Size(1076, 206);
            this.DatagridFabricColour.TabIndex = 44;
            this.DatagridFabricColour.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DatagridFabricColour_KeyDown);
            // 
            // CmbFabricCombo
            // 
            this.CmbFabricCombo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CmbFabricCombo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CmbFabricCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CmbFabricCombo.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CmbFabricCombo.FormattingEnabled = true;
            this.CmbFabricCombo.Location = new System.Drawing.Point(58, 46);
            this.CmbFabricCombo.Name = "CmbFabricCombo";
            this.CmbFabricCombo.Size = new System.Drawing.Size(573, 23);
            this.CmbFabricCombo.TabIndex = 0;
            // 
            // tabFabricAllocation
            // 
            this.tabFabricAllocation.Controls.Add(this.CmbFabAll);
            this.tabFabricAllocation.Controls.Add(this.label37);
            this.tabFabricAllocation.Controls.Add(this.txtDiaApply);
            this.tabFabricAllocation.Controls.Add(this.ChckApplyAll);
            this.tabFabricAllocation.Controls.Add(this.ChckDetailFabricClr);
            this.tabFabricAllocation.Controls.Add(this.label31);
            this.tabFabricAllocation.Controls.Add(this.DataGridSize);
            this.tabFabricAllocation.Controls.Add(this.cmbType);
            this.tabFabricAllocation.Controls.Add(this.label18);
            this.tabFabricAllocation.Controls.Add(this.label17);
            this.tabFabricAllocation.Controls.Add(this.label16);
            this.tabFabricAllocation.Controls.Add(this.label15);
            this.tabFabricAllocation.Controls.Add(this.label14);
            this.tabFabricAllocation.Controls.Add(this.label13);
            this.tabFabricAllocation.Controls.Add(this.btnOk);
            this.tabFabricAllocation.Controls.Add(this.txttAvgWt);
            this.tabFabricAllocation.Controls.Add(this.txtToterence);
            this.tabFabricAllocation.Controls.Add(this.txtGSM);
            this.tabFabricAllocation.Controls.Add(this.ChckSelectAll);
            this.tabFabricAllocation.Controls.Add(this.txtFabric);
            this.tabFabricAllocation.Controls.Add(this.txtComponent);
            this.tabFabricAllocation.Controls.Add(this.DataGridFabricColourSummary);
            this.tabFabricAllocation.Controls.Add(this.DataGridFabric);
            this.tabFabricAllocation.Image = null;
            this.tabFabricAllocation.ImageSize = new System.Drawing.Size(16, 16);
            this.tabFabricAllocation.Location = new System.Drawing.Point(1, 30);
            this.tabFabricAllocation.Name = "tabFabricAllocation";
            this.tabFabricAllocation.ShowCloseButton = true;
            this.tabFabricAllocation.Size = new System.Drawing.Size(1093, 480);
            this.tabFabricAllocation.TabIndex = 1;
            this.tabFabricAllocation.Text = "Fabric Allocation";
            this.tabFabricAllocation.ThemesEnabled = false;
            // 
            // CmbFabAll
            // 
            this.CmbFabAll.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CmbFabAll.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CmbFabAll.FormattingEnabled = true;
            this.CmbFabAll.Location = new System.Drawing.Point(11, 25);
            this.CmbFabAll.Name = "CmbFabAll";
            this.CmbFabAll.Size = new System.Drawing.Size(730, 23);
            this.CmbFabAll.TabIndex = 69;
            this.CmbFabAll.SelectedIndexChanged += new System.EventHandler(this.CmbFabAll_SelectedIndexChanged);
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.Location = new System.Drawing.Point(381, 50);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(26, 15);
            this.label37.TabIndex = 68;
            this.label37.Text = "Dia";
            // 
            // txtDiaApply
            // 
            this.txtDiaApply.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDiaApply.Location = new System.Drawing.Point(381, 67);
            this.txtDiaApply.Name = "txtDiaApply";
            this.txtDiaApply.Size = new System.Drawing.Size(73, 23);
            this.txtDiaApply.TabIndex = 67;
            // 
            // ChckApplyAll
            // 
            this.ChckApplyAll.AutoSize = true;
            this.ChckApplyAll.Location = new System.Drawing.Point(458, 67);
            this.ChckApplyAll.Name = "ChckApplyAll";
            this.ChckApplyAll.Size = new System.Drawing.Size(83, 22);
            this.ChckApplyAll.TabIndex = 66;
            this.ChckApplyAll.Text = "Apply All";
            this.ChckApplyAll.UseVisualStyleBackColor = true;
            this.ChckApplyAll.CheckedChanged += new System.EventHandler(this.ChckApplyAll_CheckedChanged);
            // 
            // ChckDetailFabricClr
            // 
            this.ChckDetailFabricClr.AutoSize = true;
            this.ChckDetailFabricClr.Location = new System.Drawing.Point(941, 212);
            this.ChckDetailFabricClr.Name = "ChckDetailFabricClr";
            this.ChckDetailFabricClr.Size = new System.Drawing.Size(99, 22);
            this.ChckDetailFabricClr.TabIndex = 51;
            this.ChckDetailFabricClr.Text = "Detail View";
            this.ChckDetailFabricClr.UseVisualStyleBackColor = true;
            this.ChckDetailFabricClr.CheckedChanged += new System.EventHandler(this.ChckDetailFabricClr_CheckedChanged);
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(73, 54);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(28, 15);
            this.label31.TabIndex = 63;
            this.label31.Text = "Size";
            // 
            // DataGridSize
            // 
            this.DataGridSize.AllowUserToAddRows = false;
            this.DataGridSize.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.DeepSkyBlue;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DataGridSize.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.DataGridSize.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridSize.EnableHeadersVisualStyles = false;
            this.DataGridSize.Location = new System.Drawing.Point(107, 50);
            this.DataGridSize.Name = "DataGridSize";
            this.DataGridSize.RowHeadersVisible = false;
            this.DataGridSize.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridSize.Size = new System.Drawing.Size(267, 184);
            this.DataGridSize.TabIndex = 62;
            // 
            // cmbType
            // 
            this.cmbType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbType.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbType.FormattingEnabled = true;
            this.cmbType.Items.AddRange(new object[] {
            "Tubular",
            "Open Ended"});
            this.cmbType.Location = new System.Drawing.Point(523, 209);
            this.cmbType.Name = "cmbType";
            this.cmbType.Size = new System.Drawing.Size(94, 23);
            this.cmbType.TabIndex = 4;
            this.cmbType.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Control_KeyUp);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(618, 192);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(94, 15);
            this.label18.TabIndex = 48;
            this.label18.Text = "Avg Pc Wt (gms)";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(527, 192);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(31, 15);
            this.label17.TabIndex = 47;
            this.label17.Text = "Type";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(457, 189);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(59, 15);
            this.label16.TabIndex = 46;
            this.label16.Text = "Tolerence";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(380, 189);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(32, 15);
            this.label15.TabIndex = 45;
            this.label15.Text = "GSM";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(8, 7);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(212, 15);
            this.label14.TabIndex = 44;
            this.label14.Text = "Component / Combo / Colour / Fabric";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(380, 7);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(69, 15);
            this.label13.TabIndex = 36;
            this.label13.Text = "Component";
            this.label13.Visible = false;
            // 
            // btnOk
            // 
            this.btnOk.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOk.Location = new System.Drawing.Point(675, 208);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(34, 24);
            this.btnOk.TabIndex = 6;
            this.btnOk.Text = "Ok";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.BtnOk_Click);
            // 
            // txttAvgWt
            // 
            this.txttAvgWt.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txttAvgWt.Location = new System.Drawing.Point(619, 209);
            this.txttAvgWt.Name = "txttAvgWt";
            this.txttAvgWt.Size = new System.Drawing.Size(56, 23);
            this.txttAvgWt.TabIndex = 5;
            this.txttAvgWt.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Control_KeyUp);
            // 
            // txtToterence
            // 
            this.txtToterence.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtToterence.Location = new System.Drawing.Point(458, 209);
            this.txtToterence.Name = "txtToterence";
            this.txtToterence.Size = new System.Drawing.Size(64, 23);
            this.txtToterence.TabIndex = 3;
            this.txtToterence.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Control_KeyUp);
            this.txtToterence.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxtGSM_KeyPress);
            // 
            // txtGSM
            // 
            this.txtGSM.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGSM.Location = new System.Drawing.Point(383, 209);
            this.txtGSM.Name = "txtGSM";
            this.txtGSM.Size = new System.Drawing.Size(73, 23);
            this.txtGSM.TabIndex = 2;
            this.txtGSM.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Control_KeyUp);
            this.txtGSM.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxtGSM_KeyPress);
            // 
            // ChckSelectAll
            // 
            this.ChckSelectAll.AutoSize = true;
            this.ChckSelectAll.Location = new System.Drawing.Point(383, 143);
            this.ChckSelectAll.Name = "ChckSelectAll";
            this.ChckSelectAll.Size = new System.Drawing.Size(85, 22);
            this.ChckSelectAll.TabIndex = 65;
            this.ChckSelectAll.Text = "Select All";
            this.ChckSelectAll.UseVisualStyleBackColor = true;
            this.ChckSelectAll.CheckedChanged += new System.EventHandler(this.ChckSelectAll_CheckedChanged);
            // 
            // txtFabric
            // 
            this.txtFabric.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFabric.Location = new System.Drawing.Point(207, 25);
            this.txtFabric.Name = "txtFabric";
            this.txtFabric.Size = new System.Drawing.Size(448, 23);
            this.txtFabric.TabIndex = 1;
            this.txtFabric.Click += new System.EventHandler(this.TxtFabric_Click);
            this.txtFabric.TextChanged += new System.EventHandler(this.TxtFabric_TextChanged);
            this.txtFabric.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Control_KeyUp);
            // 
            // txtComponent
            // 
            this.txtComponent.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtComponent.Location = new System.Drawing.Point(11, 25);
            this.txtComponent.Name = "txtComponent";
            this.txtComponent.Size = new System.Drawing.Size(194, 23);
            this.txtComponent.TabIndex = 0;
            this.txtComponent.Click += new System.EventHandler(this.TxtComponent_Click);
            this.txtComponent.TextChanged += new System.EventHandler(this.TxtComponent_TextChanged);
            this.txtComponent.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Control_KeyUp);
            // 
            // DataGridFabricColourSummary
            // 
            this.DataGridFabricColourSummary.AllowUserToAddRows = false;
            this.DataGridFabricColourSummary.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCellsExceptHeader;
            this.DataGridFabricColourSummary.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.ActiveCaption;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DataGridFabricColourSummary.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.DataGridFabricColourSummary.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridFabricColourSummary.EnableHeadersVisualStyles = false;
            this.DataGridFabricColourSummary.Location = new System.Drawing.Point(5, 238);
            this.DataGridFabricColourSummary.Name = "DataGridFabricColourSummary";
            this.DataGridFabricColourSummary.RowHeadersVisible = false;
            this.DataGridFabricColourSummary.Size = new System.Drawing.Size(1076, 225);
            this.DataGridFabricColourSummary.TabIndex = 64;
            // 
            // DataGridFabric
            // 
            this.DataGridFabric.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.DeepSkyBlue;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DataGridFabric.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.DataGridFabric.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridFabric.EnableHeadersVisualStyles = false;
            this.DataGridFabric.Location = new System.Drawing.Point(5, 238);
            this.DataGridFabric.Name = "DataGridFabric";
            this.DataGridFabric.RowHeadersVisible = false;
            this.DataGridFabric.Size = new System.Drawing.Size(1072, 225);
            this.DataGridFabric.TabIndex = 43;
            this.DataGridFabric.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DataGridFabric_KeyDown);
            // 
            // tabFabric
            // 
            this.tabFabric.Controls.Add(this.CmbFabricProcess);
            this.tabFabric.Controls.Add(this.chckFabProcessFinal);
            this.tabFabric.Controls.Add(this.label29);
            this.tabFabric.Controls.Add(this.DataGridFabricProcess);
            this.tabFabric.Controls.Add(this.btnFabricProcess);
            this.tabFabric.Controls.Add(this.txtFabricProcessLoss);
            this.tabFabric.Controls.Add(this.txtFProcess);
            this.tabFabric.Controls.Add(this.txtFabricSeqNo);
            this.tabFabric.Controls.Add(this.label25);
            this.tabFabric.Controls.Add(this.label26);
            this.tabFabric.Controls.Add(this.label27);
            this.tabFabric.Controls.Add(this.label28);
            this.tabFabric.Controls.Add(this.CmbProcessColour);
            this.tabFabric.Controls.Add(this.txtFabricColour);
            this.tabFabric.Controls.Add(this.txtFabricProcess);
            this.tabFabric.Image = null;
            this.tabFabric.ImageSize = new System.Drawing.Size(16, 16);
            this.tabFabric.Location = new System.Drawing.Point(1, 30);
            this.tabFabric.Name = "tabFabric";
            this.tabFabric.ShowCloseButton = true;
            this.tabFabric.Size = new System.Drawing.Size(1093, 480);
            this.tabFabric.TabIndex = 4;
            this.tabFabric.Text = "Fabric Process";
            this.tabFabric.ThemesEnabled = false;
            // 
            // CmbFabricProcess
            // 
            this.CmbFabricProcess.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CmbFabricProcess.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CmbFabricProcess.FormattingEnabled = true;
            this.CmbFabricProcess.Location = new System.Drawing.Point(64, 9);
            this.CmbFabricProcess.Name = "CmbFabricProcess";
            this.CmbFabricProcess.Size = new System.Drawing.Size(730, 23);
            this.CmbFabricProcess.TabIndex = 70;
            // 
            // chckFabProcessFinal
            // 
            this.chckFabProcessFinal.AutoSize = true;
            this.chckFabProcessFinal.Location = new System.Drawing.Point(639, 58);
            this.chckFabProcessFinal.Name = "chckFabProcessFinal";
            this.chckFabProcessFinal.Size = new System.Drawing.Size(120, 22);
            this.chckFabProcessFinal.TabIndex = 43;
            this.chckFabProcessFinal.Text = "Is Final Process";
            this.chckFabProcessFinal.UseVisualStyleBackColor = true;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(415, 37);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(49, 18);
            this.label29.TabIndex = 22;
            this.label29.Text = "Colour";
            // 
            // DataGridFabricProcess
            // 
            this.DataGridFabricProcess.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.Color.DeepSkyBlue;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DataGridFabricProcess.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle8;
            this.DataGridFabricProcess.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridFabricProcess.EnableHeadersVisualStyles = false;
            this.DataGridFabricProcess.Location = new System.Drawing.Point(10, 87);
            this.DataGridFabricProcess.Name = "DataGridFabricProcess";
            this.DataGridFabricProcess.RowHeadersVisible = false;
            this.DataGridFabricProcess.Size = new System.Drawing.Size(1061, 379);
            this.DataGridFabricProcess.TabIndex = 21;
            this.DataGridFabricProcess.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridFabricProcess_CellValueChanged);
            this.DataGridFabricProcess.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DataGridFabricProcess_KeyDown);
            // 
            // btnFabricProcess
            // 
            this.btnFabricProcess.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFabricProcess.Location = new System.Drawing.Point(764, 57);
            this.btnFabricProcess.Name = "btnFabricProcess";
            this.btnFabricProcess.Size = new System.Drawing.Size(34, 24);
            this.btnFabricProcess.TabIndex = 20;
            this.btnFabricProcess.Text = "Ok";
            this.btnFabricProcess.UseVisualStyleBackColor = true;
            this.btnFabricProcess.Click += new System.EventHandler(this.BtnFabricProcess_Click);
            // 
            // txtFabricProcessLoss
            // 
            this.txtFabricProcessLoss.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFabricProcessLoss.Location = new System.Drawing.Point(551, 58);
            this.txtFabricProcessLoss.Name = "txtFabricProcessLoss";
            this.txtFabricProcessLoss.Size = new System.Drawing.Size(83, 23);
            this.txtFabricProcessLoss.TabIndex = 19;
            // 
            // txtFProcess
            // 
            this.txtFProcess.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFProcess.Location = new System.Drawing.Point(154, 58);
            this.txtFProcess.Name = "txtFProcess";
            this.txtFProcess.Size = new System.Drawing.Size(257, 23);
            this.txtFProcess.TabIndex = 18;
            this.txtFProcess.Click += new System.EventHandler(this.TxtFProcess_Click);
            this.txtFProcess.TextChanged += new System.EventHandler(this.TxtFProcess_TextChanged);
            // 
            // txtFabricSeqNo
            // 
            this.txtFabricSeqNo.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFabricSeqNo.Location = new System.Drawing.Point(10, 58);
            this.txtFabricSeqNo.Name = "txtFabricSeqNo";
            this.txtFabricSeqNo.Size = new System.Drawing.Size(143, 23);
            this.txtFabricSeqNo.TabIndex = 16;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(556, 35);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(98, 18);
            this.label25.TabIndex = 15;
            this.label25.Text = "Process Loss %";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(156, 38);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(55, 18);
            this.label26.TabIndex = 14;
            this.label26.Text = "Process";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(18, 9);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(45, 18);
            this.label27.TabIndex = 13;
            this.label27.Text = "Fabric";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(10, 38);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(52, 18);
            this.label28.TabIndex = 12;
            this.label28.Text = "Seq No";
            // 
            // CmbProcessColour
            // 
            this.CmbProcessColour.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CmbProcessColour.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CmbProcessColour.FormattingEnabled = true;
            this.CmbProcessColour.Location = new System.Drawing.Point(414, 58);
            this.CmbProcessColour.Name = "CmbProcessColour";
            this.CmbProcessColour.Size = new System.Drawing.Size(136, 23);
            this.CmbProcessColour.TabIndex = 51;
            // 
            // txtFabricColour
            // 
            this.txtFabricColour.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFabricColour.Location = new System.Drawing.Point(414, 58);
            this.txtFabricColour.Name = "txtFabricColour";
            this.txtFabricColour.Size = new System.Drawing.Size(135, 23);
            this.txtFabricColour.TabIndex = 23;
            // 
            // txtFabricProcess
            // 
            this.txtFabricProcess.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFabricProcess.Location = new System.Drawing.Point(64, 9);
            this.txtFabricProcess.Name = "txtFabricProcess";
            this.txtFabricProcess.Size = new System.Drawing.Size(587, 23);
            this.txtFabricProcess.TabIndex = 17;
            this.txtFabricProcess.Click += new System.EventHandler(this.TxtFabricProcess_Click);
            // 
            // tabYarnProcess
            // 
            this.tabYarnProcess.Controls.Add(this.label41);
            this.tabYarnProcess.Controls.Add(this.CmbFabBOMYarn);
            this.tabYarnProcess.Controls.Add(this.CmbYarnProcessCombo);
            this.tabYarnProcess.Controls.Add(this.label40);
            this.tabYarnProcess.Controls.Add(this.BtnYarnColourCompOk);
            this.tabYarnProcess.Controls.Add(this.txtYarnComposition);
            this.tabYarnProcess.Controls.Add(this.txtyarnColour);
            this.tabYarnProcess.Controls.Add(this.label39);
            this.tabYarnProcess.Controls.Add(this.DataGridYarnColour);
            this.tabYarnProcess.Controls.Add(this.label38);
            this.tabYarnProcess.Controls.Add(this.label24);
            this.tabYarnProcess.Controls.Add(this.CmbStage);
            this.tabYarnProcess.Controls.Add(this.chckFinalYarn);
            this.tabYarnProcess.Controls.Add(this.CmboYarnProcessColor);
            this.tabYarnProcess.Controls.Add(this.DataGridYarnProcess);
            this.tabYarnProcess.Controls.Add(this.BtnYarnProcess);
            this.tabYarnProcess.Controls.Add(this.txtProcessLoss);
            this.tabYarnProcess.Controls.Add(this.txtProcess);
            this.tabYarnProcess.Controls.Add(this.txtSeqNo);
            this.tabYarnProcess.Controls.Add(this.label23);
            this.tabYarnProcess.Controls.Add(this.label22);
            this.tabYarnProcess.Controls.Add(this.label21);
            this.tabYarnProcess.Controls.Add(this.label20);
            this.tabYarnProcess.Controls.Add(this.CmbYarn);
            this.tabYarnProcess.Controls.Add(this.txtYarn);
            this.tabYarnProcess.Controls.Add(this.DataGridYarnProcessCombo);
            this.tabYarnProcess.Image = null;
            this.tabYarnProcess.ImageSize = new System.Drawing.Size(16, 16);
            this.tabYarnProcess.Location = new System.Drawing.Point(1, 30);
            this.tabYarnProcess.Name = "tabYarnProcess";
            this.tabYarnProcess.ShowCloseButton = true;
            this.tabYarnProcess.Size = new System.Drawing.Size(1093, 480);
            this.tabYarnProcess.TabIndex = 3;
            this.tabYarnProcess.Text = "Yarn Process";
            this.tabYarnProcess.ThemesEnabled = false;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(840, 4);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(86, 18);
            this.label41.TabIndex = 66;
            this.label41.Text = "Composition";
            // 
            // CmbFabBOMYarn
            // 
            this.CmbFabBOMYarn.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CmbFabBOMYarn.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CmbFabBOMYarn.FormattingEnabled = true;
            this.CmbFabBOMYarn.Location = new System.Drawing.Point(316, 22);
            this.CmbFabBOMYarn.Name = "CmbFabBOMYarn";
            this.CmbFabBOMYarn.Size = new System.Drawing.Size(384, 23);
            this.CmbFabBOMYarn.TabIndex = 47;
            // 
            // CmbYarnProcessCombo
            // 
            this.CmbYarnProcessCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CmbYarnProcessCombo.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CmbYarnProcessCombo.FormattingEnabled = true;
            this.CmbYarnProcessCombo.Location = new System.Drawing.Point(12, 22);
            this.CmbYarnProcessCombo.Name = "CmbYarnProcessCombo";
            this.CmbYarnProcessCombo.Size = new System.Drawing.Size(303, 23);
            this.CmbYarnProcessCombo.TabIndex = 65;
            this.CmbYarnProcessCombo.SelectedIndexChanged += new System.EventHandler(this.CmbYarnProcessCombo_SelectedIndexChanged);
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(12, 4);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(52, 18);
            this.label40.TabIndex = 62;
            this.label40.Text = "Combo";
            // 
            // BtnYarnColourCompOk
            // 
            this.BtnYarnColourCompOk.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnYarnColourCompOk.Location = new System.Drawing.Point(901, 22);
            this.BtnYarnColourCompOk.Name = "BtnYarnColourCompOk";
            this.BtnYarnColourCompOk.Size = new System.Drawing.Size(34, 23);
            this.BtnYarnColourCompOk.TabIndex = 52;
            this.BtnYarnColourCompOk.Text = "Ok";
            this.BtnYarnColourCompOk.UseVisualStyleBackColor = true;
            this.BtnYarnColourCompOk.Click += new System.EventHandler(this.BtnYarnColourCompOk_Click);
            // 
            // txtYarnComposition
            // 
            this.txtYarnComposition.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtYarnComposition.Location = new System.Drawing.Point(842, 22);
            this.txtYarnComposition.Name = "txtYarnComposition";
            this.txtYarnComposition.Size = new System.Drawing.Size(59, 23);
            this.txtYarnComposition.TabIndex = 51;
            // 
            // txtyarnColour
            // 
            this.txtyarnColour.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtyarnColour.Location = new System.Drawing.Point(702, 22);
            this.txtyarnColour.Name = "txtyarnColour";
            this.txtyarnColour.Size = new System.Drawing.Size(139, 23);
            this.txtyarnColour.TabIndex = 50;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(702, 4);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(49, 18);
            this.label39.TabIndex = 49;
            this.label39.Text = "Colour";
            // 
            // DataGridYarnColour
            // 
            this.DataGridYarnColour.AllowUserToAddRows = false;
            this.DataGridYarnColour.BackgroundColor = System.Drawing.Color.White;
            this.DataGridYarnColour.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridYarnColour.Location = new System.Drawing.Point(12, 47);
            this.DataGridYarnColour.Name = "DataGridYarnColour";
            this.DataGridYarnColour.RowHeadersVisible = false;
            this.DataGridYarnColour.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridYarnColour.Size = new System.Drawing.Size(919, 125);
            this.DataGridYarnColour.TabIndex = 48;
            this.DataGridYarnColour.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DataGridYarnColour_KeyDown);
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(314, 4);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(34, 18);
            this.label38.TabIndex = 46;
            this.label38.Text = "Yarn";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(934, 182);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(98, 18);
            this.label24.TabIndex = 4;
            this.label24.Text = "Process Loss %";
            // 
            // CmbStage
            // 
            this.CmbStage.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CmbStage.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CmbStage.FormattingEnabled = true;
            this.CmbStage.Items.AddRange(new object[] {
            "Purchase",
            "Process"});
            this.CmbStage.Location = new System.Drawing.Point(477, 203);
            this.CmbStage.Name = "CmbStage";
            this.CmbStage.Size = new System.Drawing.Size(101, 23);
            this.CmbStage.TabIndex = 44;
            // 
            // chckFinalYarn
            // 
            this.chckFinalYarn.AutoSize = true;
            this.chckFinalYarn.Location = new System.Drawing.Point(943, 157);
            this.chckFinalYarn.Name = "chckFinalYarn";
            this.chckFinalYarn.Size = new System.Drawing.Size(99, 22);
            this.chckFinalYarn.TabIndex = 43;
            this.chckFinalYarn.Text = "Is Final Yarn";
            this.chckFinalYarn.UseVisualStyleBackColor = true;
            this.chckFinalYarn.Visible = false;
            // 
            // CmboYarnProcessColor
            // 
            this.CmboYarnProcessColor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CmboYarnProcessColor.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CmboYarnProcessColor.FormattingEnabled = true;
            this.CmboYarnProcessColor.Location = new System.Drawing.Point(794, 202);
            this.CmboYarnProcessColor.Name = "CmboYarnProcessColor";
            this.CmboYarnProcessColor.Size = new System.Drawing.Size(137, 23);
            this.CmboYarnProcessColor.TabIndex = 12;
            // 
            // DataGridYarnProcess
            // 
            this.DataGridYarnProcess.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.Color.DeepSkyBlue;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DataGridYarnProcess.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle9;
            this.DataGridYarnProcess.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridYarnProcess.EnableHeadersVisualStyles = false;
            this.DataGridYarnProcess.Location = new System.Drawing.Point(13, 227);
            this.DataGridYarnProcess.Name = "DataGridYarnProcess";
            this.DataGridYarnProcess.RowHeadersVisible = false;
            this.DataGridYarnProcess.Size = new System.Drawing.Size(1064, 248);
            this.DataGridYarnProcess.TabIndex = 11;
            this.DataGridYarnProcess.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DataGridYarnProcess_KeyDown);
            // 
            // BtnYarnProcess
            // 
            this.BtnYarnProcess.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnYarnProcess.Location = new System.Drawing.Point(1042, 203);
            this.BtnYarnProcess.Name = "BtnYarnProcess";
            this.BtnYarnProcess.Size = new System.Drawing.Size(34, 23);
            this.BtnYarnProcess.TabIndex = 10;
            this.BtnYarnProcess.Text = "Ok";
            this.BtnYarnProcess.UseVisualStyleBackColor = true;
            this.BtnYarnProcess.Click += new System.EventHandler(this.BtnYarnProcess_Click);
            // 
            // txtProcessLoss
            // 
            this.txtProcessLoss.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtProcessLoss.Location = new System.Drawing.Point(934, 203);
            this.txtProcessLoss.Name = "txtProcessLoss";
            this.txtProcessLoss.Size = new System.Drawing.Size(108, 23);
            this.txtProcessLoss.TabIndex = 9;
            // 
            // txtProcess
            // 
            this.txtProcess.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtProcess.Location = new System.Drawing.Point(580, 203);
            this.txtProcess.Name = "txtProcess";
            this.txtProcess.Size = new System.Drawing.Size(213, 23);
            this.txtProcess.TabIndex = 7;
            this.txtProcess.Click += new System.EventHandler(this.TxtProcess_Click);
            this.txtProcess.TextChanged += new System.EventHandler(this.TxtProcess_TextChanged);
            // 
            // txtSeqNo
            // 
            this.txtSeqNo.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSeqNo.Location = new System.Drawing.Point(13, 203);
            this.txtSeqNo.Name = "txtSeqNo";
            this.txtSeqNo.Size = new System.Drawing.Size(75, 23);
            this.txtSeqNo.TabIndex = 5;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(793, 181);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(49, 18);
            this.label23.TabIndex = 3;
            this.label23.Text = "Colour";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(577, 181);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(55, 18);
            this.label22.TabIndex = 2;
            this.label22.Text = "Process";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(90, 183);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(34, 18);
            this.label21.TabIndex = 1;
            this.label21.Text = "Yarn";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(13, 183);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(52, 18);
            this.label20.TabIndex = 0;
            this.label20.Text = "Seq No";
            // 
            // CmbYarn
            // 
            this.CmbYarn.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CmbYarn.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CmbYarn.FormattingEnabled = true;
            this.CmbYarn.Location = new System.Drawing.Point(89, 203);
            this.CmbYarn.Name = "CmbYarn";
            this.CmbYarn.Size = new System.Drawing.Size(386, 23);
            this.CmbYarn.TabIndex = 45;
            this.CmbYarn.SelectedIndexChanged += new System.EventHandler(this.CmbYarn_SelectedIndexChanged);
            // 
            // txtYarn
            // 
            this.txtYarn.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtYarn.Location = new System.Drawing.Point(89, 203);
            this.txtYarn.Name = "txtYarn";
            this.txtYarn.Size = new System.Drawing.Size(386, 23);
            this.txtYarn.TabIndex = 6;
            this.txtYarn.Click += new System.EventHandler(this.TxtYarn_Click);
            this.txtYarn.TextChanged += new System.EventHandler(this.TxtYarn_TextChanged);
            // 
            // DataGridYarnProcessCombo
            // 
            this.DataGridYarnProcessCombo.AllowUserToAddRows = false;
            this.DataGridYarnProcessCombo.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle10.BackColor = System.Drawing.Color.DeepSkyBlue;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DataGridYarnProcessCombo.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle10;
            this.DataGridYarnProcessCombo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridYarnProcessCombo.EnableHeadersVisualStyles = false;
            this.DataGridYarnProcessCombo.Location = new System.Drawing.Point(64, 6);
            this.DataGridYarnProcessCombo.Name = "DataGridYarnProcessCombo";
            this.DataGridYarnProcessCombo.RowHeadersVisible = false;
            this.DataGridYarnProcessCombo.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridYarnProcessCombo.Size = new System.Drawing.Size(267, 154);
            this.DataGridYarnProcessCombo.TabIndex = 63;
            this.DataGridYarnProcessCombo.Visible = false;
            // 
            // tabYarn
            // 
            this.tabYarn.Controls.Add(this.BtnYarnOk);
            this.tabYarn.Controls.Add(this.txtMixing);
            this.tabYarn.Controls.Add(this.txtClr);
            this.tabYarn.Controls.Add(this.txtYarnAllec);
            this.tabYarn.Controls.Add(this.DataGridYarnComponent);
            this.tabYarn.Controls.Add(this.CmbYarnComp);
            this.tabYarn.Controls.Add(this.label19);
            this.tabYarn.Image = null;
            this.tabYarn.ImageSize = new System.Drawing.Size(16, 16);
            this.tabYarn.Location = new System.Drawing.Point(1, 30);
            this.tabYarn.Name = "tabYarn";
            this.tabYarn.ShowCloseButton = true;
            this.tabYarn.Size = new System.Drawing.Size(1093, 480);
            this.tabYarn.TabIndex = 2;
            this.tabYarn.TabVisible = false;
            this.tabYarn.Text = "Yarn";
            this.tabYarn.ThemesEnabled = false;
            // 
            // BtnYarnOk
            // 
            this.BtnYarnOk.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnYarnOk.Location = new System.Drawing.Point(598, 48);
            this.BtnYarnOk.Name = "BtnYarnOk";
            this.BtnYarnOk.Size = new System.Drawing.Size(30, 23);
            this.BtnYarnOk.TabIndex = 43;
            this.BtnYarnOk.Text = "Ok";
            this.BtnYarnOk.UseVisualStyleBackColor = true;
            this.BtnYarnOk.Click += new System.EventHandler(this.BtnYarnOk_Click);
            // 
            // txtMixing
            // 
            this.txtMixing.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMixing.Location = new System.Drawing.Point(470, 48);
            this.txtMixing.Name = "txtMixing";
            this.txtMixing.Size = new System.Drawing.Size(128, 23);
            this.txtMixing.TabIndex = 42;
            // 
            // txtClr
            // 
            this.txtClr.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtClr.Location = new System.Drawing.Point(296, 48);
            this.txtClr.Name = "txtClr";
            this.txtClr.Size = new System.Drawing.Size(173, 23);
            this.txtClr.TabIndex = 40;
            // 
            // txtYarnAllec
            // 
            this.txtYarnAllec.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtYarnAllec.Location = new System.Drawing.Point(13, 48);
            this.txtYarnAllec.Name = "txtYarnAllec";
            this.txtYarnAllec.Size = new System.Drawing.Size(282, 23);
            this.txtYarnAllec.TabIndex = 39;
            this.txtYarnAllec.Click += new System.EventHandler(this.TxtYarnAllec_Click);
            this.txtYarnAllec.TextChanged += new System.EventHandler(this.TxtYarnAllec_TextChanged);
            // 
            // DataGridYarnComponent
            // 
            this.DataGridYarnComponent.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle11.BackColor = System.Drawing.Color.DeepSkyBlue;
            dataGridViewCellStyle11.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle11.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DataGridYarnComponent.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle11;
            this.DataGridYarnComponent.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridYarnComponent.EnableHeadersVisualStyles = false;
            this.DataGridYarnComponent.Location = new System.Drawing.Point(13, 73);
            this.DataGridYarnComponent.Name = "DataGridYarnComponent";
            this.DataGridYarnComponent.RowHeadersVisible = false;
            this.DataGridYarnComponent.Size = new System.Drawing.Size(615, 333);
            this.DataGridYarnComponent.TabIndex = 38;
            this.DataGridYarnComponent.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridYarnComponent_CellEndEdit);
            // 
            // CmbYarnComp
            // 
            this.CmbYarnComp.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CmbYarnComp.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CmbYarnComp.FormattingEnabled = true;
            this.CmbYarnComp.Location = new System.Drawing.Point(85, 11);
            this.CmbYarnComp.Name = "CmbYarnComp";
            this.CmbYarnComp.Size = new System.Drawing.Size(279, 23);
            this.CmbYarnComp.TabIndex = 37;
            this.CmbYarnComp.SelectedIndexChanged += new System.EventHandler(this.CmbYarnComp_SelectedIndexChanged);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(10, 16);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(69, 15);
            this.label19.TabIndex = 36;
            this.label19.Text = "Component";
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(973, 17);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(128, 22);
            this.checkBox1.TabIndex = 50;
            this.checkBox1.Text = "Final Fabric Bom";
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.Visible = false;
            // 
            // grFront
            // 
            this.grFront.Controls.Add(this.BtnPrint);
            this.grFront.Controls.Add(this.SfdDataGridFabricBom);
            this.grFront.Controls.Add(this.SplitAdd);
            this.grFront.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grFront.Location = new System.Drawing.Point(7, -1);
            this.grFront.Name = "grFront";
            this.grFront.Size = new System.Drawing.Size(1113, 599);
            this.grFront.TabIndex = 42;
            this.grFront.TabStop = false;
            this.grFront.Enter += new System.EventHandler(this.grFront_Enter);
            // 
            // BtnPrint
            // 
            this.BtnPrint.Location = new System.Drawing.Point(6, 562);
            this.BtnPrint.Name = "BtnPrint";
            this.BtnPrint.Size = new System.Drawing.Size(87, 31);
            this.BtnPrint.TabIndex = 2;
            this.BtnPrint.Text = "Print";
            this.BtnPrint.UseVisualStyleBackColor = true;
            this.BtnPrint.Click += new System.EventHandler(this.BtnPrint_Click);
            // 
            // SfdDataGridFabricBom
            // 
            this.SfdDataGridFabricBom.AccessibleName = "Table";
            this.SfdDataGridFabricBom.AllowFiltering = true;
            this.SfdDataGridFabricBom.Location = new System.Drawing.Point(6, 17);
            this.SfdDataGridFabricBom.Name = "SfdDataGridFabricBom";
            this.SfdDataGridFabricBom.Size = new System.Drawing.Size(1099, 539);
            this.SfdDataGridFabricBom.TabIndex = 1;
            this.SfdDataGridFabricBom.Text = "sfDataGrid1";
            // 
            // FrmFabricBom
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1126, 601);
            this.Controls.Add(this.grFront);
            this.Controls.Add(this.grBack);
            this.Name = "FrmFabricBom";
            this.Text = "FrmFabricBom";
            this.Load += new System.EventHandler(this.FrmFabricBom_Load);
            this.grBack.ResumeLayout(false);
            this.grBack.PerformLayout();
            this.grSearch.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DataGridCommon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabControlAdv1)).EndInit();
            this.tabControlAdv1.ResumeLayout(false);
            this.tabPageFabricColour.ResumeLayout(false);
            this.tabPageFabricColour.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridComponent)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridComboColour)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DatGridColourCompostion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DatagridFabricColour)).EndInit();
            this.tabFabricAllocation.ResumeLayout(false);
            this.tabFabricAllocation.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridSize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridFabricColourSummary)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridFabric)).EndInit();
            this.tabFabric.ResumeLayout(false);
            this.tabFabric.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridFabricProcess)).EndInit();
            this.tabYarnProcess.ResumeLayout(false);
            this.tabYarnProcess.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridYarnColour)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridYarnProcess)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridYarnProcessCombo)).EndInit();
            this.tabYarn.ResumeLayout(false);
            this.tabYarn.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridYarnComponent)).EndInit();
            this.grFront.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.SfdDataGridFabricBom)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grBack;
        private System.Windows.Forms.DateTimePicker dtpOrderDate;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtOrderNo;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DateTimePicker dtpDocDate;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtDocNo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtMerchindiser;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.DateTimePicker dtpDeliveryDate;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox cmbYear;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox cmbSeason;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtCustomer;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cmbOrderType;
        private System.Windows.Forms.ComboBox cmbSts;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label12;
        private Syncfusion.Windows.Forms.Tools.TabControlAdv tabControlAdv1;
        private Syncfusion.Windows.Forms.Tools.TabPageAdv tabFabricAllocation;
        private Syncfusion.Windows.Forms.Tools.TabPageAdv tabYarn;
        private Syncfusion.Windows.Forms.Tools.SplitButton SplitSave;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.DataGridView DataGridFabric;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.TextBox txttAvgWt;
        private System.Windows.Forms.TextBox txtToterence;
        private System.Windows.Forms.TextBox txtGSM;
        private System.Windows.Forms.TextBox txtFabric;
        private System.Windows.Forms.TextBox txtComponent;
        private System.Windows.Forms.ComboBox CmbYarnComp;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.DataGridView DataGridYarnComponent;
        private System.Windows.Forms.GroupBox grSearch;
        private System.Windows.Forms.Button btnSelect;
        private System.Windows.Forms.DataGridView DataGridCommon;
        private System.Windows.Forms.Button btnHide;
        private System.Windows.Forms.ComboBox CmbStyle;
        private System.Windows.Forms.TextBox txtYear;
        private System.Windows.Forms.TextBox txtSeason;
        private System.Windows.Forms.TextBox txtOrderType;
        private System.Windows.Forms.TextBox txtOrderSts;
        private System.Windows.Forms.ComboBox cmbType;
        private Syncfusion.Windows.Forms.Tools.toolstripitem toolClose;
        private Syncfusion.Windows.Forms.Tools.TabPageAdv tabYarnProcess;
        private System.Windows.Forms.DataGridView DataGridYarnProcess;
        private System.Windows.Forms.Button BtnYarnProcess;
        private System.Windows.Forms.TextBox txtProcessLoss;
        private System.Windows.Forms.TextBox txtProcess;
        private System.Windows.Forms.TextBox txtYarn;
        private System.Windows.Forms.TextBox txtSeqNo;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Button BtnYarnOk;
        private System.Windows.Forms.TextBox txtMixing;
        private System.Windows.Forms.TextBox txtClr;
        private System.Windows.Forms.TextBox txtYarnAllec;
        private Syncfusion.Windows.Forms.Tools.TabPageAdv tabFabric;
        private System.Windows.Forms.DataGridView DataGridFabricProcess;
        private System.Windows.Forms.Button btnFabricProcess;
        private System.Windows.Forms.TextBox txtFabricProcessLoss;
        private System.Windows.Forms.TextBox txtFProcess;
        private System.Windows.Forms.TextBox txtFabricProcess;
        private System.Windows.Forms.TextBox txtFabricSeqNo;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.TextBox txtFabricColour;
        private System.Windows.Forms.Label label29;
        private Syncfusion.Windows.Forms.Tools.TabPageAdv tabPageFabricColour;
        private System.Windows.Forms.DataGridView DatGridColourCompostion;
        private System.Windows.Forms.DataGridView DatagridFabricColour;
        private System.Windows.Forms.ComboBox CmbComponentColour;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.ComboBox CmbFabricColourType;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.ComboBox CmbFabricCombo;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Button BtnFabricColour;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.TextBox txtFabricColourComposition;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.DataGridView DataGridComboColour;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.TextBox txtComboClr;
        private System.Windows.Forms.GroupBox grFront;
        private Syncfusion.Windows.Forms.Tools.SplitButton SplitAdd;
        private Syncfusion.Windows.Forms.Tools.toolstripitem toolEdit;
        private Syncfusion.Windows.Forms.Tools.toolstripitem toolCls;
        private System.Windows.Forms.Button button1;
        private Syncfusion.WinForms.DataGrid.SfDataGrid SfdDataGridFabricBom;
        private System.Windows.Forms.DataGridView DataGridComponent;
        private System.Windows.Forms.CheckBox ChckRollForm;
        private System.Windows.Forms.ComboBox CmboYarnProcessColor;
        private System.Windows.Forms.CheckBox chckFinalProcess;
        private System.Windows.Forms.CheckBox chckFabFinalProcess;
        private System.Windows.Forms.CheckBox chckFabProcessFinal;
        private System.Windows.Forms.CheckBox chckFinalYarn;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.DataGridView DataGridSize;
        private System.Windows.Forms.ComboBox CmbStage;
        private System.Windows.Forms.ComboBox CmbYarn;
        private System.Windows.Forms.DataGridView DataGridFabricColourSummary;
        private System.Windows.Forms.CheckBox ChckDetailFabricClr;
        private System.Windows.Forms.CheckBox ChckSelectAll;
        private System.Windows.Forms.TextBox txtDiaApply;
        private System.Windows.Forms.CheckBox ChckApplyAll;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Button BtnPrint;
        private System.Windows.Forms.ComboBox CmbFabAll;
        private System.Windows.Forms.ComboBox CmbProcessColour;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.ComboBox CmbFabBOMYarn;
        private System.Windows.Forms.TextBox txtYarnComposition;
        private System.Windows.Forms.TextBox txtyarnColour;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.DataGridView DataGridYarnColour;
        private System.Windows.Forms.Button BtnYarnColourCompOk;
        private System.Windows.Forms.ComboBox CmbFabricProcess;
        private System.Windows.Forms.DataGridView DataGridYarnProcessCombo;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.ComboBox CmbYarnProcessCombo;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.ComboBox CmbStructureType;
        private System.Windows.Forms.Label label42;
    }
}