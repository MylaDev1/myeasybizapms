﻿using Syncfusion.Windows.Forms.Grid;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Drawing.Imaging;
using System.Configuration;
using System.Diagnostics;
using excel = Microsoft.Office.Interop.Excel;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;

namespace MyEasyBizAPMS
{
    public partial class FrmRateApp : Form
    {
        public FrmRateApp()
        {
            InitializeComponent();
            this.SfDataGridPreBudjet.Style.HeaderStyle.Font.FontStyle = FontStyle.Bold;
            this.SfDataGridPreBudjet.Style.HeaderStyle.BackColor = Color.DeepSkyBlue;
            this.SfDataGridPreBudjet.Style.HeaderStyle.TextColor = Color.White;
            this.DataGridCommon.Style.HeaderStyle.Font.FontStyle = FontStyle.Bold;
            this.DataGridCommon.Style.HeaderStyle.BackColor = Color.DeepSkyBlue;
            this.DataGridCommon.Style.HeaderStyle.TextColor = Color.White;
        }
        SqlConnection connection = new SqlConnection(GeneralParameters.ConnectionString);
        SQLDBHelper db = new SQLDBHelper();
        SqlCommand qur = new SqlCommand();
        BindingSource bs = new BindingSource();
        private void FrmRateApp_Load(object sender, EventArgs e)
        {
            qur.Connection = connection;
            GrFront.Visible = false;
            grdfront.Visible = true;
            LoadFrontGrid();
        }

        protected void LoadApproveGrid()
        {
            try
            {
                var selectedItem = DataGridCommon.SelectedItems[0];
                var dataRow = (selectedItem as DataRowView).Row;
                decimal pouid = Convert.ToDecimal(dataRow["uid"].ToString());



                DataTable DataTable = new DataTable();
                SqlParameter[] para = {
                    new SqlParameter("@uid",pouid),
                   new SqlParameter("@type","0"),

                };

                DataTable = db.GetData(CommandType.StoredProcedure, "Proc_GetRateApprove", para);
                //DataTable dataTable = db.GetDataWithoutParam(CommandType.StoredProcedure, "Proc_GetPurchaseApprove", connection);
                SfDataGridPreBudjet.DataSource = null;
                SfDataGridPreBudjet.DataSource = DataTable;
                SfDataGridPreBudjet.Columns[0].Visible = false;
                SfDataGridPreBudjet.Columns[1].Width = 80;
                SfDataGridPreBudjet.Columns[2].Width = 100;
                SfDataGridPreBudjet.Columns[3].Width = 220;
                SfDataGridPreBudjet.Columns[4].Width = 70;
                SfDataGridPreBudjet.Columns[5].Width = 80;
                SfDataGridPreBudjet.Columns[6].Width = 120;
                SfDataGridPreBudjet.Columns[7].Width = 80;
                SfDataGridPreBudjet.Columns[8].Width = 80;
                SfDataGridPreBudjet.Columns[9].Width = 80;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected void LoadFrontGrid()
        {
            try
            {
                DataTable dataTable = db.GetDataWithoutParam(CommandType.StoredProcedure, "SP_RateAPPFrontGridLoad", connection);
                DataGridCommon.DataSource = null;
                DataGridCommon.DataSource = dataTable;
                DataGridCommon.Columns[0].Visible = false;
                DataGridCommon.Columns[1].Width = 100;
                DataGridCommon.Columns[2].Width = 100;
                DataGridCommon.Columns[3].Width = 100;
                DataGridCommon.Columns[4].Width = 400;
                DataGridCommon.Columns[5].Width = 100;
                DataGridCommon.Columns[6].Visible = false;
                DataGridCommon.Columns[7].Width = 100;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
        private void color()
        {


            //if(SfDataGridPreBudjet.Columns["diffvalue"]=="")



        }
        private void SfDataGridPreBudjet_CellDoubleClick(object sender, Syncfusion.WinForms.DataGrid.Events.CellClickEventArgs e)
        {
            try
            {
                //if (e.DataColumn.Index == 14)
                //{
                //    var selectedItem = SfDataGridPreBudjet.SelectedItems[0];
                //    var dataRow = (selectedItem as DataRowView).Row;
                //    decimal pouid = Convert.ToDecimal(dataRow["uid"].ToString());

                //    lblOrderNo.Text = dataRow["orderno"].ToString();
                //    lblStyle.Text = dataRow["PoNo"].ToString();
                //    lblSeason.Text = dataRow["Date"].ToString();
                //    lblOrderQty.Text = dataRow["Supplier"].ToString();
                //    lblReqQty.Text = dataRow["Netvalue"].ToString();

                //    CmbStatus.Text = dataRow["Status"].ToString();
                //    label15.Text = dataRow["style"].ToString();
                //    label9.Text = dataRow["itemname"].ToString();
                //    label8.Text = dataRow["BudjetRate"].ToString();

                //    label6.Text = dataRow["porate"].ToString();

                //    label7.Text = dataRow["Qty"].ToString();
                //    label18.Text = dataRow["DiffValue"].ToString();
                //    label20.Text = dataRow["BudjetValue"].ToString();


                //    GrApprove.Visible = true;

                //}
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void SplitSave_DropDowItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            try
            {
                if (e.ClickedItem.Text == "Back")
                {
                    GrApprove.Visible = false;
                    GrFront.Visible = false;
                    grdfront.Visible = true;
                    LoadFrontGrid();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void SplitSave_Click(object sender, EventArgs e)
        {
            try
            {
                //var selectedItem = SfDataGridPreBudjet.SelectedItems[0];
                //var dataRow = (selectedItem as DataRowView).Row;
                //decimal pouid = Convert.ToDecimal(dataRow["uid"].ToString());

                SqlParameter[] sqlParameters = {
                    new SqlParameter("@pouid", txtgrn.Tag),
                    new SqlParameter("@status",status.Text),

                };
                db.ExecuteNonQuery(CommandType.StoredProcedure, "sp_GetRateApproved", sqlParameters, connection);
                GrApprove.Visible = false;
                lblOrderNo.Text = string.Empty;
                lblStyle.Text = string.Empty;
                lblSeason.Text = string.Empty;
                lblOrderQty.Text = string.Empty;
                lblReqQty.Text = string.Empty;

                CmbStatus.Text = string.Empty;
                LoadApproveGrid();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void BtnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BtnPrint_Click(object sender, EventArgs e)
        {
            Genclass.Dtype = 7;


            connection.Close();
            connection.Open();

            var selectedItem = SfDataGridPreBudjet.SelectedItems[0];
            var dataRow = (selectedItem as DataRowView).Row;
            decimal pouid = Convert.ToDecimal(dataRow["uid"].ToString());
            if (dataRow["Approved"].ToString() == "Approved")
            {
                Genclass.address = dataRow["type"].ToString();
                qur.CommandText = "delete from  NotoWords";
                qur.ExecuteNonQuery();
                Genclass.sum1 = Convert.ToDouble(dataRow["Netvalue"].ToString());


                Int64 NumVal = Convert.ToInt64(Genclass.sum1);
                string Nw = Rupees(NumVal);

                qur.CommandText = "Insert into NotoWords values('" + Nw + "',1)";
                qur.ExecuteNonQuery();

                Genclass.Prtid = Convert.ToInt16(dataRow["uid"].ToString());


                Genclass.slno = 1;
                Crviewer crv = new Crviewer();
                crv.Show();
            }
            else
            {

                MessageBox.Show("Purchase Approved Pending");
                return;
            }


            connection.Close();
        }
        public string Rupees(Int64 rup)
        {
            string result = "";
            Int64 res;
            if ((rup / 10000000) > 0)
            {
                res = rup / 10000000;
                rup = rup % 10000000;
                result = result + ' ' + RupeesToWords(res) + " Crore";
            }
            if ((rup / 100000) > 0)
            {
                res = rup / 100000;
                rup = rup % 100000;
                result = result + ' ' + RupeesToWords(res) + " Lakh";
            }
            if ((rup / 1000) > 0)
            {
                res = rup / 1000;
                rup = rup % 1000;
                result = result + ' ' + RupeesToWords(res) + " Thousand";
            }


            if ((rup / 100) > 0)
            {
                res = rup / 100;
                rup = rup % 100;
                if (rup > 0)
                {
                    result = result + ' ' + RupeesToWords(res) + " Hundred and";
                }
                else
                {
                    result = result + ' ' + RupeesToWords(res) + " Hundred";
                }
            }
            if ((rup % 10) >= 0)
            {
                res = rup % 100;
                result = result + " " + RupeesToWords(res);
            }
            result = "Rupees " + result + ' ' + "only";
            return result;
        }

        public string RupeesToWords(Int64 rup)
        {
            string result = "";
            if ((rup >= 1) && (rup <= 10))
            {
                if ((rup % 10) == 1) result = "One";
                if ((rup % 10) == 2) result = "Two";
                if ((rup % 10) == 3) result = "Three";
                if ((rup % 10) == 4) result = "Four";
                if ((rup % 10) == 5) result = "Five";
                if ((rup % 10) == 6) result = "Six";
                if ((rup % 10) == 7) result = "Seven";
                if ((rup % 10) == 8) result = "Eight";
                if ((rup % 10) == 9) result = "Nine";
                if ((rup % 10) == 0) result = "Ten";
            }
            if (rup > 9 && rup < 20)
            {
                if (rup == 11) result = "Eleven";
                if (rup == 12) result = "Twelve";
                if (rup == 13) result = "Thirteen";
                if (rup == 14) result = "Forteen";
                if (rup == 15) result = "Fifteen";
                if (rup == 16) result = "Sixteen";
                if (rup == 17) result = "Seventeen";
                if (rup == 18) result = "Eighteen";
                if (rup == 19) result = "Nineteen";
            }

            if (rup >= 20 && (rup / 10) == 2 && (rup % 10) == 0) result = "Twenty";
            if (rup > 20 && (rup / 10) == 3 && (rup % 10) == 0) result = "Thirty";
            if (rup > 20 && (rup / 10) == 4 && (rup % 10) == 0) result = "Forty";
            if (rup > 20 && (rup / 10) == 5 && (rup % 10) == 0) result = "Fifty";
            if (rup > 20 && (rup / 10) == 6 && (rup % 10) == 0) result = "Sixty";
            if (rup > 20 && (rup / 10) == 7 && (rup % 10) == 0) result = "Seventy";
            if (rup > 20 && (rup / 10) == 8 && (rup % 10) == 0) result = "Eighty";
            if (rup > 20 && (rup / 10) == 9 && (rup % 10) == 0) result = "Ninty";

            if (rup > 20 && (rup / 10) == 2 && (rup % 10) != 0)
            {
                if ((rup % 10) == 1) result = "Twenty One";
                if ((rup % 10) == 2) result = "Twenty Two";
                if ((rup % 10) == 3) result = "Twenty Three";
                if ((rup % 10) == 4) result = "Twenty Four";
                if ((rup % 10) == 5) result = "Twenty Five";
                if ((rup % 10) == 6) result = "Twenty Six";
                if ((rup % 10) == 7) result = "Twenty Seven";
                if ((rup % 10) == 8) result = "Twenty Eight";
                if ((rup % 10) == 9) result = "Twenty Nine";
            }
            if (rup > 20 && (rup / 10) == 3 && (rup % 10) != 0)
            {
                if ((rup % 10) == 1) result = "Thirty One";
                if ((rup % 10) == 2) result = "Thirty Two";
                if ((rup % 10) == 3) result = "Thirty Three";
                if ((rup % 10) == 4) result = "Thirty Four";
                if ((rup % 10) == 5) result = "Thirty Five";
                if ((rup % 10) == 6) result = "Thirty Six";
                if ((rup % 10) == 7) result = "Thirty Seven";
                if ((rup % 10) == 8) result = "Thirty Eight";
                if ((rup % 10) == 9) result = "Thirty Nine";
            }
            if (rup > 20 && (rup / 10) == 4 && (rup % 10) != 0)
            {
                if ((rup % 10) == 1) result = "Forty One";
                if ((rup % 10) == 2) result = "Forty Two";
                if ((rup % 10) == 3) result = "Forty Three";
                if ((rup % 10) == 4) result = "Forty Four";
                if ((rup % 10) == 5) result = "Forty Five";
                if ((rup % 10) == 6) result = "Forty Six";
                if ((rup % 10) == 7) result = "Forty Seven";
                if ((rup % 10) == 8) result = "Forty Eight";
                if ((rup % 10) == 9) result = "Forty Nine";
            }
            if (rup > 20 && (rup / 10) == 5 && (rup % 10) != 0)
            {
                if ((rup % 10) == 1) result = "Fifty One";
                if ((rup % 10) == 2) result = "Fifty Two";
                if ((rup % 10) == 3) result = "Fifty Three";
                if ((rup % 10) == 4) result = "Fifty Four";
                if ((rup % 10) == 5) result = "Fifty Five";
                if ((rup % 10) == 6) result = "Fifty Six";
                if ((rup % 10) == 7) result = "Fifty Seven";
                if ((rup % 10) == 8) result = "Fifty Eight";
                if ((rup % 10) == 9) result = "Fifty Nine";
            }
            if (rup > 20 && (rup / 10) == 6 && (rup % 10) != 0)
            {
                if ((rup % 10) == 1) result = "Sixty One";
                if ((rup % 10) == 2) result = "Sixty Two";
                if ((rup % 10) == 3) result = "Sixty Three";
                if ((rup % 10) == 4) result = "Sixty Four";
                if ((rup % 10) == 5) result = "Sixty Five";
                if ((rup % 10) == 6) result = "Sixty Six";
                if ((rup % 10) == 7) result = "Sixty Seven";
                if ((rup % 10) == 8) result = "Sixty Eight";
                if ((rup % 10) == 9) result = "Sixty Nine";
            }
            if (rup > 20 && (rup / 10) == 7 && (rup % 10) != 0)
            {
                if ((rup % 10) == 1) result = "Seventy One";
                if ((rup % 10) == 2) result = "Seventy Two";
                if ((rup % 10) == 3) result = "Seventy Three";
                if ((rup % 10) == 4) result = "Seventy Four";
                if ((rup % 10) == 5) result = "Seventy Five";
                if ((rup % 10) == 6) result = "Seventy Six";
                if ((rup % 10) == 7) result = "Seventy Seven";
                if ((rup % 10) == 8) result = "Seventy Eight";
                if ((rup % 10) == 9) result = "Seventy Nine";
            }
            if (rup > 20 && (rup / 10) == 8 && (rup % 10) != 0)
            {
                if ((rup % 10) == 1) result = "Eighty One";
                if ((rup % 10) == 2) result = "Eighty Two";
                if ((rup % 10) == 3) result = "Eighty Three";
                if ((rup % 10) == 4) result = "Eighty Four";
                if ((rup % 10) == 5) result = "Eighty Five";
                if ((rup % 10) == 6) result = "Eighty Six";
                if ((rup % 10) == 7) result = "Eighty Seven";
                if ((rup % 10) == 8) result = "Eighty Eight";
                if ((rup % 10) == 9) result = "Eighty Nine";
            }
            if (rup > 20 && (rup / 10) == 9 && (rup % 10) != 0)
            {
                if ((rup % 10) == 1) result = "Ninty One";
                if ((rup % 10) == 2) result = "Ninty Two";
                if ((rup % 10) == 3) result = "Ninty Three";
                if ((rup % 10) == 4) result = "Ninty Four";
                if ((rup % 10) == 5) result = "Ninty Five";
                if ((rup % 10) == 6) result = "Ninty Six";
                if ((rup % 10) == 7) result = "Ninty Seven";
                if ((rup % 10) == 8) result = "Ninty Eight";
                if ((rup % 10) == 9) result = "Ninty Nine";
            }
            return result;
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void SfDataGridPreBudjet_Click(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void label19_Click(object sender, EventArgs e)
        {

        }

        private void DataGridCommon_CellClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void DataGridCommon_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void DataGridCommon_DoubleClick(object sender, EventArgs e)
        {
           
                var selectedItem = DataGridCommon.SelectedItems[0];
                var dataRow = (selectedItem as DataRowView).Row;
                decimal pouid = Convert.ToDecimal(dataRow["uid"].ToString());


                grdfront.Visible = false;
                GrFront.Visible = true;
                txtgrn.Tag = dataRow["uid"].ToString();
                txtgrn.Text = dataRow["RateAppNo"].ToString();
                textBox1.Text = dataRow["RateAppDate"].ToString();
                txtname.Text = dataRow["Supplier"].ToString();
                status.Text = dataRow["Status"].ToString();
                LoadApproveGrid();




            
        }



        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void DataGridCommon_CellDoubleClick(object sender, Syncfusion.WinForms.DataGrid.Events.CellClickEventArgs e)
        {
            try
            {
                //DataGridCommon.SelectionMode = SfDateGridSelectionMode.FullRowSelect;
                if (e.DataColumn.Index == 5|| e.DataColumn.Index ==4 || e.DataColumn.Index == 3 || e.DataColumn.Index == 2 || e.DataColumn.Index == 1)
                {
                    var selectedItem = DataGridCommon.SelectedItems[0];
                    var dataRow = (selectedItem as DataRowView).Row;
                    decimal pouid = Convert.ToDecimal(dataRow["uid"].ToString());
                    
                  
                    grdfront.Visible = false;
                    GrFront.Visible = true;
                    txtgrn.Tag = dataRow["uid"].ToString();
                    txtgrn.Text = dataRow["RateAppNo"].ToString();
                    textBox1.Text = dataRow["RateAppDate"].ToString();
                    txtname.Text = dataRow["Supplier"].ToString();
                    status.Text = dataRow["Status"].ToString();
                    LoadApproveGrid();




                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void SfDataGridPreBudjet_QueryCellStyle(object sender, Syncfusion.WinForms.DataGrid.Events.QueryCellStyleEventArgs e)
        {
            try
            {
                if (e.Column.MappingName == "PORate")
                {

                    if (Genclass.sum == 1)
                    {
                        e.Style.BackColor = Color.LightYellow;

                    }



                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
        private void loadllopp()
        {

            var records = SfDataGridPreBudjet.View.Records;
            foreach (var record in records)
            {
                var dataRowView = record.Data as DataRowView;
                if (dataRowView != null)
                {
                    double dd = Convert.ToDouble(dataRowView.Row["PORate"].ToString());
                    double tt = Convert.ToDouble(dataRowView.Row["BudgetRate"].ToString());
                    //if (selected.GetType() != typeof(DBNull) && (bool)selected)
                    //{
                    //    //To do 
                    //}

                    if (dd > tt)
                    {
                        Genclass.sum = 1;
                    }
                    else
                    {

                        Genclass.sum = 0;
                    }
                }
            }

        }

        private void SfDataGridPreBudjet_DrawCell(object sender, Syncfusion.WinForms.DataGrid.Events.DrawCellEventArgs e)
        {

        }

        private void DataGridCommon_CellClick(object sender, Syncfusion.WinForms.DataGrid.Events.CellClickEventArgs e)
        {
            
        }

        private void DataGridCommon_SortColumnsChanging(object sender, Syncfusion.WinForms.DataGrid.Events.SortColumnsChangingEventArgs e)
        {
            if (e.AddedItems[0].ColumnName == "Supplier")
                e.Cancel = true;
        }

        private void DataGridCommon_Click(object sender, EventArgs e)
        {

        }

        private void DataGridCommon_AutoGeneratingColumn(object sender, Syncfusion.WinForms.DataGrid.Events.AutoGeneratingColumnArgs e)
        {
            //if (e.Column.MappingName == "Supplier" || e.Column.MappingName == "PONo" || e.Column.MappingName == "PODate" || e.Column.MappingName == "Netvalue")
            //{
            //    e.Column.AllowEditing = false;
            //}
            
        }

        private void SfDataGridPreBudjet_AutoGeneratingColumn(object sender, Syncfusion.WinForms.DataGrid.Events.AutoGeneratingColumnArgs e)
        {

            //if (e.Column.MappingName == "OrderNo"|| e.Column.MappingName == "BudgetRate" || e.Column.MappingName == "BudgetValue" || e.Column.MappingName == "Qty" || e.Column.MappingName == "PORate" || e.Column.MappingName == "Netvalue" || e.Column.MappingName == "DiffValue"|| e.Column.MappingName == "Itemname" || e.Column.MappingName == "Style" || e.Column.MappingName == "Uom")
            //{
            //    e.Column.AllowEditing = false;
            //}
           
        }
    }
}
