﻿namespace MyEasyBizAPMS
{
    partial class FrmParty
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Syncfusion.Windows.Forms.Tools.SplitButtonRenderer splitButtonRenderer1 = new Syncfusion.Windows.Forms.Tools.SplitButtonRenderer();
            Syncfusion.Windows.Forms.Tools.SplitButtonRenderer splitButtonRenderer2 = new Syncfusion.Windows.Forms.Tools.SplitButtonRenderer();
            this.SplitAdd = new Syncfusion.Windows.Forms.Tools.SplitButton();
            this.toolEdit = new Syncfusion.Windows.Forms.Tools.toolstripitem();
            this.toolClose = new Syncfusion.Windows.Forms.Tools.toolstripitem();
            this.SplitSave = new Syncfusion.Windows.Forms.Tools.SplitButton();
            this.SaveClose = new Syncfusion.Windows.Forms.Tools.toolstripitem();
            this.Back = new Syncfusion.Windows.Forms.Tools.toolstripitem();
            this.toolSaveClose = new Syncfusion.Windows.Forms.Tools.toolstripitem();
            this.toolCloseSave = new Syncfusion.Windows.Forms.Tools.toolstripitem();
            this.label12 = new System.Windows.Forms.Label();
            this.txtPin = new System.Windows.Forms.TextBox();
            this.lblState = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.txtCity = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtAdd2 = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtAdd1 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtCode = new System.Windows.Forms.TextBox();
            this.txtCName = new System.Windows.Forms.TextBox();
            this.Phone = new System.Windows.Forms.Label();
            this.txtLandLine = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.grFront = new System.Windows.Forms.GroupBox();
            this.BtnDelete = new System.Windows.Forms.Button();
            this.SfDataGridPartyM = new Syncfusion.WinForms.DataGrid.SfDataGrid();
            this.grBack = new System.Windows.Forms.GroupBox();
            this.tabControlParty = new Syncfusion.Windows.Forms.Tools.TabControlAdv();
            this.tabGeneral = new Syncfusion.Windows.Forms.Tools.TabPageAdv();
            this.CmbPaymentTerms = new System.Windows.Forms.ComboBox();
            this.CmbCountry = new System.Windows.Forms.ComboBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.lblCtype = new System.Windows.Forms.Label();
            this.lblCPartyType = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.CmbState = new System.Windows.Forms.ComboBox();
            this.chckActive = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.CmbType = new System.Windows.Forms.ComboBox();
            this.CmbPartyType = new System.Windows.Forms.ComboBox();
            this.lblPType = new System.Windows.Forms.Label();
            this.txtPaymentTerms = new System.Windows.Forms.TextBox();
            this.txtConatctNo = new System.Windows.Forms.TextBox();
            this.lblPartyType = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.txtGSTNo = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.tabPageAdv1 = new Syncfusion.Windows.Forms.Tools.TabPageAdv();
            this.label17 = new System.Windows.Forms.Label();
            this.txtCEmail2 = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.txtCEmail1 = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.txtCNumber2 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtCPerson1 = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.txtCNumber1 = new System.Windows.Forms.TextBox();
            this.txtCPerson2 = new System.Windows.Forms.TextBox();
            this.grFront.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SfDataGridPartyM)).BeginInit();
            this.grBack.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tabControlParty)).BeginInit();
            this.tabControlParty.SuspendLayout();
            this.tabGeneral.SuspendLayout();
            this.tabPageAdv1.SuspendLayout();
            this.SuspendLayout();
            // 
            // SplitAdd
            // 
            this.SplitAdd.BackColor = System.Drawing.SystemColors.Control;
            this.SplitAdd.BeforeTouchSize = new System.Drawing.Size(75, 27);
            this.SplitAdd.ButtonMode = Syncfusion.Windows.Forms.Tools.ButtonMode.Toggle;
            this.SplitAdd.DropDownItems.Add(this.toolEdit);
            this.SplitAdd.DropDownItems.Add(this.toolClose);
            this.SplitAdd.DropDownPosition = Syncfusion.Windows.Forms.Tools.Position.Bottom;
            this.SplitAdd.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SplitAdd.ForeColor = System.Drawing.Color.Black;
            this.SplitAdd.Location = new System.Drawing.Point(836, 511);
            this.SplitAdd.MinimumSize = new System.Drawing.Size(75, 23);
            this.SplitAdd.Name = "SplitAdd";
            splitButtonRenderer1.SplitButton = this.SplitAdd;
            this.SplitAdd.Renderer = splitButtonRenderer1;
            this.SplitAdd.ShowDropDownOnButtonClick = false;
            this.SplitAdd.Size = new System.Drawing.Size(75, 27);
            this.SplitAdd.TabIndex = 6;
            this.SplitAdd.Text = "Add";
            this.SplitAdd.DropDowItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.SplitAdd_DropDowItemClicked);
            this.SplitAdd.Click += new System.EventHandler(this.SplitAdd_Click);
            // 
            // toolEdit
            // 
            this.toolEdit.Name = "toolEdit";
            this.toolEdit.Size = new System.Drawing.Size(23, 23);
            this.toolEdit.Text = "Edit";
            // 
            // toolClose
            // 
            this.toolClose.Name = "toolClose";
            this.toolClose.Size = new System.Drawing.Size(23, 23);
            this.toolClose.Text = "Close";
            // 
            // SplitSave
            // 
            this.SplitSave.BackColor = System.Drawing.SystemColors.Control;
            this.SplitSave.BeforeTouchSize = new System.Drawing.Size(91, 31);
            this.SplitSave.DropDownItems.Add(this.SaveClose);
            this.SplitSave.DropDownItems.Add(this.Back);
            this.SplitSave.DropDownPosition = Syncfusion.Windows.Forms.Tools.Position.Bottom;
            this.SplitSave.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SplitSave.ForeColor = System.Drawing.Color.Black;
            this.SplitSave.Location = new System.Drawing.Point(820, 498);
            this.SplitSave.MinimumSize = new System.Drawing.Size(75, 23);
            this.SplitSave.Name = "SplitSave";
            splitButtonRenderer2.SplitButton = this.SplitSave;
            this.SplitSave.Renderer = splitButtonRenderer2;
            this.SplitSave.ShowDropDownOnButtonClick = false;
            this.SplitSave.Size = new System.Drawing.Size(91, 31);
            this.SplitSave.TabIndex = 1;
            this.SplitSave.Text = "Save & New";
            this.SplitSave.DropDowItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.SplitSave_DropDowItemClicked);
            this.SplitSave.Click += new System.EventHandler(this.SplitButton1_Click);
            // 
            // SaveClose
            // 
            this.SaveClose.Name = "SaveClose";
            this.SaveClose.Size = new System.Drawing.Size(23, 23);
            this.SaveClose.Text = "Save & Close";
            // 
            // Back
            // 
            this.Back.Name = "Back";
            this.Back.Size = new System.Drawing.Size(23, 23);
            this.Back.Text = "Back";
            // 
            // toolSaveClose
            // 
            this.toolSaveClose.Name = "toolSaveClose";
            this.toolSaveClose.Size = new System.Drawing.Size(23, 23);
            this.toolSaveClose.Text = "Save and Close";
            // 
            // toolCloseSave
            // 
            this.toolCloseSave.Name = "toolCloseSave";
            this.toolCloseSave.Size = new System.Drawing.Size(23, 23);
            this.toolCloseSave.Text = "Close";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label12.Location = new System.Drawing.Point(18, 345);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(28, 18);
            this.label12.TabIndex = 175;
            this.label12.Text = "Pin";
            // 
            // txtPin
            // 
            this.txtPin.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPin.Location = new System.Drawing.Point(18, 364);
            this.txtPin.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtPin.Name = "txtPin";
            this.txtPin.Size = new System.Drawing.Size(129, 26);
            this.txtPin.TabIndex = 15;
            // 
            // lblState
            // 
            this.lblState.AutoSize = true;
            this.lblState.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblState.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblState.Location = new System.Drawing.Point(258, 287);
            this.lblState.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblState.Name = "lblState";
            this.lblState.Size = new System.Drawing.Size(40, 18);
            this.lblState.TabIndex = 173;
            this.lblState.Text = "State";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label10.Location = new System.Drawing.Point(18, 236);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(32, 18);
            this.label10.TabIndex = 171;
            this.label10.Text = "City";
            // 
            // txtCity
            // 
            this.txtCity.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCity.Location = new System.Drawing.Point(18, 255);
            this.txtCity.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtCity.Name = "txtCity";
            this.txtCity.Size = new System.Drawing.Size(223, 26);
            this.txtCity.TabIndex = 13;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label8.Location = new System.Drawing.Point(18, 183);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(65, 18);
            this.label8.TabIndex = 169;
            this.label8.Text = "Address2";
            // 
            // txtAdd2
            // 
            this.txtAdd2.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAdd2.Location = new System.Drawing.Point(18, 202);
            this.txtAdd2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtAdd2.Name = "txtAdd2";
            this.txtAdd2.Size = new System.Drawing.Size(459, 26);
            this.txtAdd2.TabIndex = 12;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label7.Location = new System.Drawing.Point(18, 133);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(65, 18);
            this.label7.TabIndex = 167;
            this.label7.Text = "Address1";
            // 
            // txtAdd1
            // 
            this.txtAdd1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAdd1.Location = new System.Drawing.Point(18, 151);
            this.txtAdd1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtAdd1.Name = "txtAdd1";
            this.txtAdd1.Size = new System.Drawing.Size(459, 26);
            this.txtAdd1.TabIndex = 11;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label4.Location = new System.Drawing.Point(18, 78);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(40, 18);
            this.label4.TabIndex = 161;
            this.label4.Text = "Code";
            // 
            // txtCode
            // 
            this.txtCode.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCode.Location = new System.Drawing.Point(18, 101);
            this.txtCode.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtCode.Name = "txtCode";
            this.txtCode.Size = new System.Drawing.Size(223, 26);
            this.txtCode.TabIndex = 2;
            // 
            // txtCName
            // 
            this.txtCName.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCName.Location = new System.Drawing.Point(18, 51);
            this.txtCName.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtCName.Name = "txtCName";
            this.txtCName.Size = new System.Drawing.Size(459, 26);
            this.txtCName.TabIndex = 0;
            // 
            // Phone
            // 
            this.Phone.AutoSize = true;
            this.Phone.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Phone.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Phone.Location = new System.Drawing.Point(246, 78);
            this.Phone.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Phone.Name = "Phone";
            this.Phone.Size = new System.Drawing.Size(66, 18);
            this.Phone.TabIndex = 163;
            this.Phone.Text = "Land Line";
            // 
            // txtLandLine
            // 
            this.txtLandLine.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLandLine.Location = new System.Drawing.Point(246, 101);
            this.txtLandLine.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtLandLine.Name = "txtLandLine";
            this.txtLandLine.Size = new System.Drawing.Size(228, 26);
            this.txtLandLine.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label3.Location = new System.Drawing.Point(18, 33);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(45, 18);
            this.label3.TabIndex = 159;
            this.label3.Text = "Name";
            // 
            // grFront
            // 
            this.grFront.Controls.Add(this.BtnDelete);
            this.grFront.Controls.Add(this.SplitAdd);
            this.grFront.Controls.Add(this.SfDataGridPartyM);
            this.grFront.Location = new System.Drawing.Point(9, 10);
            this.grFront.Name = "grFront";
            this.grFront.Size = new System.Drawing.Size(920, 544);
            this.grFront.TabIndex = 158;
            this.grFront.TabStop = false;
            // 
            // BtnDelete
            // 
            this.BtnDelete.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Location = new System.Drawing.Point(6, 510);
            this.BtnDelete.Name = "BtnDelete";
            this.BtnDelete.Size = new System.Drawing.Size(80, 27);
            this.BtnDelete.TabIndex = 7;
            this.BtnDelete.Text = "Delete";
            this.BtnDelete.UseVisualStyleBackColor = true;
            this.BtnDelete.Click += new System.EventHandler(this.BtnDelete_Click);
            // 
            // SfDataGridPartyM
            // 
            this.SfDataGridPartyM.AccessibleName = "Table";
            this.SfDataGridPartyM.AllowResizingColumns = true;
            this.SfDataGridPartyM.Location = new System.Drawing.Point(6, 16);
            this.SfDataGridPartyM.Name = "SfDataGridPartyM";
            this.SfDataGridPartyM.Size = new System.Drawing.Size(907, 488);
            this.SfDataGridPartyM.TabIndex = 5;
            this.SfDataGridPartyM.Text = "sfDataGrid1";
            // 
            // grBack
            // 
            this.grBack.Controls.Add(this.SplitSave);
            this.grBack.Controls.Add(this.tabControlParty);
            this.grBack.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grBack.Location = new System.Drawing.Point(9, 12);
            this.grBack.Name = "grBack";
            this.grBack.Size = new System.Drawing.Size(920, 542);
            this.grBack.TabIndex = 6;
            this.grBack.TabStop = false;
            // 
            // tabControlParty
            // 
            this.tabControlParty.ActiveTabForeColor = System.Drawing.Color.Empty;
            this.tabControlParty.BeforeTouchSize = new System.Drawing.Size(906, 468);
            this.tabControlParty.CloseButtonForeColor = System.Drawing.Color.Empty;
            this.tabControlParty.CloseButtonHoverForeColor = System.Drawing.Color.Empty;
            this.tabControlParty.CloseButtonPressedForeColor = System.Drawing.Color.Empty;
            this.tabControlParty.Controls.Add(this.tabGeneral);
            this.tabControlParty.Controls.Add(this.tabPageAdv1);
            this.tabControlParty.InActiveTabForeColor = System.Drawing.Color.Empty;
            this.tabControlParty.Location = new System.Drawing.Point(7, 25);
            this.tabControlParty.Name = "tabControlParty";
            this.tabControlParty.SeparatorColor = System.Drawing.SystemColors.ControlDark;
            this.tabControlParty.ShowSeparator = false;
            this.tabControlParty.Size = new System.Drawing.Size(906, 468);
            this.tabControlParty.TabIndex = 0;
            // 
            // tabGeneral
            // 
            this.tabGeneral.Controls.Add(this.CmbPaymentTerms);
            this.tabGeneral.Controls.Add(this.CmbCountry);
            this.tabGeneral.Controls.Add(this.label23);
            this.tabGeneral.Controls.Add(this.label24);
            this.tabGeneral.Controls.Add(this.lblCtype);
            this.tabGeneral.Controls.Add(this.lblCPartyType);
            this.tabGeneral.Controls.Add(this.label15);
            this.tabGeneral.Controls.Add(this.label14);
            this.tabGeneral.Controls.Add(this.label2);
            this.tabGeneral.Controls.Add(this.CmbState);
            this.tabGeneral.Controls.Add(this.chckActive);
            this.tabGeneral.Controls.Add(this.label1);
            this.tabGeneral.Controls.Add(this.label3);
            this.tabGeneral.Controls.Add(this.txtCName);
            this.tabGeneral.Controls.Add(this.CmbType);
            this.tabGeneral.Controls.Add(this.txtCode);
            this.tabGeneral.Controls.Add(this.CmbPartyType);
            this.tabGeneral.Controls.Add(this.label4);
            this.tabGeneral.Controls.Add(this.lblPType);
            this.tabGeneral.Controls.Add(this.txtAdd1);
            this.tabGeneral.Controls.Add(this.txtPaymentTerms);
            this.tabGeneral.Controls.Add(this.label7);
            this.tabGeneral.Controls.Add(this.txtAdd2);
            this.tabGeneral.Controls.Add(this.label8);
            this.tabGeneral.Controls.Add(this.txtCity);
            this.tabGeneral.Controls.Add(this.label10);
            this.tabGeneral.Controls.Add(this.txtPin);
            this.tabGeneral.Controls.Add(this.label12);
            this.tabGeneral.Controls.Add(this.txtConatctNo);
            this.tabGeneral.Controls.Add(this.Phone);
            this.tabGeneral.Controls.Add(this.txtLandLine);
            this.tabGeneral.Controls.Add(this.lblPartyType);
            this.tabGeneral.Controls.Add(this.lblState);
            this.tabGeneral.Controls.Add(this.label21);
            this.tabGeneral.Controls.Add(this.txtGSTNo);
            this.tabGeneral.Controls.Add(this.label11);
            this.tabGeneral.Controls.Add(this.txtEmail);
            this.tabGeneral.Image = null;
            this.tabGeneral.ImageSize = new System.Drawing.Size(16, 16);
            this.tabGeneral.Location = new System.Drawing.Point(1, 30);
            this.tabGeneral.Name = "tabGeneral";
            this.tabGeneral.ShowCloseButton = true;
            this.tabGeneral.Size = new System.Drawing.Size(903, 436);
            this.tabGeneral.TabIndex = 1;
            this.tabGeneral.Text = "General Details";
            this.tabGeneral.ThemesEnabled = false;
            this.tabGeneral.Click += new System.EventHandler(this.tabGeneral_Click);
            // 
            // CmbPaymentTerms
            // 
            this.CmbPaymentTerms.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CmbPaymentTerms.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CmbPaymentTerms.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CmbPaymentTerms.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CmbPaymentTerms.FormattingEnabled = true;
            this.CmbPaymentTerms.Items.AddRange(new object[] {
            "Overseas",
            "Interstate",
            "Intrastate"});
            this.CmbPaymentTerms.Location = new System.Drawing.Point(541, 151);
            this.CmbPaymentTerms.Name = "CmbPaymentTerms";
            this.CmbPaymentTerms.Size = new System.Drawing.Size(227, 26);
            this.CmbPaymentTerms.TabIndex = 225;
            // 
            // CmbCountry
            // 
            this.CmbCountry.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CmbCountry.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CmbCountry.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CmbCountry.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CmbCountry.FormattingEnabled = true;
            this.CmbCountry.Items.AddRange(new object[] {
            "Overseas",
            "Interstate",
            "Intrastate"});
            this.CmbCountry.Location = new System.Drawing.Point(18, 309);
            this.CmbCountry.Name = "CmbCountry";
            this.CmbCountry.Size = new System.Drawing.Size(223, 26);
            this.CmbCountry.TabIndex = 224;
            this.CmbCountry.SelectedIndexChanged += new System.EventHandler(this.CmbCountry_SelectedIndexChanged);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label23.Location = new System.Drawing.Point(18, 287);
            this.label23.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(57, 18);
            this.label23.TabIndex = 223;
            this.label23.Text = "Country";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label24.Location = new System.Drawing.Point(540, 131);
            this.label24.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(103, 18);
            this.label24.TabIndex = 221;
            this.label24.Text = "Payment Terms";
            // 
            // lblCtype
            // 
            this.lblCtype.AutoSize = true;
            this.lblCtype.ForeColor = System.Drawing.Color.Red;
            this.lblCtype.Location = new System.Drawing.Point(773, 266);
            this.lblCtype.Name = "lblCtype";
            this.lblCtype.Size = new System.Drawing.Size(15, 18);
            this.lblCtype.TabIndex = 214;
            this.lblCtype.Text = "*";
            // 
            // lblCPartyType
            // 
            this.lblCPartyType.AutoSize = true;
            this.lblCPartyType.ForeColor = System.Drawing.Color.Red;
            this.lblCPartyType.Location = new System.Drawing.Point(771, 216);
            this.lblCPartyType.Name = "lblCPartyType";
            this.lblCPartyType.Size = new System.Drawing.Size(15, 18);
            this.lblCPartyType.TabIndex = 213;
            this.lblCPartyType.Text = "*";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.ForeColor = System.Drawing.Color.Red;
            this.label15.Location = new System.Drawing.Point(770, 54);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(15, 18);
            this.label15.TabIndex = 212;
            this.label15.Text = "*";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.ForeColor = System.Drawing.Color.Red;
            this.label14.Location = new System.Drawing.Point(242, 313);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(15, 18);
            this.label14.TabIndex = 211;
            this.label14.Text = "*";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(481, 54);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(15, 18);
            this.label2.TabIndex = 210;
            this.label2.Text = "*";
            // 
            // CmbState
            // 
            this.CmbState.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CmbState.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CmbState.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CmbState.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CmbState.FormattingEnabled = true;
            this.CmbState.Items.AddRange(new object[] {
            "Overseas",
            "Interstate",
            "Intrastate"});
            this.CmbState.Location = new System.Drawing.Point(258, 309);
            this.CmbState.Name = "CmbState";
            this.CmbState.Size = new System.Drawing.Size(223, 26);
            this.CmbState.TabIndex = 209;
            // 
            // chckActive
            // 
            this.chckActive.AutoSize = true;
            this.chckActive.Location = new System.Drawing.Point(541, 306);
            this.chckActive.Name = "chckActive";
            this.chckActive.Size = new System.Drawing.Size(65, 22);
            this.chckActive.TabIndex = 208;
            this.chckActive.Text = "Active";
            this.chckActive.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label1.Location = new System.Drawing.Point(541, 78);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(76, 18);
            this.label1.TabIndex = 205;
            this.label1.Text = "Contact No";
            // 
            // CmbType
            // 
            this.CmbType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CmbType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CmbType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CmbType.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CmbType.FormattingEnabled = true;
            this.CmbType.Items.AddRange(new object[] {
            "Customer",
            "Supplier",
            "Customer/Supplier",
            "Service"});
            this.CmbType.Location = new System.Drawing.Point(541, 265);
            this.CmbType.Name = "CmbType";
            this.CmbType.Size = new System.Drawing.Size(225, 26);
            this.CmbType.TabIndex = 19;
            // 
            // CmbPartyType
            // 
            this.CmbPartyType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CmbPartyType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CmbPartyType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CmbPartyType.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CmbPartyType.FormattingEnabled = true;
            this.CmbPartyType.Items.AddRange(new object[] {
            "Overseas",
            "Interstate",
            "Intrastate"});
            this.CmbPartyType.Location = new System.Drawing.Point(541, 208);
            this.CmbPartyType.Name = "CmbPartyType";
            this.CmbPartyType.Size = new System.Drawing.Size(223, 26);
            this.CmbPartyType.TabIndex = 18;
            // 
            // lblPType
            // 
            this.lblPType.AutoSize = true;
            this.lblPType.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPType.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblPType.Location = new System.Drawing.Point(541, 244);
            this.lblPType.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblPType.Name = "lblPType";
            this.lblPType.Size = new System.Drawing.Size(37, 18);
            this.lblPType.TabIndex = 184;
            this.lblPType.Text = "Type";
            // 
            // txtPaymentTerms
            // 
            this.txtPaymentTerms.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPaymentTerms.Location = new System.Drawing.Point(541, 151);
            this.txtPaymentTerms.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtPaymentTerms.Name = "txtPaymentTerms";
            this.txtPaymentTerms.Size = new System.Drawing.Size(223, 26);
            this.txtPaymentTerms.TabIndex = 17;
            this.txtPaymentTerms.Text = "0";
            // 
            // txtConatctNo
            // 
            this.txtConatctNo.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtConatctNo.Location = new System.Drawing.Point(541, 101);
            this.txtConatctNo.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtConatctNo.Name = "txtConatctNo";
            this.txtConatctNo.Size = new System.Drawing.Size(223, 26);
            this.txtConatctNo.TabIndex = 4;
            // 
            // lblPartyType
            // 
            this.lblPartyType.AutoSize = true;
            this.lblPartyType.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPartyType.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblPartyType.Location = new System.Drawing.Point(541, 184);
            this.lblPartyType.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblPartyType.Name = "lblPartyType";
            this.lblPartyType.Size = new System.Drawing.Size(72, 18);
            this.lblPartyType.TabIndex = 183;
            this.lblPartyType.Text = "Party Type";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label21.Location = new System.Drawing.Point(541, 33);
            this.label21.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(52, 18);
            this.label21.TabIndex = 181;
            this.label21.Text = "GST No";
            // 
            // txtGSTNo
            // 
            this.txtGSTNo.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGSTNo.Location = new System.Drawing.Point(541, 51);
            this.txtGSTNo.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtGSTNo.Name = "txtGSTNo";
            this.txtGSTNo.Size = new System.Drawing.Size(225, 26);
            this.txtGSTNo.TabIndex = 1;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label11.Location = new System.Drawing.Point(155, 345);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(62, 18);
            this.label11.TabIndex = 177;
            this.label11.Text = "E-mail Id";
            // 
            // txtEmail
            // 
            this.txtEmail.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEmail.Location = new System.Drawing.Point(155, 364);
            this.txtEmail.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(322, 26);
            this.txtEmail.TabIndex = 16;
            // 
            // tabPageAdv1
            // 
            this.tabPageAdv1.Controls.Add(this.label17);
            this.tabPageAdv1.Controls.Add(this.txtCEmail2);
            this.tabPageAdv1.Controls.Add(this.label19);
            this.tabPageAdv1.Controls.Add(this.txtCEmail1);
            this.tabPageAdv1.Controls.Add(this.label16);
            this.tabPageAdv1.Controls.Add(this.txtCNumber2);
            this.tabPageAdv1.Controls.Add(this.label5);
            this.tabPageAdv1.Controls.Add(this.txtCPerson1);
            this.tabPageAdv1.Controls.Add(this.label6);
            this.tabPageAdv1.Controls.Add(this.label18);
            this.tabPageAdv1.Controls.Add(this.txtCNumber1);
            this.tabPageAdv1.Controls.Add(this.txtCPerson2);
            this.tabPageAdv1.Image = null;
            this.tabPageAdv1.ImageSize = new System.Drawing.Size(16, 16);
            this.tabPageAdv1.Location = new System.Drawing.Point(1, 30);
            this.tabPageAdv1.Name = "tabPageAdv1";
            this.tabPageAdv1.ShowCloseButton = true;
            this.tabPageAdv1.Size = new System.Drawing.Size(903, 436);
            this.tabPageAdv1.TabIndex = 2;
            this.tabPageAdv1.Text = "Ohter Details";
            this.tabPageAdv1.ThemesEnabled = false;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label17.Location = new System.Drawing.Point(520, 64);
            this.label17.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(47, 18);
            this.label17.TabIndex = 219;
            this.label17.Text = "E-Mail";
            // 
            // txtCEmail2
            // 
            this.txtCEmail2.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCEmail2.Location = new System.Drawing.Point(520, 82);
            this.txtCEmail2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtCEmail2.Name = "txtCEmail2";
            this.txtCEmail2.Size = new System.Drawing.Size(228, 26);
            this.txtCEmail2.TabIndex = 217;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label19.Location = new System.Drawing.Point(520, 18);
            this.label19.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(47, 18);
            this.label19.TabIndex = 218;
            this.label19.Text = "E-mail";
            // 
            // txtCEmail1
            // 
            this.txtCEmail1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCEmail1.Location = new System.Drawing.Point(520, 36);
            this.txtCEmail1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtCEmail1.Name = "txtCEmail1";
            this.txtCEmail1.Size = new System.Drawing.Size(228, 26);
            this.txtCEmail1.TabIndex = 216;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label16.Location = new System.Drawing.Point(271, 64);
            this.label16.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(109, 18);
            this.label16.TabIndex = 215;
            this.label16.Text = "Contact Number";
            // 
            // txtCNumber2
            // 
            this.txtCNumber2.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCNumber2.Location = new System.Drawing.Point(271, 82);
            this.txtCNumber2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtCNumber2.Name = "txtCNumber2";
            this.txtCNumber2.Size = new System.Drawing.Size(223, 26);
            this.txtCNumber2.TabIndex = 211;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label5.Location = new System.Drawing.Point(35, 64);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(108, 18);
            this.label5.TabIndex = 214;
            this.label5.Text = "Contact Person2";
            // 
            // txtCPerson1
            // 
            this.txtCPerson1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCPerson1.Location = new System.Drawing.Point(35, 36);
            this.txtCPerson1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtCPerson1.Name = "txtCPerson1";
            this.txtCPerson1.Size = new System.Drawing.Size(223, 26);
            this.txtCPerson1.TabIndex = 208;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label6.Location = new System.Drawing.Point(32, 18);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(108, 18);
            this.label6.TabIndex = 212;
            this.label6.Text = "Contact Person1";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label18.Location = new System.Drawing.Point(271, 13);
            this.label18.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(109, 18);
            this.label18.TabIndex = 213;
            this.label18.Text = "Contact Number";
            // 
            // txtCNumber1
            // 
            this.txtCNumber1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCNumber1.Location = new System.Drawing.Point(271, 36);
            this.txtCNumber1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtCNumber1.Name = "txtCNumber1";
            this.txtCNumber1.Size = new System.Drawing.Size(223, 26);
            this.txtCNumber1.TabIndex = 209;
            // 
            // txtCPerson2
            // 
            this.txtCPerson2.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCPerson2.Location = new System.Drawing.Point(35, 82);
            this.txtCPerson2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtCPerson2.Name = "txtCPerson2";
            this.txtCPerson2.Size = new System.Drawing.Size(223, 26);
            this.txtCPerson2.TabIndex = 210;
            // 
            // FrmParty
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(934, 569);
            this.Controls.Add(this.grBack);
            this.Controls.Add(this.grFront);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "FrmParty";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Party";
            this.Load += new System.EventHandler(this.Frmparty_Load);
            this.grFront.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.SfDataGridPartyM)).EndInit();
            this.grBack.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tabControlParty)).EndInit();
            this.tabControlParty.ResumeLayout(false);
            this.tabGeneral.ResumeLayout(false);
            this.tabGeneral.PerformLayout();
            this.tabPageAdv1.ResumeLayout(false);
            this.tabPageAdv1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtPin;
        private System.Windows.Forms.Label lblState;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtCity;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtAdd2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtAdd1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtCode;
        private System.Windows.Forms.TextBox txtCName;
        private System.Windows.Forms.Label Phone;
        private System.Windows.Forms.TextBox txtLandLine;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox grFront;
        private Syncfusion.WinForms.DataGrid.SfDataGrid SfDataGridPartyM;
        private System.Windows.Forms.GroupBox grBack;
        private System.Windows.Forms.TextBox txtGSTNo;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label lblPartyType;
        private System.Windows.Forms.Label lblPType;
        private System.Windows.Forms.TextBox txtConatctNo;
        private System.Windows.Forms.TextBox txtPaymentTerms;
        private System.Windows.Forms.ComboBox CmbType;
        private Syncfusion.Windows.Forms.Tools.TabControlAdv tabControlParty;
        private Syncfusion.Windows.Forms.Tools.TabPageAdv tabGeneral;
        private Syncfusion.Windows.Forms.Tools.TabPageAdv tabPageAdv1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox CmbPartyType;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtEmail;
        private System.Windows.Forms.CheckBox chckActive;
        private Syncfusion.Windows.Forms.Tools.toolstripitem toolSaveClose;
        private Syncfusion.Windows.Forms.Tools.SplitButton SplitAdd;
        private Syncfusion.Windows.Forms.Tools.toolstripitem toolEdit;
        private Syncfusion.Windows.Forms.Tools.toolstripitem toolClose;
        private Syncfusion.Windows.Forms.Tools.toolstripitem toolCloseSave;
        private System.Windows.Forms.ComboBox CmbState;
        private Syncfusion.Windows.Forms.Tools.SplitButton SplitSave;
        private Syncfusion.Windows.Forms.Tools.toolstripitem SaveClose;
        private Syncfusion.Windows.Forms.Tools.toolstripitem Back;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label lblCPartyType;
        private System.Windows.Forms.Label lblCtype;
        private System.Windows.Forms.Button BtnDelete;
        protected System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtCNumber2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtCPerson1;
        private System.Windows.Forms.Label label6;
        protected System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txtCNumber1;
        private System.Windows.Forms.TextBox txtCPerson2;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtCEmail2;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txtCEmail1;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.ComboBox CmbCountry;
        private System.Windows.Forms.ComboBox CmbPaymentTerms;
    }
}