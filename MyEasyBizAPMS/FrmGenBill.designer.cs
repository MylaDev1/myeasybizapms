﻿namespace MyEasyBizAPMS
{
    partial class FrmGenBill
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.GrBack = new System.Windows.Forms.GroupBox();
            this.label20 = new System.Windows.Forms.Label();
            this.txtttot = new System.Windows.Forms.TextBox();
            this.cboacchead = new System.Windows.Forms.ComboBox();
            this.txtTaxPercentage = new System.Windows.Forms.ComboBox();
            this.GrSearch = new System.Windows.Forms.GroupBox();
            this.btnGridClose = new System.Windows.Forms.Button();
            this.btnSelect = new System.Windows.Forms.Button();
            this.DataGridCommon = new System.Windows.Forms.DataGridView();
            this.label4 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.DataGridTax = new System.Windows.Forms.DataGridView();
            this.label14 = new System.Windows.Forms.Label();
            this.txtNetValue = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtRoundedOff = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtTotalTaxValue = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.TotaltaxableValue = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtTotalValue = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.DtpBillDate = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.txtBillNo = new System.Windows.Forms.TextBox();
            this.txtBillAddress = new System.Windows.Forms.RichTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtBillTo = new System.Windows.Forms.TextBox();
            this.BtnOk = new System.Windows.Forms.Button();
            this.txtTotal = new System.Windows.Forms.TextBox();
            this.txtRate = new System.Windows.Forms.TextBox();
            this.txtQty = new System.Windows.Forms.TextBox();
            this.txtItemName = new System.Windows.Forms.TextBox();
            this.DataGridItem = new System.Windows.Forms.DataGridView();
            this.label18 = new System.Windows.Forms.Label();
            this.txtUom = new System.Windows.Forms.ComboBox();
            this.txtTaxValue = new System.Windows.Forms.TextBox();
            this.BtnTaxOk = new System.Windows.Forms.Button();
            this.label17 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.txtstid = new System.Windows.Forms.TextBox();
            this.txtigval = new System.Windows.Forms.TextBox();
            this.TxtRoff = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.txtvalue = new System.Windows.Forms.TextBox();
            this.txtnarra = new System.Windows.Forms.RichTextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.txtsocno = new System.Windows.Forms.TextBox();
            this.panAdd = new System.Windows.Forms.Panel();
            this.btnEdit = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnBack = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.grFront = new System.Windows.Forms.GroupBox();
            this.txtSearch = new System.Windows.Forms.TextBox();
            this.DataGridPurchase = new System.Windows.Forms.DataGridView();
            this.GrBack.SuspendLayout();
            this.GrSearch.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridCommon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridTax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridItem)).BeginInit();
            this.panAdd.SuspendLayout();
            this.grFront.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridPurchase)).BeginInit();
            this.SuspendLayout();
            // 
            // GrBack
            // 
            this.GrBack.Controls.Add(this.label20);
            this.GrBack.Controls.Add(this.txtttot);
            this.GrBack.Controls.Add(this.cboacchead);
            this.GrBack.Controls.Add(this.txtTaxPercentage);
            this.GrBack.Controls.Add(this.GrSearch);
            this.GrBack.Controls.Add(this.label4);
            this.GrBack.Controls.Add(this.label16);
            this.GrBack.Controls.Add(this.DataGridTax);
            this.GrBack.Controls.Add(this.label14);
            this.GrBack.Controls.Add(this.txtNetValue);
            this.GrBack.Controls.Add(this.label13);
            this.GrBack.Controls.Add(this.txtRoundedOff);
            this.GrBack.Controls.Add(this.label12);
            this.GrBack.Controls.Add(this.txtTotalTaxValue);
            this.GrBack.Controls.Add(this.label11);
            this.GrBack.Controls.Add(this.TotaltaxableValue);
            this.GrBack.Controls.Add(this.label10);
            this.GrBack.Controls.Add(this.label9);
            this.GrBack.Controls.Add(this.label8);
            this.GrBack.Controls.Add(this.label7);
            this.GrBack.Controls.Add(this.label6);
            this.GrBack.Controls.Add(this.label5);
            this.GrBack.Controls.Add(this.txtTotalValue);
            this.GrBack.Controls.Add(this.label3);
            this.GrBack.Controls.Add(this.DtpBillDate);
            this.GrBack.Controls.Add(this.label2);
            this.GrBack.Controls.Add(this.txtBillNo);
            this.GrBack.Controls.Add(this.txtBillAddress);
            this.GrBack.Controls.Add(this.label1);
            this.GrBack.Controls.Add(this.txtBillTo);
            this.GrBack.Controls.Add(this.BtnOk);
            this.GrBack.Controls.Add(this.txtTotal);
            this.GrBack.Controls.Add(this.txtRate);
            this.GrBack.Controls.Add(this.txtQty);
            this.GrBack.Controls.Add(this.txtItemName);
            this.GrBack.Controls.Add(this.DataGridItem);
            this.GrBack.Controls.Add(this.label18);
            this.GrBack.Controls.Add(this.txtUom);
            this.GrBack.Controls.Add(this.txtTaxValue);
            this.GrBack.Controls.Add(this.BtnTaxOk);
            this.GrBack.Controls.Add(this.label17);
            this.GrBack.Controls.Add(this.label15);
            this.GrBack.Controls.Add(this.txtstid);
            this.GrBack.Controls.Add(this.txtigval);
            this.GrBack.Controls.Add(this.TxtRoff);
            this.GrBack.Controls.Add(this.label19);
            this.GrBack.Controls.Add(this.txtvalue);
            this.GrBack.Controls.Add(this.txtnarra);
            this.GrBack.Controls.Add(this.label22);
            this.GrBack.Controls.Add(this.label21);
            this.GrBack.Controls.Add(this.txtsocno);
            this.GrBack.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GrBack.Location = new System.Drawing.Point(7, -3);
            this.GrBack.Name = "GrBack";
            this.GrBack.Size = new System.Drawing.Size(1056, 542);
            this.GrBack.TabIndex = 0;
            this.GrBack.TabStop = false;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(903, 419);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(41, 18);
            this.label20.TabIndex = 45;
            this.label20.Text = "Total ";
            this.label20.Click += new System.EventHandler(this.label20_Click);
            // 
            // txtttot
            // 
            this.txtttot.Enabled = false;
            this.txtttot.Location = new System.Drawing.Point(950, 415);
            this.txtttot.Name = "txtttot";
            this.txtttot.Size = new System.Drawing.Size(100, 26);
            this.txtttot.TabIndex = 44;
            this.txtttot.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // cboacchead
            // 
            this.cboacchead.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboacchead.FormattingEnabled = true;
            this.cboacchead.Items.AddRange(new object[] {
            "Kg",
            "Nos",
            "Pcs",
            "Ltrs",
            "Set"});
            this.cboacchead.Location = new System.Drawing.Point(6, 139);
            this.cboacchead.Name = "cboacchead";
            this.cboacchead.Size = new System.Drawing.Size(228, 26);
            this.cboacchead.TabIndex = 40;
            // 
            // txtTaxPercentage
            // 
            this.txtTaxPercentage.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtTaxPercentage.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.txtTaxPercentage.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.txtTaxPercentage.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTaxPercentage.FormattingEnabled = true;
            this.txtTaxPercentage.Location = new System.Drawing.Point(845, 139);
            this.txtTaxPercentage.Name = "txtTaxPercentage";
            this.txtTaxPercentage.Size = new System.Drawing.Size(71, 26);
            this.txtTaxPercentage.TabIndex = 12;
            this.txtTaxPercentage.SelectedIndexChanged += new System.EventHandler(this.txtTaxPercentage_SelectedIndexChanged);
            // 
            // GrSearch
            // 
            this.GrSearch.Controls.Add(this.btnGridClose);
            this.GrSearch.Controls.Add(this.btnSelect);
            this.GrSearch.Controls.Add(this.DataGridCommon);
            this.GrSearch.Location = new System.Drawing.Point(306, 174);
            this.GrSearch.Name = "GrSearch";
            this.GrSearch.Size = new System.Drawing.Size(444, 273);
            this.GrSearch.TabIndex = 37;
            this.GrSearch.TabStop = false;
            this.GrSearch.Visible = false;
            // 
            // btnGridClose
            // 
            this.btnGridClose.BackColor = System.Drawing.Color.White;
            this.btnGridClose.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnGridClose.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnGridClose.Location = new System.Drawing.Point(3, 238);
            this.btnGridClose.Name = "btnGridClose";
            this.btnGridClose.Size = new System.Drawing.Size(79, 31);
            this.btnGridClose.TabIndex = 7;
            this.btnGridClose.Text = "Close";
            this.btnGridClose.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnGridClose.UseVisualStyleBackColor = false;
            this.btnGridClose.Click += new System.EventHandler(this.btnGridClose_Click);
            // 
            // btnSelect
            // 
            this.btnSelect.BackColor = System.Drawing.Color.White;
            this.btnSelect.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnSelect.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSelect.Location = new System.Drawing.Point(353, 238);
            this.btnSelect.Name = "btnSelect";
            this.btnSelect.Size = new System.Drawing.Size(86, 31);
            this.btnSelect.TabIndex = 6;
            this.btnSelect.Text = "Select";
            this.btnSelect.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSelect.UseVisualStyleBackColor = false;
            this.btnSelect.Click += new System.EventHandler(this.btnSelect_Click);
            // 
            // DataGridCommon
            // 
            this.DataGridCommon.AllowUserToAddRows = false;
            this.DataGridCommon.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.DataGridCommon.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridCommon.Location = new System.Drawing.Point(3, 16);
            this.DataGridCommon.Name = "DataGridCommon";
            this.DataGridCommon.ReadOnly = true;
            this.DataGridCommon.RowHeadersVisible = false;
            this.DataGridCommon.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridCommon.Size = new System.Drawing.Size(436, 222);
            this.DataGridCommon.TabIndex = 0;
            this.DataGridCommon.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridCommon_CellContentClick);
            this.DataGridCommon.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.DataGridCommon_CellMouseDoubleClick);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(10, 361);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(74, 18);
            this.label4.TabIndex = 36;
            this.label4.Text = "Tax Details";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(848, 119);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(42, 18);
            this.label16.TabIndex = 33;
            this.label16.Text = "Tax %";
            // 
            // DataGridTax
            // 
            this.DataGridTax.AllowUserToAddRows = false;
            this.DataGridTax.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.ActiveCaption;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DataGridTax.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.DataGridTax.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridTax.EnableHeadersVisualStyles = false;
            this.DataGridTax.Location = new System.Drawing.Point(10, 382);
            this.DataGridTax.Name = "DataGridTax";
            this.DataGridTax.ReadOnly = true;
            this.DataGridTax.RowHeadersVisible = false;
            this.DataGridTax.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridTax.Size = new System.Drawing.Size(435, 117);
            this.DataGridTax.TabIndex = 30;
            this.DataGridTax.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DataGridTax_KeyDown);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(877, 477);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(69, 18);
            this.label14.TabIndex = 29;
            this.label14.Text = "Net Value";
            // 
            // txtNetValue
            // 
            this.txtNetValue.Enabled = false;
            this.txtNetValue.Location = new System.Drawing.Point(950, 473);
            this.txtNetValue.Name = "txtNetValue";
            this.txtNetValue.Size = new System.Drawing.Size(100, 26);
            this.txtNetValue.TabIndex = 28;
            this.txtNetValue.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(860, 445);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(86, 18);
            this.label13.TabIndex = 27;
            this.label13.Text = "Rounded Off";
            // 
            // txtRoundedOff
            // 
            this.txtRoundedOff.Location = new System.Drawing.Point(950, 441);
            this.txtRoundedOff.Name = "txtRoundedOff";
            this.txtRoundedOff.Size = new System.Drawing.Size(100, 26);
            this.txtRoundedOff.TabIndex = 26;
            this.txtRoundedOff.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(850, 390);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(96, 18);
            this.label12.TabIndex = 25;
            this.label12.Text = "Total TaxValue";
            // 
            // txtTotalTaxValue
            // 
            this.txtTotalTaxValue.Enabled = false;
            this.txtTotalTaxValue.Location = new System.Drawing.Point(950, 386);
            this.txtTotalTaxValue.Name = "txtTotalTaxValue";
            this.txtTotalTaxValue.Size = new System.Drawing.Size(100, 26);
            this.txtTotalTaxValue.TabIndex = 24;
            this.txtTotalTaxValue.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(820, 361);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(126, 18);
            this.label11.TabIndex = 23;
            this.label11.Text = "Total Taxable Value";
            // 
            // TotaltaxableValue
            // 
            this.TotaltaxableValue.Enabled = false;
            this.TotaltaxableValue.Location = new System.Drawing.Point(950, 357);
            this.TotaltaxableValue.Name = "TotaltaxableValue";
            this.TotaltaxableValue.Size = new System.Drawing.Size(100, 26);
            this.TotaltaxableValue.TabIndex = 22;
            this.TotaltaxableValue.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(223, 48);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(58, 18);
            this.label10.TabIndex = 21;
            this.label10.Text = "Address";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(922, 118);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(38, 18);
            this.label9.TabIndex = 20;
            this.label9.Text = "Total";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(701, 118);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(36, 18);
            this.label8.TabIndex = 19;
            this.label8.Text = "Rate";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(632, 118);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(30, 18);
            this.label7.TabIndex = 18;
            this.label7.Text = "Qty";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(236, 118);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(110, 18);
            this.label6.TabIndex = 17;
            this.label6.Text = "Item Description";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 119);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(99, 18);
            this.label5.TabIndex = 16;
            this.label5.Text = "Accounts Head";
            // 
            // txtTotalValue
            // 
            this.txtTotalValue.Location = new System.Drawing.Point(122, 439);
            this.txtTotalValue.Name = "txtTotalValue";
            this.txtTotalValue.Size = new System.Drawing.Size(124, 26);
            this.txtTotalValue.TabIndex = 11;
            this.txtTotalValue.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(25, 52);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 18);
            this.label3.TabIndex = 13;
            this.label3.Text = "Bill date";
            // 
            // DtpBillDate
            // 
            this.DtpBillDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.DtpBillDate.Location = new System.Drawing.Point(84, 48);
            this.DtpBillDate.Name = "DtpBillDate";
            this.DtpBillDate.Size = new System.Drawing.Size(123, 26);
            this.DtpBillDate.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(2, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(82, 18);
            this.label2.TabIndex = 11;
            this.label2.Text = "Bill Number";
            // 
            // txtBillNo
            // 
            this.txtBillNo.Location = new System.Drawing.Point(84, 18);
            this.txtBillNo.Name = "txtBillNo";
            this.txtBillNo.Size = new System.Drawing.Size(123, 26);
            this.txtBillNo.TabIndex = 0;
            // 
            // txtBillAddress
            // 
            this.txtBillAddress.Location = new System.Drawing.Point(285, 48);
            this.txtBillAddress.Name = "txtBillAddress";
            this.txtBillAddress.Size = new System.Drawing.Size(647, 68);
            this.txtBillAddress.TabIndex = 3;
            this.txtBillAddress.Text = "";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(218, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 18);
            this.label1.TabIndex = 8;
            this.label1.Text = "Bill From";
            // 
            // txtBillTo
            // 
            this.txtBillTo.Location = new System.Drawing.Point(285, 18);
            this.txtBillTo.Name = "txtBillTo";
            this.txtBillTo.Size = new System.Drawing.Size(649, 26);
            this.txtBillTo.TabIndex = 2;
            this.txtBillTo.Click += new System.EventHandler(this.txtBillTo_Click);
            this.txtBillTo.TextChanged += new System.EventHandler(this.txtBillTo_TextChanged);
            // 
            // BtnOk
            // 
            this.BtnOk.Location = new System.Drawing.Point(1016, 137);
            this.BtnOk.Name = "BtnOk";
            this.BtnOk.Size = new System.Drawing.Size(36, 28);
            this.BtnOk.TabIndex = 10;
            this.BtnOk.Text = "Ok";
            this.BtnOk.UseVisualStyleBackColor = true;
            this.BtnOk.Click += new System.EventHandler(this.BtnOk_Click);
            // 
            // txtTotal
            // 
            this.txtTotal.Location = new System.Drawing.Point(922, 139);
            this.txtTotal.Name = "txtTotal";
            this.txtTotal.Size = new System.Drawing.Size(88, 26);
            this.txtTotal.TabIndex = 9;
            this.txtTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtRate
            // 
            this.txtRate.Location = new System.Drawing.Point(701, 139);
            this.txtRate.Name = "txtRate";
            this.txtRate.Size = new System.Drawing.Size(71, 26);
            this.txtRate.TabIndex = 8;
            this.txtRate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtRate.TextChanged += new System.EventHandler(this.txtRate_TextChanged);
            // 
            // txtQty
            // 
            this.txtQty.Location = new System.Drawing.Point(632, 139);
            this.txtQty.Name = "txtQty";
            this.txtQty.Size = new System.Drawing.Size(63, 26);
            this.txtQty.TabIndex = 7;
            this.txtQty.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtQty.TextChanged += new System.EventHandler(this.txtQty_TextChanged);
            // 
            // txtItemName
            // 
            this.txtItemName.Location = new System.Drawing.Point(236, 139);
            this.txtItemName.Name = "txtItemName";
            this.txtItemName.Size = new System.Drawing.Size(233, 26);
            this.txtItemName.TabIndex = 5;
            // 
            // DataGridItem
            // 
            this.DataGridItem.AllowUserToAddRows = false;
            this.DataGridItem.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.ActiveCaption;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DataGridItem.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.DataGridItem.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridItem.EnableHeadersVisualStyles = false;
            this.DataGridItem.Location = new System.Drawing.Point(6, 168);
            this.DataGridItem.Name = "DataGridItem";
            this.DataGridItem.ReadOnly = true;
            this.DataGridItem.RowHeadersVisible = false;
            this.DataGridItem.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridItem.Size = new System.Drawing.Size(1046, 187);
            this.DataGridItem.TabIndex = 0;
            this.DataGridItem.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DataGridItem_KeyDown);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(576, 118);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(39, 18);
            this.label18.TabIndex = 39;
            this.label18.Text = "UOM";
            // 
            // txtUom
            // 
            this.txtUom.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.txtUom.FormattingEnabled = true;
            this.txtUom.Items.AddRange(new object[] {
            "Kg",
            "Nos",
            "Pcs",
            "Ltrs",
            "Set"});
            this.txtUom.Location = new System.Drawing.Point(576, 139);
            this.txtUom.Name = "txtUom";
            this.txtUom.Size = new System.Drawing.Size(54, 26);
            this.txtUom.TabIndex = 6;
            // 
            // txtTaxValue
            // 
            this.txtTaxValue.Location = new System.Drawing.Point(285, 449);
            this.txtTaxValue.Name = "txtTaxValue";
            this.txtTaxValue.Size = new System.Drawing.Size(123, 26);
            this.txtTaxValue.TabIndex = 13;
            this.txtTaxValue.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // BtnTaxOk
            // 
            this.BtnTaxOk.Location = new System.Drawing.Point(290, 435);
            this.BtnTaxOk.Name = "BtnTaxOk";
            this.BtnTaxOk.Size = new System.Drawing.Size(36, 28);
            this.BtnTaxOk.TabIndex = 14;
            this.BtnTaxOk.Text = "Ok";
            this.BtnTaxOk.UseVisualStyleBackColor = true;
            this.BtnTaxOk.Click += new System.EventHandler(this.BtnTaxOk_Click);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(287, 429);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(66, 18);
            this.label17.TabIndex = 35;
            this.label17.Text = "Tax Value";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(124, 419);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(93, 18);
            this.label15.TabIndex = 31;
            this.label15.Text = "Taxable Value";
            // 
            // txtstid
            // 
            this.txtstid.Location = new System.Drawing.Point(68, 216);
            this.txtstid.Name = "txtstid";
            this.txtstid.Size = new System.Drawing.Size(130, 26);
            this.txtstid.TabIndex = 4;
            // 
            // txtigval
            // 
            this.txtigval.Location = new System.Drawing.Point(102, 236);
            this.txtigval.Name = "txtigval";
            this.txtigval.Size = new System.Drawing.Size(63, 26);
            this.txtigval.TabIndex = 43;
            this.txtigval.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // TxtRoff
            // 
            this.TxtRoff.Location = new System.Drawing.Point(171, 236);
            this.TxtRoff.Name = "TxtRoff";
            this.TxtRoff.Size = new System.Drawing.Size(63, 26);
            this.TxtRoff.TabIndex = 46;
            this.TxtRoff.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(777, 118);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(43, 18);
            this.label19.TabIndex = 42;
            this.label19.Text = "Value";
            // 
            // txtvalue
            // 
            this.txtvalue.Location = new System.Drawing.Point(777, 139);
            this.txtvalue.Name = "txtvalue";
            this.txtvalue.Size = new System.Drawing.Size(66, 26);
            this.txtvalue.TabIndex = 41;
            this.txtvalue.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtnarra
            // 
            this.txtnarra.Location = new System.Drawing.Point(451, 382);
            this.txtnarra.Name = "txtnarra";
            this.txtnarra.Size = new System.Drawing.Size(301, 114);
            this.txtnarra.TabIndex = 50;
            this.txtnarra.Text = "";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(451, 365);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(66, 18);
            this.label22.TabIndex = 49;
            this.label22.Text = "Narration";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(471, 118);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(47, 18);
            this.label21.TabIndex = 48;
            this.label21.Text = "SocNo";
            // 
            // txtsocno
            // 
            this.txtsocno.Location = new System.Drawing.Point(471, 139);
            this.txtsocno.Name = "txtsocno";
            this.txtsocno.Size = new System.Drawing.Size(103, 26);
            this.txtsocno.TabIndex = 47;
            this.txtsocno.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtsocno.Click += new System.EventHandler(this.txtsocno_Click);
            this.txtsocno.TextChanged += new System.EventHandler(this.txtsocno_TextChanged);
            this.txtsocno.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtsocno_KeyDown);
            // 
            // panAdd
            // 
            this.panAdd.BackColor = System.Drawing.Color.White;
            this.panAdd.Controls.Add(this.btnEdit);
            this.panAdd.Controls.Add(this.btnAdd);
            this.panAdd.Controls.Add(this.btnDelete);
            this.panAdd.Controls.Add(this.btnBack);
            this.panAdd.Controls.Add(this.btnSave);
            this.panAdd.Controls.Add(this.btnExit);
            this.panAdd.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panAdd.Location = new System.Drawing.Point(7, 502);
            this.panAdd.Name = "panAdd";
            this.panAdd.Size = new System.Drawing.Size(1056, 36);
            this.panAdd.TabIndex = 3;
            // 
            // btnEdit
            // 
            this.btnEdit.BackColor = System.Drawing.Color.White;
            this.btnEdit.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnEdit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnEdit.Location = new System.Drawing.Point(816, 3);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(74, 31);
            this.btnEdit.TabIndex = 1;
            this.btnEdit.Text = "Edit";
            this.btnEdit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnEdit.UseVisualStyleBackColor = false;
            this.btnEdit.Click += new System.EventHandler(this.BtnEdit_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.BackColor = System.Drawing.Color.White;
            this.btnAdd.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnAdd.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAdd.Location = new System.Drawing.Point(744, 3);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(74, 31);
            this.btnAdd.TabIndex = 0;
            this.btnAdd.Text = "Add";
            this.btnAdd.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAdd.UseVisualStyleBackColor = false;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.BackColor = System.Drawing.Color.White;
            this.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnDelete.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnDelete.Location = new System.Drawing.Point(893, 3);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(81, 31);
            this.btnDelete.TabIndex = 3;
            this.btnDelete.Text = "Delete";
            this.btnDelete.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnDelete.UseVisualStyleBackColor = false;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnBack
            // 
            this.btnBack.BackColor = System.Drawing.Color.White;
            this.btnBack.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnBack.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnBack.Location = new System.Drawing.Point(976, 3);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(77, 31);
            this.btnBack.TabIndex = 5;
            this.btnBack.Text = "Back";
            this.btnBack.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnBack.UseVisualStyleBackColor = false;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.Color.White;
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSave.Location = new System.Drawing.Point(893, 3);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(77, 31);
            this.btnSave.TabIndex = 2;
            this.btnSave.Text = "Save";
            this.btnSave.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnExit
            // 
            this.btnExit.BackColor = System.Drawing.Color.White;
            this.btnExit.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnExit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExit.Location = new System.Drawing.Point(976, 3);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(77, 31);
            this.btnExit.TabIndex = 4;
            this.btnExit.Text = "Exit";
            this.btnExit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnExit.UseVisualStyleBackColor = false;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // grFront
            // 
            this.grFront.Controls.Add(this.txtSearch);
            this.grFront.Controls.Add(this.DataGridPurchase);
            this.grFront.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grFront.Location = new System.Drawing.Point(7, -2);
            this.grFront.Name = "grFront";
            this.grFront.Size = new System.Drawing.Size(1055, 498);
            this.grFront.TabIndex = 3;
            this.grFront.TabStop = false;
            // 
            // txtSearch
            // 
            this.txtSearch.Location = new System.Drawing.Point(6, 14);
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(1039, 26);
            this.txtSearch.TabIndex = 2;
            // 
            // DataGridPurchase
            // 
            this.DataGridPurchase.AllowUserToAddRows = false;
            this.DataGridPurchase.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.ActiveCaption;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DataGridPurchase.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.DataGridPurchase.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridPurchase.EnableHeadersVisualStyles = false;
            this.DataGridPurchase.Location = new System.Drawing.Point(6, 42);
            this.DataGridPurchase.Name = "DataGridPurchase";
            this.DataGridPurchase.ReadOnly = true;
            this.DataGridPurchase.RowHeadersVisible = false;
            this.DataGridPurchase.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridPurchase.Size = new System.Drawing.Size(1039, 450);
            this.DataGridPurchase.TabIndex = 1;
            this.DataGridPurchase.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DataGridPurchase_KeyDown);
            // 
            // FrmGenBill
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(1064, 541);
            this.Controls.Add(this.panAdd);
            this.Controls.Add(this.grFront);
            this.Controls.Add(this.GrBack);
            this.Name = "FrmGenBill";
            this.Text = "General Bill Accouting";
            this.Load += new System.EventHandler(this.FrmGenBill_Load);
            this.GrBack.ResumeLayout(false);
            this.GrBack.PerformLayout();
            this.GrSearch.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DataGridCommon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridTax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridItem)).EndInit();
            this.panAdd.ResumeLayout(false);
            this.grFront.ResumeLayout(false);
            this.grFront.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridPurchase)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox GrBack;
        private System.Windows.Forms.Panel panAdd;
        private System.Windows.Forms.Button btnEdit;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnBack;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.DataGridView DataGridItem;
        private System.Windows.Forms.TextBox txtstid;
        private System.Windows.Forms.TextBox txtItemName;
        private System.Windows.Forms.GroupBox grFront;
        private System.Windows.Forms.DataGridView DataGridPurchase;
        private System.Windows.Forms.TextBox txtSearch;
        private System.Windows.Forms.Button BtnOk;
        private System.Windows.Forms.TextBox txtTotal;
        private System.Windows.Forms.TextBox txtRate;
        private System.Windows.Forms.TextBox txtQty;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtBillTo;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtBillNo;
        private System.Windows.Forms.RichTextBox txtBillAddress;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DateTimePicker DtpBillDate;
        private System.Windows.Forms.TextBox txtTotalValue;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtRoundedOff;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtTotalTaxValue;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox TotaltaxableValue;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtNetValue;
        private System.Windows.Forms.DataGridView DataGridTax;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtTaxValue;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button BtnTaxOk;
        private System.Windows.Forms.GroupBox GrSearch;
        private System.Windows.Forms.Button btnGridClose;
        private System.Windows.Forms.Button btnSelect;
        private System.Windows.Forms.DataGridView DataGridCommon;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.ComboBox txtTaxPercentage;
        private System.Windows.Forms.ComboBox txtUom;
        private System.Windows.Forms.ComboBox cboacchead;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txtvalue;
        private System.Windows.Forms.TextBox txtigval;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox txtttot;
        private System.Windows.Forms.TextBox TxtRoff;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox txtsocno;
        private System.Windows.Forms.RichTextBox txtnarra;
        private System.Windows.Forms.Label label22;
    }
}