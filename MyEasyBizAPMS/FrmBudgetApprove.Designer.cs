﻿namespace MyEasyBizAPMS
{
    partial class FrmBudgetApprove
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Syncfusion.Windows.Forms.Tools.SplitButtonRenderer splitButtonRenderer1 = new Syncfusion.Windows.Forms.Tools.SplitButtonRenderer();
            this.SplitSave = new Syncfusion.Windows.Forms.Tools.SplitButton();
            this.toolstripBack = new Syncfusion.Windows.Forms.Tools.toolstripitem();
            this.GrFront = new System.Windows.Forms.GroupBox();
            this.BtnDownloadtoExcel = new System.Windows.Forms.Button();
            this.GrApprove = new System.Windows.Forms.GroupBox();
            this.CmbStatus = new System.Windows.Forms.ComboBox();
            this.label17 = new System.Windows.Forms.Label();
            this.lblPLPer = new System.Windows.Forms.Label();
            this.lblPLValue = new System.Windows.Forms.Label();
            this.lblExpense = new System.Windows.Forms.Label();
            this.lblIncome = new System.Windows.Forms.Label();
            this.lblReqQty = new System.Windows.Forms.Label();
            this.lblOrderQty = new System.Windows.Forms.Label();
            this.lblSeason = new System.Windows.Forms.Label();
            this.lblStyle = new System.Windows.Forms.Label();
            this.lblOrderNo = new System.Windows.Forms.Label();
            this.txtRemarks = new System.Windows.Forms.RichTextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.BtnClose = new System.Windows.Forms.Button();
            this.BtnPrint = new System.Windows.Forms.Button();
            this.SfDataGridPreBudjet = new Syncfusion.WinForms.DataGrid.SfDataGrid();
            this.GrFront.SuspendLayout();
            this.GrApprove.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SfDataGridPreBudjet)).BeginInit();
            this.SuspendLayout();
            // 
            // SplitSave
            // 
            this.SplitSave.BackColor = System.Drawing.SystemColors.Control;
            this.SplitSave.BeforeTouchSize = new System.Drawing.Size(79, 36);
            this.SplitSave.DropDownItems.Add(this.toolstripBack);
            this.SplitSave.DropDownPosition = Syncfusion.Windows.Forms.Tools.Position.Bottom;
            this.SplitSave.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SplitSave.ForeColor = System.Drawing.Color.Black;
            this.SplitSave.Location = new System.Drawing.Point(432, 225);
            this.SplitSave.MinimumSize = new System.Drawing.Size(75, 23);
            this.SplitSave.Name = "SplitSave";
            splitButtonRenderer1.SplitButton = this.SplitSave;
            this.SplitSave.Renderer = splitButtonRenderer1;
            this.SplitSave.ShowDropDownOnButtonClick = false;
            this.SplitSave.Size = new System.Drawing.Size(79, 36);
            this.SplitSave.TabIndex = 2;
            this.SplitSave.Text = "Save";
            this.SplitSave.DropDowItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.SplitSave_DropDowItemClicked);
            this.SplitSave.Click += new System.EventHandler(this.SplitSave_Click);
            // 
            // toolstripBack
            // 
            this.toolstripBack.Name = "toolstripBack";
            this.toolstripBack.Size = new System.Drawing.Size(23, 23);
            this.toolstripBack.Text = "Back";
            // 
            // GrFront
            // 
            this.GrFront.BackColor = System.Drawing.Color.White;
            this.GrFront.Controls.Add(this.BtnDownloadtoExcel);
            this.GrFront.Controls.Add(this.GrApprove);
            this.GrFront.Controls.Add(this.BtnClose);
            this.GrFront.Controls.Add(this.BtnPrint);
            this.GrFront.Controls.Add(this.SfDataGridPreBudjet);
            this.GrFront.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GrFront.Location = new System.Drawing.Point(9, -2);
            this.GrFront.Name = "GrFront";
            this.GrFront.Size = new System.Drawing.Size(1109, 570);
            this.GrFront.TabIndex = 0;
            this.GrFront.TabStop = false;
            // 
            // BtnDownloadtoExcel
            // 
            this.BtnDownloadtoExcel.Location = new System.Drawing.Point(108, 531);
            this.BtnDownloadtoExcel.Name = "BtnDownloadtoExcel";
            this.BtnDownloadtoExcel.Size = new System.Drawing.Size(136, 35);
            this.BtnDownloadtoExcel.TabIndex = 6;
            this.BtnDownloadtoExcel.Text = "Download to Excel";
            this.BtnDownloadtoExcel.UseVisualStyleBackColor = true;
            this.BtnDownloadtoExcel.Click += new System.EventHandler(this.BtnDownloadtoExcel_Click);
            // 
            // GrApprove
            // 
            this.GrApprove.BackColor = System.Drawing.Color.LightSkyBlue;
            this.GrApprove.Controls.Add(this.SplitSave);
            this.GrApprove.Controls.Add(this.CmbStatus);
            this.GrApprove.Controls.Add(this.label17);
            this.GrApprove.Controls.Add(this.lblPLPer);
            this.GrApprove.Controls.Add(this.lblPLValue);
            this.GrApprove.Controls.Add(this.lblExpense);
            this.GrApprove.Controls.Add(this.lblIncome);
            this.GrApprove.Controls.Add(this.lblReqQty);
            this.GrApprove.Controls.Add(this.lblOrderQty);
            this.GrApprove.Controls.Add(this.lblSeason);
            this.GrApprove.Controls.Add(this.lblStyle);
            this.GrApprove.Controls.Add(this.lblOrderNo);
            this.GrApprove.Controls.Add(this.txtRemarks);
            this.GrApprove.Controls.Add(this.label10);
            this.GrApprove.Controls.Add(this.label9);
            this.GrApprove.Controls.Add(this.label8);
            this.GrApprove.Controls.Add(this.label7);
            this.GrApprove.Controls.Add(this.label6);
            this.GrApprove.Controls.Add(this.label5);
            this.GrApprove.Controls.Add(this.label4);
            this.GrApprove.Controls.Add(this.label3);
            this.GrApprove.Controls.Add(this.label2);
            this.GrApprove.Controls.Add(this.label1);
            this.GrApprove.Location = new System.Drawing.Point(306, 52);
            this.GrApprove.Name = "GrApprove";
            this.GrApprove.Size = new System.Drawing.Size(529, 299);
            this.GrApprove.TabIndex = 5;
            this.GrApprove.TabStop = false;
            this.GrApprove.Text = "Approvel";
            this.GrApprove.Visible = false;
            // 
            // CmbStatus
            // 
            this.CmbStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CmbStatus.FormattingEnabled = true;
            this.CmbStatus.Items.AddRange(new object[] {
            "Pending",
            "Approved"});
            this.CmbStatus.Location = new System.Drawing.Point(368, 163);
            this.CmbStatus.Name = "CmbStatus";
            this.CmbStatus.Size = new System.Drawing.Size(142, 26);
            this.CmbStatus.TabIndex = 21;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(314, 166);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(53, 18);
            this.label17.TabIndex = 20;
            this.label17.Text = "Status :";
            // 
            // lblPLPer
            // 
            this.lblPLPer.AutoSize = true;
            this.lblPLPer.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPLPer.Location = new System.Drawing.Point(365, 126);
            this.lblPLPer.Name = "lblPLPer";
            this.lblPLPer.Size = new System.Drawing.Size(13, 18);
            this.lblPLPer.TabIndex = 19;
            this.lblPLPer.Text = "-";
            // 
            // lblPLValue
            // 
            this.lblPLValue.AutoSize = true;
            this.lblPLValue.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPLValue.Location = new System.Drawing.Point(365, 90);
            this.lblPLValue.Name = "lblPLValue";
            this.lblPLValue.Size = new System.Drawing.Size(13, 18);
            this.lblPLValue.TabIndex = 18;
            this.lblPLValue.Text = "-";
            // 
            // lblExpense
            // 
            this.lblExpense.AutoSize = true;
            this.lblExpense.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblExpense.Location = new System.Drawing.Point(365, 62);
            this.lblExpense.Name = "lblExpense";
            this.lblExpense.Size = new System.Drawing.Size(13, 18);
            this.lblExpense.TabIndex = 17;
            this.lblExpense.Text = "-";
            // 
            // lblIncome
            // 
            this.lblIncome.AutoSize = true;
            this.lblIncome.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIncome.Location = new System.Drawing.Point(365, 34);
            this.lblIncome.Name = "lblIncome";
            this.lblIncome.Size = new System.Drawing.Size(13, 18);
            this.lblIncome.TabIndex = 16;
            this.lblIncome.Text = "-";
            // 
            // lblReqQty
            // 
            this.lblReqQty.AutoSize = true;
            this.lblReqQty.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblReqQty.Location = new System.Drawing.Point(86, 192);
            this.lblReqQty.Name = "lblReqQty";
            this.lblReqQty.Size = new System.Drawing.Size(13, 18);
            this.lblReqQty.TabIndex = 15;
            this.lblReqQty.Text = "-";
            // 
            // lblOrderQty
            // 
            this.lblOrderQty.AutoSize = true;
            this.lblOrderQty.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOrderQty.Location = new System.Drawing.Point(86, 153);
            this.lblOrderQty.Name = "lblOrderQty";
            this.lblOrderQty.Size = new System.Drawing.Size(13, 18);
            this.lblOrderQty.TabIndex = 14;
            this.lblOrderQty.Text = "-";
            // 
            // lblSeason
            // 
            this.lblSeason.AutoSize = true;
            this.lblSeason.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSeason.Location = new System.Drawing.Point(86, 114);
            this.lblSeason.Name = "lblSeason";
            this.lblSeason.Size = new System.Drawing.Size(13, 18);
            this.lblSeason.TabIndex = 13;
            this.lblSeason.Text = "-";
            // 
            // lblStyle
            // 
            this.lblStyle.AutoSize = true;
            this.lblStyle.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStyle.Location = new System.Drawing.Point(86, 74);
            this.lblStyle.Name = "lblStyle";
            this.lblStyle.Size = new System.Drawing.Size(13, 18);
            this.lblStyle.TabIndex = 12;
            this.lblStyle.Text = "-";
            // 
            // lblOrderNo
            // 
            this.lblOrderNo.AutoSize = true;
            this.lblOrderNo.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOrderNo.Location = new System.Drawing.Point(86, 34);
            this.lblOrderNo.Name = "lblOrderNo";
            this.lblOrderNo.Size = new System.Drawing.Size(13, 18);
            this.lblOrderNo.TabIndex = 11;
            this.lblOrderNo.Text = "-";
            // 
            // txtRemarks
            // 
            this.txtRemarks.Location = new System.Drawing.Point(86, 225);
            this.txtRemarks.Name = "txtRemarks";
            this.txtRemarks.Size = new System.Drawing.Size(340, 60);
            this.txtRemarks.TabIndex = 10;
            this.txtRemarks.Text = "";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(19, 232);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(61, 18);
            this.label10.TabIndex = 9;
            this.label10.Text = "Remarks";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(216, 126);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(151, 18);
            this.label9.TabIndex = 8;
            this.label9.Text = "Profit Loss Percentage :";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(251, 90);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(116, 18);
            this.label8.TabIndex = 7;
            this.label8.Text = "Profit Loss Value :";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(300, 62);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(67, 18);
            this.label7.TabIndex = 6;
            this.label7.Text = "Expense :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(306, 34);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(61, 18);
            this.label6.TabIndex = 5;
            this.label6.Text = "Income :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(20, 192);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(67, 18);
            this.label5.TabIndex = 4;
            this.label5.Text = "Plan Qty :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(11, 153);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(76, 18);
            this.label4.TabIndex = 3;
            this.label4.Text = "Order Qty :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(28, 114);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 18);
            this.label3.TabIndex = 2;
            this.label3.Text = "Season :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(41, 74);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(46, 18);
            this.label2.TabIndex = 1;
            this.label2.Text = "Style :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(15, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 18);
            this.label1.TabIndex = 0;
            this.label1.Text = "Order No :";
            // 
            // BtnClose
            // 
            this.BtnClose.Location = new System.Drawing.Point(968, 530);
            this.BtnClose.Name = "BtnClose";
            this.BtnClose.Size = new System.Drawing.Size(92, 35);
            this.BtnClose.TabIndex = 4;
            this.BtnClose.Text = "Close";
            this.BtnClose.UseVisualStyleBackColor = true;
            this.BtnClose.Click += new System.EventHandler(this.BtnClose_Click);
            // 
            // BtnPrint
            // 
            this.BtnPrint.Location = new System.Drawing.Point(10, 531);
            this.BtnPrint.Name = "BtnPrint";
            this.BtnPrint.Size = new System.Drawing.Size(97, 35);
            this.BtnPrint.TabIndex = 3;
            this.BtnPrint.Text = "Print";
            this.BtnPrint.UseVisualStyleBackColor = true;
            this.BtnPrint.Click += new System.EventHandler(this.BtnPrint_Click);
            // 
            // SfDataGridPreBudjet
            // 
            this.SfDataGridPreBudjet.AccessibleName = "Table";
            this.SfDataGridPreBudjet.AllowFiltering = true;
            this.SfDataGridPreBudjet.Location = new System.Drawing.Point(9, 17);
            this.SfDataGridPreBudjet.Name = "SfDataGridPreBudjet";
            this.SfDataGridPreBudjet.Size = new System.Drawing.Size(1094, 512);
            this.SfDataGridPreBudjet.TabIndex = 1;
            this.SfDataGridPreBudjet.Text = "sfDataGrid1";
            this.SfDataGridPreBudjet.QueryCellStyle += new Syncfusion.WinForms.DataGrid.Events.QueryCellStyleEventHandler(this.SfDataGridPreBudjet_QueryCellStyle);
            this.SfDataGridPreBudjet.CellDoubleClick += new Syncfusion.WinForms.DataGrid.Events.CellClickEventHandler(this.SfDataGridPreBudjet_CellDoubleClick);
            this.SfDataGridPreBudjet.Click += new System.EventHandler(this.SfDataGridPreBudjet_Click);
            // 
            // FrmBudgetApprove
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1123, 569);
            this.Controls.Add(this.GrFront);
            this.Name = "FrmBudgetApprove";
            this.Text = "FrmBudgetApprove";
            this.Load += new System.EventHandler(this.FrmBudgetApprove_Load);
            this.GrFront.ResumeLayout(false);
            this.GrApprove.ResumeLayout(false);
            this.GrApprove.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SfDataGridPreBudjet)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox GrFront;
        private Syncfusion.WinForms.DataGrid.SfDataGrid SfDataGridPreBudjet;
        private System.Windows.Forms.Button BtnPrint;
        private System.Windows.Forms.Button BtnClose;
        private System.Windows.Forms.GroupBox GrApprove;
        private System.Windows.Forms.RichTextBox txtRemarks;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblPLPer;
        private System.Windows.Forms.Label lblPLValue;
        private System.Windows.Forms.Label lblExpense;
        private System.Windows.Forms.Label lblIncome;
        private System.Windows.Forms.Label lblReqQty;
        private System.Windows.Forms.Label lblOrderQty;
        private System.Windows.Forms.Label lblSeason;
        private System.Windows.Forms.Label lblStyle;
        private System.Windows.Forms.Label lblOrderNo;
        private System.Windows.Forms.ComboBox CmbStatus;
        private System.Windows.Forms.Label label17;
        private Syncfusion.Windows.Forms.Tools.SplitButton SplitSave;
        private Syncfusion.Windows.Forms.Tools.toolstripitem toolstripBack;
        private System.Windows.Forms.Button BtnDownloadtoExcel;
    }
}