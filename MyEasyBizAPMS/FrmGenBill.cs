﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Drawing.Imaging;
using System.Configuration;
using System.Data.SqlClient;
using System.Diagnostics;
using excel = Microsoft.Office.Interop.Excel;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
namespace MyEasyBizAPMS
{
    public partial class FrmGenBill : Form
    {
        public FrmGenBill()
        {
            InitializeComponent();
        }
        SqlConnection conn = new SqlConnection(GeneralParameters.ConnectionString);
        SqlCommand qur = new SqlCommand();
        SQLDBHelper db = new SQLDBHelper();
        BindingSource bsBillFrom = new BindingSource();
        BindingSource bsc = new BindingSource();
        BindingSource bsp = new BindingSource();
        BindingSource bsserial = new BindingSource();
        BindingSource IN = new BindingSource();
        BindingSource OUT = new BindingSource();
        BindingSource bsFabric = new BindingSource();
        string uid = "";
        int mode = 0;
        double sum1;
        double sum2;
        int sum;
        string address;
        int ty = 0;
        int type = 0;
        double dis3 = 0;
        double dis4 = 0;
        double dd8 = 0;
        double dd1 = 0;
        double dd2 = 0;
        double hg = 0;
        int i;
        String TYPENAME;
        int Dtype = 0;
        double sum5 = 0;
        string strfin;
        int totamt;
        double dd9;
        double val2;
        double sumb;
        double uy;
        double uy1;
        double dis6;
        double dis5;
        int m; Double net1; double someInt; double rof; Double rof1; Double rof2; Double net; double bval; double dis;
        double hh; double dd; double sump; double sumq; double add; double yt7; double df = 0; double dg = 0; double dg1 = 0; double str2 = 0;
        int p = 0; double sun; Int64 NumVal; string Nw;
        int j = -1;


        private void FrmGenBill_Load(object sender, EventArgs e)
        {
            qur.Connection = conn;
            Genclass.Dtype = 1400;
            LoadItemGrid();
            LoadItemTax();
            LoadFrontGrid();
            tax();
            Accountshead();
            LoadButton(1);
            Titlegst();
        }
        protected void FillGrid1(DataTable dt, int Fillid)
        {
            try
            {
                DataGridCommon.DataSource = null;
                DataGridCommon.AutoGenerateColumns = false;
                DataGridCommon.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
                DataGridCommon.DefaultCellStyle.WrapMode = DataGridViewTriState.True;

                DataGridCommon.ColumnCount = 3;
                DataGridCommon.Columns[0].Name = "Uid";
                DataGridCommon.Columns[0].HeaderText = "Uid";
                DataGridCommon.Columns[0].DataPropertyName = "Uid";
                DataGridCommon.Columns[1].Name = "OrderNO";
                DataGridCommon.Columns[1].HeaderText = "OrderNO";
                DataGridCommon.Columns[1].DataPropertyName = "OrderNO";
                DataGridCommon.Columns[1].Width = 100;
                DataGridCommon.Columns[2].Name = "StyleName";
                DataGridCommon.Columns[2].HeaderText = "StyleName";
                DataGridCommon.Columns[2].DataPropertyName = "StyleName";
                DataGridCommon.Columns[2].Visible = false;
                DataGridCommon.DataSource = bsc;
                DataGridCommon.Columns[0].Visible = false;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        public void tax()
        {
            string qur = "select Generalname as tax,Cast( F1 as int) uid from generalm where   Typemuid in (8) and active=1 ";
            DataTable tab = db.GetData2(CommandType.Text, qur);
            txtTaxPercentage.DataSource = null;
            txtTaxPercentage.DisplayMember = "tax";
            txtTaxPercentage.ValueMember = "uid";
            txtTaxPercentage.DataSource = tab;
        }
        public void Accountshead()
        {
            string qur = "select Generalname as tax,Cast( F1 as int) uid from generalm where   Typemuid in (31) and active=1 ";
            DataTable tab = db.GetData2(CommandType.Text, qur);
            cboacchead.DataSource = null;
            cboacchead.DisplayMember = "tax";
            cboacchead.ValueMember = "uid";
            cboacchead.DataSource = tab;
        }
        private void LoadButton(int Id)
        {
            try
            {
                if (Id == 1) //Load
                {
                    GrBack.Visible = false;
                    grFront.Visible = true;
                    btnAdd.Visible = true;
                    btnEdit.Visible = true;
                    btnDelete.Visible = true;
                    btnExit.Visible = true;
                    btnSave.Visible = false;
                    btnBack.Visible = false;
                    btnSave.Text = "Save";
                }
                else if (Id == 2)//Add
                {
                    GrBack.Visible = true;
                    grFront.Visible = false;
                    btnAdd.Visible = false;
                    btnEdit.Visible = false;
                    btnDelete.Visible = false;
                    btnExit.Visible = false;
                    btnSave.Visible = true;
                    btnBack.Visible = true;
                    btnSave.Text = "Save";
                }
                else if (Id == 3)//Edit
                {
                    GrBack.Visible = true;
                    grFront.Visible = false;
                    btnAdd.Visible = false;
                    btnEdit.Visible = false;
                    btnDelete.Visible = false;
                    btnExit.Visible = false;
                    btnSave.Visible = true;
                    btnBack.Visible = true;
                    btnSave.Text = "Update";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void LoadItemGrid()
        {
            DataGridItem.DataSource = null;
            DataGridItem.AutoGenerateColumns = false;
            DataGridItem.ColumnCount = 12;
            DataGridItem.Columns[0].Name = "AccountHead";
            DataGridItem.Columns[0].HeaderText = "AccountHead";
            DataGridItem.Columns[0].Width = 120;

            DataGridItem.Columns[1].Name = "Item name";
            DataGridItem.Columns[1].HeaderText = "Item name";
            DataGridItem.Columns[1].Width = 240;


            DataGridItem.Columns[2].Name = "SocNo";
            DataGridItem.Columns[2].HeaderText = "SocNo";
            DataGridItem.Columns[2].Width = 80;

            DataGridItem.Columns[3].Name = "UOM";
            DataGridItem.Columns[3].HeaderText = "UOM";
            DataGridItem.Columns[3].Width = 80;

            DataGridItem.Columns[4].Name = "Qty";
            DataGridItem.Columns[4].HeaderText = "Qty";
            DataGridItem.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            DataGridItem.Columns[4].Width = 90;

            DataGridItem.Columns[5].Name = "Rate";
            DataGridItem.Columns[5].HeaderText = "Rate";
            DataGridItem.Columns[5].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            DataGridItem.Columns[5].Width = 70;

            DataGridItem.Columns[6].Name = "Value";
            DataGridItem.Columns[6].HeaderText = "Value";
            DataGridItem.Columns[6].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            DataGridItem.Columns[6].Width = 70;

            DataGridItem.Columns[7].Name = "Uid";
            DataGridItem.Columns[7].HeaderText = "Uid";
            DataGridItem.Columns[7].Visible = false;

            DataGridItem.Columns[8].Name = "Tax";
            DataGridItem.Columns[8].HeaderText = "Tax";
            DataGridItem.Columns[8].Width = 60;
            DataGridItem.Columns[9].Name = "TaxValue";
            DataGridItem.Columns[9].HeaderText = "TaxValue";
            DataGridItem.Columns[9].Width = 90;
            DataGridItem.Columns[10].Name = "Total";
            DataGridItem.Columns[10].HeaderText = "Total";
            DataGridItem.Columns[10].Width = 90;
            DataGridItem.Columns[11].Name = "TaxPer";
            DataGridItem.Columns[11].HeaderText = "TaxPer";
            DataGridItem.Columns[11].Visible = false;
        }

        protected void LoadItemTax()
        {
            DataGridTax.DataSource = null;
            DataGridTax.AutoGenerateColumns = false;
            DataGridTax.ColumnCount = 4;

            DataGridTax.Columns[0].Name = "Taxable Value";
            DataGridTax.Columns[0].HeaderText = "Taxable Value";
            DataGridTax.Columns[0].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            DataGridTax.Columns[0].Width = 130;

            DataGridTax.Columns[1].Name = "Tax %";
            DataGridTax.Columns[1].HeaderText = "Tax %";
            DataGridTax.Columns[1].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            DataGridTax.Columns[1].Width = 130;

            DataGridTax.Columns[2].Name = "Tax Value";
            DataGridTax.Columns[2].HeaderText = "Tax Value";
            DataGridTax.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            DataGridTax.Columns[2].Width = 130;

            DataGridTax.Columns[3].Name = "Uid";
            DataGridTax.Columns[3].HeaderText = "Uid";
            DataGridTax.Columns[3].Visible = false;
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {

            mode = 1;
            txtBillAddress.Text = string.Empty;
            txtBillNo.Text = string.Empty;
            txtBillTo.Text = string.Empty;
            TotaltaxableValue.Text = string.Empty;
            txtTotalTaxValue.Text = string.Empty;
            txtRoundedOff.Text = string.Empty;
            txtNetValue.Text = string.Empty;
            DataGridItem.Rows.Clear();
            DataGridTax.Rows.Clear();
            txtBillNo.Tag = "0";
            LoadButton(2);
            btnBack.Visible = true;
            Genclass.Gendocno();
            txtBillNo.Text = Genclass.ST;
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            LoadFrontGrid();
            LoadButton(1);
            
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BtnOk_Click(object sender, EventArgs e)
        {
            try
            {
               
                 if (txtItemName.Text == string.Empty)
                {
                    MessageBox.Show("Item Name can't Empty", "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtItemName.Focus();
                    return;
                }
                else if(txtQty.Text == string.Empty)
                {
                    MessageBox.Show("Qty can't Empty", "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtQty.Focus();
                    return;
                }
                else if(txtRate.Text == string.Empty)
                {
                    MessageBox.Show("Rate can't Empty", "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtRate.Focus();
                    return;
                }
                else if(txtTotal.Text == string.Empty)
                {
                    MessageBox.Show("Total can't Empty", "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtTotal.Focus();
                    return;
                }
                else
                {
                    int Index = DataGridItem.Rows.Add();
                    DataGridViewRow dataGridViewRow = DataGridItem.Rows[Index];
                    dataGridViewRow.Cells[0].Value = cboacchead.Text;
                    dataGridViewRow.Cells[1].Value = txtItemName.Text;
                    dataGridViewRow.Cells[2].Value = txtsocno.Text;
                    dataGridViewRow.Cells[3].Value = txtUom.Text;
                    dataGridViewRow.Cells[4].Value = txtQty.Text;
                    dataGridViewRow.Cells[5].Value = txtRate.Text;
                    dataGridViewRow.Cells[6].Value = txtvalue.Text;
                    dataGridViewRow.Cells[7].Value = "0";
                    dataGridViewRow.Cells[8].Value = txtTaxPercentage.Text;
                    dataGridViewRow.Cells[9].Value = Genclass.sum5;
                    dataGridViewRow.Cells[10].Value = txtTotal.Text;
                    dataGridViewRow.Cells[11].Value = Genclass.sum4;

                    sum1 = sum1 + Convert.ToDouble(txtvalue.Text);
                    TotaltaxableValue.Text = sum1.ToString("#,0.00");
                    if (txtstid.Text == "196")
                    {

                        dis4 = Genclass.sum4 / 2;
                        dis3 = Genclass.sum5 / 2;
                    }
                    else
                    {
                        dis4 = Genclass.sum4;
                        dis3 = Genclass.sum5;
                    }
                    loadtaxcalculations();
                    txtItemName.Text = string.Empty;
                 
                    txtQty.Text = string.Empty;
                    txtRate.Text = string.Empty;
                    txtTotal.Text = string.Empty;
                    txtvalue.Text = string.Empty;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
        private void Titlegst()
        {
            if (txtstid.Text != "")
            {
                if (txtstid.Text == "196")
                {
                    DataGridTax.ColumnCount = 5;

                    DataGridTax.Columns[0].Name = "CGST%";
                    DataGridTax.Columns[1].Name = "CGST";

                    DataGridTax.Columns[2].Name = "SGST%";
                    DataGridTax.Columns[3].Name = "SGST";
                    DataGridTax.Columns[4].Name = "Total";


                    DataGridTax.Columns[0].Width = 70;
                    DataGridTax.Columns[1].Width = 70;
                    DataGridTax.Columns[2].Width = 70;
                    DataGridTax.Columns[3].Width = 70;
                    DataGridTax.Columns[4].Width = 70;

                }


                else
                {
                    DataGridTax.ColumnCount = 2;

                    DataGridTax.Columns[0].Name = "IGST%";
                    DataGridTax.Columns[1].Name = "IGST";
                    DataGridTax.Columns[0].Width = 180;
                    DataGridTax.Columns[1].Width = 170;

                }
            }
        }


        private void loadtaxcalculations()

        {

            



             
                sum5 = 0;
                df = 0;
                hg = 0;

                DataGridTax.Refresh();
                DataGridTax.DataSource = null;
                DataGridTax.Rows.Clear();
                txtigval.Text = "0";
                txtTotalTaxValue.Text = "0";

                Titlegst();
                for (int l = 0; l < DataGridItem.RowCount; l++)
                {

                    if (j != l)
                    {

                        dis4 = Convert.ToDouble(DataGridItem.Rows[l].Cells[11].Value);


                        dis3 = Convert.ToDouble(DataGridItem.Rows[l].Cells[6].Value) - (Convert.ToDouble(DataGridItem.Rows[l].Cells[6].Value) / 100) * (Convert.ToDouble(0));
                        dis3 = (dis3 / 100) * dis4;




                        if (txtstid.Text == "196")
                        {

                            if (mode == 2)


                            {
                                dis4 = Convert.ToDouble(DataGridItem.Rows[l].Cells[11].Value);
                                //dis3 = Convert.ToDouble(HFIT.Rows[l].Cells[11].Value);
                            }
                            dis4 = dis4 / 2;
                            dis3 = dis3 / 2;

                            int boo = 1;
                            if (DataGridTax.Rows.Count == 0)
                            {

                                var index1 = DataGridTax.Rows.Add();
                                DataGridTax.Rows[index1].Cells[0].Value = dis4.ToString("0.00");
                                DataGridTax.Rows[index1].Cells[2].Value = dis4.ToString("0.00");
                                DataGridTax.Rows[index1].Cells[1].Value = dis3.ToString("0.00");
                                DataGridTax.Rows[index1].Cells[3].Value = dis3.ToString("0.00");
                                DataGridTax.Rows[index1].Cells[4].Value = Convert.ToDouble(dis3.ToString("0.00")) * 2;
                            }
                            else
                            {
                                for (int k = 0; k < DataGridTax.Rows.Count; k++)
                                {
                                    if (Convert.ToDouble(DataGridTax.Rows[k].Cells[0].Value) == dis4)
                                    {

                                        dg = Convert.ToDouble(DataGridTax.Rows[k].Cells[1].Value) + dis3;
                                        DataGridTax.Rows[k].Cells[1].Value = dg.ToString("0.00");

                                        DataGridTax.Rows[k].Cells[3].Value = dg.ToString("0.00");
                                        dg1 = Convert.ToDouble(DataGridTax.Rows[k].Cells[4].Value) + (Convert.ToDouble(dis3.ToString("0.00")) * 2);
                                        DataGridTax.Rows[k].Cells[4].Value = dg1.ToString("0.00");
                                        boo = 1;
                                        break;
                                    }
                                    else
                                    {
                                        boo = 2;
                                    }
                                }
                            }

                            if (boo == 2)
                            {

                                var index1 = DataGridTax.Rows.Add();
                                DataGridTax.Rows[index1].Cells[0].Value = dis4.ToString("0.00");
                                DataGridTax.Rows[index1].Cells[2].Value = dis4.ToString("0.00");
                                DataGridTax.Rows[index1].Cells[1].Value = dis3.ToString("0.00");
                                DataGridTax.Rows[index1].Cells[3].Value = dis3.ToString("0.00");
                                DataGridTax.Rows[index1].Cells[4].Value = Convert.ToDouble(dis3.ToString("0.00")) * 2;
                            }

                            if (txtigval.Text == "")
                            {
                                txtigval.Text = "0";
                            }
                            df = Convert.ToDouble(txtigval.Text) + (Convert.ToDouble(dis3.ToString("0.00")) * 2);
                            txtigval.Text = df.ToString("0.00");
                            txtTotalTaxValue.Text = df.ToString("0.00");
                        }
                        else
                        {
                            int boo = 1;
                            if (DataGridTax.Rows.Count == 0)
                            {
                                var index1 = DataGridTax.Rows.Add();
                                DataGridTax.Rows[index1].Cells[0].Value = dis4.ToString("0.00");
                                DataGridTax.Rows[index1].Cells[1].Value = dis3.ToString("0.00");
                            }
                            else
                            {
                                for (int k = 0; k < DataGridTax.Rows.Count; k++)
                                {
                                    if (Convert.ToDouble(DataGridTax.Rows[k].Cells[0].Value) == dis4)
                                    {

                                        dg = Convert.ToDouble(DataGridTax.Rows[k].Cells[1].Value) + dis3;
                                        DataGridTax.Rows[k].Cells[1].Value = dg.ToString("0.00");


                                        boo = 1;
                                        break;
                                    }
                                    else
                                    {
                                        boo = 2;
                                    }
                                }
                            }

                            if (boo == 2)
                            {

                                var index1 = DataGridTax.Rows.Add();
                                DataGridTax.Rows[index1].Cells[0].Value = dis4.ToString("0.00");

                                DataGridTax.Rows[index1].Cells[1].Value = dis3.ToString("0.00");


                            }
                            if (txtigval.Text == "")
                            {
                                txtigval.Text = "0";
                            }
                            df = df + Convert.ToDouble(dis3.ToString("0.00"));
                            txtigval.Text = df.ToString("0.00");
                            txtTotalTaxValue.Text = df.ToString("0.00");
                        }
                    }


                    //txtigval.Text = hg.ToString("0.00");
                    //txttaxtot.Text = hg.ToString("0.00");
                    //double tot = Convert.ToDouble(txtexcise.Text) + Convert.ToDouble(txtigval.Text);
                    //txtttot.Text = tot.ToString("0.00");
                    net1 = Convert.ToDouble(TotaltaxableValue.Text) + Convert.ToDouble(txtTotalTaxValue.Text);
                    txtttot.Text = net1.ToString("0.00");
                    txtNetValue.Text = net1.ToString("0.00");
                    someInt = (int)net1;

                    rof = Math.Round(net1 - someInt, 2);
                txtRoundedOff.Text = rof.ToString("0.00");

                    if (Convert.ToDouble(txtRoundedOff.Text) < 0.49)
                    {
                        rof1 = -1 * Convert.ToDouble(txtRoundedOff.Text);
                    txtRoundedOff.Text = rof1.ToString("0.00");
                    }
                    else
                    {
                        rof2 = 1 - Convert.ToDouble(txtRoundedOff.Text);
                    txtRoundedOff.Text = rof2.ToString("0.00");
                    }

                    net = Convert.ToDouble(txtNetValue.Text) + Convert.ToDouble(txtRoundedOff.Text);
                    //int ne=Convert.ToInt16(net);
                    txtNetValue.Text = net.ToString("0.00");
                }
            
        }
        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if(txtBillNo.Text == string.Empty)
                {
                    MessageBox.Show("Bill Number Can't Empty", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtBillNo.Focus();
                    return;
                }
                else if (txtBillTo.Text == string.Empty)
                {
                    MessageBox.Show("Bill TO Can't Empty", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtBillTo.Focus();
                    return;
                }
                else if(DataGridItem.Rows.Count == 0)
                {
                    MessageBox.Show("Enter Item Details", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtstid.Focus();
                    return;
                }
                else
                {
                    if(txtBillNo.Tag == null)
                    {
                        txtBillNo.Tag = "0";
                    }
                    SqlParameter[] sqlparameters = {
                        new SqlParameter("@Uid",txtBillNo.Tag),
                        new SqlParameter("@BillNumber",txtBillNo.Text),
                        new SqlParameter("@BillDate",Convert.ToDateTime(DtpBillDate.Text)),
                        new SqlParameter("@CustomerUid",txtBillTo.Tag),
                        new SqlParameter("@TaxableValue",Convert.ToDecimal(TotaltaxableValue.Text)),
                        new SqlParameter("@TaxValue",Convert.ToDecimal(txtTotalTaxValue.Text)),
                        new SqlParameter("@TotalValue",Convert.ToDecimal(txtttot.Text)),
                        new SqlParameter("@Roff",Convert.ToDecimal(txtRoundedOff.Text)),
                        new SqlParameter("@NetValue",Convert.ToDecimal(txtNetValue.Text)),
                        new SqlParameter("@doctypeid",Genclass.Dtype),
                        new SqlParameter("@narration",txtnarra.Text),
                        new SqlParameter("@ReturnUid",SqlDbType.BigInt)
                    };
                    sqlparameters[11].Direction = ParameterDirection.Output;
                    int PUid = db.ExecuteQuery(CommandType.StoredProcedure, "Proc_GenBillM", sqlparameters, 11);

                    for (int i = 0; i < DataGridItem.Rows.Count; i++)
                    {
                        decimal t = Convert.ToDecimal(DataGridItem.Rows[i].Cells[6].Value.ToString());
                        decimal Q = Convert.ToDecimal(DataGridItem.Rows[i].Cells[4].Value.ToString());
                        SqlParameter[] sqlparametersItem = {
                            new SqlParameter("@Uid",DataGridItem.Rows[i].Cells[7].Value.ToString()),
                            new SqlParameter("@PUid",PUid),
                            new SqlParameter("@acchead",DataGridItem.Rows[i].Cells[0].Value.ToString()),
                            new SqlParameter("@ItemName",DataGridItem.Rows[i].Cells[1].Value.ToString()),
                           
                            new SqlParameter("@Uom",DataGridItem.Rows[i].Cells[3].Value.ToString()),
                            new SqlParameter("@MRPRate",Convert.ToDecimal(DataGridItem.Rows[i].Cells[5].Value.ToString())),
                            new SqlParameter("@Qty",DataGridItem.Rows[i].Cells[4].Value.ToString()),
                            new SqlParameter("@Rate",DataGridItem.Rows[i].Cells[5].Value.ToString()),
                            new SqlParameter("@TaxableValue",DataGridItem.Rows[i].Cells[6].Value.ToString()),
                            new SqlParameter("@tax",DataGridItem.Rows[i].Cells[11].Value.ToString()),
                            new SqlParameter("@taxValue",DataGridItem.Rows[i].Cells[9].Value.ToString()),
                            new SqlParameter("@TotalValue",DataGridItem.Rows[i].Cells[10].Value.ToString()),
                             new SqlParameter("@socno",DataGridItem.Rows[i].Cells[2].Value.ToString()),
                        };
                        db.ExecuteQuery(CommandType.StoredProcedure, "Proc_GenBillD", sqlparametersItem);
                    }
                    if (mode == 1)
                    {
                        conn.Close();
                        conn.Open();
                        qur.CommandText = "update doctypem set lastno= lastno + 1 where doctypeid=" + Genclass.Dtype + " and finyear='19-20'";
                        qur.ExecuteNonQuery();
                    }


                    MessageBox.Show("record hasbeen saved Successfully", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtBillAddress.Text = string.Empty;
                    txtBillNo.Text = string.Empty;
                    txtBillTo.Text = string.Empty;
                    TotaltaxableValue.Text = string.Empty;
                    txtTotalTaxValue.Text = string.Empty;
                    txtRoundedOff.Text = string.Empty;
                    txtNetValue.Text = string.Empty;
                    DataGridItem.Rows.Clear();
                    DataGridTax.Rows.Clear();

                    LoadButton(1);
                    LoadFrontGrid();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void BtnTaxOk_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtTotalValue.Text == string.Empty)
                {
                    MessageBox.Show("taxble Value can't Empty", "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtTotalValue.Focus();
                    return;
                }
                else if (txtTaxPercentage.SelectedIndex == -1)
                {
                    MessageBox.Show("tax percentage can't Empty", "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtTaxPercentage.Focus();
                    return;
                }
                else if (txtTaxValue.Text == string.Empty)
                {
                    MessageBox.Show("Tax value can't Empty", "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtTaxValue.Focus();
                    return;
                }
                else
                {
                    int Index = DataGridTax.Rows.Add();
                    DataGridViewRow dataGridViewRow = DataGridTax.Rows[Index];
                    dataGridViewRow.Cells[0].Value = txtTotalValue.Text;
                    dataGridViewRow.Cells[1].Value = txtTaxPercentage.SelectedValue;
                    dataGridViewRow.Cells[2].Value = txtTaxValue.Text;
                    dataGridViewRow.Cells[3].Value = "0";
                    TextboxValue();
                    LoadAmountDetails();
                    txtTotalValue.Text = string.Empty;
                    txtTaxPercentage.SelectedIndex = -1;
                    txtTaxValue.Text = string.Empty;
                    txtTotalValue.Focus();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected void TextboxValue()
        {
            try
            {
                decimal totalBasicValue = 0;
                decimal TaxValue = 0;
                for (int i = 0; i < DataGridTax.Rows.Count; i++)
                {
                    totalBasicValue += Convert.ToDecimal(DataGridTax.Rows[i].Cells[0].Value.ToString());
                    TaxValue += Convert.ToDecimal(DataGridTax.Rows[i].Cells[2].Value.ToString());
                }
                TotaltaxableValue.Text = totalBasicValue.ToString("0.00");
                txtTotalTaxValue.Text = TaxValue.ToString("0.00");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void LoadAmountDetails()
        {
            try
            {
                if (txtTotalTaxValue.Text == string.Empty )
                {
                    txtTotalTaxValue.Text = "0";
                }
                else if (TotaltaxableValue.Text == string.Empty)
                {
                    TotaltaxableValue.Text = "0";
                }
                else
                {
                    decimal basicAmount = Convert.ToDecimal(TotaltaxableValue.Text);
                    decimal TaxAmount = Convert.ToDecimal(txtTotalTaxValue.Text);
                    double a = Convert.ToDouble(basicAmount + TaxAmount);
                    string[] str = a.ToString("0.00").Split('.');
                    double num1 = Convert.ToDouble("0." + str[1]);
                    double res;
                    if (num1 < 0.51)
                    {
                        res = Math.Floor(a);
                    }
                    else
                    {
                        res = Math.Round(a);
                    }
                    txtRoundedOff.Text = (res - a).ToString("0.00");
                    txtNetValue.Text = res.ToString("0.00");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected DataTable getParty()
        {
            DataTable dt = new DataTable();
            try
            {



                dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "sp_getorderno", conn);
                bsc.DataSource = dt;




            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            return dt;
        }

        private void txtBillTo_Click(object sender, EventArgs e)
        {
            try
            {
                Genclass.type = 1;
                string Query = "Select Uid,Name from SUPPLIERM Where  active=1";
                DataTable dataTable = db.GetData2(CommandType.Text, Query);
                bsBillFrom.DataSource = dataTable;
                FillDtataGrid(dataTable, 1);
                Point point = FindLocation(txtBillTo);
                GrSearch.Location = new Point(point.X, point.Y + 20);
                GrSearch.Visible = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
        int FillId = 0;
        private void FillDtataGrid(DataTable dataTable, int v)
        {
            try
            {
                if(v == 1)
                {
                    FillId = 1;
                    DataGridCommon.DataSource = null;
                    DataGridCommon.AutoGenerateColumns = false;
                    DataGridCommon.ColumnCount = 3;
                    DataGridCommon.Columns[0].Name = "Uid";
                    DataGridCommon.Columns[0].HeaderText = "Uid";
                    DataGridCommon.Columns[0].DataPropertyName = "Uid";
                    DataGridCommon.Columns[0].Visible = false;

                    DataGridCommon.Columns[1].Name = "Name";
                    DataGridCommon.Columns[1].HeaderText = "Name";
                    DataGridCommon.Columns[1].DataPropertyName = "Name";
                    DataGridCommon.Columns[1].Width = 250;

                    DataGridCommon.Columns[2].Name = "Code";
                    DataGridCommon.Columns[2].HeaderText = "Code";
                    DataGridCommon.Columns[2].DataPropertyName = "Code";
                    DataGridCommon.DataSource = bsBillFrom;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private Point FindLocation(Control ctrl)
        {
            if (ctrl.Parent is Form)
                return ctrl.Location;
            else
            {
                Point p = FindLocation(ctrl.Parent);
                p.X += ctrl.Location.X;
                p.Y += ctrl.Location.Y;
                return p;
            }
        }
        int SelectId = 0;
        private void btnSelect_Click(object sender, EventArgs e)
        {
            try
            {
                SelectId = 1;
                int Index = DataGridCommon.SelectedCells[0].RowIndex;
                if (Genclass.type == 1)
                {
                    if (FillId == 1)
                    {
                        txtBillTo.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                        txtBillTo.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                        loadsuppadd();
                    }
                }
                else if (Genclass.type == 2)

                {
                    txtsocno.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtsocno.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();

                }

                GrSearch.Visible = false;
                SelectId = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }


        private void loadsuppadd()
        {


            if (txtBillTo.Tag != null)
            {
                string strsql = "Select address1, address2 ,city,stateuid  from supplierm where uid=" + txtBillTo.Tag + " ";
                SqlCommand cmd = new SqlCommand(strsql, conn);
                SqlDataAdapter aptr1 = new SqlDataAdapter(cmd);
                DataTable tap1 = new DataTable();
                aptr1.Fill(tap1);

                if (tap1.Rows.Count > 0)
                {
                    txtBillAddress.Text = tap1.Rows[0]["address1"].ToString() + ' ' + tap1.Rows[0]["address2"].ToString() + ' ' + tap1.Rows[0]["city"].ToString();
                 
                    txtstid.Text = tap1.Rows[0]["stateuid"].ToString();
                }
            }
        }
        private void DataGridCommon_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            btnSelect_Click(sender, e);
        }

        private void btnGridClose_Click(object sender, EventArgs e)
        {
            GrSearch.Visible = false;
        }

        private void txtBillTo_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if(SelectId == 0)
                {
                    bsBillFrom.Filter = string.Format("Name Like '%{0}%'", txtBillTo.Text);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void TxtTaxPercentage_TextChanged(object sender, EventArgs e)
        {
            if (txtTaxPercentage.Text != string.Empty)
            {

                string qur = "select Generalname as type,guid,f1 AS  F2 from generalm where   Typemuid in (8)  and active=1  and generalname ='" + txtTaxPercentage.Text + "'  ";
                SqlCommand cmd = new SqlCommand(qur, conn);
                SqlDataAdapter apt = new SqlDataAdapter(cmd);
                DataTable tab = new DataTable();
                apt.Fill(tab);

                if (tab.Rows.Count > 0 && txtvalue.Text!=String.Empty)
                {
                    decimal cc = Convert.ToDecimal(txtvalue.Text);
                    decimal dd = Convert.ToDecimal(tab.Rows[0]["f2"].ToString());

                    decimal ee = (cc * dd) / 100;
                    Genclass.sum5 = Convert.ToDouble(ee);
                    Genclass.sum4 = Convert.ToDouble(dd);
                    decimal ff = cc + ee;
                    txtTotal.Text = ff.ToString();
                }


            }
        }

        private void DataGridItem_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if(e.KeyCode == Keys.Delete)
                {
                    DialogResult dialogResult = MessageBox.Show("Do you want to delete this record !", "Information", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                    if(dialogResult == DialogResult.Yes)
                    {
                        int Index = DataGridItem.SelectedCells[0].RowIndex;
                        int Uid = Convert.ToInt32(DataGridItem.Rows[Index].Cells[6].Value.ToString());
                        if (Uid == 0)
                        {
                            DataGridItem.Rows.RemoveAt(Index);
                            DataGridItem.ClearSelection();
                            DataGridTax.Rows.Clear();
                        }
                        else
                        {
                            string Query = "Delete from PurchaseList Where Uid =" + Uid + "";
                            //db.ExecuteQuery(CommandType.Text, Query);
                            DataGridItem.Rows.RemoveAt(Index);
                            DataGridItem.ClearSelection();
                            DataGridTax.Rows.Clear();
                        }
                    }
                    else
                    {
                       
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void DataGridTax_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Delete)
                {
                    DialogResult dialogResult = MessageBox.Show("Do you want to delete this record !", "Information", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                    if (dialogResult == DialogResult.Yes)
                    {
                        int Index = DataGridTax.SelectedCells[0].RowIndex;
                        int Uid = Convert.ToInt32(DataGridTax.Rows[Index].Cells[3].Value.ToString());
                        if (Uid == 0)
                        {
                            DataGridTax.Rows.RemoveAt(Index);
                            DataGridTax.ClearSelection();
                        }
                        else
                        {
                            string Query = "Delete from PurchaseTax Where Uid =" + Uid + "";
                            //db.ExecuteQuery(CommandType.Text, Query);
                            DataGridTax.Rows.RemoveAt(Index);
                            DataGridTax.ClearSelection();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                int Index = DataGridPurchase.SelectedCells[0].RowIndex;
                int Uid = Convert.ToInt32(DataGridPurchase.Rows[Index].Cells[0].Value.ToString());
                SqlParameter[] sqlParameters = { new SqlParameter("@Uid", Uid) };
                db.ExecuteQuery(CommandType.StoredProcedure, "Proc_DeleteGenBill", sqlParameters);
                MessageBox.Show("Deleted Successfully", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);

                LoadFrontGrid();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void DataGridPurchase_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if(e.KeyCode == Keys.Delete)
                {
                    btnDelete_Click(sender, e);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void BtnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                mode = 2;
                SelectId = 1;
                txtBillAddress.Text = string.Empty;
                txtBillNo.Text = string.Empty;
                txtBillTo.Text = string.Empty;
                TotaltaxableValue.Text = string.Empty;
                txtTotalTaxValue.Text = string.Empty;
                txtRoundedOff.Text = string.Empty;
                txtNetValue.Text = string.Empty;
                DataGridItem.Rows.Clear();
                DataGridTax.Rows.Clear();
                btnBack.Visible = true;
                int Index = DataGridPurchase.SelectedCells[0].RowIndex;
                int Uid = Convert.ToInt32(DataGridPurchase.Rows[Index].Cells[0].Value.ToString());
                SqlParameter[] sqlParameters = {
                    new SqlParameter("@Uid",Uid)
                };
                DataSet dataSet = db.GetMultipleData(CommandType.StoredProcedure, "Proc_EditGenBllAcc", sqlParameters);
                DataTable dtPurchase = dataSet.Tables[0];
                DataTable dtPurchaseList = dataSet.Tables[1];


                txtBillNo.Text = dtPurchase.Rows[0]["BillNumber"].ToString();
                txtBillNo.Tag = dtPurchase.Rows[0]["Uid"].ToString();
                DtpBillDate.Text = dtPurchase.Rows[0]["BillDate"].ToString();
                txtBillTo.Text = dtPurchase.Rows[0]["Name"].ToString();
                txtBillTo.Tag = dtPurchase.Rows[0]["CustomerUid"].ToString();
                txtBillAddress.Text = dtPurchase.Rows[0]["Address1"].ToString() + "," + dtPurchase.Rows[0]["Address2"].ToString() + "," + dtPurchase.Rows[0]["City"].ToString() + " State :" + dtPurchase.Rows[0]["Ste"].ToString();
                TotaltaxableValue.Text = dtPurchase.Rows[0]["TaxableValue"].ToString();
                txtTotalTaxValue.Text = dtPurchase.Rows[0]["TaxValue"].ToString();
                txtttot.Text = dtPurchase.Rows[0]["totalvalue"].ToString();
                txtRoundedOff.Text = dtPurchase.Rows[0]["Roff"].ToString();
                txtNetValue.Text = dtPurchase.Rows[0]["NetValue"].ToString();
                txtnarra.Text = dtPurchase.Rows[0]["Narration"].ToString();
                loadsuppadd();
                DataGridItem.Rows.Clear();
                sum1 = 0;
                for (int i = 0; i < dtPurchaseList.Rows.Count; i++)
                {
                    int Index1 = DataGridItem.Rows.Add();
                    DataGridViewRow dataGridViewRow = DataGridItem.Rows[Index1];
                    dataGridViewRow.Cells[0].Value = dtPurchaseList.Rows[i]["Acchead"].ToString();
                    dataGridViewRow.Cells[1].Value = dtPurchaseList.Rows[i]["ItemName"].ToString();
                    dataGridViewRow.Cells[2].Value = dtPurchaseList.Rows[i]["Socno"].ToString();
                    dataGridViewRow.Cells[3].Value = dtPurchaseList.Rows[i]["Uom"].ToString();
                    dataGridViewRow.Cells[4].Value = dtPurchaseList.Rows[i]["Qty"].ToString();
                    dataGridViewRow.Cells[5].Value = dtPurchaseList.Rows[i]["MRPRate"].ToString();
                    dataGridViewRow.Cells[6].Value = dtPurchaseList.Rows[i]["taxablevalue"].ToString();
                    dataGridViewRow.Cells[7].Value = dtPurchaseList.Rows[i]["Uid"].ToString();
                    dataGridViewRow.Cells[8].Value = dtPurchaseList.Rows[i]["tax"].ToString();
                    dataGridViewRow.Cells[9].Value = dtPurchaseList.Rows[i]["taxvalue"].ToString();
                    dataGridViewRow.Cells[10].Value = dtPurchaseList.Rows[i]["TotalValue"].ToString();
                    dataGridViewRow.Cells[11].Value = dtPurchaseList.Rows[i]["tax"].ToString();
                    sum1 = sum1 + Convert.ToDouble(dtPurchaseList.Rows[i]["taxablevalue"].ToString());
                    TotaltaxableValue.Text = sum1.ToString("#,0.00");
                    if (txtstid.Text == "196")
                    {
                        dis4 = Convert.ToDouble(dtPurchaseList.Rows[i]["tax"].ToString()) * 2;
                        dis3 = Convert.ToDouble(dtPurchaseList.Rows[i]["taxvalue"].ToString()) * 2;
                    }
                    else
                    {
                        dis4 = Convert.ToDouble(dtPurchaseList.Rows[i]["tax"].ToString());
                        dis3 = Convert.ToDouble(dtPurchaseList.Rows[i]["taxvalue"].ToString());
                    }
                    loadtaxcalculations();
                }
      

         
              
       
  
                SelectId = 0;
                LoadButton(2);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected void LoadFrontGrid()
        {
            try
            {
                SqlParameter[] sqlParameters = {
                    new SqlParameter("@Uid","0")
                };
                DataTable dataTable = db.GetData(CommandType.StoredProcedure, "Proc_EditGenBllAcc", sqlParameters);
                DataGridPurchase.DataSource = null;
                DataGridPurchase.AutoGenerateColumns = false;
                DataGridPurchase.ColumnCount = 5;

                DataGridPurchase.Columns[0].Name = "Uid";
                DataGridPurchase.Columns[0].HeaderText = "Uid";
                DataGridPurchase.Columns[0].DataPropertyName = "Uid";
                DataGridPurchase.Columns[0].Visible = false;

                DataGridPurchase.Columns[1].Name = "BillNumber";
                DataGridPurchase.Columns[1].HeaderText = "Bill Number";
                DataGridPurchase.Columns[1].DataPropertyName = "BillNumber";
                DataGridPurchase.Columns[1].Width = 120;

                DataGridPurchase.Columns[2].Name = "BillDate";
                DataGridPurchase.Columns[2].HeaderText = "Bill Date";
                DataGridPurchase.Columns[2].DataPropertyName = "BillDate";

                DataGridPurchase.Columns[3].Name = "Name";
                DataGridPurchase.Columns[3].HeaderText = "Name";
                DataGridPurchase.Columns[3].DataPropertyName = "Name";
                DataGridPurchase.Columns[3].Width = 700;

                DataGridPurchase.Columns[4].Name = "NetValue";
                DataGridPurchase.Columns[4].HeaderText = "Net Value";
                DataGridPurchase.Columns[4].DataPropertyName = "NetValue";
                DataGridPurchase.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                DataGridPurchase.DataSource = dataTable;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtTaxPercentage_SelectedIndexChanged(object sender, EventArgs e)
        {
            TxtTaxPercentage_TextChanged(sender, e);
        }

        private void txtQty_TextChanged(object sender, EventArgs e)
        {
            if(txtRate.Text!=string.Empty)
            {

                if(txtQty.Text != string.Empty)
                {

                    decimal aa = Convert.ToDecimal(txtQty.Text);
                    decimal bb = Convert.ToDecimal(txtRate.Text);
                    decimal cc = aa * bb;
                    txtvalue.Text = cc.ToString();

                    if(txtTaxPercentage.Text!= string.Empty)
                    {

                        string qur = "select Generalname as type,guid,f1 AS    from generalm where   Typemuid in (8)  and active=1  and generalname ='" + txtTaxPercentage.Text+ "'  ";
                        SqlCommand cmd = new SqlCommand(qur, conn);
                        SqlDataAdapter apt = new SqlDataAdapter(cmd);
                        DataTable tab = new DataTable();
                        apt.Fill(tab);

                        if(tab.Rows.Count>0)
                        {

                            decimal dd = Convert.ToDecimal(tab.Rows[0]["f2"].ToString());
                            Genclass.sum4 = Convert.ToDouble(dd);
                            decimal ee = (cc * dd) / 100;

                            decimal ff = cc + ee;
                            Genclass.sum5 = Convert.ToDouble(ee);
                            txtTotal.Text = ff.ToString();
                        }


                    }

                }
            }
        }

        private void txtRate_TextChanged(object sender, EventArgs e)
        {
            if (txtRate.Text != string.Empty)
            {

                if (txtQty.Text != string.Empty)
                {

                    decimal aa = Convert.ToDecimal(txtQty.Text);
                    decimal bb = Convert.ToDecimal(txtRate.Text);
                    decimal cc = aa * bb;
                    txtvalue.Text = cc.ToString();

                    if (txtTaxPercentage.Text != string.Empty)
                    {

                        string qur = "select Generalname as type,guid,f1 AS F2    from generalm where   Typemuid in (8)  and active=1  and generalname ='" + txtTaxPercentage.Text + "'  ";
                        SqlCommand cmd = new SqlCommand(qur, conn);
                        SqlDataAdapter apt = new SqlDataAdapter(cmd);
                        DataTable tab = new DataTable();
                        apt.Fill(tab);

                        if (tab.Rows.Count > 0)
                        {
                            decimal dd = Convert.ToDecimal(tab.Rows[0]["f2"].ToString());
                            Genclass.sum4 = Convert.ToDouble(dd);
                            decimal ee = (cc * dd) / 100;

                            decimal ff = cc + ee;
                            Genclass.sum5 = Convert.ToDouble(ee);
                            txtTotal.Text = ff.ToString();
                        }


                    }
                }
            }
        }

        private void label20_Click(object sender, EventArgs e)
        {

        }

        private void DataGridCommon_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void txtsocno_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bsFabric.Filter = string.Format("Itemname LIKE '%{0}%' ", txtsocno.Text);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtsocno_KeyDown(object sender, KeyEventArgs e)
        {

        }

        private void txtsocno_Click(object sender, EventArgs e)
        {
            Genclass.type = 2;
            DataTable dt = getParty();
            bsc.DataSource = dt;
            FillGrid1(dt, 1);
            Point loc = FindLocation(txtsocno);
            GrSearch.Location = new Point(loc.X, loc.Y + 20);
            GrSearch.Visible = true;
        }
    }
}