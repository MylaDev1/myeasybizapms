﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Drawing.Imaging;
using System.Configuration;
using System.Data.SqlClient;
using System.Diagnostics;
using excel = Microsoft.Office.Interop.Excel;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
namespace MyEasyBizAPMS
{
    public partial class FrmGJOR : Form
    {
        private CrystalDecisions.Windows.Forms.CrystalReportViewer Cryview;

        ReportDocument doc = new ReportDocument();
        public FrmGJOR()
        {
            this.BackColor = Color.White; ;
            InitializeComponent();
        }

        int SP;
        int i = 0;
        double sum1;
        double sum2;
        int h = 0;
        int kk = 0;
        int k = 0;
        string uid = "";
        int mode = 0;
        string tpuid = "";
        SqlConnection conn = new SqlConnection(GeneralParameters.ConnectionString);
        SqlCommand qur = new SqlCommand();
        private DataRow doc1;
        DataTable Docno = new DataTable();
        BindingSource bs = new BindingSource();
        BindingSource bsc = new BindingSource();
        BindingSource bsp = new BindingSource();
        BindingSource bsserial = new BindingSource();
        BindingSource bsc1 = new BindingSource();
        BindingSource bsc2 = new BindingSource();

        SQLDBHelper db = new SQLDBHelper();
        int Fillid;
        BindingSource bsParty = new BindingSource();
        string str1key;
        public int SelectId = 0;
        private void FrmGJOR_Load(object sender, EventArgs e)
        {
            qur.Connection = conn;
            Genclass.buttonstyleform(this);
            Genclass.buttonstylepanel(panadd);
            panadd.Visible = true;
            Genpan.Visible = true;
            Editpnl.Visible = false;

            this.HFGP.DefaultCellStyle.Font = new Font("calibri", 10);
            this.HFGP.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 10, FontStyle.Bold);
            this.HFIT.DefaultCellStyle.Font = new Font("calibri", 10);
            this.HFIT.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 10, FontStyle.Bold);

            this.HFGP1.DefaultCellStyle.Font = new Font("calibri", 10);
            this.HFGP1.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 10, FontStyle.Bold);

            this.HFGP2.DefaultCellStyle.Font = new Font("calibri", 10);
            this.HFGP2.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 10, FontStyle.Bold);

            HFIT.RowHeadersVisible = false;
            HFGP.RowHeadersVisible = false;
            HFGP2.RowHeadersVisible = false;
            HFGP1.RowHeadersVisible = false;




            HFGP2.EnableHeadersVisualStyles = false;
            HFGP2.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.GradientActiveCaption;

            HFIT.EnableHeadersVisualStyles = false;
            HFIT.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.GradientActiveCaption;
            HFGP1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            HFGP1.EnableHeadersVisualStyles = false;
            HFGP1.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.GradientActiveCaption;

            HFGP.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            HFGP.EnableHeadersVisualStyles = false;
            HFGP.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.GradientActiveCaption;
            if (Text == "Fabric Process GRN")
            {


                Genclass.Dtype = 570;
            }
            else if (GeneralParametrs.MenyKey == 44)
            {

                Genclass.Dtype = 590;
            }
            else if (GeneralParametrs.MenyKey == 46)
            {
                Genclass.Dtype = 610;

            }
            else if (GeneralParametrs.MenyKey == 48)

            {

                Genclass.Dtype = 630;

            }
            else if (Text == "Yarn Process GRN")

            {

                Genclass.Dtype = 220;

            }
            else if (GeneralParametrs.MenyKey == 61)

            {

                Genclass.Dtype = 960;


            }

            dtpfnt.Format = DateTimePickerFormat.Custom;
            dtpfnt.CustomFormat = "dd/MM/yyyy";
            dtpgrndt.Format = DateTimePickerFormat.Custom;
            dtpgrndt.CustomFormat = "dd/MM/yyyy";
            dtpsupp.Format = DateTimePickerFormat.Custom;
            dtpsupp.CustomFormat = "dd/MM/yyyy";
            chkact.Checked = true;
            type1();
            tax();

            LoadGetJobCard(1);
            Titlep();
        }
        public void tax()
        {
            conn.Open();
            string qur = "select Generalname as tax, guid as uid from generalm where   Typemuid in (7)  and active=1 ";
            SqlCommand cmd = new SqlCommand(qur, conn);
            SqlDataAdapter apt = new SqlDataAdapter(cmd);
            DataTable tab = new DataTable();
            apt.Fill(tab);
            cbouom.DataSource = null;
            cbouom.DataSource = tab;
            cbouom.DisplayMember = "tax";
            cbouom.ValueMember = "uid";
            cbouom.SelectedIndex = -1;
            conn.Close();
        }
        public void type1()
        {
            conn.Open();
            string qur = "select Generalname as type,guid as uid from generalm where   Typemuid in (40)   and active=1 ";
            SqlCommand cmd = new SqlCommand(qur, conn);
            SqlDataAdapter apt = new SqlDataAdapter(cmd);
            DataTable tab = new DataTable();
            apt.Fill(tab);
            cbotype.DataSource = null;
            cbotype.DataSource = tab;
            cbotype.DisplayMember = "type";
            cbotype.ValueMember = "uid";
            cbotype.SelectedIndex = -1;
            conn.Close();
        }


        protected DataTable LoadGetJobCard(int tag)
        {


            chkact.Checked = true;
            if (chkact.Checked == true)
            {
                SP = 1;

            }
            else
            {
                SP = 0;

            }


            DataTable dt = new DataTable();
            try
            {

                SqlParameter[] para = {
                    new SqlParameter("@active",SP),
                      new SqlParameter("@DOCTYPEID",Genclass.Dtype),


                };

                dt = db.GetData(CommandType.StoredProcedure, "SP_GETJRLOAD", para);


                LoadDataTable(dt);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        protected void LoadDataTable(DataTable dt)
        {
            try
            {
                HFGP.DataSource = null;
                HFGP.AutoGenerateColumns = false;
                HFGP.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
                HFGP.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
                HFGP.ColumnCount = 12;
                HFGP.Columns[0].Name = "Uid";
                HFGP.Columns[0].HeaderText = "Uid";
                HFGP.Columns[0].DataPropertyName = "Uid";

                HFGP.Columns[1].Name = "DOcno";
                HFGP.Columns[1].HeaderText = "Docno";
                HFGP.Columns[1].DataPropertyName = "DOcno";

                HFGP.Columns[2].Name = "Docdate";
                HFGP.Columns[2].HeaderText = "Docdate";
                HFGP.Columns[2].DataPropertyName = "Docdate";

                HFGP.Columns[3].Name = "name";
                HFGP.Columns[3].HeaderText = "name";
                HFGP.Columns[3].DataPropertyName = "name";


                HFGP.Columns[4].Name = "SocNO";
                HFGP.Columns[4].HeaderText = "SocNO";
                HFGP.Columns[4].DataPropertyName = "SocNO";

                HFGP.Columns[5].Name = "Reference";
                HFGP.Columns[5].HeaderText = "Reference";
                HFGP.Columns[5].DataPropertyName = "Reference";

                HFGP.Columns[6].Name = "WorkOrderNo";
                HFGP.Columns[6].HeaderText = "WorkOrderNo";
                HFGP.Columns[6].DataPropertyName = "WorkOrderNo";

                HFGP.Columns[7].Name = "ProcessName";
                HFGP.Columns[7].HeaderText = "ProcessName";
                HFGP.Columns[7].DataPropertyName = "ProcessName";

                HFGP.Columns[8].Name = "supplieruid";
                HFGP.Columns[8].HeaderText = "supplieruid";
                HFGP.Columns[8].DataPropertyName = "supplieruid";

                HFGP.Columns[9].Name = "refid";
                HFGP.Columns[9].HeaderText = "refid";
                HFGP.Columns[9].DataPropertyName = "refid";

                HFGP.Columns[10].Name = "vehicleno";
                HFGP.Columns[10].HeaderText = "vehicleno";
                HFGP.Columns[10].DataPropertyName = "vehicleno";


                HFGP.Columns[11].Name = "mode";
                HFGP.Columns[11].HeaderText = "mode";
                HFGP.Columns[11].DataPropertyName = "mode";



                bs.DataSource = dt;

                HFGP.DataSource = bs;

                HFGP.Columns[0].Visible = false;
                HFGP.Columns[1].Width = 90;
                HFGP.Columns[2].Width = 90;
                HFGP.Columns[3].Width = 250;

                HFGP.Columns[4].Width = 150;
                HFGP.Columns[5].Width = 150;
                HFGP.Columns[6].Width = 150;
                HFGP.Columns[7].Visible = false;
                HFGP.Columns[8].Visible = false;
                HFGP.Columns[9].Visible = false;
                HFGP.Columns[10].Visible = false;
                HFGP.Columns[11].Visible = false;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void Loadgrid()
        {
            try
            {
                conn.Close();
                conn.Open();
                Genclass.StrSrch = "";
                Genclass.FSSQLSortStr = "Docno";
                Genclass.FSSQLSortStr1 = "Docdate";
                Genclass.FSSQLSortStr2 = "Name";
                Genclass.FSSQLSortStr3 = "vehicleno";
                Genclass.FSSQLSortStr4 = "placeofsupply";
                Genclass.FSSQLSortStr5 = "Dcno";
                if (txtscr1.Text != "")
                {
                    if (Genclass.StrSrch == "")
                    {
                        Genclass.StrSrch = Genclass.FSSQLSortStr + " like '%" + txtscr1.Text + "%'";
                    }
                    else
                    {
                        Genclass.StrSrch = Genclass.StrSrch + " and " + Genclass.FSSQLSortStr + " like '%" + txtscr1.Text + "%'";
                    }
                }

                if (Txtscr2.Text != "")
                {
                    if (Genclass.StrSrch == "")
                    {
                        Genclass.StrSrch = Genclass.FSSQLSortStr1 + " like '%" + Txtscr2.Text + "%'";
                    }
                    else
                    {
                        Genclass.StrSrch = Genclass.StrSrch + " and " + Genclass.FSSQLSortStr1 + " like '%" + Txtscr2.Text + "%'";
                    }
                }

                if (txtscr5.Text != "")
                {
                    if (Genclass.StrSrch == "")
                    {
                        Genclass.StrSrch = Genclass.FSSQLSortStr2 + " like '%" + txtscr5.Text + "%'";
                    }
                    else
                    {
                        Genclass.StrSrch = Genclass.StrSrch + " and " + Genclass.FSSQLSortStr2 + " like '%" + txtscr5.Text + "%'";
                    }
                }

                if (txtscr6.Text != "")
                {
                    if (Genclass.StrSrch == "")
                    {
                        Genclass.StrSrch = Genclass.FSSQLSortStr3 + " like '%" + txtscr6.Text + "%'";
                    }
                    else
                    {
                        Genclass.StrSrch = Genclass.StrSrch + " and " + Genclass.FSSQLSortStr3 + " like '%" + txtscr6.Text + "%'";
                    }
                }
                if (textBox1.Text != "")
                {
                    if (Genclass.StrSrch == "")
                    {
                        Genclass.StrSrch = Genclass.FSSQLSortStr4 + " like '%" + textBox1.Text + "%'";
                    }
                    else
                    {
                        Genclass.StrSrch = Genclass.StrSrch + " and " + Genclass.FSSQLSortStr4 + " like '%" + textBox1.Text + "%'";
                    }
                }
                if (textBox2.Text != "")
                {
                    if (Genclass.StrSrch == "")
                    {
                        Genclass.StrSrch = Genclass.FSSQLSortStr5 + " like '%" + textBox2.Text + "%'";
                    }
                    else
                    {
                        Genclass.StrSrch = Genclass.StrSrch + " and " + Genclass.FSSQLSortStr5 + " like '%" + textBox2.Text + "%'";
                    }
                }
                if (txtscr1.Text != "")
                {
                    Genclass.StrSrch = " " + Genclass.StrSrch;
                }
                else if (Txtscr2.Text != "")
                {
                    Genclass.StrSrch = " " + Genclass.StrSrch;
                }
                else if (txtscr5.Text != "")
                {
                    Genclass.StrSrch = " " + Genclass.StrSrch;
                }
                else if (txtscr6.Text != "")
                {
                    Genclass.StrSrch = " " + Genclass.StrSrch;
                }
                else if (textBox1.Text != "")
                {
                    Genclass.StrSrch = " " + Genclass.StrSrch;
                }
                else if (textBox2.Text != "")
                {
                    Genclass.StrSrch = " " + Genclass.StrSrch;
                }
                else
                {
                    Genclass.StrSrch = "a.uid <> 0";
                }
                DateTime str9 = Convert.ToDateTime(dtpfnt.Text);

                if (chkact.Checked == true)
                {
                    string quy = "select a.uid,DOcno,DocDate as Docdate,B.name,Dcno as Reference,a.Remarks as WorkOrderNo,a.placeofsupply as ProcessName,a.supplieruid,a.refid,a.vehicleno,a.mode  from jo a inner join partym b on a.supplieruid=b.uid where doctypeid=" + Genclass.Dtype + "  and a.active=1  and  " + Genclass.StrSrch + "  order by docno desc";
                    Genclass.cmd = new SqlCommand(quy, conn);
                }
                else
                {
                    string quy = "select a.uid,DOcno,DocDate as Docdate,B.name,Dcno as Reference,a.Remarks as WorkOrderNo,a.placeofsupply as ProcessName,a.supplieruid,a.refid,a.vehicleno,a.mode   from jo a inner join partym b on a.supplieruid=b.uid where doctypeid=" + Genclass.Dtype + "  and a.active=1  and  " + Genclass.StrSrch + "  order by docno desc";
                    Genclass.cmd = new SqlCommand(quy, conn);
                }


                SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
                DataTable tap = new DataTable();
                aptr.Fill(tap);
                HFGP.AutoGenerateColumns = false;
                HFGP.Refresh();
                HFGP.DataSource = null;
                HFGP.Rows.Clear();
                HFGP.ColumnCount = tap.Columns.Count;
                i = 0;
                foreach (DataColumn column in tap.Columns)
                {
                    HFGP.Columns[i].Name = column.ColumnName;
                    HFGP.Columns[i].HeaderText = column.ColumnName;
                    HFGP.Columns[i].DataPropertyName = column.ColumnName;
                    i = i + 1;
                }
                HFGP.Columns[0].Visible = false;
                HFGP.Columns[1].Width = 90;
                HFGP.Columns[2].Width = 90;
                HFGP.Columns[3].Width = 450;
                //HFGP.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                HFGP.Columns[4].Width = 150;
                HFGP.Columns[5].Width = 150;
                HFGP.Columns[6].Visible = false;
                HFGP.Columns[7].Visible = false;
                HFGP.Columns[8].Visible = false;
                HFGP.Columns[9].Visible = false;
                HFGP.Columns[10].Visible = false;
                HFGP.DataSource = tap;
                HFGP.Columns[2].DefaultCellStyle.Format = "yyyy'/'MM'/'dd";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }
            finally
            {
                conn.Close();
                Genclass.cmd.Dispose();
            }
        }
        private void button3_Click(object sender, EventArgs e)
        {
            mode = 1;
            Genpan.Visible = false;

            panadd.Visible = false;
            h = 0;
            Genclass.ClearTextBox(this, Editpnl);
            Genclass.STR = "";
            Genclass.Gendocno();
            txtgrn.Text = Genclass.ST;
            Editpnl.Visible = true;
            lkppnl.Visible = false;
            sum1 = 0;
            sum2 = 0;
            HFIT.Refresh();
            HFIT.DataSource = null;
            HFIT.Rows.Clear();

            HFGP1.Refresh();
            HFGP1.DataSource = null;
            HFGP1.Rows.Clear();

            conn.Close();
            conn.Open();
            Docno.Clear();

            button13.Visible = false;
            if (Docno.Columns.Count == 0)
            {
                Docno.Columns.Add("uid");
                Docno.Columns.Add("Docno");
            }
            conn.Close();
            conn.Open();
            Chkedtact.Checked = true;




            HFIT.Refresh();
            HFIT.DataSource = null;
            HFIT.Rows.Clear();
            HFGP1.Refresh();
            HFGP1.DataSource = null;
            HFGP1.Rows.Clear();

            Titlep();
            if (Genclass.Dtype == 220)
            {

                label20.Visible = false;
                cbotype.Visible = false;
                txtuom.Location = new Point(601, 135);
                label15.Location = new Point(601, 114);

            }

            dtpgrndt.Focus();
        }
        private void Titlep()
        {
            HFIT.AutoGenerateColumns = false;
            HFIT.Refresh();
            HFIT.DataSource = null;
            HFIT.Rows.Clear();
            HFIT.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            HFIT.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            HFIT.ColumnCount = 19;
            HFIT.Columns[0].Name = "SLNo";
            HFIT.Columns[1].Name = "Itemname";
            HFIT.Columns[2].Name = "Color";
            HFIT.Columns[3].Name = "Uom";
            HFIT.Columns[4].Name = "BillingUOm";
            HFIT.Columns[5].Name = "Type";
            HFIT.Columns[6].Name = "Qty";
            HFIT.Columns[7].Name = "Billing Qty";
            HFIT.Columns[8].Name = "Rate";
            HFIT.Columns[9].Name = "Uomid";
            HFIT.Columns[10].Name = "BillUomid";
            HFIT.Columns[11].Name = "Itemid";
            HFIT.Columns[12].Name = "uid";
            HFIT.Columns[13].Name = "itemdes";
            HFIT.Columns[14].Name = "REFID";
            HFIT.Columns[15].Name = "useitemid";
            HFIT.Columns[16].Name = "socno";
            HFIT.Columns[17].Name = "workorder";
            HFIT.Columns[18].Name = "processid";
            //HFIT.EditMode = DataGridViewEditMode.EditOnKeystroke;

            HFIT.Columns[0].Width = 80;

            HFIT.Columns[1].Width = 400;
            HFIT.Columns[2].Visible = false;
            HFIT.Columns[3].Width = 70;
            HFIT.Columns[4].Width = 100;
            if (Genclass.Dtype == 220)
            {
                HFIT.Columns[5].Visible = false;
            }
            else
            {
                HFIT.Columns[5].Width = 80;
            }
            HFIT.Columns[6].Width = 80;
            HFIT.Columns[7].Visible = false;
            HFIT.Columns[8].Visible = false;
            HFIT.Columns[9].Visible = false;
            HFIT.Columns[10].Visible = false;
            HFIT.Columns[11].Visible = false;
            HFIT.Columns[12].Visible = false;
            HFIT.Columns[13].Visible = false;
            HFIT.Columns[14].Visible = false;
            HFIT.Columns[15].Visible = false;
            HFIT.Columns[16].Visible = false;
            HFIT.Columns[17].Visible = false;
            HFIT.Columns[18].Visible = false;
        }
        private void txtname_Click(object sender, EventArgs e)
        {
            Genclass.type = 1;
            DataTable dt = getParty();
            bsp.DataSource = dt;
            FillGrid2(dt, 1);
            Point loc = FindLocation(txtname);
            grSearch.Location = new Point(loc.X, loc.Y + 20);
            grSearch.Visible = true;
        }
        protected void FillGrid2(DataTable dt, int FillId)
        {
            try
            {
                DataGridCommon.DataSource = null;
                DataGridCommon.AutoGenerateColumns = false;
                DataGridCommon.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
                DataGridCommon.DefaultCellStyle.WrapMode = DataGridViewTriState.True;


                DataGridCommon.ColumnCount = 2;
                DataGridCommon.Columns[0].Name = "supplieruid";
                DataGridCommon.Columns[0].HeaderText = "supplieruid";
                DataGridCommon.Columns[0].DataPropertyName = "supplieruid";
                DataGridCommon.Columns[1].Name = "Name";
                DataGridCommon.Columns[1].HeaderText = "Name";
                DataGridCommon.Columns[1].DataPropertyName = "Name";
                DataGridCommon.Columns[1].Width = 350;
                //DataGridCommon.Columns[2].Name = "Dcno";
                //DataGridCommon.Columns[2].HeaderText = "Dcno";
                //DataGridCommon.Columns[2].DataPropertyName = "Dcno";
                //DataGridCommon.Columns[2].Width = 80;
                //DataGridCommon.Columns[3].Name = "uid";
                //DataGridCommon.Columns[3].HeaderText = "uid";
                //DataGridCommon.Columns[3].DataPropertyName = "uid";
                //DataGridCommon.Columns[4].Name = "docno";
                //DataGridCommon.Columns[4].HeaderText = "docno";
                //DataGridCommon.Columns[4].DataPropertyName = "docno";
                //DataGridCommon.Columns[5].Name = "Docdate";
                //DataGridCommon.Columns[5].HeaderText = "Docdate";
                //DataGridCommon.Columns[5].DataPropertyName = "Docdate";
                //DataGridCommon.Columns[5].Width = 90;

                DataGridCommon.DataSource = bsp;
                DataGridCommon.Columns[0].Visible = false;
                //DataGridCommon.Columns[3].Visible = false;
                //DataGridCommon.Columns[4].Visible = false;



            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        private void txtname_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.F10)
                {
                    grSearch.Visible = false;
                }
                else if (e.KeyCode == Keys.F2)
                {
                    SelectId = 1;
                    int Index = DataGridCommon.SelectedCells[0].RowIndex;

                    txtname.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtpuid.Text = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    //txtmillid.Text = DataGridCommon.Rows[Index].Cells[4].Value.ToString();
                    txtmode.Focus();
                    grSearch.Visible = false;
                    SelectId = 0;
                }
                else if (e.KeyCode == Keys.Escape)
                {
                    grSearch.Visible = false;
                }
                else if (e.KeyValue == 40)
                {
                    DataGridCommon.Select();
                }
                else if (e.KeyCode == Keys.Enter)
                {
                    txtname_Click(sender, e);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtname_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bsp.Filter = string.Format("Name LIKE '%{0}%' ", txtname.Text);



                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtdcqty_Click(object sender, EventArgs e)
        {

            lkppnl.Visible = true;
            txtwok.Text = "";
            txtwok.Focus();
            DataGridCommon.AutoGenerateColumns = false;
            DataGridCommon.Refresh();
            DataGridCommon.DataSource = null;
            DataGridCommon.Rows.Clear();


        }
        private void loaditems()
        {

            txtscr11.Text = "";
            Genclass.type = 9;
            if (txtname.Text == string.Empty)
            {
                MessageBox.Show("Select Supplier Name", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtname.Focus();
                return;
            }
             
            txtscr11.Focus();

            //Genclass.strsql = "select distinct b.itemid,b.Itemdes as ItemName,a.Docno as Dcno,b.qty- isnull(SUM(d.qty),0) as Pendqty,b.uid,h.generalname,c.uom_uid,b.KNITDIA from jo a inner join   JOBISSUEDETTAB  b on a.uid=B.HEADID   and a.doctypeid=540  		   	 left join itemm c on b.Itemid=c.uid   left join generalm h on c.uom_uid=h.uid left join JORKNITREC d on b.uid=d.REFID 	 left join jo e on d.HEADID=e.uid  and e.Doctypeid=550   left join partym k  on a.supplieruid=k.uid 	 where a.supplieruid=" + txtpuid.Text +"    group by a.Docno,b.qty,b.Itemid,b.Itemdes,b.uid,b.KNITDIA,h.generalname,c.uom_uid  having b.qty- isnull(SUM(d.qty),0)> 0";         
            if (Genclass.Dtype == 570)
            {
                Genclass.strsql = " SP_GETJORRECITEMS1 " + Genclass.Dtype + "," + txtpuid.Text + ",'" + txtscono.Text + "','" + txtwok.Text + "','" + txtisstot.Text + "'";
                Genclass.FSSQLSortStr = "b.Itemdes";
            }
            else if (Genclass.Dtype == 590)
            {
                Genclass.strsql = "SP_GETJORRECITEMS2  " + Genclass.Dtype + "," + txtpuid.Text + "  ";
                Genclass.FSSQLSortStr = "b.Itemdes";

            }
            else if (Genclass.Dtype == 610)
            {
                Genclass.strsql = "SP_GETJORRECITEMS3  " + Genclass.Dtype + "," + txtpuid.Text + "  ";
                Genclass.FSSQLSortStr = "b.Itemdes";

            }
            else if (Genclass.Dtype == 630)
            {
                Genclass.strsql = "SP_GETJORRECITEMS4  " + Genclass.Dtype + "," + txtpuid.Text + "  ";
                Genclass.FSSQLSortStr = "b.Itemdes";

            }
            else if (Genclass.Dtype == 220)
            {
                Genclass.strsql = "SP_GETJORRECITEMSYARN  " + Genclass.Dtype + "," + txtpuid.Text + ",'" + txtscono.Text + "','" + txtwok.Text + "','" + txtisstot.Text + "'";
                Genclass.FSSQLSortStr = "b.Itemdes";

            }
            else if (Genclass.Dtype == 960)
            {
                Genclass.strsql = " SP_GETJORRECITEMS6 " + Genclass.Dtype + "," + txtpuid.Text + ", '" + txtdcno.Text + "'";
                Genclass.FSSQLSortStr = "b.Itemdes";

            }

            Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
            SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
            DataTable tap = new DataTable();
            aptr.Fill(tap);
            bsc1.DataSource = tap;
            Point loc = FindLocation(txtdcqty);
            lkppnl.Location = new Point(loc.X, loc.Y + 20);
            lkppnl.Visible = true;
            HFGP2.AutoGenerateColumns = false;
            HFGP2.Refresh();
            HFGP2.DataSource = null;
            HFGP2.Rows.Clear();
            HFGP2.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            HFGP2.DefaultCellStyle.WrapMode = DataGridViewTriState.True;

            HFGP2.ColumnCount = tap.Columns.Count;
            i = 0;

            foreach (DataColumn column in tap.Columns)
            {
                HFGP2.Columns[i].Name = column.ColumnName;
                HFGP2.Columns[i].HeaderText = column.ColumnName;
                HFGP2.Columns[i].DataPropertyName = column.ColumnName;
                i = i + 1;
            }
            HFGP2.DataSource = tap;

            HFGP2.Columns[0].Visible = false;
            HFGP2.Columns[2].Width = 100;

            HFGP2.Columns[3].Width = 100;
            HFGP2.Columns[4].Visible = false;
            HFGP2.Columns[5].Visible = false;
            HFGP2.Columns[6].Visible = false;
            HFGP2.Columns[1].Width = 450;
            HFGP2.Columns[7].Visible = false;
            HFGP2.Columns[8].Visible = false;
            HFGP2.Columns[9].Visible = false;
            HFGP2.Columns[10].Visible = false;
            HFGP2.Columns[11].Visible = false;
            HFGP2.Columns[12].Visible = false;
            HFGP2.Columns[13].Width = 100;
            HFGP2.Columns[14].Visible = false;
            HFGP2.Columns[15].Visible = false;
            HFGP2.Columns[16].Width = 100;
            //HFGP2.Columns[17].Width = 100;

            conn.Close();
        }
        protected void FillGrid(DataTable dt, int FillId)
        {
            try
            {
                DataGridCommon.DataSource = null;
                DataGridCommon.AutoGenerateColumns = false;


                DataGridCommon.ColumnCount = 4;
                DataGridCommon.Columns[0].Name = "uid";
                DataGridCommon.Columns[0].HeaderText = "uid";
                DataGridCommon.Columns[0].DataPropertyName = "uid";
                DataGridCommon.Columns[1].Name = "Itemname";
                DataGridCommon.Columns[1].HeaderText = "Itemname";
                DataGridCommon.Columns[1].DataPropertyName = "Itemname";
                DataGridCommon.Columns[1].Width = 300;
                DataGridCommon.Columns[2].Name = "UOMID";
                DataGridCommon.Columns[2].HeaderText = "UOMID";
                DataGridCommon.Columns[2].DataPropertyName = "UOMID";

                DataGridCommon.Columns[3].Name = "GENERALNAME";
                DataGridCommon.Columns[3].HeaderText = "GENERALNAME";
                DataGridCommon.Columns[3].DataPropertyName = "GENERALNAME";



                DataGridCommon.DataSource = bsParty;
                DataGridCommon.Columns[0].Visible = false;
                DataGridCommon.Columns[2].Visible = false;
                DataGridCommon.Columns[3].Visible = false;



            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private Point FindLocation(Control ctrl)
        {
            if (ctrl.Parent is Form)
                return ctrl.Location;
            else
            {
                Point p = FindLocation(ctrl.Parent);
                p.X += ctrl.Location.X;
                p.Y += ctrl.Location.Y;
                return p;
            }
        }
        protected DataTable getParty()
        {
            DataTable dt = new DataTable();
            try
            {
                if (Genclass.type == 1 && Genclass.Dtype == 220)
                {
                    dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "SP_JORRECYARN", conn);
                    bsp.DataSource = dt;
                }
                else if (Genclass.type == 1 && Genclass.Dtype == 570)
                {
                    dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "SP_JORREC11", conn);
                    bsp.DataSource = dt;
                }
                else if (Genclass.type == 1 && Genclass.Dtype == 590)
                {
                    dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "SP_JORREC1123", conn);
                    bsp.DataSource = dt;
                }
                else if (Genclass.type == 1 && Genclass.Dtype == 610)
                {
                    dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "SP_JORREC11234", conn);
                    bsp.DataSource = dt;
                }
                else if (Genclass.type == 1 && Genclass.Dtype == 630)
                {
                    dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "SP_JORREC112345", conn);
                    bsp.DataSource = dt;
                }
                else if (Genclass.type == 1 && Genclass.Dtype == 960)
                {
                    dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "SP_JORRECFabricReIssue", conn);
                    bsp.DataSource = dt;
                }
                else if (Genclass.type == 2)
                {
                    dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "sp_getwo", conn);
                    bsc1.DataSource = dt;
                }


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            return dt;
        }
        private void txtdcqty_KeyDown(object sender, KeyEventArgs e)
        {

            try
            {
                if (e.KeyCode == Keys.F10)
                {
                    grSearch.Visible = false;
                }
                else if (e.KeyCode == Keys.F2)
                {
                    SelectId = 1;
                    int Index = DataGridCommon.SelectedCells[0].RowIndex;
                    txtdcqty.Text = HFGP2.Rows[Index].Cells[1].Value.ToString();
                    txtdcqty.Tag = HFGP2.Rows[Index].Cells[0].Value.ToString();
                    txtuom.Text = HFGP2.Rows[Index].Cells[2].Value.ToString();
                    txtitemid.Text = HFGP2.Rows[Index].Cells[3].Value.ToString();
                    txtprice.Text = HFGP2.Rows[Index].Cells[4].Value.ToString();
                    txtprice.Tag = HFGP2.Rows[Index].Cells[5].Value.ToString();
                    txtisstot.Text = HFGP2.Rows[Index].Cells[6].Value.ToString();

                    txtitem.Focus();
                    grSearch.Visible = false;
                    SelectId = 0;
                }
                else if (e.KeyCode == Keys.Escape)
                {
                    grSearch.Visible = false;
                }
                else if (e.KeyValue == 40)
                {
                    DataGridCommon.Select();
                }
                else if (e.KeyCode == Keys.Enter)
                {
                    txtdcqty_Click(sender, e);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtdcqty_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtitem_Click(object sender, EventArgs e)
        {
            Genclass.type = 2;
            DataTable dt = getParty();
            bsc1.DataSource = dt;
            FillGrid3(dt, 1);
            Point loc = FindLocation(txtitem);
            grSearch.Location = new Point(loc.X, loc.Y + 20);
            grSearch.Visible = true;
            grSearch.Text = "Name Search";
        }

        private void txtitem_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.F10)
                {
                    grSearch.Visible = false;
                }
                else if (e.KeyCode == Keys.F2)
                {
                    SelectId = 1;
                    int Index = DataGridCommon.SelectedCells[0].RowIndex;

                    txtitem.Text = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    txtitem.Tag = DataGridCommon.Rows[Index].Cells[1].Value.ToString();

                    txtmode.Focus();
                    lkppnl.Visible = false;
                    SelectId = 0;
                }
                else if (e.KeyCode == Keys.Escape)
                {
                    grSearch.Visible = false;
                }
                else if (e.KeyValue == 40)
                {
                    DataGridCommon.Select();
                }
                else if (e.KeyCode == Keys.Enter)
                {
                    txtitem_Click(sender, e);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
        protected void FillGrid3(DataTable dt, int FillId)
        {
            try
            {
                DataGridCommon.DataSource = null;
                DataGridCommon.AutoGenerateColumns = false;


                DataGridCommon.ColumnCount = 4;
                DataGridCommon.Columns[0].Name = "Uid";
                DataGridCommon.Columns[0].HeaderText = "Uid";
                DataGridCommon.Columns[0].DataPropertyName = "Uid";

                DataGridCommon.Columns[1].Name = "docno";
                DataGridCommon.Columns[1].HeaderText = "Docno";
                DataGridCommon.Columns[1].DataPropertyName = "docno";
                DataGridCommon.Columns[1].Width = 150;


                DataGridCommon.Columns[2].Name = "processname";
                DataGridCommon.Columns[2].HeaderText = "processname";
                DataGridCommon.Columns[2].DataPropertyName = "processname";




                DataGridCommon.Columns[3].Name = "processid";
                DataGridCommon.Columns[3].HeaderText = "processid";
                DataGridCommon.Columns[3].DataPropertyName = "processid";






                DataGridCommon.DataSource = bsc1;
                DataGridCommon.Columns[0].Visible = false;
                DataGridCommon.Columns[2].Visible = false;
                DataGridCommon.Columns[3].Visible = false;



            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        private void txtitem_TextChanged(object sender, EventArgs e)
        {
            //try
            //{
            //    if (SelectId == 0)
            //    {
            //        bsp.Filter = string.Format("docno LIKE '%{0}%' ", txtvehicle.Text);



            //    }
            //}
            //catch (Exception ex)
            //{
            //    MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //    return;
            //}
        }

        private void button18_Click(object sender, EventArgs e)
        {
            try
            {
                SelectId = 1;
                int Index = DataGridCommon.SelectedCells[0].RowIndex;
                if (Genclass.type == 1)
                {
                    txtname.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtpuid.Text = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                  
                    txtitem.Focus();


                }
                else if (Genclass.type == 11)
                {


                    txtisstot.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtscono.Text = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    txtwok.Text = DataGridCommon.Rows[Index].Cells[2].Value.ToString();
                    loaditems();

                }
                else if (Genclass.type == 2)
                {
                    txtitem.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtitem.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    txtmode.Focus();
                }
                else if (Genclass.type == 10)
                {

                    txtdcno.Text = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    txtwo.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtorderd.Text = DataGridCommon.Rows[Index].Cells[2].Value.ToString();
                    txtorderd.Tag = DataGridCommon.Rows[Index].Cells[3].Value.ToString();
                }

                grSearch.Visible = false;
                SelectId = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnHide_Click(object sender, EventArgs e)
        {
            grSearch.Visible = false;
        }

        private void DataGridCommon_DoubleClick(object sender, EventArgs e)
        {
            SelectId = 1;
            int Index = DataGridCommon.SelectedCells[0].RowIndex;
            if (Genclass.type == 1)
            {
                txtname.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                txtpuid.Text = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                //txtmillid.Text = DataGridCommon.Rows[Index].Cells[4].Value.ToString();
                txtmode.Focus();


            }
            else if (Genclass.type == 11)
            {
                txtisstot.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                txtscono.Text = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                txtwok.Text = DataGridCommon.Rows[Index].Cells[2].Value.ToString();
                loaditems();

            }
            else if (Genclass.type == 2)
            {
                txtitem.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                txtitem.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                txtmode.Focus();
            }
            else if (Genclass.type == 10)
            {

                txtdcno.Text = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                txtwo.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                txtorderd.Text = DataGridCommon.Rows[Index].Cells[2].Value.ToString();
                txtorderd.Tag = DataGridCommon.Rows[Index].Cells[3].Value.ToString();
            }

            grSearch.Visible = false;
            SelectId = 0;
        }

        private void DataGridCommon_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                int Index = DataGridCommon.SelectedCells[0].RowIndex;
                if (Genclass.type == 1)
                {
                    txtpuid.Text = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    txtname.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();

                    //txtmillid.Text = DataGridCommon.Rows[Index].Cells[4].Value.ToString();
                    txtmode.Focus();


                }
                else if (Genclass.type == 11)
                {
                    txtisstot.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtscono.Text = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    txtwok.Text = DataGridCommon.Rows[Index].Cells[2].Value.ToString();
                    loaditems();

                }
                else if (Genclass.type == 2)
                {
                    txtitem.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtitem.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    txtmode.Focus();
                }
                else if (Genclass.type == 10)
                {

                    txtdcno.Text = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    txtwo.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtorderd.Text = DataGridCommon.Rows[Index].Cells[2].Value.ToString();
                    txtorderd.Tag = DataGridCommon.Rows[Index].Cells[3].Value.ToString();
                }
                grSearch.Visible = false;
                SelectId = 0;
            }
        }

        private void button10_Click(object sender, EventArgs e)
        {
            if (Genclass.type == 9)
            {
                int Index = HFGP2.SelectedCells[0].RowIndex;

                txtdcqty.Text = HFGP2.Rows[Index].Cells[1].Value.ToString();
                txtdcqty.Tag = HFGP2.Rows[Index].Cells[0].Value.ToString();
                txtuom.Text = HFGP2.Rows[Index].Cells[3].Value.ToString();
                txtitemid.Text = HFGP2.Rows[Index].Cells[4].Value.ToString();
                txtprice.Text = HFGP2.Rows[Index].Cells[5].Value.ToString();
                txtprice.Tag = HFGP2.Rows[Index].Cells[6].Value.ToString();

                txtaddnotes.Text = HFGP2.Rows[Index].Cells[7].Value.ToString();
                cbotype.Text = HFGP2.Rows[Index].Cells[8].Value.ToString();
                txtisstot.Text = HFGP2.Rows[Index].Cells[9].Value.ToString();
                cbouom.Tag = HFGP2.Rows[Index].Cells[10].Value.ToString();
                cbouom.Text = HFGP2.Rows[Index].Cells[11].Value.ToString();
                txtorderd.Tag = HFGP2.Rows[Index].Cells[13].Value.ToString();
                txtrate.Tag = HFGP2.Rows[Index].Cells[14].Value.ToString();
                txtbags.Text = HFGP2.Rows[Index].Cells[15].Value.ToString();
                txtuom.Tag = HFGP2.Rows[Index].Cells[16].Value.ToString();
                txtaddnotes.Focus();
            }




            lkppnl.Visible = false;

        }

        private void HFGP2_DoubleClick(object sender, EventArgs e)
        {
            if (Genclass.type == 9)
            {
                int Index = HFGP2.SelectedCells[0].RowIndex;

                txtdcqty.Text = HFGP2.Rows[Index].Cells[1].Value.ToString();
                txtdcqty.Tag = HFGP2.Rows[Index].Cells[0].Value.ToString();
                txtuom.Text = HFGP2.Rows[Index].Cells[3].Value.ToString();
                txtitemid.Text = HFGP2.Rows[Index].Cells[4].Value.ToString();
                txtprice.Text = HFGP2.Rows[Index].Cells[5].Value.ToString();
                txtprice.Tag = HFGP2.Rows[Index].Cells[6].Value.ToString();
                //txtrate.Tag = HFGP2.Rows[Index].Cells[14].Value.ToString();
                txtaddnotes.Text = HFGP2.Rows[Index].Cells[7].Value.ToString();
                cbotype.Text = HFGP2.Rows[Index].Cells[8].Value.ToString();
                txtisstot.Text = HFGP2.Rows[Index].Cells[9].Value.ToString();
                cbouom.Tag = HFGP2.Rows[Index].Cells[10].Value.ToString();
                cbouom.Text = HFGP2.Rows[Index].Cells[11].Value.ToString();
                txtorderd.Tag = HFGP2.Rows[Index].Cells[13].Value.ToString();
                txtrate.Tag = HFGP2.Rows[Index].Cells[14].Value.ToString();
                txtbags.Text = HFGP2.Rows[Index].Cells[15].Value.ToString();
                txtuom.Tag = HFGP2.Rows[Index].Cells[16].Value.ToString();

                txtaddnotes.Focus();
            }
            else if (Genclass.type == 11)
            {
                int Index = HFGP2.SelectedCells[0].RowIndex;

                txtisstot.Text = HFGP2.Rows[Index].Cells[1].Value.ToString();
                txtscono.Text = HFGP2.Rows[Index].Cells[0].Value.ToString();
                txtwok.Text = HFGP2.Rows[Index].Cells[2].Value.ToString();
                loaditems();
            }

            else if (Genclass.type == 10)
            {
                int Index = HFGP2.SelectedCells[0].RowIndex;


                txtdcno.Text = HFGP2.Rows[Index].Cells[0].Value.ToString();
                txtwo.Text = HFGP2.Rows[Index].Cells[1].Value.ToString();
                txtorderd.Text = HFGP2.Rows[Index].Cells[2].Value.ToString();
                txtorderd.Tag = HFGP2.Rows[Index].Cells[3].Value.ToString();
            }

            lkppnl.Visible = false;

        }

        private void HFGP2_KeyDown(object sender, KeyEventArgs e)
        {

            if (e.KeyCode == Keys.Enter)
            {
                if (Genclass.type == 9)
                {
                    int Index = HFGP2.SelectedCells[0].RowIndex;

                    txtdcqty.Text = HFGP2.Rows[Index].Cells[1].Value.ToString();
                    txtdcqty.Tag = HFGP2.Rows[Index].Cells[0].Value.ToString();
                    txtuom.Text = HFGP2.Rows[Index].Cells[3].Value.ToString();
                    txtitemid.Text = HFGP2.Rows[Index].Cells[4].Value.ToString();
                    txtprice.Text = HFGP2.Rows[Index].Cells[5].Value.ToString();
                    txtprice.Tag = HFGP2.Rows[Index].Cells[6].Value.ToString();
                    txtaddnotes.Text = HFGP2.Rows[Index].Cells[7].Value.ToString();
                    cbotype.Text = HFGP2.Rows[Index].Cells[8].Value.ToString();
                    txtisstot.Text = HFGP2.Rows[Index].Cells[9].Value.ToString();
                    cbouom.Tag = HFGP2.Rows[Index].Cells[10].Value.ToString();
                    cbouom.Text = HFGP2.Rows[Index].Cells[11].Value.ToString();
                    txtorderd.Tag = HFGP2.Rows[Index].Cells[13].Value.ToString();
                    txtrate.Tag = HFGP2.Rows[Index].Cells[14].Value.ToString();
                    txtbags.Text = HFGP2.Rows[Index].Cells[15].Value.ToString();
                    txtuom.Tag = HFGP2.Rows[Index].Cells[16].Value.ToString();
                    txtaddnotes.Focus();
                }
                else if (Genclass.type == 11)
                {
                    int Index = HFGP2.SelectedCells[0].RowIndex;

                    txtisstot.Text = HFGP2.Rows[Index].Cells[1].Value.ToString();
                    txtscono.Text = HFGP2.Rows[Index].Cells[0].Value.ToString();
                    txtwok.Text = HFGP2.Rows[Index].Cells[2].Value.ToString();
                    loaditems();

                }

                else if (Genclass.type == 10)
                {
                    int Index = HFGP2.SelectedCells[0].RowIndex;

                    txtdcno.Text = HFGP2.Rows[Index].Cells[1].Value.ToString();
                    txtwo.Text = HFGP2.Rows[Index].Cells[0].Value.ToString();
                    txtorderd.Text = HFGP2.Rows[Index].Cells[2].Value.ToString();
                    txtorderd.Tag = HFGP2.Rows[Index].Cells[3].Value.ToString();
                }

                lkppnl.Visible = false;
            }
        }

        private void buttnfinbk_Click(object sender, EventArgs e)
        {
            Editpnl.Visible = false;
            Genpan.Visible = true;
            panadd.Visible = true;
            LoadGetJobCard(1);
        }

        private void btnadd_Click(object sender, EventArgs e)
        {
            if (txtdcqty.Text == "")
            {
                MessageBox.Show("Enter the Item");
                txtdcqty.Focus();
                return;
            }
         
            if (txtuom.Text == "")
            {
                MessageBox.Show("Enter the Qty");
                txtuom.Focus();
                return;
            }

            if (txtisstot.Text == "")
            {
                txtisstot.Text = "0";
            }


            double tt = Convert.ToDouble(txtisstot.Text);
            double pp = Convert.ToDouble(txtuom.Text);
            double rr = Convert.ToDouble(txtisstot.Text) * 5 / 100;
            double ll = tt + rr;
            if (tt == pp || tt > pp || ll == pp)
            {


            }
            else if (ll < pp)

            {
                MessageBox.Show("Qty Exceed 5%");
                return;

            }


            if (mode == 2)
            {
                kk = HFIT.Rows.Count - 1;
                h = kk;
            }

            if(txtrate.Tag==null)
            {
                txtrate.Tag = 0;

            }
            h = h + 1;
            var index = HFIT.Rows.Add();
            HFIT.Rows[index].Cells[0].Value = h;
            HFIT.Rows[index].Cells[1].Value = txtdcqty.Text;
            HFIT.Rows[index].Cells[2].Value = txtaddnotes.Text;
            HFIT.Rows[index].Cells[3].Value = txtprice.Text;
            HFIT.Rows[index].Cells[4].Value = cbouom.Text;
            HFIT.Rows[index].Cells[5].Value = cbotype.Text;
            HFIT.Rows[index].Cells[6].Value = txtuom.Text;

            HFIT.Rows[index].Cells[7].Value = txtisstot.Text;
            HFIT.Rows[index].Cells[8].Value = txtorderd.Tag;

            HFIT.Rows[index].Cells[9].Value = txtprice.Tag;
            HFIT.Rows[index].Cells[10].Value = cbouom.SelectedValue;
            HFIT.Rows[index].Cells[11].Value = txtdcqty.Tag;
            HFIT.Rows[index].Cells[12].Value = txtbags.Text;
            HFIT.Rows[index].Cells[13].Value = txtorderd.Tag;
            HFIT.Rows[index].Cells[14].Value = txtitemid.Text;
            HFIT.Rows[index].Cells[15].Value = txtrate.Tag;
            HFIT.Rows[index].Cells[16].Value = txtscono.Text;
            HFIT.Rows[index].Cells[17].Value = txtwok.Text;
            HFIT.Rows[index].Cells[18].Value = txtbags.Text;

            txtdcqty.Text = "";

            txtaddnotes.Text = "";
            txtbags.Text = "";
            txtuom.Text = "";
            txtisstot.Text = "";
            txtprice.Text = "";
            txtdcqty.Text = "";
            textBox1.Text = "";



            txtdcqty.Focus();
        }

        private void btnsave_Click(object sender, EventArgs e)
        {
            conn.Close();
            conn.Open();



            if (Chkedtact.Checked == true)
            {

                SP = 1;
            }
            else
            {
                SP = 0;

            }
            if (mode == 1)
            {



                SqlParameter[] para ={

                    new SqlParameter("@DOCNO",txtgrn.Text),
                    new SqlParameter("@DOCDATE",Convert.ToDateTime(dtpgrndt.Text)),
                    new SqlParameter("@REFNO",txtorderd.Text),
                    new SqlParameter("@DCNO",txtqty.Text),
                    new SqlParameter("@DCDATE",Convert.ToDateTime(dtpsupp.Text)),
                    new SqlParameter("@DOCTYPEID", Genclass.Dtype),
                    new SqlParameter("@SUPPLIERUID",txtpuid.Text),
                    new SqlParameter("@PLACEOFSUPPLY",txtdcno.Text),
                    new SqlParameter("@MODE",txtmode.Text),
                    new SqlParameter("@REMARKS",txtwo.Text),
                    new SqlParameter("@VEHICLENO",txtvehicle.Text),
                    new SqlParameter("@REASON", "0"),
                    new SqlParameter("@REFID", "0"),
                    new SqlParameter("@YEARID",Genclass.Yearid),
                    new SqlParameter("@ACTIVE", SP)


            };
                db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_JO", para, conn);

                string quy6 = "select * from jo where docno='" + txtgrn.Text + "' and doctypeid=" + Genclass.Dtype + "";
                Genclass.cmd = new SqlCommand(quy6, conn);
                SqlDataAdapter aptr3 = new SqlDataAdapter(Genclass.cmd);
                DataTable tap5 = new DataTable();
                aptr3.Fill(tap5);
                if (tap5.Rows.Count > 0)
                {
                    txtgrnid.Text = tap5.Rows[0]["uid"].ToString();

                }

                for (int i = 0; i < HFIT.RowCount - 1; i++)
                {

                    conn.Close();
                    conn.Open();

                    SqlParameter[] para1 ={


               new SqlParameter("@slno", HFIT.Rows[i].Cells[0].Value),
                    new SqlParameter("@iTEMNAME",HFIT.Rows[i].Cells[1].Value),
                    new SqlParameter("@ITEMID", HFIT.Rows[i].Cells[11].Value),
                    new SqlParameter("@BILLUOMID", HFIT.Rows[i].Cells[10].Value),
                    new SqlParameter("@UOMID", HFIT.Rows[i].Cells[9].Value),
                    new SqlParameter("@QTY", HFIT.Rows[i].Cells[6].Value),
                    new SqlParameter("@BILLQTY",HFIT.Rows[i].Cells[7].Value),
                    new SqlParameter("@TYPE", HFIT.Rows[i].Cells[5].Value),
                    new SqlParameter("@RATE", HFIT.Rows[i].Cells[8].Value),
                    new SqlParameter("@HEADID",txtgrnid.Text),
                    new SqlParameter("@COLOR",HFIT.Rows[i].Cells[12].Value),
                    new SqlParameter("@ITEMDES",HFIT.Rows[i].Cells[13].Value),

                     new SqlParameter("@REFID",HFIT.Rows[i].Cells[14].Value),
                     new SqlParameter("@useitemid",HFIT.Rows[i].Cells[14].Value),
                      new SqlParameter("@socno",HFIT.Rows[i].Cells[16].Value),
                     new SqlParameter("@workorder",HFIT.Rows[i].Cells[17].Value),
                           new SqlParameter("@processid",HFIT.Rows[i].Cells[18].Value),

                };

                    db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_JOIMREC", para1, conn);
                }

                string quy77 = "select a.headid,c.uid as dcid,a.uid as recid,isnull(a.qty,0) as qty,b.uid as issid from JOISSUEPGMITEMS a inner join JORECEIPT b on a.refid=b.uid inner join joDcitems c on b.headid=c.headid  and b.itemid=c.itemid    where a.headid=" + txtgrnid.Text + "";
                Genclass.cmd = new SqlCommand(quy77, conn);
                SqlDataAdapter aptr77 = new SqlDataAdapter(Genclass.cmd);
                DataTable tap77 = new DataTable();
                aptr77.Fill(tap77);
                if (tap77.Rows.Count > 0)
                {
                    for (int i = 0; i < tap77.Rows.Count; i++)
                    {
                        conn.Close();
                        conn.Open();

                        SqlParameter[] para1 ={

                    new SqlParameter("@headid",tap77.Rows[i]["headid"].ToString()),
                    new SqlParameter("@joiid", tap77.Rows[i]["dcid"].ToString()),

                    new SqlParameter("@recid", tap77.Rows[i]["recid"].ToString()),
                    new SqlParameter("@balqty",tap77.Rows[i]["qty"].ToString()),
                    new SqlParameter("@issid", tap77.Rows[i]["issid"].ToString()),



                };

                        db.ExecuteNonQuery(CommandType.StoredProcedure, "sp_joiitemslistmatch", para1, conn);

                    }
                }



            }
            else

            {


                conn.Close();
                conn.Open();

                SqlParameter[] para ={


                         new SqlParameter("@DOCNO",txtgrn.Text),
                    new SqlParameter("@DOCDATE",Convert.ToDateTime(dtpgrndt.Text)),
                    new SqlParameter("@REFNO",txtorderd.Text),
                    new SqlParameter("@DCNO",txtqty.Text),
                    new SqlParameter("@DCDATE",Convert.ToDateTime(dtpsupp.Text)),
                    new SqlParameter("@DOCTYPEID", Genclass.Dtype),
                    new SqlParameter("@SUPPLIERUID",txtpuid.Text),
                    new SqlParameter("@PLACEOFSUPPLY",txtdcno.Text),
                    new SqlParameter("@MODE",txtmode.Text),
                    new SqlParameter("@REMARKS",txtwo.Text),
                    new SqlParameter("@VEHICLENO",txtvehicle.Text),
                    new SqlParameter("@REASON", "0"),
                    new SqlParameter("@REFID", "0"),
                    new SqlParameter("@YEARID",Genclass.Yearid),
                    new SqlParameter("@ACTIVE", SP),
                    new SqlParameter("@UID", txtgrnid.Text)


                };
                db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_JOUPDATE", para, conn);

                conn.Close();
                conn.Open();
                qur.CommandText = "delete from JORECEIPT where headid=" + txtgrnid.Text + "";
                qur.ExecuteNonQuery();

                qur.CommandText = "delete from joiitemslistmatch where headid=" + txtgrnid.Text + "";
                qur.ExecuteNonQuery();

                for (int i = 0; i < HFIT.RowCount - 1; i++)
                {

                    conn.Close();
                    conn.Open();

                    SqlParameter[] para1 ={


                    new SqlParameter("@slno", HFIT.Rows[i].Cells[0].Value),
                    new SqlParameter("@iTEMNAME",HFIT.Rows[i].Cells[1].Value),
                    new SqlParameter("@ITEMID", HFIT.Rows[i].Cells[11].Value),
                    new SqlParameter("@BILLUOMID", HFIT.Rows[i].Cells[10].Value),
                    new SqlParameter("@UOMID", HFIT.Rows[i].Cells[9].Value),
                    new SqlParameter("@QTY", HFIT.Rows[i].Cells[6].Value),
                    new SqlParameter("@BILLQTY",HFIT.Rows[i].Cells[7].Value),
                    new SqlParameter("@TYPE", HFIT.Rows[i].Cells[5].Value),
                     new SqlParameter("@RATE",  HFIT.Rows[i].Cells[8].Value),
                    new SqlParameter("@HEADID",txtgrnid.Text),
                  new SqlParameter("@COLOR",HFIT.Rows[i].Cells[12].Value),
                    new SqlParameter("@ITEMDES",HFIT.Rows[i].Cells[13].Value),

                     new SqlParameter("@REFID",HFIT.Rows[i].Cells[14].Value),
                      new SqlParameter("@useitemid",HFIT.Rows[i].Cells[15].Value),
                               new SqlParameter("@socno",HFIT.Rows[i].Cells[16].Value),
                     new SqlParameter("@workorder",HFIT.Rows[i].Cells[17].Value),
                       new SqlParameter("@processid",HFIT.Rows[i].Cells[18].Value),

                };

                    db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_JOIMREC", para1, conn);
                }



                string quy77 = "select a.headid,c.uid as dcid,a.uid as recid,isnull(a.qty,0) as qty,b.uid as issid from JORECEIPT a inner join JOISSUEPGMITEMS b on a.refid=b.uid inner join joDcitems c on b.headid=c.headid  and b.itemid=c.itemid    where a.headid=" + txtgrnid.Text + "";
                Genclass.cmd = new SqlCommand(quy77, conn);
                SqlDataAdapter aptr77 = new SqlDataAdapter(Genclass.cmd);
                DataTable tap77 = new DataTable();
                aptr77.Fill(tap77);
                if (tap77.Rows.Count > 0)
                {
                    for (int i = 0; i < tap77.Rows.Count; i++)
                    {
                        conn.Close();
                        conn.Open();

                        SqlParameter[] para1 ={

                    new SqlParameter("@headid",tap77.Rows[i]["headid"].ToString()),
                    new SqlParameter("@joiid", tap77.Rows[i]["dcid"].ToString()),

                    new SqlParameter("@recid", tap77.Rows[i]["recid"].ToString()),
                    new SqlParameter("@balqty",tap77.Rows[i]["qty"].ToString()),
                    new SqlParameter("@issid", tap77.Rows[i]["issid"].ToString()),



                };

                        db.ExecuteNonQuery(CommandType.StoredProcedure, "sp_joiitemslistmatch", para1, conn);

                    }
                }


            }

            string qur1 = "select  DISTINCT B.uid,c.processname from jo a inner join joreceipt b on a.uid=b.headid  left join processm c on b.processid=c.uid  where a.doctypeid=570  and a.uid=" + txtgrnid.Text + "  and  c.processname is not null ";
            SqlCommand cmd = new SqlCommand(qur1, conn);
            SqlDataAdapter apt = new SqlDataAdapter(cmd);
            DataTable tab = new DataTable();
            apt.Fill(tab);
            Genclass.name = "";

            for (int k = 0; k < tab.Rows.Count; k++)
            {
                if (k == 0)
                {
                    Genclass.name = tab.Rows[k]["processname"].ToString();

                }
                else
                {

                    Genclass.name = Genclass.name + "/" + tab.Rows[k]["processname"].ToString();
                }


            }

            conn.Close();
            conn.Open();
            if (mode == 1)
            {
                qur.CommandText = "insert into FABRICPROCESSMGRN values (" + txtgrnid.Text + ",'" + Genclass.name + "')";
                qur.ExecuteNonQuery();
            }
            else
            {
                qur.CommandText = "delete from FABRICPROCESSMGRN  where refid=" + txtgrnid.Text + "";
                qur.ExecuteNonQuery();

                qur.CommandText = "insert into FABRICPROCESSMGRN values (" + txtgrnid.Text + ",'" + Genclass.name + "')";
                qur.ExecuteNonQuery();

            }


       

        conn.Close();
            conn.Open();
            if (mode == 1)
            {
                qur.CommandText = "update doctypem set lastno=lastno+1 where doctypeid=" + Genclass.Dtype + " and finyear='19-20' ";
                qur.ExecuteNonQuery();

            }
            conn.Close();
            conn.Open();
            if (txtgrnid.Text != "")
            {
                if (mode == 1)
                {
                  

                    qur.CommandText = "exec POST_SUPPLIERSTOCK_SP " + txtgrnid.Text + "," + Genclass.Dtype + " ";
                    qur.ExecuteNonQuery();
                }
                else
                {
                    qur.CommandText = "delete from SUPPLIER_STOCK_LEDGER where hdid= " + txtgrnid.Text + " and doctypeid=" + Genclass.Dtype + " ";
                    qur.ExecuteNonQuery();

              

                    qur.CommandText = "exec POST_SUPPLIERSTOCK_SP " + txtgrnid.Text + "," + Genclass.Dtype + " ";
                    qur.ExecuteNonQuery();
                }

            }

            MessageBox.Show("Save Record");
            Editpnl.Visible = false;

            Genpan.Visible = true;
            panadd.Visible = true;
            LoadGetJobCard(1);



        }

        private void butedit_Click(object sender, EventArgs e)
        {
            mode = 2;
            Chkedtact.Checked = true;
            mode = 2;
            panadd.Visible = false;
            Genpan.Visible = false;
            Editpnl.Visible = true;
            lkppnl.Visible = false;
          
            conn.Close();
            conn.Open();
            if (Genclass.Dtype == 220)
            {

                label20.Visible = false;
                cbotype.Visible = false;
                txtuom.Location = new Point(601, 135);
                label15.Location = new Point(601, 114);

            }

            //cbosGReturnItem.Items.Clear();
            int i = HFGP.SelectedCells[0].RowIndex;
            uid = HFGP.Rows[i].Cells[0].Value.ToString();
            txtgrnid.Text = HFGP.Rows[i].Cells[0].Value.ToString();
            txtgrn.Text = HFGP.Rows[i].Cells[1].Value.ToString();
            dtpgrndt.Text = HFGP.Rows[i].Cells[2].Value.ToString();

            txtname.Text = HFGP.Rows[i].Cells[3].Value.ToString();
            txtqty.Text = HFGP.Rows[i].Cells[5].Value.ToString();

            txtdcno.Text = HFGP.Rows[i].Cells[7].Value.ToString();
            txtwo.Text = HFGP.Rows[i].Cells[6].Value.ToString();


            txtpuid.Text = HFGP.Rows[i].Cells[8].Value.ToString();
            txtorderd.Tag = HFGP.Rows[i].Cells[9].Value.ToString();
            txtvehicle.Text = HFGP.Rows[i].Cells[10].Value.ToString();
            txtmode.Text = HFGP.Rows[i].Cells[11].Value.ToString();
            Chkedtact.Checked = true;
            HFIT.Refresh();
            HFIT.DataSource = null;
            HFIT.Rows.Clear();

            HFGP2.Refresh();
            HFGP2.DataSource = null;
            HFGP2.Rows.Clear();
            HFGP1.Refresh();
            HFGP1.DataSource = null;
            HFGP1.Rows.Clear();

            Titlep();
            load();

        }
        private void load()
        {
            Genclass.strsql = "SP_GETJREDITLOAD  " + txtgrnid.Text + "," + Genclass.Dtype + " ";
            Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
            SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
            DataTable tap1 = new DataTable();
            aptr1.Fill(tap1);

            sum1 = 0;


            for (k = 0; k < tap1.Rows.Count; k++)
            {
                var index = HFIT.Rows.Add();

                HFIT.Rows[index].Cells[0].Value = tap1.Rows[k]["slno"].ToString();

                HFIT.Rows[index].Cells[1].Value = tap1.Rows[k]["itemname"].ToString();

                HFIT.Rows[index].Cells[2].Value = tap1.Rows[k]["color"].ToString();
                HFIT.Rows[index].Cells[3].Value = tap1.Rows[k]["Uom"].ToString();

                HFIT.Rows[index].Cells[4].Value = tap1.Rows[k]["BillingUom"].ToString();

                HFIT.Rows[index].Cells[5].Value = tap1.Rows[k]["type"].ToString();


                HFIT.Rows[index].Cells[6].Value = tap1.Rows[k]["qty"].ToString();
                HFIT.Rows[index].Cells[7].Value = tap1.Rows[k]["billqty"].ToString();
                HFIT.Rows[index].Cells[8].Value = tap1.Rows[k]["Rate"].ToString();
                HFIT.Rows[index].Cells[9].Value = tap1.Rows[k]["uomid"].ToString();
                HFIT.Rows[index].Cells[10].Value = tap1.Rows[k]["billuomid"].ToString();
                HFIT.Rows[index].Cells[11].Value = tap1.Rows[k]["itemid"].ToString();
                HFIT.Rows[index].Cells[12].Value = tap1.Rows[k]["uid"].ToString();
                HFIT.Rows[index].Cells[13].Value = tap1.Rows[k]["itemdes"].ToString();
                HFIT.Rows[index].Cells[14].Value = tap1.Rows[k]["REFID"].ToString();
                HFIT.Rows[index].Cells[15].Value = tap1.Rows[k]["useitemid"].ToString();
                HFIT.Rows[index].Cells[16].Value = tap1.Rows[k]["socno"].ToString();
                HFIT.Rows[index].Cells[17].Value = tap1.Rows[k]["workorder"].ToString();
                HFIT.Rows[index].Cells[18].Value = tap1.Rows[k]["processid"].ToString();

            }
        }

        private void dtpgrndt_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                e.Handled = true;
                SendKeys.Send("{TAB}");
            }
        }

        private void txtqty_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                e.Handled = true;
                SendKeys.Send("{TAB}");
            }
        }

        private void txtmode_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                e.Handled = true;
                SendKeys.Send("{TAB}");
            }
        }

        private void txtvehicle_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                e.Handled = true;
                SendKeys.Send("{TAB}");
            }
        }

        private void txtdcqty_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                e.Handled = true;
                SendKeys.Send("{TAB}");
            }
        }

        private void txtaddnotes_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                e.Handled = true;
                SendKeys.Send("{TAB}");
            }
        }

        private void txtisstot_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                e.Handled = true;
                SendKeys.Send("{TAB}");
            }
        }

        private void txtbags_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                e.Handled = true;
                SendKeys.Send("{TAB}");
            }
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                e.Handled = true;
                SendKeys.Send("{TAB}");
            }
        }

        private void txtprice_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                e.Handled = true;
                SendKeys.Send("{TAB}");
            }

        }

        private void txtuom_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void button11_Click(object sender, EventArgs e)
        {
            lkppnl.Visible = false;
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void buttnext1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void DataGridCommon_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dtpsupp_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                e.Handled = true;
                SendKeys.Send("{TAB}");
            }
        }

        private void txtscr11_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.F10)
                {
                    lkppnl.Visible = false;
                }
                else if (e.KeyCode == Keys.F2)
                {
                    SelectId = 1;

                    if (Genclass.type == 10)
                    {
                        int Index = HFGP2.SelectedCells[0].RowIndex;

                        txtdcno.Text = HFGP2.Rows[Index].Cells[0].Value.ToString();
                        txtwo.Text = HFGP2.Rows[Index].Cells[1].Value.ToString();
                        txtorderd.Text = HFGP2.Rows[Index].Cells[2].Value.ToString();
                        txtvehicle.Focus();
                    }
                    else
                    {
                        int Index = HFGP2.SelectedCells[0].RowIndex;
                        txtrate.Tag = HFGP2.Rows[Index].Cells[13].Value.ToString();
                        txtdcqty.Text = HFGP2.Rows[Index].Cells[1].Value.ToString();
                        txtdcqty.Tag = HFGP2.Rows[Index].Cells[0].Value.ToString();
                        txtuom.Text = HFGP2.Rows[Index].Cells[3].Value.ToString();
                        txtitemid.Text = HFGP2.Rows[Index].Cells[4].Value.ToString();
                        txtprice.Text = HFGP2.Rows[Index].Cells[5].Value.ToString();
                        txtprice.Tag = HFGP2.Rows[Index].Cells[6].Value.ToString();
                        txtisstot.Text = HFGP2.Rows[Index].Cells[7].Value.ToString();
                        txtrate.Tag = HFGP2.Rows[Index].Cells[14].Value.ToString();
                        lkppnl.Visible = false;
                        txtaddnotes.Focus();
                    }

                    SelectId = 0;
                }
                else if (e.KeyCode == Keys.Escape)
                {
                    lkppnl.Visible = false;
                }
                else if (e.KeyValue == 40)
                {
                    HFGP2.Select();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtscr11_TextChanged(object sender, EventArgs e)
        {

            if (SelectId == 0)
            {
                //if (Genclass.type == 10)
                //{

                //    bsc.Filter = string.Format("docno LIKE '%{0}%' ", txtscr11.Text);
                //}
                //else
                //{

                //    bsc1.Filter = string.Format("Itemdes LIKE '%{0}%' ", txtscr11.Text);
                //}
            }
        }

        private void txtuom_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtuom_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)

            {
                btnadd_Click(sender, e);

            }
        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void txtprice_TextChanged(object sender, EventArgs e)
        {

        }

        private void label15_Click(object sender, EventArgs e)
        {

        }

        private void label21_Click(object sender, EventArgs e)
        {

        }

        private void cbouom_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void label20_Click(object sender, EventArgs e)
        {

        }

        private void cbotype_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtdcno_Click(object sender, EventArgs e)
        {
            if (txtname.Text == string.Empty)
            {
                MessageBox.Show("Select Supplier Name", "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtname.Focus();
                return;
            }
            //if (txtmode.Text == string.Empty)
            //{
            //    MessageBox.Show("Enter Supplier DC Number and Date", "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //    txtmode.Focus();
            //    return;
            //}

            txtscr11.Text = "";

            Genclass.type = 10;


            txtscr11.Focus();

            //Genclass.strsql = "select distinct b.itemid,b.Itemdes as ItemName,a.Docno as Dcno,b.qty- isnull(SUM(d.qty),0) as Pendqty,b.uid,h.generalname,c.uom_uid,b.KNITDIA from jo a inner join   JOBISSUEDETTAB  b on a.uid=B.HEADID   and a.doctypeid=540  		   	 left join itemm c on b.Itemid=c.uid   left join generalm h on c.uom_uid=h.uid left join JORKNITREC d on b.uid=d.REFID 	 left join jo e on d.HEADID=e.uid  and e.Doctypeid=550   left join partym k  on a.supplieruid=k.uid 	 where a.supplieruid=" + txtpuid.Text +"    group by a.Docno,b.qty,b.Itemid,b.Itemdes,b.uid,b.KNITDIA,h.generalname,c.uom_uid  having b.qty- isnull(SUM(d.qty),0)> 0";         
            if (Genclass.Dtype == 570)
            {
                Genclass.strsql = "SP_GETYARNRECDCNO1  " + Genclass.Dtype + "," + txtpuid.Text + "  ";
                Genclass.FSSQLSortStr = "Docno";
            }
            else if (Genclass.Dtype == 590)
            {
                Genclass.strsql = "SP_GETYARNRECDCNO2  " + Genclass.Dtype + "," + txtpuid.Text + "  ";
                Genclass.FSSQLSortStr = "Docno";

            }
            else if (Genclass.Dtype == 610)
            {
                Genclass.strsql = "SP_GETYARNRECDCNO3  " + Genclass.Dtype + "," + txtpuid.Text + "  ";
                Genclass.FSSQLSortStr = "Docno";


            }
            else if (Genclass.Dtype == 630)
            {
                Genclass.strsql = "SP_GETYARNRECDCNO4  " + Genclass.Dtype + "," + txtpuid.Text + "  ";
                Genclass.FSSQLSortStr = "Docno";

            }
            else if (Genclass.Dtype == 220)
            {
                Genclass.strsql = "SP_GETYARNRECDCNO5  " + Genclass.Dtype + "," + txtpuid.Text + "  ";
                Genclass.FSSQLSortStr = "Docno";

            }
            else if (Genclass.Dtype == 960)
            {
                Genclass.strsql = "SP_GETYARNRECDCNO6  " + Genclass.Dtype + "," + txtpuid.Text + "  ";
                Genclass.FSSQLSortStr = "Docno";

            }

            Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
            SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
            DataTable tap = new DataTable();
            aptr.Fill(tap);
            bsc.DataSource = tap;

            Point loc = FindLocation(txtdcno);
            grSearch.Location = new Point(loc.X, loc.Y + 20);
            grSearch.Visible = true;
            DataGridCommon.AutoGenerateColumns = false;
            DataGridCommon.Refresh();
            DataGridCommon.DataSource = null;
            DataGridCommon.Rows.Clear();
            DataGridCommon.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            DataGridCommon.DefaultCellStyle.WrapMode = DataGridViewTriState.True;

            DataGridCommon.ColumnCount = tap.Columns.Count;
            i = 0;

            foreach (DataColumn column in tap.Columns)
            {
                DataGridCommon.Columns[i].Name = column.ColumnName;
                DataGridCommon.Columns[i].HeaderText = column.ColumnName;
                DataGridCommon.Columns[i].DataPropertyName = column.ColumnName;
                i = i + 1;
            }
            DataGridCommon.DataSource = tap;

            DataGridCommon.Columns[0].Width = 100;
            DataGridCommon.Columns[1].Width = 100;

            DataGridCommon.Columns[2].Width = 150;
            DataGridCommon.Columns[3].Visible = false;



            conn.Close();
        }

        private void dtpsupp_ValueChanged(object sender, EventArgs e)
        {

        }

        private void txtdcno_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                e.Handled = true;
                SendKeys.Send("{TAB}");
            }
        }

        private void txtdcno_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.F10)
                {
                    lkppnl.Visible = false;
                }
                else if (e.KeyCode == Keys.F2)
                {
                    SelectId = 1;


                    int Index = HFGP2.SelectedCells[0].RowIndex;

                    txtdcno.Text = HFGP2.Rows[Index].Cells[1].Value.ToString();
                    txtwo.Text = HFGP2.Rows[Index].Cells[0].Value.ToString();
                    txtorderd.Text = HFGP2.Rows[Index].Cells[2].Value.ToString();
                    txtvehicle.Focus();
                    lkppnl.Visible = false;
                    SelectId = 0;
                }
                else if (e.KeyCode == Keys.Escape)
                {
                    lkppnl.Visible = false;
                }
                else if (e.KeyValue == 40)
                {
                    HFGP2.Select();
                }
                else if (e.KeyCode == Keys.Enter)
                {
                    txtdcno_Click(sender, e);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void HFGP2_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void HFIT_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                conn.Close();
                conn.Open();



            }
        }

        private void txtisstot_TextChanged(object sender, EventArgs e)
        {

        }

        private void butcan_Click(object sender, EventArgs e)
        {
            conn.Close();
            conn.Open();


            int i = HFGP.SelectedCells[0].RowIndex;
            Genclass.strsql = "SP_CHECKFABRICGRN  " + HFGP.Rows[i].Cells[0].Value.ToString() + "";
            Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
            SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
            DataTable tap1 = new DataTable();
            aptr1.Fill(tap1);
            if (tap1.Rows.Count == 0)
            {



                qur.CommandText = " EXEC SP_CHECKDELETEFABRICGRN  " + HFGP.Rows[i].Cells[0].Value.ToString() + "";
                qur.ExecuteNonQuery();


            }
            else

            {
                MessageBox.Show("Already Converted QC");
                return;
            }
 

        

            LoadGetJobCard(1);
        }

        private void HFIT_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (HFIT.CurrentRow.Cells[6].Value != null)
            {
                if (HFIT.CurrentCell.ColumnIndex == 6)

                {
                    DataGridViewCell cell = HFIT.CurrentRow.Cells[6];
                    HFIT.CurrentCell = cell;
                    HFIT.BeginEdit(true);
                }
            }
            if (HFIT.CurrentRow.Cells[7].Value != null)
            {
                if (HFIT.CurrentCell.ColumnIndex == 7)

                {
                    DataGridViewCell cell = HFIT.CurrentRow.Cells[7];
                    HFIT.CurrentCell = cell;
                    HFIT.BeginEdit(true);
                }
            }
            if (HFIT.CurrentRow.Cells[8].Value != null)
            {
                if (HFIT.CurrentCell.ColumnIndex == 8)

                {
                    DataGridViewCell cell = HFIT.CurrentRow.Cells[8];
                    HFIT.CurrentCell = cell;
                    HFIT.BeginEdit(true);
                }
            }
        }

        private void chkact_CheckedChanged(object sender, EventArgs e)
        {
            LoadGetJobCard(1);
        }

        private void txtscr1_TextChanged(object sender, EventArgs e)
        {
            bs.Filter = string.Format("DOcno Like '%{0}%'  or  Docdate Like '%{1}%' or  name Like '%{1}%' or  Reference Like '%{1}%' or  WorkOrderNo Like '%{1}%'  or  ProcessName Like '%{1}%'  or  vehicleno Like '%{1}%' ", txtscr1.Text, txtscr1.Text, txtscr1.Text, txtscr1.Text, txtscr1.Text, txtscr1.Text, txtscr1.Text);
        }

        private void txtwo_TextChanged(object sender, EventArgs e)
        {

        }

        private void Chkedtact_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void txtscono_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bsc.Filter = string.Format("socno LIKE '%{0}%' ", txtscono.Text);



                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtscono_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtname.Text == string.Empty)
                {
                    MessageBox.Show("Select Supplier Name", "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtname.Focus();
                    return;
                }
                if (txtmode.Text == string.Empty)
                {
                    MessageBox.Show("Enter Supplier DC Number and Date", "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtmode.Focus();
                    return;
                }
                txtscr11.Text = "";
                Genclass.type = 11;
                txtscr11.Focus();
                if (this.Text == "Fabric Process GRN")
                {
                    Genclass.strsql = "SP_GETJORRECITEMSSOCNO   " + Genclass.Dtype + "," + txtpuid.Text + "";
                    Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                }
                else if (this.Text == "Yarn Process GRN")
                {
                    Genclass.strsql = "SP_GETJORRECITEMSSOCNOyarn   " + Genclass.Dtype + "," + txtpuid.Text + "";
                    Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                }
                SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
                DataTable tap = new DataTable();
                aptr.Fill(tap);
                bsc.DataSource = tap;
                Point loc = FindLocation(txtscono);
                grSearch.Location = new Point(loc.X, loc.Y + 20);
                grSearch.Visible = true;
                DataGridCommon.AutoGenerateColumns = false;
                DataGridCommon.Refresh();
                DataGridCommon.DataSource = null;
                DataGridCommon.Rows.Clear();
                DataGridCommon.ColumnCount = tap.Columns.Count;
                i = 0;
                foreach (DataColumn column in tap.Columns)
                {
                    DataGridCommon.Columns[i].Name = column.ColumnName;
                    DataGridCommon.Columns[i].HeaderText = column.ColumnName;
                    DataGridCommon.Columns[i].DataPropertyName = column.ColumnName;
                    i = i + 1;
                }
                DataGridCommon.DataSource = tap;
                DataGridCommon.Columns[0].Visible = true;
                DataGridCommon.Columns[0].Width = 100;
                DataGridCommon.Columns[1].Width = 100;
                DataGridCommon.Columns[2].Width = 100;
                //DataGridCommon.Columns[3].Width = 100;
                DataGridCommon.Columns[3].Visible = false;
                //DataGridCommon.Columns[5].Visible = false;
                conn.Close();
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
    

