﻿namespace MyEasyBizAPMS
{
    partial class FrmMaterialBom
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Syncfusion.Windows.Forms.Tools.SplitButtonRenderer splitButtonRenderer3 = new Syncfusion.Windows.Forms.Tools.SplitButtonRenderer();
            Syncfusion.Windows.Forms.Tools.SplitButtonRenderer splitButtonRenderer4 = new Syncfusion.Windows.Forms.Tools.SplitButtonRenderer();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.SplitSave = new Syncfusion.Windows.Forms.Tools.SplitButton();
            this.toolBack = new Syncfusion.Windows.Forms.Tools.toolstripitem();
            this.SpliAdd = new Syncfusion.Windows.Forms.Tools.SplitButton();
            this.toolstripEdit = new Syncfusion.Windows.Forms.Tools.toolstripitem();
            this.toolstripClose = new Syncfusion.Windows.Forms.Tools.toolstripitem();
            this.grFront = new System.Windows.Forms.GroupBox();
            this.BtnDelete = new System.Windows.Forms.Button();
            this.BtnPrint = new System.Windows.Forms.Button();
            this.SfdDataGridBOM = new Syncfusion.WinForms.DataGrid.SfDataGrid();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtDcoNo = new System.Windows.Forms.TextBox();
            this.txtOrderNO = new System.Windows.Forms.TextBox();
            this.OrderDate = new System.Windows.Forms.DateTimePicker();
            this.DocDate = new System.Windows.Forms.DateTimePicker();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtCtgy = new System.Windows.Forms.TextBox();
            this.txtItemName = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txtQtyPer = new System.Windows.Forms.TextBox();
            this.txtQtyUsed = new System.Windows.Forms.TextBox();
            this.txtExcess = new System.Windows.Forms.TextBox();
            this.txtRate = new System.Windows.Forms.TextBox();
            this.CmbSts = new System.Windows.Forms.ComboBox();
            this.DataGridMBOMItem = new System.Windows.Forms.DataGridView();
            this.BtnAddItem = new System.Windows.Forms.Button();
            this.CmbType = new System.Windows.Forms.ComboBox();
            this.label15 = new System.Windows.Forms.Label();
            this.tabSizeWise = new Syncfusion.Windows.Forms.Tools.TabPageAdv();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label20 = new System.Windows.Forms.Label();
            this.DataGridSizeCombo = new System.Windows.Forms.DataGridView();
            this.label14 = new System.Windows.Forms.Label();
            this.DataGridSizeWise = new System.Windows.Forms.DataGridView();
            this.tabComboWise = new Syncfusion.Windows.Forms.Tools.TabPageAdv();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label21 = new System.Windows.Forms.Label();
            this.DataGridComboStyle = new System.Windows.Forms.DataGridView();
            this.txtMBomClour = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.DataGridComboWise = new System.Windows.Forms.DataGridView();
            this.tabStyleWise = new Syncfusion.Windows.Forms.Tools.TabPageAdv();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.DataGridStyleWise = new System.Windows.Forms.DataGridView();
            this.tabOrderWise = new Syncfusion.Windows.Forms.Tools.TabPageAdv();
            this.gr = new System.Windows.Forms.GroupBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.DataGridOrderSize = new System.Windows.Forms.DataGridView();
            this.DataGridOrderCombo = new System.Windows.Forms.DataGridView();
            this.DataGridOrderStyle = new System.Windows.Forms.DataGridView();
            this.grSearch = new System.Windows.Forms.GroupBox();
            this.btnSelect = new System.Windows.Forms.Button();
            this.DataGridCommon = new System.Windows.Forms.DataGridView();
            this.btnHide = new System.Windows.Forms.Button();
            this.grBack = new System.Windows.Forms.GroupBox();
            this.cbomaterialtype = new System.Windows.Forms.ComboBox();
            this.label22 = new System.Windows.Forms.Label();
            this.ChckUpdateQty = new System.Windows.Forms.CheckBox();
            this.tabControlAdv1 = new Syncfusion.Windows.Forms.Tools.TabControlAdv();
            this.grFront.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SfdDataGridBOM)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridMBOMItem)).BeginInit();
            this.tabSizeWise.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridSizeCombo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridSizeWise)).BeginInit();
            this.tabComboWise.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridComboStyle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridComboWise)).BeginInit();
            this.tabStyleWise.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridStyleWise)).BeginInit();
            this.tabOrderWise.SuspendLayout();
            this.gr.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridOrderSize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridOrderCombo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridOrderStyle)).BeginInit();
            this.grSearch.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridCommon)).BeginInit();
            this.grBack.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tabControlAdv1)).BeginInit();
            this.tabControlAdv1.SuspendLayout();
            this.SuspendLayout();
            // 
            // SplitSave
            // 
            this.SplitSave.BackColor = System.Drawing.SystemColors.Control;
            this.SplitSave.BeforeTouchSize = new System.Drawing.Size(93, 34);
            this.SplitSave.DropDownItems.Add(this.toolBack);
            this.SplitSave.DropDownPosition = Syncfusion.Windows.Forms.Tools.Position.Bottom;
            this.SplitSave.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SplitSave.ForeColor = System.Drawing.Color.Black;
            this.SplitSave.Location = new System.Drawing.Point(880, 560);
            this.SplitSave.MinimumSize = new System.Drawing.Size(75, 23);
            this.SplitSave.Name = "SplitSave";
            splitButtonRenderer3.SplitButton = this.SplitSave;
            this.SplitSave.Renderer = splitButtonRenderer3;
            this.SplitSave.ShowDropDownOnButtonClick = false;
            this.SplitSave.Size = new System.Drawing.Size(93, 34);
            this.SplitSave.TabIndex = 35;
            this.SplitSave.Text = "Save";
            this.SplitSave.DropDowItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.SplitSave_DropDowItemClicked);
            this.SplitSave.Click += new System.EventHandler(this.SplitSave_Click_1);
            // 
            // toolBack
            // 
            this.toolBack.Name = "toolBack";
            this.toolBack.Size = new System.Drawing.Size(23, 23);
            this.toolBack.Text = "Back";
            // 
            // SpliAdd
            // 
            this.SpliAdd.BackColor = System.Drawing.SystemColors.Control;
            this.SpliAdd.BeforeTouchSize = new System.Drawing.Size(92, 34);
            this.SpliAdd.DropDownItems.Add(this.toolstripEdit);
            this.SpliAdd.DropDownItems.Add(this.toolstripClose);
            this.SpliAdd.DropDownPosition = Syncfusion.Windows.Forms.Tools.Position.Bottom;
            this.SpliAdd.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SpliAdd.ForeColor = System.Drawing.Color.Black;
            this.SpliAdd.Location = new System.Drawing.Point(963, 556);
            this.SpliAdd.MinimumSize = new System.Drawing.Size(75, 23);
            this.SpliAdd.Name = "SpliAdd";
            splitButtonRenderer4.SplitButton = this.SpliAdd;
            this.SpliAdd.Renderer = splitButtonRenderer4;
            this.SpliAdd.ShowDropDownOnButtonClick = false;
            this.SpliAdd.Size = new System.Drawing.Size(92, 34);
            this.SpliAdd.TabIndex = 35;
            this.SpliAdd.Text = "Add";
            this.SpliAdd.DropDowItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.SpliAdd_DropDowItemClicked);
            this.SpliAdd.Click += new System.EventHandler(this.SpliAdd_Click);
            // 
            // toolstripEdit
            // 
            this.toolstripEdit.Name = "toolstripEdit";
            this.toolstripEdit.Size = new System.Drawing.Size(23, 23);
            this.toolstripEdit.Text = "Edit";
            // 
            // toolstripClose
            // 
            this.toolstripClose.Name = "toolstripClose";
            this.toolstripClose.Size = new System.Drawing.Size(23, 23);
            this.toolstripClose.Text = "Close";
            // 
            // grFront
            // 
            this.grFront.BackColor = System.Drawing.Color.White;
            this.grFront.Controls.Add(this.BtnDelete);
            this.grFront.Controls.Add(this.BtnPrint);
            this.grFront.Controls.Add(this.SpliAdd);
            this.grFront.Controls.Add(this.SfdDataGridBOM);
            this.grFront.Location = new System.Drawing.Point(6, 1);
            this.grFront.Name = "grFront";
            this.grFront.Size = new System.Drawing.Size(1146, 597);
            this.grFront.TabIndex = 35;
            this.grFront.TabStop = false;
            // 
            // BtnDelete
            // 
            this.BtnDelete.Location = new System.Drawing.Point(139, 560);
            this.BtnDelete.Name = "BtnDelete";
            this.BtnDelete.Size = new System.Drawing.Size(85, 33);
            this.BtnDelete.TabIndex = 37;
            this.BtnDelete.Text = "Delete";
            this.BtnDelete.UseVisualStyleBackColor = true;
            this.BtnDelete.Click += new System.EventHandler(this.BtnDelete_Click);
            // 
            // BtnPrint
            // 
            this.BtnPrint.Location = new System.Drawing.Point(6, 558);
            this.BtnPrint.Name = "BtnPrint";
            this.BtnPrint.Size = new System.Drawing.Size(85, 33);
            this.BtnPrint.TabIndex = 36;
            this.BtnPrint.Text = "Print";
            this.BtnPrint.UseVisualStyleBackColor = true;
            this.BtnPrint.Click += new System.EventHandler(this.BtnPrint_Click);
            // 
            // SfdDataGridBOM
            // 
            this.SfdDataGridBOM.AccessibleName = "Table";
            this.SfdDataGridBOM.AllowFiltering = true;
            this.SfdDataGridBOM.Location = new System.Drawing.Point(5, 14);
            this.SfdDataGridBOM.Name = "SfdDataGridBOM";
            this.SfdDataGridBOM.Size = new System.Drawing.Size(1135, 540);
            this.SfdDataGridBOM.TabIndex = 0;
            this.SfdDataGridBOM.Text = "sfDataGrid1";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(136, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "Doc No";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(252, 10);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 15);
            this.label2.TabIndex = 1;
            this.label2.Text = "Doc Date";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(23, 11);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(59, 15);
            this.label4.TabIndex = 2;
            this.label4.Text = "Order No";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(366, 10);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(56, 15);
            this.label3.TabIndex = 3;
            this.label3.Text = "Order Dt";
            // 
            // txtDcoNo
            // 
            this.txtDcoNo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.txtDcoNo.Enabled = false;
            this.txtDcoNo.Location = new System.Drawing.Point(136, 31);
            this.txtDcoNo.Name = "txtDcoNo";
            this.txtDcoNo.Size = new System.Drawing.Size(109, 23);
            this.txtDcoNo.TabIndex = 5;
            // 
            // txtOrderNO
            // 
            this.txtOrderNO.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.txtOrderNO.Location = new System.Drawing.Point(26, 31);
            this.txtOrderNO.Name = "txtOrderNO";
            this.txtOrderNO.Size = new System.Drawing.Size(108, 23);
            this.txtOrderNO.TabIndex = 6;
            this.txtOrderNO.Click += new System.EventHandler(this.TxtOrderNO_Click);
            this.txtOrderNO.TextChanged += new System.EventHandler(this.TxtOrderNO_TextChanged);
            this.txtOrderNO.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TxtOrderNO_KeyDown);
            // 
            // OrderDate
            // 
            this.OrderDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.OrderDate.Location = new System.Drawing.Point(364, 31);
            this.OrderDate.Name = "OrderDate";
            this.OrderDate.Size = new System.Drawing.Size(111, 23);
            this.OrderDate.TabIndex = 7;
            // 
            // DocDate
            // 
            this.DocDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.DocDate.Location = new System.Drawing.Point(247, 31);
            this.DocDate.Name = "DocDate";
            this.DocDate.Size = new System.Drawing.Size(111, 23);
            this.DocDate.TabIndex = 8;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(23, 57);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(56, 15);
            this.label6.TabIndex = 10;
            this.label6.Text = "Category";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(254, 56);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(69, 15);
            this.label7.TabIndex = 11;
            this.label7.Text = "Item Name";
            // 
            // txtCtgy
            // 
            this.txtCtgy.Location = new System.Drawing.Point(26, 73);
            this.txtCtgy.Name = "txtCtgy";
            this.txtCtgy.Size = new System.Drawing.Size(217, 23);
            this.txtCtgy.TabIndex = 13;
            this.txtCtgy.Click += new System.EventHandler(this.TxtCtgy_Click);
            this.txtCtgy.TextChanged += new System.EventHandler(this.TxtCtgy_TextChanged);
            // 
            // txtItemName
            // 
            this.txtItemName.BackColor = System.Drawing.Color.White;
            this.txtItemName.Location = new System.Drawing.Point(247, 73);
            this.txtItemName.Name = "txtItemName";
            this.txtItemName.ReadOnly = true;
            this.txtItemName.Size = new System.Drawing.Size(301, 23);
            this.txtItemName.TabIndex = 14;
            this.txtItemName.Click += new System.EventHandler(this.TxtItemName_Click);
            this.txtItemName.TextChanged += new System.EventHandler(this.TxtItemName_TextChanged);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(989, 97);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(49, 15);
            this.label13.TabIndex = 17;
            this.label13.Text = "Qty Per";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(989, 141);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(57, 15);
            this.label12.TabIndex = 18;
            this.label12.Text = "Qty Used";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(989, 190);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(53, 15);
            this.label11.TabIndex = 19;
            this.label11.Text = "% Excess";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(731, 58);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(32, 15);
            this.label10.TabIndex = 20;
            this.label10.Text = "Rate";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(824, 58);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(41, 15);
            this.label9.TabIndex = 21;
            this.label9.Text = "Status";
            // 
            // txtQtyPer
            // 
            this.txtQtyPer.Location = new System.Drawing.Point(984, 115);
            this.txtQtyPer.Name = "txtQtyPer";
            this.txtQtyPer.Size = new System.Drawing.Size(99, 23);
            this.txtQtyPer.TabIndex = 22;
            // 
            // txtQtyUsed
            // 
            this.txtQtyUsed.Location = new System.Drawing.Point(986, 162);
            this.txtQtyUsed.Name = "txtQtyUsed";
            this.txtQtyUsed.Size = new System.Drawing.Size(89, 23);
            this.txtQtyUsed.TabIndex = 23;
            // 
            // txtExcess
            // 
            this.txtExcess.Location = new System.Drawing.Point(986, 211);
            this.txtExcess.Name = "txtExcess";
            this.txtExcess.Size = new System.Drawing.Size(89, 23);
            this.txtExcess.TabIndex = 24;
            // 
            // txtRate
            // 
            this.txtRate.Location = new System.Drawing.Point(724, 73);
            this.txtRate.Name = "txtRate";
            this.txtRate.Size = new System.Drawing.Size(91, 23);
            this.txtRate.TabIndex = 25;
            // 
            // CmbSts
            // 
            this.CmbSts.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CmbSts.FormattingEnabled = true;
            this.CmbSts.Items.AddRange(new object[] {
            "Confirmed",
            "To Be Confirmed"});
            this.CmbSts.Location = new System.Drawing.Point(817, 73);
            this.CmbSts.Name = "CmbSts";
            this.CmbSts.Size = new System.Drawing.Size(120, 23);
            this.CmbSts.TabIndex = 26;
            // 
            // DataGridMBOMItem
            // 
            this.DataGridMBOMItem.AllowUserToAddRows = false;
            this.DataGridMBOMItem.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.DeepSkyBlue;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DataGridMBOMItem.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.DataGridMBOMItem.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridMBOMItem.EnableHeadersVisualStyles = false;
            this.DataGridMBOMItem.Location = new System.Drawing.Point(6, 309);
            this.DataGridMBOMItem.Name = "DataGridMBOMItem";
            this.DataGridMBOMItem.RowHeadersVisible = false;
            this.DataGridMBOMItem.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridMBOMItem.Size = new System.Drawing.Size(1134, 248);
            this.DataGridMBOMItem.TabIndex = 29;
            this.DataGridMBOMItem.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridMBOMItem_CellClick);
            this.DataGridMBOMItem.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridMBOMItem_CellValueChanged);
            this.DataGridMBOMItem.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DataGridMBOMItem_KeyDown);
            // 
            // BtnAddItem
            // 
            this.BtnAddItem.Location = new System.Drawing.Point(1000, 274);
            this.BtnAddItem.Name = "BtnAddItem";
            this.BtnAddItem.Size = new System.Drawing.Size(75, 23);
            this.BtnAddItem.TabIndex = 30;
            this.BtnAddItem.Text = "Add Item";
            this.BtnAddItem.UseVisualStyleBackColor = true;
            this.BtnAddItem.Click += new System.EventHandler(this.BtnAddItem_Click);
            // 
            // CmbType
            // 
            this.CmbType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CmbType.FormattingEnabled = true;
            this.CmbType.Items.AddRange(new object[] {
            "Order Wise",
            "Style Wise",
            "Combo Wise",
            "Size Wise"});
            this.CmbType.Location = new System.Drawing.Point(551, 73);
            this.CmbType.Name = "CmbType";
            this.CmbType.Size = new System.Drawing.Size(168, 23);
            this.CmbType.TabIndex = 31;
            this.CmbType.SelectedIndexChanged += new System.EventHandler(this.CmbCategory_SelectedIndexChanged);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(558, 56);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(33, 15);
            this.label15.TabIndex = 32;
            this.label15.Text = "Type";
            // 
            // tabSizeWise
            // 
            this.tabSizeWise.Controls.Add(this.groupBox3);
            this.tabSizeWise.Image = null;
            this.tabSizeWise.ImageSize = new System.Drawing.Size(16, 16);
            this.tabSizeWise.Location = new System.Drawing.Point(1, 27);
            this.tabSizeWise.Name = "tabSizeWise";
            this.tabSizeWise.ShowCloseButton = true;
            this.tabSizeWise.Size = new System.Drawing.Size(969, 169);
            this.tabSizeWise.TabIndex = 4;
            this.tabSizeWise.Text = "Size Wise";
            this.tabSizeWise.ThemesEnabled = false;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label20);
            this.groupBox3.Controls.Add(this.DataGridSizeCombo);
            this.groupBox3.Controls.Add(this.label14);
            this.groupBox3.Controls.Add(this.DataGridSizeWise);
            this.groupBox3.Location = new System.Drawing.Point(3, 4);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(963, 164);
            this.groupBox3.TabIndex = 1;
            this.groupBox3.TabStop = false;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(10, 11);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(46, 15);
            this.label20.TabIndex = 15;
            this.label20.Text = "Combo";
            // 
            // DataGridSizeCombo
            // 
            this.DataGridSizeCombo.AllowUserToAddRows = false;
            this.DataGridSizeCombo.BackgroundColor = System.Drawing.Color.White;
            this.DataGridSizeCombo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridSizeCombo.Location = new System.Drawing.Point(58, 12);
            this.DataGridSizeCombo.Name = "DataGridSizeCombo";
            this.DataGridSizeCombo.RowHeadersVisible = false;
            this.DataGridSizeCombo.Size = new System.Drawing.Size(240, 150);
            this.DataGridSizeCombo.TabIndex = 14;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(326, 13);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(28, 15);
            this.label14.TabIndex = 13;
            this.label14.Text = "Size";
            // 
            // DataGridSizeWise
            // 
            this.DataGridSizeWise.AllowUserToAddRows = false;
            this.DataGridSizeWise.BackgroundColor = System.Drawing.Color.White;
            this.DataGridSizeWise.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridSizeWise.Location = new System.Drawing.Point(360, 11);
            this.DataGridSizeWise.Name = "DataGridSizeWise";
            this.DataGridSizeWise.RowHeadersVisible = false;
            this.DataGridSizeWise.Size = new System.Drawing.Size(478, 150);
            this.DataGridSizeWise.TabIndex = 2;
            // 
            // tabComboWise
            // 
            this.tabComboWise.Controls.Add(this.groupBox2);
            this.tabComboWise.Image = null;
            this.tabComboWise.ImageSize = new System.Drawing.Size(16, 16);
            this.tabComboWise.Location = new System.Drawing.Point(1, 27);
            this.tabComboWise.Name = "tabComboWise";
            this.tabComboWise.ShowCloseButton = true;
            this.tabComboWise.Size = new System.Drawing.Size(969, 169);
            this.tabComboWise.TabIndex = 3;
            this.tabComboWise.Text = "Combo Wise";
            this.tabComboWise.ThemesEnabled = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label21);
            this.groupBox2.Controls.Add(this.DataGridComboStyle);
            this.groupBox2.Controls.Add(this.txtMBomClour);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.label19);
            this.groupBox2.Controls.Add(this.DataGridComboWise);
            this.groupBox2.Location = new System.Drawing.Point(3, 4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(963, 164);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(3, 12);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(39, 15);
            this.label21.TabIndex = 40;
            this.label21.Text = "Styles";
            // 
            // DataGridComboStyle
            // 
            this.DataGridComboStyle.AllowUserToAddRows = false;
            this.DataGridComboStyle.BackgroundColor = System.Drawing.Color.White;
            this.DataGridComboStyle.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridComboStyle.Location = new System.Drawing.Point(48, 10);
            this.DataGridComboStyle.Name = "DataGridComboStyle";
            this.DataGridComboStyle.RowHeadersVisible = false;
            this.DataGridComboStyle.Size = new System.Drawing.Size(240, 150);
            this.DataGridComboStyle.TabIndex = 39;
            // 
            // txtMBomClour
            // 
            this.txtMBomClour.Location = new System.Drawing.Point(589, 33);
            this.txtMBomClour.Name = "txtMBomClour";
            this.txtMBomClour.Size = new System.Drawing.Size(154, 23);
            this.txtMBomClour.TabIndex = 38;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(293, 12);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(46, 15);
            this.label8.TabIndex = 12;
            this.label8.Text = "Combo";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(592, 12);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(43, 15);
            this.label19.TabIndex = 37;
            this.label19.Text = "Colour";
            // 
            // DataGridComboWise
            // 
            this.DataGridComboWise.AllowUserToAddRows = false;
            this.DataGridComboWise.BackgroundColor = System.Drawing.Color.White;
            this.DataGridComboWise.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridComboWise.Location = new System.Drawing.Point(341, 11);
            this.DataGridComboWise.Name = "DataGridComboWise";
            this.DataGridComboWise.RowHeadersVisible = false;
            this.DataGridComboWise.Size = new System.Drawing.Size(240, 150);
            this.DataGridComboWise.TabIndex = 2;
            // 
            // tabStyleWise
            // 
            this.tabStyleWise.Controls.Add(this.groupBox1);
            this.tabStyleWise.Image = null;
            this.tabStyleWise.ImageSize = new System.Drawing.Size(16, 16);
            this.tabStyleWise.Location = new System.Drawing.Point(1, 27);
            this.tabStyleWise.Name = "tabStyleWise";
            this.tabStyleWise.ShowCloseButton = true;
            this.tabStyleWise.Size = new System.Drawing.Size(969, 169);
            this.tabStyleWise.TabIndex = 2;
            this.tabStyleWise.Text = "Style Wise";
            this.tabStyleWise.ThemesEnabled = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.DataGridStyleWise);
            this.groupBox1.Location = new System.Drawing.Point(3, 4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(963, 163);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(37, 12);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(39, 15);
            this.label5.TabIndex = 11;
            this.label5.Text = "Styles";
            // 
            // DataGridStyleWise
            // 
            this.DataGridStyleWise.AllowUserToAddRows = false;
            this.DataGridStyleWise.BackgroundColor = System.Drawing.Color.White;
            this.DataGridStyleWise.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridStyleWise.Location = new System.Drawing.Point(82, 10);
            this.DataGridStyleWise.Name = "DataGridStyleWise";
            this.DataGridStyleWise.RowHeadersVisible = false;
            this.DataGridStyleWise.Size = new System.Drawing.Size(240, 150);
            this.DataGridStyleWise.TabIndex = 2;
            // 
            // tabOrderWise
            // 
            this.tabOrderWise.Controls.Add(this.gr);
            this.tabOrderWise.Image = null;
            this.tabOrderWise.ImageSize = new System.Drawing.Size(16, 16);
            this.tabOrderWise.Location = new System.Drawing.Point(1, 27);
            this.tabOrderWise.Name = "tabOrderWise";
            this.tabOrderWise.ShowCloseButton = true;
            this.tabOrderWise.Size = new System.Drawing.Size(969, 169);
            this.tabOrderWise.TabIndex = 1;
            this.tabOrderWise.Text = "Order Wise";
            this.tabOrderWise.ThemesEnabled = false;
            // 
            // gr
            // 
            this.gr.Controls.Add(this.label16);
            this.gr.Controls.Add(this.label18);
            this.gr.Controls.Add(this.label17);
            this.gr.Controls.Add(this.DataGridOrderSize);
            this.gr.Controls.Add(this.DataGridOrderCombo);
            this.gr.Controls.Add(this.DataGridOrderStyle);
            this.gr.Location = new System.Drawing.Point(3, 2);
            this.gr.Name = "gr";
            this.gr.Size = new System.Drawing.Size(963, 164);
            this.gr.TabIndex = 0;
            this.gr.TabStop = false;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(12, 17);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(39, 15);
            this.label16.TabIndex = 40;
            this.label16.Text = "Styles";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(661, 17);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(28, 15);
            this.label18.TabIndex = 39;
            this.label18.Text = "Size";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(332, 17);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(46, 15);
            this.label17.TabIndex = 38;
            this.label17.Text = "Combo";
            // 
            // DataGridOrderSize
            // 
            this.DataGridOrderSize.AllowUserToAddRows = false;
            this.DataGridOrderSize.BackgroundColor = System.Drawing.Color.White;
            this.DataGridOrderSize.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridOrderSize.Location = new System.Drawing.Point(697, 10);
            this.DataGridOrderSize.Name = "DataGridOrderSize";
            this.DataGridOrderSize.RowHeadersVisible = false;
            this.DataGridOrderSize.Size = new System.Drawing.Size(247, 150);
            this.DataGridOrderSize.TabIndex = 2;
            // 
            // DataGridOrderCombo
            // 
            this.DataGridOrderCombo.AllowUserToAddRows = false;
            this.DataGridOrderCombo.BackgroundColor = System.Drawing.Color.White;
            this.DataGridOrderCombo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridOrderCombo.Location = new System.Drawing.Point(383, 10);
            this.DataGridOrderCombo.Name = "DataGridOrderCombo";
            this.DataGridOrderCombo.RowHeadersVisible = false;
            this.DataGridOrderCombo.Size = new System.Drawing.Size(247, 150);
            this.DataGridOrderCombo.TabIndex = 1;
            // 
            // DataGridOrderStyle
            // 
            this.DataGridOrderStyle.AllowUserToAddRows = false;
            this.DataGridOrderStyle.BackgroundColor = System.Drawing.Color.White;
            this.DataGridOrderStyle.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridOrderStyle.Location = new System.Drawing.Point(57, 10);
            this.DataGridOrderStyle.Name = "DataGridOrderStyle";
            this.DataGridOrderStyle.RowHeadersVisible = false;
            this.DataGridOrderStyle.Size = new System.Drawing.Size(247, 150);
            this.DataGridOrderStyle.TabIndex = 0;
            // 
            // grSearch
            // 
            this.grSearch.Controls.Add(this.btnSelect);
            this.grSearch.Controls.Add(this.DataGridCommon);
            this.grSearch.Controls.Add(this.btnHide);
            this.grSearch.Location = new System.Drawing.Point(25, 56);
            this.grSearch.Name = "grSearch";
            this.grSearch.Size = new System.Drawing.Size(346, 229);
            this.grSearch.TabIndex = 33;
            this.grSearch.TabStop = false;
            this.grSearch.Visible = false;
            // 
            // btnSelect
            // 
            this.btnSelect.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnSelect.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSelect.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnSelect.Location = new System.Drawing.Point(281, 197);
            this.btnSelect.Name = "btnSelect";
            this.btnSelect.Size = new System.Drawing.Size(61, 28);
            this.btnSelect.TabIndex = 396;
            this.btnSelect.Text = "Select";
            this.btnSelect.UseVisualStyleBackColor = false;
            this.btnSelect.Click += new System.EventHandler(this.BtnSelect_Click);
            // 
            // DataGridCommon
            // 
            this.DataGridCommon.AllowUserToAddRows = false;
            this.DataGridCommon.BackgroundColor = System.Drawing.Color.White;
            this.DataGridCommon.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridCommon.Location = new System.Drawing.Point(4, 12);
            this.DataGridCommon.MultiSelect = false;
            this.DataGridCommon.Name = "DataGridCommon";
            this.DataGridCommon.ReadOnly = true;
            this.DataGridCommon.RowHeadersVisible = false;
            this.DataGridCommon.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridCommon.Size = new System.Drawing.Size(336, 183);
            this.DataGridCommon.TabIndex = 0;
            this.DataGridCommon.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.DataGridCommon_CellMouseDoubleClick);
            // 
            // btnHide
            // 
            this.btnHide.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnHide.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHide.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnHide.Location = new System.Drawing.Point(6, 197);
            this.btnHide.Name = "btnHide";
            this.btnHide.Size = new System.Drawing.Size(65, 28);
            this.btnHide.TabIndex = 395;
            this.btnHide.Text = "Close";
            this.btnHide.UseVisualStyleBackColor = false;
            this.btnHide.Click += new System.EventHandler(this.BtnHide_Click);
            // 
            // grBack
            // 
            this.grBack.BackColor = System.Drawing.Color.White;
            this.grBack.Controls.Add(this.cbomaterialtype);
            this.grBack.Controls.Add(this.label22);
            this.grBack.Controls.Add(this.ChckUpdateQty);
            this.grBack.Controls.Add(this.grSearch);
            this.grBack.Controls.Add(this.SplitSave);
            this.grBack.Controls.Add(this.label15);
            this.grBack.Controls.Add(this.CmbType);
            this.grBack.Controls.Add(this.BtnAddItem);
            this.grBack.Controls.Add(this.DataGridMBOMItem);
            this.grBack.Controls.Add(this.CmbSts);
            this.grBack.Controls.Add(this.txtRate);
            this.grBack.Controls.Add(this.txtExcess);
            this.grBack.Controls.Add(this.txtQtyUsed);
            this.grBack.Controls.Add(this.txtQtyPer);
            this.grBack.Controls.Add(this.label9);
            this.grBack.Controls.Add(this.label10);
            this.grBack.Controls.Add(this.label11);
            this.grBack.Controls.Add(this.label12);
            this.grBack.Controls.Add(this.label13);
            this.grBack.Controls.Add(this.txtItemName);
            this.grBack.Controls.Add(this.txtCtgy);
            this.grBack.Controls.Add(this.label7);
            this.grBack.Controls.Add(this.label6);
            this.grBack.Controls.Add(this.DocDate);
            this.grBack.Controls.Add(this.OrderDate);
            this.grBack.Controls.Add(this.txtOrderNO);
            this.grBack.Controls.Add(this.txtDcoNo);
            this.grBack.Controls.Add(this.label3);
            this.grBack.Controls.Add(this.label4);
            this.grBack.Controls.Add(this.label2);
            this.grBack.Controls.Add(this.label1);
            this.grBack.Controls.Add(this.tabControlAdv1);
            this.grBack.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grBack.Location = new System.Drawing.Point(6, 1);
            this.grBack.Name = "grBack";
            this.grBack.Size = new System.Drawing.Size(1146, 596);
            this.grBack.TabIndex = 0;
            this.grBack.TabStop = false;
            // 
            // cbomaterialtype
            // 
            this.cbomaterialtype.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbomaterialtype.FormattingEnabled = true;
            this.cbomaterialtype.Items.AddRange(new object[] {
            "Purchase",
            "Process",
            "Conversion"});
            this.cbomaterialtype.Location = new System.Drawing.Point(943, 73);
            this.cbomaterialtype.Name = "cbomaterialtype";
            this.cbomaterialtype.Size = new System.Drawing.Size(140, 23);
            this.cbomaterialtype.TabIndex = 39;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(950, 58);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(82, 15);
            this.label22.TabIndex = 38;
            this.label22.Text = "Material Type";
            // 
            // ChckUpdateQty
            // 
            this.ChckUpdateQty.AutoSize = true;
            this.ChckUpdateQty.Location = new System.Drawing.Point(5, 567);
            this.ChckUpdateQty.Name = "ChckUpdateQty";
            this.ChckUpdateQty.Size = new System.Drawing.Size(87, 19);
            this.ChckUpdateQty.TabIndex = 37;
            this.ChckUpdateQty.Text = "Update Qty";
            this.ChckUpdateQty.UseVisualStyleBackColor = true;
            this.ChckUpdateQty.CheckedChanged += new System.EventHandler(this.ChckUpdateQty_CheckedChanged);
            // 
            // tabControlAdv1
            // 
            this.tabControlAdv1.ActiveTabForeColor = System.Drawing.Color.Empty;
            this.tabControlAdv1.BeforeTouchSize = new System.Drawing.Size(972, 198);
            this.tabControlAdv1.CloseButtonForeColor = System.Drawing.Color.Empty;
            this.tabControlAdv1.CloseButtonHoverForeColor = System.Drawing.Color.Empty;
            this.tabControlAdv1.CloseButtonPressedForeColor = System.Drawing.Color.Empty;
            this.tabControlAdv1.Controls.Add(this.tabOrderWise);
            this.tabControlAdv1.Controls.Add(this.tabStyleWise);
            this.tabControlAdv1.Controls.Add(this.tabComboWise);
            this.tabControlAdv1.Controls.Add(this.tabSizeWise);
            this.tabControlAdv1.InActiveTabForeColor = System.Drawing.Color.Empty;
            this.tabControlAdv1.Location = new System.Drawing.Point(6, 99);
            this.tabControlAdv1.Name = "tabControlAdv1";
            this.tabControlAdv1.SeparatorColor = System.Drawing.SystemColors.ControlDark;
            this.tabControlAdv1.ShowSeparator = false;
            this.tabControlAdv1.Size = new System.Drawing.Size(972, 198);
            this.tabControlAdv1.TabIndex = 36;
            // 
            // FrmMaterialBom
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1156, 603);
            this.Controls.Add(this.grFront);
            this.Controls.Add(this.grBack);
            this.Name = "FrmMaterialBom";
            this.Text = "Material Bom";
            this.Load += new System.EventHandler(this.FrmMaterialBom_Load);
            this.grFront.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.SfdDataGridBOM)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridMBOMItem)).EndInit();
            this.tabSizeWise.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridSizeCombo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridSizeWise)).EndInit();
            this.tabComboWise.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridComboStyle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridComboWise)).EndInit();
            this.tabStyleWise.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridStyleWise)).EndInit();
            this.tabOrderWise.ResumeLayout(false);
            this.gr.ResumeLayout(false);
            this.gr.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridOrderSize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridOrderCombo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridOrderStyle)).EndInit();
            this.grSearch.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DataGridCommon)).EndInit();
            this.grBack.ResumeLayout(false);
            this.grBack.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tabControlAdv1)).EndInit();
            this.tabControlAdv1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private Syncfusion.Windows.Forms.Tools.toolstripitem toolBack;
        private System.Windows.Forms.GroupBox grFront;
        private Syncfusion.Windows.Forms.Tools.toolstripitem toolstripEdit;
        private Syncfusion.Windows.Forms.Tools.toolstripitem toolstripClose;
        private Syncfusion.WinForms.DataGrid.SfDataGrid SfdDataGridBOM;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtDcoNo;
        private System.Windows.Forms.TextBox txtOrderNO;
        private System.Windows.Forms.DateTimePicker OrderDate;
        private System.Windows.Forms.DateTimePicker DocDate;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtCtgy;
        private System.Windows.Forms.TextBox txtItemName;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtQtyPer;
        private System.Windows.Forms.TextBox txtQtyUsed;
        private System.Windows.Forms.TextBox txtExcess;
        private System.Windows.Forms.TextBox txtRate;
        private System.Windows.Forms.ComboBox CmbSts;
        private System.Windows.Forms.DataGridView DataGridMBOMItem;
        private System.Windows.Forms.Button BtnAddItem;
        private System.Windows.Forms.ComboBox CmbType;
        private System.Windows.Forms.Label label15;
        private Syncfusion.Windows.Forms.Tools.SplitButton SplitSave;
        private Syncfusion.Windows.Forms.Tools.TabPageAdv tabSizeWise;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.DataGridView DataGridSizeWise;
        private Syncfusion.Windows.Forms.Tools.TabPageAdv tabComboWise;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.DataGridView DataGridComboWise;
        private Syncfusion.Windows.Forms.Tools.TabPageAdv tabStyleWise;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DataGridView DataGridStyleWise;
        private Syncfusion.Windows.Forms.Tools.TabPageAdv tabOrderWise;
        private System.Windows.Forms.GroupBox gr;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.DataGridView DataGridOrderSize;
        private System.Windows.Forms.DataGridView DataGridOrderCombo;
        private System.Windows.Forms.DataGridView DataGridOrderStyle;
        private System.Windows.Forms.GroupBox grSearch;
        private System.Windows.Forms.Button btnSelect;
        private System.Windows.Forms.DataGridView DataGridCommon;
        private System.Windows.Forms.Button btnHide;
        private System.Windows.Forms.GroupBox grBack;
        private Syncfusion.Windows.Forms.Tools.TabControlAdv tabControlAdv1;
        private System.Windows.Forms.TextBox txtMBomClour;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.DataGridView DataGridSizeCombo;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.DataGridView DataGridComboStyle;
        private System.Windows.Forms.Button BtnPrint;
        private System.Windows.Forms.Button BtnDelete;
        private Syncfusion.Windows.Forms.Tools.SplitButton SpliAdd;
        private System.Windows.Forms.CheckBox ChckUpdateQty;
        private System.Windows.Forms.ComboBox cbomaterialtype;
        private System.Windows.Forms.Label label22;
    }
}