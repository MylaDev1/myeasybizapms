﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MyEasyBizAPMS
{
    public partial class FrmClassification : Form
    {
        public FrmClassification()
        {
            InitializeComponent();
        }

        private void FrmClassification_Load(object sender, EventArgs e)
        {
            LoadClassification();
        }

        protected void LoadClassification()
        {
            try
            {
                DataGridClassification.DataSource = null;
                DataGridClassification.AutoGenerateColumns = false;
                DataGridClassification.ColumnCount = 1;
                DataGridClassification.Columns[0].Name = "Type";
                DataGridClassification.Columns[0].HeaderText = "Type";
                DataGridClassification.Columns[0].Width = 250;

                DataGridViewCheckBoxColumn checkBoxColumn = new DataGridViewCheckBoxColumn();
                checkBoxColumn.Name = "System Defined";
                checkBoxColumn.HeaderText = "System Defined";
                checkBoxColumn.Width = 100;
                DataGridClassification.Columns.Insert(1, checkBoxColumn);
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void tabControl1_Click(object sender, EventArgs e)
        {
            if(tabControl1.TabPages[1].Name == "tabSystemAttribute")
            {
                cmbType.Items.Clear();
                LoadS();
                for (int i = 0; i < DataGridClassification.Rows.Count - 1; i++)
                {
                    cmbType.Items.Add(DataGridClassification.Rows[i].Cells[0].Value.ToString());
                }
            }
        }

        protected void LoadS()
        {
            try
            {
                DataGridSys.DataSource = null;
                DataGridSys.AutoGenerateColumns = false;
                DataGridSys.ColumnCount = 1;
                DataGridSys.Columns[0].Name = "Type";
                DataGridSys.Columns[0].HeaderText = "Type";
                DataGridSys.Columns[0].Width = 250;

                DataGridViewComboBoxColumn dgvCmb = new DataGridViewComboBoxColumn();
                dgvCmb.HeaderText = "Value";
                dgvCmb.Items.Add("2");
                dgvCmb.Items.Add("3");
                dgvCmb.Items.Add("4");
                dgvCmb.Items.Add("5");
                dgvCmb.Name = "Value";
                DataGridSys.Columns.Add(dgvCmb);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
    }
}
