﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Drawing.Imaging;
using System.Configuration;
using System.Data.SqlClient;
using System.Diagnostics;
using excel = Microsoft.Office.Interop.Excel;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;

namespace MyEasyBizAPMS
{
    public partial class FrmCuttingIss : Form
    {
        public FrmCuttingIss()
        {
            InitializeComponent();
        }
        int LoadId = 0;
        string uid = "";
        int FillId;
        int mode = 0;
        string tpuid = "";
        int Dtype = 0;
        int i;
        int k;
        int h;
        double sum1;

        SqlConnection conn = new SqlConnection(GeneralParameters.ConnectionString);
        SqlCommand qur = new SqlCommand();
        private DataRow doc1;
        private DataRow doc2;
        BindingSource bs = new BindingSource();
        DataTable Docno = new DataTable();
        DataTable Docno1 = new DataTable();
        BindingSource bsc = new BindingSource();
        BindingSource bsp = new BindingSource();
        BindingSource bsserial = new BindingSource();
        BindingSource IN = new BindingSource();
        BindingSource OUT = new BindingSource();
        BindingSource bsFabric = new BindingSource();
        SQLDBHelper db = new SQLDBHelper();
        int Fillid;
        BindingSource bsParty = new BindingSource();
        string str1key;
        public int SelectId = 0;
        private void txtqty_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bsc.Filter = string.Format("OrderNO LIKE '%{0}%' ", txtqty.Text);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void comboBox3_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            mode = 1;

            qur.CommandText = "truncate table CutingissueTemp";
            qur.ExecuteNonQuery();
            h = 0;
            Genclass.ClearTextBox(this, Editpnl);
            Genclass.STR = "";
            Genclass.Gendocno();
            txtgrn.Text = Genclass.ST;
            Editpnl.Visible = true;
            lkppnl.Visible = false;
            grSearch.Visible = false;
            HFIT.Refresh();
            HFIT.DataSource = null;
            HFIT.Rows.Clear();
            RQGR.Refresh();
            RQGR.DataSource = null;
            RQGR.Rows.Clear();
            txtgrn.Tag = "0";





            Chkedtact.Checked = true;
            RQGR.Refresh();
            RQGR.DataSource = null;
            RQGR.Rows.Clear();
            HFIT.Refresh();
            HFIT.DataSource = null;
            HFIT.Rows.Clear();

            Titlep1();
            Titlep();

            panadd.Visible = false;
            Genpan.Visible = false;
            Editpnl.Visible = true;
        }

        private void FrmCuttingIss_Load(object sender, EventArgs e)
        {
            qur.Connection = conn;
            tabC.TabPages.Remove(tabPage3);
            Genclass.buttonstyleform(this);
            Genclass.buttonstylepanel(panadd);
            Genclass.buttonstyleform(this);
            Genclass.buttonstylepanel(panadd);
            panadd.Visible = true;
            Genpan.Visible = true;
            Editpnl.Visible = false;
            contractor();

            this.HFGP.DefaultCellStyle.Font = new Font("calibri", 10);
            this.HFGP.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 10, FontStyle.Bold);
            this.HFIT.DefaultCellStyle.Font = new Font("calibri", 10);
            this.HFIT.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 10, FontStyle.Bold);
            this.HFGP2.DefaultCellStyle.Font = new Font("calibri", 10);
            this.HFGP2.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 10, FontStyle.Bold);
            this.RQGR.DefaultCellStyle.Font = new Font("calibri", 10);
            this.RQGR.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 10, FontStyle.Bold);
            this.DataGridCommon.DefaultCellStyle.Font = new Font("calibri", 10);
            this.DataGridCommon.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 10, FontStyle.Bold);
            DataGridCommon.RowHeadersVisible = false;
            RQGR.RowHeadersVisible = false;
            HFGP.RowHeadersVisible = false;
            HFGP2.RowHeadersVisible = false;
            HFIT.RowHeadersVisible = false;
            Genclass.Dtype = 1390;
            RQGR.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            RQGR.EnableHeadersVisualStyles = false;
            RQGR.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.GradientActiveCaption;
            HFGP.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            HFGP.EnableHeadersVisualStyles = false;
            HFGP.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.GradientActiveCaption;
            HFIT.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            HFIT.EnableHeadersVisualStyles = false;
            HFIT.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.GradientActiveCaption;
            HFGP2.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            HFGP2.EnableHeadersVisualStyles = false;
            HFGP2.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.GradientActiveCaption;
            grSearch.Visible = false;
            lkppnl.Visible = false;
            Titlep();
            Titlep1();
            LoadGetJobCard(1);
            loaduom();

        }


        private void loaduom()
        {



       
            string qur1 = " select Guid,Generalname from Generalm  where typemuid=7";
            SqlCommand cmd = new SqlCommand(qur1, conn);
            SqlDataAdapter apt = new SqlDataAdapter(cmd);
            DataTable tab = new DataTable();
            apt.Fill(tab);
            txtuomm.DataSource = null;
            txtuomm.DataSource = tab;
            txtuomm.DisplayMember = "Generalname";
            txtuomm.ValueMember = "Guid";
            txtuomm.SelectedIndex = -1;
          
        }
        protected DataTable LoadGetJobCard(int tag)
        {

            DataTable dt = new DataTable();
            try
            {

                SqlParameter[] para = {
                 new SqlParameter("@DOCTYPEID",Genclass.Dtype),


                };

                dt = db.GetData(CommandType.StoredProcedure, "SP_GETCUTTINGENTRYIssue", para);


                LoadDataTable(dt);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        protected void LoadDataTable(DataTable dt)
        {
            try
            {
                HFGP.DataSource = null;
                HFGP.AutoGenerateColumns = false;
                HFGP.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
                HFGP.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
                HFGP.ColumnCount = 10;
                HFGP.Columns[0].Name = "Uid";
                HFGP.Columns[0].HeaderText = "Uid";
                HFGP.Columns[0].DataPropertyName = "Uid";

                HFGP.Columns[1].Name = "Docno";
                HFGP.Columns[1].HeaderText = "Docno";
                HFGP.Columns[1].DataPropertyName = "Docno";

                HFGP.Columns[2].Name = "Docdate";
                HFGP.Columns[2].HeaderText = "Docdate";
                HFGP.Columns[2].DataPropertyName = "Docdate";

                HFGP.Columns[3].Name = "JOBYPE";
                HFGP.Columns[3].HeaderText = "JobType";
                HFGP.Columns[3].DataPropertyName = "JOBYPE";

                HFGP.Columns[4].Name = "CUTTINGTYPE";
                HFGP.Columns[4].HeaderText = "CuttingType";
                HFGP.Columns[4].DataPropertyName = "CUTTINGTYPE";

                HFGP.Columns[5].Name = "WORKTYPE";
                HFGP.Columns[5].HeaderText = "WorkType";
                HFGP.Columns[5].DataPropertyName = "WORKTYPE";

                HFGP.Columns[6].Name = "SOCNO";
                HFGP.Columns[6].HeaderText = "SocNo";
                HFGP.Columns[6].DataPropertyName = "SOCNO";

                HFGP.Columns[7].Name = "Style";
                HFGP.Columns[7].HeaderText = "Style";
                HFGP.Columns[7].DataPropertyName = "Style";

                HFGP.Columns[8].Name = "CONTRATOR";
                HFGP.Columns[8].HeaderText = "Contratcor";
                HFGP.Columns[8].DataPropertyName = "CONTRATOR";

                HFGP.Columns[9].Name = "OUTSOURCETYPE";
                HFGP.Columns[9].HeaderText = "OUTSOURCETYPE";
                HFGP.Columns[9].DataPropertyName = "OUTSOURCETYPE";




                bs.DataSource = dt;

                HFGP.DataSource = bs;


                HFGP.Columns[0].Visible = false;
                HFGP.Columns[1].Width = 170;
                HFGP.Columns[2].Width = 170;
                HFGP.Columns[3].Visible = false;

                HFGP.Columns[4].Visible = false;
                HFGP.Columns[5].Visible = false;
                HFGP.Columns[6].Width = 170;
                HFGP.Columns[7].Width = 170;
                HFGP.Columns[8].Visible = false;
                HFGP.Columns[9].Visible = false;


                HFGP.Columns[2].DefaultCellStyle.Format = "yyyy'/'MM'/'dd";


            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private void buttnext1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttnfinbk_Click(object sender, EventArgs e)
        {
            panadd.Visible = true;
            Genpan.Visible = true;
            Editpnl.Visible = false;
        }
        protected void FillGrid1(DataTable dt, int Fillid)
        {
            try
            {
                HFGP2.DataSource = null;
                HFGP2.AutoGenerateColumns = false;
                HFGP2.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
                HFGP2.DefaultCellStyle.WrapMode = DataGridViewTriState.True;

                HFGP2.ColumnCount = 4;
                HFGP2.Columns[0].Name = "Uid";
                HFGP2.Columns[0].HeaderText = "Uid";
                HFGP2.Columns[0].DataPropertyName = "Uid";
                HFGP2.Columns[1].Name = "OrderNO";
                HFGP2.Columns[1].HeaderText = "OrderNO";
                HFGP2.Columns[1].DataPropertyName = "OrderNO";
                HFGP2.Columns[1].Width = 100;
                HFGP2.Columns[2].Name = "StyleName";
                HFGP2.Columns[2].HeaderText = "StyleName";
                HFGP2.Columns[2].DataPropertyName = "StyleName";
                HFGP2.Columns[2].Width = 200;
                HFGP2.Columns[3].Name = "ordermstyleuid";
                HFGP2.Columns[3].HeaderText = "ordermstyleuid";
                HFGP2.Columns[3].DataPropertyName = "ordermstyleuid";
                HFGP2.DataSource = bsc;
                HFGP2.Columns[0].Visible = false;
                HFGP2.Columns[3].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        private Point FindLocation(Control ctrl)
        {
            if (ctrl.Parent is Form)
                return ctrl.Location;
            else
            {
                Point p = FindLocation(ctrl.Parent);
                p.X += ctrl.Location.X;
                p.Y += ctrl.Location.Y;
                return p;
            }
        }

        private void txtqty_Click(object sender, EventArgs e)
        {
            Genclass.type = 1;
            DataTable dt = getParty();
            bsc.DataSource = dt;
            FillGrid1(dt, 1);
            Point loc = FindLocation(txtqty);
            lkppnl.Location = new Point(loc.X, loc.Y + 20);
            lkppnl.Visible = true;
        }
        public void contractor()
        {
            conn.Close();
            conn.Open();
            string qur = "select UID,contractname from contractorm  ";
            SqlCommand cmd = new SqlCommand(qur, conn);
            SqlDataAdapter apt = new SqlDataAdapter(cmd);
            DataTable tab = new DataTable();
            apt.Fill(tab);
            cbocontratorname.DataSource = null;
            cbocontratorname.DataSource = tab;
            cbocontratorname.DisplayMember = "contractname";
            cbocontratorname.ValueMember = "uid";
            cbocontratorname.SelectedIndex = -1;

        }
        protected DataTable getParty()
        {
            DataTable dt = new DataTable();
            try
            {

                if (Genclass.type == 1)
                {

                    dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "sp_getpoRDERENRYSOCNO", conn);
                    bsc.DataSource = dt;
                }




            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            return dt;
        }
        private void txtqty_KeyDown(object sender, KeyEventArgs e)
        {

        }

        private void btnSelect_Click(object sender, EventArgs e)
        {

        }

        private void DataGridCommonNew_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void DataGridCommonNew_Click(object sender, EventArgs e)
        {

        }

        private void button10_Click(object sender, EventArgs e)
        {

        }

        private void cbopono_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button10_Click_1(object sender, EventArgs e)
        {
            try
            {
                SelectId = 1;
                int Index = HFGP2.SelectedCells[0].RowIndex;
                if (Genclass.type == 1)
                {
                    txtqty.Text = HFGP2.Rows[Index].Cells[1].Value.ToString();
                    txtqty.Tag = HFGP2.Rows[Index].Cells[0].Value.ToString();
                    txtterms.Text = HFGP2.Rows[Index].Cells[2].Value.ToString();
                    txtterms.Tag = HFGP2.Rows[Index].Cells[3].Value.ToString();
                }
                else if (Genclass.type == 2)
                {
                    txtoutitem.Text = HFGP2.Rows[Index].Cells[1].Value.ToString();
                    txtoutitem.Tag = HFGP2.Rows[Index].Cells[0].Value.ToString();
                    txtoutqty.Text = HFGP2.Rows[Index].Cells[2].Value.ToString();
                    txtoutuom.Text = HFGP2.Rows[Index].Cells[3].Value.ToString();
                    txtoutuom.Tag = HFGP2.Rows[Index].Cells[3].Value.ToString();

                }
                SelectId = 0;
                lkppnl.Visible = false;
                SelectId = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }

        private void txtoutitem_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bsFabric.Filter = string.Format("itemname LIKE '%{0}%' ", txtoutitem.Text);



                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtoutitem_Click(object sender, EventArgs e)
        {
            Genclass.type = 2;
            fabricname();
            //Point loc = FindLocation(txtoutitem);
            //lkppnl.Location = new Point(loc.X, loc.Y + 20);
            //lkppnl.Visible = true;
        }
        private void fabricname()

        {
            string qur = "select seqno from (select max(seqno) as seqno  from joreceipt  a inner join processdet b on a.workorder=b.docno and a.socno=b.socno  inner join processdetlist c on b.uid=c.headid and a.processid=c.processid   where  a.socno='" + txtqty.Text + "' and b.orderref='" + txtterms.Text + "')tab  where seqno is not null";
            SqlCommand cmd = new SqlCommand(qur, conn);
            SqlDataAdapter apt = new SqlDataAdapter(cmd);
            DataTable tab = new DataTable();
            apt.Fill(tab);

            if (tab.Rows.Count > 0)
            {
                string qur1 = "select a.itemid,a.Itemname,isnull(sum(a.qty), 0) as QTy,d.generalname uom from joreceipt a inner join processdet b on a.workorder = b.docno and a.socno = b.socno  inner join processdetlist c on b.uid = c.headid  and a.processid = c.processid  and a.itemid = c.itemid    inner join generalm d on a.uomid=d.guid  where c.seqno <= " + tab.Rows[0]["seqno"].ToString() + " and a.socno = '" + txtqty.Text + "' and b.orderref='" + txtterms.Text + "'  group by a.Itemname,a.itemid,d.generalname";
                SqlCommand cmd1 = new SqlCommand(qur1, conn);
                SqlDataAdapter apt1 = new SqlDataAdapter(cmd1);
                DataTable tab1 = new DataTable();
                apt1.Fill(tab1);
                bsFabric.DataSource = tab1;
                //Point loc = FindLocation(txtoutitem);
                //lkppnl.Location = new Point(loc.X, loc.Y + 20);
                lkppnl.Visible = true;
                HFGP2.AutoGenerateColumns = false;
                HFGP2.Refresh();
                HFGP2.DataSource = null;
                HFGP2.Rows.Clear();
                HFGP2.ColumnCount = tab1.Columns.Count;
                i = 0;
                foreach (DataColumn column in tab1.Columns)
                {
                    HFGP2.Columns[i].Name = column.ColumnName;
                    HFGP2.Columns[i].HeaderText = column.ColumnName;
                    HFGP2.Columns[i].DataPropertyName = column.ColumnName;
                    i = i + 1;
                }

                HFGP2.DataSource = tab1;
                HFGP2.Columns[0].Visible = false;
                //HFGP2.Columns[0].Name = "itemid";
                //HFGP2.Columns[0].HeaderText = "itemid";
                //HFGP2.Columns[0].DataPropertyName = "itemid";
                //HFGP2.Columns[1].Name = "Itemname";
                //HFGP2.Columns[1].HeaderText = "Itemname";
                //HFGP2.Columns[1].DataPropertyName = "Itemname";
                HFGP2.Columns[1].Width = 300;

                //HFGP2.Columns[2].Name = "QTy";
                //HFGP2.Columns[2].HeaderText = "QTy";
                //HFGP2.Columns[2].DataPropertyName = "QTy";
                HFGP2.Columns[2].Width = 100;

                //HFGP2.Columns[3].Name = "uom";
                //HFGP2.Columns[3].HeaderText = "Uom";
                //HFGP2.Columns[3].DataPropertyName = "Uom";
                HFGP2.Columns[3].Width = 100;





            }

        }
        private void comboname()

        {

            string qur1 = "select  distinct b.uid,b.combouid as ComboName from ordermfabric  a  inner join  stylecombo b on a.ordermuid=b.ordermuid and a.combouid=b.uid  where a.ordermuid=" + txtqty.Tag + "";
            SqlCommand cmd1 = new SqlCommand(qur1, conn);
            SqlDataAdapter apt1 = new SqlDataAdapter(cmd1);
            DataTable tab1 = new DataTable();
            apt1.Fill(tab1);
            bsp.DataSource = tab1;
            //Point loc = FindLocation(txtdcqty);
            //grSearch.Location = new Point(loc.X, loc.Y + 20);
            grSearch.Visible = true;
            DataGridCommon.AutoGenerateColumns = false;
            DataGridCommon.Refresh();
            DataGridCommon.DataSource = null;
            DataGridCommon.Rows.Clear();
            DataGridCommon.ColumnCount = tab1.Columns.Count;
            i = 0;
            foreach (DataColumn column in tab1.Columns)
            {
                DataGridCommon.Columns[i].Name = column.ColumnName;
                DataGridCommon.Columns[i].HeaderText = column.ColumnName;
                DataGridCommon.Columns[i].DataPropertyName = column.ColumnName;
                i = i + 1;
            }

            DataGridCommon.DataSource = tab1;

            DataGridCommon.Columns[1].Width = 300;

            DataGridCommon.Columns[0].Visible = false;



        }
        private void componentname()

        {

            string qur1 = "select distinct a.componentuid,c.generalname as COmponentName  from ordermfabric  a  inner join  stylecombo b on a.ordermuid=b.ordermuid and a.combouid=b.uid   inner join generalm c on a.componentuid=c.guid where a.ordermuid=" + txtqty.Tag + "";
            SqlCommand cmd1 = new SqlCommand(qur1, conn);
            SqlDataAdapter apt1 = new SqlDataAdapter(cmd1);
            DataTable tab1 = new DataTable();
            apt1.Fill(tab1);
            bsParty.DataSource = tab1;
            //Point loc = FindLocation(txtdcqty);
            //grSearch.Location = new Point(loc.X, loc.Y + 20);
            grSearch.Visible = true;
            DataGridCommon.AutoGenerateColumns = false;
            DataGridCommon.Refresh();
            DataGridCommon.DataSource = null;
            DataGridCommon.Rows.Clear();
            DataGridCommon.ColumnCount = tab1.Columns.Count;
            i = 0;
            foreach (DataColumn column in tab1.Columns)
            {
                DataGridCommon.Columns[i].Name = column.ColumnName;
                DataGridCommon.Columns[i].HeaderText = column.ColumnName;
                DataGridCommon.Columns[i].DataPropertyName = column.ColumnName;
                i = i + 1;
            }

            DataGridCommon.DataSource = tab1;

            DataGridCommon.Columns[1].Width = 300;

            DataGridCommon.Columns[0].Visible = false;



        }

        private void size()

        {

            string qur1 = "select distinct a.sizeuid,d.sizename as sizename  from ordermfabric  a  inner join  stylecombo b on a.ordermuid=b.ordermuid and a.combouid=b.uid   inner join generalm c on a.componentuid=c.guid  inner join  StyleSize d on a.sizeuid=d.uid   where a.ordermuid=" + txtqty.Tag + "";
            SqlCommand cmd1 = new SqlCommand(qur1, conn);
            SqlDataAdapter apt1 = new SqlDataAdapter(cmd1);
            DataTable tab1 = new DataTable();
            apt1.Fill(tab1);



            bsserial.DataSource = tab1;
            //Point loc = FindLocation(txtdcqty);
            //grSearch.Location = new Point(loc.X, loc.Y + 20);
            grSearch.Visible = true;
            DataGridCommon.AutoGenerateColumns = false;
            DataGridCommon.Refresh();
            DataGridCommon.DataSource = null;
            DataGridCommon.Rows.Clear();
            DataGridCommon.ColumnCount = tab1.Columns.Count;
            i = 0;
            foreach (DataColumn column in tab1.Columns)
            {
                DataGridCommon.Columns[i].Name = column.ColumnName;
                DataGridCommon.Columns[i].HeaderText = column.ColumnName;
                DataGridCommon.Columns[i].DataPropertyName = column.ColumnName;
                i = i + 1;
            }

            DataGridCommon.DataSource = tab1;

            DataGridCommon.Columns[1].Width = 300;

            DataGridCommon.Columns[0].Visible = false;


        }

        private void txtoutitem_KeyDown(object sender, KeyEventArgs e)
        {

        }

        private void HFGP2_Click(object sender, EventArgs e)
        {
            try
            {
                SelectId = 1;
                int Index = HFGP2.SelectedCells[0].RowIndex;
                if (Genclass.type == 1)
                {
                    txtqty.Text = HFGP2.Rows[Index].Cells[1].Value.ToString();
                    txtqty.Tag = HFGP2.Rows[Index].Cells[0].Value.ToString();
                    txtterms.Text = HFGP2.Rows[Index].Cells[2].Value.ToString();
                    txtterms.Tag = HFGP2.Rows[Index].Cells[3].Value.ToString();
                }
                else if (Genclass.type == 2)
                {
                    txtoutitem.Text = HFGP2.Rows[Index].Cells[1].Value.ToString();
                    txtoutitem.Tag = HFGP2.Rows[Index].Cells[0].Value.ToString();
                    txtoutqty.Text = HFGP2.Rows[Index].Cells[2].Value.ToString();
                    txtoutuom.Text = HFGP2.Rows[Index].Cells[3].Value.ToString();


                }
                SelectId = 0;
                lkppnl.Visible = false;
                SelectId = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }
        private void Titlep()
        {
            RQGR.AutoGenerateColumns = false;
            RQGR.Refresh();
            RQGR.DataSource = null;
            RQGR.Rows.Clear();
            RQGR.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            RQGR.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            RQGR.ColumnCount = 7;
            RQGR.Columns[0].Name = "Itemname";
            RQGR.Columns[1].Name = "Uom";
            RQGR.Columns[2].Name = "Weight";
            RQGR.Columns[3].Name = "Rolls";
            RQGR.Columns[4].Name = "Refid";
            RQGR.Columns[5].Name = "Itemid";
            RQGR.Columns[6].Name = "uid";
            RQGR.Columns[0].Width = 400;
            RQGR.Columns[1].Width = 100;
            RQGR.Columns[2].Width = 100;
            RQGR.Columns[3].Width = 100;
            RQGR.Columns[4].Visible = false;
            RQGR.Columns[5].Visible = false;
            RQGR.Columns[6].Visible = false;

        }

        private void Titlep1()
        {
            HFIT.AutoGenerateColumns = false;
            HFIT.Refresh();
            HFIT.DataSource = null;
            HFIT.Rows.Clear();
            HFIT.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            HFIT.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            HFIT.ColumnCount = 9;
            HFIT.Columns[0].Name = "ComboName";
            HFIT.Columns[1].Name = "ComponentName";
            HFIT.Columns[2].Name = "Uom";
            HFIT.Columns[3].Name = "Size";
            HFIT.Columns[4].Name = "Qty";
            HFIT.Columns[5].Name = "combouid";
            HFIT.Columns[6].Name = "uid";
         
            HFIT.Columns[7].Name = "fabricid";
            HFIT.Columns[8].Name = "Weight";
            HFIT.Columns[0].Width = 400;
            HFIT.Columns[1].Width = 100;
            HFIT.Columns[2].Width = 100;
            HFIT.Columns[3].Width = 100;
            HFIT.Columns[4].Width = 100;
            HFIT.Columns[5].Visible = false;
            HFIT.Columns[6].Visible = false;
            HFIT.Columns[7].Visible = false;
            HFIT.Columns[8].Width = 100;
        }
        private void HFGP2_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                SelectId = 1;
                int Index = HFGP2.SelectedCells[0].RowIndex;
                if (Genclass.type == 1)
                {
                    txtqty.Text = HFGP2.Rows[Index].Cells[1].Value.ToString();
                    txtqty.Tag = HFGP2.Rows[Index].Cells[0].Value.ToString();
                    txtterms.Text = HFGP2.Rows[Index].Cells[2].Value.ToString();
                    txtterms.Tag = HFGP2.Rows[Index].Cells[3].Value.ToString();
                }
                else if (Genclass.type == 2)
                {
                    txtoutitem.Text = HFGP2.Rows[Index].Cells[1].Value.ToString();
                    txtoutitem.Tag = HFGP2.Rows[Index].Cells[0].Value.ToString();
                    txtoutqty.Text = HFGP2.Rows[Index].Cells[2].Value.ToString();
                    txtoutuom.Text = HFGP2.Rows[Index].Cells[3].Value.ToString();


                }
                SelectId = 0;
                lkppnl.Visible = false;
                SelectId = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }

        private void button17_Click(object sender, EventArgs e)
        {
            var index = RQGR.Rows.Add();
            RQGR.Rows[index].Cells[0].Value = txtoutitem.Text;
            RQGR.Rows[index].Cells[1].Value = txtoutuom.Text;

            RQGR.Rows[index].Cells[2].Value = txtoutqty.Text;
            RQGR.Rows[index].Cells[3].Value = txtoutrolls.Text;
            RQGR.Rows[index].Cells[4].Value = txtqty.Tag;

            RQGR.Rows[index].Cells[5].Value = txtoutitem.Tag;
            RQGR.Rows[index].Cells[6].Value = "0";

            conn.Close();
            conn.Open();

            SqlParameter[] para1 ={


                   new SqlParameter("@fabricname",txtoutitem.Text),
                   new SqlParameter("@fabircid", txtoutitem.Tag),
                   new SqlParameter("@weight", txtoutqty.Text),

                };

            db.ExecuteNonQuery(CommandType.StoredProcedure, "sp_getCutingissueTemp", para1, conn);
       

            conn.Close();
            conn.Open();
            string qur1 = "  select distinct  fabricname,fabircid from CutingissueTemp ";
            SqlCommand cmd = new SqlCommand(qur1, conn);
            SqlDataAdapter apt = new SqlDataAdapter(cmd);
            DataTable tab = new DataTable();
            apt.Fill(tab);
            cbofabric.DataSource = null;
            cbofabric.DataSource = tab;
            cbofabric.DisplayMember = "fabricname";
            cbofabric.ValueMember = "fabircid";
            cbofabric.SelectedIndex = -1;
           


        }

        private void button11_Click(object sender, EventArgs e)
        {

            double pp = Convert.ToDouble(txtuom.Text);
            if(Genclass.sum5> pp)
            {

                MessageBox.Show("Qty Exceed this size");
                return;
            }
            string qur1 = "select weight from CutingissueTemp where fabircid="+ cbofabric .SelectedValue + " ";
            SqlCommand cmd = new SqlCommand(qur1, conn);
            SqlDataAdapter apt = new SqlDataAdapter(cmd);
            DataTable tab = new DataTable();
            apt.Fill(tab);

            if(tab.Rows.Count>0)

            {
                double tt= Convert.ToDouble(txtwt.Text);
                double rr = Convert.ToDouble(tab.Rows[0]["weight"].ToString());
                if (rr > tt)
                {

                    MessageBox.Show("Qty Exceed this size");
                    return;
                }

            }

            var index = HFIT.Rows.Add();
            HFIT.Rows[index].Cells[0].Value = txtdcqty.Text;
            HFIT.Rows[index].Cells[1].Value = txtoutoutqty.Text;
            HFIT.Rows[index].Cells[2].Value = txtuomm.Text;
            HFIT.Rows[index].Cells[3].Value = txtbillqty.Text;

            HFIT.Rows[index].Cells[4].Value = txtuom.Text;
            HFIT.Rows[index].Cells[5].Value = txtdcqty.Tag;

            HFIT.Rows[index].Cells[6].Value = "0";
          
            HFIT.Rows[index].Cells[7].Value = cbofabric.SelectedValue;
            HFIT.Rows[index].Cells[8].Value = txtwt.Text;
        }

        private void txtdcqty_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bsp.Filter = string.Format("comboname LIKE '%{0}%' ", txtdcqty.Text);



                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtdcqty_Click(object sender, EventArgs e)
        {
            Genclass.type = 3;
            comboname();
        }

        private void txtdcqty_KeyDown(object sender, KeyEventArgs e)
        {

        }

        private void txtoutoutqty_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bsParty.Filter = string.Format("componentname LIKE '%{0}%' ", txtoutoutqty.Text);



                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtbillqty_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bsserial.Filter = string.Format("sizename LIKE '%{0}%' ", txtbillqty.Text);



                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void button18_Click(object sender, EventArgs e)
        {
            try
            {
                SelectId = 1;
                int Index = DataGridCommon.SelectedCells[0].RowIndex;
                if (Genclass.type == 3)
                {
                    txtdcqty.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtdcqty.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();


                }
                else if (Genclass.type == 4)
                {
                    txtoutoutqty.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtoutoutqty.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();

                }
                else if (Genclass.type == 5)
                {
                    txtbillqty.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtbillqty.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    checkqty();
                }
                SelectId = 0;
                grSearch.Visible = false;
                SelectId = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }


        private void checkqty()
        {
            Genclass.strsql = "SP_GetSizeMatrixQtyNewCheck  " + txtqty.Tag + " ," + txtterms.Tag + ",'"+ txtbillqty.Text + "','"+ txtdcqty.Text + "'";
            Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
            SqlDataAdapter aptr3 = new SqlDataAdapter(Genclass.cmd);
            DataTable tap3 = new DataTable();
            aptr3.Fill(tap3);
            if (tap3.Rows.Count > 0)
            {
                Genclass.sum5 = Convert.ToDouble(tap3.Rows[0]["Qty"].ToString());

            }
        }
            private void DataGridCommon_Click(object sender, EventArgs e)
        {
            try
            {
                SelectId = 1;
                int Index = DataGridCommon.SelectedCells[0].RowIndex;
                if (Genclass.type == 3)
                {
                    txtdcqty.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtdcqty.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();


                }
                else if (Genclass.type == 4)
                {
                    txtoutoutqty.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtoutoutqty.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();

                }
                else if (Genclass.type == 5)
                {
                    txtbillqty.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtbillqty.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    checkqty();

                }
                SelectId = 0;
                grSearch.Visible = false;
                SelectId = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }
    
        private void txtoutoutqty_Click(object sender, EventArgs e)
        {
            if (cbocuttype.Text == "PANEL FROM FABRIC ROLL")
            {
                Genclass.type = 4;
                componentname();
            }
        }

        private void txtbillqty_Click(object sender, EventArgs e)
        {
            Genclass.type = 5;
            size();
        }

        private void DataGridCommon_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void btnsave_Click(object sender, EventArgs e)
        {
            conn.Close();
            conn.Open();



          

                SqlParameter[] para ={

       
                    new SqlParameter("@uid",txtgrn.Tag),
                    new SqlParameter("@DOCNO",txtgrn.Text),
                    new SqlParameter("@DOCDATE",Convert.ToDateTime(dtpgrndt.Text)),
                    new SqlParameter("@jobype","0"),
                    new SqlParameter("@cuttingtype","0"),
                    new SqlParameter("@worktype","0"),
                    new SqlParameter("@socno", txtqty.Text),
                    new SqlParameter("@style",txtterms.Text),
                    new SqlParameter("@contrator","0"),
                    new SqlParameter("@outsourcetype","0"),
               


            };
                DataTable dataTable = db.GetDataWithParam(CommandType.StoredProcedure, "sp_cuttingEntryM", para, conn);
                int ResponseCode = Convert.ToInt32(dataTable.Rows[0]["ResponseCode"].ToString());
                int ReturnId = Convert.ToInt32(dataTable.Rows[0]["ReturnId"].ToString());
                //db.ExecuteNonQuery(CommandType.StoredProcedure, "sp_cuttingEntryM", para, conn);
                //txtgrnid.Text= Convert.ToInt32(dataTable.Rows[0]["ReturnId"].ToString());


                for (int i = 0; i < RQGR.RowCount - 1; i++)
                {

                    conn.Close();
                    conn.Open();

                    SqlParameter[] para1 ={


                   new SqlParameter("@uid", RQGR.Rows[i].Cells[6].Value),
                   new SqlParameter("@ItemName", RQGR.Rows[i].Cells[0].Value),
                    new SqlParameter("@uom",RQGR.Rows[i].Cells[1].Value),
                    new SqlParameter("@weight", RQGR.Rows[i].Cells[2].Value),
                    new SqlParameter("@rolls", RQGR.Rows[i].Cells[3].Value),
                    new SqlParameter("@Refid", RQGR.Rows[i].Cells[4].Value),
                    new SqlParameter("@itemid", RQGR.Rows[i].Cells[5].Value),
                       new SqlParameter("@headid", ReturnId),

                };

                    db.ExecuteNonQuery(CommandType.StoredProcedure, "sp_cuttingFabricList", para1, conn);
                }



                for (int i = 0; i < HFIT.RowCount - 1; i++)
                {

                    conn.Close();
                    conn.Open();

                    SqlParameter[] para1 ={

            new SqlParameter("@uid", HFIT.Rows[i].Cells[6].Value),
                   new SqlParameter("@ComboName", HFIT.Rows[i].Cells[0].Value),
                    new SqlParameter("@ComponentName",HFIT.Rows[i].Cells[1].Value),
                    new SqlParameter("@Size", HFIT.Rows[i].Cells[3].Value),
                    new SqlParameter("@weight", HFIT.Rows[i].Cells[4].Value),
                    new SqlParameter("@combouid", HFIT.Rows[i].Cells[5].Value),
                              new SqlParameter("@headid", ReturnId),
                                 new SqlParameter("@fabricuid", HFIT.Rows[i].Cells[7].Value),
                    new SqlParameter("@Uom", HFIT.Rows[i].Cells[2].Value),
                    new SqlParameter("@qty", HFIT.Rows[i].Cells[8].Value),


                };

                    db.ExecuteNonQuery(CommandType.StoredProcedure, "sp_cuttingComboComponentList", para1, conn);
                }

               


            conn.Close();
            conn.Open();
            if (mode == 1)
            {
                qur.CommandText = "update doctypem set lastno=lastno+1 where doctypeid=" + Genclass.Dtype + " and finyear='19-20' ";
                qur.ExecuteNonQuery();

            }
            conn.Close();
            conn.Open();
            if (ReturnId != 0)
            {
                if (mode == 1)
                {
                    qur.CommandText = "exec SP_stocklegCUttingIssue " + ReturnId + "," + Genclass.Dtype + "   ,1";
                    qur.ExecuteNonQuery();

                    qur.CommandText = "exec SP_CUTTINGRECWIP_STOCK_LEDGER " + ReturnId + ",1," + Genclass.Dtype + "";
                    qur.ExecuteNonQuery();

                }
                else
                {
                    qur.CommandText = "exec SP_stocklegCUttingIssue " + ReturnId + "," + Genclass.Dtype + "   ,2";
                    qur.ExecuteNonQuery();

                    qur.CommandText = "exec SP_CUTTINGRECWIP_STOCK_LEDGER " + ReturnId + ",2," + Genclass.Dtype + "";
                    qur.ExecuteNonQuery();
                }

            }
            MessageBox.Show("Save Record");
            Editpnl.Visible = false;

            Genpan.Visible = true;
            panadd.Visible = true;
            LoadGetJobCard(1);

        }

        private void butedit_Click(object sender, EventArgs e)
        {
            mode = 2;
            panadd.Visible = false;
            Genpan.Visible = false;
            Editpnl.Visible = true;

            qur.CommandText = "truncate table CutingissueTemp";
            qur.ExecuteNonQuery();
            tabC.Visible = true;
            conn.Close();
            conn.Open();

            //cbosGReturnItem.Items.Clear();
            int i = HFGP.SelectedCells[0].RowIndex;
            uid = HFGP.Rows[i].Cells[0].Value.ToString();
            txtgrn.Tag= HFGP.Rows[i].Cells[0].Value.ToString();
            txtgrnid.Text = HFGP.Rows[i].Cells[0].Value.ToString();
            txtgrn.Text = HFGP.Rows[i].Cells[1].Value.ToString();
            dtpgrndt.Text = HFGP.Rows[i].Cells[2].Value.ToString();

            cbojobtype.Text = HFGP.Rows[i].Cells[3].Value.ToString();
            cbocuttype.Text = HFGP.Rows[i].Cells[4].Value.ToString();
            cboworktype.Text = HFGP.Rows[i].Cells[5].Value.ToString();

           txtqty.Text = HFGP.Rows[i].Cells[6].Value.ToString();
         
            txtterms.Text = HFGP.Rows[i].Cells[7].Value.ToString();
            cbocontratorname.Text = HFGP.Rows[i].Cells[8].Value.ToString();
            txtoutsourcety.Text = HFGP.Rows[i].Cells[9].Value.ToString();
           

                  
            HFIT.Refresh();
            HFIT.DataSource = null;
            HFIT.Rows.Clear();
            RQGR.Refresh();
            RQGR.DataSource = null;
            RQGR.Rows.Clear();
            

        
            Titlep();
       

            Titlep1();
            Genclass.strsql = "SP_GETCUTTINGAFABIRCC  " + txtgrnid.Text + " ";
            Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
            SqlDataAdapter aptr3 = new SqlDataAdapter(Genclass.cmd);
            DataTable tap3 = new DataTable();
            aptr3.Fill(tap3);
            if (tap3.Rows.Count > 0)
            {
                
                for (int m = 0; m < tap3.Rows.Count; m++)
                {
                    var index = RQGR.Rows.Add();
                    RQGR.Rows[index].Cells[0].Value = tap3.Rows[m]["itemname"].ToString();
                    RQGR.Rows[index].Cells[1].Value = tap3.Rows[m]["Uom"].ToString();
                    RQGR.Rows[index].Cells[2].Value = tap3.Rows[m]["WEIGHT"].ToString();
                    RQGR.Rows[index].Cells[3].Value = tap3.Rows[m]["rolls"].ToString();
                    RQGR.Rows[index].Cells[4].Value = tap3.Rows[m]["REFID"].ToString();
                    RQGR.Rows[index].Cells[5].Value = tap3.Rows[m]["itemid"].ToString();
                    RQGR.Rows[index].Cells[6].Value = tap3.Rows[m]["uid"].ToString();
                   


                }



            }
            load();
        }
            private void load()
            {
                Genclass.strsql = "sp_getcuttingComboComponentList " + txtgrnid.Text + " ";
                Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
                DataTable tap1 = new DataTable();
                aptr1.Fill(tap1);

                Genclass.sum1 = 0;


                for (int k = 0; k < tap1.Rows.Count; k++)
                {
                    var index = HFIT.Rows.Add();

                    HFIT.Rows[index].Cells[0].Value = tap1.Rows[k]["COMBONAME"].ToString();

                    HFIT.Rows[index].Cells[1].Value = tap1.Rows[k]["componentname"].ToString();
                HFIT.Rows[index].Cells[2].Value = tap1.Rows[k]["uom"].ToString();

                HFIT.Rows[index].Cells[3].Value = tap1.Rows[k]["size"].ToString();
                    HFIT.Rows[index].Cells[4].Value = tap1.Rows[k]["weight"].ToString();

                    HFIT.Rows[index].Cells[5].Value = tap1.Rows[k]["combouid"].ToString();

                    HFIT.Rows[index].Cells[6].Value = tap1.Rows[k]["UID"].ToString();

  

                HFIT.Rows[index].Cells[7].Value = tap1.Rows[k]["fabricuid"].ToString();
                HFIT.Rows[index].Cells[8].Value = tap1.Rows[k]["qty"].ToString();

            }
            }

        private void Editpnl_Paint(object sender, PaintEventArgs e)
        {

        }

        private void cbofabric_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void cbocuttype_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(cbocuttype.Text== "BIT FROM FABRIC ROLL")
            {
                txtoutoutqty.Text = "Bit";
                
            }
        }

        private void HFGP2_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void txtoutsourcety_TextChanged(object sender, EventArgs e)
        {

        }

        private void butcan_Click(object sender, EventArgs e)
        {
            conn.Close();
            conn.Open();



            int i = HFGP.SelectedCells[0].RowIndex;


            qur.CommandText = "delete  from  cuttingFabricList  where headid=" + HFGP.Rows[i].Cells[0].Value.ToString() + "";
            qur.ExecuteNonQuery();
            qur.CommandText = "delete  from  cuttingComboComponentList  where headid=" + HFGP.Rows[i].Cells[0].Value.ToString() + "";
            qur.ExecuteNonQuery();
            qur.CommandText = "delete  from  cuttingEntryM  where uid=" + HFGP.Rows[i].Cells[0].Value.ToString() + "";
            qur.ExecuteNonQuery();
            MessageBox.Show("Deleted");
            LoadGetJobCard(1);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            lkppnl.Visible = false;
        }
    }
    }

