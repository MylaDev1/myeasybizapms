﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MyEasyBizAPMS
{
    public partial class FrmPreBudget : Form
    {
        public FrmPreBudget()
        {
            InitializeComponent();
            this.TabControlBudjet.TabStyle = typeof(Syncfusion.Windows.Forms.Tools.OneNoteStyleRenderer);
            this.tabContronYan.TabStyle = typeof(Syncfusion.Windows.Forms.Tools.OneNoteStyleRenderer);
            this.tabControlFabric.TabStyle = typeof(Syncfusion.Windows.Forms.Tools.OneNoteStyleRenderer);
            this.SfDataGridPreBudjet.Style.HeaderStyle.Font.FontStyle = FontStyle.Bold;
            this.SfDataGridPreBudjet.Style.HeaderStyle.BackColor = Color.DeepSkyBlue;
        }
        SqlConnection connection = new SqlConnection(GeneralParameters.ConnectionString);
        SQLDBHelper db = new SQLDBHelper();
        BindingSource bsDocNo = new BindingSource();
        DataTable dtOrderQty = new DataTable();
        DataTable dtPanQty = new DataTable();

        int Fillid = 0;
        private int SelectId = 0;
        int loadid = 0;
        int YarnRefresh;
        int FabricRefresh;
        int TrimsReferesh;
        int TrimsProcessRefresh;
        int GarmentProcessRefresh;

        private void FillGrid(DataTable dt, int FillId)
        {
            try
            {
                DataGridCommon.DataSource = null;
                DataGridCommon.AutoGenerateColumns = false;
                if (FillId == 1)
                {
                    Fillid = 1;
                    DataGridCommon.ColumnCount = 3;
                    DataGridCommon.Columns[0].Name = "Uid";
                    DataGridCommon.Columns[0].HeaderText = "Uid";
                    DataGridCommon.Columns[0].DataPropertyName = "Uid";
                    DataGridCommon.Columns[0].Visible = false;
                    DataGridCommon.Columns[0].Width = 0;
                    DataGridCommon.Columns[1].Name = "DocNo";
                    DataGridCommon.Columns[1].HeaderText = "DocNo";
                    DataGridCommon.Columns[1].DataPropertyName = "DocNo";
                    DataGridCommon.Columns[1].Width = 100;
                    DataGridCommon.Columns[2].Name = "StyleDesc";
                    DataGridCommon.Columns[2].HeaderText = "StyleDesc";
                    DataGridCommon.Columns[2].DataPropertyName = "StyleDesc";
                    DataGridCommon.Columns[2].Width = 210;
                    DataGridCommon.DataSource = bsDocNo;
                }
                grSearch.Visible = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
        private void FrmPreBudget_Load(object sender, EventArgs e)
        {
            GrFront.Visible = true;
            GrBack.Visible = false;
            LoadYarnProcess();
            LoadYarnPurchase();
            LoadFabricProcess();
            LoadFabricPurchase();
            LoadTrims();
            LoadOtherExpenses();
            LoadFrontGrid();
            LoadTrimsProcess();
            LoadGarmentProcess();
            LoadCMT();
            LoadGenerals();
        }

        protected void LoadFrontGrid()
        {
            try
            {
                DataTable data = db.GetDataWithoutParam(CommandType.StoredProcedure, "Proc_GetOrderMBudjet", connection);
                SfDataGridPreBudjet.DataSource = null;
                SfDataGridPreBudjet.DataSource = data;
                SfDataGridPreBudjet.Columns[0].Visible = false;
                SfDataGridPreBudjet.Columns[3].Visible = false;
                SfDataGridPreBudjet.Columns[7].Visible = false;
                SfDataGridPreBudjet.Columns[1].Width = 100;
                SfDataGridPreBudjet.Columns[2].Width = 100;
                SfDataGridPreBudjet.Columns[3].Width = 100;
                SfDataGridPreBudjet.Columns[4].Width = 100;
                SfDataGridPreBudjet.Columns[5].Width = 240;
                SfDataGridPreBudjet.Columns[6].Width = 430;
                SfDataGridPreBudjet.Columns[8].Width = 100;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void TxtDocNo_Click(object sender, EventArgs e)
        {
            try
            {
                string Query = "Select Uid,DocNo,DocDate,StyleDesc from OrderM Where Uid Not in (Select OrderMuid from OrderMBudjet) and DocSts = 'Entry Completed' order by Uid desc";
                DataTable dt = db.GetDataWithoutParam(CommandType.Text, Query, connection);
                bsDocNo.DataSource = dt;
                FillGrid(dt, 1);
                Point loc = Genclass.FindLocation(txtDocNo);
                grSearch.Location = new Point(loc.X - 10, loc.Y + 20);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void BtnSelect_Click(object sender, EventArgs e)
        {
            try
            {
                SelectId = 1;
                int Index = DataGridCommon.SelectedCells[0].RowIndex;
                if (Fillid == 1)
                {
                    txtDocNo.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtDocNo.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    GetOrderMEdit(Convert.ToDecimal(txtDocNo.Tag));
                    LoadGrids();
                    SqlParameter[] sqlParameters = { new SqlParameter("@OrderMuid", txtDocNo.Tag) };
                    DataSet dataSetQty = db.GetDataWithParamMultiple(CommandType.StoredProcedure, "Proc_GetOrderTotalQty", sqlParameters, connection);
                    dtOrderQty = dataSetQty.Tables[0];
                    SqlParameter[] sqlParameters1 = { new SqlParameter("@OrderUid", txtDocNo.Tag) };
                    DataTable dataTable = db.GetDataWithParam(CommandType.StoredProcedure, "Proc_GetOrderQty", sqlParameters1, connection);
                    txtExrate.Tag = dataTable.Rows[0]["StyleQty"].ToString();
                    txtPcrate.Text = dataTable.Rows[0]["AvgRate"].ToString();
                    txtExrate.Text = dataTable.Rows[0]["ExRate"].ToString();
                    txtGrossValue.Text = dataTable.Rows[0]["GrossValue"].ToString();
                }
                grSearch.Visible = false;
                SelectId = 0;
                TxtPcrate_TextChanged(sender, e);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        protected void GetOrderMEdit(decimal OrderMuid)
        {
            try
            {
                SelectId = 1;
                decimal CUid = Convert.ToDecimal(OrderMuid);
                SqlParameter[] parameters = { new SqlParameter("@OrderUid", CUid) };
                DataSet ds = db.GetDataWithParamMultiple(CommandType.StoredProcedure, "SP_GetOrderMEdit", parameters, connection);
                DataTable dt = ds.Tables[0];
                DataTable dtStyle = ds.Tables[1];
                FillTextboxs(dt);
                DataTable dtq = GetorderStyle(CUid);
                CmbStyle.DisplayMember = "StyleName";
                CmbStyle.ValueMember = "Uid";
                CmbStyle.DataSource = dtq;
                SelectId = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected DataTable GetorderStyle(decimal orderId)
        {
            DataTable dataTable = new DataTable();
            try
            {
                SqlParameter[] parameters = { new SqlParameter("@OrderMUid", orderId) };
                dataTable = db.GetDataWithParam(CommandType.StoredProcedure, "SP_GetOrderMStyles", parameters, connection);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            return dataTable;
        }

        private void FillTextboxs(DataTable dt)
        {
            try
            {
                txtDocNo.Text = dt.Rows[0]["DocNo"].ToString();
                txtDocNo.Tag = dt.Rows[0]["Uid"].ToString();
                dtpDocDate.Text = Convert.ToDateTime(dt.Rows[0]["DocDate"].ToString()).ToString();
                //txtOrderNo.Text = dt.Rows[0]["OrderNo"].ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void TabControlFabric_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (tabControlFabric.SelectedIndex == 1)
                {
                    SqlParameter[] sqlParameters = { new SqlParameter("@OrderMUid", txtDocNo.Tag), new SqlParameter("@StyleUid", CmbStyle.SelectedValue) };
                    DataTable dt = db.GetDataWithParam(CommandType.StoredProcedure, "Proc_GetFabricComponent", sqlParameters, connection);
                    CmbFabricPurchase.DataSource = null;
                    CmbFabricPurchase.DisplayMember = "FabricName";
                    CmbFabricPurchase.ValueMember = "FabricUid";
                    CmbFabricPurchase.DataSource = dt;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void SplitAdd_Click(object sender, EventArgs e)
        {
            try
            {
                YarnRefresh = 0;
                FabricRefresh = 0;
                TrimsReferesh = 0;
                TrimsProcessRefresh = 0;
                GarmentProcessRefresh = 0;
                GrFront.Visible = false;
                GrBack.Visible = true;
                DataGridFabricProcess.Rows.Clear();
                DataGridFabricPurchase.Rows.Clear();
                DataGridYarnProcess.Rows.Clear();
                DataGridYarnPurchase.Rows.Clear();
                DataGridTrims.Rows.Clear();
                DataGridOtherExpenses.Rows.Clear();
                DataGridCMT.Rows.Clear();
                DataGridTrimsProcess.Rows.Clear();
                DataGridGarmentProcess.Rows.Clear();
                txtDocNo.Text = string.Empty;
                txtDocNo.Tag = string.Empty;
                txtOrderNo.Text = string.Empty;
                txtOrderNo.Tag = string.Empty;
                txtGrossValue.Text = string.Empty;
                txtExrate.Text = string.Empty;
                txtExrate.Tag = string.Empty;
                txtPcrate.Text = string.Empty;
                txtOrderNo.Text = GeneralParameters.GetDocNo(4, connection);
                txtOrderNo.Tag = "0";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void SplitAdd_DropDowItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            try
            {
                if (e.ClickedItem.Text == "Close")
                {
                    this.Close();
                }
                else
                {
                    if (SfDataGridPreBudjet.SelectedIndex == -1)
                    {
                        MessageBox.Show("Select a row", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                    SelectId = 1;
                    YarnRefresh = 0;
                    FabricRefresh = 0;
                    TrimsReferesh = 0;
                    TrimsProcessRefresh = 0;
                    GarmentProcessRefresh = 0;
                    txtGrossValue.Text = string.Empty;
                    txtExrate.Text = string.Empty;
                    txtExrate.Tag = string.Empty;
                    txtPcrate.Text = string.Empty;
                    var selectedItem = SfDataGridPreBudjet.SelectedItems[0];
                    var dataRow = (selectedItem as DataRowView).Row;
                    string Status = dataRow["Status"].ToString();
                    if (Status == "Pending")
                    {
                        decimal CUid = Convert.ToDecimal(dataRow["UID"].ToString());
                        decimal OrderMUid = Convert.ToDecimal(dataRow["OrdermUid"].ToString());
                        txtDocNo.Text = dataRow["OrderNo"].ToString();
                        txtDocNo.Tag = OrderMUid;
                        txtOrderNo.Text = dataRow["DocNo"].ToString();
                        txtOrderNo.Tag = CUid;
                        DataTable dtq = GetorderStyle(OrderMUid);
                        CmbStyle.DisplayMember = "StyleName";
                        CmbStyle.ValueMember = "Uid";
                        CmbStyle.DataSource = dtq;
                        SqlParameter[] sqlParameters = { new SqlParameter("@OrderMUid", OrderMUid), new SqlParameter("@OrderMBudjetUid", CUid) };
                        DataSet dataSet = db.GetDataWithParamMultiple(CommandType.StoredProcedure, "Proc_OrderMBudjetEdit", sqlParameters, connection);
                        LoadGridsOnEdit(dataSet);
                        SqlParameter[] sqlParameters2 = { new SqlParameter("@OrderMuid", OrderMUid) };
                        DataSet dataSetQty = db.GetDataWithParamMultiple(CommandType.StoredProcedure, "Proc_GetOrderTotalQty", sqlParameters2, connection);
                        dtOrderQty = dataSetQty.Tables[0];
                        SqlParameter[] sqlParameters1 = { new SqlParameter("@OrderUid", OrderMUid) };
                        DataTable dataTable = db.GetDataWithParam(CommandType.StoredProcedure, "Proc_GetOrderQty", sqlParameters1, connection);
                        txtExrate.Text = dataTable.Rows[0]["ExRate"].ToString();
                        txtGrossValue.Text = dataTable.Rows[0]["GrossValue"].ToString();
                        txtExrate.Tag = dataTable.Rows[0]["StyleQty"].ToString();
                        txtPcrate.Text = dataTable.Rows[0]["AvgRate"].ToString();
                        GrFront.Visible = false;
                        GrBack.Visible = true;
                        SelectId = 0;
                    }
                    else
                    {
                        MessageBox.Show("Budget already approved can't edit ", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void SplitSave_DropDowItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            try
            {
                if (e.ClickedItem.Text == "Back")
                {
                    LoadFrontGrid();
                    GrFront.Visible = true;
                    GrBack.Visible = false;
                    DataGridFabricProcess.Rows.Clear();
                    DataGridFabricPurchase.Rows.Clear();
                    DataGridYarnProcess.Rows.Clear();
                    DataGridYarnPurchase.Rows.Clear();
                    DataGridTrims.Rows.Clear();
                    DataGridOtherExpenses.Rows.Clear();
                    DataGridCMT.Rows.Clear();
                    DataGridTrimsProcess.Rows.Clear();
                    DataGridGarmentProcess.Rows.Clear();
                    txtDocNo.Text = string.Empty;
                    txtDocNo.Tag = string.Empty;
                    txtOrderNo.Text = string.Empty;
                    txtOrderNo.Tag = string.Empty;

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void DataGridCommon_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                BtnSelect_Click(sender, e);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected void LoadGrids()
        {
            try
            {
                loadid = 1;
                SqlParameter[] sqlParameters11 = { new SqlParameter("@OrderMUid", txtDocNo.Tag) };
                DataTable dtYanrPr = db.GetDataWithParam(CommandType.StoredProcedure, "Proc_GetYarnProcess", sqlParameters11, connection);
                for (int i = 0; i < dtYanrPr.Rows.Count; i++)
                {
                    int Index = DataGridYarnProcess.Rows.Add();
                    DataGridViewRow dataGrid = DataGridYarnProcess.Rows[Index];
                    dataGrid.Cells[0].Value = i + 1;
                    dataGrid.Cells[1].Value = dtYanrPr.Rows[i]["YarnName"].ToString();
                    dataGrid.Cells[2].Value = dtYanrPr.Rows[i]["Qty"].ToString();
                    dataGrid.Cells[3].Value = "0";
                    dataGrid.Cells[4].Value = "0";
                    dataGrid.Cells[5].Value = dtYanrPr.Rows[i]["Yarnuid"].ToString();
                    dataGrid.Cells[6].Value = "0";
                    dataGrid.Cells[7].Value = "Process";
                    dataGrid.Cells[8].Value = "0";
                    dataGrid.Cells[9].Value = false;
                }

                SqlParameter[] sqlParameters = { new SqlParameter("@OrderMUid", txtDocNo.Tag) };
                DataTable dataTable = db.GetDataWithParam(CommandType.StoredProcedure, "Proc_OrderMYarnReqReport", sqlParameters, connection);
                for (int i = 0; i < dataTable.Rows.Count; i++)
                {
                    int Index = DataGridYarnPurchase.Rows.Add();
                    DataGridViewRow dataGrid = DataGridYarnPurchase.Rows[Index];
                    dataGrid.Cells[0].Value = i + 1;
                    dataGrid.Cells[1].Value = dataTable.Rows[i]["Yarnname"].ToString();
                    dataGrid.Cells[2].Value = dataTable.Rows[i]["PWt"].ToString();
                    dataGrid.Cells[3].Value = "0";
                    dataGrid.Cells[4].Value = "0";
                    dataGrid.Cells[5].Value = dataTable.Rows[i]["YarnId"].ToString();
                    dataGrid.Cells[6].Value = dataTable.Rows[i]["FabricUid"].ToString();
                    dataGrid.Cells[7].Value = "Purchase";
                    dataGrid.Cells[8].Value = "0";
                    dataGrid.Cells[9].Value = false;
                }

                SqlParameter[] parameters1 = { new SqlParameter("@OrderMUid", txtDocNo.Tag) };
                DataTable dt = db.GetDataWithParam(CommandType.StoredProcedure, "SP_GetOrderMFabricProcess", parameters1, connection);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    int Index = DataGridFabricProcess.Rows.Add();
                    DataGridViewRow dataGridViewRow = DataGridFabricProcess.Rows[Index];
                    dataGridViewRow.Cells[0].Value = i + 1;
                    dataGridViewRow.Cells[1].Value = dt.Rows[i]["FabricName"].ToString();
                    dataGridViewRow.Cells[2].Value = dt.Rows[i]["FabricColour"].ToString();
                    dataGridViewRow.Cells[3].Value = dt.Rows[i]["ProcessName"].ToString();
                    dataGridViewRow.Cells[4].Value = dt.Rows[i]["InputWght"].ToString();
                    dataGridViewRow.Cells[5].Value = "0";
                    dataGridViewRow.Cells[6].Value = "0";
                    dataGridViewRow.Cells[7].Value = dt.Rows[i]["OrderMFabricUid"].ToString();
                    dataGridViewRow.Cells[8].Value = "Process";
                    dataGridViewRow.Cells[9].Value = "0";
                }

                SqlParameter[] sqlParameters1 = { new SqlParameter("@OrderMUid", txtDocNo.Tag) };
                DataTable dataTable1 = db.GetDataWithParam(CommandType.StoredProcedure, "Proc_MaterialBomTrimsReport", sqlParameters1, connection);
                for (int i = 0; i < dataTable1.Rows.Count; i++)
                {
                    int Index = DataGridTrims.Rows.Add();
                    DataGridViewRow dataGridViewRow = DataGridTrims.Rows[Index];
                    dataGridViewRow.Cells[0].Value = i + 1;
                    dataGridViewRow.Cells[1].Value = dataTable1.Rows[i]["Item"].ToString();
                    dataGridViewRow.Cells[2].Value = dataTable1.Rows[i]["Qty"].ToString();
                    dataGridViewRow.Cells[3].Value = dataTable1.Rows[i]["Rate"].ToString();
                    dataGridViewRow.Cells[4].Value = dataTable1.Rows[i]["Total"].ToString();
                    dataGridViewRow.Cells[5].Value = dataTable1.Rows[i]["Uid"].ToString();
                    dataGridViewRow.Cells[6].Value = "0";
                }


                SqlParameter[] sqlParameters12 = { new SqlParameter("@OrderMUid", txtDocNo.Tag) };
                DataTable dtTrimsProcess = db.GetDataWithParam(CommandType.StoredProcedure, "Proc_OrderMTrimsProcessEdit", sqlParameters12, connection);
                for (int i = 0; i < dtTrimsProcess.Rows.Count; i++)
                {
                    int Index = DataGridTrimsProcess.Rows.Add();
                    DataGridViewRow dataGridViewRow = DataGridTrimsProcess.Rows[Index];
                    dataGridViewRow.Cells[0].Value = i + 1;
                    dataGridViewRow.Cells[1].Value = dtTrimsProcess.Rows[i]["ItemSpec"].ToString();
                    dataGridViewRow.Cells[2].Value = dtTrimsProcess.Rows[i]["process"].ToString();
                    dataGridViewRow.Cells[3].Value = dtTrimsProcess.Rows[i]["Qty"].ToString();
                    dataGridViewRow.Cells[4].Value = dtTrimsProcess.Rows[i]["Rate"].ToString();
                    dataGridViewRow.Cells[5].Value = Convert.ToDecimal(dtTrimsProcess.Rows[i]["Qty"].ToString()) * Convert.ToDecimal(dtTrimsProcess.Rows[i]["Rate"].ToString());
                    dataGridViewRow.Cells[6].Value = dtTrimsProcess.Rows[i]["TrimsItemUid"].ToString();
                    dataGridViewRow.Cells[7].Value = dtTrimsProcess.Rows[i]["TrimsProcessUid"].ToString();
                    dataGridViewRow.Cells[8].Value = "0";
                }

                SqlParameter[] sqlParameters13 = { new SqlParameter("@OrderMUid", txtDocNo.Tag) };
                DataTable dtGarmentProcess = db.GetDataWithParam(CommandType.StoredProcedure, "Proc_OrderMGarmentProcessEdit", sqlParameters13, connection);
                for (int i = 0; i < dtGarmentProcess.Rows.Count; i++)
                {
                    int Index = DataGridGarmentProcess.Rows.Add();
                    DataGridViewRow dataGridViewRow = DataGridGarmentProcess.Rows[Index];
                    dataGridViewRow.Cells[0].Value = i + 1;
                    dataGridViewRow.Cells[1].Value = dtGarmentProcess.Rows[i]["Component"].ToString();
                    dataGridViewRow.Cells[2].Value = dtGarmentProcess.Rows[i]["process"].ToString();
                    dataGridViewRow.Cells[3].Value = dtGarmentProcess.Rows[i]["Qty"].ToString();
                    dataGridViewRow.Cells[4].Value = "0";
                    dataGridViewRow.Cells[5].Value = "0";
                    dataGridViewRow.Cells[6].Value = dtGarmentProcess.Rows[i]["ComponentUid"].ToString();
                    dataGridViewRow.Cells[7].Value = dtGarmentProcess.Rows[i]["GarmentProcessUid"].ToString();
                    dataGridViewRow.Cells[8].Value = "0";
                }
                loadid = 0;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void LoadGridsOnEdit(DataSet dataSet)
        {
            try
            {
                loadid = 1;
                DataTable dtYarn = dataSet.Tables[0];
                DataTable dtFabric = dataSet.Tables[1];
                DataTable dtTrims = dataSet.Tables[2];
                DataTable dtAccounts = dataSet.Tables[3];
                DataTable dtTrimsProcess = dataSet.Tables[4];
                DataTable dtGarmentProcess = dataSet.Tables[5];
                DataTable dtCMTProcess = dataSet.Tables[6];
                DataGridOtherExpenses.Rows.Clear();

                DataRow[] dataYarn = dtYarn.Select("YarnTag = 'Process'");
                if (dataYarn.Length > 0)
                {
                    DataTable dtProcessYarn = dtYarn.Select("YarnTag = 'Process'").CopyToDataTable();
                    for (int i = 0; i < dtProcessYarn.Rows.Count; i++)
                    {
                        int IndexPr = DataGridYarnProcess.Rows.Add();
                        DataGridViewRow dataGrid = DataGridYarnProcess.Rows[IndexPr];
                        dataGrid.Cells[0].Value = i + 1;
                        dataGrid.Cells[1].Value = dtProcessYarn.Rows[i]["Yarnname"].ToString();
                        dataGrid.Cells[2].Value = dtProcessYarn.Rows[i]["Qty"].ToString();
                        dataGrid.Cells[3].Value = dtProcessYarn.Rows[i]["Rate"].ToString();
                        dataGrid.Cells[4].Value = dtProcessYarn.Rows[i]["Total"].ToString();
                        dataGrid.Cells[5].Value = dtProcessYarn.Rows[i]["YarnUid"].ToString();
                        dataGrid.Cells[6].Value = "0";
                        dataGrid.Cells[7].Value = dtProcessYarn.Rows[i]["YarnTag"].ToString();
                        bool freoofcost = Convert.ToBoolean(dtProcessYarn.Rows[i]["FreeOfCost"].ToString());
                        if (freoofcost == true)
                        {
                            dataGrid.Cells[9].Value = true;
                        }
                        else
                        {
                            dataGrid.Cells[9].Value = false;
                        }
                        dataGrid.Cells[8].Value = dtProcessYarn.Rows[i]["Uid"].ToString(); ;
                    }
                }
                DataRow[] dataYarnP = dtYarn.Select("YarnTag = 'Purchase'");
                if (dataYarnP.Length > 0)
                {
                    DataTable dtYarnPurchase = dtYarn.Select("YarnTag = 'Purchase'").CopyToDataTable();
                    for (int i = 0; i < dtYarnPurchase.Rows.Count; i++)
                    {
                        int IndexPu = DataGridYarnPurchase.Rows.Add();
                        DataGridViewRow dataGrid = DataGridYarnPurchase.Rows[IndexPu];
                        dataGrid.Cells[0].Value = i + 1;
                        dataGrid.Cells[1].Value = dtYarnPurchase.Rows[i]["Yarnname"].ToString();
                        dataGrid.Cells[2].Value = dtYarnPurchase.Rows[i]["Qty"].ToString();
                        dataGrid.Cells[3].Value = dtYarnPurchase.Rows[i]["Rate"].ToString();
                        dataGrid.Cells[4].Value = dtYarnPurchase.Rows[i]["Total"].ToString();
                        dataGrid.Cells[5].Value = dtYarnPurchase.Rows[i]["YarnUid"].ToString();
                        dataGrid.Cells[6].Value = "0";
                        dataGrid.Cells[7].Value = dtYarnPurchase.Rows[i]["YarnTag"].ToString();
                        bool freoofcost = Convert.ToBoolean(dtYarnPurchase.Rows[i]["FreeOfCost"].ToString());
                        if (freoofcost == true)
                        {
                            dataGrid.Cells[9].Value = true;
                        }
                        else
                        {
                            dataGrid.Cells[9].Value = false;
                        }
                        dataGrid.Cells[8].Value = dtYarnPurchase.Rows[i]["Uid"].ToString(); ;
                    }
                }

                DataRow[] dataFabricP = dtFabric.Select("FabTag = 'Process'");
                if (dataFabricP.Length > 0)
                {
                    DataTable dtFabricProcess = dtFabric.Select("FabTag = 'Process'").CopyToDataTable();
                    for (int i = 0; i < dtFabricProcess.Rows.Count; i++)
                    {
                        int IndexFp = DataGridFabricProcess.Rows.Add();
                        DataGridViewRow dataGridViewRowPr = DataGridFabricProcess.Rows[IndexFp];
                        dataGridViewRowPr.Cells[0].Value = i + 1;
                        dataGridViewRowPr.Cells[1].Value = dtFabricProcess.Rows[i]["FabricName"].ToString();
                        dataGridViewRowPr.Cells[2].Value = dtFabricProcess.Rows[i]["Colour"].ToString();
                        dataGridViewRowPr.Cells[3].Value = dtFabricProcess.Rows[i]["Process"].ToString();
                        dataGridViewRowPr.Cells[4].Value = dtFabricProcess.Rows[i]["Qty"].ToString();
                        dataGridViewRowPr.Cells[5].Value = dtFabricProcess.Rows[i]["Rate"].ToString();
                        dataGridViewRowPr.Cells[6].Value = dtFabricProcess.Rows[i]["Total"].ToString();
                        dataGridViewRowPr.Cells[7].Value = dtFabricProcess.Rows[i]["FabricUid"].ToString();
                        dataGridViewRowPr.Cells[8].Value = dtFabricProcess.Rows[i]["FabTag"].ToString();
                        dataGridViewRowPr.Cells[9].Value = dtFabricProcess.Rows[i]["Uid"].ToString();
                    }
                }
                DataRow[] dataFabricPUr = dtFabric.Select("FabTag = 'Purchase'");
                if (dataFabricPUr.Length > 0)
                {
                    DataTable dtFabricPurchase = dtFabric.Select("FabTag = 'Purchase'").CopyToDataTable();
                    for (int i = 0; i < dtFabricPurchase.Rows.Count; i++)
                    {
                        int IndexFPu = DataGridFabricProcess.Rows.Add();
                        DataGridViewRow dataGridViewRowP = DataGridFabricProcess.Rows[IndexFPu];
                        dataGridViewRowP.Cells[0].Value = i + 1;
                        dataGridViewRowP.Cells[1].Value = dtFabricPurchase.Rows[i]["FabricName"].ToString();
                        dataGridViewRowP.Cells[2].Value = dtFabricPurchase.Rows[i]["Colour"].ToString();
                        dataGridViewRowP.Cells[3].Value = dtFabricPurchase.Rows[i]["Process"].ToString();
                        dataGridViewRowP.Cells[4].Value = dtFabricPurchase.Rows[i]["Qty"].ToString();
                        dataGridViewRowP.Cells[5].Value = dtFabricPurchase.Rows[i]["Rate"].ToString();
                        dataGridViewRowP.Cells[6].Value = dtFabricPurchase.Rows[i]["Total"].ToString();
                        dataGridViewRowP.Cells[7].Value = dtFabricPurchase.Rows[i]["FabricUid"].ToString();
                        dataGridViewRowP.Cells[8].Value = dtFabricPurchase.Rows[i]["FabTag"].ToString();
                        dataGridViewRowP.Cells[9].Value = dtFabricPurchase.Rows[i]["Uid"].ToString();
                    }
                }

                for (int i = 0; i < dtTrims.Rows.Count; i++)
                {
                    int IndexTr = DataGridTrims.Rows.Add();
                    DataGridViewRow dataGridViewRowT = DataGridTrims.Rows[IndexTr];
                    dataGridViewRowT.Cells[0].Value = i + 1;
                    dataGridViewRowT.Cells[1].Value = dtTrims.Rows[i]["TrimsName"].ToString();
                    dataGridViewRowT.Cells[2].Value = dtTrims.Rows[i]["Qty"].ToString();
                    dataGridViewRowT.Cells[3].Value = dtTrims.Rows[i]["Rate"].ToString();
                    dataGridViewRowT.Cells[4].Value = dtTrims.Rows[i]["Total"].ToString();
                    dataGridViewRowT.Cells[5].Value = dtTrims.Rows[i]["TrimsUid"].ToString();
                    dataGridViewRowT.Cells[6].Value = dtTrims.Rows[i]["Uid"].ToString();
                }

                for (int i = 0; i < dtAccounts.Rows.Count; i++)
                {
                    int Index = DataGridOtherExpenses.Rows.Add();
                    DataGridViewRow dataGridViewRow = DataGridOtherExpenses.Rows[Index];
                    dataGridViewRow.Cells[0].Value = i + 1;
                    dataGridViewRow.Cells[1].Value = dtAccounts.Rows[i]["Name"].ToString();
                    dataGridViewRow.Cells[2].Value = dtAccounts.Rows[i]["Exptype"].ToString();
                    dataGridViewRow.Cells[3].Value = dtAccounts.Rows[i]["Qty"].ToString();
                    dataGridViewRow.Cells[4].Value = dtAccounts.Rows[i]["rate"].ToString();
                    dataGridViewRow.Cells[5].Value = dtAccounts.Rows[i]["TotalValue"].ToString();
                    dataGridViewRow.Cells[6].Value = dtAccounts.Rows[i]["ExpenseUid"].ToString();
                    dataGridViewRow.Cells[7].Value = dtAccounts.Rows[i]["Uid"].ToString();
                }

                for (int i = 0; i < dtTrimsProcess.Rows.Count; i++)
                {
                    int Index = DataGridTrimsProcess.Rows.Add();
                    DataGridViewRow dataGridViewRow = DataGridTrimsProcess.Rows[Index];
                    dataGridViewRow.Cells[0].Value = i + 1;
                    dataGridViewRow.Cells[1].Value = dtTrimsProcess.Rows[i]["TrimsName"].ToString();
                    dataGridViewRow.Cells[2].Value = dtTrimsProcess.Rows[i]["Process"].ToString();
                    dataGridViewRow.Cells[3].Value = dtTrimsProcess.Rows[i]["Qty"].ToString();
                    dataGridViewRow.Cells[4].Value = dtTrimsProcess.Rows[i]["rate"].ToString();
                    dataGridViewRow.Cells[5].Value = dtTrimsProcess.Rows[i]["Total"].ToString();
                    dataGridViewRow.Cells[6].Value = dtTrimsProcess.Rows[i]["TrimsUid"].ToString();
                    dataGridViewRow.Cells[7].Value = dtTrimsProcess.Rows[i]["ProcessUid"].ToString();
                    dataGridViewRow.Cells[8].Value = dtTrimsProcess.Rows[i]["Uid"].ToString();
                }

                for (int i = 0; i < dtGarmentProcess.Rows.Count; i++)
                {
                    int Index = DataGridGarmentProcess.Rows.Add();
                    DataGridViewRow dataGridViewRow = DataGridGarmentProcess.Rows[Index];
                    dataGridViewRow.Cells[0].Value = i + 1;
                    dataGridViewRow.Cells[1].Value = dtGarmentProcess.Rows[i]["Component"].ToString();
                    dataGridViewRow.Cells[2].Value = dtGarmentProcess.Rows[i]["Process"].ToString();
                    dataGridViewRow.Cells[3].Value = dtGarmentProcess.Rows[i]["Qty"].ToString();
                    dataGridViewRow.Cells[4].Value = dtGarmentProcess.Rows[i]["rate"].ToString();
                    dataGridViewRow.Cells[5].Value = dtGarmentProcess.Rows[i]["Total"].ToString();
                    dataGridViewRow.Cells[6].Value = dtGarmentProcess.Rows[i]["ComponentUid"].ToString();
                    dataGridViewRow.Cells[7].Value = dtGarmentProcess.Rows[i]["ProcessUid"].ToString();
                    dataGridViewRow.Cells[8].Value = dtGarmentProcess.Rows[i]["Uid"].ToString();
                }

                for (int i = 0; i < dtCMTProcess.Rows.Count; i++)
                {
                    int Index = DataGridCMT.Rows.Add();
                    DataGridViewRow dataGridViewRow = DataGridCMT.Rows[Index];
                    dataGridViewRow.Cells[0].Value = i + 1;
                    dataGridViewRow.Cells[1].Value = dtCMTProcess.Rows[i]["Coordinates"].ToString();
                    dataGridViewRow.Cells[2].Value = dtCMTProcess.Rows[i]["Operation"].ToString();
                    dataGridViewRow.Cells[3].Value = dtCMTProcess.Rows[i]["Qty"].ToString();
                    dataGridViewRow.Cells[4].Value = dtCMTProcess.Rows[i]["rate"].ToString();
                    dataGridViewRow.Cells[5].Value = dtCMTProcess.Rows[i]["Total"].ToString();
                    dataGridViewRow.Cells[6].Value = dtCMTProcess.Rows[i]["COOrdinatesUid"].ToString();
                    dataGridViewRow.Cells[7].Value = dtCMTProcess.Rows[i]["OperationUid"].ToString();
                    dataGridViewRow.Cells[8].Value = dtCMTProcess.Rows[i]["Uid"].ToString();
                }
                loadid = 0;
            }
            catch (Exception)
            {

            }
        }

        protected void LoadGenerals()
        {
            try
            {
                SelectId = 1;
                DataTable data = GetGeneralData(35, 1);
                CmbCOORDINATES.DataSource = null;
                CmbCOORDINATES.DisplayMember = "GeneralName";
                CmbCOORDINATES.ValueMember = "GUid";
                CmbCOORDINATES.DataSource = data;
                DataTable dataOp = GetGeneralData(34, 1);
                CmbOperations.DataSource = null;
                CmbOperations.DisplayMember = "GeneralName";
                CmbOperations.ValueMember = "GUid";
                CmbOperations.DataSource = dataOp;

                SqlParameter[] sqlParameters2 = { new SqlParameter("@TypeMUid", 31), new SqlParameter("@Active", 1) };
                DataTable dtGeneral = db.GetDataWithParam(CommandType.StoredProcedure, "SP_GetGeneralM", sqlParameters2, connection);
                CmbOtherExpHead.DataSource = null;
                CmbOtherExpHead.DisplayMember = "GeneralName";
                CmbOtherExpHead.ValueMember = "Guid";
                CmbOtherExpHead.DataSource = dtGeneral;
                SelectId = 0;
            }
            catch (Exception)
            {

                throw;
            }
        }

        private void TabControlBudjet_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void LoadYarnProcess()
        {
            try
            {
                DataGridYarnProcess.DataSource = null;
                DataGridYarnProcess.AutoGenerateColumns = false;
                DataGridYarnProcess.ColumnCount = 9;
                DataGridYarnProcess.Columns[0].Name = "SlNo";
                DataGridYarnProcess.Columns[0].HeaderText = "SlNo";
                DataGridYarnProcess.Columns[0].Width = 50;

                DataGridYarnProcess.Columns[1].Name = "YarnName";
                DataGridYarnProcess.Columns[1].HeaderText = "YarnName";
                DataGridYarnProcess.Columns[1].Width = 250;

                DataGridYarnProcess.Columns[2].Name = "Qty";
                DataGridYarnProcess.Columns[2].HeaderText = "Qty";
                DataGridYarnProcess.Columns[2].Width = 150;

                DataGridYarnProcess.Columns[3].Name = "Rate";
                DataGridYarnProcess.Columns[3].HeaderText = "Rate";
                DataGridYarnProcess.Columns[3].Width = 150;

                DataGridYarnProcess.Columns[4].Name = "Total";
                DataGridYarnProcess.Columns[4].HeaderText = "Total";
                DataGridYarnProcess.Columns[4].Width = 150;

                DataGridYarnProcess.Columns[5].Name = "YarnId";
                DataGridYarnProcess.Columns[5].HeaderText = "YarnId";
                DataGridYarnProcess.Columns[5].Visible = false;

                DataGridYarnProcess.Columns[6].Name = "FabricUid";
                DataGridYarnProcess.Columns[6].HeaderText = "FabricUid";
                DataGridYarnProcess.Columns[6].Visible = false;

                DataGridYarnProcess.Columns[7].Name = "Type";
                DataGridYarnProcess.Columns[7].HeaderText = "Type";
                DataGridYarnProcess.Columns[7].Visible = false;

                DataGridYarnProcess.Columns[8].Name = "BYarnUid";
                DataGridYarnProcess.Columns[8].HeaderText = "BYarnUid";
                DataGridYarnProcess.Columns[8].Visible = false;

                DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn = new DataGridViewCheckBoxColumn();
                dataGridViewCheckBoxColumn.Name = "Free of Cost";
                dataGridViewCheckBoxColumn.HeaderText = "Free of Cost";
                dataGridViewCheckBoxColumn.Width = 100;
                DataGridYarnProcess.Columns.Insert(9, dataGridViewCheckBoxColumn);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected void LoadYarnPurchase()
        {
            try
            {
                DataGridYarnPurchase.ColumnCount = 9;
                DataGridYarnPurchase.DataSource = null;
                DataGridYarnPurchase.AutoGenerateColumns = false;
                DataGridYarnPurchase.Columns[0].Name = "SlNo";
                DataGridYarnPurchase.Columns[0].HeaderText = "SlNo";
                DataGridYarnPurchase.Columns[0].Width = 50;

                DataGridYarnPurchase.Columns[1].Name = "YarnName";
                DataGridYarnPurchase.Columns[1].HeaderText = "YarnName";
                DataGridYarnPurchase.Columns[1].Width = 250;

                DataGridYarnPurchase.Columns[2].Name = "Qty";
                DataGridYarnPurchase.Columns[2].HeaderText = "Qty";
                DataGridYarnPurchase.Columns[2].Width = 150;

                DataGridYarnPurchase.Columns[3].Name = "Rate";
                DataGridYarnPurchase.Columns[3].HeaderText = "Rate";
                DataGridYarnPurchase.Columns[3].Width = 150;

                DataGridYarnPurchase.Columns[4].Name = "Total";
                DataGridYarnPurchase.Columns[4].HeaderText = "Total";
                DataGridYarnPurchase.Columns[4].Width = 150;

                DataGridYarnPurchase.Columns[5].Name = "YarnId";
                DataGridYarnPurchase.Columns[5].HeaderText = "YarnId";
                DataGridYarnPurchase.Columns[5].Visible = false;

                DataGridYarnPurchase.Columns[6].Name = "FabricUid";
                DataGridYarnPurchase.Columns[6].HeaderText = "FabricUid";
                DataGridYarnPurchase.Columns[6].Visible = false;

                DataGridYarnPurchase.Columns[7].Name = "Type";
                DataGridYarnPurchase.Columns[7].HeaderText = "Type";
                DataGridYarnPurchase.Columns[7].Visible = false;

                DataGridYarnPurchase.Columns[8].Name = "BYarnUid";
                DataGridYarnPurchase.Columns[8].HeaderText = "BYarnUid";
                DataGridYarnPurchase.Columns[8].Visible = false;

                DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn = new DataGridViewCheckBoxColumn();
                dataGridViewCheckBoxColumn.Name = "Free of Cost";
                dataGridViewCheckBoxColumn.HeaderText = "Free of Cost";
                dataGridViewCheckBoxColumn.Width = 100;
                DataGridYarnPurchase.Columns.Insert(9, dataGridViewCheckBoxColumn);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

        }

        protected void LoadFabricProcess()
        {
            try
            {
                DataGridFabricProcess.DataSource = null;
                DataGridFabricProcess.AutoGenerateColumns = false;
                DataGridFabricProcess.ColumnCount = 10;
                DataGridFabricProcess.Columns[0].Name = "SlNo";
                DataGridFabricProcess.Columns[0].HeaderText = "SlNo";
                DataGridFabricProcess.Columns[0].Width = 50;

                DataGridFabricProcess.Columns[1].Name = "Fabric";
                DataGridFabricProcess.Columns[1].HeaderText = "Fabric";
                DataGridFabricProcess.Columns[1].Width = 420;

                DataGridFabricProcess.Columns[2].Name = "Colour";
                DataGridFabricProcess.Columns[2].HeaderText = "Colour";
                DataGridFabricProcess.Columns[2].Width = 150;

                DataGridFabricProcess.Columns[3].Name = "Process";
                DataGridFabricProcess.Columns[3].HeaderText = "Process";
                DataGridFabricProcess.Columns[3].Width = 150;

                DataGridFabricProcess.Columns[4].Name = "Qty";
                DataGridFabricProcess.Columns[4].HeaderText = "Qty";
                DataGridFabricProcess.Columns[4].Width = 80;
                DataGridFabricProcess.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

                DataGridFabricProcess.Columns[5].Name = "Rate";
                DataGridFabricProcess.Columns[5].HeaderText = "Rate";
                DataGridFabricProcess.Columns[5].Width = 80;
                DataGridFabricProcess.Columns[5].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

                DataGridFabricProcess.Columns[6].Name = "Total";
                DataGridFabricProcess.Columns[6].HeaderText = "Total";
                DataGridFabricProcess.Columns[6].Width = 80;
                DataGridFabricProcess.Columns[6].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

                DataGridFabricProcess.Columns[7].Name = "FabricUid";
                DataGridFabricProcess.Columns[7].HeaderText = "FabricUid";
                DataGridFabricProcess.Columns[7].Visible = false;

                DataGridFabricProcess.Columns[8].Name = "Type";
                DataGridFabricProcess.Columns[8].HeaderText = "Type";
                DataGridFabricProcess.Columns[8].Visible = false;

                DataGridFabricProcess.Columns[9].Name = "FBUid";
                DataGridFabricProcess.Columns[9].HeaderText = "FBUid";
                DataGridFabricProcess.Columns[9].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected void LoadFabricPurchase()
        {
            try
            {
                DataGridFabricPurchase.DataSource = null;
                DataGridFabricPurchase.AutoGenerateColumns = false;
                DataGridFabricPurchase.ColumnCount = 10;
                DataGridFabricPurchase.Columns[0].Name = "SlNo";
                DataGridFabricPurchase.Columns[0].HeaderText = "SlNo";
                DataGridFabricPurchase.Columns[0].Width = 50;

                DataGridFabricPurchase.Columns[1].Name = "Fabric";
                DataGridFabricPurchase.Columns[1].HeaderText = "Fabric";
                DataGridFabricPurchase.Columns[1].Width = 400;


                DataGridFabricPurchase.Columns[2].Name = "Colour";
                DataGridFabricPurchase.Columns[2].HeaderText = "Colour";
                DataGridFabricPurchase.Columns[2].Width = 150;

                DataGridFabricPurchase.Columns[3].Name = "Process";
                DataGridFabricPurchase.Columns[3].HeaderText = "Process";
                DataGridFabricPurchase.Columns[3].Width = 150;

                DataGridFabricPurchase.Columns[4].Name = "Qty";
                DataGridFabricPurchase.Columns[4].HeaderText = "Qty";
                DataGridFabricPurchase.Columns[4].Width = 100;
                DataGridFabricPurchase.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

                DataGridFabricPurchase.Columns[5].Name = "Rate";
                DataGridFabricPurchase.Columns[5].HeaderText = "Rate";
                DataGridFabricPurchase.Columns[5].Width = 100;
                DataGridFabricPurchase.Columns[5].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

                DataGridFabricPurchase.Columns[6].Name = "Total";
                DataGridFabricPurchase.Columns[6].HeaderText = "Total";
                DataGridFabricPurchase.Columns[6].Width = 100;
                DataGridFabricPurchase.Columns[6].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

                DataGridFabricPurchase.Columns[7].Name = "FabricUid";
                DataGridFabricPurchase.Columns[7].HeaderText = "FabricUid";
                DataGridFabricPurchase.Columns[7].Visible = false;

                DataGridFabricPurchase.Columns[8].Name = "Type";
                DataGridFabricPurchase.Columns[8].HeaderText = "Type";
                DataGridFabricPurchase.Columns[8].Visible = false;

                DataGridFabricPurchase.Columns[9].Name = "FBUid";
                DataGridFabricPurchase.Columns[9].HeaderText = "FBUid";
                DataGridFabricPurchase.Columns[9].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected void LoadTrims()
        {
            try
            {
                DataGridTrims.DataSource = null;
                DataGridTrims.AutoGenerateColumns = false;
                DataGridTrims.ColumnCount = 7;
                DataGridTrims.Columns[0].Name = "SlNo";
                DataGridTrims.Columns[0].HeaderText = "SlNo";
                DataGridTrims.Columns[0].Width = 50;

                DataGridTrims.Columns[1].Name = "TrimsName";
                DataGridTrims.Columns[1].HeaderText = "TrimsName";
                DataGridTrims.Columns[1].Width = 350;

                DataGridTrims.Columns[2].Name = "Qty";
                DataGridTrims.Columns[2].HeaderText = "Qty";
                DataGridTrims.Columns[2].Width = 100;
                DataGridTrims.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

                DataGridTrims.Columns[3].Name = "Rate";
                DataGridTrims.Columns[3].HeaderText = "Rate";
                DataGridTrims.Columns[3].Width = 100;
                DataGridTrims.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

                DataGridTrims.Columns[4].Name = "Total";
                DataGridTrims.Columns[4].HeaderText = "Total";
                DataGridTrims.Columns[4].Width = 100;
                DataGridTrims.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

                DataGridTrims.Columns[5].Name = "Uid";
                DataGridTrims.Columns[5].HeaderText = "Uid";
                DataGridTrims.Columns[5].Visible = false;

                DataGridTrims.Columns[6].Name = "TUid";
                DataGridTrims.Columns[6].HeaderText = "TUid";
                DataGridTrims.Columns[6].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected void LoadTrimsProcess()
        {
            try
            {
                DataGridTrimsProcess.DataSource = null;
                DataGridTrimsProcess.AutoGenerateColumns = false;
                DataGridTrimsProcess.ColumnCount = 9;
                DataGridTrimsProcess.Columns[0].Name = "SlNo";
                DataGridTrimsProcess.Columns[0].HeaderText = "SlNo";
                DataGridTrimsProcess.Columns[0].Width = 50;

                DataGridTrimsProcess.Columns[1].Name = "ItemName";
                DataGridTrimsProcess.Columns[1].HeaderText = "ItemName";
                DataGridTrimsProcess.Columns[1].Width = 350;

                DataGridTrimsProcess.Columns[2].Name = "Process";
                DataGridTrimsProcess.Columns[2].HeaderText = "Process";
                DataGridTrimsProcess.Columns[2].Width = 150;

                DataGridTrimsProcess.Columns[3].Name = "Qty";
                DataGridTrimsProcess.Columns[3].HeaderText = "Qty";
                DataGridTrimsProcess.Columns[3].Width = 100;
                DataGridTrimsProcess.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

                DataGridTrimsProcess.Columns[4].Name = "Rate";
                DataGridTrimsProcess.Columns[4].HeaderText = "Rate";
                DataGridTrimsProcess.Columns[4].Width = 100;
                DataGridTrimsProcess.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

                DataGridTrimsProcess.Columns[5].Name = "Total";
                DataGridTrimsProcess.Columns[5].HeaderText = "Total";
                DataGridTrimsProcess.Columns[5].Width = 100;
                DataGridTrimsProcess.Columns[5].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

                DataGridTrimsProcess.Columns[6].Name = "TrimsUid";
                DataGridTrimsProcess.Columns[6].HeaderText = "Uid";
                DataGridTrimsProcess.Columns[6].Visible = false;

                DataGridTrimsProcess.Columns[7].Name = "ProcessUid";
                DataGridTrimsProcess.Columns[7].HeaderText = "ProcessUid";
                DataGridTrimsProcess.Columns[7].Visible = false;

                DataGridTrimsProcess.Columns[8].Name = "TUid";
                DataGridTrimsProcess.Columns[8].HeaderText = "TUid";
                DataGridTrimsProcess.Columns[8].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected void LoadGarmentProcess()
        {
            try
            {
                DataGridGarmentProcess.DataSource = null;
                DataGridGarmentProcess.AutoGenerateColumns = false;
                DataGridGarmentProcess.ColumnCount = 9;
                DataGridGarmentProcess.Columns[0].Name = "SlNo";
                DataGridGarmentProcess.Columns[0].HeaderText = "SlNo";
                DataGridGarmentProcess.Columns[0].Width = 50;

                DataGridGarmentProcess.Columns[1].Name = "Component";
                DataGridGarmentProcess.Columns[1].HeaderText = "Component";
                DataGridGarmentProcess.Columns[1].Width = 350;

                DataGridGarmentProcess.Columns[2].Name = "Process";
                DataGridGarmentProcess.Columns[2].HeaderText = "Process";
                DataGridGarmentProcess.Columns[2].Width = 150;

                DataGridGarmentProcess.Columns[3].Name = "Qty";
                DataGridGarmentProcess.Columns[3].HeaderText = "Qty";
                DataGridGarmentProcess.Columns[3].Width = 100;
                DataGridGarmentProcess.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

                DataGridGarmentProcess.Columns[4].Name = "Rate";
                DataGridGarmentProcess.Columns[4].HeaderText = "Rate";
                DataGridGarmentProcess.Columns[4].Width = 100;
                DataGridGarmentProcess.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

                DataGridGarmentProcess.Columns[5].Name = "Total";
                DataGridGarmentProcess.Columns[5].HeaderText = "Total";
                DataGridGarmentProcess.Columns[5].Width = 100;
                DataGridGarmentProcess.Columns[5].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

                DataGridGarmentProcess.Columns[6].Name = "ComponentUid";
                DataGridGarmentProcess.Columns[6].HeaderText = "ComponentUid";
                DataGridGarmentProcess.Columns[6].Visible = false;

                DataGridGarmentProcess.Columns[7].Name = "ProcessUid";
                DataGridGarmentProcess.Columns[7].HeaderText = "ProcessUid";
                DataGridGarmentProcess.Columns[7].Visible = false;

                DataGridGarmentProcess.Columns[8].HeaderText = "TUid";
                DataGridGarmentProcess.Columns[8].Name = "TUid";
                DataGridGarmentProcess.Columns[8].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected void LoadCMT()
        {
            try
            {
                DataGridCMT.DataSource = null;
                DataGridCMT.AutoGenerateColumns = false;
                DataGridCMT.ColumnCount = 9;
                DataGridCMT.Columns[0].Name = "SlNo";
                DataGridCMT.Columns[0].HeaderText = "SlNo";
                DataGridCMT.Columns[0].Width = 50;

                DataGridCMT.Columns[1].Name = "CO-ORDINATES";
                DataGridCMT.Columns[1].HeaderText = "CO-ORDINATES";
                DataGridCMT.Columns[1].Width = 200;

                DataGridCMT.Columns[2].Name = "Operations";
                DataGridCMT.Columns[2].HeaderText = "Operations";
                DataGridCMT.Columns[2].Width = 150;

                DataGridCMT.Columns[3].Name = "Qty";
                DataGridCMT.Columns[3].HeaderText = "Qty";
                DataGridCMT.Columns[3].Width = 100;
                DataGridCMT.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

                DataGridCMT.Columns[4].Name = "Rate";
                DataGridCMT.Columns[4].HeaderText = "Rate";
                DataGridCMT.Columns[4].Width = 100;
                DataGridCMT.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

                DataGridCMT.Columns[5].Name = "Total";
                DataGridCMT.Columns[5].HeaderText = "Total";
                DataGridCMT.Columns[5].Width = 100;
                DataGridCMT.Columns[5].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

                DataGridCMT.Columns[6].Name = "CUid";
                DataGridCMT.Columns[6].HeaderText = "CUid";
                DataGridCMT.Columns[6].Visible = false;

                DataGridCMT.Columns[7].Name = "OUid";
                DataGridCMT.Columns[7].HeaderText = "OUid";
                DataGridCMT.Columns[7].Visible = false;

                DataGridCMT.Columns[8].HeaderText = "TUid";
                DataGridCMT.Columns[8].Name = "TUid";
                DataGridCMT.Columns[8].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected void LoadOtherExpenses()
        {
            try
            {
                DataGridOtherExpenses.DataSource = null;
                DataGridOtherExpenses.AutoGenerateColumns = false;
                DataGridOtherExpenses.ColumnCount = 8;
                DataGridOtherExpenses.Columns[0].Name = "SlNo";
                DataGridOtherExpenses.Columns[0].HeaderText = "SlNo";
                DataGridOtherExpenses.Columns[0].Width = 50;

                DataGridOtherExpenses.Columns[1].Name = "Name";
                DataGridOtherExpenses.Columns[1].HeaderText = "Name";
                DataGridOtherExpenses.Columns[1].Width = 150;

                DataGridOtherExpenses.Columns[2].Name = "Type";
                DataGridOtherExpenses.Columns[2].HeaderText = "Type";
                DataGridOtherExpenses.Columns[2].Width = 100;

                DataGridOtherExpenses.Columns[3].Name = "Qty/Value";
                DataGridOtherExpenses.Columns[3].HeaderText = "Qty/Value";
                DataGridOtherExpenses.Columns[3].Width = 100;

                DataGridOtherExpenses.Columns[4].Name = "Rate/Percentage";
                DataGridOtherExpenses.Columns[4].HeaderText = "Rate/Percentage";
                DataGridOtherExpenses.Columns[4].Width = 100;

                DataGridOtherExpenses.Columns[5].Name = "Total";
                DataGridOtherExpenses.Columns[5].HeaderText = "Total";
                DataGridOtherExpenses.Columns[5].Width = 100;

                DataGridOtherExpenses.Columns[6].Name = "Guid";
                DataGridOtherExpenses.Columns[6].HeaderText = "Guid";
                DataGridOtherExpenses.Columns[6].Visible = false;

                DataGridOtherExpenses.Columns[7].Name = "Ouid";
                DataGridOtherExpenses.Columns[7].HeaderText = "Ouid";
                DataGridOtherExpenses.Columns[7].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void TabContronYan_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (tabContronYan.SelectedIndex == 1)
                {
                    SqlParameter[] parameters = { new SqlParameter("@OrderMuid", txtDocNo.Tag) };
                    DataTable dt = db.GetDataWithParam(CommandType.StoredProcedure, "Proc_GetYarnforBom", parameters, connection);
                    CmbYarn.DisplayMember = "YarnName";
                    CmbYarn.ValueMember = "YarnId";
                    CmbYarn.DataSource = dt;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void BtnYarnProcessOk_Click(object sender, EventArgs e)
        {
            try
            {
                if (CmbYarn.SelectedIndex != -1 && txtPurchaseRate.Text != string.Empty && txtYarnPurchaseQty.Text != string.Empty)
                {
                    decimal Qty = Convert.ToDecimal(txtYarnPurchaseQty.Text);
                    decimal Rate = Convert.ToDecimal(txtPurchaseRate.Text);
                    int Index = DataGridYarnPurchase.Rows.Add();
                    DataGridViewRow dataGridViewRow = DataGridYarnPurchase.Rows[Index];
                    dataGridViewRow.Cells[0].Value = DataGridYarnPurchase.Rows.Count;
                    dataGridViewRow.Cells[1].Value = CmbYarn.Text;
                    dataGridViewRow.Cells[2].Value = txtYarnPurchaseQty.Text;
                    dataGridViewRow.Cells[3].Value = txtPurchaseRate.Text;
                    dataGridViewRow.Cells[4].Value = (Qty * Rate).ToString("0.00");
                    dataGridViewRow.Cells[5].Value = CmbYarn.SelectedValue;
                    dataGridViewRow.Cells[6].Value = "0";
                    dataGridViewRow.Cells[7].Value = "Purchase";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void DataGridYarnProcess_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex == 3 && loadid == 0)
                {
                    decimal qty = Convert.ToDecimal(DataGridYarnProcess.CurrentRow.Cells[2].Value.ToString());
                    decimal rate = Convert.ToDecimal(DataGridYarnProcess.CurrentRow.Cells[3].Value.ToString());
                    DataGridYarnProcess.CurrentRow.Cells[4].Value = (qty * rate).ToString("0.00");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void BtnFabricOk_Click(object sender, EventArgs e)
        {
            try
            {
                if (CmbFabricPurchase.SelectedIndex != -1 && txtFabricQty.Text != string.Empty && txtFabricAmount.Text != string.Empty)
                {
                    decimal Qty = Convert.ToDecimal(txtFabricQty.Text);
                    decimal Rate = Convert.ToDecimal(txtFabricAmount.Text);
                    int Index = DataGridFabricPurchase.Rows.Add();
                    DataGridViewRow dataGridViewRow = DataGridFabricPurchase.Rows[Index];
                    dataGridViewRow.Cells[0].Value = DataGridFabricPurchase.Rows.Count;
                    dataGridViewRow.Cells[1].Value = CmbYarn.Text;
                    dataGridViewRow.Cells[2].Value = CmbYarn.Text;
                    dataGridViewRow.Cells[3].Value = CmbYarn.Text;
                    dataGridViewRow.Cells[4].Value = txtYarnPurchaseQty.Text;
                    dataGridViewRow.Cells[5].Value = txtPurchaseRate.Text;
                    dataGridViewRow.Cells[6].Value = (Qty * Rate).ToString("0.00");
                    dataGridViewRow.Cells[7].Value = CmbYarn.SelectedValue;
                    dataGridViewRow.Cells[8].Value = "Purchase";
                    dataGridViewRow.Cells[9].Value = "0";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void DataGridFabricProcess_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex == 5 && loadid == 0)
                {
                    decimal qty = Convert.ToDecimal(DataGridFabricProcess.CurrentRow.Cells[4].Value.ToString());
                    decimal rate = Convert.ToDecimal(DataGridFabricProcess.CurrentRow.Cells[5].Value.ToString());
                    DataGridFabricProcess.CurrentRow.Cells[6].Value = (qty * rate).ToString("0.00");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void DataGridTrims_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex == 3 && loadid == 0)
                {
                    decimal qty = Convert.ToDecimal(DataGridTrims.CurrentRow.Cells[2].Value.ToString());
                    decimal rate = Convert.ToDecimal(DataGridTrims.CurrentRow.Cells[3].Value.ToString());
                    DataGridTrims.CurrentRow.Cells[4].Value = (qty * rate).ToString("0.00");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void SplitSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtPcrate.Text == string.Empty && txtGrossValue.Text == string.Empty && txtExrate.Text == string.Empty)
                {
                    MessageBox.Show("Rate Can't 0");
                    txtPcrate.Focus();
                    return;
                }
                if (SplitSave.Text == "Save")
                {
                    if (txtDocNo.Text != string.Empty)
                    {
                        if (txtOrderNo.Tag.ToString() == "")
                        {
                            txtOrderNo.Tag = "0";
                        }
                        string Query = "Update OrderM Set ExRate =" + txtExrate.Text + " ,AvgRate=" + txtPcrate.Text + " ,GrossValue =" + txtGrossValue.Text + " Where Uid = " + txtDocNo.Tag + "";
                        db.ExecuteNonQuery(CommandType.Text, Query, connection);
                        SqlParameter[] sqlParameters = {
                            new SqlParameter("@Uid",txtOrderNo.Tag),
                            new SqlParameter("@DocNo",txtOrderNo.Text),
                            new SqlParameter("@DocDate",Convert.ToDateTime(dtpDocDate.Text)),
                            new SqlParameter("@OrdermUid",txtDocNo.Tag),
                            new SqlParameter("@ReturnUid",SqlDbType.Decimal),
                            new SqlParameter("@Sts","Pending")
                        };
                        sqlParameters[4].Direction = ParameterDirection.Output;
                        decimal uid = db.ExecuteNonQuery(CommandType.StoredProcedure, "Proc_OrderMBudjet", sqlParameters, connection, 4);

                        if (YarnRefresh == 1)
                        {
                            SqlParameter[] sqlParametersYarn = {
                                new SqlParameter("@OrderMuid",txtDocNo.Tag),
                                new SqlParameter("@OrderMBudjetUid",uid)
                            };
                            db.ExecuteNonQuery(CommandType.StoredProcedure, "Proc_OrderMBudjetYarn_Deleted", sqlParametersYarn, connection);
                        }
                        for (int i = 0; i < DataGridYarnProcess.Rows.Count; i++)
                        {
                            bool freeofcost = false;
                            DataGridViewCheckBoxCell cellComponent = DataGridYarnProcess.Rows[i].Cells[9] as DataGridViewCheckBoxCell;
                            if (cellComponent.Value != null)
                            {
                                if (cellComponent.Value.ToString() == "True")
                                {
                                    freeofcost = true;
                                }
                            }
                            SqlParameter[] yarnProcessParameter = {
                                    new SqlParameter("@Uid",DataGridYarnProcess.Rows[i].Cells[8].Value.ToString()),
                                    new SqlParameter("@OrdermUid",txtDocNo.Tag),
                                    new SqlParameter("@OrderMBudjetUid",uid),
                                    new SqlParameter("@YarnUid",DataGridYarnProcess.Rows[i].Cells[5].Value.ToString()),
                                    new SqlParameter("@YarnName",DataGridYarnProcess.Rows[i].Cells[1].Value.ToString()),
                                    new SqlParameter("@Rate",DataGridYarnProcess.Rows[i].Cells[3].Value.ToString()),
                                    new SqlParameter("@Total",DataGridYarnProcess.Rows[i].Cells[4].Value.ToString()),
                                    new SqlParameter("@YarnTag",DataGridYarnProcess.Rows[i].Cells[7].Value.ToString()),
                                    new SqlParameter("@Qty",DataGridYarnProcess.Rows[i].Cells[2].Value.ToString()),
                                    new SqlParameter("@FreeOfCost",freeofcost),
                                };
                            db.ExecuteNonQuery(CommandType.StoredProcedure, "Proc_OrderMBudjetYarn", yarnProcessParameter, connection);
                        }


                        for (int i = 0; i < DataGridYarnPurchase.Rows.Count; i++)
                        {
                            bool freeofcost = false;
                            DataGridViewCheckBoxCell cellComponent = DataGridYarnPurchase.Rows[i].Cells[9] as DataGridViewCheckBoxCell;
                            if (cellComponent.Value != null)
                            {
                                if (cellComponent.Value.ToString() == "True")
                                {
                                    freeofcost = true;
                                }
                            }
                            SqlParameter[] yarnPurchaseParameter = {
                                new SqlParameter("@Uid",DataGridYarnPurchase.Rows[i].Cells[8].Value.ToString()),
                                new SqlParameter("@OrdermUid",txtDocNo.Tag),
                                new SqlParameter("@OrderMBudjetUid",uid),
                                new SqlParameter("@YarnUid",DataGridYarnPurchase.Rows[i].Cells[5].Value.ToString()),
                                new SqlParameter("@YarnName",DataGridYarnPurchase.Rows[i].Cells[1].Value.ToString()),
                                new SqlParameter("@Rate",Convert.ToDecimal(DataGridYarnPurchase.Rows[i].Cells[3].Value.ToString())),
                                new SqlParameter("@Total",DataGridYarnPurchase.Rows[i].Cells[4].Value.ToString()),
                                new SqlParameter("@YarnTag",DataGridYarnPurchase.Rows[i].Cells[7].Value.ToString()),
                                new SqlParameter("@Qty",DataGridYarnPurchase.Rows[i].Cells[2].Value.ToString()),
                                new SqlParameter("@FreeOfCost",freeofcost),
                            };
                            db.ExecuteNonQuery(CommandType.StoredProcedure, "Proc_OrderMBudjetYarn", yarnPurchaseParameter, connection);
                        }

                        if (FabricRefresh == 1)
                        {
                            SqlParameter[] sqlParametersFabric = {
                                new SqlParameter("@OrderMuid",txtDocNo.Tag),
                                new SqlParameter("@OrderMBudjetUid",uid)
                            };
                            db.ExecuteNonQuery(CommandType.StoredProcedure, "Proc_OrderMBudjetFabric_Deleted", sqlParametersFabric, connection);
                        }
                        for (int i = 0; i < DataGridFabricProcess.Rows.Count; i++)
                        {
                            SqlParameter[] fabricProcessParameter = {
                                new SqlParameter("@Uid",DataGridFabricProcess.Rows[i].Cells[9].Value.ToString()),
                                new SqlParameter("@OrdermUid",txtDocNo.Tag),
                                new SqlParameter("@OrderMBudjetUid",uid),
                                new SqlParameter("@FabricUid",DataGridFabricProcess.Rows[i].Cells[7].Value.ToString()),
                                new SqlParameter("@FabricName",DataGridFabricProcess.Rows[i].Cells[1].Value.ToString()),
                                new SqlParameter("@Rate",DataGridFabricProcess.Rows[i].Cells[5].Value.ToString()),
                                new SqlParameter("@Total",DataGridFabricProcess.Rows[i].Cells[6].Value.ToString()),
                                new SqlParameter("@FabTag",DataGridFabricProcess.Rows[i].Cells[8].Value.ToString()),
                                new SqlParameter("@Qty",DataGridFabricProcess.Rows[i].Cells[4].Value.ToString()),
                                new SqlParameter("@Colour",DataGridFabricProcess.Rows[i].Cells[2].Value.ToString()),
                                new SqlParameter("@Process",DataGridFabricProcess.Rows[i].Cells[3].Value.ToString()),
                            };
                            db.ExecuteNonQuery(CommandType.StoredProcedure, "Proc_OrderMBudjetFabric", fabricProcessParameter, connection);
                        }

                        for (int i = 0; i < DataGridFabricPurchase.Rows.Count; i++)
                        {
                            SqlParameter[] fabricPurchaseParameter = {
                                new SqlParameter("@Uid",DataGridFabricPurchase.Rows[i].Cells[9].Value.ToString()),
                                new SqlParameter("@OrdermUid",txtDocNo.Tag),
                                new SqlParameter("@OrderMBudjetUid",uid),
                                new SqlParameter("@FabricUid",DataGridFabricPurchase.Rows[i].Cells[7].Value.ToString()),
                                new SqlParameter("@FabricName",DataGridFabricPurchase.Rows[i].Cells[1].Value.ToString()),
                                new SqlParameter("@Rate",DataGridFabricPurchase.Rows[i].Cells[5].Value.ToString()),
                                new SqlParameter("@Total",DataGridFabricPurchase.Rows[i].Cells[6].Value.ToString()),
                                new SqlParameter("@FabTag",DataGridFabricPurchase.Rows[i].Cells[8].Value.ToString()),
                                new SqlParameter("@Qty",DataGridFabricPurchase.Rows[i].Cells[4].Value.ToString()),
                                new SqlParameter("@Colour",DataGridFabricPurchase.Rows[i].Cells[2].Value.ToString()),
                                new SqlParameter("@Process",DataGridFabricPurchase.Rows[i].Cells[3].Value.ToString()),
                            };
                            db.ExecuteNonQuery(CommandType.StoredProcedure, "Proc_OrderMBudjetFabric", fabricPurchaseParameter, connection);
                        }

                        if (TrimsReferesh == 1)
                        {
                            SqlParameter[] sqlParametersTrims = {
                                new SqlParameter("@OrderMuid",txtDocNo.Tag),
                                new SqlParameter("@OrderMBudjetUid",uid)
                            };
                            db.ExecuteNonQuery(CommandType.StoredProcedure, "Proc_OrderMBudjetTrims_Deleted", sqlParametersTrims, connection);
                        }

                        for (int i = 0; i < DataGridTrims.Rows.Count; i++)
                        {
                            SqlParameter[] TrimsParameter = {
                                new SqlParameter("@Uid",DataGridTrims.Rows[i].Cells[6].Value.ToString()),
                                new SqlParameter("@OrdermUid",txtDocNo.Tag),
                                new SqlParameter("@OrderMBudjetUid",uid),
                                new SqlParameter("@TrimsUid",DataGridTrims.Rows[i].Cells[5].Value.ToString()),
                                new SqlParameter("@TrimsName",DataGridTrims.Rows[i].Cells[1].Value.ToString()),
                                new SqlParameter("@Rate",DataGridTrims.Rows[i].Cells[3].Value.ToString()),
                                new SqlParameter("@Total",DataGridTrims.Rows[i].Cells[4].Value.ToString()),
                                new SqlParameter("@Qty",DataGridTrims.Rows[i].Cells[2].Value.ToString()),
                            };
                            db.ExecuteNonQuery(CommandType.StoredProcedure, "Proc_OrderMBudjetTrims", TrimsParameter, connection);
                        }

                        for (int i = 0; i < DataGridOtherExpenses.Rows.Count; i++)
                        {
                            SqlParameter[] sqlParametersExp = {
                                new SqlParameter("@Uid",DataGridOtherExpenses.Rows[i].Cells[7].Value.ToString()),
                                new SqlParameter("@BudjectUid",uid),
                                new SqlParameter("@OrderMUid",txtDocNo.Tag),
                                new SqlParameter("@ExpenseUid",DataGridOtherExpenses.Rows[i].Cells[6].Value.ToString()),
                                new SqlParameter("@ExpType",DataGridOtherExpenses.Rows[i].Cells[2].Value.ToString()),
                                new SqlParameter("@Qty",DataGridOtherExpenses.Rows[i].Cells[3].Value.ToString()),
                                new SqlParameter("@Percentage",DataGridOtherExpenses.Rows[i].Cells[4].Value.ToString()),
                                new SqlParameter("@Rate",DataGridOtherExpenses.Rows[i].Cells[4].Value.ToString()),
                                new SqlParameter("@TotalValue",DataGridOtherExpenses.Rows[i].Cells[5].Value.ToString()),
                            };
                            db.ExecuteNonQuery(CommandType.StoredProcedure, "Proc_OrderMBudjetOtherExp", sqlParametersExp, connection);
                        }

                        if (TrimsProcessRefresh == 1)
                        {
                            SqlParameter[] sqlParametersTrimsProcess = {
                                new SqlParameter("@OrderMuid",txtDocNo.Tag),
                                new SqlParameter("@OrderMBudjetUid",uid)
                            };
                            db.ExecuteNonQuery(CommandType.StoredProcedure, "Proc_OrderMBudjetTrimsProcess_Deleted", sqlParametersTrimsProcess, connection);
                        }

                        for (int i = 0; i < DataGridTrimsProcess.Rows.Count; i++)
                        {
                            SqlParameter[] sqlParametersExp = {
                                new SqlParameter("@Uid",DataGridTrimsProcess.Rows[i].Cells[8].Value.ToString()),
                                new SqlParameter("@OrderMBudjetUid",uid),
                                new SqlParameter("@OrderMUid",txtDocNo.Tag),
                                new SqlParameter("@TrimsUid",DataGridTrimsProcess.Rows[i].Cells[6].Value.ToString()),
                                new SqlParameter("@ProcessUid",DataGridTrimsProcess.Rows[i].Cells[7].Value.ToString()),
                                new SqlParameter("@Qty",DataGridTrimsProcess.Rows[i].Cells[3].Value.ToString()),
                                new SqlParameter("@Rate",DataGridTrimsProcess.Rows[i].Cells[4].Value.ToString()),
                                new SqlParameter("@Total",DataGridTrimsProcess.Rows[i].Cells[5].Value.ToString()),
                            };
                            db.ExecuteNonQuery(CommandType.StoredProcedure, "Proc_OrderMBudjetTrimsProcess", sqlParametersExp, connection);
                        }

                        if (GarmentProcessRefresh == 1)
                        {
                            SqlParameter[] sqlParametersGarmentProcess = {
                                new SqlParameter("@OrderMuid",txtDocNo.Tag),
                                new SqlParameter("@OrderMBudjetUid",uid)
                            };
                            db.ExecuteNonQuery(CommandType.StoredProcedure, "Proc_OrderMBudjetGarmentProcess_Deleted", sqlParametersGarmentProcess, connection);
                        }

                        for (int i = 0; i < DataGridGarmentProcess.Rows.Count; i++)
                        {
                            SqlParameter[] sqlParametersTrimsProcess = {
                                new SqlParameter("@Uid",DataGridGarmentProcess.Rows[i].Cells[8].Value.ToString()),
                                new SqlParameter("@OrderMBudjetUid",uid),
                                new SqlParameter("@OrderMUid",txtDocNo.Tag),
                                new SqlParameter("@ComponentUid",DataGridGarmentProcess.Rows[i].Cells[6].Value.ToString()),
                                new SqlParameter("@ProcessUid",DataGridGarmentProcess.Rows[i].Cells[7].Value.ToString()),
                                new SqlParameter("@Qty",DataGridGarmentProcess.Rows[i].Cells[3].Value.ToString()),
                                new SqlParameter("@Rate",DataGridGarmentProcess.Rows[i].Cells[4].Value.ToString()),
                                new SqlParameter("@Total",DataGridGarmentProcess.Rows[i].Cells[5].Value.ToString()),
                            };
                            db.ExecuteNonQuery(CommandType.StoredProcedure, "Proc_OrderMBudjetGarmentProcess", sqlParametersTrimsProcess, connection);
                        }

                        for (int i = 0; i < DataGridCMT.Rows.Count; i++)
                        {
                            SqlParameter[] sqlParametersTrimsProcess = {
                                new SqlParameter("@Uid",DataGridCMT.Rows[i].Cells[8].Value.ToString()),
                                new SqlParameter("@OrderMBudjetUid",uid),
                                new SqlParameter("@OrderMUid",txtDocNo.Tag),
                                new SqlParameter("@COOrdinatesUid",DataGridCMT.Rows[i].Cells[6].Value.ToString()),
                                new SqlParameter("@OperationUid",DataGridCMT.Rows[i].Cells[7].Value.ToString()),
                                new SqlParameter("@Qty",DataGridCMT.Rows[i].Cells[3].Value.ToString()),
                                new SqlParameter("@Rate",DataGridCMT.Rows[i].Cells[4].Value.ToString()),
                                new SqlParameter("@Total",DataGridCMT.Rows[i].Cells[5].Value.ToString()),
                            };
                            db.ExecuteNonQuery(CommandType.StoredProcedure, "Proc_OrderMBudjetCMTProcess", sqlParametersTrimsProcess, connection);
                        }

                        if (txtOrderNo.Tag.ToString() == "0")
                        {
                            string Query1 = "Update DocTypeM Set Lastno =Lastno+1 Where DocTypeId = 4";
                            db.ExecuteNonQuery(CommandType.Text, Query1, connection);
                        }
                        MessageBox.Show("Record has been saved successfully", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        DataGridFabricProcess.Rows.Clear();
                        DataGridFabricPurchase.Rows.Clear();
                        DataGridYarnProcess.Rows.Clear();
                        DataGridYarnPurchase.Rows.Clear();
                        DataGridTrims.Rows.Clear();
                        DataGridTrimsProcess.Rows.Clear();
                        DataGridGarmentProcess.Rows.Clear();
                        DataGridCMT.Rows.Clear();
                        txtDocNo.Text = string.Empty;
                        txtOrderNo.Text = string.Empty;
                        GrFront.Visible = true;
                        GrBack.Visible = false;
                        YarnRefresh = 0;
                        FabricRefresh = 0;
                        TrimsReferesh = 0;
                        TrimsProcessRefresh = 0;
                        GarmentProcessRefresh = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void BtnExpensesOk_Click(object sender, EventArgs e)
        {
            try
            {
                if (CmbOtherExpHead.SelectedIndex != -1 || CmbType.SelectedIndex != -1)
                {
                    if (txtPercentage.Text == string.Empty)
                    {
                        txtPercentage.Text = "0";
                    }
                    if (txtRate.Text == string.Empty)
                    {
                        txtRate.Text = "0";
                    }
                    decimal qty = 0;
                    decimal Percentage = 0;
                    decimal Value = 0;
                    decimal rate = 0;
                    if (CmbType.Text == "Flat Rate")
                    {
                        qty = Convert.ToDecimal(dtOrderQty.Rows[0]["OrderPlanQty"].ToString());
                        Value = Convert.ToDecimal(txtRate.Text);
                        rate = 0;
                    }
                    else if (CmbType.Text == "Pcs Rate")
                    {
                        qty = Convert.ToDecimal(dtOrderQty.Rows[0]["OrderPlanQty"].ToString());
                        Value = qty * Convert.ToDecimal(txtRate.Text);
                        rate = Convert.ToDecimal(txtRate.Text);
                    }
                    else
                    {
                        qty = Convert.ToDecimal(txtGrossValue.Text);
                        Value = ((qty * Convert.ToDecimal(txtPercentage.Text)) / 100);
                        rate = Convert.ToDecimal(txtPercentage.Text);
                    }
                    SelectId = 1;
                    Percentage = Convert.ToDecimal(txtPercentage.Text);
                    int Index = DataGridOtherExpenses.Rows.Add();
                    DataGridViewRow dataGridViewRow = DataGridOtherExpenses.Rows[Index];
                    dataGridViewRow.Cells[0].Value = DataGridOtherExpenses.Rows.Count;
                    dataGridViewRow.Cells[1].Value = CmbOtherExpHead.Text;
                    dataGridViewRow.Cells[2].Value = CmbType.Text;
                    dataGridViewRow.Cells[3].Value = qty;
                    dataGridViewRow.Cells[4].Value = rate;
                    dataGridViewRow.Cells[5].Value = Value.ToString("0.00");
                    dataGridViewRow.Cells[6].Value = CmbOtherExpHead.SelectedValue;
                    dataGridViewRow.Cells[7].Value = "0";
                    SelectId = 0;
                }
                txtRate.Text = string.Empty;
                txtPercentage.Text = string.Empty;
                CmbType.SelectedIndex = -1;
                CmbOtherExpHead.SelectedIndex = -1;


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void CmbType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (CmbType.Text == "Flat Rate")
                {
                    txtRate.Visible = true;
                    lblRate.Visible = true;
                    lblPercentage.Visible = false;
                    txtPercentage.Visible = false;

                }
                else if (CmbType.Text == "Pcs Rate")
                {
                    txtRate.Visible = true;
                    lblRate.Visible = true;
                    lblPercentage.Visible = false;
                    txtPercentage.Visible = false;
                }
                else
                {
                    lblPercentage.Visible = true;
                    txtPercentage.Visible = true;
                    txtRate.Visible = false;
                    lblRate.Visible = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void BtnHide_Click(object sender, EventArgs e)
        {
            grSearch.Visible = false;
        }

        private void DataGridYarnProcess_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            e.Cancel = true;
        }

        private void DataGridYarnPurchase_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            e.Cancel = true;
        }

        private void DataGridFabricPurchase_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            e.Cancel = true;
        }

        private void DataGridFabricProcess_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            e.Cancel = true;
        }

        private void DataGridTrims_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            e.Cancel = true;
        }

        private void DataGridOtherExpenses_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            e.Cancel = true;
        }

        private void txtDocNo_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bsDocNo.Filter = string.Format("DocNo LIKE '%{0}%'", txtDocNo.Text);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void DataGridTrimsProcess_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {

            }
            catch (Exception)
            {

                throw;
            }
        }

        private void DataGridTrimsProcess_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex == 3 && loadid == 0)
                {
                    decimal qty = Convert.ToDecimal(DataGridTrimsProcess.CurrentRow.Cells[2].Value.ToString());
                    decimal rate = Convert.ToDecimal(DataGridTrimsProcess.CurrentRow.Cells[3].Value.ToString());
                    DataGridTrimsProcess.CurrentRow.Cells[4].Value = (qty * rate).ToString("0.00");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void DataGridGarmentProcess_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex == 4 && loadid == 0)
                {
                    decimal qty = Convert.ToDecimal(DataGridGarmentProcess.CurrentRow.Cells[3].Value.ToString());
                    decimal rate = Convert.ToDecimal(DataGridGarmentProcess.CurrentRow.Cells[4].Value.ToString());
                    DataGridGarmentProcess.CurrentRow.Cells[5].Value = (qty * rate).ToString("0.00");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }



        protected DataTable GetGeneralData(int TypemUid, int Active)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlParameter[] sqlParameters = { new SqlParameter("@TypeMUid", TypemUid), new SqlParameter("@Active", Active) };
                dt = db.GetDataWithParam(CommandType.StoredProcedure, "SP_GetGeneralM", sqlParameters, connection);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            return dt;
        }

        private void CmbOperations_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (dtOrderQty.Rows.Count > 0)
                {
                    txtQty.Text = dtOrderQty.Rows[0]["OrderPlanQty"].ToString();
                    txtCMTRate.Focus();

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void BtnCMTOK_Click(object sender, EventArgs e)
        {
            try
            {
                if (CmbCOORDINATES.SelectedIndex != -1 && CmbOperations.SelectedIndex != -1 && txtCMTRate.Text != string.Empty)
                {
                    int index = DataGridCMT.Rows.Add();
                    DataGridViewRow dataGridViewRow = DataGridCMT.Rows[index];
                    dataGridViewRow.Cells[0].Value = DataGridCMT.Rows.Count;
                    dataGridViewRow.Cells[1].Value = CmbCOORDINATES.Text;
                    dataGridViewRow.Cells[2].Value = CmbOperations.Text;
                    dataGridViewRow.Cells[3].Value = txtQty.Text;
                    dataGridViewRow.Cells[4].Value = txtCMTRate.Text;
                    dataGridViewRow.Cells[5].Value = (Convert.ToDecimal(txtQty.Text) * Convert.ToDecimal(txtCMTRate.Text)).ToString("0.00");
                    dataGridViewRow.Cells[6].Value = CmbCOORDINATES.SelectedValue;
                    dataGridViewRow.Cells[7].Value = CmbOperations.SelectedValue;
                    dataGridViewRow.Cells[8].Value = "0";

                    txtQty.Text = string.Empty;
                    txtCMTRate.Text = string.Empty;
                    CmbOperations.SelectedIndex = -1;
                    CmbOperations.Focus();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void DataGridYarnPurchase_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex == 3 && loadid == 0)
                {
                    decimal qty = Convert.ToDecimal(DataGridYarnPurchase.CurrentRow.Cells[2].Value.ToString());
                    decimal rate = Convert.ToDecimal(DataGridYarnPurchase.CurrentRow.Cells[3].Value.ToString());
                    DataGridYarnPurchase.CurrentRow.Cells[4].Value = (qty * rate).ToString("0.00");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void BtnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                if (SfDataGridPreBudjet.SelectedIndex != -1)
                {
                    var selectedItem = SfDataGridPreBudjet.SelectedItems[0];
                    var dataRow = (selectedItem as DataRowView).Row;
                    decimal CUid = Convert.ToDecimal(dataRow["OrderMUid"].ToString());
                    GeneralParameters.ReportUid = CUid;
                    GeneralParameters.ReportName = "Budget";
                    FrmReportviwer frmReportviwer = new FrmReportviwer
                    {
                        MdiParent = this.MdiParent,
                        StartPosition = FormStartPosition.Manual,
                        Location = new Point(200, 80)
                    };
                    frmReportviwer.Show();
                }
                else
                {
                    MessageBox.Show("Select a row", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void BtnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                if (SfDataGridPreBudjet.SelectedIndex != -1)
                {
                    DialogResult result = MessageBox.Show("Do you want to delete thid budget entry ?", "Information", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2); ;
                    if (result == DialogResult.Yes)
                    {
                        var selectedItem = SfDataGridPreBudjet.SelectedItems[0];
                        var dataRow = (selectedItem as DataRowView).Row;
                        decimal CUid = Convert.ToDecimal(dataRow["UID"].ToString());
                        decimal OrderMUid = Convert.ToDecimal(dataRow["OrdermUid"].ToString());
                        SqlParameter[] sqlParameters = { new SqlParameter("@OrderMuid", OrderMUid), new SqlParameter("@OrderMBudjetUid", CUid) };
                        db.ExecuteNonQuery(CommandType.StoredProcedure, "Proc_DeleteBudget", sqlParameters, connection);
                        MessageBox.Show("Deleted Successfully !", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        LoadFrontGrid();
                    }
                    else
                    {
                        MessageBox.Show("Select a row", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void TabPageYarnPurchase_Click(object sender, EventArgs e)
        {

        }

        private void DataGridOtherExpenses_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Delete)
                {
                    DialogResult dialogResult = MessageBox.Show("Do you want to delete this record ?", "Information", MessageBoxButtons.YesNo, MessageBoxIcon.Information, MessageBoxDefaultButton.Button2);
                    if (dialogResult == DialogResult.Yes)
                    {
                        int Index = DataGridOtherExpenses.SelectedCells[0].RowIndex;
                        decimal Uid = Convert.ToDecimal(DataGridOtherExpenses.Rows[Index].Cells[7].Value.ToString());
                        if (Uid != 0)
                        {
                            string Query = "Delete from OrderMBudjetOtherExp Where Uid =" + Uid + " and OrderMUid =" + txtDocNo.Tag + " ";
                            db.ExecuteNonQuery(CommandType.Text, Query, connection);
                            DataGridOtherExpenses.Rows.RemoveAt(Index);
                            DataGridOtherExpenses.ClearSelection();
                        }
                        else
                        {
                            DataGridOtherExpenses.Rows.RemoveAt(Index);
                            DataGridOtherExpenses.ClearSelection();
                        }
                        MessageBox.Show("Deleted Succssfully !", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void TxtPcrate_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    decimal Qty = 0;
                    decimal AvgRate = 0;
                    decimal GrosValue = 0;
                    decimal ExRate = 0;
                    if (txtPcrate.Text == string.Empty)
                    {
                        AvgRate = 0;
                    }
                    else
                    {
                        AvgRate = Convert.ToDecimal(txtPcrate.Text);
                    }
                    if (txtExrate.Text == string.Empty)
                    {
                        ExRate = 0;
                    }
                    else
                    {
                        ExRate = Convert.ToDecimal(txtExrate.Text);
                    }
                    if (txtExrate.Tag.ToString() == string.Empty)
                    {
                        Qty = 0;
                    }
                    else
                    {
                        Qty = Convert.ToDecimal(txtExrate.Tag);
                    }
                    GrosValue = ((Qty * AvgRate) * ExRate);
                    txtGrossValue.Text = GrosValue.ToString("0.00");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void DataGridCMT_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Delete)
                {
                    DialogResult dialogResult = MessageBox.Show("Do you want to delete this record ?", "Information", MessageBoxButtons.YesNo, MessageBoxIcon.Information, MessageBoxDefaultButton.Button2);
                    if (dialogResult == DialogResult.Yes)
                    {
                        int Index = DataGridCMT.SelectedCells[0].RowIndex;
                        decimal Uid = Convert.ToDecimal(DataGridCMT.Rows[Index].Cells[8].Value.ToString());
                        if (Uid != 0)
                        {
                            string Query = "Delete from OrderMBudjetCMTProcess Where Uid =" + Uid + " and OrderMUid =" + txtDocNo.Tag + " ";
                            db.ExecuteNonQuery(CommandType.Text, Query, connection);
                            DataGridCMT.Rows.RemoveAt(Index);
                            DataGridCMT.ClearSelection();
                        }
                        else
                        {
                            DataGridCMT.Rows.RemoveAt(Index);
                            DataGridCMT.ClearSelection();
                        }
                        MessageBox.Show("Deleted Succssfully !", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void ChckYarnRefresh_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (ChckYarnRefresh.Checked == true)
                {
                    YarnRefresh = 1;
                    SqlParameter[] sqlParameters = { new SqlParameter("@OrderMUid", txtDocNo.Tag) };
                    DataTable dataTable = db.GetDataWithParam(CommandType.StoredProcedure, "Proc_OrderMYarnReqReport", sqlParameters, connection);
                    DataGridYarnPurchase.Rows.Clear();
                    for (int i = 0; i < dataTable.Rows.Count; i++)
                    {
                        int Index = DataGridYarnPurchase.Rows.Add();
                        DataGridViewRow dataGrid = DataGridYarnPurchase.Rows[Index];
                        dataGrid.Cells[0].Value = i + 1;
                        dataGrid.Cells[1].Value = dataTable.Rows[i]["Yarnname"].ToString();
                        if (string.IsNullOrEmpty(dataTable.Rows[i]["PWt"].ToString()))
                        {
                            dataGrid.Cells[2].Value = 0;
                        }
                        else
                        {
                            dataGrid.Cells[2].Value = dataTable.Rows[i]["PWt"].ToString();
                        }

                        dataGrid.Cells[3].Value = "0";
                        dataGrid.Cells[4].Value = "0";
                        dataGrid.Cells[5].Value = dataTable.Rows[i]["YarnId"].ToString();
                        dataGrid.Cells[6].Value = dataTable.Rows[i]["FabricUid"].ToString();
                        dataGrid.Cells[7].Value = "Purchase";
                        dataGrid.Cells[8].Value = "0";
                        dataGrid.Cells[9].Value = false;
                    }
                }
                else
                {
                    YarnRefresh = 0;
                    SqlParameter[] sqlParameters = { new SqlParameter("@OrderMUid", txtDocNo.Tag), new SqlParameter("@OrderMBudjetUid", txtOrderNo.Tag) };
                    DataSet dataSet = db.GetDataWithParamMultiple(CommandType.StoredProcedure, "Proc_OrderMBudjetEdit", sqlParameters, connection);
                    DataTable dtYarn = dataSet.Tables[0];
                    DataGridYarnPurchase.Rows.Clear();
                    DataRow[] dataYarnP = dtYarn.Select("YarnTag = 'Purchase'");
                    if (dataYarnP.Length > 0)
                    {
                        DataTable dtYarnPurchase = dtYarn.Select("YarnTag = 'Purchase'").CopyToDataTable();
                        for (int i = 0; i < dtYarnPurchase.Rows.Count; i++)
                        {
                            int IndexPu = DataGridYarnPurchase.Rows.Add();
                            DataGridViewRow dataGrid = DataGridYarnPurchase.Rows[IndexPu];
                            dataGrid.Cells[0].Value = i + 1;
                            dataGrid.Cells[1].Value = dtYarnPurchase.Rows[i]["Yarnname"].ToString();
                            dataGrid.Cells[2].Value = dtYarnPurchase.Rows[i]["Qty"].ToString();
                            dataGrid.Cells[3].Value = dtYarnPurchase.Rows[i]["Rate"].ToString();
                            dataGrid.Cells[4].Value = dtYarnPurchase.Rows[i]["Total"].ToString();
                            dataGrid.Cells[5].Value = dtYarnPurchase.Rows[i]["YarnUid"].ToString();
                            dataGrid.Cells[6].Value = "0";
                            dataGrid.Cells[7].Value = dtYarnPurchase.Rows[i]["YarnTag"].ToString();
                            bool freoofcost = Convert.ToBoolean(dtYarnPurchase.Rows[i]["FreeOfCost"].ToString());
                            if (freoofcost == true)
                            {
                                dataGrid.Cells[9].Value = true;
                            }
                            else
                            {
                                dataGrid.Cells[9].Value = false;
                            }
                            dataGrid.Cells[8].Value = dtYarnPurchase.Rows[i]["Uid"].ToString(); ;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.StackTrace, ex.Message, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void ChckFabricRefresh_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (ChckFabricRefresh.Checked == true)
                {
                    FabricRefresh = 1;
                    SqlParameter[] parameters1 = { new SqlParameter("@OrderMUid", txtDocNo.Tag) };
                    DataTable dt = db.GetDataWithParam(CommandType.StoredProcedure, "SP_GetOrderMFabricProcess", parameters1, connection);
                    DataGridFabricProcess.Rows.Clear();
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        int Index = DataGridFabricProcess.Rows.Add();
                        DataGridViewRow dataGridViewRow = DataGridFabricProcess.Rows[Index];
                        dataGridViewRow.Cells[0].Value = i + 1;
                        dataGridViewRow.Cells[1].Value = dt.Rows[i]["FabricName"].ToString();
                        dataGridViewRow.Cells[2].Value = dt.Rows[i]["FabricColour"].ToString();
                        dataGridViewRow.Cells[3].Value = dt.Rows[i]["ProcessName"].ToString();
                        dataGridViewRow.Cells[4].Value = dt.Rows[i]["InputWght"].ToString();
                        dataGridViewRow.Cells[5].Value = "0";
                        dataGridViewRow.Cells[6].Value = "0";
                        dataGridViewRow.Cells[7].Value = dt.Rows[i]["OrderMFabricUid"].ToString();
                        dataGridViewRow.Cells[8].Value = "Process";
                        dataGridViewRow.Cells[9].Value = "0";
                    }
                }
                else
                {
                    FabricRefresh = 0;
                    SqlParameter[] sqlParameters = { new SqlParameter("@OrderMUid", txtDocNo.Tag), new SqlParameter("@OrderMBudjetUid", txtOrderNo.Tag) };
                    DataSet dataSet = db.GetDataWithParamMultiple(CommandType.StoredProcedure, "Proc_OrderMBudjetEdit", sqlParameters, connection);
                    DataTable dtFabric = dataSet.Tables[1];
                    DataGridFabricProcess.Rows.Clear();
                    DataRow[] dataFabricPUr = dtFabric.Select("FabTag = 'Purchase'");
                    if (dataFabricPUr.Length > 0)
                    {
                        DataTable dtFabricPurchase = dtFabric.Select("FabTag = 'Purchase'").CopyToDataTable();
                        for (int i = 0; i < dtFabricPurchase.Rows.Count; i++)
                        {
                            int IndexFPu = DataGridFabricProcess.Rows.Add();
                            DataGridViewRow dataGridViewRowP = DataGridFabricProcess.Rows[IndexFPu];
                            dataGridViewRowP.Cells[0].Value = i + 1;
                            dataGridViewRowP.Cells[1].Value = dtFabricPurchase.Rows[i]["FabricName"].ToString();
                            dataGridViewRowP.Cells[2].Value = dtFabricPurchase.Rows[i]["Colour"].ToString();
                            dataGridViewRowP.Cells[3].Value = dtFabricPurchase.Rows[i]["Process"].ToString();
                            dataGridViewRowP.Cells[4].Value = dtFabricPurchase.Rows[i]["Qty"].ToString();
                            dataGridViewRowP.Cells[5].Value = dtFabricPurchase.Rows[i]["Rate"].ToString();
                            dataGridViewRowP.Cells[6].Value = dtFabricPurchase.Rows[i]["Total"].ToString();
                            dataGridViewRowP.Cells[7].Value = dtFabricPurchase.Rows[i]["FabricUid"].ToString();
                            dataGridViewRowP.Cells[8].Value = dtFabricPurchase.Rows[i]["FabTag"].ToString();
                            dataGridViewRowP.Cells[9].Value = dtFabricPurchase.Rows[i]["Uid"].ToString();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.StackTrace, ex.Message, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void ChckTrimsRefresh_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (ChckTrimsRefresh.Checked == true)
                {
                    TrimsReferesh = 1;
                    SqlParameter[] sqlParameters1 = { new SqlParameter("@OrderMUid", txtDocNo.Tag) };
                    DataTable dataTable1 = db.GetDataWithParam(CommandType.StoredProcedure, "Proc_MaterialBomTrimsReport", sqlParameters1, connection);
                    DataGridTrims.Rows.Clear();
                    for (int i = 0; i < dataTable1.Rows.Count; i++)
                    {
                        int Index = DataGridTrims.Rows.Add();
                        DataGridViewRow dataGridViewRow = DataGridTrims.Rows[Index];
                        dataGridViewRow.Cells[0].Value = i + 1;
                        dataGridViewRow.Cells[1].Value = dataTable1.Rows[i]["Item"].ToString();
                        dataGridViewRow.Cells[2].Value = dataTable1.Rows[i]["Qty"].ToString();
                        dataGridViewRow.Cells[3].Value = dataTable1.Rows[i]["Rate"].ToString();
                        dataGridViewRow.Cells[4].Value = dataTable1.Rows[i]["Total"].ToString();
                        dataGridViewRow.Cells[5].Value = dataTable1.Rows[i]["Uid"].ToString();
                        dataGridViewRow.Cells[6].Value = "0";
                    }
                }
                else
                {
                    TrimsReferesh = 0;
                    SqlParameter[] sqlParameters = { new SqlParameter("@OrderMUid", txtDocNo.Tag), new SqlParameter("@OrderMBudjetUid", txtOrderNo.Tag) };
                    DataSet dataSet = db.GetDataWithParamMultiple(CommandType.StoredProcedure, "Proc_OrderMBudjetEdit", sqlParameters, connection);
                    DataTable dtTrims = dataSet.Tables[2];
                    DataGridTrims.Rows.Clear();
                    for (int i = 0; i < dtTrims.Rows.Count; i++)
                    {
                        int IndexTr = DataGridTrims.Rows.Add();
                        DataGridViewRow dataGridViewRowT = DataGridTrims.Rows[IndexTr];
                        dataGridViewRowT.Cells[0].Value = i + 1;
                        dataGridViewRowT.Cells[1].Value = dtTrims.Rows[i]["TrimsName"].ToString();
                        dataGridViewRowT.Cells[2].Value = dtTrims.Rows[i]["Qty"].ToString();
                        dataGridViewRowT.Cells[3].Value = dtTrims.Rows[i]["Rate"].ToString();
                        dataGridViewRowT.Cells[4].Value = dtTrims.Rows[i]["Total"].ToString();
                        dataGridViewRowT.Cells[5].Value = dtTrims.Rows[i]["TrimsUid"].ToString();
                        dataGridViewRowT.Cells[6].Value = dtTrims.Rows[i]["Uid"].ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.StackTrace, ex.Message, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void ChckTrimsProcessRefresh_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (ChckTrimsProcessRefresh.Checked == true)
                {
                    TrimsProcessRefresh = 1;
                    SqlParameter[] sqlParameters12 = { new SqlParameter("@OrderMUid", txtDocNo.Tag) };
                    DataTable dtTrimsProcess = db.GetDataWithParam(CommandType.StoredProcedure, "Proc_OrderMTrimsProcessEdit", sqlParameters12, connection);
                    DataGridTrimsProcess.Rows.Clear();
                    for (int i = 0; i < dtTrimsProcess.Rows.Count; i++)
                    {
                        int Index = DataGridTrimsProcess.Rows.Add();
                        DataGridViewRow dataGridViewRow = DataGridTrimsProcess.Rows[Index];
                        dataGridViewRow.Cells[0].Value = i + 1;
                        dataGridViewRow.Cells[1].Value = dtTrimsProcess.Rows[i]["ItemSpec"].ToString();
                        dataGridViewRow.Cells[2].Value = dtTrimsProcess.Rows[i]["process"].ToString();
                        dataGridViewRow.Cells[3].Value = dtTrimsProcess.Rows[i]["Qty"].ToString();
                        dataGridViewRow.Cells[4].Value = dtTrimsProcess.Rows[i]["Rate"].ToString();
                        dataGridViewRow.Cells[5].Value = Convert.ToDecimal(dtTrimsProcess.Rows[i]["Qty"].ToString()) * Convert.ToDecimal(dtTrimsProcess.Rows[i]["Rate"].ToString());
                        dataGridViewRow.Cells[6].Value = dtTrimsProcess.Rows[i]["TrimsItemUid"].ToString();
                        dataGridViewRow.Cells[7].Value = dtTrimsProcess.Rows[i]["TrimsProcessUid"].ToString();
                        dataGridViewRow.Cells[8].Value = "0";
                    }
                }
                else
                {
                    TrimsProcessRefresh = 0;
                    SqlParameter[] sqlParameters = { new SqlParameter("@OrderMUid", txtDocNo.Tag), new SqlParameter("@OrderMBudjetUid", txtOrderNo.Tag) };
                    DataSet dataSet = db.GetDataWithParamMultiple(CommandType.StoredProcedure, "Proc_OrderMBudjetEdit", sqlParameters, connection);
                    DataGridTrimsProcess.Rows.Clear();
                    DataTable dtTrimsProcess = dataSet.Tables[4];
                    for (int i = 0; i < dtTrimsProcess.Rows.Count; i++)
                    {
                        int Index = DataGridTrimsProcess.Rows.Add();
                        DataGridViewRow dataGridViewRow = DataGridTrimsProcess.Rows[Index];
                        dataGridViewRow.Cells[0].Value = i + 1;
                        dataGridViewRow.Cells[1].Value = dtTrimsProcess.Rows[i]["TrimsName"].ToString();
                        dataGridViewRow.Cells[2].Value = dtTrimsProcess.Rows[i]["Process"].ToString();
                        dataGridViewRow.Cells[3].Value = dtTrimsProcess.Rows[i]["Qty"].ToString();
                        dataGridViewRow.Cells[4].Value = dtTrimsProcess.Rows[i]["rate"].ToString();
                        dataGridViewRow.Cells[5].Value = dtTrimsProcess.Rows[i]["Total"].ToString();
                        dataGridViewRow.Cells[6].Value = dtTrimsProcess.Rows[i]["TrimsUid"].ToString();
                        dataGridViewRow.Cells[7].Value = dtTrimsProcess.Rows[i]["ProcessUid"].ToString();
                        dataGridViewRow.Cells[8].Value = dtTrimsProcess.Rows[i]["Uid"].ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.StackTrace, ex.Message, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void ChckGarmenrRefresh_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (ChckGarmenrRefresh.Checked == true)
                {
                    GarmentProcessRefresh = 1;
                    SqlParameter[] sqlParameters13 = { new SqlParameter("@OrderMUid", txtDocNo.Tag) };
                    DataTable dtGarmentProcess = db.GetDataWithParam(CommandType.StoredProcedure, "Proc_OrderMGarmentProcessEdit", sqlParameters13, connection);
                    DataGridGarmentProcess.Rows.Clear();
                    for (int i = 0; i < dtGarmentProcess.Rows.Count; i++)
                    {
                        int Index = DataGridGarmentProcess.Rows.Add();
                        DataGridViewRow dataGridViewRow = DataGridGarmentProcess.Rows[Index];
                        dataGridViewRow.Cells[0].Value = i + 1;
                        dataGridViewRow.Cells[1].Value = dtGarmentProcess.Rows[i]["Component"].ToString();
                        dataGridViewRow.Cells[2].Value = dtGarmentProcess.Rows[i]["process"].ToString();
                        dataGridViewRow.Cells[3].Value = dtGarmentProcess.Rows[i]["Qty"].ToString();
                        dataGridViewRow.Cells[4].Value = dtGarmentProcess.Rows[i]["Rate"].ToString();
                        dataGridViewRow.Cells[5].Value = Convert.ToDecimal(dtGarmentProcess.Rows[i]["Qty"].ToString()) * Convert.ToDecimal(dtGarmentProcess.Rows[i]["Rate"].ToString());
                        dataGridViewRow.Cells[6].Value = dtGarmentProcess.Rows[i]["ComponentUid"].ToString();
                        dataGridViewRow.Cells[7].Value = dtGarmentProcess.Rows[i]["GarmentProcessUid"].ToString();
                        dataGridViewRow.Cells[8].Value = "0";
                    }
                }
                else
                {
                    GarmentProcessRefresh = 0;
                    SqlParameter[] sqlParameters = { new SqlParameter("@OrderMUid", txtDocNo.Tag), new SqlParameter("@OrderMBudjetUid", txtOrderNo.Tag) };
                    DataSet dataSet = db.GetDataWithParamMultiple(CommandType.StoredProcedure, "Proc_OrderMBudjetEdit", sqlParameters, connection);
                    DataGridGarmentProcess.Rows.Clear();
                    DataTable dtGarmentProcess = dataSet.Tables[5];
                    for (int i = 0; i < dtGarmentProcess.Rows.Count; i++)
                    {
                        int Index = DataGridGarmentProcess.Rows.Add();
                        DataGridViewRow dataGridViewRow = DataGridGarmentProcess.Rows[Index];
                        dataGridViewRow.Cells[0].Value = i + 1;
                        dataGridViewRow.Cells[1].Value = dtGarmentProcess.Rows[i]["Component"].ToString();
                        dataGridViewRow.Cells[2].Value = dtGarmentProcess.Rows[i]["Process"].ToString();
                        dataGridViewRow.Cells[3].Value = dtGarmentProcess.Rows[i]["Qty"].ToString();
                        dataGridViewRow.Cells[4].Value = dtGarmentProcess.Rows[i]["rate"].ToString();
                        dataGridViewRow.Cells[5].Value = dtGarmentProcess.Rows[i]["Total"].ToString();
                        dataGridViewRow.Cells[6].Value = dtGarmentProcess.Rows[i]["ComponentUid"].ToString();
                        dataGridViewRow.Cells[7].Value = dtGarmentProcess.Rows[i]["ProcessUid"].ToString();
                        dataGridViewRow.Cells[8].Value = dtGarmentProcess.Rows[i]["Uid"].ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.StackTrace, ex.Message, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void CmbOtherExpHead_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void SfDataGridPreBudjet_QueryCellStyle(object sender, Syncfusion.WinForms.DataGrid.Events.QueryCellStyleEventArgs e)
        {
            try
            {
                if (e.Column.MappingName == "Status")
                {
                    if (e.DisplayText == "Pending")
                    {
                        e.Style.BackColor = Color.Orange;
                        e.Style.TextColor = Color.Black;
                    }
                    else
                    {
                        e.Style.BackColor = Color.LightGreen;
                        e.Style.TextColor = Color.DarkSlateBlue;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtExrate_TextChanged(object sender, EventArgs e)
        {
            TxtPcrate_TextChanged(sender, e);
        }

        private void DataGridCMT_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex == 4 && loadid == 0)
                {
                    decimal qty = Convert.ToDecimal(DataGridCMT.CurrentRow.Cells[3].Value.ToString());
                    decimal rate = Convert.ToDecimal(DataGridCMT.CurrentRow.Cells[4].Value.ToString());
                    DataGridCMT.CurrentRow.Cells[5].Value = (qty * rate).ToString("0.00");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void DataGridOtherExpenses_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex == 4 && loadid == 0 && SelectId == 0)
                {
                    decimal qty = Convert.ToDecimal(DataGridOtherExpenses.CurrentRow.Cells[3].Value.ToString());
                    decimal rate = Convert.ToDecimal(DataGridOtherExpenses.CurrentRow.Cells[4].Value.ToString());
                    DataGridOtherExpenses.CurrentRow.Cells[5].Value = (qty * rate).ToString("0.00");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void TxtCMTRate_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    BtnCMTOK_Click(sender, e);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void ChckYarnProcessRefresh_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                loadid = 1;
                if (ChckYarnProcessRefresh.Checked == true)
                {
                    SqlParameter[] sqlParameters11 = { new SqlParameter("@OrderMUid", txtDocNo.Tag) };
                    DataTable dtYanrPr = db.GetDataWithParam(CommandType.StoredProcedure, "Proc_GetYarnProcess", sqlParameters11, connection);
                    for (int i = 0; i < dtYanrPr.Rows.Count; i++)
                    {
                        int Index = DataGridYarnProcess.Rows.Add();
                        DataGridViewRow dataGrid = DataGridYarnProcess.Rows[Index];
                        dataGrid.Cells[0].Value = i + 1;
                        dataGrid.Cells[1].Value = dtYanrPr.Rows[i]["YarnName"].ToString();
                        dataGrid.Cells[2].Value = dtYanrPr.Rows[i]["Qty"].ToString();
                        dataGrid.Cells[3].Value = "0";
                        dataGrid.Cells[4].Value = "0";
                        dataGrid.Cells[5].Value = dtYanrPr.Rows[i]["Yarnuid"].ToString();
                        dataGrid.Cells[6].Value = "0";
                        dataGrid.Cells[7].Value = "Process";
                        dataGrid.Cells[8].Value = "0";
                        dataGrid.Cells[9].Value = false;
                    }
                }
                else
                {
                    SqlParameter[] sqlParameters = { new SqlParameter("@OrderMUid", txtDocNo.Tag), new SqlParameter("@OrderMBudjetUid", txtOrderNo.Tag) };
                    DataSet dataSet = db.GetDataWithParamMultiple(CommandType.StoredProcedure, "Proc_OrderMBudjetEdit", sqlParameters, connection);
                    DataTable dtYarn = dataSet.Tables[0];
                    DataGridYarnProcess.Rows.Clear();
                    DataRow[] dataYarn = dtYarn.Select("YarnTag = 'Process'");
                    if (dataYarn.Length > 0)
                    {
                        DataTable dtProcessYarn = dtYarn.Select("YarnTag = 'Process'").CopyToDataTable();
                        for (int i = 0; i < dtProcessYarn.Rows.Count; i++)
                        {
                            int IndexPr = DataGridYarnProcess.Rows.Add();
                            DataGridViewRow dataGrid = DataGridYarnProcess.Rows[IndexPr];
                            dataGrid.Cells[0].Value = i + 1;
                            dataGrid.Cells[1].Value = dtProcessYarn.Rows[i]["Yarnname"].ToString();
                            dataGrid.Cells[2].Value = dtProcessYarn.Rows[i]["Qty"].ToString();
                            dataGrid.Cells[3].Value = dtProcessYarn.Rows[i]["Rate"].ToString();
                            dataGrid.Cells[4].Value = dtProcessYarn.Rows[i]["Total"].ToString();
                            dataGrid.Cells[5].Value = dtProcessYarn.Rows[i]["YarnUid"].ToString();
                            dataGrid.Cells[6].Value = "0";
                            dataGrid.Cells[7].Value = dtProcessYarn.Rows[i]["YarnTag"].ToString();
                            bool freoofcost = Convert.ToBoolean(dtProcessYarn.Rows[i]["FreeOfCost"].ToString());
                            if (freoofcost == true)
                            {
                                dataGrid.Cells[9].Value = true;
                            }
                            else
                            {
                                dataGrid.Cells[9].Value = false;
                            }
                            dataGrid.Cells[8].Value = dtProcessYarn.Rows[i]["Uid"].ToString(); ;
                        }
                    }
                }
                loadid = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void BtnPrintgarment_Click(object sender, EventArgs e)
        {
            try
            {
                if (SfDataGridPreBudjet.SelectedIndex != -1)
                {
                    var selectedItem = SfDataGridPreBudjet.SelectedItems[0];
                    var dataRow = (selectedItem as DataRowView).Row;
                    decimal CUid = Convert.ToDecimal(dataRow["OrderMUid"].ToString());
                    GeneralParameters.ReportUid = CUid;
                    GeneralParameters.ReportName = "Garment PPM";
                    FrmReportviwer frmReportviwer = new FrmReportviwer
                    {
                        MdiParent = this.MdiParent,
                        StartPosition = FormStartPosition.Manual,
                        Location = new Point(200, 80)
                    };
                    frmReportviwer.Show();
                }
                else
                {
                    MessageBox.Show("Select a row", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
    }
}