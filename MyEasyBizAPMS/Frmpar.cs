﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Drawing.Imaging;
using System.Configuration;
using System.Data.SqlClient;
namespace MyEasyBizAPMS
{
    public partial class Frmpar : Form
    {
        public Frmpar()
        {
            InitializeComponent();
        }

        string uid = "";
        int mode = 0;

        SqlConnection conn = new SqlConnection(GeneralParameters.ConnectionString);

        SqlCommand qur = new SqlCommand();
        SQLDBHelper db = new SQLDBHelper();
        BindingSource bs = new BindingSource();

        private void Frmit_Load(object sender, EventArgs e)
        {
            qur.Connection = conn;
            //Left = (MdiParent.ClientRectangle.Width - Width) / 3;
            //Top = (MdiParent.ClientRectangle.Height - Height) / 3;
            Genclass.buttonstyleform(this);
            Genclass.buttonstylepanel(panadd);
            Genclass.buttonstylepanel(fraitem);
            panadd.Visible = true;
            chkact.Checked = true;
            HFGP.RowHeadersVisible = false;
            HFG3.RowHeadersVisible = false;
            this.HFGP.DefaultCellStyle.Font = new Font("Calibri", 10);
            this.HFGP.ColumnHeadersDefaultCellStyle.Font = new Font("Calibri", 10, FontStyle.Regular);
            this.HFG3.DefaultCellStyle.Font = new Font("Calibri", 10);
            this.HFG3.ColumnHeadersDefaultCellStyle.Font = new Font("Calibri", 10, FontStyle.Regular);
            HFGP.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            HFG3.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            HFGP.EnableHeadersVisualStyles = false;
            HFGP.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.GradientActiveCaption;
            HFG3.EnableHeadersVisualStyles = false;
            HFG3.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.GradientActiveCaption;
            fraitem.Visible = false;
            HFGP.Focus();

            cbocr.Items.Clear();
            loadstate();

            label3.Text = "Supplier Master";
            cbocr.Items.Add("Traders");
            cbocr.Items.Add("Processors");
            cbocr.Items.Add("Job Work");
            cbocr.Items.Add("Contractors");
            //cbocr.Items.Add("Supplier");
            cbocr.Items.Add("Service Provider");
            cbocr.Items.Add("Manufacturer");
            cbocr.Text = "Traders";


            LoadGetJobCard(1);


        }
        private void loadstate()
        {

            conn.Open();
            string qur = "select Generalname,guid from Generalm where active=1 and TypeMUid=15 ";
            SqlCommand cmd = new SqlCommand(qur, conn);
            SqlDataAdapter apt = new SqlDataAdapter(cmd);
            DataTable tab = new DataTable();
            apt.Fill(tab);
            cbostate.DataSource = null;
            cbostate.DataSource = tab;
            cbostate.DisplayMember = "Generalname";
            cbostate.ValueMember = "guid";
            //cbostate.SelectedIndex = -1;
            conn.Close();



        }
        protected DataTable LoadGetJobCard(int tag)
        {

            int SP;
            chkact.Checked = true;
            if (chkact.Checked == true)
            {
                SP = 1;

            }
            else
            {
                SP = 0;

            }


            DataTable dt = new DataTable();
            try
            {

                SqlParameter[] para = {
                    new SqlParameter("@active",SP),
                      new SqlParameter("@COMPANYID","1"),


                };
                //if (GeneralParametrs.MenyKey == 6)
                //{
                //    dt = db.GetData(CommandType.StoredProcedure, "SP_GETPARTYCUSTOMER", para);
                //}
                //else if (GeneralParametrs.MenyKey == 7)
                //{
                dt = db.GetData(CommandType.StoredProcedure, "SP_GETPARTYSUPPLIER", para);
                //}
                //else if (GeneralParametrs.MenyKey == 290)
                //{
                //    dt = db.GetData(CommandType.StoredProcedure, "SP_GETPARTYJOB", para);
                //}

                //else if (GeneralParametrs.MenyKey == 8)
                //{
                //    dt = db.GetData(CommandType.StoredProcedure, "SP_GETPARTYMILL", para);
                //}


                LoadDataTable(dt);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        protected void LoadDataTable(DataTable dt)
        {
            try
            {
                HFGP.DataSource = null;
                HFGP.AutoGenerateColumns = false;
                HFGP.ColumnCount = 7;
                HFGP.Columns[0].Name = "Uid";
                HFGP.Columns[0].HeaderText = "Uid";
                HFGP.Columns[0].DataPropertyName = "Uid";

                HFGP.Columns[1].Name = "Name";
                HFGP.Columns[1].HeaderText = "Name";
                HFGP.Columns[1].DataPropertyName = "Name";

                HFGP.Columns[2].Name = "GSTIN";
                HFGP.Columns[2].HeaderText = "GSTIN";
                HFGP.Columns[2].DataPropertyName = "GSTIN";

                HFGP.Columns[3].Name = "Type";
                HFGP.Columns[3].HeaderText = "Type";
                HFGP.Columns[3].DataPropertyName = "Type";

                HFGP.Columns[4].Name = "Phone";
                HFGP.Columns[4].HeaderText = "Phone";
                HFGP.Columns[4].DataPropertyName = "Phone";

                HFGP.Columns[5].Name = "active";
                HFGP.Columns[5].HeaderText = "active";
                HFGP.Columns[5].DataPropertyName = "active";

                HFGP.Columns[6].Name = "tag";
                HFGP.Columns[6].HeaderText = "tag";
                HFGP.Columns[6].DataPropertyName = "tag";


                bs.DataSource = dt;

                HFGP.DataSource = bs;



                HFGP.Columns[0].Visible = false;

                HFGP.Columns[1].Width = 350;
                HFGP.Columns[2].Width = 150;
                HFGP.Columns[3].Width = 150;
                HFGP.Columns[4].Width = 150;
                HFGP.Columns[5].Visible = false;
                HFGP.Columns[6].Visible = false;


            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        private void chkact_CheckedChanged(object sender, EventArgs e)
        {
            LoadGetJobCard(1);
        }

        private void HFGP_CellClick(object sender, DataGridViewCellEventArgs e)
        {


            HFGP.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
        }

        private void txtscr1_TextChanged(object sender, EventArgs e)
        {

            bs.Filter = string.Format("Name Like '%{0}%' or  GSTIN Like '%{1}%' or  type Like '%{1}%' or  phone Like '%{1}%'  ", txtscr1.Text, txtscr1.Text, txtscr1.Text, txtscr1.Text);
        }

        private void Txtscr2_TextChanged(object sender, EventArgs e)
        {

        }

        private void Txtscr3_TextChanged(object sender, EventArgs e)
        {

        }

        private void CmdItem1_Click(object sender, EventArgs e)
        {
            if (mode == 1)
            {
                return;
            }

            else
            {
                fraitem.Visible = true;
                conn.Open();
                string quy = "select a.Uid,c.itemname as Item,isnull(price,0) as price,d.generalname as tax from Partym  A  INNER JOIN pur_price_list b on a.uid=b.suppuid  inner join itemm c on b.itemuid=c.uid   left join generalm d on c.tax=d.uid  where b.eff_to is null   and  a.active=1 and a.companyid=" + GeneralParameters.UserdId + "  and a.uid=" + uid + "";
                Genclass.cmd = new SqlCommand(quy, conn);



                SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
                DataTable tap = new DataTable();
                aptr.Fill(tap);

                this.HFG3.DefaultCellStyle.Font = new Font("Calibri", 10);
                //dgv.ColumnHeadersDefaultCellStyle.Font = new Font(dgv.ColumnHeadersDefaultCellStyle.Font, FontStyle.Bold);
                this.HFG3.ColumnHeadersDefaultCellStyle.Font = new Font("Calibri", 10, FontStyle.Regular);
                HFG3.AutoGenerateColumns = false;
                HFG3.Refresh();
                HFG3.DataSource = null;
                HFG3.Rows.Clear();


                HFG3.ColumnCount = tap.Columns.Count;
                Genclass.i = 0;
                foreach (DataColumn column in tap.Columns)
                {
                    HFG3.Columns[Genclass.i].Name = column.ColumnName;
                    HFG3.Columns[Genclass.i].HeaderText = column.ColumnName;
                    HFG3.Columns[Genclass.i].DataPropertyName = column.ColumnName;
                    Genclass.i = Genclass.i + 1;
                }



                HFG3.Columns[0].Visible = false;

                HFG3.Columns[1].Width = 620;
                HFG3.Columns[2].Width = 80;
                HFG3.Columns[3].Width = 80;






                conn.Close();
                HFG3.DataSource = tap;

            }
        }

        private void txtstate_KeyDown(object sender, KeyEventArgs e)
        {
            //  if (e.KeyCode == Keys.Enter)
            //{
            //    conn.Open();

            //    Genclass.Partylistviewcont("uid", "Generalname", Genclass.strsql, this, Txtstid, txtstate, Editpnl);
            //    Genclass.strsql = "select uid,Generalname as State from Generalm where active=1 and TypeM_Uid=2";
            //    Genclass.FSSQLSortStr = "Generalname";
            //    Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
            //    SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
            //    DataTable tap = new DataTable();
            //    aptr.Fill(tap);
            //    Frmlookup contc = new Frmlookup();
            //    DataGridView dt = (DataGridView)contc.Controls["HFGP"];
            //    dt.Refresh();
            //    dt.ColumnCount = tap.Columns.Count;
            //    dt.Columns[0].Visible = false;
            //    dt.Columns[1].Width = 200;


            //    dt.DefaultCellStyle.Font = new Font("Calibri", 10);

            //    dt.ColumnHeadersDefaultCellStyle.Font = new Font("Calibri", 10, FontStyle.Bold);
            //    dt.AutoGenerateColumns = false;

            //    Genclass.i = 0;
            //    foreach (DataColumn column in tap.Columns)
            //    {
            //        dt.Columns[Genclass.i].Name = column.ColumnName;
            //        dt.Columns[Genclass.i].HeaderText = column.ColumnName;
            //        dt.Columns[Genclass.i].DataPropertyName = column.ColumnName;
            //        Genclass.i = Genclass.i + 1;
            //    }

            //    dt.DataSource = tap;
            //    contc.Show();
            //    conn.Close();
            //}
        }

        private void btnadd_Click(object sender, EventArgs e)
        {
            uid = "";
            mode = 1;
            Genpan.Visible = false;
            fraitem.Visible = false;
            panadd.Visible = false;
            Editpnl.Visible = true;
            chkact.Checked = true;
            Chkedtact.Checked = true;
            Genclass.ClearTextBox(this, Editpnl);
            txtname.Focus();
            checkBox1.Checked = false;
            cbostate.Text = "Tamilnadu";
            cbotype.Text = "Traders";
            if (GeneralParametrs.MenyKey == 6)
            {
                label16.Text = "Customer Master";
                checkBox1.Visible = true;


                txtbank.Enabled = false;
                txtbracnid.Enabled = false;
                txtbranch.Enabled = false;
                txtifsc.Enabled = false;
                txtacno.Enabled = false;



            }
            else if (GeneralParametrs.MenyKey == 7)
            {
                label16.Text = "Supplier Master";
                checkBox1.Visible = false;

                txtbank.Enabled = true;
                txtbracnid.Enabled = true;
                txtbranch.Enabled = true;
                txtifsc.Enabled = true;
                txtacno.Enabled = true;


            }







        }

        private void butedit_Click(object sender, EventArgs e)
        {
            mode = 2;
            panadd.Visible = false;
            fraitem.Visible = false;
            Genpan.Visible = false;
            Editpnl.Visible = true;
            int i = HFGP.SelectedCells[0].RowIndex;
            txtname.Text = HFGP.Rows[i].Cells[1].Value.ToString();
            Txtctper.Text = HFGP.Rows[i].Cells[2].Value.ToString();
            txtphone.Text = HFGP.Rows[i].Cells[4].Value.ToString();
            uid = HFGP.Rows[i].Cells[0].Value.ToString();
            if (HFGP.Rows[i].Cells[5].Value.ToString() == "True")
            {
                Chkedtact.Checked = true;
            }
            else
            {
                Chkedtact.Checked = false;
            }
            int val = Convert.ToInt16(HFGP.Rows[i].Cells[6].Value.ToString());
            if (val == 1)
            {
                checkBox1.Checked = true;
            }
            else
            {
                checkBox1.Checked = false;
            }
            conn.Open();


            Genclass.strsql = "select a.*,b.generalname as state  from supplierm a inner join generalm b on a.stateuid=b.guid   where a.uid=" + uid + "";
            Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
            SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
            DataTable tap = new DataTable();
            aptr.Fill(tap);

            txtcode.Text = tap.Rows[0]["Code"].ToString();
            txtadd1.Text = tap.Rows[0]["address1"].ToString();
            txtadd2.Text = tap.Rows[0]["address2"].ToString();
            txtcity.Text = tap.Rows[0]["City"].ToString();
            cbostate.Text = tap.Rows[0]["state"].ToString();
            Txtstid.Text = tap.Rows[0]["stateuid"].ToString();
            txtpin.Text = tap.Rows[0]["pin"].ToString();
            txtcereg.Text = tap.Rows[0]["CERegno"].ToString();
            txtcrange.Text = tap.Rows[0]["CERange"].ToString();
            txtecc.Text = tap.Rows[0]["ECCNo"].ToString();
            txtdiv.Text = tap.Rows[0]["Division"].ToString();
            txtcoll.Text = tap.Rows[0]["Collectorate"].ToString();
            tngno.Text = tap.Rows[0]["TNGST"].ToString();
            txtcst.Text = tap.Rows[0]["CST"].ToString();
            txtcrdlmt.Text = tap.Rows[0]["CreditLimit"].ToString();
            txtcrddays.Text = tap.Rows[0]["CreditDays"].ToString();
            cbotype.Text = tap.Rows[0]["PartyType"].ToString();
            txtmail.Text = tap.Rows[0]["Emailid"].ToString();
            cbocr.Text = tap.Rows[0]["Type"].ToString();
            txtvendor.Text = tap.Rows[0]["vendor"].ToString();

            txtbank.Enabled = false;
            txtbracnid.Enabled = false;
            txtbranch.Enabled = false;
            txtifsc.Enabled = false;
            txtacno.Enabled = false;





            conn.Close();
        }

        private void butexit_Click(object sender, EventArgs e)
        {
            conn.Open();

            int i = HFGP.SelectedCells[0].RowIndex;

            uid = HFGP.Rows[i].Cells[0].Value.ToString();

            mode = 3;
            qur.CommandText = "";

            int j = HFGP.SelectedCells[0].RowIndex;
            uid = HFGP.Rows[j].Cells[0].Value.ToString();
            string quy = "select * from purchasem where partyuid='" + uid + "'";
            Genclass.cmd = new SqlCommand(quy, conn);

            SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
            DataTable tap = new DataTable();
            aptr.Fill(tap);

            if (tap.Rows.Count > 0)
            {
                MessageBox.Show("Could Not Delete,Transactionsmade");
                conn.Close();
                return;
            }
            else
            {
                string message = "Are You Sure to Delete this Party ?";
                string caption = "Dilama";
                MessageBoxButtons buttons = MessageBoxButtons.YesNo;
                DialogResult result;

                result = MessageBox.Show(message, caption, buttons);

                if (result == System.Windows.Forms.DialogResult.Yes)
                {
                    qur.CommandText = "update partym set active=0  where uid='" + uid + "'";
                    //qur.CommandText = "exec sp_party '" + txtname.Text + "','" + txtcode.Text + "','" + cbotype.Text + "','" + txtadd1.Text + "','" + txtadd2.Text + "','" + txtcity.Text + "'," + Txtstid.Text + ",'" + txtpin.Text + "','" + txtcereg.Text + "','" + txtcrange.Text + "','" + txtecc.Text + "','" + txtdiv.Text + "','" + txtcoll.Text + "','" + tngno.Text + "','" + txtcst.Text + "'," + txtcrddays.Text + "," + txtcrdlmt.Text + ",1,'" + txtphone.Text + "','" + txtmail.Text + "','" + cbocr.Text + "','" + Txtctper.Text + "',0,'" + uid + "'," + mode + "," + Genclass.data1 + "";
                    qur.ExecuteNonQuery();
                }

            }
            conn.Close();
            LoadGetJobCard(1);
            //MessageBox.Show(tap.Rows[0]["Msg"].ToString(), "Save", MessageBoxButtons.OK);
        }

        private void butnsave_Click(object sender, EventArgs e)
        {
            if (txtname.Text == "")
            {
                MessageBox.Show("Enter the Name");
                txtname.Focus();
                return;
            }
            if (cbostate.Text == "")
            {
                MessageBox.Show("Select the State");
                cbostate.Focus();
                return;
            }
            if (cbotype.Text == "")
            {
                MessageBox.Show("Enter the Party Type");
                cbotype.Focus();
                return;
            }

            if (cbocr.Text == "")
            {
                MessageBox.Show("Enter the  Type");
                cbocr.Focus();
                return;
            }
            

            conn.Open();
            //if (mode == 1)
            //{
            //    string quy = "select  Code from partym where Code='" + txtcode.Text + "'";
            //    Genclass.cmd = new SqlCommand(quy, conn);

            //    SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
            //    DataTable tap = new DataTable();
            //    aptr.Fill(tap);
            //    if (tap.Rows.Count > 0)
            //    {
            //        MessageBox.Show("Record Already Exist");
            //        txtcode.Text = "";
            //        txtcode.Focus();
            //        conn.Close();
            //        return;
            //    }

            //    string quy1 = "select  Name from partym where Name='" + txtname.Text + "'";
            //    Genclass.cmd = new SqlCommand(quy1, conn);

            //    SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
            //    DataTable tap1 = new DataTable();
            //    aptr1.Fill(tap1);
            //    if (tap.Rows.Count > 0)
            //    {
            //        MessageBox.Show("Record Already Exist");
            //        txtname.Text = "";
            //        txtname.Focus();
            //        conn.Close();
            //        return;
            //    }

            if (txtbank.Text == "")
            {
                txtbank.Text = "0";
            }
            if (txtbracnid.Text == "")
            {
                txtbracnid.Text = "0";
            }
            if (txtbranch.Text == "")
            {
                txtbranch.Text = "0";
            }
            if (txtifsc.Text == "")
            {
                txtifsc.Text = "0";
            }
            if (txtacno.Text == "")
            {
                txtacno.Text = "0";
            }
            if (txtvendor.Text == "")
            {
                txtvendor.Text = "0";
            }
            if (txtcode.Text == "")
            {
                txtcode.Text = "0";
            }

            if (txtcode.Text == "")
            {
                txtcode.Text = "0";
            }

            if (txtcereg.Text == "")
            {
                txtcereg.Text = "0";
            }
            if (txtcrange.Text == "")
            {
                txtcrange.Text = "0";
            }
            if (txtecc.Text == "")
            {
                txtecc.Text = "0";
            }
            if (checkBox1.Checked == false)
            {
                qur.CommandText = "exec sp_party '" + txtname.Text + "','" + txtcode.Text + "','" + cbotype.Text + "','" + txtadd1.Text + "','" + txtadd2.Text + "','" + txtcity.Text + "'," + cbostate.SelectedValue + ",'" + txtpin.Text + "','" + txtcereg.Text + "','" + txtcrange.Text + "','" + txtecc.Text + "','" + txtdiv.Text + "','" + txtcoll.Text + "','" + tngno.Text + "','" + txtcst.Text + "',0,0," + Chkedtact.Checked + ",'" + txtphone.Text + "','" + txtmail.Text + "','" + cbocr.Text + "','" + Txtctper.Text + "',0,'" + uid + "'," + mode + "," + GeneralParameters.UserdId + ",0,'" + txtvendor.Text + "'," + txtbracnid.Text + ",'" + txtbranch.Text + "','" + txtifsc.Text + "','" + txtacno.Text + "'";
            }
            else
            {
                qur.CommandText = "exec sp_party '" + txtname.Text + "','" + txtcode.Text + "','" + cbotype.Text + "','" + txtadd1.Text + "','" + txtadd2.Text + "','" + txtcity.Text + "'," + cbostate.SelectedValue + ",'" + txtpin.Text + "','" + txtcereg.Text + "','" + txtcrange.Text + "','" + txtecc.Text + "','" + txtdiv.Text + "','" + txtcoll.Text + "','" + tngno.Text + "','" + txtcst.Text + "',0,0," + Chkedtact.Checked + ",'" + txtphone.Text + "','" + txtmail.Text + "','" + cbocr.Text + "','" + Txtctper.Text + "',0,'" + uid + "'," + mode + "," + GeneralParameters.UserdId + ",1,'" + txtvendor.Text + "'," + txtbracnid.Text + ",'" + txtbranch.Text + "','" + txtifsc.Text + "','" + txtacno.Text + "'";
            }


            qur.ExecuteNonQuery();
            MessageBox.Show("Record has been saved", "Save", MessageBoxButtons.OK);




            conn.Close();
            LoadGetJobCard(1);
            Genpan.Visible = true;
            panadd.Visible = true;
            chkact.Checked = true;
        }

        private void btnaddrcan_Click_1(object sender, EventArgs e)
        {

            //fraitem.Visible = false;
            Editpnl.Visible = false;
            Genpan.Visible = true;
            //Genclass.Module.ClearTextBox(this, Editpnl);

            LoadGetJobCard(1);
            panadd.Visible = true;
        }

        private void btnaddrcan_Click(object sender, EventArgs e)
        {
            fraitem.Visible = false;
            Editpnl.Visible = false;
            Genpan.Visible = true;
            //Genclass.Module.ClearTextBox(this, Editpnl);
            panadd.Visible = true;
            chkact.Checked = true;
            //Loadgrid();
        }

        private void chkact_CheckedChanged_1(object sender, EventArgs e)
        {
            LoadGetJobCard(1);
        }





        private void buttnext1_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void buttnext2_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void buttrnxt_Click(object sender, EventArgs e)
        {

            int index = HFGP.SelectedRows[0].Index;
            int index2 = HFGP.SelectedRows[0].Index + 2;
            int index1 = HFGP.Rows.Count - 1; ;
            if (index < HFGP.Rows.Count - 2)
            {
                HFGP.Rows[index].Selected = false;
                HFGP.Rows[index + 1].Selected = true;
                lblno1.Text = index2.ToString();
                lblno2.Text = "of " + index1.ToString();

            }


        }

        private void buttnnxtlft_Click
            (object sender, EventArgs e)
        {

            int index = HFGP.SelectedRows[0].Index;
            int index1 = HFGP.Rows.Count - 1; ;
            if (index > 0)
            {
                HFGP.Rows[index].Selected = false;
                HFGP.Rows[index - 1].Selected = true;
                lblno1.Text = index.ToString();
                lblno2.Text = "of " + index1.ToString();
            }
        }

        private void btnfinnxt_Click(object sender, EventArgs e)
        {
            int index = HFGP.SelectedRows[0].Index;
            int index1 = HFGP.Rows.Count - 1;


            HFGP.Rows[index].Selected = false;

            HFGP.Rows[index1 - 1].Selected = true;
            lblno1.Text = index1.ToString();
            lblno2.Text = "of " + index1.ToString();

        }

        private void buttnnvfst_Click(object sender, EventArgs e)
        {
            int index = HFGP.SelectedRows[0].Index;
            int index1 = 0;
            int index2 = HFGP.Rows.Count - 1;

            HFGP.Rows[index].Selected = false;
            HFGP.Rows[index1].Selected = true;
            index1 = index1 + 1;
            lblno1.Text = index1.ToString();
            lblno2.Text = "of " + index2.ToString();
        }

        private void HFGP_CellMouseUp(object sender, DataGridViewCellMouseEventArgs e)
        {

        }

        private void HFGP_KeyUp(object sender, KeyEventArgs e)
        {
            if (HFGP.Rows[0].Cells[0].Value != "")
            {
                int index = HFGP.SelectedRows[0].Index;
                if (index < HFGP.Rows.Count - 1)
                {
                    index = index + 1;
                    int index1 = HFGP.Rows.Count - 1;
                    lblno1.Text = index.ToString();
                    lblno2.Text = "of " + index1.ToString();
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {

            fraitem.Visible = false;
        }

        private void cbocr_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void label25_Click(object sender, EventArgs e)
        {

        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtbank_Click(object sender, EventArgs e)
        {
            Genclass.Dtype = 1;
            loadput();
        }

        private void loadput()
        {
            //conn.Open();

            //Genclass.fieldone = "";
            //Genclass.fieldtwo = "";
            //Genclass.fieldthree = "";
            //Genclass.fieldFour = "";
            //Genclass.fieldFive = "";

            //if (Genclass.Dtype == 1)
            //{
            //    Genclass.Partylistviewcont("uid", "bankname", Genclass.strsql, this, txtbracnid, txtbank, Editpnl);
            //    //Genclass.strsql = "select uid,Name as Party from Partym where active=1 and  companyid=" + Genclass.data1 + "";
            //    //Genclass.strsql = " select distinct c.Uid as puid,c.name   from stransactionsp a inner join  stransactionsplist b   on  a.uid=b.transactionspuid and a.doctypeid=80 left join transactionsplist f on b.uid=f.refuid   and f.DocTypeID=110  left join  stransactionsplist g on f.Uid=g.Refuid and g.DocTypeID=40    inner join  partym c  on a.partyuid=c.uid group by a.uid,docno,b.pqty,c.Uid,c.name      having isnull(b.pqty,0)-ISNULL(SUM(f.pqty),0)-ISNULL(sum(g.pqty),0)>0";
            //    Genclass.strsql = "select uid,bankname from bank   order by bankname";
            //    Genclass.FSSQLSortStr = "bankname";
            //}



            //Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
            //SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
            //DataTable tap = new DataTable();
            //aptr.Fill(tap);
            //Frmlookup contc = new Frmlookup();
            //DataGridView dt = (DataGridView)contc.Controls["HFGP"];
            //dt.Refresh();
            //dt.ColumnCount = tap.Columns.Count;
            //dt.Columns[0].Visible = false;
            //dt.Columns[1].Width = 400;

            //if (Genclass.type == 2 || Genclass.type == 3 || Genclass.type == 11)
            //{
            //    dt.Columns[2].Width = 100;
            //    dt.Columns[3].Visible = false;
            //}
            ////else if(Genclass.type == 4)
            ////{
            ////    dt.Columns[2].Visible = false;
            ////}

            //dt.DefaultCellStyle.Font = new Font("Calibri", 10);

            //dt.ColumnHeadersDefaultCellStyle.Font = new Font("Calibri", 10, FontStyle.Bold);
            //dt.AutoGenerateColumns = false;

            //Genclass.i = 0;
            //foreach (DataColumn column in tap.Columns)
            //{
            //    dt.Columns[Genclass.i].Name = column.ColumnName;
            //    dt.Columns[Genclass.i].HeaderText = column.ColumnName;
            //    dt.Columns[Genclass.i].DataPropertyName = column.ColumnName;
            //    Genclass.i = Genclass.i + 1;
            //}

            //dt.DataSource = tap;
            //contc.Show();
            //conn.Close();


        }

        private void txtstate_TextChanged(object sender, EventArgs e)
        {

        }

        private void Txtscr4_TextChanged(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            fraitem.Visible = false;
        }

        private void Editpnl_Paint(object sender, PaintEventArgs e)
        {

        }

        private void txtname_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                e.Handled = true;
                SendKeys.Send("{TAB}");
            }
        }

        private void txtcode_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                e.Handled = true;
                SendKeys.Send("{TAB}");
            }
        }

        private void txtphone_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                e.Handled = true;
                SendKeys.Send("{TAB}");
            }

        }

        private void Txtctper_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                e.Handled = true;
                SendKeys.Send("{TAB}");
            }
        }

        private void txtcoll_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                e.Handled = true;
                SendKeys.Send("{TAB}");
            }
        }

        private void txtadd1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                e.Handled = true;
                SendKeys.Send("{TAB}");
            }
        }

        private void txtadd2_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                e.Handled = true;
                SendKeys.Send("{TAB}");
            }
        }

        private void txtcity_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                e.Handled = true;
                SendKeys.Send("{TAB}");
            }
        }

        private void txtpin_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                e.Handled = true;
                SendKeys.Send("{TAB}");
            }
        }

        private void txtmail_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                e.Handled = true;
                SendKeys.Send("{TAB}");
            }
        }

        private void tngno_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                e.Handled = true;
                SendKeys.Send("{TAB}");
            }
        }

        private void txtecc_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                e.Handled = true;
                SendKeys.Send("{TAB}");
            }
        }

        private void txtdiv_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                e.Handled = true;
                SendKeys.Send("{TAB}");
            }
        }

        private void txtcereg_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                e.Handled = true;
                SendKeys.Send("{TAB}");
            }
        }

        private void txtcrange_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                e.Handled = true;
                SendKeys.Send("{TAB}");
            }
        }

        private void txtcrddays_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                e.Handled = true;
                SendKeys.Send("{TAB}");
            }
        }

        private void txtcrdlmt_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                e.Handled = true;
                SendKeys.Send("{TAB}");
            }
        }

        private void cbotype_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                e.Handled = true;
                SendKeys.Send("{TAB}");
            }
        }
    }


}
