﻿using System;
using System.Drawing;
using System.Data;
using System.Windows.Forms;

namespace MyEasyBizAPMS
{
    public partial class CBox : UserControl
    {
        public CBox()
        {
            InitializeComponent();
        }
        BindingSource bindingSource = new BindingSource();
        int id = 0;
        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            if(id == 1)
            {
                DataGridLookup.Visible = true;
                string FilteName = DataGridLookup.Columns[1].Name;
                bindingSource.Filter = string.Format("" + FilteName + " Like '%{0}%'", textBox1.Text);
            }
        }

        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.AutoSize = false;
                this.Height = 30;
                this.Width = 250;
            }
            else if(e.KeyCode == Keys.Down)
            {
                DataGridLookup.Focus();
            }
            
        }

        public void LoadGrid(DataTable dataTable)
        {
            try
            {
                DataGridLookup.DataSource = null;
                bindingSource.DataSource = dataTable;
                DataGridLookup.DataSource = bindingSource;
                DataGridLookup.Columns[0].Visible = false;
                DataGridLookup.Columns[1].Width = 200;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void DataGridLookup_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                id = 0;
                int Index = DataGridLookup.SelectedCells[0].RowIndex;
                textBox1.Text = DataGridLookup.Rows[Index].Cells[1].Value.ToString();
                textBox1.Tag = DataGridLookup.Rows[Index].Cells[0].Value.ToString();
                this.Tag = textBox1.Tag;
                Text = textBox1.Text;
                this.AutoSize = false;
                this.Height = 30;
                this.Width = 250;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void CBox_Load(object sender, EventArgs e)
        {

        }
        protected override void OnLoad(EventArgs e)
        {
            var btn = new Button();
            btn.Size = new Size(25, textBox1.ClientSize.Height + 2);
            btn.Location = new Point(textBox1.ClientSize.Width - btn.Width, -1);
            btn.Cursor = Cursors.Default;
            btn.Image = Properties.Resources.down_arrow1;
            btn.BackgroundImageLayout = ImageLayout.Stretch;
            btn.FlatStyle = FlatStyle.Standard;
            btn.FlatAppearance.BorderColor = Color.FromArgb(0, 255, 255, 255);
            btn.Click += Btn_Click;
            textBox1.Controls.Add(btn);
            // Send EM_SETMARGINS to prevent text from disappearing underneath the button
            SendMessage(textBox1.Handle, 0xd3, (IntPtr)2, (IntPtr)(btn.Width << 16));
            base.OnLoad(e);
        }

        private void Btn_Click(object sender, EventArgs e)
        {
            this.Height = 230;
            this.Width = 250;
            DataGridLookup.Visible = true;
            textBox1.Focus();
            id = 1;
        }

        [System.Runtime.InteropServices.DllImport("user32.dll")]
        private static extern IntPtr SendMessage(IntPtr hWnd, int msg, IntPtr wp, IntPtr lp);

        private void DataGridLookup_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                int Index = DataGridLookup.SelectedCells[0].RowIndex;
                textBox1.Text = DataGridLookup.Rows[Index].Cells[1].Value.ToString();
                textBox1.Tag = DataGridLookup.Rows[Index].Cells[0].Value.ToString();
                this.Tag = textBox1.Tag;
                Text = textBox1.Text;
                this.AutoSize = false;
                this.Height = 30;
                this.Width = 250;
            }
        }

        private void textBox1_Click(object sender, EventArgs e)
        {
            this.Height = 250;
            this.Width = 250;
            DataGridLookup.Visible = true;
            textBox1.Focus();
            id = 1;
        }
    }

}
