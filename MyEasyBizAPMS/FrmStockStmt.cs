﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Drawing.Imaging;
using System.Configuration;
using System.Data.SqlClient;
using System.Diagnostics;
using excel = Microsoft.Office.Interop.Excel;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;

namespace MyEasyBizAPMS
{
    public partial class FrmStockStmt : Form
    {
        private CrystalDecisions.Windows.Forms.CrystalReportViewer Cryview;

        ReportDocument doc = new ReportDocument();
        public FrmStockStmt()
        {
            InitializeComponent();
        }
        string uid = "";
        int mode = 0;
        string tpuid = "";
        SqlConnection conn = new SqlConnection(GeneralParameters.ConnectionString);
        SqlCommand qur = new SqlCommand();
        private DataRow doc1;
        DataTable Docno = new DataTable();

        BindingSource bsc = new BindingSource();
        BindingSource bsp = new BindingSource();
        BindingSource bsserial = new BindingSource();
        BindingSource bsc1 = new BindingSource();
        BindingSource bsc2 = new BindingSource();

        SQLDBHelper db = new SQLDBHelper();
        int Fillid;
        BindingSource bsParty = new BindingSource();
        string str1key;
        public int SelectId = 0;
        private void button1_Click(object sender, EventArgs e)
        {
            if (cbotype.Text == "Supplier Stock Statement" && cbogrp.Text != "FABRIC")
            {
                
                
                    if (checkBox1.Checked == true)
                    {
                        Genclass.Dtype = 121;
                        textBox1.Text = dtpfrom.Text;
                        textBox2.Text = dtpto.Text;
                        Genclass.rrtt = textBox1.Text;
                        Genclass.err = textBox2.Text;
                        Genclass.itemgrpid = Convert.ToInt16(cbogrp.SelectedValue);
                    }
                    else
                    {

                        Genclass.Dtype = 120;
                        textBox1.Text = dtpfrom.Text;
                        textBox2.Text = dtpto.Text;
                        Genclass.rrtt = textBox1.Text;
                        Genclass.err = textBox2.Text;
                        Genclass.itemgrpid = Convert.ToInt16(txtname.Tag);
                        Genclass.itemgrid = Convert.ToInt16(cbogrp.SelectedValue);
                    }
                }
            else if (cbotype.Text == "Supplier Stock Statement" && cbogrp.Text == "FABRIC")
            {
                if (checkBox1.Checked == true)
                {
                    Genclass.Dtype = 140;
                    textBox1.Text = dtpfrom.Text;
                    textBox2.Text = dtpto.Text;
                    Genclass.rrtt = textBox1.Text;
                    Genclass.err = textBox2.Text;
                    Genclass.itemgrid = Convert.ToInt16(cbogrp.SelectedValue);
                }
                else
                {

                    Genclass.Dtype = 141;
                    textBox1.Text = dtpfrom.Text;
                    textBox2.Text = dtpto.Text;
                    Genclass.rrtt = textBox1.Text;
                    Genclass.err = textBox2.Text;
                    Genclass.itemgrpid = Convert.ToInt16(txtname.Tag);
                    Genclass.itemgrid = Convert.ToInt16(cbogrp.SelectedValue);
                }
            }

            else  if (cbotype.Text == "Issue Receipt Matching Register")
            {
                if (checkBox1.Checked == true)
                {
                    Genclass.Dtype = 103;

                    textBox1.Text = dtpfrom.Text;
                    textBox2.Text = dtpto.Text;
                    Genclass.rrtt = textBox1.Text;
                    Genclass.err = textBox2.Text;
                }
                else
                {

                    Genclass.Dtype = 104;
                    textBox1.Text = dtpfrom.Text;
                    textBox2.Text = dtpto.Text;
                    Genclass.rrtt = textBox1.Text;
                    Genclass.err = textBox2.Text;
                    Genclass.itemgrpid = Convert.ToInt16(txtname.Tag);
                }

            }
            else if (cbotype.Text == "Issue Receipt Matching ProcessWise Register")
            {
                if (checkBox1.Checked == true)
                {
                    Genclass.Dtype = 115;

                    textBox1.Text = dtpfrom.Text;
                    textBox2.Text = dtpto.Text;
                    Genclass.rrtt = textBox1.Text;
                    Genclass.err = textBox2.Text;
                }
                else
                {

                    Genclass.Dtype = 116;
                    textBox1.Text = dtpfrom.Text;
                    textBox2.Text = dtpto.Text;
                    Genclass.rrtt = textBox1.Text;
                    Genclass.err = textBox2.Text;
                }

            }


            
            else if (cbotype.Text == "Purchase Status Register")
            {
                if (checkBox1.Checked == true)
                {
                    Genclass.Dtype = 115;

                    textBox1.Text = dtpfrom.Text;
                    textBox2.Text = dtpto.Text;
                    Genclass.rrtt = textBox1.Text;
                    Genclass.err = textBox2.Text;
                    Genclass.itemgrpid = Convert.ToInt16(txtname.Tag);
                }
                else
                {

                    Genclass.Dtype = 114;
                    textBox1.Text = dtpfrom.Text;
                    textBox2.Text = dtpto.Text;
                    Genclass.rrtt = textBox1.Text;
                    Genclass.err = textBox2.Text;
                    Genclass.itemgrpid = Convert.ToInt16(txtname.Tag);
                }

            }
            else if (cbotype.Text == "Purchase Register" && comboBox1.Text=="Yarn")
            {
                if (checkBox1.Checked == true)
                {
                    Genclass.Dtype = 107;

                    textBox1.Text = dtpfrom.Text;
                    textBox2.Text = dtpto.Text;
                    Genclass.rrtt = textBox1.Text;
                    Genclass.err = textBox2.Text;
                    Genclass.address = comboBox1.Text;
                    //Genclass.itemgrpid = Convert.ToInt16(txtname.Tag);
                }
                else
                {

                    Genclass.Dtype = 106;
                    textBox1.Text = dtpfrom.Text;
                    textBox2.Text = dtpto.Text;
                    Genclass.rrtt = textBox1.Text;
                    Genclass.err = textBox2.Text;
                    Genclass.address = comboBox1.Text;
                    Genclass.itemgrpid = Convert.ToInt16(txtname.Tag);
                }

            }
            else if (cbotype.Text == "Purchase Register" && comboBox1.Text == "Trims")
            {
                if (checkBox1.Checked == true)
                {
                    Genclass.Dtype = 107;

                    textBox1.Text = dtpfrom.Text;
                    textBox2.Text = dtpto.Text;
                    Genclass.rrtt = textBox1.Text;
                    Genclass.err = textBox2.Text;
                    Genclass.address = comboBox1.Text;
                    //Genclass.itemgrpid = Convert.ToInt16(txtname.Tag);
                }
                else
                {

                    Genclass.Dtype = 106;
                    textBox1.Text = dtpfrom.Text;
                    textBox2.Text = dtpto.Text;
                    Genclass.rrtt = textBox1.Text;
                    Genclass.err = textBox2.Text;
                    Genclass.address = comboBox1.Text;
                    Genclass.itemgrpid = Convert.ToInt16(txtname.Tag);
                }

            }
            else if (cbotype.Text == "GRN Register" && comboBox1.Text == "Yarn")
            {
                if (checkBox1.Checked == true)
                {
                    Genclass.Dtype = 109;

                    textBox1.Text = dtpfrom.Text;
                    textBox2.Text = dtpto.Text;
                    Genclass.rrtt = textBox1.Text;
                    Genclass.err = textBox2.Text;
                    Genclass.address = comboBox1.Text;
                    //Genclass.itemgrpid = Convert.ToInt16(txtname.Tag);
                }
                else
                {

                    Genclass.Dtype = 108;
                    textBox1.Text = dtpfrom.Text;
                    textBox2.Text = dtpto.Text;
                    Genclass.rrtt = textBox1.Text;
                    Genclass.err = textBox2.Text;
                    Genclass.address = comboBox1.Text;
                    Genclass.itemgrpid = Convert.ToInt16(txtname.Tag);
                }

            }
            else if (cbotype.Text == "GRN Register" && comboBox1.Text == "Trims")
            {
                if (checkBox1.Checked == true)
                {
                    Genclass.Dtype = 109;

                    textBox1.Text = dtpfrom.Text;
                    textBox2.Text = dtpto.Text;
                    Genclass.rrtt = textBox1.Text;
                    Genclass.err = textBox2.Text;
                    Genclass.address = comboBox1.Text;
                    //Genclass.itemgrpid = Convert.ToInt16(txtname.Tag);
                }
                else
                {

                    Genclass.Dtype = 108;
                    textBox1.Text = dtpfrom.Text;
                    textBox2.Text = dtpto.Text;
                    Genclass.rrtt = textBox1.Text;
                    Genclass.err = textBox2.Text;
                    Genclass.address = comboBox1.Text;
                    Genclass.itemgrpid = Convert.ToInt16(txtname.Tag);
                }

            }
            else if (cbotype.Text == "Issue Register")
            {
                if (checkBox1.Checked == true)
                {
                    Genclass.Dtype = 111;

                    textBox1.Text = dtpfrom.Text;
                    textBox2.Text = dtpto.Text;
                    Genclass.rrtt = textBox1.Text;
                    Genclass.err = textBox2.Text;
                    Genclass.itemgrpid = Convert.ToInt16(txtname.Tag);
                }
                else
                {

                    Genclass.Dtype = 110;
                    textBox1.Text = dtpfrom.Text;
                    textBox2.Text = dtpto.Text;
                    Genclass.rrtt = textBox1.Text;
                    Genclass.err = textBox2.Text;
                    Genclass.itemgrpid = Convert.ToInt16(txtname.Tag);
                }

            }
            else if (cbotype.Text == "General Stock Statement")
            {

                Genclass.Dtype = 105;
                textBox1.Text = dtpfrom.Text;
                textBox2.Text = dtpto.Text;
                Genclass.rrtt = textBox1.Text;
                Genclass.err = textBox2.Text;
                Genclass.itemgrpid = Convert.ToInt16(cbogrp.SelectedValue);

            }
            else if (cbotype.Text == "Receipt Register")
            {
                if (checkBox1.Checked == true)
                {
                    Genclass.Dtype = 113;

                    textBox1.Text = dtpfrom.Text;
                    textBox2.Text = dtpto.Text;
                    Genclass.rrtt = textBox1.Text;
                    Genclass.err = textBox2.Text;
                    Genclass.itemgrpid = Convert.ToInt16(txtname.Tag);
                }
                else
                {

                    Genclass.Dtype = 112;
                    textBox1.Text = dtpfrom.Text;
                    textBox2.Text = dtpto.Text;
                    Genclass.rrtt = textBox1.Text;
                    Genclass.err = textBox2.Text;
                    Genclass.itemgrpid = Convert.ToInt16(txtname.Tag);
                }

            }
        
            conn.Close();
            conn.Open();
            Genclass.Prtid = Convert.ToInt16(txtname.Tag);


            Genclass.slno = 1;
            Crviewer crv = new Crviewer();
            crv.Show();



            conn.Close();
        }

        private void txtname_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bsp.Filter = string.Format("Name LIKE '%{0}%' ", txtname.Text);



                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtname_Click(object sender, EventArgs e)
        {
            DataTable dt = getParty();
            bsp.DataSource = dt;
            FillGrid2(dt, 1);
            Point loc = FindLocation(txtname);
            //grSearch.Location = new Point(500, 32);
            grSearch.Visible = true;
            grSearch.Text = "Name Search";
        }
        private Point FindLocation(Control ctrl)
        {
            if (ctrl.Parent is Form)
                return ctrl.Location;
            else
            {
                Point p = FindLocation(ctrl.Parent);
                p.X += ctrl.Location.X;
                p.Y += ctrl.Location.Y;
                return p;
            }
        }
        protected void FillGrid2(DataTable dt, int FillId)
        {
            try
            {
                DataGridCommon.DataSource = null;
                DataGridCommon.AutoGenerateColumns = false;


                DataGridCommon.ColumnCount = 2;
                DataGridCommon.Columns[0].Name = "Uid";
                DataGridCommon.Columns[0].HeaderText = "Uid";
                DataGridCommon.Columns[0].DataPropertyName = "Uid";

                DataGridCommon.Columns[1].Name = "Name";
                DataGridCommon.Columns[1].HeaderText = "Name";
                DataGridCommon.Columns[1].DataPropertyName = "Name";
                DataGridCommon.Columns[1].Width = 300;





                DataGridCommon.DataSource = bsp;
                DataGridCommon.Columns[0].Visible = false;



            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        protected DataTable getParty()
        {
            DataTable dt = new DataTable();
            try
            {
             
                    dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "SP_GETPARTYSUPPLIER1", conn);
                    bsp.DataSource = dt;
              

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            return dt;
        }
        private void txtname_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.F10)
                {
                    grSearch.Visible = false;
                }
                else if (e.KeyCode == Keys.F2)
                {
                    SelectId = 1;
                    int Index = DataGridCommon.SelectedCells[0].RowIndex;

                    txtname.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtname.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();

          
                    grSearch.Visible = false;
                    SelectId = 0;
                }
                else if (e.KeyCode == Keys.Escape)
                {
                    grSearch.Visible = false;
                }
                else if (e.KeyValue == 40)
                {
                    DataGridCommon.Select();
                }
                else if (e.KeyCode == Keys.Enter)
                {
                    txtname_Click(sender, e);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void button18_Click(object sender, EventArgs e)
        {
            try
            {
                SelectId = 1;
                int Index = DataGridCommon.SelectedCells[0].RowIndex;
               
                    txtname.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                txtname.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
               

               
                grSearch.Visible = false;
                SelectId = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnHide_Click(object sender, EventArgs e)
        {

        }

        private void DataGridCommon_DoubleClick(object sender, EventArgs e)
        {
            SelectId = 1;
            int Index = DataGridCommon.SelectedCells[0].RowIndex;

            txtname.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
            txtname.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
         


            grSearch.Visible = false;
            SelectId = 0;
        }

        private void DataGridCommon_KeyDown(object sender, KeyEventArgs e)
        {

            if (e.KeyCode == Keys.Enter)
            {
                SelectId = 1;
                int Index = DataGridCommon.SelectedCells[0].RowIndex;

                
                txtname.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                txtname.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                 
              
                grSearch.Visible = false;
                SelectId = 0;
            }
        }

        private void cbotype_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbotype.Text == "Supplier Stock Statement")
            {
                label3.Visible = false;
                txtname.Visible = false;
                checkBox1.Visible = false;

                dtpfrom.Visible = true;
                dtpto.Visible = true;
                label1.Visible = true;
                label6.Visible = true;
                label2.Visible = true;
                cbogrp.Visible = true;
                label3.Visible = true;
                txtname.Visible = true;
                checkBox1.Visible = true;
            }

            else if (cbotype.Text == "Issue Receipt Matching Register")



            {
                dtpfrom.Visible = true;
                dtpto.Visible = true;
                label1.Visible = true;
                label6.Visible = true;
                label2.Visible = false;
                cbogrp.Visible = false;
                label3.Visible = true;
                txtname.Visible = true;
                checkBox1.Visible = true;

            }

            else if (cbotype.Text == "Issue Receipt Matching ProcessWise Register")



            {
                dtpfrom.Visible = true;
                dtpto.Visible = true;
                label1.Visible = true;
                label6.Visible = true;
                label2.Visible = false;
                cbogrp.Visible = false;
                label3.Visible = true;
                txtname.Visible = true;
                checkBox1.Visible = true;

            }
            else if (cbotype.Text == "General Stock Statement")

            {
                label3.Visible = false;
                txtname.Visible = false;
                checkBox1.Visible = false;

                dtpfrom.Visible = true;
                dtpto.Visible = true;
                label1.Visible = true;
                label6.Visible = true;
                label2.Visible = true;
                cbogrp.Visible = true;
              
            }
            else if (cbotype.Text == "Purchase Register")

            {
                dtpfrom.Visible = true;
                dtpto.Visible = true;
                label1.Visible = true;
                label6.Visible = true;
                label2.Visible = false;
                cbogrp.Visible = false;
                label3.Visible = true;
                txtname.Visible = true;
                checkBox1.Visible = true;
            }
            else if (cbotype.Text == "Purchase Status Register")

            {
                dtpfrom.Visible = true;
                dtpto.Visible = true;
                label1.Visible = true;
                label6.Visible = true;
                label2.Visible = false;
                cbogrp.Visible = false;
                label3.Visible = true;
                txtname.Visible = true;
                checkBox1.Visible = true;
            }
            else if (cbotype.Text == "GRN Register")

            {
                dtpfrom.Visible = true;
                dtpto.Visible = true;
                label1.Visible = true;
                label6.Visible = true;
                label2.Visible = false;
                cbogrp.Visible = false;
                label3.Visible = true;
                txtname.Visible = true;
                checkBox1.Visible = true;
            }
            else if (cbotype.Text == "Job Order Issue Register")

            {
                dtpfrom.Visible = true;
                dtpto.Visible = true;
                label1.Visible = true;
                label6.Visible = true;
                label2.Visible = false;
                cbogrp.Visible = false;
                label3.Visible = true;
                txtname.Visible = true;
                checkBox1.Visible = true;
            }
            else if (cbotype.Text == "Job Order Receipt Register")

            {
                dtpfrom.Visible = true;
                dtpto.Visible = true;
                label1.Visible = true;
                label6.Visible = true;
                label2.Visible = false;
                cbogrp.Visible = false;
                label3.Visible = true;
                txtname.Visible = true;
                checkBox1.Visible = true;
            }
        }
    

        private void FrmStockStmt_Load(object sender, EventArgs e)
        {


            dtpfrom.Format = DateTimePickerFormat.Custom;
            dtpfrom.CustomFormat = "dd/MM/yyyy";

            dtpto.Format = DateTimePickerFormat.Custom;
            dtpto.CustomFormat = "dd/MM/yyyy";

            //Category();
            label2.Visible = false;
            cbogrp.Visible = false;

            if (Text=="Purchase")
            {
           
                cbotype.Items.Add("Purchase Register");
                cbotype.Items.Add("GRN Register");
                //cbotype.Items.Add("Purchase Status Register");

            }
            else if (GeneralParametrs.MenyKey == 91)
            {

                cbotype.Items.Add("General Stock Statement");
                cbotype.Items.Add("Supplier Stock Statement");


            }
            else if (Text == "Process Register")
            {

          
                cbotype.Items.Add("Issue Receipt Matching Register");
                cbotype.Items.Add("Issue Receipt Matching ProcessWise Register");
    


            }
        }
        public void Category()
        {
            conn.Open();
            string qur = "select * from itemgroup";
            SqlCommand cmd = new SqlCommand(qur, conn);
            SqlDataAdapter apt = new SqlDataAdapter(cmd);
            DataTable tab = new DataTable();
            apt.Fill(tab);
            cbogrp.DataSource = null;
            cbogrp.DataSource = tab;
            cbogrp.DisplayMember = "itemgroup";
            cbogrp.ValueMember = "uid";
            cbogrp.SelectedIndex = -1;
            conn.Close();
        }
        private void btnexit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void cbogrp_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
