﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data;
using Syncfusion.WinForms.DataGrid;
using Syncfusion.WinForms.DataGrid.Events;
using System.Collections.Generic;

namespace MyEasyBizAPMS
{
    public partial class FrmParty : Form
    {
        public FrmParty()
        {
            CenterToParent();
            this.BackColor = Color.White;
            InitializeComponent();
            this.tabControlParty.TabStyle = typeof(Syncfusion.Windows.Forms.Tools.OneNoteStyleRenderer);
            this.SplitAdd.ButtonMode = Syncfusion.Windows.Forms.Tools.ButtonMode.Normal;
            //this.SfDataGridPartyM.Style.CellStyle.BackColor = Color.LightSteelBlue;
            this.SfDataGridPartyM.Style.HeaderStyle.Font.FontStyle = FontStyle.Bold;
            this.SfDataGridPartyM.Style.HeaderStyle.BackColor = Color.DeepSkyBlue;
        }
        new int Tag = 0;
        SQLDBHelper db = new SQLDBHelper();
        SqlConnection conn = new SqlConnection(GeneralParameters.ConnectionString);
        SqlCommand qur = new SqlCommand();
        RowAutoFitOptions autoFitOptions = new RowAutoFitOptions();
        List<string> excludeColumns = new List<string>();

        private void Frmparty_Load(object sender, EventArgs e)
        {
            grFront.Visible = true;
            grBack.Visible = false;
            ClearTextBoxes();
            LoadPaymode();
            LoadCountry();
            if (this.Text == "Vendor Master")
            {
                Tag = 1;
                lblPartyType.Text = "Party Type";
                CmbType.Items.Clear();
                CmbType.Items.Add("Purchase Supplier");
                CmbType.Items.Add("Production Supplier");
                CmbType.Items.Add("Processor");
                CmbType.Items.Add("Service Provide");
                CmbCountry.SelectedIndex = 0;
                lblPartyType.Visible = true;
                lblCPartyType.Visible = true;
                CmbPartyType.Visible = true;
                CmbType.Visible = true;
                lblCtype.Visible = true;
                lblPType.Visible = true;
            }
            else
            {
                Tag = 0;
                lblPartyType.Visible = false;
                lblCPartyType.Visible = false;
                CmbPartyType.Visible = false;
                CmbType.Visible = false;
                lblCtype.Visible = false;
                lblPType.Visible = false;
                lblPartyType.Text = "Customer Type";
                CmbType.Items.Clear();
                CmbType.Items.Add("Customer");
                CmbState.SelectedIndex = -1;
            }
            LoadDaaGrid(0);
            GetState();
          
        }

        private void SplitAdd_Click(object sender, EventArgs e)
        {
            try
            {
                txtCName.Tag = "0";
                grFront.Visible = false;
                grBack.Visible = true;
                ClearTextBoxes();
                chckActive.Checked = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                throw;
            }
        }

        private void SplitAdd_DropDowItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            try
            {
                if (e.ClickedItem.Text == "Edit")
                {
                    var selectedItem = SfDataGridPartyM.SelectedItems[0];
                    var dataRow = (selectedItem as DataRowView).Row;
                    decimal CUid = Convert.ToDecimal(dataRow["CUid"].ToString());
                    DataTable dt = LoadDaaGridGet(CUid);
                    if (dt.Rows.Count > 0)
                    {
                        txtCName.Text = dt.Rows[0]["CompanyName"].ToString();
                        txtCName.Tag = dt.Rows[0]["CUid"].ToString();
                        txtGSTNo.Text = dt.Rows[0]["GSTNo"].ToString();
                        txtCode.Text = dt.Rows[0]["CCode"].ToString();
                        txtLandLine.Text = dt.Rows[0]["LandLine"].ToString();
                        txtConatctNo.Text = dt.Rows[0]["ContactNo"].ToString();
                        txtCPerson1.Text = dt.Rows[0]["CPerson1"].ToString();
                        txtCPerson2.Text = dt.Rows[0]["CPerson2"].ToString();
                        txtCNumber1.Text = dt.Rows[0]["CContact1"].ToString();
                        txtCNumber2.Text = dt.Rows[0]["CContact2"].ToString();
                        txtCEmail1.Text = dt.Rows[0]["CEmai1"].ToString();
                        txtCEmail2.Text = dt.Rows[0]["CEmail2"].ToString();
                        txtAdd1.Text = dt.Rows[0]["Address1"].ToString();
                        txtAdd2.Text = dt.Rows[0]["Address2"].ToString();
                        txtCity.Text = dt.Rows[0]["City"].ToString();
                        CmbState.SelectedValue = dt.Rows[0]["StateUid"].ToString();
                        txtPin.Text = dt.Rows[0]["Pincode"].ToString();
                        txtEmail.Text = dt.Rows[0]["CEmail"].ToString();
                        CmbPaymentTerms.SelectedValue = dt.Rows[0]["PaymentTerms"].ToString();
                        CmbPartyType.Text = dt.Rows[0]["PartyType"].ToString();
                        CmbType.Text = dt.Rows[0]["Type"].ToString();
                        CmbCountry.Text = dt.Rows[0]["Country"].ToString();
                        bool Active = Convert.ToBoolean(dt.Rows[0]["Active"].ToString());
                        if (Active == true)
                        {
                            chckActive.Checked = true;
                        }
                        else
                        {
                            chckActive.Checked = false;
                        }
                        grFront.Visible = false;
                        grBack.Visible = true;
                    }
                    else
                    {
                        MessageBox.Show("No Records Found", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                }
                else
                {
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                return;
            }
        }

        public void ClearTextBoxes(bool searchRecursively = true)
        {
            void clearTextBoxes(Control.ControlCollection controls, bool searchChildren)
            {
                foreach (Control c in controls)
                {
                    TextBox txt = c as TextBox;
                    txt?.Clear();
                    if (searchChildren && c.HasChildren)
                        clearTextBoxes(c.Controls, true);
                }
            }

            clearTextBoxes(this.Controls, searchRecursively);
        }

        private void SplitSave_DropDowItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            try
            {
                if (e.ClickedItem.Text == "Save & Close")
                {
                    SplitButton1_Click(sender, e);
                    grFront.Visible = true;
                    grBack.Visible = false;
                    ClearTextBoxes();
                }
                else
                {
                    grFront.Visible = true;
                    grBack.Visible = false;
                    ClearTextBoxes();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                return;
            }
        }

        protected void LoadDaaGrid(decimal Uid)
        {
            try
            {
                SqlParameter[] parameters = { new SqlParameter("@Uid", Uid), new SqlParameter("@Tag", Tag) };
                DataTable dataTable = db.GetDataWithParam(CommandType.StoredProcedure, "SP_GetPartyM", parameters, conn);
                DataTable dt = new DataTable();
                dt.Columns.Add("Name", typeof(string));
                dt.Columns.Add("Code", typeof(string));
                dt.Columns.Add("GST No", typeof(string));
                dt.Columns.Add("City", typeof(string));
                dt.Columns.Add("State", typeof(string));
                dt.Columns.Add("Contact", typeof(string));
                dt.Columns.Add("CUid", typeof(decimal));
                dt.Columns.Add("Country", typeof(string));
                for (int i = 0; i < dataTable.Rows.Count; i++)
                {
                    System.Data.DataRow row = dt.NewRow();
                    row[0] = dataTable.Rows[i]["CompanyName"].ToString().Trim();
                    row[1] = dataTable.Rows[i]["CCode"].ToString();
                    row[2] = dataTable.Rows[i]["GSTNo"].ToString();
                    row[3] = dataTable.Rows[i]["City"].ToString();
                    row[4] = dataTable.Rows[i]["StateName"].ToString();
                    row[5] = dataTable.Rows[i]["ContactNo"].ToString();
                    row[6] = dataTable.Rows[i]["CUid"].ToString();
                    row[7] = dataTable.Rows[i]["Country"].ToString();
                    dt.Rows.Add(row);
                }
                SfDataGridPartyM.DataSource = dt;
                SfDataGridPartyM.Columns[0].Width = 300;
                SfDataGridPartyM.Columns[4].Width = 150;
                if(Tag == 0)
                {
                    SfDataGridPartyM.Columns[2].Visible = false;
                    SfDataGridPartyM.Columns[7].Visible = true;
                    SfDataGridPartyM.Columns[7].Width = 150;
                }
                else
                {
                    SfDataGridPartyM.Columns[2].Width = 150;
                    SfDataGridPartyM.Columns[7].Visible = false;
                }
                //SfDataGridPartyM. ColStyles[2].WrapText = true;
                foreach (var column in this.SfDataGridPartyM.Columns)
                {
                    if (!column.MappingName.Equals("Name") && !column.MappingName.Equals("Code"))
                        excludeColumns.Add(column.MappingName);
                }

                (SfDataGridPartyM.Columns["Name"] as GridTextColumn).AllowTextWrapping = true;
                SfDataGridPartyM.QueryRowHeight += SfDataGrid_QueryRowHeight;
                gridRowResizingOptions.ExcludeColumns = excludeColumns;

                SfDataGridPartyM.Columns[6].Visible = false;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void GetState()
        {
            try
            {
                SqlParameter[] parameters = { new SqlParameter("@TypeMUid", 15), new SqlParameter("@Active", "1") };
                DataTable dt = db.GetDataWithParam(CommandType.StoredProcedure, "SP_GetGeneralM", parameters, conn);
                CmbState.DataSource = null;
                CmbState.DisplayMember = "GeneralName";
                CmbState.ValueMember = "GUid";
                CmbState.DataSource = dt;
                CmbState.SelectedIndex = 32;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void SplitButton1_Click(object sender, EventArgs e)
        {
            try
            {
                bool Active = false;
                if (chckActive.Checked == true)
                {
                    Active = true;
                }
                else
                {
                    Active = false;
                }
                if (txtPaymentTerms.Text == string.Empty)
                {
                    txtPaymentTerms.Text = "0";
                }
                decimal stateid = 0;
                if(CmbState.SelectedIndex == -1)
                {
                    stateid = 0;
                }
                else
                {
                    stateid = Convert.ToDecimal(CmbState.SelectedValue);
                }
                if(Tag == 0)
                {
                    stateid = 0;
                }
                if (txtCName.Text != string.Empty || txtGSTNo.Text != string.Empty || CmbState.SelectedIndex != -1)
                {
                    SqlParameter[] parameters = {
                        new SqlParameter("@Uid",txtCName.Tag),
                        new SqlParameter("@Name",txtCName.Text),
                        new SqlParameter("@GSTNumber",txtGSTNo.Text),
                        new SqlParameter("@Code",txtCode.Text),
                        new SqlParameter("@LandLine",txtLandLine.Text),
                        new SqlParameter("@ContactNo",txtConatctNo.Text),
                        new SqlParameter("@ContactPerson1",txtCPerson1.Text),
                        new SqlParameter("@ContactPerson2",txtCPerson2.Text),
                        new SqlParameter("@CPNumber1",txtCNumber1.Text),
                        new SqlParameter("@CPNumber2",txtCNumber2.Text),
                        new SqlParameter("@CPEmail1",txtCEmail1.Text),
                        new SqlParameter("@CPEmail2",txtCEmail2.Text),
                        new SqlParameter("@Address1",txtAdd1.Text),
                        new SqlParameter("@Address2",txtAdd2.Text),
                        new SqlParameter("@City",txtCity.Text),
                        new SqlParameter("@State",stateid),
                        new SqlParameter("@Pincode",txtPin.Text),
                        new SqlParameter("@CEmail",txtEmail.Text),
                        new SqlParameter("@PaymentTerms",CmbPaymentTerms.SelectedValue),
                        new SqlParameter("@PartyType",CmbPartyType.Text),
                        new SqlParameter("@Type",CmbType.Text),
                        new SqlParameter("@Active",Active),
                        new SqlParameter("@Tag",Tag),
                        new SqlParameter("@Country",CmbCountry.Text)
                    };
                    db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_PartyM", parameters, conn);
                    ClearTextBoxes();
                    CmbType.SelectedIndex = -1;
                    CmbPartyType.SelectedIndex = -1;
                    CmbState.SelectedIndex = -1;
                    MessageBox.Show("Record Saved Successfully", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                return;
            }
        }
        protected DataTable LoadDaaGridGet(decimal Uid)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlParameter[] parameters = { new SqlParameter("@Uid", Uid) };
                dt = db.GetDataWithParam(CommandType.StoredProcedure, "SP_GetPartyM", parameters, conn);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }
        //To get the calculated height from GetAutoRowHeight method.
        int height;
        RowAutoFitOptions gridRowResizingOptions = new RowAutoFitOptions();
        void SfDataGrid_QueryRowHeight(object sender, QueryRowHeightEventArgs e)
        {
            if (this.SfDataGridPartyM.TableControl.IsTableSummaryIndex(e.RowIndex))
            {
                e.Height = 40;
                e.Handled = true;
            }
            else if (this.SfDataGridPartyM.AutoSizeController.GetAutoRowHeight(e.RowIndex, gridRowResizingOptions, out height))
            {
                if (height > this.SfDataGridPartyM.RowHeight)
                {
                    e.Height = height;
                    e.Handled = true;
                }
            }
        }

        private void BtnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                var selectedItem = SfDataGridPartyM.SelectedItems[0];
                var dataRow = (selectedItem as DataRowView).Row;
                decimal CUid = Convert.ToDecimal(dataRow["CUid"].ToString());
                DialogResult dialogResult = MessageBox.Show("Do you want do delete this record", "Infromation", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2); ;
                if (dialogResult == DialogResult.Yes)
                {
                    SqlParameter[] sqlParameters = {
                        new SqlParameter("@Tag",Tag),
                        new SqlParameter("@Uid",CUid),
                        new SqlParameter("@ReturnID",SqlDbType.Int)
                    };
                    sqlParameters[2].Direction = ParameterDirection.Output;
                    int uid = db.ExecuteNonQuery(CommandType.StoredProcedure, "Proc_DeletePartyM", sqlParameters, conn, 2);
                    if (uid == 1)
                    {
                        MessageBox.Show("Record Deleted Successfully", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        MessageBox.Show("already refered can't delete this record", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    LoadDaaGrid(0);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void CmbCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (CmbCountry.Text == "India")
                {
                    CmbState.Visible = true;
                    lblState.Visible = true;
                }
                else
                {
                    CmbState.Visible = false;
                    lblState.Visible = false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void LoadCountry()
        {
            try
            {
                DataTable dt = GetGeneralData(26, 1);
                CmbCountry.DataSource = null;
                CmbCountry.DisplayMember = "GeneralName";
                CmbCountry.ValueMember = "Guid";
                CmbCountry.DataSource = dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void LoadPaymode()
        {
            try
            {
                DataTable dt = GetGeneralData(25, 1);
                CmbPaymentTerms.DataSource = null;
                CmbPaymentTerms.DisplayMember = "GeneralName";
                CmbPaymentTerms.ValueMember = "Guid";
                CmbPaymentTerms.DataSource = dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected DataTable GetGeneralData(int TypemUid, int Active)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlParameter[] sqlParameters = { new SqlParameter("@TypeMUid", TypemUid), new SqlParameter("@Active", Active) };
                dt = db.GetDataWithParam(CommandType.StoredProcedure, "SP_GetGeneralM", sqlParameters, conn);
            }
            catch(Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        private void tabGeneral_Click(object sender, EventArgs e)
        {

        }
    }
}
