﻿namespace MyEasyBizAPMS
{
    partial class FrmCon
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmCon));
            this.tabC = new System.Windows.Forms.TabControl();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.label1 = new System.Windows.Forms.Label();
            this.cbost = new System.Windows.Forms.ComboBox();
            this.cbopro1 = new System.Windows.Forms.ComboBox();
            this.label32 = new System.Windows.Forms.Label();
            this.RQGR = new System.Windows.Forms.DataGridView();
            this.txtoutqty = new System.Windows.Forms.TextBox();
            this.label34 = new System.Windows.Forms.Label();
            this.txtoutitem = new System.Windows.Forms.TextBox();
            this.label35 = new System.Windows.Forms.Label();
            this.button17 = new System.Windows.Forms.Button();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.label3 = new System.Windows.Forms.Label();
            this.cboty = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.cbocon = new System.Windows.Forms.ComboBox();
            this.cbouom = new System.Windows.Forms.ComboBox();
            this.HFIT = new System.Windows.Forms.DataGridView();
            this.button11 = new System.Windows.Forms.Button();
            this.txtdcqty = new System.Windows.Forms.TextBox();
            this.buttnext1 = new System.Windows.Forms.Button();
            this.tabC.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RQGR)).BeginInit();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HFIT)).BeginInit();
            this.SuspendLayout();
            // 
            // tabC
            // 
            this.tabC.Controls.Add(this.tabPage2);
            this.tabC.Controls.Add(this.tabPage3);
            this.tabC.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabC.Location = new System.Drawing.Point(5, 5);
            this.tabC.Name = "tabC";
            this.tabC.SelectedIndex = 0;
            this.tabC.Size = new System.Drawing.Size(959, 490);
            this.tabC.TabIndex = 444;
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.tabPage2.Controls.Add(this.label1);
            this.tabPage2.Controls.Add(this.cbost);
            this.tabPage2.Controls.Add(this.cbopro1);
            this.tabPage2.Controls.Add(this.label32);
            this.tabPage2.Controls.Add(this.RQGR);
            this.tabPage2.Controls.Add(this.txtoutqty);
            this.tabPage2.Controls.Add(this.label34);
            this.tabPage2.Controls.Add(this.txtoutitem);
            this.tabPage2.Controls.Add(this.label35);
            this.tabPage2.Controls.Add(this.button17);
            this.tabPage2.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabPage2.Location = new System.Drawing.Point(4, 28);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(955, 458);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Contractor";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(738, 21);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(42, 21);
            this.label1.TabIndex = 5604;
            this.label1.Text = "Type";
            // 
            // cbost
            // 
            this.cbost.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbost.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbost.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbost.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbost.FormattingEnabled = true;
            this.cbost.Items.AddRange(new object[] {
            "Active",
            "InActive"});
            this.cbost.Location = new System.Drawing.Point(742, 44);
            this.cbost.Name = "cbost";
            this.cbost.Size = new System.Drawing.Size(126, 27);
            this.cbost.TabIndex = 5603;
            // 
            // cbopro1
            // 
            this.cbopro1.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbopro1.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbopro1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbopro1.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbopro1.FormattingEnabled = true;
            this.cbopro1.Location = new System.Drawing.Point(41, 44);
            this.cbopro1.Name = "cbopro1";
            this.cbopro1.Size = new System.Drawing.Size(182, 27);
            this.cbopro1.TabIndex = 5602;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.Location = new System.Drawing.Point(46, 20);
            this.label32.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(121, 21);
            this.label32.TabIndex = 426;
            this.label32.Text = "Contractor Type";
            // 
            // RQGR
            // 
            this.RQGR.BackgroundColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.RQGR.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.RQGR.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.RQGR.Location = new System.Drawing.Point(43, 79);
            this.RQGR.Name = "RQGR";
            this.RQGR.Size = new System.Drawing.Size(876, 359);
            this.RQGR.TabIndex = 416;
            this.RQGR.DoubleClick += new System.EventHandler(this.RQGR_DoubleClick);
            // 
            // txtoutqty
            // 
            this.txtoutqty.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtoutqty.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtoutqty.Location = new System.Drawing.Point(611, 45);
            this.txtoutqty.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtoutqty.Name = "txtoutqty";
            this.txtoutqty.Size = new System.Drawing.Size(124, 27);
            this.txtoutqty.TabIndex = 418;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.Location = new System.Drawing.Point(611, 21);
            this.label34.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(83, 21);
            this.label34.TabIndex = 421;
            this.label34.Text = "Mobile No";
            // 
            // txtoutitem
            // 
            this.txtoutitem.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtoutitem.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtoutitem.Location = new System.Drawing.Point(230, 45);
            this.txtoutitem.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtoutitem.Name = "txtoutitem";
            this.txtoutitem.Size = new System.Drawing.Size(373, 27);
            this.txtoutitem.TabIndex = 417;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.Location = new System.Drawing.Point(226, 21);
            this.label35.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(85, 21);
            this.label35.TabIndex = 418;
            this.label35.Text = "Contractor";
            // 
            // button17
            // 
            this.button17.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button17.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button17.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button17.Location = new System.Drawing.Point(874, 43);
            this.button17.Name = "button17";
            this.button17.Size = new System.Drawing.Size(45, 29);
            this.button17.TabIndex = 420;
            this.button17.Text = "OK";
            this.button17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button17.UseVisualStyleBackColor = false;
            this.button17.Click += new System.EventHandler(this.button17_Click);
            // 
            // tabPage3
            // 
            this.tabPage3.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.tabPage3.Controls.Add(this.label3);
            this.tabPage3.Controls.Add(this.cboty);
            this.tabPage3.Controls.Add(this.label2);
            this.tabPage3.Controls.Add(this.label16);
            this.tabPage3.Controls.Add(this.label14);
            this.tabPage3.Controls.Add(this.cbocon);
            this.tabPage3.Controls.Add(this.cbouom);
            this.tabPage3.Controls.Add(this.HFIT);
            this.tabPage3.Controls.Add(this.button11);
            this.tabPage3.Controls.Add(this.txtdcqty);
            this.tabPage3.Location = new System.Drawing.Point(4, 28);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(951, 458);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Contract Worker";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(779, 28);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(42, 21);
            this.label3.TabIndex = 5606;
            this.label3.Text = "Type";
            // 
            // cboty
            // 
            this.cboty.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboty.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboty.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboty.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboty.FormattingEnabled = true;
            this.cboty.Items.AddRange(new object[] {
            "Active",
            "InActive"});
            this.cboty.Location = new System.Drawing.Point(783, 51);
            this.cboty.Name = "cboty";
            this.cboty.Size = new System.Drawing.Size(126, 27);
            this.cboty.TabIndex = 5605;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(382, 26);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(105, 21);
            this.label2.TabIndex = 5598;
            this.label2.Text = "Worker Name";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(48, 26);
            this.label16.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(85, 21);
            this.label16.TabIndex = 5597;
            this.label16.Text = "Contractor";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(627, 26);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(109, 21);
            this.label14.TabIndex = 5596;
            this.label14.Text = "Opeartor Type";
            // 
            // cbocon
            // 
            this.cbocon.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbocon.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbocon.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbocon.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbocon.FormattingEnabled = true;
            this.cbocon.Location = new System.Drawing.Point(48, 52);
            this.cbocon.Name = "cbocon";
            this.cbocon.Size = new System.Drawing.Size(331, 27);
            this.cbocon.TabIndex = 5561;
            // 
            // cbouom
            // 
            this.cbouom.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbouom.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbouom.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbouom.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbouom.FormattingEnabled = true;
            this.cbouom.Location = new System.Drawing.Point(627, 52);
            this.cbouom.Name = "cbouom";
            this.cbouom.Size = new System.Drawing.Size(150, 27);
            this.cbouom.TabIndex = 5560;
            // 
            // HFIT
            // 
            this.HFIT.BackgroundColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.HFIT.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.HFIT.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.HFIT.Location = new System.Drawing.Point(48, 87);
            this.HFIT.Name = "HFIT";
            this.HFIT.Size = new System.Drawing.Size(895, 361);
            this.HFIT.TabIndex = 5587;
            this.HFIT.DoubleClick += new System.EventHandler(this.HFIT_DoubleClick);
            // 
            // button11
            // 
            this.button11.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button11.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button11.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button11.Location = new System.Drawing.Point(914, 51);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(37, 28);
            this.button11.TabIndex = 5567;
            this.button11.Text = "OK";
            this.button11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button11.UseVisualStyleBackColor = false;
            this.button11.Click += new System.EventHandler(this.button11_Click);
            // 
            // txtdcqty
            // 
            this.txtdcqty.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtdcqty.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtdcqty.Location = new System.Drawing.Point(386, 52);
            this.txtdcqty.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtdcqty.Name = "txtdcqty";
            this.txtdcqty.Size = new System.Drawing.Size(234, 27);
            this.txtdcqty.TabIndex = 5558;
            // 
            // buttnext1
            // 
            this.buttnext1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttnext1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttnext1.Image = ((System.Drawing.Image)(resources.GetObject("buttnext1.Image")));
            this.buttnext1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttnext1.Location = new System.Drawing.Point(439, 497);
            this.buttnext1.Name = "buttnext1";
            this.buttnext1.Size = new System.Drawing.Size(57, 30);
            this.buttnext1.TabIndex = 445;
            this.buttnext1.Text = "Exit";
            this.buttnext1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttnext1.UseVisualStyleBackColor = false;
            this.buttnext1.Click += new System.EventHandler(this.buttnext1_Click);
            // 
            // FrmCon
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.ClientSize = new System.Drawing.Size(959, 523);
            this.Controls.Add(this.buttnext1);
            this.Controls.Add(this.tabC);
            this.Name = "FrmCon";
            this.Text = "Contractor";
            this.Load += new System.EventHandler(this.FrmCon_Load);
            this.tabC.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RQGR)).EndInit();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HFIT)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabC;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.DataGridView RQGR;
        private System.Windows.Forms.TextBox txtoutqty;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.TextBox txtoutitem;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Button button17;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ComboBox cbocon;
        private System.Windows.Forms.ComboBox cbouom;
        private System.Windows.Forms.DataGridView HFIT;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.TextBox txtdcqty;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cbost;
        private System.Windows.Forms.ComboBox cbopro1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cboty;
        private System.Windows.Forms.Button buttnext1;
    }
}