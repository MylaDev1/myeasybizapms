﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MyEasyBizAPMS
{
    public class GeneralParametrs
    {
        public static int MenyKey = 0;
    }
    public class GeneralParameters
    {
        public static string ServerName = string.Empty;
        public static string CompanyId = string.Empty;
        public static string Password = string.Empty;
        public static string UserName = string.Empty;
        public static string DbName = string.Empty;
        public static string ConnectionString = string.Empty;
        public static int UserdId;
        public static string LoginUserName = string.Empty;
        public static int MenuId = 0;
        public static DataTable dt = new DataTable();
        public static DataTable dtMenus = new DataTable();
        public static DataTable dtSubmenu = new DataTable();
        public static DataTable dtsubmenuofsubmenu = new DataTable();
        public static DataTable dtCompany = new DataTable();
        public static string ReportName = string.Empty;
        public static decimal ReportUid = 0;

        public static Point FindLocation(Control ctrl)
        {
            if (ctrl.Parent is Form)
                return ctrl.Location;
            else
            {
                Point p = FindLocation(ctrl.Parent);
                p.X += ctrl.Location.X;
                p.Y += ctrl.Location.Y;
                return p;
            }
        }

        public static DataTable ReadExcel(string fileName, string fileExt)
        {
            string conn = string.Empty;
            DataTable dtexcel = new DataTable();
            DataTable dtexcel1 = new DataTable();
            if (fileExt.CompareTo(".xls") == 0)
                conn = @"provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + fileName + ";Extended Properties='Excel 8.0;HRD=Yes;IMEX=1';"; //for below excel 2007  
            else
                conn = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + fileName + ";Extended Properties='Excel 12.0;HDR=NO';"; //for above excel 2007  
            using (OleDbConnection con = new OleDbConnection(conn))
            {
                try
                {
                    OleDbDataAdapter oleAdpt = new OleDbDataAdapter("select * from [Sheet1$]", con); //here we read data from sheet1  
                    oleAdpt.Fill(dtexcel); //fill excel data into dataTable  
                }
                catch(Exception ex)
                {
                    throw ex;
                }
            }
            return dtexcel;
        }

        public static string GetDocNo(decimal DocTypeId,SqlConnection connection)
        {
            string DocNo = string.Empty;
            try
            {
                SqlParameter[] parameters = {
                    new SqlParameter("@DocTypeId",DocTypeId)
                };
                SQLDBHelper db = new SQLDBHelper();
                DataTable dt = db.GetDataWithParam(CommandType.StoredProcedure, "SP_GetDocTypeM", parameters, connection);
                if(dt.Rows.Count > 0)
                {
                    decimal LastNo = Convert.ToDecimal(dt.Rows[0]["Lastno"].ToString()) + 1;
                    string Prefix = dt.Rows[0]["PrFix"].ToString();
                    string Suffix = dt.Rows[0]["suffix"].ToString();
                    DocNo = Prefix + Suffix + "/" + LastNo.ToString("00000");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return DocNo;
        }

        public static void UpdateDocNo(decimal DocTypeId,decimal LastNo, SqlConnection connection)
        {
            try
            {
                SqlParameter[] parameters = {
                    new SqlParameter("@DocTypeId",DocTypeId),
                    new SqlParameter("@Lastno",LastNo)
                };
                SQLDBHelper db = new SQLDBHelper();
                db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_UpdateDocTypeM", parameters, connection);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
