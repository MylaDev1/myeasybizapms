﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Drawing.Imaging;
using System.Configuration;
using System.Data.SqlClient;
using System.Diagnostics;
using excel = Microsoft.Office.Interop.Excel;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;

namespace MyEasyBizAPMS
{
    public partial class FrmGenStockTr : Form
    {
        public FrmGenStockTr()
        {
            CenterToParent();
            this.BackColor = Color.White;
            InitializeComponent();
        }
        SqlCommand qur = new SqlCommand();
        ReportDocument doc = new ReportDocument();
        string uid = "";
        int mode = 0;
        double dis9 = 0;
        double dis3 = 0;
        double dis4 = 0;
        double dd8 = 0;
        double dd1 = 0;
        double dd2 = 0;
        double dd3 = 0;
        int cell9 = 0;
        double hg = 0;
        double df = 0;
        int ty = 0;
        int j = -1;
        BindingSource bsc = new BindingSource();
        BindingSource bs = new BindingSource();
        SQLDBHelper db = new SQLDBHelper();
        int Fillid;
        BindingSource bsParty = new BindingSource();
        string str1key;
        public int SelectId = 0;
        private static Microsoft.Office.Interop.Excel.Workbook mWorkBook;
        private static Microsoft.Office.Interop.Excel.Sheets mWorkSheets;
        private static Microsoft.Office.Interop.Excel.Worksheet mWSheet1;
        private static Microsoft.Office.Interop.Excel.Application oXL;
        //SqlCommand cmd;
        SqlConnection conn = new SqlConnection(GeneralParameters.ConnectionString);
        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtname_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bsParty.Filter = string.Format("workorder LIKE '%{0}%' ", txtname.Text);



                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
        public void prgrp()
        {
            conn.Close();
            conn.Open();
            string qur = "select distinct socno,0 as uid   from processdet   ";
            SqlCommand cmd = new SqlCommand(qur, conn);
            SqlDataAdapter apt = new SqlDataAdapter(cmd);
            DataTable tab = new DataTable();
            apt.Fill(tab);
            cbotype.DataSource = null;
            cbotype.DataSource = tab;
            cbotype.DisplayMember = "socno";
            cbotype.ValueMember = "uid";
            cbotype.SelectedIndex = -1;

        }
        protected DataTable getParty()
        {
            DataTable dt = new DataTable();
            try
            {

                if (ty == 1)
                {
                    dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "sp_getsttransfersocno", conn);
                    bsParty.DataSource = dt;
                }
                else if (ty == 2)
                {
                    //SqlParameter[] para = { new SqlParameter("@socno", txtname.Text) };
                    //dt = db.getDataWithParameterCmdType(CommandType.StoredProcedure, "sp_getsttransferitem", para);
                    dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "sp_getsttransferitemGen", conn);
                    bsc.DataSource = dt;
                   
                }
                else if (ty == 3)
                {
                    dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "sp_getTrimssocno", conn);
                    bs.DataSource = dt;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            return dt;
        }
        private void txtname_Click(object sender, EventArgs e)
        {
            ty = 1;
            DataTable dt = getParty();
            bsParty.DataSource = dt;
            FillGrid(dt, 1);
            Point loc = FindLocation(txtname);
            grSearch.Location = new Point(loc.X, loc.Y + 20);
            grSearch.Visible = true;
            grSearch.Text = "Party Search";
        }
        private Point FindLocation(Control ctrl)
        {
            if (ctrl.Parent is Form)
                return ctrl.Location;
            else
            {
                Point p = FindLocation(ctrl.Parent);
                p.X += ctrl.Location.X;
                p.Y += ctrl.Location.Y;
                return p;
            }
        }
        protected void FillGrid(DataTable dt, int FillId)
        {
            try
            {
                DataGridCommon.DataSource = null;
                DataGridCommon.AutoGenerateColumns = false;
                DataGridCommon.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
                DataGridCommon.DefaultCellStyle.WrapMode = DataGridViewTriState.True;

                DataGridCommon.ColumnCount = 2;
                DataGridCommon.Columns[0].Name = "Uid";
                DataGridCommon.Columns[0].HeaderText = "Uid";
                DataGridCommon.Columns[0].DataPropertyName = "Uid";
                DataGridCommon.Columns[1].Name = "workorder";
                DataGridCommon.Columns[1].HeaderText = "Socno";
                DataGridCommon.Columns[1].DataPropertyName = "workorder";


                DataGridCommon.Columns[1].Width = 350;
                DataGridCommon.DataSource = bsParty;
                DataGridCommon.Columns[0].Visible = false;


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void txtitem_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bsc.Filter = string.Format("Item LIKE '%{0}%' ", txtitem.Text);



                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
        protected void FillGrid1(DataTable dt, int FillId)
        {
            try
            {
                DataGridCommon.DataSource = null;
                DataGridCommon.AutoGenerateColumns = false;
                DataGridCommon.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
                DataGridCommon.DefaultCellStyle.WrapMode = DataGridViewTriState.True;

                DataGridCommon.ColumnCount = 5;
                DataGridCommon.Columns[0].Name = "Uid";
                DataGridCommon.Columns[0].HeaderText = "Uid";
                DataGridCommon.Columns[0].DataPropertyName = "Uid";
                DataGridCommon.Columns[1].Name = "workorder";
                DataGridCommon.Columns[1].HeaderText = "workorder";
                DataGridCommon.Columns[1].DataPropertyName = "workorder";
                DataGridCommon.Columns[2].Name = "Item";
                DataGridCommon.Columns[2].HeaderText = "Item";
                DataGridCommon.Columns[2].DataPropertyName = "Item";
                DataGridCommon.Columns[3].Name = "Qty";
                DataGridCommon.Columns[3].HeaderText = "Qty";
                DataGridCommon.Columns[3].DataPropertyName = "Qty";
                DataGridCommon.Columns[4].Name = "itemid";
                DataGridCommon.Columns[4].HeaderText = "itemid";
                DataGridCommon.Columns[4].DataPropertyName = "itemid";

      
                DataGridCommon.Columns[2].Width = 250;
                DataGridCommon.Columns[3].Width = 100;
                DataGridCommon.DataSource = bsc;
                DataGridCommon.Columns[1].Visible = false;
                DataGridCommon.Columns[0].Visible = false;
                DataGridCommon.Columns[4].Visible = false;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        private void txtitem_Click(object sender, EventArgs e)
        {
            ty = 2;
            DataTable dt = getParty();
            bsParty.DataSource = dt;
            FillGrid1(dt, 1);
            Point loc = FindLocation(txtitem);
            grSearch.Location = new Point(loc.X, loc.Y + 20);
            grSearch.Visible = true;
            grSearch.Text = "Party Search";
        }

        private void btnHide_Click(object sender, EventArgs e)
        {
            grSearch.Visible = false;
        }

        private void button18_Click(object sender, EventArgs e)
        {
            try
            {
                SelectId = 1;
                int Index = DataGridCommon.SelectedCells[0].RowIndex;
                if (ty == 1)
                {
                    txtname.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                 

                }
                else if (ty == 2)
                {

                    txtitem.Text = DataGridCommon.Rows[Index].Cells[2].Value.ToString();
                    txtitem.Tag = DataGridCommon.Rows[Index].Cells[4].Value.ToString();
                    txtqty.Text = DataGridCommon.Rows[Index].Cells[3].Value.ToString();
                    txtqty.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();

                }

                grSearch.Visible = false;
                SelectId = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void buttcusok_Click(object sender, EventArgs e)
        {
            if(txtreason.Text=="")
            {
                MessageBox.Show("Enter the reson");
                return;
            }


            conn.Close();
            conn.Open();

            qur.CommandText = "exec sp_getlessqtystk "+ txtqty.Tag +" ,"+ txtqty.Text +"";
            qur.ExecuteNonQuery(); 
            conn.Close();
            conn.Open();
            {

              
                                SqlParameter[] parameters = {
                    new SqlParameter("@docno", "1"),
                    new SqlParameter("@docdate", Convert.ToDateTime(txtdcodate.Text).ToString("yyyy-MM-dd")),
                    new SqlParameter("@doctypeid", "9" ),
                    new SqlParameter("@trantype","General Stock Transfer"),
                    new SqlParameter("@itemid", txtitem.Tag),
                    new SqlParameter("@issqty", "0"),
                    new SqlParameter("@recqty", txtqty.Text),
                    new SqlParameter("@yearid", "0" ),
                    new SqlParameter("@logdate", Convert.ToDateTime(txtdcodate.Text).ToString("yyyy-MM-dd")),
                    new SqlParameter("@headttable_uid", "1"),
                            new SqlParameter("@dettable_uid", "0"),
                    new SqlParameter("@itemname", txtitem.Text),
                    new SqlParameter("@partyuid", "0"),
                    new SqlParameter("@companyid","1"),
                                    new SqlParameter("@color", txtreason.Text),
                    new SqlParameter("@dia", "0"),
                    new SqlParameter("@gsm", "0"),
                    new SqlParameter("@workorderdettable_uid","0" ),
      
                    new SqlParameter("@workorder", cbotype.Text),
                        new SqlParameter("@active", "1"),
            



                    };
                db.ExecuteNonQuery(CommandType.StoredProcedure, "sp_stoktrans", parameters, conn);

                MessageBox.Show("Record has been saved", "Save", MessageBoxButtons.OK);


            }
            txtreason.Text = "";
            txtitem.Text = "";
            txtname.Text = "";
            txtqty.Text = "";
            LoadGetJobCard(1);

        }

        private void DataGridCommon_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                SelectId = 1;
                int Index = DataGridCommon.SelectedCells[0].RowIndex;
                if (Genclass.ty == 1)
                {
                    txtname.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();


                }
                else if (Genclass.ty == 2)
                {

                    txtitem.Text = DataGridCommon.Rows[Index].Cells[2].Value.ToString();
                    txtitem.Tag = DataGridCommon.Rows[Index].Cells[4].Value.ToString();
                    txtqty.Text = DataGridCommon.Rows[Index].Cells[3].Value.ToString();
                    txtqty.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();

                }

                grSearch.Visible = false;
                SelectId = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void FrmGenStockTr_Load(object sender, EventArgs e)
        {
            qur.Connection = conn;
            HFIT.RowHeadersVisible = false;
            this.HFIT.DefaultCellStyle.Font = new Font("calibri", 9);
            this.HFIT.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 9, FontStyle.Bold);
            HFIT.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            HFIT.EnableHeadersVisualStyles = false;
            HFIT.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.GradientActiveCaption;
            LoadGetJobCard(1);
            prgrp();
        }

        protected DataTable LoadGetJobCard(int tag)
        {


           


            DataTable dt = new DataTable();
            try
            {



                SqlParameter[] para = {
                    new SqlParameter("@active","1"),
              


                };

                dt = db.GetData(CommandType.StoredProcedure, "sp_getstksttranfer", para);

                LoadDataTable(dt);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        protected void LoadDataTable(DataTable dt)
        {
            try
            {
                HFIT.DataSource = null;
                HFIT.AutoGenerateColumns = false;
                HFIT.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
                HFIT.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
                HFIT.ColumnCount = 7;

                HFIT.Columns[0].Name = "Uid";
                HFIT.Columns[0].HeaderText = "Uid";
                HFIT.Columns[0].DataPropertyName = "Uid";

                HFIT.Columns[1].Name = "Socno";
                HFIT.Columns[1].HeaderText = "Socno";
                HFIT.Columns[1].DataPropertyName = "Socno";

                HFIT.Columns[2].Name = "Item";
                HFIT.Columns[2].HeaderText = "Item";
                HFIT.Columns[2].DataPropertyName = "Item";

                HFIT.Columns[3].Name = "TrSocno";
                HFIT.Columns[3].HeaderText = "TrSocno";
                HFIT.Columns[3].DataPropertyName = "TrSocno";

                HFIT.Columns[4].Name = "Qty";
                HFIT.Columns[4].HeaderText = "Qty";
                HFIT.Columns[4].DataPropertyName = "Qty";

                HFIT.Columns[5].Name = "Reason";
                HFIT.Columns[5].HeaderText = "Reason";
                HFIT.Columns[5].DataPropertyName = "Reason";

                HFIT.Columns[6].Name = "itemid";
                HFIT.Columns[6].HeaderText = "itemid";
                HFIT.Columns[6].DataPropertyName = "itemid";




                bs.DataSource = dt;

                HFIT.DataSource = bs;
                HFIT.Columns[0].Visible = false;
                HFIT.Columns[1].Visible = false;
                //HFIT.Columns[1].Width = 87;
                HFIT.Columns[2].Width = 660;
                HFIT.Columns[3].Width = 100;
                HFIT.Columns[4].Width = 100;
                HFIT.Columns[5].Width = 250;
                HFIT.Columns[6].Visible = false;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
