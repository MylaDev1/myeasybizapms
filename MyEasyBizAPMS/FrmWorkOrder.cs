﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Drawing.Imaging;
using System.Configuration;
using System.Data.SqlClient;
using System.Diagnostics;
using excel = Microsoft.Office.Interop.Excel;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;

namespace MyEasyBizAPMS
{
    public partial class FrmWorkOrder : Form
    {
        public FrmWorkOrder()
        {
            this.BackColor = Color.White; ;
            InitializeComponent();
        }
        string uid = "";
        int FillId;
        int mode = 0;
        string tpuid = "";
        SqlConnection conn = new SqlConnection(GeneralParameters.ConnectionString);
        SqlCommand qur = new SqlCommand();
        private DataRow doc1;
        private DataRow doc2;
        BindingSource bs = new BindingSource();
        DataTable Docno = new DataTable();
        DataTable Docno1 = new DataTable();
        BindingSource bsc = new BindingSource();
        BindingSource bsp = new BindingSource();
        BindingSource bsserial = new BindingSource();
        BindingSource IN = new BindingSource();
        BindingSource OUT = new BindingSource();
        BindingSource bsFabric = new BindingSource();
        SQLDBHelper db = new SQLDBHelper();
        int Fillid;
        BindingSource bsParty = new BindingSource();
        string str1key;
        public int SelectId = 0;
        private void Titlep5()
        {
            DataGridViewCheckBoxColumn checkColumn = new DataGridViewCheckBoxColumn();
            checkColumn.Name = "CHK";
            checkColumn.HeaderText = "CHK";
            checkColumn.Width = 50;
            checkColumn.ReadOnly = false;
            checkColumn.FillWeight = 10; //if the datagridview is resized (on form resize) the checkbox won't take up too much; value is relative to the other columns' fill values
            YARNLOAD.Columns.Add(checkColumn);


        }
        private void FrmWorkOrder_Load(object sender, EventArgs e)
        {
            qur.Connection = conn;

            Genclass.buttonstyleform(this);
            Genclass.buttonstylepanel(panadd);
            panadd.Visible = true;
            tabControl1.TabPages.Remove(tabPage4);
            tabControl1.TabPages.Remove(tabPage5);
            this.YARNCOLORLOAD.DefaultCellStyle.Font = new Font("calibri", 10);
            this.YARNCOLORLOAD.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 10, FontStyle.Bold);
            this.YARNLOAD.DefaultCellStyle.Font = new Font("calibri", 10);
            this.YARNLOAD.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 10, FontStyle.Bold);
            this.HFGP.DefaultCellStyle.Font = new Font("calibri", 10);
            this.HFGP.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 10, FontStyle.Bold);
            this.HFIT.DefaultCellStyle.Font = new Font("calibri", 10);
            this.HFIT.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 10, FontStyle.Bold);
            this.RQGR.DefaultCellStyle.Font = new Font("calibri", 10);
            this.RQGR.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 10, FontStyle.Bold);
            this.HFIT1.DefaultCellStyle.Font = new Font("calibri", 10);
            this.HFIT1.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 10, FontStyle.Bold);
            this.HFIPOUT.DefaultCellStyle.Font = new Font("calibri", 10);
            this.HFIPOUT.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 10, FontStyle.Bold);
            HFIT.RowHeadersVisible = false;
            HFIPOUT.RowHeadersVisible = false;
            HFIT1.RowHeadersVisible = false;
            HFGP.RowHeadersVisible = false;
            RQGR.RowHeadersVisible = false;
            chkgrd.RowHeadersVisible = false;
            YARNCOLORLOAD.RowHeadersVisible = false;
            YARNLOAD.RowHeadersVisible = false;
            HFITIN.RowHeadersVisible = false;
            HFITIN1.RowHeadersVisible = false;
            HFITOUT.RowHeadersVisible = false;
            HFITOUT1.RowHeadersVisible = false;

            Genpan.Visible = true;
            Editpan.Visible = false;
            Genclass.Dtype = 520;
            cbotype.Text = "Yarn";
            chkact.Checked = true;
         
            Titlep();
            Titlep1();
            Titlep2();
            Titlep3();
            Titlep4();
            HFIPOUT.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            HFIPOUT.EnableHeadersVisualStyles = false;
            HFIPOUT.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.GradientActiveCaption;
            HFGP.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            HFGP.EnableHeadersVisualStyles = false;
            HFGP.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.GradientActiveCaption;

            HFIT1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            HFIT1.EnableHeadersVisualStyles = false;
            HFIT1.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.GradientActiveCaption;

            HFITIN.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            HFITIN.EnableHeadersVisualStyles = false;
            HFITIN.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.GradientActiveCaption;

            HFITIN1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            HFITIN1.EnableHeadersVisualStyles = false;
            HFITIN1.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.GradientActiveCaption;

            HFITOUT.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            HFITOUT.EnableHeadersVisualStyles = false;
            HFITOUT.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.GradientActiveCaption;

            HFITOUT1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            HFITOUT1.EnableHeadersVisualStyles = false;
            HFITOUT1.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.GradientActiveCaption;
            dtpgrndt.Format = DateTimePickerFormat.Custom;
            dtpgrndt.CustomFormat = "dd/MM/yyyy";

            HFGP.Focus();

            LoadGetJobCard(1);
            prgrp();
            dabricitems();
            Titlep5();
            funyarnload();
            TitlepCOLORLOAD();
        }
        private void dabricitems()
        {
            //conn.Open();
            //string qur = "  select distinct  a.uid,itemspec1 from itemsm a inner join itemsmuom b on a.uid=b.itemuid where a.itemgroup_uid=67 and active=1 ";
            //SqlCommand cmd = new SqlCommand(qur, conn);
            //SqlDataAdapter apt = new SqlDataAdapter(cmd);
            //DataTable tab = new DataTable();
            //apt.Fill(tab);
            //cbofabric.DataSource = null;
            //cbofabric.DataSource = tab;
            //cbofabric.DisplayMember = "itemspec1";
            //cbofabric.ValueMember = "uid";
            //cbofabric.SelectedIndex = -1;
            //conn.Close();
        }
        public void prgrp()
        {
            conn.Close();
            conn.Open();
            string qur = "select Generalname,guid uid from generalm where   Typemuid in (30) and userid=" + GeneralParameters.UserdId + " and active=1  order by guid  ";
            SqlCommand cmd = new SqlCommand(qur, conn);
            SqlDataAdapter apt = new SqlDataAdapter(cmd);
            DataTable tab = new DataTable();
            apt.Fill(tab);
            cbosGReturnItem.DataSource = null;
            cbosGReturnItem.DataSource = tab;
            cbosGReturnItem.DisplayMember = "Generalname";
            cbosGReturnItem.ValueMember = "uid";
            cbosGReturnItem.SelectedIndex = -1;
         
        }
        protected DataTable LoadGetJobCard(int tag)
        {

          

            DataTable dt = new DataTable();
            try
            {


                dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "SP_GETWORKORDERLOAD", conn);
             

                LoadDataTable(dt);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        protected void LoadDataTable(DataTable dt)
        {
            try
            {
                HFGP.DataSource = null;
                HFGP.AutoGenerateColumns = false;
                HFGP.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
                HFGP.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
                HFGP.ColumnCount = 7;
                HFGP.Columns[0].Name = "Uid";
                HFGP.Columns[0].HeaderText = "Uid";
                HFGP.Columns[0].DataPropertyName = "Uid";

                HFGP.Columns[1].Name = "Docno";
                HFGP.Columns[1].HeaderText = "Docno";
                HFGP.Columns[1].DataPropertyName = "Docno";

                HFGP.Columns[2].Name = "Docdate";
                HFGP.Columns[2].HeaderText = "Docdate";
                HFGP.Columns[2].DataPropertyName = "Docdate";

                HFGP.Columns[3].Name = "OrderReference";
                HFGP.Columns[3].HeaderText = "Style";
                HFGP.Columns[3].DataPropertyName = "OrderReference";

                HFGP.Columns[4].Name = "Narration";
                HFGP.Columns[4].HeaderText = "Narration";
                HFGP.Columns[4].DataPropertyName = "Narration";

                HFGP.Columns[5].Name = "Type";
                HFGP.Columns[5].HeaderText = "Type";
                HFGP.Columns[5].DataPropertyName = "Type";


                HFGP.Columns[6].Name = "Socno";
                HFGP.Columns[6].HeaderText = "Socno";
                HFGP.Columns[6].DataPropertyName = "Socno";
                bs.DataSource = dt;

                HFGP.DataSource = bs;
           
        
                HFGP.Columns[0].Visible = false;
                HFGP.Columns[1].Width = 90;
                HFGP.Columns[2].Width = 90;
                HFGP.Columns[3].Width = 338;

                HFGP.Columns[4].Width = 374;
                HFGP.Columns[5].Visible = false;
                HFGP.Columns[6].Width = 90;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        

        private void Titlep()
        {
            HFIT.AutoGenerateColumns = false;
            HFIT.Refresh();
            HFIT.DataSource = null;
            HFIT.Rows.Clear();
            HFIT.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            HFIT.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            HFIT.ColumnCount = 11;
            HFIT.Columns[0].Name = "Refid";
            HFIT.Columns[1].Name = "Seq No";
            HFIT.Columns[2].Name = "Process Name";
            HFIT.Columns[3].Name = "Process Details";
            HFIT.Columns[4].Name = "Colur";
            HFIT.Columns[5].Name = "Rate";
            HFIT.Columns[6].Name = "ProcessGroup";
            HFIT.Columns[7].Name = "processuid";
            HFIT.Columns[8].Name = "Type";
            HFIT.Columns[9].Name = "uid";
            HFIT.Columns[10].Name = "itemid";
            HFIT.Columns[0].Visible = false;
            HFIT.Columns[1].Width = 70;
            HFIT.Columns[2].Width = 280;
            HFIT.Columns[3].Width = 150;
            HFIT.Columns[4].Width = 150;
            HFIT.Columns[5].Visible = false;
            HFIT.Columns[6].Width = 250;
            HFIT.Columns[7].Visible = false;
            HFIT.Columns[8].Visible = false;
            HFIT.Columns[9].Visible = false;
            HFIT.Columns[10].Visible = false;
        }
        private void Titlep1()
        {
            HFIT1.AutoGenerateColumns = false;
            HFIT1.Refresh();
            HFIT1.DataSource = null;
            HFIT1.Rows.Clear();
            HFIT1.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            HFIT1.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            HFIT1.ColumnCount = 11;
            HFIT1.Columns[0].Name = "Refid";
            HFIT1.Columns[1].Name = "Seq No";
            HFIT1.Columns[2].Name = "Process Name";
            HFIT1.Columns[3].Name = "Process Details";
            HFIT1.Columns[4].Name = "Colur";
            HFIT1.Columns[5].Name = "Rate";
            HFIT1.Columns[6].Name = "ProcessGroup";
            HFIT1.Columns[7].Name = "processuid";
            HFIT1.Columns[8].Name = "Type";
            HFIT1.Columns[9].Name = "uid";
            HFIT1.Columns[10].Name = "itemid";
            HFIT1.Columns[0].Visible = false;
            HFIT1.Columns[1].Width = 70;
            HFIT1.Columns[2].Width = 280;
            HFIT1.Columns[3].Width = 150;
            HFIT1.Columns[4].Width = 150;
            HFIT1.Columns[5].Visible = false;
            HFIT1.Columns[6].Width = 250;
            HFIT1.Columns[7].Visible = false;
            HFIT1.Columns[8].Visible = false;
            HFIT1.Columns[9].Visible = false;
            HFIT1.Columns[10].Visible = false;
        }
        private void TitlepCOLORLOAD()
        {
            YARNCOLORLOAD.AutoGenerateColumns = false;
            YARNCOLORLOAD.Refresh();
            YARNCOLORLOAD.DataSource = null;
            YARNCOLORLOAD.Rows.Clear();
            YARNCOLORLOAD.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            YARNCOLORLOAD.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            YARNCOLORLOAD.ColumnCount = 5;

            YARNCOLORLOAD.Columns[0].Name = "Itemname";
            YARNCOLORLOAD.Columns[1].Name = "Color";
            YARNCOLORLOAD.Columns[2].Name = "Qty";
            YARNCOLORLOAD.Columns[3].Name = "Itemid";
            YARNCOLORLOAD.Columns[4].Name = "uid";

            YARNCOLORLOAD.Columns[0].Width = 280;
            YARNCOLORLOAD.Columns[1].Width = 100;
            YARNCOLORLOAD.Columns[2].Width = 100;
            YARNCOLORLOAD.Columns[3].Visible = false;
            YARNCOLORLOAD.Columns[4].Visible = false;
           
        }
        private void Titlep2()
        {
            HFITIN1.AutoGenerateColumns = false;
            HFITIN1.Refresh();
            HFITIN1.DataSource = null;
            HFITIN1.Rows.Clear();
            HFITIN1.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            HFITIN1.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            HFITIN1.ColumnCount = 8;
            HFITIN1.Columns[0].Name = "Itemname";
            HFITIN1.Columns[1].Name = "Qty";
            HFITIN1.Columns[2].Name = "Itemid";
            HFITIN1.Columns[3].Name = "Color";
            HFITIN1.Columns[4].Name = "Process";

            HFITIN1.Columns[5].Name = "uid";
            HFITIN1.Columns[6].Name = "workid";
            HFITIN1.Columns[7].Name = "UOM";

            HFITIN1.Columns[0].Width = 600;
            HFITIN1.Columns[1].Width = 100;
            HFITIN1.Columns[2].Visible = false;
            HFITIN1.Columns[3].Width = 100;
            HFITIN1.Columns[4].Width = 150;
            HFITIN1.Columns[5].Visible = false;
            HFITIN1.Columns[6].Visible = false;
            HFITIN1.Columns[7].Visible = false;
        }

        private void Titlep4()
        {
            HFIPOUT.AutoGenerateColumns = false;
            HFIPOUT.Refresh();
            HFIPOUT.DataSource = null;
            HFIPOUT.Rows.Clear();
            HFIPOUT.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            HFIPOUT.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            HFIPOUT.ColumnCount = 4;
            HFIPOUT.Columns[0].Name = "uid";
            HFIPOUT.Columns[1].Name = "itemname";
            HFIPOUT.Columns[2].Name = "qty";
            HFIPOUT.Columns[3].Name = "itemid";


            HFIPOUT.Columns[0].Visible = false;
            HFIPOUT.Columns[1].Width = 340;
            HFIPOUT.Columns[2].Width = 100;
            HFIPOUT.Columns[3].Visible = false;


        }
        private void Titlep3()
        {
            HFITOUT1.AutoGenerateColumns = false;
            HFITOUT1.Refresh();
            HFITOUT1.DataSource = null;
            HFITOUT1.Rows.Clear();
            HFITOUT1.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            HFITOUT1.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            HFITOUT1.ColumnCount = 7;
            HFITOUT1.Columns[0].Name = "Itemname";
            HFITOUT1.Columns[1].Name = "Qty";
            HFITOUT1.Columns[2].Name = "Itemid";
            HFITOUT1.Columns[3].Name = "Type";
            HFITOUT1.Columns[4].Name = "Process";

            HFITOUT1.Columns[5].Name = "uid";
            HFITOUT1.Columns[6].Name = "workid";

            HFITOUT1.Columns[0].Width = 390;
            HFITOUT1.Columns[1].Width = 100;
            HFITOUT1.Columns[2].Visible = false;
            HFITOUT1.Columns[3].Visible = false;
            HFITOUT1.Columns[4].Visible = false;
            HFITOUT1.Columns[5].Visible = false;
            HFITOUT1.Columns[6].Visible = false;


        }
        private void btnadd_Click(object sender, EventArgs e)
        {
            if (HFIT.Rows.Count - 1 > 0)
            {
                for (int k = 0; k < HFIT.RowCount - 1; k++)

                {
                    if (HFIT.Rows[k].Cells[1].Value.ToString() == txtdcqty.Text)
                    {
                        MessageBox.Show("Seq No Already Exist");
                        return;

                    }

                }

            }




            if (txtaddnotes.Tag != "")
            {
                Genclass.strsql = "select processname,uid from processm1 where uid=" + txtaddnotes.Tag + "";

                Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
                DataTable tap1 = new DataTable();
                aptr1.Fill(tap1);


                if (tap1.Rows.Count > 0)
                {
                    Genclass.i = Genclass.i + 1;
                    Genclass.k = Genclass.k + 1;
                    var index = HFIT.Rows.Add();



                    HFIT.Rows[index].Cells[0].Value = Genclass.i;


                    HFIT.Rows[index].Cells[1].Value = txtdcqty.Text;

                    HFIT.Rows[index].Cells[2].Value = tap1.Rows[0]["processname"].ToString();
                    HFIT.Rows[index].Cells[3].Value = txtprocessdet.Text;
                    HFIT.Rows[index].Cells[4].Value = tap1.Rows[0]["uid"].ToString();
                    HFIT.Rows[index].Cells[5].Value = 0;
                    HFIT.Rows[index].Cells[6].Value = cbosGReturnItem.Text;
                    HFIT.Rows[index].Cells[7].Value = cbosGReturnItem.SelectedValue;
                    HFIT.Rows[index].Cells[8].Value = cbotype.Text;
                    var index1 = HFIT1.Rows.Add();

                    HFIT1.Rows[index1].Cells[0].Value = Genclass.k;


                    HFIT1.Rows[index1].Cells[1].Value = txtdcqty.Text;

                    HFIT1.Rows[index1].Cells[2].Value = tap1.Rows[0]["processname"].ToString();
                    HFIT1.Rows[index1].Cells[3].Value = txtprocessdet.Text;
                    HFIT1.Rows[index1].Cells[4].Value = tap1.Rows[0]["uid"].ToString();
                    HFIT1.Rows[index1].Cells[5].Value = 0;
                    HFIT1.Rows[index1].Cells[6].Value = cbosGReturnItem.Text;
                    HFIT1.Rows[index1].Cells[7].Value = cbosGReturnItem.SelectedValue;
                    HFIT1.Rows[index1].Cells[8].Value = cbotype.Text;

                    doc1 = Docno.NewRow();

                    Genclass.Barcode = HFIT1.Rows[index1].Cells[2].Value.ToString();
                    doc1["uid"] = HFIT1.Rows[index1].Cells[8].Value;
                    doc1["docno"] = Genclass.Barcode;
                    Docno.Rows.Add(doc1);
                    cboin.DataSource = Docno;
                    cboin.DisplayMember = "docno";
                    cboout.DataSource = Docno;
                    cboout.DisplayMember = "docno";

                    doc2 = Docno1.NewRow();
                    doc2["uid"] = HFIT1.Rows[index1].Cells[8].Value;
                    doc2["docno"] = HFIT1.Rows[index1].Cells[8].Value;
                    Docno1.Rows.Add(doc2);
                    type.DataSource = Docno1;
                    type.DisplayMember = "docno";
                    cbotypeout.DataSource = Docno1;
                    cbotypeout.DisplayMember = "docno";
                    //type.DisplayMember = "uid";



                    txtqty.Text = "";
                    txtdcqty.Text = "";
                    //txtqty.Text = "";
                    txtprocessdet.Text = "";
                    txtaddnotes.Text = "";
                }
            }

            txtdcqty.Focus();

        }

        private void txtaddnotes_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bsc.Filter = string.Format("processname LIKE '%{0}%' ", txtaddnotes.Text);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtaddnotes_Click(object sender, EventArgs e)
        {
            Genclass.type = 1;
            button22.Visible = false;
            DataTable dt = getParty();
            bsc.DataSource = dt;
            FillGrid1(dt, 1);
            Point loc = FindLocation(txtaddnotes);
            grNewSearch.Location = new Point(loc.X, loc.Y + 20);
            grNewSearch.Visible = true;
        }
        private Point FindLocation(Control ctrl)
        {
            if (ctrl.Parent is Form)
                return ctrl.Location;
            else
            {
                Point p = FindLocation(ctrl.Parent);
                p.X += ctrl.Location.X;
                p.Y += ctrl.Location.Y;
                return p;
            }
        }

        protected void FillGrid1(DataTable dt, int Fillid)
        {
            try
            {
                DataGridCommonNew.DataSource = null;
                DataGridCommonNew.AutoGenerateColumns = false;
                DataGridCommonNew.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
                DataGridCommonNew.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
              
                    DataGridCommonNew.ColumnCount = 3;
                    DataGridCommonNew.Columns[0].Name = "Uid";
                    DataGridCommonNew.Columns[0].HeaderText = "Uid";
                    DataGridCommonNew.Columns[0].DataPropertyName = "Uid";
                    DataGridCommonNew.Columns[1].Name = "OrderNO";
                    DataGridCommonNew.Columns[1].HeaderText = "OrderNO";
                    DataGridCommonNew.Columns[1].DataPropertyName = "OrderNO";
                    DataGridCommonNew.Columns[1].Width = 100;
                DataGridCommonNew.Columns[2].Name = "StyleName";
                DataGridCommonNew.Columns[2].HeaderText = "StyleName";
                DataGridCommonNew.Columns[2].DataPropertyName = "StyleName";
                DataGridCommonNew.Columns[2].Width = 200;
                DataGridCommonNew.DataSource = bsc;
                    DataGridCommonNew.Columns[0].Visible = false;
              
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        protected void FillGrid2(DataTable dt, int FillId)
        {
            try
            {
                HFITIN.DataSource = null;
                HFITIN.AutoGenerateColumns = false;


                HFITIN.ColumnCount = 2;
                HFITIN.Columns[0].Name = "Uid";
                HFITIN.Columns[0].HeaderText = "Uid";
                HFITIN.Columns[0].DataPropertyName = "Uid";

                HFITIN.Columns[1].Name = "Itemname";
                HFITIN.Columns[1].HeaderText = "ItemName";
                HFITIN.Columns[1].DataPropertyName = "Itemname";
                HFITIN.Columns[1].Width = 300;





                HFITIN.DataSource = IN;
                HFITIN.Columns[0].Visible = false;



            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        protected void FillGrid3(DataTable dt, int FillId)
        {
            try
            {
                HFITOUT.DataSource = null;
                HFITOUT.AutoGenerateColumns = false;


                HFITOUT.ColumnCount = 2;
                HFITOUT.Columns[0].Name = "Uid";
                HFITOUT.Columns[0].HeaderText = "Uid";
                HFITOUT.Columns[0].DataPropertyName = "Uid";

                HFITOUT.Columns[1].Name = "Itemname";
                HFITOUT.Columns[1].HeaderText = "ItemName";
                HFITOUT.Columns[1].DataPropertyName = "Itemname";
                HFITOUT.Columns[1].Width = 300;





                HFITOUT.DataSource = OUT;
                HFITOUT.Columns[0].Visible = false;



            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        protected DataTable getParty()
        {
            DataTable dt = new DataTable();
            try
            {


             
                    dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "sp_getorderno", conn);
                    bsc.DataSource = dt;
                



            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            return dt;
        }

        private void button18_Click(object sender, EventArgs e)
        {
            try
            {
                SelectId = 1;
                int Index = DataGridCommon.SelectedCells[0].RowIndex;

                txtaddnotes.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                txtaddnotes.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();



                grSearch.Visible = false;
                SelectId = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void DataGridCommon_DoubleClick(object sender, EventArgs e)
        {
            SelectId = 1;
            int Index = DataGridCommon.SelectedCells[0].RowIndex;

            txtaddnotes.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
            txtaddnotes.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();

            txtprocessdet.Focus();

            grSearch.Visible = false;
            SelectId = 0;
        }

        private void DataGridCommon_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SelectId = 1;
                int Index = DataGridCommon.SelectedCells[0].RowIndex;


                txtaddnotes.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                txtaddnotes.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                txtprocessdet.Focus();
                grSearch.Visible = false;
                SelectId = 0;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            HFIT1.AutoGenerateColumns = false;
            HFIT1.Refresh();
            HFIT1.DataSource = null;
            HFIT1.Rows.Clear();
            mode = 1;
            Genpan.Visible = false;
            Genclass.i = 0;
            Genclass.k = 0;
            panadd.Visible = false;
            Genclass.ClearTextBox(this, Editpan);
            Genclass.STR = "";
            Genclass.Gendocno();
            txtgrn.Text = Genclass.ST;
            Editpan.Visible = true;
            Genclass.sum1 = 0;
            Genclass.sum2 = 0;
            HFIPOUT.AutoGenerateColumns = true;
            HFIPOUT.Refresh();
            HFIPOUT.DataSource = null;
            HFIPOUT.Rows.Clear();

            HFITIN1.AutoGenerateColumns = true;
            HFITIN1.Refresh();
            HFITIN1.DataSource = null;
            HFITIN1.Rows.Clear();

            YARNLOAD.AutoGenerateColumns = true;
            YARNLOAD.Refresh();
            YARNLOAD.DataSource = null;
            YARNLOAD.Rows.Clear();
            conn.Close();
            conn.Open();
            qur.CommandText = "truncate table wotemp";
            qur.ExecuteNonQuery();
            qur.CommandText = "truncate table testtemp";
            qur.ExecuteNonQuery();
            qur.CommandText = "truncate table testyarn";
            qur.ExecuteNonQuery();
            qur.CommandText = "truncate table woprocess";
            qur.ExecuteNonQuery();
            
            txtaddnotes.Text = "";
            txtaddnotes.Text = "";
            txtprocessdet.Text = "";

            Docno.Clear();
            if (Docno.Columns.Count == 0)
            {
                Docno.Columns.Add("uid");
                Docno.Columns.Add("Docno");
            }

            Docno1.Clear();
            if (Docno1.Columns.Count == 0)
            {
                Docno1.Columns.Add("uid");
                Docno1.Columns.Add("Docno");
            }
            RQGR.Refresh();
            RQGR.DataSource = null;
            RQGR.Rows.Clear();
            HFIT.Refresh();
            HFIT.DataSource = null;
            HFIT.Rows.Clear();
            Titlep();
            Titlep1();
            dtpgrndt.Focus();
        }

        private void buttnext1_Click(object sender, EventArgs e)
        {

            this.Dispose();
            Docno.Dispose();
        }

        private void btnsave_Click(object sender, EventArgs e)
        {
            conn.Close();
            conn.Open();
            Genclass.Dtype = 520;

            if(HFITIN1.RowCount <= 1)
            {
                MessageBox.Show("Enter the Fabric Details");
                return;

            }
            //if (HFIT.RowCount <= 1)
            //{
            //    MessageBox.Show("Enter the Process Details");
            //    return;

            //}
            //if (YARNLOAD.RowCount <= 1)
            //{
            //    MessageBox.Show("Enter the yarn Details");
            //    return;

            //}
            if (mode == 1)
            {


                SqlParameter[] para ={
                       new SqlParameter("@UID","0"),
                    new SqlParameter("@DOCNO",txtgrn.Text),
                    new SqlParameter("@DOCDATE",Convert.ToDateTime(dtpgrndt.Text)),
                    new SqlParameter("@ref",txtdcno.Text),
                    new SqlParameter("@orderref",txtnar.Text),
                    new SqlParameter("@socno",textBox2.Text),

                    new SqlParameter("@Returnid",SqlDbType.Int),



                };

                para[6].Direction = ParameterDirection.Output;
                int ReturnId = db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_processdet", para, conn, 6);
                Genclass.h = ReturnId;
                for (int i = 0; i < HFIT1.RowCount - 1; i++)
                {

                    conn.Close();
                    conn.Open();

                    if (HFIT1.Rows[i].Cells[3].Value.ToString()=="")

                    {
                        HFIT1.Rows[i].Cells[3].Value = "0";

                    }
                    if (HFIT1.Rows[i].Cells[6].Value.ToString() == "")

                    {
                        HFIT1.Rows[i].Cells[6].Value = "0";

                    }

                    SqlParameter[] para1 ={

                    new SqlParameter("@slno",HFIT1.Rows[i].Cells[0].Value),
                    new SqlParameter("@seqno",HFIT1.Rows[i].Cells[1].Value),
                      new SqlParameter("@processid", HFIT1.Rows[i].Cells[7].Value),
                    new SqlParameter("@proceddet", HFIT1.Rows[i].Cells[3].Value),
                    new SqlParameter("@headid",Genclass.h ),
                    new SqlParameter("@prgrpid",HFIT1.Rows[i].Cells[6].Value ),
                    new SqlParameter("@type",cbotype.Text ),
                       new SqlParameter("@colour",HFIT1.Rows[i].Cells[4].Value ),
                    new SqlParameter("@itemid",HFIT1.Rows[i].Cells[10].Value ),

                   
                };

                    db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_processdetlist", para1, conn);
                }


                for (int i = 0; i < HFITIN1.RowCount - 1; i++)
                {



                    conn.Close();
                    conn.Open();
                    string qty = HFITIN1.Rows[i].Cells[1].Value.ToString();
                    if (qty == "")
                    {
                        qty = "0";
                    }
                    SqlParameter[] para1 ={

                    new SqlParameter("@workid",Genclass.h),
                    new SqlParameter("@itemid",HFITIN1.Rows[i].Cells[2].Value),
                      new SqlParameter("@type",HFITIN1.Rows[i].Cells[3].Value),
                    new SqlParameter("@process", HFITIN1.Rows[i].Cells[4].Value),

                    new SqlParameter("@qty", HFITIN1.Rows[i].Cells[1].Value ),
                    new SqlParameter("@itemname",HFITIN1.Rows[i].Cells[0].Value ),
                     new SqlParameter("@uom",HFITIN1.Rows[i].Cells[7].Value ),

                };

                    db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_workorderinM", para1, conn);
                }

                for (int i = 0; i < HFIPOUT.RowCount - 1; i++)
                {



                    conn.Close();
                    conn.Open();

                    SqlParameter[] para1 ={

                    new SqlParameter("@workid",Genclass.h),
                    new SqlParameter("@USEDID",HFIPOUT.Rows[i].Cells[0].Value),
                      new SqlParameter("@itemname", HFIPOUT.Rows[i].Cells[1].Value),
                    new SqlParameter("@qty", HFIPOUT.Rows[i].Cells[2].Value),

                    new SqlParameter("@itemid",HFIPOUT.Rows[i].Cells[3].Value ),



                };

                    db.ExecuteNonQuery(CommandType.StoredProcedure, "sp_OUTPUTITEMSW", para1, conn);
                }
                for (int i = 0; i < HFITOUT1.RowCount - 1; i++)
                {



                    conn.Close();
                    conn.Open();

                    SqlParameter[] para1 ={

                    new SqlParameter("@workid",Genclass.h),
                    new SqlParameter("@itemid",HFITOUT1.Rows[i].Cells[2].Value),
                      new SqlParameter("@type", HFITOUT1.Rows[i].Cells[3].Value),
                    new SqlParameter("@process", HFITOUT1.Rows[i].Cells[4].Value),

                    new SqlParameter("@qty",HFITOUT1.Rows[i].Cells[1].Value ),
                    new SqlParameter("@itemname",HFITOUT1.Rows[i].Cells[0].Value ),


                };


                    db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_workorderoutM", para1, conn);
                }
                    for (int i = 0; i < YARNLOAD.RowCount - 1; i++)
                    {



                        conn.Close();
                        conn.Open();

                        SqlParameter[] para1 ={

                  
                    new SqlParameter("@itemid",YARNLOAD.Rows[i].Cells[2].Value),

                    new SqlParameter("@FABRICID", YARNLOAD.Rows[i].Cells[3].Value),
                      new SqlParameter("@HEADID",Genclass.h),



                };

                        db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_SAVEWORKORDERYARN", para1, conn);
                    }
                for (int i = 0; i < YARNCOLORLOAD.RowCount - 1; i++)
                {



                    conn.Close();
                    conn.Open();

                    SqlParameter[] para1 ={

    
                    new SqlParameter("@itemid",YARNCOLORLOAD.Rows[i].Cells[3].Value),
                    new SqlParameter("@color",YARNCOLORLOAD.Rows[i].Cells[1].Value),
                    new SqlParameter("@PER", YARNCOLORLOAD.Rows[i].Cells[2].Value),
                    new SqlParameter("@HEADID",Genclass.h),



                };

                    db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_SAVEWORKORDERYARNcolor", para1, conn);
                }
            }
            else

            {

                if (mode == 2)
                {
                    qur.CommandText = "delete from processdetlist where headid=" + Genclass.h + "";
                    qur.ExecuteNonQuery();
                    qur.CommandText = "delete from workorderINM where WORKID=" + Genclass.h + "";
                    qur.ExecuteNonQuery();

                }

                SqlParameter[] para ={
                       new SqlParameter("@UID",Genclass.h),
                    new SqlParameter("@DOCNO",txtgrn.Text),
                    new SqlParameter("@DOCDATE",Convert.ToDateTime(dtpgrndt.Text)),
                    new SqlParameter("@ref",txtdcno.Text),
                    new SqlParameter("@orderref",txtnar.Text),

                                      new SqlParameter("@socno",textBox2.Text),


                };
                db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_processdetupdate", para, conn);

             

                for (int i = 0; i < HFIT1.RowCount - 1; i++)
                {

                    if (HFIT1.Rows[i].Cells[3].Value.ToString() == "")

                    {
                        HFIT1.Rows[i].Cells[3].Value = "0";

                    }
                    if (HFIT1.Rows[i].Cells[6].Value.ToString() == "")

                    {
                        HFIT1.Rows[i].Cells[6].Value = "0";

                    }

                    conn.Close();
                    conn.Open();

                    SqlParameter[] para1 ={

                       new SqlParameter("@slno",HFIT1.Rows[i].Cells[0].Value),
                    new SqlParameter("@seqno",HFIT1.Rows[i].Cells[1].Value),
                      new SqlParameter("@processid", HFIT1.Rows[i].Cells[7].Value),
                    new SqlParameter("@proceddet", HFIT1.Rows[i].Cells[3].Value),
                    new SqlParameter("@headid",Genclass.h ),
                    new SqlParameter("@prgrpid",HFIT1.Rows[i].Cells[6].Value ),
                    new SqlParameter("@type",cbotype.Text ),
                       new SqlParameter("@colour",HFIT1.Rows[i].Cells[4].Value ),
                    new SqlParameter("@itemid",HFIT1.Rows[i].Cells[10].Value ),



                };

                    db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_processdetlist", para1, conn);
                }
                for (int i = 0; i < HFITIN1.RowCount - 1; i++)
                {



                    conn.Close();
                    conn.Open();

                    SqlParameter[] para1 ={

                   new SqlParameter("@workid",Genclass.h),
                    new SqlParameter("@itemid",HFITIN1.Rows[i].Cells[2].Value),
                       new SqlParameter("@type",HFITIN1.Rows[i].Cells[3].Value),
                    new SqlParameter("@process", HFITIN1.Rows[i].Cells[4].Value),

                    new SqlParameter("@qty", HFITIN1.Rows[i].Cells[1].Value ),
                    new SqlParameter("@itemname",HFITIN1.Rows[i].Cells[0].Value ),
                     new SqlParameter("@uom",HFITIN1.Rows[i].Cells[7].Value ),

                };

                    db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_workorderinM", para1, conn);
                }


            }

            if (mode == 1)
            {
                qur.CommandText = "update doctypem set lastno= lastno + 1 where doctypeid=" + Genclass.Dtype + "  and finyear='19-20'";
                qur.ExecuteNonQuery();


            }
            MessageBox.Show("Records Saved");
            Editpan.Visible = false;
            Genpan.Visible = true;
            panadd.Visible = true;
            LoadGetJobCard(1);

        }

        private void butedit_Click(object sender, EventArgs e)
        {
            HFIT1.AutoGenerateColumns = false;
            HFIT1.Refresh();
            HFIT1.DataSource = null;
            HFIT1.Rows.Clear();
            mode = 2;
            int Index = HFGP.SelectedCells[0].RowIndex;
            Genclass.h = Convert.ToInt32(HFGP.Rows[Index].Cells[0].Value.ToString());
            txtgrn.Text = HFGP.Rows[Index].Cells[1].Value.ToString();
            dtpgrndt.Text = HFGP.Rows[Index].Cells[2].Value.ToString();
            txtdcno.Text = HFGP.Rows[Index].Cells[4].Value.ToString();
            txtnar.Text = HFGP.Rows[Index].Cells[3].Value.ToString();
            cbotype.Text = HFGP.Rows[Index].Cells[5].Value.ToString();
            textBox2.Text = HFGP.Rows[Index].Cells[6].Value.ToString();
            Genpan.Visible = false;
            panadd.Visible = false;
            Editpan.Visible = true;
            tabControl1.TabPages.Remove(tabPage4);
            HFIPOUT.AutoGenerateColumns = true;
            HFIPOUT.Refresh();
            HFIPOUT.DataSource = null;
            HFIPOUT.Rows.Clear();
            Genclass.i = 0;
            Genclass.k = 0;
            conn.Close();
            conn.Open();
            Docno1.Clear();
            TitlepCOLORLOAD();
            if (Docno1.Columns.Count == 0)
            {
                Docno1.Columns.Add("uid");
                Docno1.Columns.Add("Docno");
            }
            Docno.Clear();
            if (Docno.Columns.Count == 0)
            {
                Docno.Columns.Add("uid");
                Docno.Columns.Add("Docno");
            }

        
            conn.Close();
            conn.Open();
            qur.CommandText = "truncate table wotemp";
            qur.ExecuteNonQuery();
            qur.CommandText = "truncate table testtemp";
            qur.ExecuteNonQuery();
            qur.CommandText = "truncate table testyarn";
            qur.ExecuteNonQuery();
            txtaddnotes.Text = "";
            txtprocessdet.Text = "";
            HFIT1.Refresh();
            HFIT1.DataSource = null;
            HFIT1.Rows.Clear();
            HFIT1.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            HFIT1.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            Genclass.strsql = "SP_GETWORKORDEREDITTOT  " + Genclass.h + ",'"+ cbotype.Text +"' ";
            Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
            SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
            DataTable tap1 = new DataTable();
            aptr1.Fill(tap1);

            if (tap1.Rows.Count > 0)
            {
                Genclass.i = 0;

                for (int k = 0; k < tap1.Rows.Count; k++)
                {
                    var index = HFIT1.Rows.Add();
                    HFIT1.Rows[index].Cells[0].Value = tap1.Rows[k]["slno"].ToString();
                    HFIT1.Rows[index].Cells[1].Value = tap1.Rows[k]["seqno"].ToString();
                    HFIT1.Rows[index].Cells[2].Value = tap1.Rows[k]["processname"].ToString();
                    HFIT1.Rows[index].Cells[3].Value = tap1.Rows[k]["proceddet"].ToString();
                    HFIT1.Rows[index].Cells[4].Value = tap1.Rows[k]["Colour"].ToString();
                    HFIT1.Rows[index].Cells[5].Value = "0";
                    HFIT1.Rows[index].Cells[6].Value = tap1.Rows[k]["processgroup"].ToString();
                    HFIT1.Rows[index].Cells[7].Value = tap1.Rows[k]["processid"].ToString();
                    HFIT1.Rows[index].Cells[8].Value = tap1.Rows[k]["type"].ToString();
                    HFIT1.Rows[index].Cells[9].Value = tap1.Rows[k]["uid"].ToString();
                    HFIT1.Rows[index].Cells[10].Value = tap1.Rows[k]["itemid"].ToString();

                    Genclass.k = tap1.Rows.Count;
                }


            }
            
            loadIN();
            //loadINOUT();
            ////loadOUT();
            //loadYARN();
            //loadYARNCOLOR();
            loadtit();



        }

        private void loadYARN()
        {
            YARNLOAD.Refresh();
            YARNLOAD.DataSource = null;
            YARNLOAD.Rows.Clear();
            YARNLOAD.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            YARNLOAD.DefaultCellStyle.WrapMode = DataGridViewTriState.True;

            Genclass.strsql1 = "SP_GETWORKORDERYARN " + Genclass.h + "   ";
            Genclass.cmd = new SqlCommand(Genclass.strsql1, conn);
            SqlDataAdapter aptr2 = new SqlDataAdapter(Genclass.cmd);
            DataTable tap2 = new DataTable();
            aptr2.Fill(tap2);

            YARNLOAD.AutoGenerateColumns = false;
            YARNLOAD.Refresh();
            YARNLOAD.DataSource = null;
            YARNLOAD.Rows.Clear();

            YARNLOAD.ColumnCount = tap2.Columns.Count + 1;
            Genclass.i = 1;
            foreach (DataColumn column in tap2.Columns)
            {
                YARNLOAD.Columns[Genclass.i].Name = column.ColumnName;
                YARNLOAD.Columns[Genclass.i].HeaderText = column.ColumnName;
                YARNLOAD.Columns[Genclass.i].DataPropertyName = column.ColumnName;
                Genclass.i = Genclass.i + 1;
            }

            YARNLOAD.Columns[1].Width = 340;
            YARNLOAD.Columns[2].Visible = false;
            YARNLOAD.Columns[3].Visible = false;
            YARNLOAD.Columns[4].Visible = false;

            YARNLOAD.DataSource = tap2;
        }
        private void loadYARNCOLOR()
        {
            YARNCOLORLOAD.Refresh();
            YARNCOLORLOAD.DataSource = null;
            YARNCOLORLOAD.Rows.Clear();
            YARNCOLORLOAD.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            YARNCOLORLOAD.DefaultCellStyle.WrapMode = DataGridViewTriState.True;

            Genclass.strsql1 = "SP_GETWORKORDERYARNcolor " + Genclass.h + "   ";
            Genclass.cmd = new SqlCommand(Genclass.strsql1, conn);
            SqlDataAdapter aptr2 = new SqlDataAdapter(Genclass.cmd);
            DataTable tap2 = new DataTable();
            aptr2.Fill(tap2);

            if (tap2.Rows.Count > 0)
            {
                for (int k = 0; k < tap2.Rows.Count; k++)
                {
                    var index = YARNCOLORLOAD.Rows.Add();
                    YARNCOLORLOAD.Rows[index].Cells[0].Value = tap2.Rows[k]["ITEMNAME"].ToString();
                    YARNCOLORLOAD.Rows[index].Cells[1].Value = tap2.Rows[k]["COLOR"].ToString();
                    YARNCOLORLOAD.Rows[index].Cells[2].Value = tap2.Rows[k]["PER"].ToString();
                    YARNCOLORLOAD.Rows[index].Cells[3].Value = tap2.Rows[k]["ITEMID"].ToString();
                    YARNCOLORLOAD.Rows[index].Cells[4].Value = tap2.Rows[k]["UID"].ToString();



                }

            }
        }
        private void loadtit()
        {
            HFIT.Refresh();
            HFIT.DataSource = null;
            HFIT.Rows.Clear();
            HFIT.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            HFIT.DefaultCellStyle.WrapMode = DataGridViewTriState.True;

            Genclass.strsql1 = "SP_GETWORKORDEREDITTOT  " + Genclass.h + ",'" + cbotype.Text + "'";
            Genclass.cmd = new SqlCommand(Genclass.strsql1, conn);
            SqlDataAdapter aptr2 = new SqlDataAdapter(Genclass.cmd);
            DataTable tap2 = new DataTable();
            aptr2.Fill(tap2);

            if (tap2.Rows.Count > 0)
            {
                Genclass.sum1 = 0;

                for (int k = 0; k < tap2.Rows.Count; k++)
                {
                    var index = HFIT.Rows.Add();
                    HFIT.Rows[index].Cells[0].Value = tap2.Rows[k]["slno"].ToString();
                    HFIT.Rows[index].Cells[1].Value = tap2.Rows[k]["seqno"].ToString();
                    HFIT.Rows[index].Cells[2].Value = tap2.Rows[k]["processname"].ToString();
                    HFIT.Rows[index].Cells[3].Value = tap2.Rows[k]["proceddet"].ToString();
                    HFIT.Rows[index].Cells[4].Value = tap2.Rows[k]["Colour"].ToString();
                    HFIT.Rows[index].Cells[5].Value = "0";
                    HFIT.Rows[index].Cells[6].Value = tap2.Rows[k]["processgroup"].ToString();
                    HFIT.Rows[index].Cells[7].Value = tap2.Rows[k]["processid"].ToString();
                    HFIT.Rows[index].Cells[8].Value = tap2.Rows[k]["type"].ToString();
                    HFIT.Rows[index].Cells[9].Value = tap2.Rows[k]["uid"].ToString();
                    HFIT.Rows[index].Cells[10].Value = tap2.Rows[k]["itemid"].ToString();
                    Genclass.i = Genclass.k;
                }

            }
        }
        private void loadIN()
        {
            HFITIN1.Refresh();
            HFITIN1.DataSource = null;
            HFITIN1.Rows.Clear();
            HFITIN1.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            HFITIN1.DefaultCellStyle.WrapMode = DataGridViewTriState.True;

            Genclass.strsql1 = "SP_GETWORKORDEREDITINITEM " + Genclass.h + "   ";
            Genclass.cmd = new SqlCommand(Genclass.strsql1, conn);
            SqlDataAdapter aptr2 = new SqlDataAdapter(Genclass.cmd);
            DataTable tap2 = new DataTable();
            aptr2.Fill(tap2);

            if (tap2.Rows.Count > 0)
            {
                for (int k = 0; k < tap2.Rows.Count; k++)
                {
                    var index = HFITIN1.Rows.Add();
                    HFITIN1.Rows[index].Cells[0].Value = tap2.Rows[k]["ITEMNAME"].ToString();
                    HFITIN1.Rows[index].Cells[1].Value = tap2.Rows[k]["QTY"].ToString();
                    HFITIN1.Rows[index].Cells[2].Value = tap2.Rows[k]["ITEMID"].ToString();
                    HFITIN1.Rows[index].Cells[3].Value = tap2.Rows[k]["TYPE"].ToString();
                    HFITIN1.Rows[index].Cells[4].Value = tap2.Rows[k]["PROCESS"].ToString();
                    HFITIN1.Rows[index].Cells[5].Value = tap2.Rows[k]["UID"].ToString();
                    HFITIN1.Rows[index].Cells[6].Value = tap2.Rows[k]["WORKID"].ToString();
                    HFITIN1.Rows[index].Cells[7].Value = tap2.Rows[k]["uom"].ToString();

                }

            }
        }
        private void loadINOUT()
        {








        }
        private void loadOUT()
        {
            HFITOUT1.Refresh();
            HFITOUT1.DataSource = null;
            HFITOUT1.Rows.Clear();

            Genclass.strsql1 = "select  B.ITEMNAME,B.QTY,B.ITEMID,B.TYPE,B.PROCESS,B.UID,B.WORKID from processdet a  INNER JOIN workorderoutM B ON A.UID=B.WORKID where a.uid=" + Genclass.h + "   order by B.UID DESC";
            Genclass.cmd = new SqlCommand(Genclass.strsql1, conn);
            SqlDataAdapter aptr2 = new SqlDataAdapter(Genclass.cmd);
            DataTable tap2 = new DataTable();
            aptr2.Fill(tap2);

            if (tap2.Rows.Count > 0)
            {
                for (int k = 0; k < tap2.Rows.Count; k++)
                {
                    var index = HFITOUT1.Rows.Add();
                    HFITOUT1.Rows[index].Cells[0].Value = tap2.Rows[k]["ITEMNAME"].ToString();
                    HFITOUT1.Rows[index].Cells[1].Value = tap2.Rows[k]["QTY"].ToString();
                    HFITOUT1.Rows[index].Cells[2].Value = tap2.Rows[k]["ITEMID"].ToString();
                    HFITOUT1.Rows[index].Cells[3].Value = tap2.Rows[k]["TYPE"].ToString();
                    HFITOUT1.Rows[index].Cells[4].Value = tap2.Rows[k]["PROCESS"].ToString();
                    HFITOUT1.Rows[index].Cells[5].Value = tap2.Rows[k]["UID"].ToString();
                    HFITOUT1.Rows[index].Cells[6].Value = tap2.Rows[k]["WORKID"].ToString();


                }

            }
        }

        private void btnaddrcan_Click(object sender, EventArgs e)
        {
            Editpan.Visible = false;
            Genpan.Visible = true;
            panadd.Visible = true;
            LoadGetJobCard(1);
        }

        private void button9_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dtpgrndt_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                e.Handled = true;
                SendKeys.Send("{TAB}");
            }
        }

        private void txtdcno_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                e.Handled = true;
                SendKeys.Send("{TAB}");
            }
        }

        private void txtnar_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                e.Handled = true;
                SendKeys.Send("{TAB}");
            }
        }

        private void txtdcqty_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                e.Handled = true;
                SendKeys.Send("{TAB}");
            }
        }

        private void txtaddnotes_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                e.Handled = true;
                SendKeys.Send("{TAB}");
            }
        }

        private void txtprocessdet_KeyDown(object sender, KeyEventArgs e)
        {

        }

        private void cbosGReturnItem_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                e.Handled = true;
                SendKeys.Send("{TAB}");
            }
        }

        private void btnadd_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                e.Handled = true;
                SendKeys.Send("{TAB}");
            }
        }

        private void txtprocessdet_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                e.Handled = true;
                SendKeys.Send("{TAB}");
            }
        }

        private void txtaddnotes_KeyDown(object sender, KeyEventArgs e)
        {
           
                try
                {
                    if (e.KeyCode == Keys.F10)
                    {
                        DataGridCommonNew.Visible = false;
                    }
                    else if (e.KeyCode == Keys.F2)
                    {
                    FillId = 1;
                        SelectId = 1;
                        int Index = DataGridCommon.SelectedCells[0].RowIndex;
                        txtaddnotes.Text = DataGridCommonNew.Rows[Index].Cells[1].Value.ToString();
                        txtaddnotes.Tag = DataGridCommonNew.Rows[Index].Cells[0].Value.ToString();
                        txtprocessdet.Focus();
                        grNewSearch.Visible = false;
                        SelectId = 0;
                    }
                    else if (e.KeyCode == Keys.Escape)
                    {
                        DataGridCommonNew.Visible = false;
                    }

                    else if (e.KeyValue == 40)
                    {
                        DataGridCommonNew.Select();
                    }
                    else if (e.KeyCode == Keys.Enter)
                    {
                        txtaddnotes_Click(sender, e);
                    }
                e.Handled = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnHide_Click(object sender, EventArgs e)
        {
            grSearch.Visible = false;
        }

        private void cbotype_SelectedIndexChanged(object sender, EventArgs e)
        {
           
            conn.Close();
            conn.Open();
            qur.CommandText = "truncate table     woprocess ";
            qur.ExecuteNonQuery();

            if (cbotype.Text=="Yarn" && textBox2.Text!="")
            {
                tabPage3.Text = "Yarn Details";
                
                string qur = "exec sp_getwoitemyarn " + textBox2.Tag + ",'" + textBox2.Text + "'";
                SqlCommand cmd = new SqlCommand(qur, conn);
                SqlDataAdapter apt = new SqlDataAdapter(cmd);
                DataTable tab = new DataTable();
                apt.Fill(tab);
                cboitem.DataSource = null;
                cboitem.DataSource = tab;
                cboitem.DisplayMember = "itemname";
                cboitem.ValueMember = "uid";
                cboitem.SelectedIndex = -1;
                loadyarn();

            }
            else if (cbotype.Text == "Fabric" && textBox2.Text != "")
            {
                tabPage3.Text = "Fabric Details";

                string qur = "exec sp_getwoitemfabric  "+ textBox2.Tag +",'"+ textBox2.Text + "'";
                SqlCommand cmd = new SqlCommand(qur, conn);
                SqlDataAdapter apt = new SqlDataAdapter(cmd);
                DataTable tab = new DataTable();
                apt.Fill(tab);
                cboitem.DataSource = null;
                cboitem.DataSource = tab;
                cboitem.DisplayMember = "itemname";
                cboitem.ValueMember = "uid";
                cboitem.SelectedIndex = -1;
                loadprocess();

            }

          
        

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button5_Click(object sender, EventArgs e)
        {
            Genclass.strsql = "select itemname,uid from iTEMM where uid=" + TXTIN.Tag + "";

            Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
            SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
            DataTable tap1 = new DataTable();
            aptr1.Fill(tap1);


            if (tap1.Rows.Count > 0)
            {

                var index = HFITIN1.Rows.Add();



                HFITIN1.Rows[index].Cells[0].Value = TXTIN.Text;


                HFITIN1.Rows[index].Cells[1].Value = TXTINQTY.Text;
                txtsamqty.Text = TXTINQTY.Text;
                HFITIN1.Rows[index].Cells[2].Value = TXTIN.Tag;
                HFITIN1.Rows[index].Cells[3].Value = 0; ;
                HFITIN1.Rows[index].Cells[4].Value = 0; ;
                HFITIN1.Rows[index].Cells[5].Value = 0;
                HFITIN1.Rows[index].Cells[6].Value = 0;


                TXTIN.Text = "";
                TXTINQTY.Text = "0";

                conn.Close();
                conn.Open();

                qur.CommandText = "insert into wotemp values (" + TXTIN.Tag + ")";
                qur.ExecuteNonQuery();

            }
            funoutload();
        }


        private void cboout_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void TXTIN_Click(object sender, EventArgs e)
        {
            //Genclass.type = 2;
            //button22.Visible = true;
            //type.Text = "Fabric";
            //Genclass.strsql = "select distinct  a.uid,itemspec1 from itemm a inner join itemsmbom b on a.uid=b.itemm_uid where a.itemgroup_uid=67 and active=1";
            //Genclass.FSSQLSortStr = "itemspec1";
            //Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
            //SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
            //DataTable tap = new DataTable();
            //aptr.Fill(tap);
            //bsFabric.DataSource = tap;
            //FillGrid1(tap, 2);
            //Point loc = FindLocation(TXTIN);
            //grNewSearch.Location = new Point(loc.X, loc.Y + 20);
            //grNewSearch.Visible = true;
        }

        private void TXTIN_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bsFabric.Filter = string.Format("itemspec1 LIKE '%{0}%' ", TXTIN.Text);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void TXTOUT_Click(object sender, EventArgs e)
        {

            Genclass.type = 3;
            if (cbotypeout.Text == "Yarn")
            {
                DataTable dt = getParty();
                OUT.DataSource = dt;
                FillGrid3(dt, 1);
                Point loc = FindLocation(TXTOUT);
                Pnlout.Location = new Point(loc.X, loc.Y + 20);
                Pnlout.Visible = true;
                Pnlout.Text = "Itemname Search";
            }
            else
            {
                Pnlout.Visible = true;
                if (cbotypeout.Text == "Fabric")
                {
                    Genclass.strsql = "select distinct  b.uid ,(b.itemspec1 + '  ' +'" + cbotypeout.Text + "' ) as ITEMNAME  from itemsmbom a inner join itemm b on a.useditem_uid=b.uid inner join generalm c on b.uom_uid=c.uid inner join   wotemp d on a.itemm_uid=d.itemid    ";
                    //Genclass.strsql = "SELECT   b.itemid AS UID ,(d.itemname + '  ' +'" + textBox16.Text + "' ) AS ITEMNAME,H.UID AS UOMID,H.GENERALNAME from jo a inner join   JOBISSUEDETTAB  b on a.uid=B.HEADID   and a.doctypeid=540  		   	 left join itemm c on b.Itemid=c.uid   left join generalm h on c.uom_uid=h.uid     left join JORKNITREC d on b.uid=d.REFID 	 left join jo e on d.HEADID=e.uid  and e.Doctypeid=550     left join partym k  on a.supplieruid=k.uid 	where a.remarks='" +  txtitem.Text  + "' 	   group by b.qty,b.Itemid,d.itemname,h.generalname,h.uid   ";
                    Genclass.FSSQLSortStr = "b.itemspec1";

                }
                else if (cbotypeout.Text == "Component")
                {
                    Genclass.strsql = "select distinct  b.uid ,(b.itemspec1 + '  ' +'" + cbotypeout.Text + "' ) as ITEMNAME  from itemsmbom a inner join itemm b on a.useditem_uid=b.uid inner join generalm c on b.uom_uid=c.uid inner join   wotemp d on a.itemm_uid=d.itemid     ";
                    //Genclass.strsql = "SELECT   b.itemid AS UID ,(d.itemname + '  ' +'" + textBox16.Text + "' ) AS ITEMNAME,H.UID AS UOMID,H.GENERALNAME from jo a inner join   JOBISSUEDETTAB  b on a.uid=B.HEADID   and a.doctypeid=540  		   	 left join itemm c on b.Itemid=c.uid   left join generalm h on c.uom_uid=h.uid     left join JORKNITREC d on b.uid=d.REFID 	 left join jo e on d.HEADID=e.uid  and e.Doctypeid=550     left join partym k  on a.supplieruid=k.uid 	where a.remarks='" +  txtitem.Text  + "' 	   group by b.qty,b.Itemid,d.itemname,h.generalname,h.uid   ";
                    Genclass.FSSQLSortStr = "b.itemspec1";

                }
                else if (cbotypeout.Text == "Garment")
                {
                    Genclass.strsql = "select distinct  b.uid ,(b.itemspec1 + '  ' +'" + cbotypeout.Text + "' ) as ITEMNAME  from itemsmbom a inner join itemm b on a.useditem_uid=b.uid inner join generalm c on b.uom_uid=c.uid inner join   wotemp d on a.itemm_uid=d.itemid     ";
                    //Genclass.strsql = "SELECT   b.itemid AS UID ,(d.itemname + '  ' +'" + textBox16.Text + "' ) AS ITEMNAME,H.UID AS UOMID,H.GENERALNAME from jo a inner join   JOBISSUEDETTAB  b on a.uid=B.HEADID   and a.doctypeid=540  		   	 left join itemm c on b.Itemid=c.uid   left join generalm h on c.uom_uid=h.uid     left join JORKNITREC d on b.uid=d.REFID 	 left join jo e on d.HEADID=e.uid  and e.Doctypeid=550     left join partym k  on a.supplieruid=k.uid 	where a.remarks='" +  txtitem.Text  + "' 	   group by b.qty,b.Itemid,d.itemname,h.generalname,h.uid   ";
                    Genclass.FSSQLSortStr = "b.itemspec1";

                }
                else if (cbotypeout.Text == "Trims")
                {
                    Genclass.strsql = "select distinct  b.uid ,(b.itemspec1 + '  ' +'" + cbotypeout.Text + "' ) as ITEMNAME  from itemsmbom a inner join itemm b on a.useditem_uid=b.uid inner join generalm c on b.uom_uid=c.uid inner join   wotemp d on a.itemm_uid=d.itemid      ";
                    //Genclass.strsql = "SELECT   b.itemid AS UID ,(d.itemname + '  ' +'" + textBox16.Text + "' ) AS ITEMNAME,H.UID AS UOMID,H.GENERALNAME from jo a inner join   JOBISSUEDETTAB  b on a.uid=B.HEADID   and a.doctypeid=540  		   	 left join itemm c on b.Itemid=c.uid   left join generalm h on c.uom_uid=h.uid     left join JORKNITREC d on b.uid=d.REFID 	 left join jo e on d.HEADID=e.uid  and e.Doctypeid=550     left join partym k  on a.supplieruid=k.uid 	where a.remarks='" +  txtitem.Text  + "' 	   group by b.qty,b.Itemid,d.itemname,h.generalname,h.uid   ";
                    Genclass.FSSQLSortStr = "b.itemspec1";

                }


                Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
                DataTable tap = new DataTable();
                aptr.Fill(tap);
                OUT.DataSource = tap;



                HFITOUT.DataSource = null;
                HFITOUT.AutoGenerateColumns = false;


                HFITOUT.ColumnCount = 2;
                HFITOUT.Columns[0].Name = "Uid";
                HFITOUT.Columns[0].HeaderText = "Uid";
                HFITOUT.Columns[0].DataPropertyName = "Uid";

                HFITOUT.Columns[1].Name = "Itemname";
                HFITOUT.Columns[1].HeaderText = "ItemName";
                HFITOUT.Columns[1].DataPropertyName = "Itemname";
                HFITOUT.Columns[1].Width = 300;





                HFITOUT.DataSource = OUT;
                HFITOUT.Columns[0].Visible = false;





                conn.Close();



            }
        }

        private void button18_Click_1(object sender, EventArgs e)
        {
            button18_Click(sender, e);
        }

        private void btnHide_Click_1(object sender, EventArgs e)
        {
            grSearch.Visible = false;
        }

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void tabPage4_Click(object sender, EventArgs e)
        {

        }

        private void dataGridView3_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void cboin_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboin.SelectedValue != "")
            {



            }
        }

        private void cboin_Click(object sender, EventArgs e)
        {
            //if (HFIT.RowCount > 1)
            //{
            //    Genclass.strsql = "select uid,isnull(b.processname,'') + ' - ' +  isnull(a.type,'')  as Name from wotemp a inner join processm1 b on a.process=b.processname";
            //    Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
            //    SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
            //    DataTable tap = new DataTable();
            //    aptr.Fill(tap);

            //    if (tap.Rows.Count > 0)
            //    {
            //        cboin.DataSource = null;
            //        cboin.DataSource = tap;
            //        cboin.DisplayMember = "Name";
            //        cboin.ValueMember = "uid";
            //        cboin.SelectedIndex = -1;
            //        conn.Close();

            //    }


            //}
        }

        private void button8_Click(object sender, EventArgs e)
        {
            Genclass.strsql = "select itemname,uid from iTEMM where uid=" + TXTIN.Tag + "";

            Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
            SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
            DataTable tap1 = new DataTable();
            aptr1.Fill(tap1);


            if (tap1.Rows.Count > 0)
            {

                var index = HFITOUT1.Rows.Add();



                HFITOUT1.Rows[index].Cells[0].Value = TXTOUT.Text;


                HFITOUT1.Rows[index].Cells[1].Value = TXTOUTQTY.Text;
                HFITOUT1.Rows[index].Cells[2].Value = TXTOUT.Tag;
                HFITOUT1.Rows[index].Cells[3].Value = type.Text;
                HFITOUT1.Rows[index].Cells[4].Value = cboout.Text;
                HFITOUT1.Rows[index].Cells[5].Value = 0;
                HFITOUT1.Rows[index].Cells[6].Value = 0;

            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Pnlin.Visible = false;
        }

        private void button7_Click(object sender, EventArgs e)
        {
            Pnlout.Visible = false;
        }

        private void button6_Click(object sender, EventArgs e)
        {
            try
            {
                SelectId = 1;
                int Index = HFITOUT.SelectedCells[0].RowIndex;

                TXTOUT.Text = HFITOUT.Rows[Index].Cells[1].Value.ToString();
                TXTOUT.Tag = HFITOUT.Rows[Index].Cells[0].Value.ToString();



                Pnlout.Visible = false;
                SelectId = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
               
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
        private void funoutload()
        {

            conn.Close();
            conn.Open();


            if(txtsamqty.Text=="")
            {
                txtsamqty.Text = "0";
            }
           
            //string quy = "select distinct d.itemname,f.generalname as uom,b.prate,c.pqty as qty,c.dcqty,c.itemuid,c.pqty as aqty,isnull(c.regqty, 0) as regqty,c.basicvalue,b.addnotes ,g.docno as refno,b.disp,b.disval,0 as edid,0 as edval,0 as edname,b.addper,b.addduty,b.vatid,b.vatval,n.generalname as vatname,b.totvalue,'' as tallname,'' as tallytaxid , c.tag1,'' as edledname,'' as edledid,'' as vatledname,'' as vatledid  from transactionsp a inner join transactionsplist b on a.uid = b.transactionspuid   and a.doctypeid = 110  left join transactionsplist c  on a.docno = c.refno   and b.itemuid = c.itemuid left join transactionsp g on g.uid = c.transactionspuid and c.doctypeid = 125   left join itemm d on c.itemuid = d.uid  inner join partym e on g.partyuid = e.uid   left join generalm f on d.uom_uid = f.uid left join generalm n  on b.vatid = n.uid   left join transactionsplist o on g.docno = o.refno and b.itemuid = o.itemuid and o.doctypeid = 450 where o.refno Is Null And g.active = 1 and   g.docno = '" + txtgrnrefno.Text  +"'";
            qur.CommandText = "insert into  testtemp  select a.uid,a.itemspec1,((" + txtsamqty.Text + " * (b.usedqty))/100 )  as Qty,b.itemm_uid from itemm   a inner join itemsmbom b on a.uid=b.useditem_uid  where a.itemgroup_uid=66 and b.itemm_uid=" + TXTIN.Tag + " ";
            qur.ExecuteNonQuery();

            string quy = "select * from testtemp";
            Genclass.cmd = new SqlCommand(quy, conn);


            SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
            DataTable tap = new DataTable();
            aptr.Fill(tap);

            HFIPOUT.AutoGenerateColumns = true;
            HFIPOUT.Refresh();
            HFIPOUT.DataSource = null;
            HFIPOUT.Rows.Clear();


            HFIPOUT.ColumnCount = tap.Columns.Count;
            Genclass.i = 0;
            foreach (DataColumn column in tap.Columns)
            {
                HFIPOUT.Columns[Genclass.i].Name = column.ColumnName;
                HFIPOUT.Columns[Genclass.i].HeaderText = column.ColumnName;
                HFIPOUT.Columns[Genclass.i].DataPropertyName = column.ColumnName;
                Genclass.i = Genclass.i + 1;
            }




            HFIPOUT.Columns[0].Visible = false;
            HFIPOUT.Columns[1].Width = 340;
            HFIPOUT.Columns[2].Width = 100;
            HFIPOUT.Columns[3].Visible = false;



            HFIPOUT.DataSource = tap;



        }
        private void funyarnload()
        {

            conn.Close();
            conn.Open();


            if (txtsamqty.Text == "")
            {
                txtsamqty.Text = "0";
            }
            if (cbofabric.Text != "" && cbofabric.ValueMember != "" && cbofabric.SelectedValue != null)
            {
                //string quy = "select distinct d.itemname,f.generalname as uom,b.prate,c.pqty as qty,c.dcqty,c.itemuid,c.pqty as aqty,isnull(c.regqty, 0) as regqty,c.basicvalue,b.addnotes ,g.docno as refno,b.disp,b.disval,0 as edid,0 as edval,0 as edname,b.addper,b.addduty,b.vatid,b.vatval,n.generalname as vatname,b.totvalue,'' as tallname,'' as tallytaxid , c.tag1,'' as edledname,'' as edledid,'' as vatledname,'' as vatledid  from transactionsp a inner join transactionsplist b on a.uid = b.transactionspuid   and a.doctypeid = 110  left join transactionsplist c  on a.docno = c.refno   and b.itemuid = c.itemuid left join transactionsp g on g.uid = c.transactionspuid and c.doctypeid = 125   left join itemm d on c.itemuid = d.uid  inner join partym e on g.partyuid = e.uid   left join generalm f on d.uom_uid = f.uid left join generalm n  on b.vatid = n.uid   left join transactionsplist o on g.docno = o.refno and b.itemuid = o.itemuid and o.doctypeid = 450 where o.refno Is Null And g.active = 1 and   g.docno = '" + txtgrnrefno.Text  +"'";
                qur.CommandText = "insert into  testyarn  select distinct a.uid,a.itemspec1,b.itemm_uid,0 as Qty from itemm   a inner join itemsmbom b on a.uid=b.useditem_uid  where a.itemgroup_uid=66 and b.itemm_uid=" + cbofabric.SelectedValue + " ";
                qur.ExecuteNonQuery();
            }

       
            string quy = "select itemname AS Item,itemid as Uid,uid as fabricid,0 as ii from testyarn";
            Genclass.cmd = new SqlCommand(quy, conn);


            SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
            DataTable tap = new DataTable();
            aptr.Fill(tap);
            YARNLOAD.AutoGenerateColumns = false;
            YARNLOAD.Refresh();
            YARNLOAD.DataSource = null;
            YARNLOAD.Rows.Clear();

            YARNLOAD.ColumnCount = tap.Columns.Count + 1;
            Genclass.i = 1;
            foreach (DataColumn column in tap.Columns)
            {
                YARNLOAD.Columns[Genclass.i].Name = column.ColumnName;
                YARNLOAD.Columns[Genclass.i].HeaderText = column.ColumnName;
                YARNLOAD.Columns[Genclass.i].DataPropertyName = column.ColumnName;
                Genclass.i = Genclass.i + 1;
            }

            YARNLOAD.Columns[1].Width = 340;
            YARNLOAD.Columns[2].Visible = false;
            YARNLOAD.Columns[3].Visible = false;
            YARNLOAD.Columns[4].Visible = false;

            YARNLOAD.DataSource = tap;



        }
        private void funoutload1()
        {

            conn.Close();
            conn.Open();
      

            string quy = "select * from testtemp ";
            Genclass.cmd = new SqlCommand(quy, conn);


            SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
            DataTable tap = new DataTable();
            aptr.Fill(tap);

            HFIPOUT.AutoGenerateColumns = true;
            HFIPOUT.Refresh();
            HFIPOUT.DataSource = null;
            HFIPOUT.Rows.Clear();


            HFIPOUT.ColumnCount = tap.Columns.Count;
            Genclass.i = 0;
            foreach (DataColumn column in tap.Columns)
            {
                HFIPOUT.Columns[Genclass.i].Name = column.ColumnName;
                HFIPOUT.Columns[Genclass.i].HeaderText = column.ColumnName;
                HFIPOUT.Columns[Genclass.i].DataPropertyName = column.ColumnName;
                Genclass.i = Genclass.i + 1;
            }




            HFIPOUT.Columns[0].Visible = false;
            HFIPOUT.Columns[1].Width = 340;
            HFIPOUT.Columns[2].Width = 100;
            HFIPOUT.Columns[3].Visible = false;



            HFIPOUT.DataSource = tap;



        }


        private void TXTOUT_TextChanged(object sender, EventArgs e)
        {

        }

        private void TXTOUT_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.F10)
                {
                    Pnlout.Visible = false;
                }
                else if (e.KeyCode == Keys.F2)
                {
                    SelectId = 1;
                    int Index = HFITOUT.SelectedCells[0].RowIndex;
                    TXTOUT.Text = HFITOUT.Rows[Index].Cells[1].Value.ToString();
                    TXTOUT.Tag = HFITOUT.Rows[Index].Cells[0].Value.ToString();

                    TXTOUTQTY.Focus();
                    Pnlout.Visible = false;
                    SelectId = 0;
                }
                else if (e.KeyCode == Keys.Escape)
                {
                    Pnlout.Visible = false;
                }
                else if (e.KeyValue == 40)
                {
                    HFITOUT.Select();
                }
                else if (e.KeyCode == Keys.Enter)
                {
                    TXTOUT_Click(sender, e);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void TXTIN_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.F10)
                {
                    Pnlin.Visible = false;
                }
                else if (e.KeyCode == Keys.F2)
                {
                    SelectId = 1;
                    int Index = HFITIN.SelectedCells[0].RowIndex;
                    TXTIN.Text = HFITIN.Rows[Index].Cells[1].Value.ToString();
                    TXTIN.Tag = HFITIN.Rows[Index].Cells[0].Value.ToString();

                    TXTINQTY.Focus();
                    Pnlin.Visible = false;
                    SelectId = 0;
                }
                else if (e.KeyCode == Keys.Escape)
                {
                    Pnlin.Visible = false;
                }
                else if (e.KeyValue == 40)
                {
                    HFITIN.Select();
                }
                else if (e.KeyCode == Keys.Enter)
                {
                    TXTIN_Click(sender, e);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
        
        private void HFITIN1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {

                string message = "Are you sure delete  this item ?";
                string caption = "Dilama";
                MessageBoxButtons buttons = MessageBoxButtons.YesNo;
                DialogResult result;

                result = MessageBox.Show(message, caption, buttons);

                if (result == System.Windows.Forms.DialogResult.Yes)
                {
                    conn.Close();
                    conn.Open();
                    if (mode == 2)
                    {
                        qur.CommandText = "Delete from workorderINM where  uid=" + HFITIN1.CurrentRow.Cells[5].Value.ToString() + "  and workid="+ HFITIN1.CurrentRow.Cells[6].Value.ToString() + " ";
                        qur.ExecuteNonQuery();

                        conn.Close();
                        conn.Open();

                        qur.CommandText = "delete from OUTPUTITEMSW where itemid=" + HFITIN1.CurrentRow.Cells[2].Value.ToString()  + "  and workid=" + HFITIN1.CurrentRow.Cells[6].Value.ToString() + " ";
                        qur.ExecuteNonQuery();

                        loadINOUT();
                        loadIN();
                    }
                    else
                    {
                        qur.CommandText = "Delete from testtemp where  itemid=" + HFITIN1.CurrentRow.Cells[2].Value.ToString() + " ";
                        qur.ExecuteNonQuery();


                        funoutload1();

                    }

                }
               

            }
        }

        private void tabPage3_Click(object sender, EventArgs e)
        {

        }

        private void Editpan_Paint(object sender, PaintEventArgs e)
        {

        }

        private void BtnSelect_Click(object sender, EventArgs e)
        {
            try
            {
                SelectId = 1;
                int Index = DataGridCommonNew.SelectedCells[0].RowIndex;
              
                    textBox2.Text = DataGridCommonNew.Rows[Index].Cells[1].Value.ToString();
                     textBox2.Tag = DataGridCommonNew.Rows[Index].Cells[0].Value.ToString();
                txtnar.Text = DataGridCommonNew.Rows[Index].Cells[2].Value.ToString();
                txtprocessdet.Focus();
               
                SelectId = 0;
                grNewSearch.Visible = false;
                SelectId = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }


        private void loaditemm()
        {
            string qur = "exec sp_getwoitemnames ";
            SqlCommand cmd = new SqlCommand(qur, conn);
            SqlDataAdapter apt = new SqlDataAdapter(cmd);
            DataTable tab = new DataTable();
            apt.Fill(tab);
            cboitem.DataSource = null;
            cboitem.DataSource = tab;
            cboitem.DisplayMember = "itemname";
            cboitem.ValueMember = "uid";
            cboitem.SelectedIndex = -1;

        }
        private void loadprocess()
        {
            if(textBox2.Tag!=null)
            {
                HFIT1.AutoGenerateColumns = false;
                HFIT1.Refresh();
                HFIT1.DataSource = null;
                HFIT1.Rows.Clear();

                HFIT1.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            HFIT1.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            string qur = " exec sp_getwoProces " + textBox2.Tag + ",'" + textBox2.Text + "'"; 
            SqlCommand cmd = new SqlCommand(qur, conn);
            SqlDataAdapter apt = new SqlDataAdapter(cmd);
            DataTable tab = new DataTable();
            apt.Fill(tab);
          
                string qur1 = "select refid,Seqno,Processname,Processdet,Colur,Rate,SupplierName,processuid,0 as id,itemid from woprocess order by itemid";
                SqlCommand cmd1 = new SqlCommand(qur1, conn);
                SqlDataAdapter apt1 = new SqlDataAdapter(cmd1);
                DataTable tab1 = new DataTable();
                apt1.Fill(tab1);

                if (tab1.Rows.Count > 0)

                {
                    for (int k = 0; k < tab1.Rows.Count; k++)
                    {
                        var index = HFIT1.Rows.Add();
                        HFIT1.Rows[index].Cells[0].Value = tab1.Rows[k]["refid"].ToString();
                        HFIT1.Rows[index].Cells[1].Value = tab1.Rows[k]["seqno"].ToString();
                        HFIT1.Rows[index].Cells[2].Value = tab1.Rows[k]["processname"].ToString();
                        HFIT1.Rows[index].Cells[3].Value = tab1.Rows[k]["Processdet"].ToString();
                        HFIT1.Rows[index].Cells[4].Value = tab1.Rows[k]["Colur"].ToString();
                        HFIT1.Rows[index].Cells[5].Value = tab1.Rows[k]["Rate"].ToString();
                        HFIT1.Rows[index].Cells[6].Value = tab1.Rows[k]["SupplierName"].ToString();
                        HFIT1.Rows[index].Cells[7].Value = tab1.Rows[k]["processuid"].ToString();
                        HFIT1.Rows[index].Cells[8].Value = cbotype.Text;
                        HFIT1.Rows[index].Cells[9].Value = tab1.Rows[k]["id"].ToString();
                        HFIT1.Rows[index].Cells[10].Value = tab1.Rows[k]["itemid"].ToString();
               

                    }

                }




            }
         
        }
       
        private void loadyarn()

        {
            if (textBox2.Tag != null)
            {
                HFIT1.AutoGenerateColumns = false;
                HFIT1.Refresh();
                HFIT1.DataSource = null;
                HFIT1.Rows.Clear();

                HFIT1.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
                HFIT1.DefaultCellStyle.WrapMode = DataGridViewTriState.True;



                string qur = " exec sp_getwoProcesYarn " + textBox2.Tag + ",'" + textBox2.Text + "'";
                SqlCommand cmd = new SqlCommand(qur, conn);
                SqlDataAdapter apt = new SqlDataAdapter(cmd);
                DataTable tab = new DataTable();
                apt.Fill(tab);

                string qur1 = "select refid,Seqno,Processname,Processdet,Colur,Rate,SupplierName,processuid,0 as id,itemid from woprocess order by itemid";
                SqlCommand cmd1 = new SqlCommand(qur1, conn);
                SqlDataAdapter apt1 = new SqlDataAdapter(cmd1);
                DataTable tab1 = new DataTable();
                apt1.Fill(tab1);

                if (tab1.Rows.Count > 0)

                {
                    for (int k = 0; k < tab1.Rows.Count; k++)
                    {
                        var index = HFIT1.Rows.Add();
                        HFIT1.Rows[index].Cells[0].Value = tab1.Rows[k]["refid"].ToString();
                        HFIT1.Rows[index].Cells[1].Value = tab1.Rows[k]["seqno"].ToString();
                        HFIT1.Rows[index].Cells[2].Value = tab1.Rows[k]["processname"].ToString();
                        HFIT1.Rows[index].Cells[3].Value = tab1.Rows[k]["Processdet"].ToString();
                        HFIT1.Rows[index].Cells[4].Value = tab1.Rows[k]["Colur"].ToString();
                        HFIT1.Rows[index].Cells[5].Value = tab1.Rows[k]["Rate"].ToString();
                        HFIT1.Rows[index].Cells[6].Value = tab1.Rows[k]["SupplierName"].ToString();
                        HFIT1.Rows[index].Cells[7].Value = tab1.Rows[k]["processuid"].ToString();
                        HFIT1.Rows[index].Cells[8].Value = cbotype.Text;
                        HFIT1.Rows[index].Cells[9].Value = tab1.Rows[k]["id"].ToString();
                        HFIT1.Rows[index].Cells[10].Value = tab1.Rows[k]["itemid"].ToString();


                    }

                }




            }
        }
        
private void loaditems()
        {
          

                HFIT1.AutoGenerateColumns = false;
                HFIT1.Refresh();
                HFIT1.DataSource = null;
                HFIT1.Rows.Clear();
                HFIT1.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
                HFIT1.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
                string qur = " exec sp_getwoProces " + textBox2.Tag + ",'" + textBox2.Text + "'";
                SqlCommand cmd = new SqlCommand(qur, conn);
                SqlDataAdapter apt = new SqlDataAdapter(cmd);
                DataTable tab = new DataTable();
                apt.Fill(tab);

                string qur1 = "select refid,Seqno,Processname,Processdet,Colur,Rate,SupplierName,processuid,0 as id,itemid from woprocess";
                SqlCommand cmd1 = new SqlCommand(qur1, conn);
                SqlDataAdapter apt1 = new SqlDataAdapter(cmd1);
                DataTable tab1 = new DataTable();
                apt1.Fill(tab1);

            if (tab1.Rows.Count > 0)

            {
                for (int k = 0; k < tab1.Rows.Count; k++)
                {
                    var index = HFIT1.Rows.Add();
                    HFIT1.Rows[index].Cells[0].Value = tab1.Rows[k]["refid"].ToString();
                    HFIT1.Rows[index].Cells[1].Value = tab1.Rows[k]["seqno"].ToString();
                    HFIT1.Rows[index].Cells[2].Value = tab1.Rows[k]["processname"].ToString();
                    HFIT1.Rows[index].Cells[3].Value = tab1.Rows[k]["Processdet"].ToString();
                    HFIT1.Rows[index].Cells[4].Value = tab1.Rows[k]["Colur"].ToString();
                    HFIT1.Rows[index].Cells[5].Value = tab1.Rows[k]["Rate"].ToString();
                    HFIT1.Rows[index].Cells[6].Value = tab1.Rows[k]["SupplierName"].ToString();
                    HFIT1.Rows[index].Cells[7].Value = tab1.Rows[k]["processuid"].ToString();
                    HFIT1.Rows[index].Cells[8].Value = cbotype.Text;
                    HFIT1.Rows[index].Cells[9].Value = tab1.Rows[k]["id"].ToString();
                    HFIT1.Rows[index].Cells[10].Value = tab1.Rows[k]["itemid"].ToString();


                }




            }



                

            }
        private void button10_Click(object sender, EventArgs e)
        {
            grNewSearch.Visible = false;
        }

        private void DataGridCommonNew_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                SelectId = 1;
                int Index = DataGridCommonNew.SelectedCells[0].RowIndex;
                textBox2.Text = DataGridCommonNew.Rows[Index].Cells[1].Value.ToString();
                textBox2.Tag = DataGridCommonNew.Rows[Index].Cells[0].Value.ToString();
                txtnar.Text = DataGridCommonNew.Rows[Index].Cells[2].Value.ToString();
                grNewSearch.Visible = false;
                SelectId = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }

        private void MoveUp(DataGridView dataGridView)
        {
            if (dataGridView.RowCount > 0)
            {
                int rowCount = dataGridView.Rows.Count;
                int index = dataGridView.SelectedCells[0].OwningRow.Index;

                if (index == 0)
                {
                    return;
                }
                dataGridView.ClearSelection();
                dataGridView.Rows[index - 1].Selected = true;
            }
        }

        private void MoveDown(DataGridView dataGridView)
        {
            if (dataGridView.RowCount > 0)
            {
                int rowCount = dataGridView.Rows.Count;
                int index = dataGridView.SelectedCells[0].OwningRow.Index;
                if (index == (rowCount - 1)) // include the header row
                {
                    return;
                }
                dataGridView.ClearSelection();
                dataGridView.Rows[index + 1].Selected = true;
            }
        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void chkact_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void txtdcno_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtdcqty_KeyPress_1(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                e.Handled = true;
                SendKeys.Send("{TAB}");
            }
        }

        private void txtdcqty_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void TXTIN_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                e.Handled = true;
                SendKeys.Send("{TAB}");
            }
        }

        private void TXTINQTY_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                e.Handled = true;
                SendKeys.Send("{TAB}");
            }
        }

        private void DataGridCommonNew_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    SelectId = 1;
                    int Index = DataGridCommonNew.SelectedCells[0].RowIndex;
                    textBox2.Text = DataGridCommonNew.Rows[Index].Cells[1].Value.ToString();
                    textBox2.Tag = DataGridCommonNew.Rows[Index].Cells[0].Value.ToString();
                    txtnar.Text = DataGridCommonNew.Rows[Index].Cells[2].Value.ToString();
                    SelectId = 0;
                    grNewSearch.Visible = false;
                    SelectId = 0;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }

        private void button22_Click(object sender, EventArgs e)
        {

            //Frmitem contc = new Frmitem();
            //contc.Show();
            //contc.FormBorderStyle = FormBorderStyle.None;
            //contc.StartPosition = FormStartPosition.Manual;
            //contc.Location = new System.Drawing.Point(200, 80);
        }

        private void button3_Click(object sender, EventArgs e)
        {

        }

        private void button11_Click(object sender, EventArgs e)
        {

        }

        private void butexit_Click(object sender, EventArgs e)
        {
            conn.Close();
            conn.Open();



            int i = HFGP.SelectedCells[0].RowIndex;

         
            qur.CommandText = "delete  from  workorderinM  where workid=" + HFGP.Rows[i].Cells[0].Value.ToString() + "";
            qur.ExecuteNonQuery();

            //qur.CommandText = "delete  from  workorderoutM  where workid=" + HFGP.Rows[i].Cells[0].Value.ToString() + "";
            //qur.ExecuteNonQuery();
            //qur.CommandText = "delete  from  WORKORDERYARN  where headid=" + HFGP.Rows[i].Cells[0].Value.ToString() + "";
            //qur.ExecuteNonQuery();
            //qur.CommandText = "delete  from  WORKORDERYARNcolor  where headid=" + HFGP.Rows[i].Cells[0].Value.ToString() + "";
            //qur.ExecuteNonQuery();

            qur.CommandText = "delete  from  processdetlist  where headid=" + HFGP.Rows[i].Cells[0].Value.ToString() + "";
            qur.ExecuteNonQuery();
         

            qur.CommandText = "delete  from  processdet  where uid=" + HFGP.Rows[i].Cells[0].Value.ToString() + "";
            qur.ExecuteNonQuery();

            LoadGetJobCard(1);
        }

        private void Genpan_Paint(object sender, PaintEventArgs e)
        {

        }

        private void cbosGReturnItem_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void txtscr1_TextChanged(object sender, EventArgs e)
        {
            bs.Filter = string.Format("Docno Like '%{0}%'  or  Docdate Like '%{1}%' or  OrderReference Like '%{1}%' or  Narration Like '%{1}%'   ", txtscr1.Text, txtscr1.Text, txtscr1.Text, txtscr1.Text);

        }

        private void cbofabric_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbofabric.Text != "" && cbofabric.ValueMember != "" && cbofabric.SelectedValue != null)
            {
                //CHK();
                funyarnload();
            }
        }
            private void CHK()
        {
            DataGridViewCheckBoxColumn checkColumn = new DataGridViewCheckBoxColumn();
            checkColumn.Name = "CHK";
            checkColumn.HeaderText = "CHK";
            checkColumn.Width = 50;
            checkColumn.ReadOnly = false;
            checkColumn.FillWeight = 10; //if the datagridview is resized (on form resize) the checkbox won't take up too much; value is relative to the other columns' fill values
            YARNLOAD.Columns.Add(checkColumn);


        }

        private void YARNLOAD_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            
        }

        private void YARNLOAD_CellClick_1(object sender, DataGridViewCellEventArgs e)
        {
           
        }

        private void YARNLOAD_CellClick_2(object sender, DataGridViewCellEventArgs e)
        {
            DataGridViewCheckBoxCell ch1 = new DataGridViewCheckBoxCell();
            ch1 = (DataGridViewCheckBoxCell)YARNLOAD.Rows[YARNLOAD.CurrentRow.Index].Cells[0];

            if (ch1.Value == null)
                ch1.Value = false;
            switch (ch1.Value.ToString())
            {
                case "True":
                    {
                        ch1.Value = false;

                        break;
                    }
                case "False":
                    {
                        ch1.Value = true;
                        //Where should I put the selected cell here?
                        break;
                    }
            }
        }

        private void button11_Click_1(object sender, EventArgs e)
        {
            Genclass.strsql = "select itemname,uid from iTEMM where uid=" + YARNLOAD.CurrentRow.Cells[3].Value.ToString() + "";

            Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
            SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
            DataTable tap1 = new DataTable();
            aptr1.Fill(tap1);


            if (tap1.Rows.Count > 0)
            {

                var index = YARNCOLORLOAD.Rows.Add();



                YARNCOLORLOAD.Rows[index].Cells[0].Value = YARNLOAD.CurrentRow.Cells[1].Value.ToString();


                YARNCOLORLOAD.Rows[index].Cells[1].Value = txtcolor.Text;

                YARNCOLORLOAD.Rows[index].Cells[2].Value = textBox3.Text;
                YARNCOLORLOAD.Rows[index].Cells[3].Value = YARNLOAD.CurrentRow.Cells[3].Value.ToString();
                YARNCOLORLOAD.Rows[index].Cells[4].Value = 0;



                txtcolor.Text = "";
                textBox3.Text = "0";


            }
        }

        private void txtgrn_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bsFabric.Filter = string.Format("Itemname LIKE '%{0}%' ", textBox2.Text);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void textBox2_Click(object sender, EventArgs e)
        {
            Genclass.type = 1;
            button22.Visible = false;
            DataTable dt = getParty();
            bsc.DataSource = dt;
            FillGrid1(dt, 1);
            Point loc = FindLocation(textBox2);
            grNewSearch.Location = new Point(loc.X, loc.Y + 20);
            grNewSearch.Visible = true;
        }

        private void textBox2_KeyDown(object sender, KeyEventArgs e)
        {

        }
        private void loadfabric()
        {
            if (textBox2.Text !="" && cboitem.SelectedValue!=null && cboitem.ValueMember != "")
            {
                string sp;
                HFITIN1.AutoGenerateColumns = false;
                HFITIN1.Refresh();
                HFITIN1.DataSource = null;
                HFITIN1.Rows.Clear();
          
                if(cbotype.Text=="Fabric")
                {
                    Genclass.name = "sp_GetLoadFabric";
                }
                else if (cbotype.Text == "Yarn")
                {
                    Genclass.name = "sp_GetLoadYarn";

                }
                Genclass.strsql1 = ""+ Genclass.name + " '" + textBox2.Text + "' ";
                Genclass.cmd = new SqlCommand(Genclass.strsql1, conn);
                SqlDataAdapter aptr2 = new SqlDataAdapter(Genclass.cmd);
                DataTable tap2 = new DataTable();
                aptr2.Fill(tap2);

                if (tap2.Rows.Count > 0)
                {
                    for (int k = 0; k < tap2.Rows.Count; k++)
                    {
                        var index = HFITIN1.Rows.Add();
                        HFITIN1.Rows[index].Cells[0].Value = tap2.Rows[k]["ITEMNAME"].ToString();
                        HFITIN1.Rows[index].Cells[1].Value = tap2.Rows[k]["QTY"].ToString();
                        HFITIN1.Rows[index].Cells[2].Value = tap2.Rows[k]["ITEMID"].ToString();
                        HFITIN1.Rows[index].Cells[3].Value = tap2.Rows[k]["TYPE"].ToString();
                        HFITIN1.Rows[index].Cells[4].Value = tap2.Rows[k]["PROCESS"].ToString();
                        HFITIN1.Rows[index].Cells[5].Value = tap2.Rows[k]["UID"].ToString();
                        HFITIN1.Rows[index].Cells[6].Value = tap2.Rows[k]["WORKID"].ToString();
                        HFITIN1.Rows[index].Cells[7].Value = tap2.Rows[k]["Uom"].ToString();

                    }
                }
            }

        }
        private void cboitem_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboitem.SelectedValue != null  && cboitem.ValueMember != "")
            {
                HFIT.AutoGenerateColumns = false;
                HFIT.Refresh();
                HFIT.DataSource = null;
                HFIT.Rows.Clear();
                HFIT.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
                HFIT.DefaultCellStyle.WrapMode = DataGridViewTriState.True;

                string qur1 = "select refid,Seqno,Processname,Processdet,Colur,Rate,SupplierName,processuid,0 as id,itemid from woprocess where itemid =" + cboitem.SelectedValue + " ";
                SqlCommand cmd1 = new SqlCommand(qur1, conn);
                SqlDataAdapter apt1 = new SqlDataAdapter(cmd1);
                DataTable tab1 = new DataTable();
                apt1.Fill(tab1);

                if (tab1.Rows.Count > 0)

                {
                    for (int k = 0; k < tab1.Rows.Count; k++)
                    {
                        var index1 = HFIT.Rows.Add();
                        HFIT.Rows[index1].Cells[0].Value = tab1.Rows[k]["refid"].ToString();
                        HFIT.Rows[index1].Cells[1].Value = tab1.Rows[k]["seqno"].ToString();
                        HFIT.Rows[index1].Cells[2].Value = tab1.Rows[k]["processname"].ToString();
                        HFIT.Rows[index1].Cells[3].Value = tab1.Rows[k]["Processdet"].ToString();
                        HFIT.Rows[index1].Cells[4].Value = tab1.Rows[k]["Colur"].ToString();
                        HFIT.Rows[index1].Cells[5].Value = tab1.Rows[k]["Rate"].ToString();
                        HFIT.Rows[index1].Cells[6].Value = tab1.Rows[k]["SupplierName"].ToString();
                        HFIT.Rows[index1].Cells[7].Value = tab1.Rows[k]["processuid"].ToString();
                        HFIT.Rows[index1].Cells[8].Value = cbotype.Text;
                        HFIT.Rows[index1].Cells[9].Value = tab1.Rows[k]["id"].ToString();
                        HFIT.Rows[index1].Cells[10].Value = tab1.Rows[k]["itemid"].ToString();
                    }
                }
           
            }
            loadfabric();

        }

        private void DataGridCommonNew_Click(object sender, EventArgs e)
        {
            int Index = DataGridCommonNew.SelectedCells[0].RowIndex;

            textBox2.Text = DataGridCommonNew.Rows[Index].Cells[1].Value.ToString();
            textBox2.Tag = DataGridCommonNew.Rows[Index].Cells[0].Value.ToString();
            txtnar.Text = DataGridCommonNew.Rows[Index].Cells[2].Value.ToString();
            txtprocessdet.Focus();

            SelectId = 0;
            grNewSearch.Visible = false;
            SelectId = 0;
        }

        private void DataGridCommonNew_DoubleClick(object sender, EventArgs e)
        {
            int Index = DataGridCommonNew.SelectedCells[0].RowIndex;

            textBox2.Text = DataGridCommonNew.Rows[Index].Cells[1].Value.ToString();
            textBox2.Tag = DataGridCommonNew.Rows[Index].Cells[0].Value.ToString();
            txtnar.Text = DataGridCommonNew.Rows[Index].Cells[2].Value.ToString();
            txtprocessdet.Focus();

            SelectId = 0;
            grNewSearch.Visible = false;
            SelectId = 0;
        }

        private void DataGridCommonNew_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void label31_Click(object sender, EventArgs e)
        {

        }
    }
}
