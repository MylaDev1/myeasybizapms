﻿namespace MyEasyBizAPMS
{
    partial class FrmClassification
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabDesingnAttribute = new System.Windows.Forms.TabPage();
            this.DataGridClassification = new System.Windows.Forms.DataGridView();
            this.tabSystemAttribute = new System.Windows.Forms.TabPage();
            this.label2 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.cmbType = new System.Windows.Forms.ComboBox();
            this.DataGridSys = new System.Windows.Forms.DataGridView();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabDesingnAttribute.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridClassification)).BeginInit();
            this.tabSystemAttribute.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridSys)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tabControl1);
            this.groupBox1.Controls.Add(this.textBox1);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(9, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(884, 462);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabDesingnAttribute);
            this.tabControl1.Controls.Add(this.tabSystemAttribute);
            this.tabControl1.Location = new System.Drawing.Point(12, 68);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(503, 335);
            this.tabControl1.TabIndex = 2;
            this.tabControl1.Click += new System.EventHandler(this.tabControl1_Click);
            // 
            // tabDesingnAttribute
            // 
            this.tabDesingnAttribute.Controls.Add(this.DataGridClassification);
            this.tabDesingnAttribute.Location = new System.Drawing.Point(4, 24);
            this.tabDesingnAttribute.Name = "tabDesingnAttribute";
            this.tabDesingnAttribute.Padding = new System.Windows.Forms.Padding(3);
            this.tabDesingnAttribute.Size = new System.Drawing.Size(495, 307);
            this.tabDesingnAttribute.TabIndex = 0;
            this.tabDesingnAttribute.Text = "Design Attribute";
            this.tabDesingnAttribute.UseVisualStyleBackColor = true;
            // 
            // DataGridClassification
            // 
            this.DataGridClassification.BackgroundColor = System.Drawing.Color.White;
            this.DataGridClassification.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridClassification.Location = new System.Drawing.Point(6, 6);
            this.DataGridClassification.Name = "DataGridClassification";
            this.DataGridClassification.Size = new System.Drawing.Size(484, 295);
            this.DataGridClassification.TabIndex = 0;
            // 
            // tabSystemAttribute
            // 
            this.tabSystemAttribute.Controls.Add(this.label2);
            this.tabSystemAttribute.Controls.Add(this.comboBox1);
            this.tabSystemAttribute.Controls.Add(this.cmbType);
            this.tabSystemAttribute.Controls.Add(this.DataGridSys);
            this.tabSystemAttribute.Location = new System.Drawing.Point(4, 24);
            this.tabSystemAttribute.Name = "tabSystemAttribute";
            this.tabSystemAttribute.Padding = new System.Windows.Forms.Padding(3);
            this.tabSystemAttribute.Size = new System.Drawing.Size(495, 307);
            this.tabSystemAttribute.TabIndex = 1;
            this.tabSystemAttribute.Text = "System Attribute";
            this.tabSystemAttribute.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(194, 18);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(79, 15);
            this.label2.TabIndex = 4;
            this.label2.Text = "Under Values";
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(279, 15);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(174, 23);
            this.comboBox1.TabIndex = 3;
            // 
            // cmbType
            // 
            this.cmbType.FormattingEnabled = true;
            this.cmbType.Location = new System.Drawing.Point(6, 15);
            this.cmbType.Name = "cmbType";
            this.cmbType.Size = new System.Drawing.Size(174, 23);
            this.cmbType.TabIndex = 2;
            // 
            // DataGridSys
            // 
            this.DataGridSys.BackgroundColor = System.Drawing.Color.White;
            this.DataGridSys.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridSys.Location = new System.Drawing.Point(5, 44);
            this.DataGridSys.Name = "DataGridSys";
            this.DataGridSys.Size = new System.Drawing.Size(484, 257);
            this.DataGridSys.TabIndex = 1;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(70, 22);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(312, 23);
            this.textBox1.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(55, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "Category";
            // 
            // FrmClassification
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(905, 479);
            this.Controls.Add(this.groupBox1);
            this.Name = "FrmClassification";
            this.Text = "Classification";
            this.Load += new System.EventHandler(this.FrmClassification_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabDesingnAttribute.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DataGridClassification)).EndInit();
            this.tabSystemAttribute.ResumeLayout(false);
            this.tabSystemAttribute.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridSys)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabDesingnAttribute;
        private System.Windows.Forms.TabPage tabSystemAttribute;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView DataGridClassification;
        private System.Windows.Forms.DataGridView DataGridSys;
        private System.Windows.Forms.ComboBox cmbType;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox comboBox1;
    }
}