﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace MyEasyBizAPMS
{
    public partial class FrmTrimsRequirement : Form
    {
        public FrmTrimsRequirement()
        {
            InitializeComponent();
            this.SfdDataGridTrims.Style.HeaderStyle.Font.FontStyle = FontStyle.Bold;
            this.SfdDataGridTrims.Style.HeaderStyle.BackColor = Color.DeepSkyBlue;
        }
 
        SQLDBHelper db = new SQLDBHelper();
        SqlConnection sqlconnection = new SqlConnection(GeneralParameters.ConnectionString);
        BindingSource bsClass = new BindingSource();
        BindingSource bsItem = new BindingSource();
        int Fillid = 0; int SelectId = 0;
        private int LoadId =0;int Mode = 0;
        DataTable dtAttribute = new DataTable();

        public DataTable LoadGeneralM()
        {
            DataTable dt = new DataTable();
            try
            {
                dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "SP_GetGeneralsTrims", sqlconnection);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            return dt;
        }


        private void TxtCategory_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dt = LoadGeneralM();
                System.Data.DataRow[] dataRows = dt.Select("TypeMUid = 13 and F2=" + CmbClassification.SelectedValue + "");
                if(dataRows.Length > 0)
                {
                    DataTable dtclass = dt.Select("TypeMUid = 13 and F2=" + CmbClassification.SelectedValue + "").CopyToDataTable();
                    bsClass.DataSource = dtclass;
                    FillGrid(dtclass, 1);
                    Point loc = Genclass.FindLocation(txtCategory);
                    grSearch.Location = new Point(loc.X - 10, loc.Y + 20);
                    grSearch.Visible = true;
                }
                else
                {
                    txtCategory.Text = string.Empty;
                    MessageBox.Show("No data found", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void FillGrid(DataTable dt, int FillId)
        {
            try
            {
                DataGridCommon.DataSource = null;
                DataGridCommon.AutoGenerateColumns = false;
                if (FillId == 1)
                {
                    Fillid = 1;
                    DataGridCommon.ColumnCount = 2;
                    DataGridCommon.Columns[0].Name = "Uid";
                    DataGridCommon.Columns[0].HeaderText = "Uid";
                    DataGridCommon.Columns[0].DataPropertyName = "Guid";
                    DataGridCommon.Columns[0].Visible = false;
                    DataGridCommon.Columns[0].Width = 0;
                    DataGridCommon.Columns[1].Name = "Category";
                    DataGridCommon.Columns[1].HeaderText = "Category";
                    DataGridCommon.Columns[1].DataPropertyName = "GeneralName";
                    DataGridCommon.Columns[1].Width = 220;
                    DataGridCommon.DataSource = bsClass;
                }
                if (FillId == 3)
                {
                    Fillid = 3;
                    DataGridCommon.ColumnCount = 2;
                    DataGridCommon.Columns[0].Name = "Uid";
                    DataGridCommon.Columns[0].HeaderText = "Uid";
                    DataGridCommon.Columns[0].DataPropertyName = "UID";
                    DataGridCommon.Columns[0].Visible = false;
                    DataGridCommon.Columns[0].Width = 0;
                    DataGridCommon.Columns[1].Name = "Category";
                    DataGridCommon.Columns[1].HeaderText = "Category";
                    DataGridCommon.Columns[1].DataPropertyName = "ItemName";
                    DataGridCommon.Columns[1].Width = 220;
                    DataGridCommon.DataSource = bsItem;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void TxtItemName_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtCategory.Text != string.Empty)
                {
                    string Query = "select UID,ItemName from ItemsM Where ItemCategoryUID  =" + txtCategory.Tag + "";
                    DataTable dt = db.GetDataWithoutParam(CommandType.Text, Query, sqlconnection);
                    bsItem.DataSource = dt;
                    FillGrid(dt, 2);
                    //Point loc = Genclass.FindLocation(txtItemName);
                    //grSearch.Location = new Point(loc.X - 10, loc.Y + 20);
                    //grSearch.Visible = true;
                }
                else
                {
                    MessageBox.Show("Select Catgory", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtCategory.Focus();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void BtnSelect_Click(object sender, EventArgs e)
        {
            try
            {
                SelectId = 1;
                int Index = DataGridCommon.SelectedCells[0].RowIndex;
                if (Fillid == 1)
                {
                    txtCategory.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtCategory.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    dtAttribute = GetAttributebyCategory(Convert.ToDecimal(txtCategory.Tag), CmbClassification.Text + " " + txtCategory.Text);
                    FillDataGrid(dtAttribute);
                }
                else if (Fillid == 2)
                {

                }
                grSearch.Visible = false;
                SelectId = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private DataTable GetAttributebyCategory(decimal v,string ItemName)
        {
            DataTable dt = new DataTable();
            try
            {
                //SqlParameter[] sqlParameters = { new SqlParameter("@ItemCategoryUID", v) };
                //dt = db.GetDataWithParam(CommandType.StoredProcedure, "Proc_GetAttributeReq", sqlParameters, sqlconnection);3
                SqlParameter[] sqlParameters = { new SqlParameter("@ItemCategoryUID", v), new SqlParameter("@ItemName", ItemName) };
                dt = db.GetDataWithParam(CommandType.StoredProcedure, "Proc_GetAttributeReqNew", sqlParameters, sqlconnection);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            return dt;
        }

        private void GetAttribute(decimal v)
        {
            try
            {
                DataGridAttribute.Rows.Clear();
                SqlParameter[] parameters = { new SqlParameter("@ItemUId", v) };
                DataTable dt = db.GetDataWithParam(CommandType.StoredProcedure, "SP_GetAttributeForBOM", parameters, sqlconnection);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    int index = DataGridAttribute.Rows.Add();
                    DataGridViewRow row = (DataGridViewRow)DataGridAttribute.Rows[index];
                    row.Cells[0].Value = dt.Rows[i]["Attribute"].ToString();
                    row.Cells[1].Value = "";
                    row.Cells[2].Value = dt.Rows[i]["Uid"].ToString();
                    row.Cells[3].Value = dt.Rows[i]["UserDefined"].ToString();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }

        protected void LoadAttribute()
        {
            try
            {
                DataGridAttribute.AutoGenerateColumns = false;
                DataGridAttribute.ColumnCount = 4;
                DataGridAttribute.Columns[0].Name = "Description";
                DataGridAttribute.Columns[0].HeaderText = "Description";
                DataGridAttribute.Columns[0].Width = 130;

                DataGridAttribute.Columns[1].Name = "Value";
                DataGridAttribute.Columns[1].HeaderText = "Value";
                DataGridAttribute.Columns[1].Width = 245;

                DataGridAttribute.Columns[2].Name = "Uid";
                DataGridAttribute.Columns[2].HeaderText = "Uid";
                DataGridAttribute.Columns[2].Visible = false;

                DataGridAttribute.Columns[3].Name = "UserDefined";
                DataGridAttribute.Columns[3].HeaderText = "UserDefined";
                DataGridAttribute.Columns[3].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                throw;
            }
        }

        private void FrmTrimsRequirement_Load(object sender, EventArgs e)
        {
          
            grFront.Visible = true;
            grBack.Visible = false;
            DataTable dt = LoadGeneralM();
            DataTable dtclass = dt.Select("TypeMUid = 9").CopyToDataTable();
            CmbClassification.DisplayMember = "GeneralName";
            CmbClassification.ValueMember = "Guid";
            CmbClassification.DataSource = dtclass;
            LoadAttribute();
            GetTrims();
        }

        private void BtnHide_Click(object sender, EventArgs e)
        {
            grSearch.Visible = false;
        }

        private void SplitSave_DropDowItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            try
            {
                if (e.ClickedItem.Text == "Back")
                {
                    grBack.Visible = false;
                    grFront.Visible = true;
                    GetTrims();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }

        private void DataGridCommon_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {

        }

        private void DataGridCommon_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                SelectId = 1;
                int Index = DataGridCommon.SelectedCells[0].RowIndex;
                if (Fillid == 1)
                {
                    txtCategory.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtCategory.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    dtAttribute = GetAttributebyCategory(Convert.ToDecimal(txtCategory.Tag), CmbClassification.Text + " " + txtCategory.Text);
                    if(dtAttribute.Rows.Count > 0)
                    {
                        FillDataGrid(dtAttribute);
                    }
                    else
                    {
                        MessageBox.Show("No Data Found", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                }
                else if (Fillid == 2)
                {

                }
                grSearch.Visible = false;
                SelectId = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected void FillDataGrid(DataTable data)
        {
            try
            {
           
                if (Mode == 1)
                {
                    CmbAttribute.DataSource = null;
                    CmbAttribute.DisplayMember = "Attribute";
                    CmbAttribute.ValueMember = "Uid";
                    CmbAttribute.DataSource = data;

                    //DataView view = new DataView(data);
                    //DataTable dt2 = view.ToTable(true, "Attribute");
                    //DataGridAttribute.DataSource = null;
                    //DataGridAttribute.ColumnCount = 6;
                    //DataGridAttribute.Columns[0].Name = "Attribute";
                    //DataGridAttribute.Columns[0].HeaderText = "Attribute";
                    //DataGridAttribute.Columns[0].DataPropertyName = "Attribute";

                    //DataGridAttribute.Columns[1].Name = "AttributeValue";
                    //DataGridAttribute.Columns[1].HeaderText = "AttributeValue";

                    //DataGridAttribute.Columns[2].Name = "AttributeUid";
                    //DataGridAttribute.Columns[2].HeaderText = "AttributeUid";
                    //DataGridAttribute.Columns[2].DataPropertyName = "AttributeUid";
                    //DataGridAttribute.Columns[2].Visible = false;

                    //DataGridAttribute.Columns[3].Name = "Uid";
                    //DataGridAttribute.Columns[3].HeaderText = "Uid";
                    //DataGridAttribute.Columns[3].DataPropertyName = "Uid";
                    //DataGridAttribute.Columns[3].Visible = false;

                    //DataGridAttribute.Columns[4].Name = "ItemUid";
                    //DataGridAttribute.Columns[4].HeaderText = "ItemUid";
                    //DataGridAttribute.Columns[4].DataPropertyName = "ItemUid";
                    //DataGridAttribute.Columns[4].Visible = false;

                    //DataGridAttribute.Columns[5].Name = "AttributeValueUid";
                    //DataGridAttribute.Columns[5].HeaderText = "AttributeValueUid";
                    //DataGridAttribute.Columns[5].Visible = false;
                    //DataGridAttribute.DataSource = dt2;
                }
                else
                {
                    string[] text = txtItemName.Text.Split('/');
                    CmbAttribute.DataSource = null;
                    CmbAttribute.DisplayMember = "Attribute";
                    CmbAttribute.ValueMember = "AttributeUid";
                    CmbAttribute.DataSource = data;
                    for (int i = 0; i < data.Rows.Count; i++)
                    {
                        int Index = DataGridAttribute.Rows.Add();
                        DataGridViewRow dataGridViewRow = DataGridAttribute.Rows[Index];
                        dataGridViewRow.Cells[0].Value = data.Rows[i]["Attribute"].ToString();
                        dataGridViewRow.Cells[1].Value = "";
                        dataGridViewRow.Cells[2].Value = data.Rows[i]["ItemUid"].ToString();
                        dataGridViewRow.Cells[3].Value = data.Rows[i]["AttributeUid"].ToString();
                    }
                    //DataView view = new DataView(data);
                    //DataTable dt2 = view.ToTable(true, "Attribute");
                    //DataGridAttribute.DataSource = null;
                    //DataGridAttribute.ColumnCount = 6;
                    //DataGridAttribute.Columns[0].Name = "Attribute";
                    //DataGridAttribute.Columns[0].HeaderText = "Attribute";
                    //DataGridAttribute.Columns[0].DataPropertyName = "Attribute";

                    //DataGridAttribute.Columns[1].Name = "AttributeValue";
                    //DataGridAttribute.Columns[1].HeaderText = "AttributeValue";

                    //DataGridAttribute.Columns[2].Name = "AttributeUid";
                    //DataGridAttribute.Columns[2].HeaderText = "AttributeUid";
                    //DataGridAttribute.Columns[2].DataPropertyName = "AttributeUid";
                    //DataGridAttribute.Columns[2].Visible = false;

                    //DataGridAttribute.Columns[3].Name = "Uid";
                    //DataGridAttribute.Columns[3].HeaderText = "Uid";
                    //DataGridAttribute.Columns[3].DataPropertyName = "Uid";
                    //DataGridAttribute.Columns[3].Visible = false;

                    //DataGridAttribute.Columns[4].Name = "ItemUid";
                    //DataGridAttribute.Columns[4].HeaderText = "ItemUid";
                    //DataGridAttribute.Columns[4].DataPropertyName = "ItemUid";
                    //DataGridAttribute.Columns[4].Visible = false;

                    //DataGridAttribute.Columns[5].Name = "AttributeValueUid";
                    //DataGridAttribute.Columns[5].HeaderText = "AttributeValueUid";
                    //DataGridAttribute.Columns[5].Visible = false;
                    //DataGridAttribute.DataSource = dt2;

                    for (int i = 0; i < DataGridAttribute.Rows.Count; i++)
                    {
                        DataGridAttribute.Rows[i].Cells[1].Value = text[i + 1].ToString();
                    }

                }
            }
            catch (Exception)
            {
                //MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                //return;
            }
        }

        private void SplitButton1_Click(object sender, EventArgs e)
        {
            try
            {
             
                grFront.Visible = false;
                grBack.Visible = true;
                DataGridAttribute.DataSource = null;
                txtCategory.Text = string.Empty;
                txtItemName.Tag = "0";
                txtItemName.Text = string.Empty;
                CmbAttribute.DataSource = null;
                CmbValues.DataSource = null;
                txtShortName.Text = string.Empty;
                DataGridAttribute.Rows.Clear();
                Mode = 1;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void SplitButton1_DropDowItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            try
            {
                if (e.ClickedItem.Text == "Close")
                {
                    this.Close();
                }
                else if(e.ClickedItem.Text == "Edit")
                {
                    
                    if(SfdDataGridTrims.SelectedIndex != -1)
                    {
                        Mode = 2;
                        SelectId = 1;
                        var selectedItem = SfdDataGridTrims.SelectedItems[0];
                        var dataRow = (selectedItem as DataRowView).Row;
                        decimal TrimsUid = Convert.ToDecimal(dataRow["UID"].ToString());
                        CmbClassification.SelectedValue = dataRow["ClassificationUid"].ToString();
                        txtCategory.Text = dataRow["Category"].ToString();
                        txtCategory.Tag = dataRow["CategoryUid"].ToString();
                        txtItemName.Text = dataRow["ItemName"].ToString();
                        txtItemName.Tag = TrimsUid;
                        txtShortName.Text = dataRow["ShortName"].ToString();
                        SqlParameter[] sqlParameters = { new SqlParameter("@ItemCategoryUID", txtCategory.Tag), new SqlParameter("@ItemName", CmbClassification.Text + " " + txtCategory.Text) };
                        dtAttribute = db.GetDataWithParam(CommandType.StoredProcedure, "Proc_GetAttributeReq", sqlParameters, sqlconnection);
                        DataGridAttribute.Rows.Clear();
                        FillDataGrid(dtAttribute);
                        grFront.Visible = false;
                        grBack.Visible = true;
                        SelectId = 0;
                    }
                    else
                    {
                        MessageBox.Show("Select a Row", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void TxtCategory_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bsClass.Filter = string.Format("GeneralName LIKE '%{0}%'", txtCategory.Text);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void TxtItemName_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    //bsItem.Filter = string.Format("ItemName LIKE '%{0}%'", txtItemName.Text);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void GrSearch_Enter(object sender, EventArgs e)
        {

        }

        private DataTable GetAttributeValue(string Attribute)
        {
            DataTable tab = new DataTable();
            if (dtAttribute.Rows.Count > 0)
            {
                tab = dtAttribute.Select("Attribute ='" + Attribute + "'", "AttributeValueUid").CopyToDataTable();
            }
            else
            {
                tab = dtAttribute.Clone();
            }
            return tab;
        }

        private void DataGridAttribute_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                DataGridViewComboBoxCell l_objGridDropbox = new DataGridViewComboBoxCell();
                if (DataGridAttribute.CurrentRow.Cells[0].Value != null)
                {
                    DataTable nm = new DataTable();
                    string attribute = DataGridAttribute.CurrentRow.Cells[0].Value.ToString();
                    if (e.ColumnIndex == 1)
                    {
                        nm = GetAttributeValue(attribute);
                        l_objGridDropbox.DisplayMember = "AttributeValue";
                        l_objGridDropbox.ValueMember = "AttributeValueUid";
                        l_objGridDropbox.DataSource = nm;
                        DataGridAttribute[e.ColumnIndex, e.RowIndex] = l_objGridDropbox;
                    }
                }
            }
            catch (Exception)
            {
                //MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                //return;
            }
        }

        private void DataGridAttribute_MouseLeave(object sender, EventArgs e)
        {
            try
            {
                //string Namenn = string.Empty;
                //for (int i = 0; i < DataGridAttribute.Rows.Count; i++)
                //{
                //    string SelectedText = string.Empty;
                //    Namenn += Convert.ToString((DataGridAttribute.Rows[i].Cells[2] as DataGridViewComboBoxCell).FormattedValue.ToString());
                //}
                //txtItemName.Text = txtItemName.Text + "( " + Namenn + " )";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void SplitSave_Click(object sender, EventArgs e)
        {
            try
            {
                if(txtCategory.Text != string.Empty && DataGridAttribute.Rows.Count > 0)
                {
                    string Namenn = string.Empty;
                    txtItemName.Text = string.Empty;
                    for (int i = 0; i < DataGridAttribute.Rows.Count; i++)
                    {
                        string SelectedText = string.Empty;
                        var isNumeric = false;
                        if (Mode == 2 || Mode ==1)
                        {
                            isNumeric = int.TryParse(DataGridAttribute.Rows[i].Cells[1].Value.ToString(), out int n);
                        }
                        else
                        {
                            isNumeric = false;
                        }
                        if (isNumeric == true)
                        {
                            if (i == 0)
                            {
                                Namenn = Convert.ToString((DataGridAttribute.Rows[i].Cells[1] as DataGridViewComboBoxCell).FormattedValue.ToString());
                            }
                            else
                            {
                                Namenn = Namenn + " / " + Convert.ToString((DataGridAttribute.Rows[i].Cells[1] as DataGridViewComboBoxCell).FormattedValue.ToString());
                            }
                        }
                        else
                        {
                            if (i == 0)
                            {
                                Namenn = DataGridAttribute.Rows[i].Cells[1].Value.ToString();
                            }
                            else
                            {
                                Namenn = Namenn + " / " + DataGridAttribute.Rows[i].Cells[1].Value.ToString();
                            }
                        }
                        
                    }
                    txtItemName.Text = txtCategory.Text + " / " + Namenn + "";
                    if (Mode == 1)
                    {
                        SqlParameter[] sqlParametersD = { new SqlParameter("@ItemName", txtItemName.Text) };
                        DataTable dataTable = db.GetDataWithParam(CommandType.StoredProcedure, "Proc_CheckDuplicateTrims", sqlParametersD, sqlconnection);
                        if (dataTable.Rows.Count > 0)
                        {
                            MessageBox.Show("Name Already Found", "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            return;
                        }
                    }
                    SqlParameter[] sqlParameters = {
                        new SqlParameter("@Uid",txtItemName.Tag),
                        new SqlParameter("@ClassificationUid",CmbClassification.SelectedValue),
                        new SqlParameter("@CategoryUid",txtCategory.Tag),
                        new SqlParameter("@ItemName",txtItemName.Text),
                        new SqlParameter("@ShortName",txtShortName.Text),
                    };
                    db.ExecuteNonQuery(CommandType.StoredProcedure, "Proc_TrimsM", sqlParameters, sqlconnection);
                    MessageBox.Show("Record Saved Successfully", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtCategory.Text = string.Empty;
                    DataGridAttribute.DataSource = null;
                    txtItemName.Text = string.Empty;
                    txtShortName.Text = string.Empty;
                    DataGridAttribute.Rows.Clear();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected void GetTrims()
        {
            try
            {
                DataTable dataTable = db.GetDataWithoutParam(CommandType.StoredProcedure, "Proc_GetTrims", sqlconnection);
                SfdDataGridTrims.DataSource = null;
                SfdDataGridTrims.DataSource = dataTable;
                SfdDataGridTrims.Columns[0].Visible = false;
                SfdDataGridTrims.Columns[1].Visible = false;
                SfdDataGridTrims.Columns[2].Visible = false;
                SfdDataGridTrims.Columns[3].Width = 320;
                SfdDataGridTrims.Columns[5].Width = 200;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void CmbAttribute_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if(CmbAttribute.SelectedIndex != -1 && SelectId == 0)
                {
                    SqlParameter[] sqlParameters = { new SqlParameter("@ItemCategoryUID", txtCategory.Tag), new SqlParameter("@AttributeUid", CmbAttribute.SelectedValue) };
                    DataTable data = db.GetDataWithParam(CommandType.StoredProcedure, "Proc_GetAttributevaluesByAttribute", sqlParameters, sqlconnection);
                    if(data.Rows.Count > 0)
                    {
                        CmbValues.DataSource = null;
                        CmbValues.DisplayMember = "AttributeValue";
                        CmbValues.ValueMember = "ItemUid";
                        CmbValues.DataSource = data;
                    }
                    else
                    {
                        MessageBox.Show("No Data Found", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void BtnOk_Click(object sender, EventArgs e)
        {
            try
            {
                if(CmbAttribute.SelectedIndex != -1 && CmbValues.SelectedIndex != -1)
                {
                    int Index = DataGridAttribute.Rows.Add();
                    DataGridViewRow dataGridViewRow = DataGridAttribute.Rows[Index];
                    dataGridViewRow.Cells[0].Value = CmbAttribute.Text;
                    dataGridViewRow.Cells[1].Value = CmbValues.Text;
                    dataGridViewRow.Cells[2].Value = CmbValues.SelectedValue;
                    dataGridViewRow.Cells[3].Value = "0";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void CmbClassification_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void DataGridAttribute_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void DataGridAttribute_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {




                int index = DataGridAttribute.SelectedCells[0].RowIndex;

                if (Mode == 1)

                {
                    DataGridAttribute.Rows.RemoveAt(index);
                    DataGridAttribute.ClearSelection();
                }
                else


                {
                    SqlParameter[] sqlParameters = { new SqlParameter("@uid", DataGridAttribute.CurrentRow.Cells[3].Value.ToString()), new SqlParameter("@itemid", DataGridAttribute.CurrentRow.Cells[2].Value.ToString()) };
                    dtAttribute = db.GetDataWithParam(CommandType.StoredProcedure, "sp_DeleteItemsMAttributeValues", sqlParameters, sqlconnection);
                    DataGridAttribute.Rows.RemoveAt(index);
                    DataGridAttribute.ClearSelection();
                }

            }
        }

        private void CmbValues_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
