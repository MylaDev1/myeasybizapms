﻿namespace MyEasyBizAPMS
{
    partial class FrmOpstk
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmOpstk));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panadd = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblno1 = new System.Windows.Forms.Label();
            this.lblno2 = new System.Windows.Forms.Label();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.buttnnvfst = new System.Windows.Forms.Button();
            this.buttnnxtlft = new System.Windows.Forms.Button();
            this.btnfinnxt = new System.Windows.Forms.Button();
            this.buttrnxt = new System.Windows.Forms.Button();
            this.buttnext1 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.Genpan = new System.Windows.Forms.Panel();
            this.label10 = new System.Windows.Forms.Label();
            this.CNOTYPE = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtuom = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtbags = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtprice = new System.Windows.Forms.TextBox();
            this.grSearch = new System.Windows.Forms.Panel();
            this.button18 = new System.Windows.Forms.Button();
            this.btnHide = new System.Windows.Forms.Button();
            this.DataGridCommon = new System.Windows.Forms.DataGridView();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.buttrqok = new System.Windows.Forms.Button();
            this.HFGP = new System.Windows.Forms.DataGridView();
            this.txtitem = new System.Windows.Forms.TextBox();
            this.txtop = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtpuid = new System.Windows.Forms.TextBox();
            this.txtitid = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.Dtpdt = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.txtitcode = new System.Windows.Forms.TextBox();
            this.panadd.SuspendLayout();
            this.panel1.SuspendLayout();
            this.Genpan.SuspendLayout();
            this.grSearch.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridCommon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HFGP)).BeginInit();
            this.SuspendLayout();
            // 
            // panadd
            // 
            this.panadd.BackColor = System.Drawing.Color.White;
            this.panadd.Controls.Add(this.panel1);
            this.panadd.Controls.Add(this.buttnnvfst);
            this.panadd.Controls.Add(this.buttnnxtlft);
            this.panadd.Controls.Add(this.btnfinnxt);
            this.panadd.Controls.Add(this.buttrnxt);
            this.panadd.Controls.Add(this.buttnext1);
            this.panadd.Location = new System.Drawing.Point(12, 531);
            this.panadd.Name = "panadd";
            this.panadd.Size = new System.Drawing.Size(847, 40);
            this.panadd.TabIndex = 407;
            this.panadd.Paint += new System.Windows.Forms.PaintEventHandler(this.panadd_Paint);
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.lblno1);
            this.panel1.Controls.Add(this.lblno2);
            this.panel1.Controls.Add(this.flowLayoutPanel3);
            this.panel1.Controls.Add(this.flowLayoutPanel2);
            this.panel1.Controls.Add(this.flowLayoutPanel1);
            this.panel1.Location = new System.Drawing.Point(64, 5);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(74, 30);
            this.panel1.TabIndex = 214;
            // 
            // lblno1
            // 
            this.lblno1.AutoSize = true;
            this.lblno1.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblno1.ForeColor = System.Drawing.Color.Black;
            this.lblno1.Location = new System.Drawing.Point(4, 5);
            this.lblno1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblno1.Name = "lblno1";
            this.lblno1.Size = new System.Drawing.Size(14, 15);
            this.lblno1.TabIndex = 163;
            this.lblno1.Text = "1";
            // 
            // lblno2
            // 
            this.lblno2.AutoSize = true;
            this.lblno2.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblno2.ForeColor = System.Drawing.Color.Black;
            this.lblno2.Location = new System.Drawing.Point(27, 5);
            this.lblno2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblno2.Name = "lblno2";
            this.lblno2.Size = new System.Drawing.Size(28, 15);
            this.lblno2.TabIndex = 162;
            this.lblno2.Text = "of 1";
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.Location = new System.Drawing.Point(61, 30);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(200, 100);
            this.flowLayoutPanel3.TabIndex = 2;
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.Location = new System.Drawing.Point(38, 35);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(200, 100);
            this.flowLayoutPanel2.TabIndex = 1;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Location = new System.Drawing.Point(62, -129);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(200, 100);
            this.flowLayoutPanel1.TabIndex = 0;
            // 
            // buttnnvfst
            // 
            this.buttnnvfst.BackColor = System.Drawing.Color.White;
            this.buttnnvfst.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.buttnnvfst.FlatAppearance.BorderSize = 0;
            this.buttnnvfst.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttnnvfst.Image = ((System.Drawing.Image)(resources.GetObject("buttnnvfst.Image")));
            this.buttnnvfst.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttnnvfst.Location = new System.Drawing.Point(6, 5);
            this.buttnnvfst.Name = "buttnnvfst";
            this.buttnnvfst.Size = new System.Drawing.Size(19, 31);
            this.buttnnvfst.TabIndex = 213;
            this.buttnnvfst.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttnnvfst.UseVisualStyleBackColor = false;
            // 
            // buttnnxtlft
            // 
            this.buttnnxtlft.BackColor = System.Drawing.Color.White;
            this.buttnnxtlft.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.buttnnxtlft.FlatAppearance.BorderSize = 0;
            this.buttnnxtlft.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttnnxtlft.Image = ((System.Drawing.Image)(resources.GetObject("buttnnxtlft.Image")));
            this.buttnnxtlft.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttnnxtlft.Location = new System.Drawing.Point(40, 5);
            this.buttnnxtlft.Name = "buttnnxtlft";
            this.buttnnxtlft.Size = new System.Drawing.Size(18, 31);
            this.buttnnxtlft.TabIndex = 212;
            this.buttnnxtlft.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttnnxtlft.UseVisualStyleBackColor = false;
            // 
            // btnfinnxt
            // 
            this.btnfinnxt.BackColor = System.Drawing.Color.White;
            this.btnfinnxt.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnfinnxt.FlatAppearance.BorderSize = 0;
            this.btnfinnxt.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnfinnxt.Image = ((System.Drawing.Image)(resources.GetObject("btnfinnxt.Image")));
            this.btnfinnxt.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnfinnxt.Location = new System.Drawing.Point(178, 5);
            this.btnfinnxt.Name = "btnfinnxt";
            this.btnfinnxt.Size = new System.Drawing.Size(19, 31);
            this.btnfinnxt.TabIndex = 211;
            this.btnfinnxt.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnfinnxt.UseVisualStyleBackColor = false;
            // 
            // buttrnxt
            // 
            this.buttrnxt.BackColor = System.Drawing.Color.White;
            this.buttrnxt.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.buttrnxt.FlatAppearance.BorderSize = 0;
            this.buttrnxt.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttrnxt.Image = ((System.Drawing.Image)(resources.GetObject("buttrnxt.Image")));
            this.buttrnxt.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttrnxt.Location = new System.Drawing.Point(144, 5);
            this.buttrnxt.Name = "buttrnxt";
            this.buttrnxt.Size = new System.Drawing.Size(18, 31);
            this.buttrnxt.TabIndex = 210;
            this.buttrnxt.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttrnxt.UseVisualStyleBackColor = false;
            // 
            // buttnext1
            // 
            this.buttnext1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttnext1.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttnext1.Image = ((System.Drawing.Image)(resources.GetObject("buttnext1.Image")));
            this.buttnext1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttnext1.Location = new System.Drawing.Point(273, 4);
            this.buttnext1.Name = "buttnext1";
            this.buttnext1.Size = new System.Drawing.Size(54, 30);
            this.buttnext1.TabIndex = 208;
            this.buttnext1.Text = "Exit";
            this.buttnext1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttnext1.UseVisualStyleBackColor = false;
            this.buttnext1.Click += new System.EventHandler(this.buttnext1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(-35, 99);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(36, 15);
            this.label1.TabIndex = 403;
            this.label1.Text = "Party";
            // 
            // Genpan
            // 
            this.Genpan.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.Genpan.Controls.Add(this.label10);
            this.Genpan.Controls.Add(this.CNOTYPE);
            this.Genpan.Controls.Add(this.label8);
            this.Genpan.Controls.Add(this.txtuom);
            this.Genpan.Controls.Add(this.label7);
            this.Genpan.Controls.Add(this.txtbags);
            this.Genpan.Controls.Add(this.label6);
            this.Genpan.Controls.Add(this.txtprice);
            this.Genpan.Controls.Add(this.grSearch);
            this.Genpan.Controls.Add(this.label4);
            this.Genpan.Controls.Add(this.label5);
            this.Genpan.Controls.Add(this.buttrqok);
            this.Genpan.Controls.Add(this.HFGP);
            this.Genpan.Controls.Add(this.txtitem);
            this.Genpan.Controls.Add(this.txtop);
            this.Genpan.Controls.Add(this.label2);
            this.Genpan.Controls.Add(this.txtpuid);
            this.Genpan.Controls.Add(this.txtitid);
            this.Genpan.Controls.Add(this.label9);
            this.Genpan.Controls.Add(this.Dtpdt);
            this.Genpan.Controls.Add(this.label3);
            this.Genpan.Controls.Add(this.txtitcode);
            this.Genpan.Location = new System.Drawing.Point(1, 0);
            this.Genpan.Name = "Genpan";
            this.Genpan.Size = new System.Drawing.Size(871, 530);
            this.Genpan.TabIndex = 408;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(344, 18);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(42, 21);
            this.label10.TabIndex = 458;
            this.label10.Text = "Type";
            // 
            // CNOTYPE
            // 
            this.CNOTYPE.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CNOTYPE.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CNOTYPE.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CNOTYPE.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CNOTYPE.FormattingEnabled = true;
            this.CNOTYPE.Items.AddRange(new object[] {
            "Yarn",
            "Trims",
            "Fabric"});
            this.CNOTYPE.Location = new System.Drawing.Point(393, 13);
            this.CNOTYPE.Name = "CNOTYPE";
            this.CNOTYPE.Size = new System.Drawing.Size(170, 26);
            this.CNOTYPE.TabIndex = 457;
            this.CNOTYPE.SelectedIndexChanged += new System.EventHandler(this.CNOTYPE_SelectedIndexChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(435, 50);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(33, 15);
            this.label8.TabIndex = 446;
            this.label8.Text = "Uom";
            // 
            // txtuom
            // 
            this.txtuom.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtuom.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtuom.Location = new System.Drawing.Point(438, 70);
            this.txtuom.Margin = new System.Windows.Forms.Padding(4);
            this.txtuom.Name = "txtuom";
            this.txtuom.Size = new System.Drawing.Size(76, 23);
            this.txtuom.TabIndex = 445;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(721, 50);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(31, 15);
            this.label7.TabIndex = 444;
            this.label7.Text = "Bags";
            // 
            // txtbags
            // 
            this.txtbags.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtbags.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtbags.Location = new System.Drawing.Point(724, 70);
            this.txtbags.Margin = new System.Windows.Forms.Padding(4);
            this.txtbags.Name = "txtbags";
            this.txtbags.Size = new System.Drawing.Size(88, 23);
            this.txtbags.TabIndex = 436;
            this.txtbags.TextChanged += new System.EventHandler(this.txtbags_TextChanged_1);
            this.txtbags.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtbags_KeyDown);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(618, 50);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(34, 15);
            this.label6.TabIndex = 442;
            this.label6.Text = "Price";
            // 
            // txtprice
            // 
            this.txtprice.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtprice.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtprice.Location = new System.Drawing.Point(621, 70);
            this.txtprice.Margin = new System.Windows.Forms.Padding(4);
            this.txtprice.Name = "txtprice";
            this.txtprice.Size = new System.Drawing.Size(95, 23);
            this.txtprice.TabIndex = 435;
            // 
            // grSearch
            // 
            this.grSearch.BackColor = System.Drawing.Color.White;
            this.grSearch.Controls.Add(this.button18);
            this.grSearch.Controls.Add(this.btnHide);
            this.grSearch.Controls.Add(this.DataGridCommon);
            this.grSearch.Location = new System.Drawing.Point(226, 97);
            this.grSearch.Name = "grSearch";
            this.grSearch.Size = new System.Drawing.Size(426, 306);
            this.grSearch.TabIndex = 438;
            this.grSearch.Visible = false;
            // 
            // button18
            // 
            this.button18.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button18.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button18.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button18.Location = new System.Drawing.Point(213, 269);
            this.button18.Name = "button18";
            this.button18.Size = new System.Drawing.Size(100, 28);
            this.button18.TabIndex = 394;
            this.button18.Text = "Select (F2)";
            this.button18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button18.UseVisualStyleBackColor = false;
            this.button18.Click += new System.EventHandler(this.button18_Click);
            // 
            // btnHide
            // 
            this.btnHide.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnHide.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHide.Image = ((System.Drawing.Image)(resources.GetObject("btnHide.Image")));
            this.btnHide.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnHide.Location = new System.Drawing.Point(323, 270);
            this.btnHide.Name = "btnHide";
            this.btnHide.Size = new System.Drawing.Size(100, 27);
            this.btnHide.TabIndex = 393;
            this.btnHide.Text = "Close (F10)";
            this.btnHide.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnHide.UseVisualStyleBackColor = false;
            this.btnHide.Click += new System.EventHandler(this.btnHide_Click);
            // 
            // DataGridCommon
            // 
            this.DataGridCommon.AllowUserToAddRows = false;
            this.DataGridCommon.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.DataGridCommon.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridCommon.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.DataGridCommon.Location = new System.Drawing.Point(3, 3);
            this.DataGridCommon.Name = "DataGridCommon";
            this.DataGridCommon.ReadOnly = true;
            this.DataGridCommon.RowHeadersVisible = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.DataGridCommon.RowsDefaultCellStyle = dataGridViewCellStyle1;
            this.DataGridCommon.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridCommon.Size = new System.Drawing.Size(420, 264);
            this.DataGridCommon.TabIndex = 0;
            this.DataGridCommon.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridCommon_CellContentClick_1);
            this.DataGridCommon.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DataGridCommon_KeyDown);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(514, 50);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(27, 15);
            this.label4.TabIndex = 437;
            this.label4.Text = "Qty";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(14, 53);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(33, 15);
            this.label5.TabIndex = 436;
            this.label5.Text = "Item";
            // 
            // buttrqok
            // 
            this.buttrqok.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttrqok.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttrqok.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.buttrqok.Location = new System.Drawing.Point(819, 60);
            this.buttrqok.Name = "buttrqok";
            this.buttrqok.Size = new System.Drawing.Size(39, 37);
            this.buttrqok.TabIndex = 437;
            this.buttrqok.Text = "OK";
            this.buttrqok.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttrqok.UseVisualStyleBackColor = false;
            this.buttrqok.Click += new System.EventHandler(this.buttrqok_Click_1);
            // 
            // HFGP
            // 
            this.HFGP.AllowUserToDeleteRows = false;
            this.HFGP.AllowUserToOrderColumns = true;
            this.HFGP.BackgroundColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.HFGP.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.HFGP.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.HFGP.Location = new System.Drawing.Point(9, 103);
            this.HFGP.Name = "HFGP";
            this.HFGP.Size = new System.Drawing.Size(849, 380);
            this.HFGP.TabIndex = 432;
            this.HFGP.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.HFGP_CellClick);
            this.HFGP.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.HFGP_CellContentClick_1);
            this.HFGP.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.HFGP_CellValueChanged);
            this.HFGP.KeyDown += new System.Windows.Forms.KeyEventHandler(this.HFGP_KeyDown);
            // 
            // txtitem
            // 
            this.txtitem.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtitem.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtitem.Location = new System.Drawing.Point(7, 70);
            this.txtitem.Margin = new System.Windows.Forms.Padding(4);
            this.txtitem.Name = "txtitem";
            this.txtitem.Size = new System.Drawing.Size(426, 23);
            this.txtitem.TabIndex = 433;
            this.txtitem.Click += new System.EventHandler(this.txtitem_Click);
            this.txtitem.TextChanged += new System.EventHandler(this.txtitem_TextChanged_1);
            this.txtitem.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtitem_KeyDown);
            // 
            // txtop
            // 
            this.txtop.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtop.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtop.Location = new System.Drawing.Point(517, 70);
            this.txtop.Margin = new System.Windows.Forms.Padding(4);
            this.txtop.Name = "txtop";
            this.txtop.Size = new System.Drawing.Size(96, 23);
            this.txtop.TabIndex = 434;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(3, 13);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(109, 19);
            this.label2.TabIndex = 136;
            this.label2.Text = "Opening Stock";
            // 
            // txtpuid
            // 
            this.txtpuid.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtpuid.Location = new System.Drawing.Point(746, 190);
            this.txtpuid.Margin = new System.Windows.Forms.Padding(4);
            this.txtpuid.Name = "txtpuid";
            this.txtpuid.Size = new System.Drawing.Size(25, 22);
            this.txtpuid.TabIndex = 87;
            // 
            // txtitid
            // 
            this.txtitid.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtitid.Location = new System.Drawing.Point(760, 186);
            this.txtitid.Margin = new System.Windows.Forms.Padding(4);
            this.txtitid.Name = "txtitid";
            this.txtitid.Size = new System.Drawing.Size(22, 22);
            this.txtitid.TabIndex = 89;
            this.txtitid.TextChanged += new System.EventHandler(this.txtitid_TextChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(779, 212);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(54, 15);
            this.label9.TabIndex = 447;
            this.label9.Text = "Ref Date";
            // 
            // Dtpdt
            // 
            this.Dtpdt.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Dtpdt.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.Dtpdt.Location = new System.Drawing.Point(683, 230);
            this.Dtpdt.Name = "Dtpdt";
            this.Dtpdt.Size = new System.Drawing.Size(99, 23);
            this.Dtpdt.TabIndex = 448;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(27, 169);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 15);
            this.label3.TabIndex = 440;
            this.label3.Text = "Itemcode";
            // 
            // txtitcode
            // 
            this.txtitcode.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtitcode.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtitcode.Location = new System.Drawing.Point(28, 189);
            this.txtitcode.Margin = new System.Windows.Forms.Padding(4);
            this.txtitcode.Name = "txtitcode";
            this.txtitcode.Size = new System.Drawing.Size(207, 23);
            this.txtitcode.TabIndex = 439;
            // 
            // FrmOpstk
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(872, 574);
            this.Controls.Add(this.panadd);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Genpan);
            this.Name = "FrmOpstk";
            this.Text = "Opening Stock Details";
            this.Load += new System.EventHandler(this.FrmOpstk_Load);
            this.panadd.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.Genpan.ResumeLayout(false);
            this.Genpan.PerformLayout();
            this.grSearch.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DataGridCommon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HFGP)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Panel panadd;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblno1;
        private System.Windows.Forms.Label lblno2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Button buttnnvfst;
        private System.Windows.Forms.Button buttnnxtlft;
        private System.Windows.Forms.Button btnfinnxt;
        private System.Windows.Forms.Button buttrnxt;
        private System.Windows.Forms.Button buttnext1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel Genpan;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtpuid;
        private System.Windows.Forms.TextBox txtitid;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtuom;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtbags;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtprice;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtitcode;
        private System.Windows.Forms.Panel grSearch;
        private System.Windows.Forms.Button button18;
        private System.Windows.Forms.Button btnHide;
        private System.Windows.Forms.DataGridView DataGridCommon;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button buttrqok;
        private System.Windows.Forms.DataGridView HFGP;
        private System.Windows.Forms.TextBox txtitem;
        private System.Windows.Forms.TextBox txtop;
        private System.Windows.Forms.DateTimePicker Dtpdt;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox CNOTYPE;
        private System.Windows.Forms.Label label10;
    }
}