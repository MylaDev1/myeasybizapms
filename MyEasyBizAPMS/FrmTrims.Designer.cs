﻿namespace MyEasyBizAPMS
{
    partial class FrmTrims
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Syncfusion.Windows.Forms.Tools.SplitButtonRenderer splitButtonRenderer1 = new Syncfusion.Windows.Forms.Tools.SplitButtonRenderer();
            Syncfusion.Windows.Forms.Tools.SplitButtonRenderer splitButtonRenderer2 = new Syncfusion.Windows.Forms.Tools.SplitButtonRenderer();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.BtnSave = new Syncfusion.Windows.Forms.Tools.SplitButton();
            this.toolBack = new Syncfusion.Windows.Forms.Tools.toolstripitem();
            this.SplitAdd = new Syncfusion.Windows.Forms.Tools.SplitButton();
            this.toolstripEdit = new Syncfusion.Windows.Forms.Tools.toolstripitem();
            this.toolstripClose = new Syncfusion.Windows.Forms.Tools.toolstripitem();
            this.grBack = new System.Windows.Forms.GroupBox();
            this.ChckActive = new System.Windows.Forms.CheckBox();
            this.CmbUom = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.CmbTax = new System.Windows.Forms.ComboBox();
            this.CmbCategory = new System.Windows.Forms.ComboBox();
            this.CmbClassification = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtShortName = new System.Windows.Forms.TextBox();
            this.txtItemName = new System.Windows.Forms.RichTextBox();
            this.tabControlAdv1 = new Syncfusion.Windows.Forms.Tools.TabControlAdv();
            this.tabAttribute = new Syncfusion.Windows.Forms.Tools.TabPageAdv();
            this.CmbAttributeAdd = new System.Windows.Forms.ComboBox();
            this.BtnAttributeOk = new System.Windows.Forms.Button();
            this.label13 = new System.Windows.Forms.Label();
            this.CmbSortOrder = new System.Windows.Forms.ComboBox();
            this.chckUserefined = new System.Windows.Forms.CheckBox();
            this.label12 = new System.Windows.Forms.Label();
            this.DataGridAttribute = new System.Windows.Forms.DataGridView();
            this.txtAttribute = new System.Windows.Forms.TextBox();
            this.grSearch = new System.Windows.Forms.GroupBox();
            this.btnSelect = new System.Windows.Forms.Button();
            this.DataGridCommon = new System.Windows.Forms.DataGridView();
            this.btnHide = new System.Windows.Forms.Button();
            this.tabAttributeValue = new Syncfusion.Windows.Forms.Tools.TabPageAdv();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.txtSlno = new System.Windows.Forms.TextBox();
            this.BtnOkAttributeVal = new System.Windows.Forms.Button();
            this.txtAttributeval = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.CmbAttribute = new System.Windows.Forms.ComboBox();
            this.DataGridAttributeValue = new System.Windows.Forms.DataGridView();
            this.tabUom = new Syncfusion.Windows.Forms.Tools.TabPageAdv();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.GrSearchUom = new System.Windows.Forms.GroupBox();
            this.txtSearchUom = new System.Windows.Forms.TextBox();
            this.BtnSelectSearch = new System.Windows.Forms.Button();
            this.DataGridSearchUom = new System.Windows.Forms.DataGridView();
            this.BtnSearchClose = new System.Windows.Forms.Button();
            this.CmbPalnningUom = new System.Windows.Forms.ComboBox();
            this.CmbSalesUom = new System.Windows.Forms.ComboBox();
            this.CmbPurchaseUom = new System.Windows.Forms.ComboBox();
            this.CmbStockUom = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.DataGridAlternateUom = new System.Windows.Forms.DataGridView();
            this.button1 = new System.Windows.Forms.Button();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.grFront = new System.Windows.Forms.GroupBox();
            this.SfDataGridTrims = new Syncfusion.WinForms.DataGrid.SfDataGrid();
            this.grBack.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tabControlAdv1)).BeginInit();
            this.tabControlAdv1.SuspendLayout();
            this.tabAttribute.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridAttribute)).BeginInit();
            this.grSearch.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridCommon)).BeginInit();
            this.tabAttributeValue.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridAttributeValue)).BeginInit();
            this.tabUom.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.GrSearchUom.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridSearchUom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridAlternateUom)).BeginInit();
            this.grFront.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SfDataGridTrims)).BeginInit();
            this.SuspendLayout();
            // 
            // BtnSave
            // 
            this.BtnSave.BackColor = System.Drawing.SystemColors.Control;
            this.BtnSave.BeforeTouchSize = new System.Drawing.Size(80, 30);
            this.BtnSave.DropDownItems.Add(this.toolBack);
            this.BtnSave.DropDownPosition = Syncfusion.Windows.Forms.Tools.Position.Bottom;
            this.BtnSave.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.ForeColor = System.Drawing.Color.Black;
            this.BtnSave.Location = new System.Drawing.Point(725, 499);
            this.BtnSave.MinimumSize = new System.Drawing.Size(75, 23);
            this.BtnSave.Name = "BtnSave";
            splitButtonRenderer1.SplitButton = this.BtnSave;
            this.BtnSave.Renderer = splitButtonRenderer1;
            this.BtnSave.ShowDropDownOnButtonClick = false;
            this.BtnSave.Size = new System.Drawing.Size(80, 30);
            this.BtnSave.TabIndex = 11;
            this.BtnSave.Text = "Save";
            this.BtnSave.DropDowItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.BtnSave_DropDowItemClicked);
            this.BtnSave.Click += new System.EventHandler(this.BtnSave_Click);
            // 
            // toolBack
            // 
            this.toolBack.Name = "toolBack";
            this.toolBack.Size = new System.Drawing.Size(23, 23);
            this.toolBack.Text = "Back";
            // 
            // SplitAdd
            // 
            this.SplitAdd.BackColor = System.Drawing.SystemColors.Control;
            this.SplitAdd.BeforeTouchSize = new System.Drawing.Size(75, 30);
            this.SplitAdd.DropDownItems.Add(this.toolstripEdit);
            this.SplitAdd.DropDownItems.Add(this.toolstripClose);
            this.SplitAdd.DropDownPosition = Syncfusion.Windows.Forms.Tools.Position.Bottom;
            this.SplitAdd.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SplitAdd.ForeColor = System.Drawing.Color.Black;
            this.SplitAdd.Location = new System.Drawing.Point(736, 496);
            this.SplitAdd.MinimumSize = new System.Drawing.Size(75, 23);
            this.SplitAdd.Name = "SplitAdd";
            splitButtonRenderer2.SplitButton = this.SplitAdd;
            this.SplitAdd.Renderer = splitButtonRenderer2;
            this.SplitAdd.ShowDropDownOnButtonClick = false;
            this.SplitAdd.Size = new System.Drawing.Size(75, 30);
            this.SplitAdd.TabIndex = 1;
            this.SplitAdd.Text = "Add";
            this.SplitAdd.DropDowItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.SplitAdd_DropDowItemClicked);
            this.SplitAdd.Click += new System.EventHandler(this.SplitAdd_Click);
            // 
            // toolstripEdit
            // 
            this.toolstripEdit.Name = "toolstripEdit";
            this.toolstripEdit.Size = new System.Drawing.Size(23, 23);
            this.toolstripEdit.Text = "Edit";
            // 
            // toolstripClose
            // 
            this.toolstripClose.Name = "toolstripClose";
            this.toolstripClose.Size = new System.Drawing.Size(23, 23);
            this.toolstripClose.Text = "Close";
            // 
            // grBack
            // 
            this.grBack.Controls.Add(this.ChckActive);
            this.grBack.Controls.Add(this.CmbUom);
            this.grBack.Controls.Add(this.label6);
            this.grBack.Controls.Add(this.CmbTax);
            this.grBack.Controls.Add(this.CmbCategory);
            this.grBack.Controls.Add(this.CmbClassification);
            this.grBack.Controls.Add(this.BtnSave);
            this.grBack.Controls.Add(this.label5);
            this.grBack.Controls.Add(this.label4);
            this.grBack.Controls.Add(this.label3);
            this.grBack.Controls.Add(this.label2);
            this.grBack.Controls.Add(this.label1);
            this.grBack.Controls.Add(this.txtShortName);
            this.grBack.Controls.Add(this.txtItemName);
            this.grBack.Controls.Add(this.tabControlAdv1);
            this.grBack.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grBack.Location = new System.Drawing.Point(6, -2);
            this.grBack.Name = "grBack";
            this.grBack.Size = new System.Drawing.Size(815, 535);
            this.grBack.TabIndex = 0;
            this.grBack.TabStop = false;
            // 
            // ChckActive
            // 
            this.ChckActive.AutoSize = true;
            this.ChckActive.Location = new System.Drawing.Point(740, 139);
            this.ChckActive.Name = "ChckActive";
            this.ChckActive.Size = new System.Drawing.Size(65, 22);
            this.ChckActive.TabIndex = 6;
            this.ChckActive.Text = "Active";
            this.ChckActive.UseVisualStyleBackColor = true;
            this.ChckActive.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Control_KeyUp);
            // 
            // CmbUom
            // 
            this.CmbUom.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CmbUom.FormattingEnabled = true;
            this.CmbUom.Location = new System.Drawing.Point(121, 132);
            this.CmbUom.Name = "CmbUom";
            this.CmbUom.Size = new System.Drawing.Size(279, 26);
            this.CmbUom.TabIndex = 4;
            this.CmbUom.SelectedIndexChanged += new System.EventHandler(this.CmbUom_SelectedIndexChanged);
            this.CmbUom.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Control_KeyUp);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(50, 135);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(69, 18);
            this.label6.TabIndex = 15;
            this.label6.Text = "Base Uom";
            // 
            // CmbTax
            // 
            this.CmbTax.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CmbTax.FormattingEnabled = true;
            this.CmbTax.Location = new System.Drawing.Point(121, 98);
            this.CmbTax.Name = "CmbTax";
            this.CmbTax.Size = new System.Drawing.Size(279, 26);
            this.CmbTax.TabIndex = 3;
            this.CmbTax.SelectedIndexChanged += new System.EventHandler(this.CmbTax_SelectedIndexChanged);
            this.CmbTax.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Control_KeyUp);
            // 
            // CmbCategory
            // 
            this.CmbCategory.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CmbCategory.FormattingEnabled = true;
            this.CmbCategory.Location = new System.Drawing.Point(121, 62);
            this.CmbCategory.Name = "CmbCategory";
            this.CmbCategory.Size = new System.Drawing.Size(279, 26);
            this.CmbCategory.TabIndex = 2;
            this.CmbCategory.SelectedIndexChanged += new System.EventHandler(this.CmbCategory_SelectedIndexChanged);
            this.CmbCategory.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Control_KeyUp);
            // 
            // CmbClassification
            // 
            this.CmbClassification.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CmbClassification.FormattingEnabled = true;
            this.CmbClassification.Location = new System.Drawing.Point(121, 29);
            this.CmbClassification.Name = "CmbClassification";
            this.CmbClassification.Size = new System.Drawing.Size(279, 26);
            this.CmbClassification.TabIndex = 0;
            this.CmbClassification.SelectedIndexChanged += new System.EventHandler(this.CmbClassification_SelectedIndexChanged);
            this.CmbClassification.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Control_KeyUp);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(418, 106);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(81, 18);
            this.label5.TabIndex = 9;
            this.label5.Text = "Short Name";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(421, 29);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(77, 18);
            this.label4.TabIndex = 8;
            this.label4.Text = "Item Name";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(88, 102);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(31, 18);
            this.label3.TabIndex = 7;
            this.label3.Text = "TAX";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(47, 66);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(72, 18);
            this.label2.TabIndex = 6;
            this.label2.Text = "CATEGORY";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 33);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(107, 18);
            this.label1.TabIndex = 5;
            this.label1.Text = "CLASSIFICATION";
            // 
            // txtShortName
            // 
            this.txtShortName.Location = new System.Drawing.Point(504, 102);
            this.txtShortName.Name = "txtShortName";
            this.txtShortName.Size = new System.Drawing.Size(303, 26);
            this.txtShortName.TabIndex = 5;
            this.txtShortName.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Control_KeyUp);
            // 
            // txtItemName
            // 
            this.txtItemName.Location = new System.Drawing.Point(504, 25);
            this.txtItemName.Name = "txtItemName";
            this.txtItemName.ReadOnly = true;
            this.txtItemName.Size = new System.Drawing.Size(303, 70);
            this.txtItemName.TabIndex = 22;
            this.txtItemName.Text = "";
            // 
            // tabControlAdv1
            // 
            this.tabControlAdv1.ActiveTabForeColor = System.Drawing.Color.Empty;
            this.tabControlAdv1.BeforeTouchSize = new System.Drawing.Size(796, 326);
            this.tabControlAdv1.CloseButtonForeColor = System.Drawing.Color.Empty;
            this.tabControlAdv1.CloseButtonHoverForeColor = System.Drawing.Color.Empty;
            this.tabControlAdv1.CloseButtonPressedForeColor = System.Drawing.Color.Empty;
            this.tabControlAdv1.Controls.Add(this.tabAttribute);
            this.tabControlAdv1.Controls.Add(this.tabAttributeValue);
            this.tabControlAdv1.Controls.Add(this.tabUom);
            this.tabControlAdv1.InActiveTabForeColor = System.Drawing.Color.Empty;
            this.tabControlAdv1.Location = new System.Drawing.Point(11, 167);
            this.tabControlAdv1.Name = "tabControlAdv1";
            this.tabControlAdv1.SeparatorColor = System.Drawing.SystemColors.ControlDark;
            this.tabControlAdv1.ShowSeparator = false;
            this.tabControlAdv1.Size = new System.Drawing.Size(796, 326);
            this.tabControlAdv1.TabIndex = 7;
            this.tabControlAdv1.SelectedIndexChanged += new System.EventHandler(this.TabControlAdv1_SelectedIndexChanged);
            // 
            // tabAttribute
            // 
            this.tabAttribute.Controls.Add(this.CmbAttributeAdd);
            this.tabAttribute.Controls.Add(this.BtnAttributeOk);
            this.tabAttribute.Controls.Add(this.label13);
            this.tabAttribute.Controls.Add(this.CmbSortOrder);
            this.tabAttribute.Controls.Add(this.chckUserefined);
            this.tabAttribute.Controls.Add(this.label12);
            this.tabAttribute.Controls.Add(this.DataGridAttribute);
            this.tabAttribute.Controls.Add(this.txtAttribute);
            this.tabAttribute.Controls.Add(this.grSearch);
            this.tabAttribute.Image = null;
            this.tabAttribute.ImageSize = new System.Drawing.Size(16, 16);
            this.tabAttribute.Location = new System.Drawing.Point(1, 30);
            this.tabAttribute.Name = "tabAttribute";
            this.tabAttribute.ShowCloseButton = true;
            this.tabAttribute.Size = new System.Drawing.Size(793, 294);
            this.tabAttribute.TabIndex = 1;
            this.tabAttribute.Text = "ATTRIBUTE";
            this.tabAttribute.ThemesEnabled = false;
            // 
            // CmbAttributeAdd
            // 
            this.CmbAttributeAdd.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CmbAttributeAdd.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CmbAttributeAdd.FormattingEnabled = true;
            this.CmbAttributeAdd.Location = new System.Drawing.Point(13, 29);
            this.CmbAttributeAdd.Name = "CmbAttributeAdd";
            this.CmbAttributeAdd.Size = new System.Drawing.Size(317, 23);
            this.CmbAttributeAdd.TabIndex = 6;
            // 
            // BtnAttributeOk
            // 
            this.BtnAttributeOk.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnAttributeOk.Location = new System.Drawing.Point(455, 28);
            this.BtnAttributeOk.Name = "BtnAttributeOk";
            this.BtnAttributeOk.Size = new System.Drawing.Size(33, 25);
            this.BtnAttributeOk.TabIndex = 3;
            this.BtnAttributeOk.Text = "Ok";
            this.BtnAttributeOk.UseVisualStyleBackColor = true;
            this.BtnAttributeOk.Click += new System.EventHandler(this.BtnAttributeOk_Click);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(331, 11);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(64, 15);
            this.label13.TabIndex = 5;
            this.label13.Text = "Sort Order";
            // 
            // CmbSortOrder
            // 
            this.CmbSortOrder.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CmbSortOrder.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CmbSortOrder.FormattingEnabled = true;
            this.CmbSortOrder.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10"});
            this.CmbSortOrder.Location = new System.Drawing.Point(331, 29);
            this.CmbSortOrder.Name = "CmbSortOrder";
            this.CmbSortOrder.Size = new System.Drawing.Size(124, 23);
            this.CmbSortOrder.TabIndex = 2;
            // 
            // chckUserefined
            // 
            this.chckUserefined.AutoSize = true;
            this.chckUserefined.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chckUserefined.Location = new System.Drawing.Point(331, 31);
            this.chckUserefined.Name = "chckUserefined";
            this.chckUserefined.Size = new System.Drawing.Size(102, 19);
            this.chckUserefined.TabIndex = 1;
            this.chckUserefined.Text = "User Definned";
            this.chckUserefined.UseVisualStyleBackColor = true;
            this.chckUserefined.Visible = false;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(14, 13);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(55, 15);
            this.label12.TabIndex = 1;
            this.label12.Text = "Attribute";
            // 
            // DataGridAttribute
            // 
            this.DataGridAttribute.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.DeepSkyBlue;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DataGridAttribute.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.DataGridAttribute.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridAttribute.EnableHeadersVisualStyles = false;
            this.DataGridAttribute.Location = new System.Drawing.Point(12, 55);
            this.DataGridAttribute.Name = "DataGridAttribute";
            this.DataGridAttribute.RowHeadersVisible = false;
            this.DataGridAttribute.Size = new System.Drawing.Size(476, 236);
            this.DataGridAttribute.TabIndex = 0;
            this.DataGridAttribute.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridAttribute_CellEnter);
            this.DataGridAttribute.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DataGridAttribute_KeyDown);
            // 
            // txtAttribute
            // 
            this.txtAttribute.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAttribute.Location = new System.Drawing.Point(13, 29);
            this.txtAttribute.Name = "txtAttribute";
            this.txtAttribute.Size = new System.Drawing.Size(315, 23);
            this.txtAttribute.TabIndex = 0;
            this.txtAttribute.Click += new System.EventHandler(this.TxtAttribute_Click);
            this.txtAttribute.TextChanged += new System.EventHandler(this.TxtAttribute_TextChanged);
            // 
            // grSearch
            // 
            this.grSearch.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.grSearch.Controls.Add(this.btnSelect);
            this.grSearch.Controls.Add(this.DataGridCommon);
            this.grSearch.Controls.Add(this.btnHide);
            this.grSearch.Location = new System.Drawing.Point(12, 55);
            this.grSearch.Name = "grSearch";
            this.grSearch.Size = new System.Drawing.Size(287, 238);
            this.grSearch.TabIndex = 0;
            this.grSearch.TabStop = false;
            this.grSearch.Visible = false;
            // 
            // btnSelect
            // 
            this.btnSelect.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnSelect.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSelect.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnSelect.Location = new System.Drawing.Point(222, 208);
            this.btnSelect.Name = "btnSelect";
            this.btnSelect.Size = new System.Drawing.Size(61, 28);
            this.btnSelect.TabIndex = 396;
            this.btnSelect.Text = "Select";
            this.btnSelect.UseVisualStyleBackColor = false;
            this.btnSelect.Click += new System.EventHandler(this.BtnSelect_Click);
            // 
            // DataGridCommon
            // 
            this.DataGridCommon.AllowUserToAddRows = false;
            this.DataGridCommon.BackgroundColor = System.Drawing.Color.White;
            this.DataGridCommon.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridCommon.Location = new System.Drawing.Point(4, 15);
            this.DataGridCommon.MultiSelect = false;
            this.DataGridCommon.Name = "DataGridCommon";
            this.DataGridCommon.ReadOnly = true;
            this.DataGridCommon.RowHeadersVisible = false;
            this.DataGridCommon.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridCommon.Size = new System.Drawing.Size(279, 193);
            this.DataGridCommon.TabIndex = 0;
            this.DataGridCommon.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.DataGridCommon_CellMouseDoubleClick);
            this.DataGridCommon.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DataGridCommon_KeyDown);
            // 
            // btnHide
            // 
            this.btnHide.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnHide.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHide.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnHide.Location = new System.Drawing.Point(5, 208);
            this.btnHide.Name = "btnHide";
            this.btnHide.Size = new System.Drawing.Size(65, 28);
            this.btnHide.TabIndex = 395;
            this.btnHide.Text = "Close";
            this.btnHide.UseVisualStyleBackColor = false;
            this.btnHide.Click += new System.EventHandler(this.BtnHide_Click);
            // 
            // tabAttributeValue
            // 
            this.tabAttributeValue.Controls.Add(this.label15);
            this.tabAttributeValue.Controls.Add(this.label14);
            this.tabAttributeValue.Controls.Add(this.txtSlno);
            this.tabAttributeValue.Controls.Add(this.BtnOkAttributeVal);
            this.tabAttributeValue.Controls.Add(this.txtAttributeval);
            this.tabAttributeValue.Controls.Add(this.label7);
            this.tabAttributeValue.Controls.Add(this.CmbAttribute);
            this.tabAttributeValue.Controls.Add(this.DataGridAttributeValue);
            this.tabAttributeValue.Image = null;
            this.tabAttributeValue.ImageSize = new System.Drawing.Size(16, 16);
            this.tabAttributeValue.Location = new System.Drawing.Point(1, 30);
            this.tabAttributeValue.Name = "tabAttributeValue";
            this.tabAttributeValue.ShowCloseButton = true;
            this.tabAttributeValue.Size = new System.Drawing.Size(793, 294);
            this.tabAttributeValue.TabIndex = 2;
            this.tabAttributeValue.Text = "ATTRIBUTE VALUE";
            this.tabAttributeValue.ThemesEnabled = false;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(79, 41);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(88, 15);
            this.label15.TabIndex = 22;
            this.label15.Text = "Attribute Value";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(12, 41);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(32, 15);
            this.label14.TabIndex = 21;
            this.label14.Text = "SlNo";
            // 
            // txtSlno
            // 
            this.txtSlno.Location = new System.Drawing.Point(9, 59);
            this.txtSlno.Name = "txtSlno";
            this.txtSlno.Size = new System.Drawing.Size(68, 26);
            this.txtSlno.TabIndex = 20;
            // 
            // BtnOkAttributeVal
            // 
            this.BtnOkAttributeVal.Location = new System.Drawing.Point(444, 58);
            this.BtnOkAttributeVal.Name = "BtnOkAttributeVal";
            this.BtnOkAttributeVal.Size = new System.Drawing.Size(43, 28);
            this.BtnOkAttributeVal.TabIndex = 19;
            this.BtnOkAttributeVal.Text = "Ok";
            this.BtnOkAttributeVal.UseVisualStyleBackColor = true;
            this.BtnOkAttributeVal.Click += new System.EventHandler(this.BtnOkAttributeVal_Click);
            // 
            // txtAttributeval
            // 
            this.txtAttributeval.Location = new System.Drawing.Point(79, 59);
            this.txtAttributeval.Name = "txtAttributeval";
            this.txtAttributeval.Size = new System.Drawing.Size(365, 26);
            this.txtAttributeval.TabIndex = 18;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(18, 12);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(55, 15);
            this.label7.TabIndex = 17;
            this.label7.Text = "Attribute";
            // 
            // CmbAttribute
            // 
            this.CmbAttribute.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CmbAttribute.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CmbAttribute.FormattingEnabled = true;
            this.CmbAttribute.Location = new System.Drawing.Point(79, 9);
            this.CmbAttribute.Name = "CmbAttribute";
            this.CmbAttribute.Size = new System.Drawing.Size(279, 23);
            this.CmbAttribute.TabIndex = 2;
            this.CmbAttribute.SelectedIndexChanged += new System.EventHandler(this.CmbAttribute_SelectedIndexChanged);
            // 
            // DataGridAttributeValue
            // 
            this.DataGridAttributeValue.BackgroundColor = System.Drawing.Color.White;
            this.DataGridAttributeValue.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridAttributeValue.Location = new System.Drawing.Point(9, 88);
            this.DataGridAttributeValue.Name = "DataGridAttributeValue";
            this.DataGridAttributeValue.RowHeadersVisible = false;
            this.DataGridAttributeValue.Size = new System.Drawing.Size(478, 201);
            this.DataGridAttributeValue.TabIndex = 1;
            // 
            // tabUom
            // 
            this.tabUom.Controls.Add(this.groupBox1);
            this.tabUom.Image = null;
            this.tabUom.ImageSize = new System.Drawing.Size(16, 16);
            this.tabUom.Location = new System.Drawing.Point(1, 30);
            this.tabUom.Name = "tabUom";
            this.tabUom.ShowCloseButton = true;
            this.tabUom.Size = new System.Drawing.Size(793, 294);
            this.tabUom.TabIndex = 3;
            this.tabUom.Text = "Alternate UOM";
            this.tabUom.ThemesEnabled = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.GrSearchUom);
            this.groupBox1.Controls.Add(this.CmbPalnningUom);
            this.groupBox1.Controls.Add(this.CmbSalesUom);
            this.groupBox1.Controls.Add(this.CmbPurchaseUom);
            this.groupBox1.Controls.Add(this.CmbStockUom);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.DataGridAlternateUom);
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.label18);
            this.groupBox1.Controls.Add(this.label17);
            this.groupBox1.Controls.Add(this.label16);
            this.groupBox1.Controls.Add(this.textBox3);
            this.groupBox1.Controls.Add(this.textBox2);
            this.groupBox1.Controls.Add(this.textBox1);
            this.groupBox1.Location = new System.Drawing.Point(6, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(781, 289);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // GrSearchUom
            // 
            this.GrSearchUom.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.GrSearchUom.Controls.Add(this.txtSearchUom);
            this.GrSearchUom.Controls.Add(this.BtnSelectSearch);
            this.GrSearchUom.Controls.Add(this.DataGridSearchUom);
            this.GrSearchUom.Controls.Add(this.BtnSearchClose);
            this.GrSearchUom.Location = new System.Drawing.Point(241, 18);
            this.GrSearchUom.Name = "GrSearchUom";
            this.GrSearchUom.Size = new System.Drawing.Size(290, 238);
            this.GrSearchUom.TabIndex = 1;
            this.GrSearchUom.TabStop = false;
            this.GrSearchUom.Visible = false;
            // 
            // txtSearchUom
            // 
            this.txtSearchUom.Location = new System.Drawing.Point(5, 14);
            this.txtSearchUom.Name = "txtSearchUom";
            this.txtSearchUom.Size = new System.Drawing.Size(276, 26);
            this.txtSearchUom.TabIndex = 0;
            // 
            // BtnSelectSearch
            // 
            this.BtnSelectSearch.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.BtnSelectSearch.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSelectSearch.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.BtnSelectSearch.Location = new System.Drawing.Point(222, 208);
            this.BtnSelectSearch.Name = "BtnSelectSearch";
            this.BtnSelectSearch.Size = new System.Drawing.Size(61, 28);
            this.BtnSelectSearch.TabIndex = 396;
            this.BtnSelectSearch.Text = "Select";
            this.BtnSelectSearch.UseVisualStyleBackColor = false;
            this.BtnSelectSearch.Click += new System.EventHandler(this.BtnSelectSearch_Click);
            // 
            // DataGridSearchUom
            // 
            this.DataGridSearchUom.AllowUserToAddRows = false;
            this.DataGridSearchUom.BackgroundColor = System.Drawing.Color.White;
            this.DataGridSearchUom.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridSearchUom.Location = new System.Drawing.Point(4, 42);
            this.DataGridSearchUom.MultiSelect = false;
            this.DataGridSearchUom.Name = "DataGridSearchUom";
            this.DataGridSearchUom.ReadOnly = true;
            this.DataGridSearchUom.RowHeadersVisible = false;
            this.DataGridSearchUom.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridSearchUom.Size = new System.Drawing.Size(279, 163);
            this.DataGridSearchUom.TabIndex = 0;
            // 
            // BtnSearchClose
            // 
            this.BtnSearchClose.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.BtnSearchClose.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSearchClose.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.BtnSearchClose.Location = new System.Drawing.Point(5, 208);
            this.BtnSearchClose.Name = "BtnSearchClose";
            this.BtnSearchClose.Size = new System.Drawing.Size(65, 28);
            this.BtnSearchClose.TabIndex = 395;
            this.BtnSearchClose.Text = "Close";
            this.BtnSearchClose.UseVisualStyleBackColor = false;
            this.BtnSearchClose.Click += new System.EventHandler(this.BtnSearchClose_Click);
            // 
            // CmbPalnningUom
            // 
            this.CmbPalnningUom.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CmbPalnningUom.FormattingEnabled = true;
            this.CmbPalnningUom.Location = new System.Drawing.Point(560, 160);
            this.CmbPalnningUom.Name = "CmbPalnningUom";
            this.CmbPalnningUom.Size = new System.Drawing.Size(192, 26);
            this.CmbPalnningUom.TabIndex = 8;
            // 
            // CmbSalesUom
            // 
            this.CmbSalesUom.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CmbSalesUom.FormattingEnabled = true;
            this.CmbSalesUom.Location = new System.Drawing.Point(560, 116);
            this.CmbSalesUom.Name = "CmbSalesUom";
            this.CmbSalesUom.Size = new System.Drawing.Size(192, 26);
            this.CmbSalesUom.TabIndex = 7;
            // 
            // CmbPurchaseUom
            // 
            this.CmbPurchaseUom.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CmbPurchaseUom.FormattingEnabled = true;
            this.CmbPurchaseUom.Location = new System.Drawing.Point(560, 68);
            this.CmbPurchaseUom.Name = "CmbPurchaseUom";
            this.CmbPurchaseUom.Size = new System.Drawing.Size(192, 26);
            this.CmbPurchaseUom.TabIndex = 6;
            // 
            // CmbStockUom
            // 
            this.CmbStockUom.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CmbStockUom.FormattingEnabled = true;
            this.CmbStockUom.Location = new System.Drawing.Point(560, 25);
            this.CmbStockUom.Name = "CmbStockUom";
            this.CmbStockUom.Size = new System.Drawing.Size(192, 26);
            this.CmbStockUom.TabIndex = 5;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(492, 164);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(62, 18);
            this.label11.TabIndex = 4;
            this.label11.Text = "Planning";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(514, 120);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(40, 18);
            this.label10.TabIndex = 3;
            this.label10.Text = "Sales";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(490, 72);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(64, 18);
            this.label9.TabIndex = 2;
            this.label9.Text = "Purchase";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(513, 29);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(41, 18);
            this.label8.TabIndex = 1;
            this.label8.Text = "Stock";
            // 
            // DataGridAlternateUom
            // 
            this.DataGridAlternateUom.BackgroundColor = System.Drawing.Color.White;
            this.DataGridAlternateUom.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridAlternateUom.Location = new System.Drawing.Point(6, 18);
            this.DataGridAlternateUom.Name = "DataGridAlternateUom";
            this.DataGridAlternateUom.RowHeadersVisible = false;
            this.DataGridAlternateUom.Size = new System.Drawing.Size(440, 229);
            this.DataGridAlternateUom.TabIndex = 0;
            this.DataGridAlternateUom.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridAlternateUom_CellEnter);
            this.DataGridAlternateUom.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.DataGridAlternateUom_CellMouseClick);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button1.Location = new System.Drawing.Point(405, 38);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(42, 28);
            this.button1.TabIndex = 397;
            this.button1.Text = "Ok";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Visible = false;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(289, 18);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(94, 18);
            this.label18.TabIndex = 401;
            this.label18.Text = "Base Uom Qty";
            this.label18.Visible = false;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(159, 18);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(124, 18);
            this.label17.TabIndex = 400;
            this.label17.Text = "Alternate Uom Qty";
            this.label17.Visible = false;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(6, 18);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(99, 18);
            this.label16.TabIndex = 23;
            this.label16.Text = "Alternate Uom";
            this.label16.Visible = false;
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(313, 39);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(90, 26);
            this.textBox3.TabIndex = 399;
            this.textBox3.Visible = false;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(224, 39);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(84, 26);
            this.textBox2.TabIndex = 398;
            this.textBox2.Visible = false;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(6, 39);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(138, 26);
            this.textBox1.TabIndex = 397;
            this.textBox1.Visible = false;
            // 
            // grFront
            // 
            this.grFront.Controls.Add(this.SplitAdd);
            this.grFront.Controls.Add(this.SfDataGridTrims);
            this.grFront.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grFront.Location = new System.Drawing.Point(6, 1);
            this.grFront.Name = "grFront";
            this.grFront.Size = new System.Drawing.Size(815, 532);
            this.grFront.TabIndex = 12;
            this.grFront.TabStop = false;
            // 
            // SfDataGridTrims
            // 
            this.SfDataGridTrims.AccessibleName = "Table";
            this.SfDataGridTrims.AllowFiltering = true;
            this.SfDataGridTrims.Location = new System.Drawing.Point(6, 22);
            this.SfDataGridTrims.Name = "SfDataGridTrims";
            this.SfDataGridTrims.Size = new System.Drawing.Size(802, 468);
            this.SfDataGridTrims.TabIndex = 0;
            this.SfDataGridTrims.Text = "sfDataGrid1";
            // 
            // FrmTrims
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(829, 542);
            this.Controls.Add(this.grFront);
            this.Controls.Add(this.grBack);
            this.Name = "FrmTrims";
            this.Text = "Trims";
            this.Load += new System.EventHandler(this.FrmTrims_Load);
            this.grBack.ResumeLayout(false);
            this.grBack.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tabControlAdv1)).EndInit();
            this.tabControlAdv1.ResumeLayout(false);
            this.tabAttribute.ResumeLayout(false);
            this.tabAttribute.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridAttribute)).EndInit();
            this.grSearch.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DataGridCommon)).EndInit();
            this.tabAttributeValue.ResumeLayout(false);
            this.tabAttributeValue.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridAttributeValue)).EndInit();
            this.tabUom.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.GrSearchUom.ResumeLayout(false);
            this.GrSearchUom.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridSearchUom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridAlternateUom)).EndInit();
            this.grFront.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.SfDataGridTrims)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grBack;
        private System.Windows.Forms.RichTextBox txtItemName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtShortName;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private Syncfusion.Windows.Forms.Tools.TabControlAdv tabControlAdv1;
        private Syncfusion.Windows.Forms.Tools.TabPageAdv tabAttribute;
        private Syncfusion.Windows.Forms.Tools.TabPageAdv tabAttributeValue;
        private System.Windows.Forms.DataGridView DataGridAttributeValue;
        private Syncfusion.Windows.Forms.Tools.SplitButton BtnSave;
        private Syncfusion.Windows.Forms.Tools.toolstripitem toolBack;
        private System.Windows.Forms.GroupBox grFront;
        private Syncfusion.Windows.Forms.Tools.SplitButton SplitAdd;
        private Syncfusion.WinForms.DataGrid.SfDataGrid SfDataGridTrims;
        private Syncfusion.Windows.Forms.Tools.toolstripitem toolstripEdit;
        private Syncfusion.Windows.Forms.Tools.toolstripitem toolstripClose;
        private System.Windows.Forms.ComboBox CmbTax;
        private System.Windows.Forms.ComboBox CmbCategory;
        private System.Windows.Forms.ComboBox CmbClassification;
        private System.Windows.Forms.DataGridView DataGridAttribute;
        private System.Windows.Forms.ComboBox CmbUom;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.GroupBox grSearch;
        private System.Windows.Forms.Button btnSelect;
        private System.Windows.Forms.DataGridView DataGridCommon;
        private System.Windows.Forms.Button btnHide;
        private Syncfusion.Windows.Forms.Tools.TabPageAdv tabUom;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox CmbAttribute;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView DataGridAlternateUom;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox CmbPalnningUom;
        private System.Windows.Forms.ComboBox CmbSalesUom;
        private System.Windows.Forms.ComboBox CmbPurchaseUom;
        private System.Windows.Forms.ComboBox CmbStockUom;
        private System.Windows.Forms.CheckBox ChckActive;
        private System.Windows.Forms.GroupBox GrSearchUom;
        private System.Windows.Forms.TextBox txtSearchUom;
        private System.Windows.Forms.Button BtnSelectSearch;
        private System.Windows.Forms.DataGridView DataGridSearchUom;
        private System.Windows.Forms.Button BtnSearchClose;
        private System.Windows.Forms.Button BtnAttributeOk;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtAttribute;
        private System.Windows.Forms.ComboBox CmbSortOrder;
        private System.Windows.Forms.CheckBox chckUserefined;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtAttributeval;
        private System.Windows.Forms.Button BtnOkAttributeVal;
        private System.Windows.Forms.TextBox txtSlno;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ComboBox CmbAttributeAdd;
    }
}