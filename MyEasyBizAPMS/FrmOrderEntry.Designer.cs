﻿namespace MyEasyBizAPMS
{
    partial class FrmOrderEntry
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Syncfusion.Windows.Forms.Tools.SplitButtonRenderer splitButtonRenderer3 = new Syncfusion.Windows.Forms.Tools.SplitButtonRenderer();
            Syncfusion.Windows.Forms.Tools.SplitButtonRenderer splitButtonRenderer4 = new Syncfusion.Windows.Forms.Tools.SplitButtonRenderer();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle20 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle21 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle22 = new System.Windows.Forms.DataGridViewCellStyle();
            this.btnAdd = new Syncfusion.Windows.Forms.Tools.SplitButton();
            this.btnSave = new Syncfusion.Windows.Forms.Tools.SplitButton();
            this.grBack = new System.Windows.Forms.GroupBox();
            this.grSearch = new System.Windows.Forms.GroupBox();
            this.btnSelect = new System.Windows.Forms.Button();
            this.DataGridCommon = new System.Windows.Forms.DataGridView();
            this.btnHide = new System.Windows.Forms.Button();
            this.label97 = new System.Windows.Forms.Label();
            this.label96 = new System.Windows.Forms.Label();
            this.label95 = new System.Windows.Forms.Label();
            this.label61 = new System.Windows.Forms.Label();
            this.txtStyleDesc = new System.Windows.Forms.TextBox();
            this.tabControlOrderEntry = new Syncfusion.Windows.Forms.Tools.TabControlAdv();
            this.tabStyleNo = new Syncfusion.Windows.Forms.Tools.TabPageAdv();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label66 = new System.Windows.Forms.Label();
            this.label65 = new System.Windows.Forms.Label();
            this.label64 = new System.Windows.Forms.Label();
            this.label63 = new System.Windows.Forms.Label();
            this.label62 = new System.Windows.Forms.Label();
            this.BtnReferesh = new System.Windows.Forms.Button();
            this.label41 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.btnStyleOk = new System.Windows.Forms.Button();
            this.txtQuantity = new System.Windows.Forms.TextBox();
            this.txtdescription = new System.Windows.Forms.TextBox();
            this.txtStyleNo = new System.Windows.Forms.TextBox();
            this.DataGridStyle = new System.Windows.Forms.DataGridView();
            this.CmbPlanUom = new System.Windows.Forms.ComboBox();
            this.cmbOrderUOm = new System.Windows.Forms.ComboBox();
            this.tabAssortment = new Syncfusion.Windows.Forms.Tools.TabPageAdv();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.label72 = new System.Windows.Forms.Label();
            this.label71 = new System.Windows.Forms.Label();
            this.label70 = new System.Windows.Forms.Label();
            this.label69 = new System.Windows.Forms.Label();
            this.label68 = new System.Windows.Forms.Label();
            this.label67 = new System.Windows.Forms.Label();
            this.grImport = new System.Windows.Forms.GroupBox();
            this.BtnImportHide = new System.Windows.Forms.Button();
            this.CmbAssortImport = new System.Windows.Forms.ComboBox();
            this.CmbStyleImport = new System.Windows.Forms.ComboBox();
            this.lblPath = new System.Windows.Forms.Label();
            this.label60 = new System.Windows.Forms.Label();
            this.label59 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.label54 = new System.Windows.Forms.Label();
            this.label53 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.txtMCBox = new System.Windows.Forms.TextBox();
            this.cmbCountry = new System.Windows.Forms.ComboBox();
            this.btnAssortOk = new System.Windows.Forms.Button();
            this.txtFinalDistination = new System.Windows.Forms.TextBox();
            this.txtPortofDelivery = new System.Windows.Forms.TextBox();
            this.dtpAssortShipDt = new System.Windows.Forms.DateTimePicker();
            this.label34 = new System.Windows.Forms.Label();
            this.cmbStyleAssort = new System.Windows.Forms.ComboBox();
            this.dtpAssortDelDt = new System.Windows.Forms.DateTimePicker();
            this.txtAssortQuantity = new System.Windows.Forms.TextBox();
            this.txtAssortmentType = new System.Windows.Forms.TextBox();
            this.txtRefNo = new System.Windows.Forms.TextBox();
            this.DataGridAssort = new System.Windows.Forms.DataGridView();
            this.tabPageSize = new Syncfusion.Windows.Forms.Tools.TabPageAdv();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label75 = new System.Windows.Forms.Label();
            this.label74 = new System.Windows.Forms.Label();
            this.label73 = new System.Windows.Forms.Label();
            this.txtCombo = new System.Windows.Forms.TextBox();
            this.label45 = new System.Windows.Forms.Label();
            this.txtComboQty = new System.Windows.Forms.TextBox();
            this.label33 = new System.Windows.Forms.Label();
            this.btnComboOk = new System.Windows.Forms.Button();
            this.DataGridStyleSizeCombo = new System.Windows.Forms.DataGridView();
            this.label32 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.CmbStyle = new System.Windows.Forms.ComboBox();
            this.btnStyleSizeOk = new System.Windows.Forms.Button();
            this.txtStyleSize = new System.Windows.Forms.TextBox();
            this.DataGridStyleSize = new System.Windows.Forms.DataGridView();
            this.tabCombo = new Syncfusion.Windows.Forms.Tools.TabPageAdv();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label76 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.cmbQtyStyle = new System.Windows.Forms.ComboBox();
            this.cmbAssort = new System.Windows.Forms.ComboBox();
            this.DataGridCombo = new System.Windows.Forms.DataGridView();
            this.label12 = new System.Windows.Forms.Label();
            this.tabSizeMatrix = new Syncfusion.Windows.Forms.Tools.TabPageAdv();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.label77 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.cmbMatrixStyle = new System.Windows.Forms.ComboBox();
            this.DataGridSizeMatrix = new System.Windows.Forms.DataGridView();
            this.tabPlanning = new Syncfusion.Windows.Forms.Tools.TabPageAdv();
            this.label79 = new System.Windows.Forms.Label();
            this.label78 = new System.Windows.Forms.Label();
            this.ChckRefresh = new System.Windows.Forms.CheckBox();
            this.ChckAll = new System.Windows.Forms.CheckBox();
            this.btnCalculate = new System.Windows.Forms.Button();
            this.txtCuttingTolerance = new System.Windows.Forms.TextBox();
            this.label58 = new System.Windows.Forms.Label();
            this.txtBuyerTolerance = new System.Windows.Forms.TextBox();
            this.label57 = new System.Windows.Forms.Label();
            this.label56 = new System.Windows.Forms.Label();
            this.CmbPlanningStyleCombo = new System.Windows.Forms.ComboBox();
            this.label55 = new System.Windows.Forms.Label();
            this.CmbPlanningStyle = new System.Windows.Forms.ComboBox();
            this.DataGridPlanning = new System.Windows.Forms.DataGridView();
            this.tabPrice = new Syncfusion.Windows.Forms.Tools.TabPageAdv();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.BtnUpdatePrice = new System.Windows.Forms.Button();
            this.label80 = new System.Windows.Forms.Label();
            this.cmbStylePrice = new System.Windows.Forms.ComboBox();
            this.DataGridStylePrice = new System.Windows.Forms.DataGridView();
            this.cmbPriceType = new System.Windows.Forms.ComboBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.tabLogistics = new Syncfusion.Windows.Forms.Tools.TabPageAdv();
            this.LblAvgDate = new System.Windows.Forms.Label();
            this.label91 = new System.Windows.Forms.Label();
            this.label90 = new System.Windows.Forms.Label();
            this.label89 = new System.Windows.Forms.Label();
            this.label88 = new System.Windows.Forms.Label();
            this.label87 = new System.Windows.Forms.Label();
            this.label86 = new System.Windows.Forms.Label();
            this.label85 = new System.Windows.Forms.Label();
            this.label84 = new System.Windows.Forms.Label();
            this.label83 = new System.Windows.Forms.Label();
            this.label82 = new System.Windows.Forms.Label();
            this.label81 = new System.Windows.Forms.Label();
            this.btnDiscountOk = new System.Windows.Forms.Button();
            this.txtDicType = new System.Windows.Forms.TextBox();
            this.txtDisAmount = new System.Windows.Forms.TextBox();
            this.txtDisPer = new System.Windows.Forms.TextBox();
            this.txtDiscount = new System.Windows.Forms.TextBox();
            this.btnChargesOk = new System.Windows.Forms.Button();
            this.txtChargesType = new System.Windows.Forms.TextBox();
            this.txtChargesAmout = new System.Windows.Forms.TextBox();
            this.txtChargesPercetage = new System.Windows.Forms.TextBox();
            this.txtCharges = new System.Windows.Forms.TextBox();
            this.DataGridDiscounts = new System.Windows.Forms.DataGridView();
            this.DataGridCharges = new System.Windows.Forms.DataGridView();
            this.label30 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.txtExRate = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.txtCurrency = new System.Windows.Forms.TextBox();
            this.txtCountry = new System.Windows.Forms.TextBox();
            this.txtContact = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.txtPayTerms = new System.Windows.Forms.TextBox();
            this.txtPayMode = new System.Windows.Forms.TextBox();
            this.txtShiftMode = new System.Windows.Forms.TextBox();
            this.txtShipType = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.txtCrossValue = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.txtAvgRate = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.txtReceivedBy = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.dtpRecivedDt = new System.Windows.Forms.DateTimePicker();
            this.label17 = new System.Windows.Forms.Label();
            this.txtAgent = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.txtDepartment = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.tabFabric = new Syncfusion.Windows.Forms.Tools.TabPageAdv();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.label94 = new System.Windows.Forms.Label();
            this.label93 = new System.Windows.Forms.Label();
            this.label92 = new System.Windows.Forms.Label();
            this.btnFabric = new System.Windows.Forms.Button();
            this.txtTalarance = new System.Windows.Forms.TextBox();
            this.txtGSM = new System.Windows.Forms.TextBox();
            this.txtStructureType = new System.Windows.Forms.TextBox();
            this.DataGridFabricDet = new System.Windows.Forms.DataGridView();
            this.label44 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.cmbYear = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.cmbSeason = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtCustomer = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.dtpOrderDate = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.txtOrderNo = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.dtpDocDate = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.txtDocNo = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtMerchindiser = new System.Windows.Forms.TextBox();
            this.dtpDeliveryDate = new System.Windows.Forms.DateTimePicker();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.cmbOrderType = new System.Windows.Forms.ComboBox();
            this.cmbSts = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.grFront = new System.Windows.Forms.GroupBox();
            this.BtnDownloadtoExcel = new System.Windows.Forms.Button();
            this.btnPrint = new System.Windows.Forms.Button();
            this.SfdDataGridOrderEntry = new Syncfusion.WinForms.DataGrid.SfDataGrid();
            this.grBack.SuspendLayout();
            this.grSearch.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridCommon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabControlOrderEntry)).BeginInit();
            this.tabControlOrderEntry.SuspendLayout();
            this.tabStyleNo.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridStyle)).BeginInit();
            this.tabAssortment.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.grImport.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridAssort)).BeginInit();
            this.tabPageSize.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridStyleSizeCombo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridStyleSize)).BeginInit();
            this.tabCombo.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridCombo)).BeginInit();
            this.tabSizeMatrix.SuspendLayout();
            this.groupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridSizeMatrix)).BeginInit();
            this.tabPlanning.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridPlanning)).BeginInit();
            this.tabPrice.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridStylePrice)).BeginInit();
            this.tabLogistics.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridDiscounts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridCharges)).BeginInit();
            this.tabFabric.SuspendLayout();
            this.groupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridFabricDet)).BeginInit();
            this.grFront.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SfdDataGridOrderEntry)).BeginInit();
            this.SuspendLayout();
            // 
            // btnAdd
            // 
            this.btnAdd.BackColor = System.Drawing.SystemColors.Control;
            this.btnAdd.BeforeTouchSize = new System.Drawing.Size(99, 30);
            this.btnAdd.DropDownPosition = Syncfusion.Windows.Forms.Tools.Position.Bottom;
            this.btnAdd.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAdd.ForeColor = System.Drawing.Color.Black;
            this.btnAdd.Location = new System.Drawing.Point(674, 535);
            this.btnAdd.MinimumSize = new System.Drawing.Size(75, 23);
            this.btnAdd.Name = "btnAdd";
            splitButtonRenderer3.SplitButton = this.btnAdd;
            this.btnAdd.Renderer = splitButtonRenderer3;
            this.btnAdd.ShowDropDownOnButtonClick = false;
            this.btnAdd.Size = new System.Drawing.Size(99, 30);
            this.btnAdd.TabIndex = 1;
            this.btnAdd.Text = "Add New";
            this.btnAdd.DropDowItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.BtnAdd_DropDowItemClicked);
            this.btnAdd.Click += new System.EventHandler(this.BtnAdd_Click);
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.SystemColors.Control;
            this.btnSave.BeforeTouchSize = new System.Drawing.Size(105, 41);
            this.btnSave.DropDownPosition = Syncfusion.Windows.Forms.Tools.Position.Bottom;
            this.btnSave.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.ForeColor = System.Drawing.Color.Black;
            this.btnSave.Location = new System.Drawing.Point(809, 531);
            this.btnSave.MinimumSize = new System.Drawing.Size(75, 23);
            this.btnSave.Name = "btnSave";
            splitButtonRenderer4.SplitButton = this.btnSave;
            this.btnSave.Renderer = splitButtonRenderer4;
            this.btnSave.ShowDropDownOnButtonClick = false;
            this.btnSave.Size = new System.Drawing.Size(105, 41);
            this.btnSave.TabIndex = 23;
            this.btnSave.Text = "Save & Continue";
            this.btnSave.DropDowItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.BtnSave_DropDowItemClicked);
            this.btnSave.Click += new System.EventHandler(this.BtnSave_Click);
            // 
            // grBack
            // 
            this.grBack.Controls.Add(this.grSearch);
            this.grBack.Controls.Add(this.label97);
            this.grBack.Controls.Add(this.label96);
            this.grBack.Controls.Add(this.label95);
            this.grBack.Controls.Add(this.label61);
            this.grBack.Controls.Add(this.txtStyleDesc);
            this.grBack.Controls.Add(this.tabControlOrderEntry);
            this.grBack.Controls.Add(this.cmbYear);
            this.grBack.Controls.Add(this.label7);
            this.grBack.Controls.Add(this.cmbSeason);
            this.grBack.Controls.Add(this.label6);
            this.grBack.Controls.Add(this.txtCustomer);
            this.grBack.Controls.Add(this.label5);
            this.grBack.Controls.Add(this.dtpOrderDate);
            this.grBack.Controls.Add(this.label3);
            this.grBack.Controls.Add(this.txtOrderNo);
            this.grBack.Controls.Add(this.label4);
            this.grBack.Controls.Add(this.dtpDocDate);
            this.grBack.Controls.Add(this.label2);
            this.grBack.Controls.Add(this.txtDocNo);
            this.grBack.Controls.Add(this.label1);
            this.grBack.Controls.Add(this.txtMerchindiser);
            this.grBack.Controls.Add(this.dtpDeliveryDate);
            this.grBack.Controls.Add(this.label8);
            this.grBack.Controls.Add(this.label9);
            this.grBack.Controls.Add(this.btnSave);
            this.grBack.Controls.Add(this.cmbOrderType);
            this.grBack.Controls.Add(this.cmbSts);
            this.grBack.Controls.Add(this.label11);
            this.grBack.Controls.Add(this.label10);
            this.grBack.Location = new System.Drawing.Point(7, 3);
            this.grBack.Name = "grBack";
            this.grBack.Size = new System.Drawing.Size(1134, 574);
            this.grBack.TabIndex = 0;
            this.grBack.TabStop = false;
            this.grBack.Enter += new System.EventHandler(this.GrBack_Enter);
            // 
            // grSearch
            // 
            this.grSearch.Controls.Add(this.btnSelect);
            this.grSearch.Controls.Add(this.DataGridCommon);
            this.grSearch.Controls.Add(this.btnHide);
            this.grSearch.Location = new System.Drawing.Point(568, 11);
            this.grSearch.Name = "grSearch";
            this.grSearch.Size = new System.Drawing.Size(346, 232);
            this.grSearch.TabIndex = 24;
            this.grSearch.TabStop = false;
            this.grSearch.Visible = false;
            // 
            // btnSelect
            // 
            this.btnSelect.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnSelect.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSelect.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnSelect.Location = new System.Drawing.Point(281, 197);
            this.btnSelect.Name = "btnSelect";
            this.btnSelect.Size = new System.Drawing.Size(61, 28);
            this.btnSelect.TabIndex = 396;
            this.btnSelect.Text = "Select";
            this.btnSelect.UseVisualStyleBackColor = false;
            this.btnSelect.Click += new System.EventHandler(this.BtnSelect_Click);
            // 
            // DataGridCommon
            // 
            this.DataGridCommon.AllowUserToAddRows = false;
            this.DataGridCommon.BackgroundColor = System.Drawing.Color.White;
            this.DataGridCommon.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridCommon.Location = new System.Drawing.Point(4, 12);
            this.DataGridCommon.MultiSelect = false;
            this.DataGridCommon.Name = "DataGridCommon";
            this.DataGridCommon.ReadOnly = true;
            this.DataGridCommon.RowHeadersVisible = false;
            this.DataGridCommon.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridCommon.Size = new System.Drawing.Size(336, 183);
            this.DataGridCommon.TabIndex = 0;
            this.DataGridCommon.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.DataGridCommon_CellMouseDoubleClick);
            this.DataGridCommon.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DataGridCommon_KeyDown);
            // 
            // btnHide
            // 
            this.btnHide.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnHide.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHide.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnHide.Location = new System.Drawing.Point(6, 197);
            this.btnHide.Name = "btnHide";
            this.btnHide.Size = new System.Drawing.Size(65, 28);
            this.btnHide.TabIndex = 395;
            this.btnHide.Text = "Close";
            this.btnHide.UseVisualStyleBackColor = false;
            this.btnHide.Click += new System.EventHandler(this.BtnHide_Click);
            // 
            // label97
            // 
            this.label97.AutoSize = true;
            this.label97.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label97.ForeColor = System.Drawing.Color.Red;
            this.label97.Location = new System.Drawing.Point(211, 85);
            this.label97.Name = "label97";
            this.label97.Size = new System.Drawing.Size(13, 15);
            this.label97.TabIndex = 1008;
            this.label97.Text = "*";
            // 
            // label96
            // 
            this.label96.AutoSize = true;
            this.label96.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label96.ForeColor = System.Drawing.Color.Red;
            this.label96.Location = new System.Drawing.Point(704, 118);
            this.label96.Name = "label96";
            this.label96.Size = new System.Drawing.Size(13, 15);
            this.label96.TabIndex = 1007;
            this.label96.Text = "*";
            // 
            // label95
            // 
            this.label95.AutoSize = true;
            this.label95.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label95.ForeColor = System.Drawing.Color.Red;
            this.label95.Location = new System.Drawing.Point(705, 18);
            this.label95.Name = "label95";
            this.label95.Size = new System.Drawing.Size(13, 15);
            this.label95.TabIndex = 1006;
            this.label95.Text = "*";
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label61.Location = new System.Drawing.Point(762, 83);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(99, 15);
            this.label61.TabIndex = 26;
            this.label61.Text = "Style Description";
            // 
            // txtStyleDesc
            // 
            this.txtStyleDesc.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtStyleDesc.Location = new System.Drawing.Point(762, 109);
            this.txtStyleDesc.Name = "txtStyleDesc";
            this.txtStyleDesc.Size = new System.Drawing.Size(348, 23);
            this.txtStyleDesc.TabIndex = 25;
            // 
            // tabControlOrderEntry
            // 
            this.tabControlOrderEntry.ActiveTabForeColor = System.Drawing.Color.Empty;
            this.tabControlOrderEntry.BeforeTouchSize = new System.Drawing.Size(1122, 385);
            this.tabControlOrderEntry.CloseButtonForeColor = System.Drawing.Color.Empty;
            this.tabControlOrderEntry.CloseButtonHoverForeColor = System.Drawing.Color.Empty;
            this.tabControlOrderEntry.CloseButtonPressedForeColor = System.Drawing.Color.Empty;
            this.tabControlOrderEntry.Controls.Add(this.tabStyleNo);
            this.tabControlOrderEntry.Controls.Add(this.tabAssortment);
            this.tabControlOrderEntry.Controls.Add(this.tabPageSize);
            this.tabControlOrderEntry.Controls.Add(this.tabCombo);
            this.tabControlOrderEntry.Controls.Add(this.tabSizeMatrix);
            this.tabControlOrderEntry.Controls.Add(this.tabPlanning);
            this.tabControlOrderEntry.Controls.Add(this.tabPrice);
            this.tabControlOrderEntry.Controls.Add(this.tabLogistics);
            this.tabControlOrderEntry.Controls.Add(this.tabFabric);
            this.tabControlOrderEntry.InActiveTabForeColor = System.Drawing.Color.Empty;
            this.tabControlOrderEntry.Location = new System.Drawing.Point(6, 143);
            this.tabControlOrderEntry.Name = "tabControlOrderEntry";
            this.tabControlOrderEntry.SeparatorColor = System.Drawing.SystemColors.ControlDark;
            this.tabControlOrderEntry.ShowSeparator = false;
            this.tabControlOrderEntry.Size = new System.Drawing.Size(1122, 385);
            this.tabControlOrderEntry.TabIndex = 11;
            this.tabControlOrderEntry.SelectedIndexChanged += new System.EventHandler(this.TabControlOrderEntry_SelectedIndexChanged);
            // 
            // tabStyleNo
            // 
            this.tabStyleNo.Controls.Add(this.groupBox1);
            this.tabStyleNo.Image = null;
            this.tabStyleNo.ImageSize = new System.Drawing.Size(16, 16);
            this.tabStyleNo.Location = new System.Drawing.Point(1, 25);
            this.tabStyleNo.Name = "tabStyleNo";
            this.tabStyleNo.ShowCloseButton = true;
            this.tabStyleNo.Size = new System.Drawing.Size(1119, 358);
            this.tabStyleNo.TabIndex = 1;
            this.tabStyleNo.Text = "Style No";
            this.tabStyleNo.ThemesEnabled = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label66);
            this.groupBox1.Controls.Add(this.label65);
            this.groupBox1.Controls.Add(this.label64);
            this.groupBox1.Controls.Add(this.label63);
            this.groupBox1.Controls.Add(this.label62);
            this.groupBox1.Controls.Add(this.BtnReferesh);
            this.groupBox1.Controls.Add(this.label41);
            this.groupBox1.Controls.Add(this.label40);
            this.groupBox1.Controls.Add(this.label39);
            this.groupBox1.Controls.Add(this.label38);
            this.groupBox1.Controls.Add(this.label37);
            this.groupBox1.Controls.Add(this.btnStyleOk);
            this.groupBox1.Controls.Add(this.txtQuantity);
            this.groupBox1.Controls.Add(this.txtdescription);
            this.groupBox1.Controls.Add(this.txtStyleNo);
            this.groupBox1.Controls.Add(this.DataGridStyle);
            this.groupBox1.Controls.Add(this.CmbPlanUom);
            this.groupBox1.Controls.Add(this.cmbOrderUOm);
            this.groupBox1.Location = new System.Drawing.Point(3, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1113, 350);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label66.ForeColor = System.Drawing.Color.Red;
            this.label66.Location = new System.Drawing.Point(902, 12);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(13, 15);
            this.label66.TabIndex = 1005;
            this.label66.Text = "*";
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label65.ForeColor = System.Drawing.Color.Red;
            this.label65.Location = new System.Drawing.Point(827, 11);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(13, 15);
            this.label65.TabIndex = 1004;
            this.label65.Text = "*";
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label64.ForeColor = System.Drawing.Color.Red;
            this.label64.Location = new System.Drawing.Point(728, 11);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(13, 15);
            this.label64.TabIndex = 1003;
            this.label64.Text = "*";
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label63.ForeColor = System.Drawing.Color.Red;
            this.label63.Location = new System.Drawing.Point(375, 11);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(13, 15);
            this.label63.TabIndex = 1002;
            this.label63.Text = "*";
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label62.ForeColor = System.Drawing.Color.Red;
            this.label62.Location = new System.Drawing.Point(59, 11);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(13, 15);
            this.label62.TabIndex = 1001;
            this.label62.Text = "*";
            // 
            // BtnReferesh
            // 
            this.BtnReferesh.Location = new System.Drawing.Point(1028, 160);
            this.BtnReferesh.Name = "BtnReferesh";
            this.BtnReferesh.Size = new System.Drawing.Size(75, 23);
            this.BtnReferesh.TabIndex = 1000;
            this.BtnReferesh.Text = "Referesh";
            this.BtnReferesh.UseVisualStyleBackColor = true;
            this.BtnReferesh.Click += new System.EventHandler(this.BtnReferesh_Click);
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label41.Location = new System.Drawing.Point(877, 11);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(27, 15);
            this.label41.TabIndex = 33;
            this.label41.Text = "Qty";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.Location = new System.Drawing.Point(769, 11);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(59, 15);
            this.label40.TabIndex = 32;
            this.label40.Text = "Plan Uom";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.Location = new System.Drawing.Point(662, 11);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(69, 15);
            this.label39.TabIndex = 31;
            this.label39.Text = "Order Uom";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.Location = new System.Drawing.Point(280, 11);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(99, 15);
            this.label38.TabIndex = 30;
            this.label38.Text = "Style Description";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.Location = new System.Drawing.Point(9, 11);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(53, 15);
            this.label37.TabIndex = 25;
            this.label37.Text = "Style No";
            // 
            // btnStyleOk
            // 
            this.btnStyleOk.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStyleOk.Location = new System.Drawing.Point(983, 28);
            this.btnStyleOk.Name = "btnStyleOk";
            this.btnStyleOk.Size = new System.Drawing.Size(39, 24);
            this.btnStyleOk.TabIndex = 5;
            this.btnStyleOk.Text = "Ok";
            this.btnStyleOk.UseVisualStyleBackColor = true;
            this.btnStyleOk.Click += new System.EventHandler(this.BtnStyleOk_Click);
            // 
            // txtQuantity
            // 
            this.txtQuantity.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtQuantity.Location = new System.Drawing.Point(877, 29);
            this.txtQuantity.Name = "txtQuantity";
            this.txtQuantity.Size = new System.Drawing.Size(106, 23);
            this.txtQuantity.TabIndex = 4;
            this.txtQuantity.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Control_KeyUp);
            this.txtQuantity.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxtQuantity_KeyPress);
            // 
            // txtdescription
            // 
            this.txtdescription.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtdescription.Location = new System.Drawing.Point(280, 29);
            this.txtdescription.Name = "txtdescription";
            this.txtdescription.Size = new System.Drawing.Size(381, 23);
            this.txtdescription.TabIndex = 1;
            this.txtdescription.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Control_KeyUp);
            // 
            // txtStyleNo
            // 
            this.txtStyleNo.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtStyleNo.Location = new System.Drawing.Point(9, 29);
            this.txtStyleNo.Name = "txtStyleNo";
            this.txtStyleNo.Size = new System.Drawing.Size(270, 23);
            this.txtStyleNo.TabIndex = 0;
            this.txtStyleNo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Control_KeyUp);
            // 
            // DataGridStyle
            // 
            this.DataGridStyle.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle12.BackColor = System.Drawing.Color.DeepSkyBlue;
            dataGridViewCellStyle12.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle12.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DataGridStyle.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle12;
            this.DataGridStyle.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridStyle.EnableHeadersVisualStyles = false;
            this.DataGridStyle.Location = new System.Drawing.Point(9, 53);
            this.DataGridStyle.Name = "DataGridStyle";
            this.DataGridStyle.RowHeadersVisible = false;
            this.DataGridStyle.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridStyle.Size = new System.Drawing.Size(1013, 290);
            this.DataGridStyle.TabIndex = 999;
            this.DataGridStyle.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.DataGridStyle_CellMouseDoubleClick);
            this.DataGridStyle.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DataGridStyle_KeyDown);
            // 
            // CmbPlanUom
            // 
            this.CmbPlanUom.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CmbPlanUom.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CmbPlanUom.FormattingEnabled = true;
            this.CmbPlanUom.Location = new System.Drawing.Point(770, 29);
            this.CmbPlanUom.Name = "CmbPlanUom";
            this.CmbPlanUom.Size = new System.Drawing.Size(106, 23);
            this.CmbPlanUom.TabIndex = 3;
            this.CmbPlanUom.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Control_KeyUp);
            // 
            // cmbOrderUOm
            // 
            this.cmbOrderUOm.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbOrderUOm.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbOrderUOm.FormattingEnabled = true;
            this.cmbOrderUOm.Location = new System.Drawing.Point(662, 29);
            this.cmbOrderUOm.Name = "cmbOrderUOm";
            this.cmbOrderUOm.Size = new System.Drawing.Size(106, 23);
            this.cmbOrderUOm.TabIndex = 2;
            this.cmbOrderUOm.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Control_KeyUp);
            // 
            // tabAssortment
            // 
            this.tabAssortment.Controls.Add(this.groupBox5);
            this.tabAssortment.Image = null;
            this.tabAssortment.ImageSize = new System.Drawing.Size(16, 16);
            this.tabAssortment.Location = new System.Drawing.Point(1, 25);
            this.tabAssortment.Name = "tabAssortment";
            this.tabAssortment.ShowCloseButton = true;
            this.tabAssortment.Size = new System.Drawing.Size(1119, 358);
            this.tabAssortment.TabIndex = 6;
            this.tabAssortment.Text = "Assortment";
            this.tabAssortment.ThemesEnabled = false;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.label72);
            this.groupBox5.Controls.Add(this.label71);
            this.groupBox5.Controls.Add(this.label70);
            this.groupBox5.Controls.Add(this.label69);
            this.groupBox5.Controls.Add(this.label68);
            this.groupBox5.Controls.Add(this.label67);
            this.groupBox5.Controls.Add(this.grImport);
            this.groupBox5.Controls.Add(this.label54);
            this.groupBox5.Controls.Add(this.label53);
            this.groupBox5.Controls.Add(this.label52);
            this.groupBox5.Controls.Add(this.label51);
            this.groupBox5.Controls.Add(this.label50);
            this.groupBox5.Controls.Add(this.label49);
            this.groupBox5.Controls.Add(this.label48);
            this.groupBox5.Controls.Add(this.label47);
            this.groupBox5.Controls.Add(this.label46);
            this.groupBox5.Controls.Add(this.txtMCBox);
            this.groupBox5.Controls.Add(this.cmbCountry);
            this.groupBox5.Controls.Add(this.btnAssortOk);
            this.groupBox5.Controls.Add(this.txtFinalDistination);
            this.groupBox5.Controls.Add(this.txtPortofDelivery);
            this.groupBox5.Controls.Add(this.dtpAssortShipDt);
            this.groupBox5.Controls.Add(this.label34);
            this.groupBox5.Controls.Add(this.cmbStyleAssort);
            this.groupBox5.Controls.Add(this.dtpAssortDelDt);
            this.groupBox5.Controls.Add(this.txtAssortQuantity);
            this.groupBox5.Controls.Add(this.txtAssortmentType);
            this.groupBox5.Controls.Add(this.txtRefNo);
            this.groupBox5.Controls.Add(this.DataGridAssort);
            this.groupBox5.Location = new System.Drawing.Point(5, 3);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(1111, 350);
            this.groupBox5.TabIndex = 0;
            this.groupBox5.TabStop = false;
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label72.ForeColor = System.Drawing.Color.Red;
            this.label72.Location = new System.Drawing.Point(723, 37);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(13, 15);
            this.label72.TabIndex = 1007;
            this.label72.Text = "*";
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label71.ForeColor = System.Drawing.Color.Red;
            this.label71.Location = new System.Drawing.Point(619, 37);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(13, 15);
            this.label71.TabIndex = 1006;
            this.label71.Text = "*";
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label70.ForeColor = System.Drawing.Color.Red;
            this.label70.Location = new System.Drawing.Point(498, 38);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(13, 15);
            this.label70.TabIndex = 1005;
            this.label70.Text = "*";
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label69.ForeColor = System.Drawing.Color.Red;
            this.label69.Location = new System.Drawing.Point(304, 39);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(13, 15);
            this.label69.TabIndex = 1004;
            this.label69.Text = "*";
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label68.ForeColor = System.Drawing.Color.Red;
            this.label68.Location = new System.Drawing.Point(170, 37);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(13, 15);
            this.label68.TabIndex = 1003;
            this.label68.Text = "*";
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label67.ForeColor = System.Drawing.Color.Red;
            this.label67.Location = new System.Drawing.Point(53, 39);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(13, 15);
            this.label67.TabIndex = 1002;
            this.label67.Text = "*";
            // 
            // grImport
            // 
            this.grImport.Controls.Add(this.BtnImportHide);
            this.grImport.Controls.Add(this.CmbAssortImport);
            this.grImport.Controls.Add(this.CmbStyleImport);
            this.grImport.Controls.Add(this.lblPath);
            this.grImport.Controls.Add(this.label60);
            this.grImport.Controls.Add(this.label59);
            this.grImport.Controls.Add(this.button1);
            this.grImport.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grImport.Location = new System.Drawing.Point(185, 99);
            this.grImport.Name = "grImport";
            this.grImport.Size = new System.Drawing.Size(539, 219);
            this.grImport.TabIndex = 64;
            this.grImport.TabStop = false;
            this.grImport.Visible = false;
            // 
            // BtnImportHide
            // 
            this.BtnImportHide.Location = new System.Drawing.Point(453, 190);
            this.BtnImportHide.Name = "BtnImportHide";
            this.BtnImportHide.Size = new System.Drawing.Size(75, 23);
            this.BtnImportHide.TabIndex = 6;
            this.BtnImportHide.Text = "Close";
            this.BtnImportHide.UseVisualStyleBackColor = true;
            this.BtnImportHide.Click += new System.EventHandler(this.BtnImportHide_Click);
            // 
            // CmbAssortImport
            // 
            this.CmbAssortImport.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CmbAssortImport.FormattingEnabled = true;
            this.CmbAssortImport.Location = new System.Drawing.Point(95, 47);
            this.CmbAssortImport.Name = "CmbAssortImport";
            this.CmbAssortImport.Size = new System.Drawing.Size(182, 23);
            this.CmbAssortImport.TabIndex = 5;
            // 
            // CmbStyleImport
            // 
            this.CmbStyleImport.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CmbStyleImport.FormattingEnabled = true;
            this.CmbStyleImport.Location = new System.Drawing.Point(95, 16);
            this.CmbStyleImport.Name = "CmbStyleImport";
            this.CmbStyleImport.Size = new System.Drawing.Size(182, 23);
            this.CmbStyleImport.TabIndex = 4;
            // 
            // lblPath
            // 
            this.lblPath.AutoSize = true;
            this.lblPath.Location = new System.Drawing.Point(92, 131);
            this.lblPath.Name = "lblPath";
            this.lblPath.Size = new System.Drawing.Size(11, 15);
            this.lblPath.TabIndex = 3;
            this.lblPath.Text = "-";
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Location = new System.Drawing.Point(22, 47);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(69, 15);
            this.label60.TabIndex = 2;
            this.label60.Text = "Assort Type";
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Location = new System.Drawing.Point(38, 16);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(51, 15);
            this.label59.TabIndex = 1;
            this.label59.Text = "Style No";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(95, 95);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "Import";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.Button1_Click);
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label54.Location = new System.Drawing.Point(1012, 38);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(34, 15);
            this.label54.TabIndex = 63;
            this.label54.Text = "CBox";
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label53.Location = new System.Drawing.Point(854, 38);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(98, 15);
            this.label53.TabIndex = 62;
            this.label53.Text = "Final Destination";
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label52.Location = new System.Drawing.Point(738, 39);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(93, 15);
            this.label52.TabIndex = 61;
            this.label52.Text = "Port of Delivery";
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label51.Location = new System.Drawing.Point(635, 38);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(89, 15);
            this.label51.TabIndex = 60;
            this.label51.Text = "Shipment Date";
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label50.Location = new System.Drawing.Point(542, 39);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(81, 15);
            this.label50.TabIndex = 59;
            this.label50.Text = "Delivery Date";
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label49.Location = new System.Drawing.Point(447, 38);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(55, 15);
            this.label49.TabIndex = 58;
            this.label49.Text = "Quantity";
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label48.Location = new System.Drawing.Point(265, 38);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(42, 15);
            this.label48.TabIndex = 57;
            this.label48.Text = "Assort";
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label47.Location = new System.Drawing.Point(132, 38);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(41, 15);
            this.label47.TabIndex = 56;
            this.label47.Text = "RefNo";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label46.Location = new System.Drawing.Point(3, 38);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(51, 15);
            this.label46.TabIndex = 55;
            this.label46.Text = "Country";
            // 
            // txtMCBox
            // 
            this.txtMCBox.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMCBox.Location = new System.Drawing.Point(1012, 57);
            this.txtMCBox.Name = "txtMCBox";
            this.txtMCBox.Size = new System.Drawing.Size(63, 23);
            this.txtMCBox.TabIndex = 9;
            this.txtMCBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Control_KeyUp);
            this.txtMCBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxtQuantity_KeyPress);
            // 
            // cmbCountry
            // 
            this.cmbCountry.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbCountry.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbCountry.FormattingEnabled = true;
            this.cmbCountry.Location = new System.Drawing.Point(2, 57);
            this.cmbCountry.Name = "cmbCountry";
            this.cmbCountry.Size = new System.Drawing.Size(128, 23);
            this.cmbCountry.TabIndex = 1;
            this.cmbCountry.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Control_KeyUp);
            // 
            // btnAssortOk
            // 
            this.btnAssortOk.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAssortOk.Location = new System.Drawing.Point(1075, 56);
            this.btnAssortOk.Name = "btnAssortOk";
            this.btnAssortOk.Size = new System.Drawing.Size(30, 25);
            this.btnAssortOk.TabIndex = 10;
            this.btnAssortOk.Text = "Ok";
            this.btnAssortOk.UseVisualStyleBackColor = true;
            this.btnAssortOk.Click += new System.EventHandler(this.BtnAssortOk_Click);
            // 
            // txtFinalDistination
            // 
            this.txtFinalDistination.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFinalDistination.Location = new System.Drawing.Point(854, 57);
            this.txtFinalDistination.Name = "txtFinalDistination";
            this.txtFinalDistination.Size = new System.Drawing.Size(157, 23);
            this.txtFinalDistination.TabIndex = 8;
            this.txtFinalDistination.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Control_KeyUp);
            // 
            // txtPortofDelivery
            // 
            this.txtPortofDelivery.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPortofDelivery.Location = new System.Drawing.Point(738, 57);
            this.txtPortofDelivery.Name = "txtPortofDelivery";
            this.txtPortofDelivery.Size = new System.Drawing.Size(114, 23);
            this.txtPortofDelivery.TabIndex = 7;
            this.txtPortofDelivery.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Control_KeyUp);
            // 
            // dtpAssortShipDt
            // 
            this.dtpAssortShipDt.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpAssortShipDt.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpAssortShipDt.Location = new System.Drawing.Point(638, 57);
            this.dtpAssortShipDt.Name = "dtpAssortShipDt";
            this.dtpAssortShipDt.Size = new System.Drawing.Size(97, 23);
            this.dtpAssortShipDt.TabIndex = 6;
            this.dtpAssortShipDt.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Control_KeyUp);
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.Location = new System.Drawing.Point(3, 14);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(34, 15);
            this.label34.TabIndex = 54;
            this.label34.Text = "Style";
            // 
            // cmbStyleAssort
            // 
            this.cmbStyleAssort.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbStyleAssort.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbStyleAssort.FormattingEnabled = true;
            this.cmbStyleAssort.Location = new System.Drawing.Point(41, 10);
            this.cmbStyleAssort.Name = "cmbStyleAssort";
            this.cmbStyleAssort.Size = new System.Drawing.Size(260, 23);
            this.cmbStyleAssort.TabIndex = 0;
            this.cmbStyleAssort.SelectedIndexChanged += new System.EventHandler(this.CmbStyleAssort_SelectedIndexChanged);
            this.cmbStyleAssort.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Control_KeyUp);
            // 
            // dtpAssortDelDt
            // 
            this.dtpAssortDelDt.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpAssortDelDt.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpAssortDelDt.Location = new System.Drawing.Point(542, 57);
            this.dtpAssortDelDt.Name = "dtpAssortDelDt";
            this.dtpAssortDelDt.Size = new System.Drawing.Size(95, 23);
            this.dtpAssortDelDt.TabIndex = 5;
            this.dtpAssortDelDt.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Control_KeyUp);
            // 
            // txtAssortQuantity
            // 
            this.txtAssortQuantity.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAssortQuantity.Location = new System.Drawing.Point(447, 57);
            this.txtAssortQuantity.Name = "txtAssortQuantity";
            this.txtAssortQuantity.Size = new System.Drawing.Size(94, 23);
            this.txtAssortQuantity.TabIndex = 4;
            this.txtAssortQuantity.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Control_KeyUp);
            this.txtAssortQuantity.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxtQuantity_KeyPress);
            // 
            // txtAssortmentType
            // 
            this.txtAssortmentType.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAssortmentType.Location = new System.Drawing.Point(265, 57);
            this.txtAssortmentType.Name = "txtAssortmentType";
            this.txtAssortmentType.Size = new System.Drawing.Size(181, 23);
            this.txtAssortmentType.TabIndex = 3;
            this.txtAssortmentType.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Control_KeyUp);
            // 
            // txtRefNo
            // 
            this.txtRefNo.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRefNo.Location = new System.Drawing.Point(132, 57);
            this.txtRefNo.Name = "txtRefNo";
            this.txtRefNo.Size = new System.Drawing.Size(131, 23);
            this.txtRefNo.TabIndex = 2;
            this.txtRefNo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Control_KeyUp);
            // 
            // DataGridAssort
            // 
            this.DataGridAssort.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle13.BackColor = System.Drawing.Color.DeepSkyBlue;
            dataGridViewCellStyle13.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle13.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle13.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle13.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle13.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DataGridAssort.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle13;
            this.DataGridAssort.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridAssort.EnableHeadersVisualStyles = false;
            this.DataGridAssort.Location = new System.Drawing.Point(2, 82);
            this.DataGridAssort.Name = "DataGridAssort";
            this.DataGridAssort.ReadOnly = true;
            this.DataGridAssort.RowHeadersVisible = false;
            this.DataGridAssort.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridAssort.Size = new System.Drawing.Size(1104, 262);
            this.DataGridAssort.TabIndex = 50;
            this.DataGridAssort.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.DataGridAssort_CellMouseDoubleClick);
            this.DataGridAssort.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DataGridAssort_KeyDown);
            this.DataGridAssort.MouseClick += new System.Windows.Forms.MouseEventHandler(this.DataGridAssort_MouseClick);
            // 
            // tabPageSize
            // 
            this.tabPageSize.Controls.Add(this.groupBox4);
            this.tabPageSize.Image = null;
            this.tabPageSize.ImageSize = new System.Drawing.Size(16, 16);
            this.tabPageSize.Location = new System.Drawing.Point(1, 25);
            this.tabPageSize.Name = "tabPageSize";
            this.tabPageSize.ShowCloseButton = true;
            this.tabPageSize.Size = new System.Drawing.Size(1119, 358);
            this.tabPageSize.TabIndex = 5;
            this.tabPageSize.Text = "Size";
            this.tabPageSize.ThemesEnabled = false;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.label75);
            this.groupBox4.Controls.Add(this.label74);
            this.groupBox4.Controls.Add(this.label73);
            this.groupBox4.Controls.Add(this.txtCombo);
            this.groupBox4.Controls.Add(this.label45);
            this.groupBox4.Controls.Add(this.txtComboQty);
            this.groupBox4.Controls.Add(this.label33);
            this.groupBox4.Controls.Add(this.btnComboOk);
            this.groupBox4.Controls.Add(this.DataGridStyleSizeCombo);
            this.groupBox4.Controls.Add(this.label32);
            this.groupBox4.Controls.Add(this.label31);
            this.groupBox4.Controls.Add(this.CmbStyle);
            this.groupBox4.Controls.Add(this.btnStyleSizeOk);
            this.groupBox4.Controls.Add(this.txtStyleSize);
            this.groupBox4.Controls.Add(this.DataGridStyleSize);
            this.groupBox4.Location = new System.Drawing.Point(5, 3);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(1111, 350);
            this.groupBox4.TabIndex = 0;
            this.groupBox4.TabStop = false;
            // 
            // label75
            // 
            this.label75.AutoSize = true;
            this.label75.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label75.ForeColor = System.Drawing.Color.Red;
            this.label75.Location = new System.Drawing.Point(754, 78);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(13, 15);
            this.label75.TabIndex = 1005;
            this.label75.Text = "*";
            // 
            // label74
            // 
            this.label74.AutoSize = true;
            this.label74.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label74.ForeColor = System.Drawing.Color.Red;
            this.label74.Location = new System.Drawing.Point(473, 12);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(13, 15);
            this.label74.TabIndex = 1004;
            this.label74.Text = "*";
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.label73.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label73.ForeColor = System.Drawing.Color.Red;
            this.label73.Location = new System.Drawing.Point(183, 12);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(13, 15);
            this.label73.TabIndex = 1003;
            this.label73.Text = "*";
            // 
            // txtCombo
            // 
            this.txtCombo.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCombo.Location = new System.Drawing.Point(436, 73);
            this.txtCombo.Name = "txtCombo";
            this.txtCombo.Size = new System.Drawing.Size(285, 23);
            this.txtCombo.TabIndex = 3;
            this.txtCombo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Control_KeyUp);
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label45.Location = new System.Drawing.Point(626, 54);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(27, 15);
            this.label45.TabIndex = 44;
            this.label45.Text = "Qty";
            this.label45.Visible = false;
            // 
            // txtComboQty
            // 
            this.txtComboQty.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtComboQty.Location = new System.Drawing.Point(626, 73);
            this.txtComboQty.Name = "txtComboQty";
            this.txtComboQty.Size = new System.Drawing.Size(95, 23);
            this.txtComboQty.TabIndex = 4;
            this.txtComboQty.Visible = false;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(436, 56);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(46, 15);
            this.label33.TabIndex = 43;
            this.label33.Text = "Combo";
            // 
            // btnComboOk
            // 
            this.btnComboOk.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnComboOk.Location = new System.Drawing.Point(721, 72);
            this.btnComboOk.Name = "btnComboOk";
            this.btnComboOk.Size = new System.Drawing.Size(30, 25);
            this.btnComboOk.TabIndex = 5;
            this.btnComboOk.Text = "Ok";
            this.btnComboOk.UseVisualStyleBackColor = true;
            this.btnComboOk.Click += new System.EventHandler(this.BtnComboOk_Click);
            // 
            // DataGridStyleSizeCombo
            // 
            this.DataGridStyleSizeCombo.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle14.BackColor = System.Drawing.Color.DeepSkyBlue;
            dataGridViewCellStyle14.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle14.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle14.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle14.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DataGridStyleSizeCombo.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle14;
            this.DataGridStyleSizeCombo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridStyleSizeCombo.EnableHeadersVisualStyles = false;
            this.DataGridStyleSizeCombo.Location = new System.Drawing.Point(436, 97);
            this.DataGridStyleSizeCombo.Name = "DataGridStyleSizeCombo";
            this.DataGridStyleSizeCombo.ReadOnly = true;
            this.DataGridStyleSizeCombo.RowHeadersVisible = false;
            this.DataGridStyleSizeCombo.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridStyleSizeCombo.Size = new System.Drawing.Size(311, 218);
            this.DataGridStyleSizeCombo.TabIndex = 40;
            this.DataGridStyleSizeCombo.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.DataGridStyleSizeCombo_CellMouseDoubleClick);
            this.DataGridStyleSizeCombo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DataGridStyleSizeCombo_KeyDown);
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.Location = new System.Drawing.Point(155, 10);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(28, 15);
            this.label32.TabIndex = 38;
            this.label32.Text = "Size";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(433, 12);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(34, 15);
            this.label31.TabIndex = 24;
            this.label31.Text = "Style";
            // 
            // CmbStyle
            // 
            this.CmbStyle.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CmbStyle.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CmbStyle.FormattingEnabled = true;
            this.CmbStyle.Location = new System.Drawing.Point(436, 30);
            this.CmbStyle.Name = "CmbStyle";
            this.CmbStyle.Size = new System.Drawing.Size(264, 23);
            this.CmbStyle.TabIndex = 2;
            this.CmbStyle.SelectedIndexChanged += new System.EventHandler(this.CmbStyle_SelectedIndexChanged);
            this.CmbStyle.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Control_KeyUp);
            // 
            // btnStyleSizeOk
            // 
            this.btnStyleSizeOk.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStyleSizeOk.Location = new System.Drawing.Point(367, 28);
            this.btnStyleSizeOk.Name = "btnStyleSizeOk";
            this.btnStyleSizeOk.Size = new System.Drawing.Size(30, 24);
            this.btnStyleSizeOk.TabIndex = 1;
            this.btnStyleSizeOk.Text = "Ok";
            this.btnStyleSizeOk.UseVisualStyleBackColor = true;
            this.btnStyleSizeOk.Click += new System.EventHandler(this.BtnStyleSizeOk_Click);
            // 
            // txtStyleSize
            // 
            this.txtStyleSize.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtStyleSize.Location = new System.Drawing.Point(155, 29);
            this.txtStyleSize.Name = "txtStyleSize";
            this.txtStyleSize.Size = new System.Drawing.Size(212, 23);
            this.txtStyleSize.TabIndex = 0;
            this.txtStyleSize.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Control_KeyUp);
            // 
            // DataGridStyleSize
            // 
            this.DataGridStyleSize.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle15.BackColor = System.Drawing.Color.DeepSkyBlue;
            dataGridViewCellStyle15.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle15.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle15.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle15.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle15.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DataGridStyleSize.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle15;
            this.DataGridStyleSize.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridStyleSize.EnableHeadersVisualStyles = false;
            this.DataGridStyleSize.Location = new System.Drawing.Point(155, 54);
            this.DataGridStyleSize.Name = "DataGridStyleSize";
            this.DataGridStyleSize.ReadOnly = true;
            this.DataGridStyleSize.RowHeadersVisible = false;
            this.DataGridStyleSize.Size = new System.Drawing.Size(242, 268);
            this.DataGridStyleSize.TabIndex = 35;
            this.DataGridStyleSize.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.DataGridStyleSize_CellMouseDoubleClick);
            this.DataGridStyleSize.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DataGridStyleSize_KeyDown);
            // 
            // tabCombo
            // 
            this.tabCombo.Controls.Add(this.groupBox2);
            this.tabCombo.Image = null;
            this.tabCombo.ImageSize = new System.Drawing.Size(16, 16);
            this.tabCombo.Location = new System.Drawing.Point(1, 25);
            this.tabCombo.Name = "tabCombo";
            this.tabCombo.ShowCloseButton = true;
            this.tabCombo.Size = new System.Drawing.Size(1119, 358);
            this.tabCombo.TabIndex = 2;
            this.tabCombo.Text = "Quantities";
            this.tabCombo.ThemesEnabled = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label76);
            this.groupBox2.Controls.Add(this.label35);
            this.groupBox2.Controls.Add(this.cmbQtyStyle);
            this.groupBox2.Controls.Add(this.cmbAssort);
            this.groupBox2.Controls.Add(this.DataGridCombo);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Location = new System.Drawing.Point(3, 3);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(1113, 350);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            // 
            // label76
            // 
            this.label76.AutoSize = true;
            this.label76.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label76.ForeColor = System.Drawing.Color.Red;
            this.label76.Location = new System.Drawing.Point(635, 17);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(13, 15);
            this.label76.TabIndex = 1003;
            this.label76.Text = "*";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.Location = new System.Drawing.Point(340, 17);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(34, 15);
            this.label35.TabIndex = 56;
            this.label35.Text = "Style";
            // 
            // cmbQtyStyle
            // 
            this.cmbQtyStyle.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbQtyStyle.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbQtyStyle.FormattingEnabled = true;
            this.cmbQtyStyle.Location = new System.Drawing.Point(378, 13);
            this.cmbQtyStyle.Name = "cmbQtyStyle";
            this.cmbQtyStyle.Size = new System.Drawing.Size(251, 23);
            this.cmbQtyStyle.TabIndex = 1;
            this.cmbQtyStyle.SelectedIndexChanged += new System.EventHandler(this.CmbQtyStyle_SelectedIndexChanged);
            this.cmbQtyStyle.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Control_KeyUp);
            // 
            // cmbAssort
            // 
            this.cmbAssort.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbAssort.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbAssort.FormattingEnabled = true;
            this.cmbAssort.Location = new System.Drawing.Point(105, 13);
            this.cmbAssort.Name = "cmbAssort";
            this.cmbAssort.Size = new System.Drawing.Size(231, 23);
            this.cmbAssort.TabIndex = 0;
            this.cmbAssort.SelectedIndexChanged += new System.EventHandler(this.CmbStyleCombo_SelectedIndexChanged);
            this.cmbAssort.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Control_KeyUp);
            // 
            // DataGridCombo
            // 
            this.DataGridCombo.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle16.BackColor = System.Drawing.Color.DeepSkyBlue;
            dataGridViewCellStyle16.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle16.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle16.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle16.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle16.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DataGridCombo.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle16;
            this.DataGridCombo.ColumnHeadersHeight = 30;
            this.DataGridCombo.EnableHeadersVisualStyles = false;
            this.DataGridCombo.Location = new System.Drawing.Point(9, 45);
            this.DataGridCombo.Name = "DataGridCombo";
            this.DataGridCombo.RowHeadersVisible = false;
            this.DataGridCombo.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridCombo.Size = new System.Drawing.Size(1098, 299);
            this.DataGridCombo.TabIndex = 24;
            this.DataGridCombo.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridCombo_CellEndEdit);
            this.DataGridCombo.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.DataGridCombo_CellFormatting);
            this.DataGridCombo.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridCombo_CellValueChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(6, 16);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(98, 15);
            this.label12.TabIndex = 23;
            this.label12.Text = "AssortmentType";
            // 
            // tabSizeMatrix
            // 
            this.tabSizeMatrix.Controls.Add(this.groupBox6);
            this.tabSizeMatrix.Image = null;
            this.tabSizeMatrix.ImageSize = new System.Drawing.Size(16, 16);
            this.tabSizeMatrix.Location = new System.Drawing.Point(1, 25);
            this.tabSizeMatrix.Name = "tabSizeMatrix";
            this.tabSizeMatrix.ShowCloseButton = true;
            this.tabSizeMatrix.Size = new System.Drawing.Size(1119, 358);
            this.tabSizeMatrix.TabIndex = 7;
            this.tabSizeMatrix.Text = "Size Matrix";
            this.tabSizeMatrix.ThemesEnabled = false;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.label77);
            this.groupBox6.Controls.Add(this.label36);
            this.groupBox6.Controls.Add(this.cmbMatrixStyle);
            this.groupBox6.Controls.Add(this.DataGridSizeMatrix);
            this.groupBox6.Location = new System.Drawing.Point(4, 3);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(1112, 350);
            this.groupBox6.TabIndex = 0;
            this.groupBox6.TabStop = false;
            // 
            // label77
            // 
            this.label77.AutoSize = true;
            this.label77.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label77.ForeColor = System.Drawing.Color.Red;
            this.label77.Location = new System.Drawing.Point(300, 24);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(13, 15);
            this.label77.TabIndex = 1003;
            this.label77.Text = "*";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.Location = new System.Drawing.Point(5, 24);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(34, 15);
            this.label36.TabIndex = 59;
            this.label36.Text = "Style";
            // 
            // cmbMatrixStyle
            // 
            this.cmbMatrixStyle.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbMatrixStyle.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbMatrixStyle.FormattingEnabled = true;
            this.cmbMatrixStyle.Location = new System.Drawing.Point(43, 20);
            this.cmbMatrixStyle.Name = "cmbMatrixStyle";
            this.cmbMatrixStyle.Size = new System.Drawing.Size(251, 23);
            this.cmbMatrixStyle.TabIndex = 0;
            this.cmbMatrixStyle.SelectedIndexChanged += new System.EventHandler(this.CmbMatrixStyle_SelectedIndexChanged);
            // 
            // DataGridSizeMatrix
            // 
            this.DataGridSizeMatrix.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle17.BackColor = System.Drawing.Color.DeepSkyBlue;
            dataGridViewCellStyle17.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle17.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle17.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle17.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle17.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DataGridSizeMatrix.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle17;
            this.DataGridSizeMatrix.ColumnHeadersHeight = 30;
            this.DataGridSizeMatrix.EnableHeadersVisualStyles = false;
            this.DataGridSizeMatrix.Location = new System.Drawing.Point(5, 48);
            this.DataGridSizeMatrix.Name = "DataGridSizeMatrix";
            this.DataGridSizeMatrix.RowHeadersVisible = false;
            this.DataGridSizeMatrix.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridSizeMatrix.Size = new System.Drawing.Size(1101, 296);
            this.DataGridSizeMatrix.TabIndex = 58;
            this.DataGridSizeMatrix.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.DataGridSizeMatrix_CellFormatting);
            this.DataGridSizeMatrix.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridSizeMatrix_CellValueChanged);
            this.DataGridSizeMatrix.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DataGridSizeMatrix_KeyDown);
            // 
            // tabPlanning
            // 
            this.tabPlanning.Controls.Add(this.label79);
            this.tabPlanning.Controls.Add(this.label78);
            this.tabPlanning.Controls.Add(this.ChckRefresh);
            this.tabPlanning.Controls.Add(this.ChckAll);
            this.tabPlanning.Controls.Add(this.btnCalculate);
            this.tabPlanning.Controls.Add(this.txtCuttingTolerance);
            this.tabPlanning.Controls.Add(this.label58);
            this.tabPlanning.Controls.Add(this.txtBuyerTolerance);
            this.tabPlanning.Controls.Add(this.label57);
            this.tabPlanning.Controls.Add(this.label56);
            this.tabPlanning.Controls.Add(this.CmbPlanningStyleCombo);
            this.tabPlanning.Controls.Add(this.label55);
            this.tabPlanning.Controls.Add(this.CmbPlanningStyle);
            this.tabPlanning.Controls.Add(this.DataGridPlanning);
            this.tabPlanning.Image = null;
            this.tabPlanning.ImageSize = new System.Drawing.Size(16, 16);
            this.tabPlanning.Location = new System.Drawing.Point(1, 25);
            this.tabPlanning.Name = "tabPlanning";
            this.tabPlanning.ShowCloseButton = true;
            this.tabPlanning.Size = new System.Drawing.Size(1119, 358);
            this.tabPlanning.TabIndex = 9;
            this.tabPlanning.Text = "Planning";
            this.tabPlanning.ThemesEnabled = false;
            // 
            // label79
            // 
            this.label79.AutoSize = true;
            this.label79.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label79.ForeColor = System.Drawing.Color.Red;
            this.label79.Location = new System.Drawing.Point(949, 24);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(13, 15);
            this.label79.TabIndex = 1004;
            this.label79.Text = "*";
            // 
            // label78
            // 
            this.label78.AutoSize = true;
            this.label78.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label78.ForeColor = System.Drawing.Color.Red;
            this.label78.Location = new System.Drawing.Point(826, 5);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(13, 15);
            this.label78.TabIndex = 1003;
            this.label78.Text = "*";
            // 
            // ChckRefresh
            // 
            this.ChckRefresh.AutoSize = true;
            this.ChckRefresh.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChckRefresh.Location = new System.Drawing.Point(1041, 20);
            this.ChckRefresh.Name = "ChckRefresh";
            this.ChckRefresh.Size = new System.Drawing.Size(67, 19);
            this.ChckRefresh.TabIndex = 68;
            this.ChckRefresh.Text = "Refresh";
            this.ChckRefresh.UseVisualStyleBackColor = true;
            this.ChckRefresh.CheckedChanged += new System.EventHandler(this.ChckRefresh_CheckedChanged);
            // 
            // ChckAll
            // 
            this.ChckAll.AutoSize = true;
            this.ChckAll.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChckAll.Location = new System.Drawing.Point(807, 22);
            this.ChckAll.Name = "ChckAll";
            this.ChckAll.Size = new System.Drawing.Size(89, 19);
            this.ChckAll.TabIndex = 67;
            this.ChckAll.Text = "Apply to All";
            this.ChckAll.UseVisualStyleBackColor = true;
            // 
            // btnCalculate
            // 
            this.btnCalculate.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCalculate.Location = new System.Drawing.Point(902, 20);
            this.btnCalculate.Name = "btnCalculate";
            this.btnCalculate.Size = new System.Drawing.Size(41, 23);
            this.btnCalculate.TabIndex = 66;
            this.btnCalculate.Text = "Ok";
            this.btnCalculate.UseVisualStyleBackColor = true;
            this.btnCalculate.Click += new System.EventHandler(this.BtnCalculate_Click);
            // 
            // txtCuttingTolerance
            // 
            this.txtCuttingTolerance.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCuttingTolerance.Location = new System.Drawing.Point(729, 20);
            this.txtCuttingTolerance.Name = "txtCuttingTolerance";
            this.txtCuttingTolerance.Size = new System.Drawing.Size(74, 23);
            this.txtCuttingTolerance.TabIndex = 65;
            this.txtCuttingTolerance.TextChanged += new System.EventHandler(this.TxtCuttingTolerance_TextChanged);
            this.txtCuttingTolerance.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxtQuantity_KeyPress);
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label58.Location = new System.Drawing.Point(726, 3);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(101, 15);
            this.label58.TabIndex = 64;
            this.label58.Text = "Cutting Tolerance";
            // 
            // txtBuyerTolerance
            // 
            this.txtBuyerTolerance.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBuyerTolerance.Location = new System.Drawing.Point(627, 20);
            this.txtBuyerTolerance.Name = "txtBuyerTolerance";
            this.txtBuyerTolerance.Size = new System.Drawing.Size(99, 23);
            this.txtBuyerTolerance.TabIndex = 63;
            this.txtBuyerTolerance.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxtQuantity_KeyPress);
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label57.Location = new System.Drawing.Point(624, 3);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(94, 15);
            this.label57.TabIndex = 62;
            this.label57.Text = "Buyer Tolerance";
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label56.Location = new System.Drawing.Point(315, 24);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(46, 15);
            this.label56.TabIndex = 61;
            this.label56.Text = "Combo";
            // 
            // CmbPlanningStyleCombo
            // 
            this.CmbPlanningStyleCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CmbPlanningStyleCombo.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CmbPlanningStyleCombo.FormattingEnabled = true;
            this.CmbPlanningStyleCombo.Location = new System.Drawing.Point(367, 20);
            this.CmbPlanningStyleCombo.Name = "CmbPlanningStyleCombo";
            this.CmbPlanningStyleCombo.Size = new System.Drawing.Size(251, 23);
            this.CmbPlanningStyleCombo.TabIndex = 60;
            this.CmbPlanningStyleCombo.SelectedIndexChanged += new System.EventHandler(this.CmbPlanningStyleCombo_SelectedIndexChanged);
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label55.Location = new System.Drawing.Point(11, 24);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(34, 15);
            this.label55.TabIndex = 59;
            this.label55.Text = "Style";
            // 
            // CmbPlanningStyle
            // 
            this.CmbPlanningStyle.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CmbPlanningStyle.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CmbPlanningStyle.FormattingEnabled = true;
            this.CmbPlanningStyle.Location = new System.Drawing.Point(49, 20);
            this.CmbPlanningStyle.Name = "CmbPlanningStyle";
            this.CmbPlanningStyle.Size = new System.Drawing.Size(251, 23);
            this.CmbPlanningStyle.TabIndex = 57;
            this.CmbPlanningStyle.SelectedIndexChanged += new System.EventHandler(this.CmbPlanningStyle_SelectedIndexChanged);
            // 
            // DataGridPlanning
            // 
            this.DataGridPlanning.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle18.BackColor = System.Drawing.Color.DeepSkyBlue;
            dataGridViewCellStyle18.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle18.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle18.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle18.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle18.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DataGridPlanning.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle18;
            this.DataGridPlanning.ColumnHeadersHeight = 30;
            this.DataGridPlanning.EnableHeadersVisualStyles = false;
            this.DataGridPlanning.Location = new System.Drawing.Point(10, 45);
            this.DataGridPlanning.Name = "DataGridPlanning";
            this.DataGridPlanning.RowHeadersVisible = false;
            this.DataGridPlanning.Size = new System.Drawing.Size(1098, 299);
            this.DataGridPlanning.TabIndex = 58;
            this.DataGridPlanning.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridPlanning_CellValueChanged);
            // 
            // tabPrice
            // 
            this.tabPrice.Controls.Add(this.groupBox3);
            this.tabPrice.Image = null;
            this.tabPrice.ImageSize = new System.Drawing.Size(16, 16);
            this.tabPrice.Location = new System.Drawing.Point(1, 25);
            this.tabPrice.Name = "tabPrice";
            this.tabPrice.ShowCloseButton = true;
            this.tabPrice.Size = new System.Drawing.Size(1119, 358);
            this.tabPrice.TabIndex = 3;
            this.tabPrice.Text = "Price Details";
            this.tabPrice.ThemesEnabled = false;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.BtnUpdatePrice);
            this.groupBox3.Controls.Add(this.label80);
            this.groupBox3.Controls.Add(this.cmbStylePrice);
            this.groupBox3.Controls.Add(this.DataGridStylePrice);
            this.groupBox3.Controls.Add(this.cmbPriceType);
            this.groupBox3.Controls.Add(this.label14);
            this.groupBox3.Controls.Add(this.label13);
            this.groupBox3.Location = new System.Drawing.Point(3, 3);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(1113, 350);
            this.groupBox3.TabIndex = 0;
            this.groupBox3.TabStop = false;
            // 
            // BtnUpdatePrice
            // 
            this.BtnUpdatePrice.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.BtnUpdatePrice.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnUpdatePrice.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.BtnUpdatePrice.Location = new System.Drawing.Point(619, 82);
            this.BtnUpdatePrice.Name = "BtnUpdatePrice";
            this.BtnUpdatePrice.Size = new System.Drawing.Size(107, 30);
            this.BtnUpdatePrice.TabIndex = 397;
            this.BtnUpdatePrice.Text = "Update Price";
            this.BtnUpdatePrice.UseVisualStyleBackColor = false;
            this.BtnUpdatePrice.Visible = false;
            this.BtnUpdatePrice.Click += new System.EventHandler(this.BtnUpdatePrice_Click);
            // 
            // label80
            // 
            this.label80.AutoSize = true;
            this.label80.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label80.ForeColor = System.Drawing.Color.Red;
            this.label80.Location = new System.Drawing.Point(623, 114);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(13, 15);
            this.label80.TabIndex = 1003;
            this.label80.Text = "*";
            // 
            // cmbStylePrice
            // 
            this.cmbStylePrice.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbStylePrice.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbStylePrice.FormattingEnabled = true;
            this.cmbStylePrice.Location = new System.Drawing.Point(74, 49);
            this.cmbStylePrice.Name = "cmbStylePrice";
            this.cmbStylePrice.Size = new System.Drawing.Size(344, 23);
            this.cmbStylePrice.TabIndex = 0;
            this.cmbStylePrice.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Control_KeyUp);
            // 
            // DataGridStylePrice
            // 
            this.DataGridStylePrice.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle19.BackColor = System.Drawing.Color.DeepSkyBlue;
            dataGridViewCellStyle19.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle19.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle19.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle19.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle19.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DataGridStylePrice.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle19;
            this.DataGridStylePrice.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridStylePrice.EnableHeadersVisualStyles = false;
            this.DataGridStylePrice.Location = new System.Drawing.Point(9, 78);
            this.DataGridStylePrice.Name = "DataGridStylePrice";
            this.DataGridStylePrice.RowHeadersVisible = false;
            this.DataGridStylePrice.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridStylePrice.Size = new System.Drawing.Size(608, 257);
            this.DataGridStylePrice.TabIndex = 26;
            this.DataGridStylePrice.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridStylePrice_CellValueChanged);
            this.DataGridStylePrice.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DataGridStylePrice_KeyDown);
            // 
            // cmbPriceType
            // 
            this.cmbPriceType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbPriceType.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbPriceType.FormattingEnabled = true;
            this.cmbPriceType.Items.AddRange(new object[] {
            "Style Wise",
            "Combo wise",
            "Size wise"});
            this.cmbPriceType.Location = new System.Drawing.Point(74, 19);
            this.cmbPriceType.Name = "cmbPriceType";
            this.cmbPriceType.Size = new System.Drawing.Size(150, 23);
            this.cmbPriceType.TabIndex = 1;
            this.cmbPriceType.SelectedIndexChanged += new System.EventHandler(this.CmbPriceType_SelectedIndexChanged);
            this.cmbPriceType.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Control_KeyUp);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(7, 23);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(63, 15);
            this.label14.TabIndex = 23;
            this.label14.Text = "Price Type";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(17, 53);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(53, 15);
            this.label13.TabIndex = 25;
            this.label13.Text = "Style No";
            // 
            // tabLogistics
            // 
            this.tabLogistics.Controls.Add(this.LblAvgDate);
            this.tabLogistics.Controls.Add(this.label91);
            this.tabLogistics.Controls.Add(this.label90);
            this.tabLogistics.Controls.Add(this.label89);
            this.tabLogistics.Controls.Add(this.label88);
            this.tabLogistics.Controls.Add(this.label87);
            this.tabLogistics.Controls.Add(this.label86);
            this.tabLogistics.Controls.Add(this.label85);
            this.tabLogistics.Controls.Add(this.label84);
            this.tabLogistics.Controls.Add(this.label83);
            this.tabLogistics.Controls.Add(this.label82);
            this.tabLogistics.Controls.Add(this.label81);
            this.tabLogistics.Controls.Add(this.btnDiscountOk);
            this.tabLogistics.Controls.Add(this.txtDicType);
            this.tabLogistics.Controls.Add(this.txtDisAmount);
            this.tabLogistics.Controls.Add(this.txtDisPer);
            this.tabLogistics.Controls.Add(this.txtDiscount);
            this.tabLogistics.Controls.Add(this.btnChargesOk);
            this.tabLogistics.Controls.Add(this.txtChargesType);
            this.tabLogistics.Controls.Add(this.txtChargesAmout);
            this.tabLogistics.Controls.Add(this.txtChargesPercetage);
            this.tabLogistics.Controls.Add(this.txtCharges);
            this.tabLogistics.Controls.Add(this.DataGridDiscounts);
            this.tabLogistics.Controls.Add(this.DataGridCharges);
            this.tabLogistics.Controls.Add(this.label30);
            this.tabLogistics.Controls.Add(this.label29);
            this.tabLogistics.Controls.Add(this.txtExRate);
            this.tabLogistics.Controls.Add(this.label28);
            this.tabLogistics.Controls.Add(this.txtCurrency);
            this.tabLogistics.Controls.Add(this.txtCountry);
            this.tabLogistics.Controls.Add(this.txtContact);
            this.tabLogistics.Controls.Add(this.label27);
            this.tabLogistics.Controls.Add(this.label26);
            this.tabLogistics.Controls.Add(this.label25);
            this.tabLogistics.Controls.Add(this.txtPayTerms);
            this.tabLogistics.Controls.Add(this.txtPayMode);
            this.tabLogistics.Controls.Add(this.txtShiftMode);
            this.tabLogistics.Controls.Add(this.txtShipType);
            this.tabLogistics.Controls.Add(this.label23);
            this.tabLogistics.Controls.Add(this.label24);
            this.tabLogistics.Controls.Add(this.label22);
            this.tabLogistics.Controls.Add(this.label21);
            this.tabLogistics.Controls.Add(this.txtCrossValue);
            this.tabLogistics.Controls.Add(this.label20);
            this.tabLogistics.Controls.Add(this.txtAvgRate);
            this.tabLogistics.Controls.Add(this.label19);
            this.tabLogistics.Controls.Add(this.txtReceivedBy);
            this.tabLogistics.Controls.Add(this.label18);
            this.tabLogistics.Controls.Add(this.dtpRecivedDt);
            this.tabLogistics.Controls.Add(this.label17);
            this.tabLogistics.Controls.Add(this.txtAgent);
            this.tabLogistics.Controls.Add(this.label16);
            this.tabLogistics.Controls.Add(this.txtDepartment);
            this.tabLogistics.Controls.Add(this.label15);
            this.tabLogistics.Image = null;
            this.tabLogistics.ImageSize = new System.Drawing.Size(16, 16);
            this.tabLogistics.Location = new System.Drawing.Point(1, 25);
            this.tabLogistics.Name = "tabLogistics";
            this.tabLogistics.ShowCloseButton = true;
            this.tabLogistics.Size = new System.Drawing.Size(1119, 358);
            this.tabLogistics.TabIndex = 4;
            this.tabLogistics.Text = "Logistics";
            this.tabLogistics.ThemesEnabled = false;
            // 
            // LblAvgDate
            // 
            this.LblAvgDate.AutoSize = true;
            this.LblAvgDate.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblAvgDate.Location = new System.Drawing.Point(170, 99);
            this.LblAvgDate.Name = "LblAvgDate";
            this.LblAvgDate.Size = new System.Drawing.Size(31, 15);
            this.LblAvgDate.TabIndex = 1014;
            this.LblAvgDate.Text = "0.00";
            // 
            // label91
            // 
            this.label91.AutoSize = true;
            this.label91.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label91.ForeColor = System.Drawing.Color.Red;
            this.label91.Location = new System.Drawing.Point(1000, 70);
            this.label91.Name = "label91";
            this.label91.Size = new System.Drawing.Size(13, 15);
            this.label91.TabIndex = 1013;
            this.label91.Text = "*";
            // 
            // label90
            // 
            this.label90.AutoSize = true;
            this.label90.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label90.ForeColor = System.Drawing.Color.Red;
            this.label90.Location = new System.Drawing.Point(859, 69);
            this.label90.Name = "label90";
            this.label90.Size = new System.Drawing.Size(13, 15);
            this.label90.TabIndex = 1012;
            this.label90.Text = "*";
            // 
            // label89
            // 
            this.label89.AutoSize = true;
            this.label89.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label89.ForeColor = System.Drawing.Color.Red;
            this.label89.Location = new System.Drawing.Point(1000, 41);
            this.label89.Name = "label89";
            this.label89.Size = new System.Drawing.Size(13, 15);
            this.label89.TabIndex = 1011;
            this.label89.Text = "*";
            // 
            // label88
            // 
            this.label88.AutoSize = true;
            this.label88.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label88.ForeColor = System.Drawing.Color.Red;
            this.label88.Location = new System.Drawing.Point(1000, 102);
            this.label88.Name = "label88";
            this.label88.Size = new System.Drawing.Size(13, 15);
            this.label88.TabIndex = 1010;
            this.label88.Text = "*";
            // 
            // label87
            // 
            this.label87.AutoSize = true;
            this.label87.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label87.ForeColor = System.Drawing.Color.Red;
            this.label87.Location = new System.Drawing.Point(684, 73);
            this.label87.Name = "label87";
            this.label87.Size = new System.Drawing.Size(13, 15);
            this.label87.TabIndex = 1009;
            this.label87.Text = "*";
            // 
            // label86
            // 
            this.label86.AutoSize = true;
            this.label86.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label86.ForeColor = System.Drawing.Color.Red;
            this.label86.Location = new System.Drawing.Point(684, 44);
            this.label86.Name = "label86";
            this.label86.Size = new System.Drawing.Size(13, 15);
            this.label86.TabIndex = 1008;
            this.label86.Text = "*";
            // 
            // label85
            // 
            this.label85.AutoSize = true;
            this.label85.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label85.ForeColor = System.Drawing.Color.Red;
            this.label85.Location = new System.Drawing.Point(684, 14);
            this.label85.Name = "label85";
            this.label85.Size = new System.Drawing.Size(13, 15);
            this.label85.TabIndex = 1007;
            this.label85.Text = "*";
            // 
            // label84
            // 
            this.label84.AutoSize = true;
            this.label84.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label84.ForeColor = System.Drawing.Color.Red;
            this.label84.Location = new System.Drawing.Point(431, 99);
            this.label84.Name = "label84";
            this.label84.Size = new System.Drawing.Size(13, 15);
            this.label84.TabIndex = 1006;
            this.label84.Text = "*";
            // 
            // label83
            // 
            this.label83.AutoSize = true;
            this.label83.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label83.ForeColor = System.Drawing.Color.Red;
            this.label83.Location = new System.Drawing.Point(157, 99);
            this.label83.Name = "label83";
            this.label83.Size = new System.Drawing.Size(13, 15);
            this.label83.TabIndex = 1005;
            this.label83.Text = "*";
            // 
            // label82
            // 
            this.label82.AutoSize = true;
            this.label82.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label82.ForeColor = System.Drawing.Color.Red;
            this.label82.Location = new System.Drawing.Point(435, 39);
            this.label82.Name = "label82";
            this.label82.Size = new System.Drawing.Size(13, 15);
            this.label82.TabIndex = 1004;
            this.label82.Text = "*";
            // 
            // label81
            // 
            this.label81.AutoSize = true;
            this.label81.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label81.ForeColor = System.Drawing.Color.Red;
            this.label81.Location = new System.Drawing.Point(435, 10);
            this.label81.Name = "label81";
            this.label81.Size = new System.Drawing.Size(13, 15);
            this.label81.TabIndex = 1003;
            this.label81.Text = "*";
            // 
            // btnDiscountOk
            // 
            this.btnDiscountOk.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDiscountOk.Location = new System.Drawing.Point(969, 150);
            this.btnDiscountOk.Name = "btnDiscountOk";
            this.btnDiscountOk.Size = new System.Drawing.Size(33, 23);
            this.btnDiscountOk.TabIndex = 23;
            this.btnDiscountOk.Text = "Ok";
            this.btnDiscountOk.UseVisualStyleBackColor = true;
            // 
            // txtDicType
            // 
            this.txtDicType.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDicType.Location = new System.Drawing.Point(883, 150);
            this.txtDicType.Name = "txtDicType";
            this.txtDicType.Size = new System.Drawing.Size(86, 23);
            this.txtDicType.TabIndex = 22;
            this.txtDicType.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Control_KeyUp);
            // 
            // txtDisAmount
            // 
            this.txtDisAmount.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDisAmount.Location = new System.Drawing.Point(796, 150);
            this.txtDisAmount.Name = "txtDisAmount";
            this.txtDisAmount.Size = new System.Drawing.Size(86, 23);
            this.txtDisAmount.TabIndex = 21;
            this.txtDisAmount.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Control_KeyUp);
            // 
            // txtDisPer
            // 
            this.txtDisPer.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDisPer.Location = new System.Drawing.Point(727, 150);
            this.txtDisPer.Name = "txtDisPer";
            this.txtDisPer.Size = new System.Drawing.Size(68, 23);
            this.txtDisPer.TabIndex = 20;
            this.txtDisPer.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Control_KeyUp);
            // 
            // txtDiscount
            // 
            this.txtDiscount.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDiscount.Location = new System.Drawing.Point(513, 150);
            this.txtDiscount.Name = "txtDiscount";
            this.txtDiscount.Size = new System.Drawing.Size(213, 23);
            this.txtDiscount.TabIndex = 19;
            this.txtDiscount.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Control_KeyUp);
            // 
            // btnChargesOk
            // 
            this.btnChargesOk.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnChargesOk.Location = new System.Drawing.Point(473, 150);
            this.btnChargesOk.Name = "btnChargesOk";
            this.btnChargesOk.Size = new System.Drawing.Size(33, 23);
            this.btnChargesOk.TabIndex = 18;
            this.btnChargesOk.Text = "Ok";
            this.btnChargesOk.UseVisualStyleBackColor = true;
            // 
            // txtChargesType
            // 
            this.txtChargesType.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtChargesType.Location = new System.Drawing.Point(388, 150);
            this.txtChargesType.Name = "txtChargesType";
            this.txtChargesType.Size = new System.Drawing.Size(85, 23);
            this.txtChargesType.TabIndex = 17;
            this.txtChargesType.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Control_KeyUp);
            // 
            // txtChargesAmout
            // 
            this.txtChargesAmout.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtChargesAmout.Location = new System.Drawing.Point(301, 150);
            this.txtChargesAmout.Name = "txtChargesAmout";
            this.txtChargesAmout.Size = new System.Drawing.Size(86, 23);
            this.txtChargesAmout.TabIndex = 16;
            this.txtChargesAmout.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Control_KeyUp);
            // 
            // txtChargesPercetage
            // 
            this.txtChargesPercetage.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtChargesPercetage.Location = new System.Drawing.Point(230, 150);
            this.txtChargesPercetage.Name = "txtChargesPercetage";
            this.txtChargesPercetage.Size = new System.Drawing.Size(69, 23);
            this.txtChargesPercetage.TabIndex = 15;
            this.txtChargesPercetage.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Control_KeyUp);
            // 
            // txtCharges
            // 
            this.txtCharges.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCharges.Location = new System.Drawing.Point(17, 150);
            this.txtCharges.Name = "txtCharges";
            this.txtCharges.Size = new System.Drawing.Size(212, 23);
            this.txtCharges.TabIndex = 15;
            this.txtCharges.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Control_KeyUp);
            // 
            // DataGridDiscounts
            // 
            this.DataGridDiscounts.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle20.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle20.BackColor = System.Drawing.Color.DeepSkyBlue;
            dataGridViewCellStyle20.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle20.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle20.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle20.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle20.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DataGridDiscounts.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle20;
            this.DataGridDiscounts.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridDiscounts.EnableHeadersVisualStyles = false;
            this.DataGridDiscounts.Location = new System.Drawing.Point(512, 174);
            this.DataGridDiscounts.Name = "DataGridDiscounts";
            this.DataGridDiscounts.ReadOnly = true;
            this.DataGridDiscounts.RowHeadersVisible = false;
            this.DataGridDiscounts.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridDiscounts.Size = new System.Drawing.Size(490, 135);
            this.DataGridDiscounts.TabIndex = 51;
            // 
            // DataGridCharges
            // 
            this.DataGridCharges.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle21.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle21.BackColor = System.Drawing.Color.DeepSkyBlue;
            dataGridViewCellStyle21.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle21.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle21.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle21.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle21.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DataGridCharges.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle21;
            this.DataGridCharges.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridCharges.EnableHeadersVisualStyles = false;
            this.DataGridCharges.Location = new System.Drawing.Point(17, 174);
            this.DataGridCharges.Name = "DataGridCharges";
            this.DataGridCharges.ReadOnly = true;
            this.DataGridCharges.RowHeadersVisible = false;
            this.DataGridCharges.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridCharges.Size = new System.Drawing.Size(487, 135);
            this.DataGridCharges.TabIndex = 50;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(509, 132);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(59, 15);
            this.label30.TabIndex = 49;
            this.label30.Text = "Discounts";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(14, 132);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(50, 15);
            this.label29.TabIndex = 48;
            this.label29.Text = "Charges";
            // 
            // txtExRate
            // 
            this.txtExRate.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtExRate.Location = new System.Drawing.Point(926, 65);
            this.txtExRate.Name = "txtExRate";
            this.txtExRate.Size = new System.Drawing.Size(71, 23);
            this.txtExRate.TabIndex = 12;
            this.txtExRate.TextChanged += new System.EventHandler(this.TxtAvgRate_TextChanged);
            this.txtExRate.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Control_KeyUp);
            this.txtExRate.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxtQuantity_KeyPress);
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(873, 69);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(47, 15);
            this.label28.TabIndex = 46;
            this.label28.Text = "Ex-Rate";
            // 
            // txtCurrency
            // 
            this.txtCurrency.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCurrency.Location = new System.Drawing.Point(755, 65);
            this.txtCurrency.Name = "txtCurrency";
            this.txtCurrency.Size = new System.Drawing.Size(102, 23);
            this.txtCurrency.TabIndex = 11;
            this.txtCurrency.Click += new System.EventHandler(this.TxtCurrency_Click);
            this.txtCurrency.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TxtOrderUnit_KeyDown);
            // 
            // txtCountry
            // 
            this.txtCountry.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCountry.Location = new System.Drawing.Point(755, 36);
            this.txtCountry.Name = "txtCountry";
            this.txtCountry.Size = new System.Drawing.Size(242, 23);
            this.txtCountry.TabIndex = 10;
            this.txtCountry.Click += new System.EventHandler(this.TxtCountry_Click);
            this.txtCountry.TextChanged += new System.EventHandler(this.TxtCountry_TextChanged);
            this.txtCountry.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TxtOrderUnit_KeyDown);
            // 
            // txtContact
            // 
            this.txtContact.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtContact.Location = new System.Drawing.Point(755, 7);
            this.txtContact.Name = "txtContact";
            this.txtContact.Size = new System.Drawing.Size(242, 23);
            this.txtContact.TabIndex = 9;
            this.txtContact.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Control_KeyUp);
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(693, 69);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(56, 15);
            this.label27.TabIndex = 42;
            this.label27.Text = "Currency";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(699, 40);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(51, 15);
            this.label26.TabIndex = 41;
            this.label26.Text = "Country";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(700, 11);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(49, 15);
            this.label25.TabIndex = 40;
            this.label25.Text = "Contact";
            // 
            // txtPayTerms
            // 
            this.txtPayTerms.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPayTerms.Location = new System.Drawing.Point(512, 98);
            this.txtPayTerms.Name = "txtPayTerms";
            this.txtPayTerms.Size = new System.Drawing.Size(485, 23);
            this.txtPayTerms.TabIndex = 13;
            this.txtPayTerms.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Control_KeyUp);
            // 
            // txtPayMode
            // 
            this.txtPayMode.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPayMode.Location = new System.Drawing.Point(512, 68);
            this.txtPayMode.Name = "txtPayMode";
            this.txtPayMode.Size = new System.Drawing.Size(169, 23);
            this.txtPayMode.TabIndex = 8;
            this.txtPayMode.Click += new System.EventHandler(this.TxtPayMode_Click);
            this.txtPayMode.TextChanged += new System.EventHandler(this.TxtPayMode_TextChanged);
            this.txtPayMode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TxtOrderUnit_KeyDown);
            // 
            // txtShiftMode
            // 
            this.txtShiftMode.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtShiftMode.Location = new System.Drawing.Point(512, 39);
            this.txtShiftMode.Name = "txtShiftMode";
            this.txtShiftMode.Size = new System.Drawing.Size(168, 23);
            this.txtShiftMode.TabIndex = 7;
            this.txtShiftMode.Click += new System.EventHandler(this.TxtShiftMode_Click);
            this.txtShiftMode.TextChanged += new System.EventHandler(this.TxtShiftMode_TextChanged);
            this.txtShiftMode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TxtOrderUnit_KeyDown);
            // 
            // txtShipType
            // 
            this.txtShipType.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtShipType.Location = new System.Drawing.Point(512, 10);
            this.txtShipType.Name = "txtShipType";
            this.txtShipType.Size = new System.Drawing.Size(167, 23);
            this.txtShipType.TabIndex = 6;
            this.txtShipType.Click += new System.EventHandler(this.TxtShipType_Click);
            this.txtShipType.TextChanged += new System.EventHandler(this.TxtShipType_TextChanged);
            this.txtShipType.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TxtOrderUnit_KeyDown);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(446, 101);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(62, 15);
            this.label23.TabIndex = 35;
            this.label23.Text = "Pay Terms";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(454, 72);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(61, 15);
            this.label24.TabIndex = 34;
            this.label24.Text = "Pay Mode";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(446, 43);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(66, 15);
            this.label22.TabIndex = 33;
            this.label22.Text = "Shift Mode";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(454, 14);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(59, 15);
            this.label21.TabIndex = 32;
            this.label21.Text = "Ship Type";
            // 
            // txtCrossValue
            // 
            this.txtCrossValue.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCrossValue.Location = new System.Drawing.Point(303, 94);
            this.txtCrossValue.Name = "txtCrossValue";
            this.txtCrossValue.Size = new System.Drawing.Size(127, 23);
            this.txtCrossValue.TabIndex = 5;
            this.txtCrossValue.TextChanged += new System.EventHandler(this.txtCrossValue_TextChanged);
            this.txtCrossValue.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Control_KeyUp);
            this.txtCrossValue.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxtQuantity_KeyPress);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(227, 98);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(72, 15);
            this.label20.TabIndex = 30;
            this.label20.Text = "Gross Value";
            // 
            // txtAvgRate
            // 
            this.txtAvgRate.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAvgRate.Location = new System.Drawing.Point(85, 95);
            this.txtAvgRate.Name = "txtAvgRate";
            this.txtAvgRate.Size = new System.Drawing.Size(69, 23);
            this.txtAvgRate.TabIndex = 4;
            this.txtAvgRate.TextChanged += new System.EventHandler(this.TxtAvgRate_TextChanged);
            this.txtAvgRate.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Control_KeyUp);
            this.txtAvgRate.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxtQuantity_KeyPress);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(30, 99);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(55, 15);
            this.label19.TabIndex = 28;
            this.label19.Text = "Avg Rate";
            // 
            // txtReceivedBy
            // 
            this.txtReceivedBy.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtReceivedBy.Location = new System.Drawing.Point(239, 64);
            this.txtReceivedBy.Name = "txtReceivedBy";
            this.txtReceivedBy.Size = new System.Drawing.Size(193, 23);
            this.txtReceivedBy.TabIndex = 3;
            this.txtReceivedBy.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Control_KeyUp);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(215, 68);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(20, 15);
            this.label18.TabIndex = 27;
            this.label18.Text = "By";
            // 
            // dtpRecivedDt
            // 
            this.dtpRecivedDt.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpRecivedDt.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpRecivedDt.Location = new System.Drawing.Point(86, 64);
            this.dtpRecivedDt.Name = "dtpRecivedDt";
            this.dtpRecivedDt.Size = new System.Drawing.Size(128, 23);
            this.dtpRecivedDt.TabIndex = 2;
            this.dtpRecivedDt.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Control_KeyUp);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(14, 68);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(72, 15);
            this.label17.TabIndex = 26;
            this.label17.Text = "Received Dt";
            // 
            // txtAgent
            // 
            this.txtAgent.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAgent.Location = new System.Drawing.Point(86, 36);
            this.txtAgent.Name = "txtAgent";
            this.txtAgent.Size = new System.Drawing.Size(346, 23);
            this.txtAgent.TabIndex = 1;
            this.txtAgent.Click += new System.EventHandler(this.TxtAgent_Click);
            this.txtAgent.TextChanged += new System.EventHandler(this.TxtAgent_TextChanged);
            this.txtAgent.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TxtOrderUnit_KeyDown);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(47, 39);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(40, 15);
            this.label16.TabIndex = 25;
            this.label16.Text = "Agent";
            // 
            // txtDepartment
            // 
            this.txtDepartment.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDepartment.Location = new System.Drawing.Point(86, 7);
            this.txtDepartment.Name = "txtDepartment";
            this.txtDepartment.Size = new System.Drawing.Size(346, 23);
            this.txtDepartment.TabIndex = 0;
            this.txtDepartment.Click += new System.EventHandler(this.TxtDepartment_Click);
            this.txtDepartment.TextChanged += new System.EventHandler(this.TxtDepartment_TextChanged);
            this.txtDepartment.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TxtOrderUnit_KeyDown);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(13, 10);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(75, 15);
            this.label15.TabIndex = 23;
            this.label15.Text = "Department";
            // 
            // tabFabric
            // 
            this.tabFabric.Controls.Add(this.groupBox7);
            this.tabFabric.Image = null;
            this.tabFabric.ImageSize = new System.Drawing.Size(16, 16);
            this.tabFabric.Location = new System.Drawing.Point(1, 25);
            this.tabFabric.Name = "tabFabric";
            this.tabFabric.ShowCloseButton = true;
            this.tabFabric.Size = new System.Drawing.Size(1119, 358);
            this.tabFabric.TabIndex = 8;
            this.tabFabric.Text = "Fabric Details";
            this.tabFabric.ThemesEnabled = false;
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.label94);
            this.groupBox7.Controls.Add(this.label93);
            this.groupBox7.Controls.Add(this.label92);
            this.groupBox7.Controls.Add(this.btnFabric);
            this.groupBox7.Controls.Add(this.txtTalarance);
            this.groupBox7.Controls.Add(this.txtGSM);
            this.groupBox7.Controls.Add(this.txtStructureType);
            this.groupBox7.Controls.Add(this.DataGridFabricDet);
            this.groupBox7.Controls.Add(this.label44);
            this.groupBox7.Controls.Add(this.label43);
            this.groupBox7.Controls.Add(this.label42);
            this.groupBox7.Location = new System.Drawing.Point(5, 3);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(1111, 350);
            this.groupBox7.TabIndex = 0;
            this.groupBox7.TabStop = false;
            // 
            // label94
            // 
            this.label94.AutoSize = true;
            this.label94.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label94.ForeColor = System.Drawing.Color.Red;
            this.label94.Location = new System.Drawing.Point(464, 44);
            this.label94.Name = "label94";
            this.label94.Size = new System.Drawing.Size(13, 15);
            this.label94.TabIndex = 1012;
            this.label94.Text = "*";
            // 
            // label93
            // 
            this.label93.AutoSize = true;
            this.label93.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label93.ForeColor = System.Drawing.Color.Red;
            this.label93.Location = new System.Drawing.Point(335, 41);
            this.label93.Name = "label93";
            this.label93.Size = new System.Drawing.Size(13, 15);
            this.label93.TabIndex = 1011;
            this.label93.Text = "*";
            // 
            // label92
            // 
            this.label92.AutoSize = true;
            this.label92.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label92.ForeColor = System.Drawing.Color.Red;
            this.label92.Location = new System.Drawing.Point(139, 44);
            this.label92.Name = "label92";
            this.label92.Size = new System.Drawing.Size(13, 15);
            this.label92.TabIndex = 1010;
            this.label92.Text = "*";
            // 
            // btnFabric
            // 
            this.btnFabric.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFabric.Location = new System.Drawing.Point(505, 58);
            this.btnFabric.Name = "btnFabric";
            this.btnFabric.Size = new System.Drawing.Size(35, 25);
            this.btnFabric.TabIndex = 3;
            this.btnFabric.Text = "Ok";
            this.btnFabric.UseVisualStyleBackColor = true;
            this.btnFabric.Click += new System.EventHandler(this.BtnFabric_Click);
            // 
            // txtTalarance
            // 
            this.txtTalarance.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTalarance.Location = new System.Drawing.Point(408, 59);
            this.txtTalarance.Name = "txtTalarance";
            this.txtTalarance.Size = new System.Drawing.Size(97, 23);
            this.txtTalarance.TabIndex = 2;
            this.txtTalarance.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Control_KeyUp);
            // 
            // txtGSM
            // 
            this.txtGSM.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGSM.Location = new System.Drawing.Point(305, 59);
            this.txtGSM.Name = "txtGSM";
            this.txtGSM.Size = new System.Drawing.Size(102, 23);
            this.txtGSM.TabIndex = 1;
            this.txtGSM.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Control_KeyUp);
            // 
            // txtStructureType
            // 
            this.txtStructureType.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtStructureType.Location = new System.Drawing.Point(46, 59);
            this.txtStructureType.Name = "txtStructureType";
            this.txtStructureType.Size = new System.Drawing.Size(258, 23);
            this.txtStructureType.TabIndex = 0;
            this.txtStructureType.Click += new System.EventHandler(this.TxtStructureType_Click);
            this.txtStructureType.TextChanged += new System.EventHandler(this.TxtStructureType_TextChanged);
            this.txtStructureType.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TxtOrderUnit_KeyDown);
            // 
            // DataGridFabricDet
            // 
            this.DataGridFabricDet.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle22.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle22.BackColor = System.Drawing.Color.DeepSkyBlue;
            dataGridViewCellStyle22.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle22.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle22.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle22.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle22.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DataGridFabricDet.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle22;
            this.DataGridFabricDet.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridFabricDet.EnableHeadersVisualStyles = false;
            this.DataGridFabricDet.Location = new System.Drawing.Point(46, 84);
            this.DataGridFabricDet.Name = "DataGridFabricDet";
            this.DataGridFabricDet.ReadOnly = true;
            this.DataGridFabricDet.RowHeadersVisible = false;
            this.DataGridFabricDet.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridFabricDet.Size = new System.Drawing.Size(493, 254);
            this.DataGridFabricDet.TabIndex = 28;
            this.DataGridFabricDet.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.DataGridFabricDet_CellMouseDoubleClick);
            this.DataGridFabricDet.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DataGridFabricDet_KeyDown);
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label44.Location = new System.Drawing.Point(408, 41);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(57, 15);
            this.label44.TabIndex = 27;
            this.label44.Text = "Talarance";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label43.Location = new System.Drawing.Point(305, 41);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(32, 15);
            this.label43.TabIndex = 26;
            this.label43.Text = "GSM";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label42.Location = new System.Drawing.Point(46, 42);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(94, 15);
            this.label42.TabIndex = 25;
            this.label42.Text = "Fabric Structure";
            // 
            // cmbYear
            // 
            this.cmbYear.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbYear.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbYear.FormattingEnabled = true;
            this.cmbYear.Items.AddRange(new object[] {
            "2010",
            "2011",
            "2012",
            "2013",
            "2014",
            "2015",
            "2016",
            "2017",
            "2018"});
            this.cmbYear.Location = new System.Drawing.Point(573, 44);
            this.cmbYear.Name = "cmbYear";
            this.cmbYear.Size = new System.Drawing.Size(128, 23);
            this.cmbYear.TabIndex = 6;
            this.cmbYear.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Control_KeyUp);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(539, 48);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(30, 15);
            this.label7.TabIndex = 13;
            this.label7.Text = "Year";
            // 
            // cmbSeason
            // 
            this.cmbSeason.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSeason.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbSeason.FormattingEnabled = true;
            this.cmbSeason.Location = new System.Drawing.Point(353, 44);
            this.cmbSeason.Name = "cmbSeason";
            this.cmbSeason.Size = new System.Drawing.Size(180, 23);
            this.cmbSeason.TabIndex = 5;
            this.cmbSeason.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Control_KeyUp);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(300, 48);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(45, 15);
            this.label6.TabIndex = 11;
            this.label6.Text = "Season";
            // 
            // txtCustomer
            // 
            this.txtCustomer.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCustomer.Location = new System.Drawing.Point(353, 14);
            this.txtCustomer.Name = "txtCustomer";
            this.txtCustomer.Size = new System.Drawing.Size(348, 23);
            this.txtCustomer.TabIndex = 4;
            this.txtCustomer.Click += new System.EventHandler(this.TxtCustomer_Click);
            this.txtCustomer.TextChanged += new System.EventHandler(this.TxtCustomer_TextChanged);
            this.txtCustomer.Enter += new System.EventHandler(this.TxtCustomer_Click);
            this.txtCustomer.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TxtOrderUnit_KeyDown);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(284, 17);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(61, 15);
            this.label5.TabIndex = 9;
            this.label5.Text = "Customer";
            // 
            // dtpOrderDate
            // 
            this.dtpOrderDate.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpOrderDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpOrderDate.Location = new System.Drawing.Point(72, 114);
            this.dtpOrderDate.Name = "dtpOrderDate";
            this.dtpOrderDate.Size = new System.Drawing.Size(136, 23);
            this.dtpOrderDate.TabIndex = 3;
            this.dtpOrderDate.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Control_KeyUp);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(3, 118);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(69, 15);
            this.label3.TabIndex = 7;
            this.label3.Text = "Order Date";
            // 
            // txtOrderNo
            // 
            this.txtOrderNo.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtOrderNo.Location = new System.Drawing.Point(72, 80);
            this.txtOrderNo.Name = "txtOrderNo";
            this.txtOrderNo.Size = new System.Drawing.Size(136, 23);
            this.txtOrderNo.TabIndex = 2;
            this.txtOrderNo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Control_KeyUp);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(13, 84);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(59, 15);
            this.label4.TabIndex = 5;
            this.label4.Text = "Order No";
            // 
            // dtpDocDate
            // 
            this.dtpDocDate.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpDocDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpDocDate.Location = new System.Drawing.Point(72, 45);
            this.dtpDocDate.Name = "dtpDocDate";
            this.dtpDocDate.Size = new System.Drawing.Size(136, 23);
            this.dtpDocDate.TabIndex = 1;
            this.dtpDocDate.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Control_KeyUp);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(16, 49);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 15);
            this.label2.TabIndex = 3;
            this.label2.Text = "Doc Date";
            // 
            // txtDocNo
            // 
            this.txtDocNo.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDocNo.Location = new System.Drawing.Point(72, 14);
            this.txtDocNo.Name = "txtDocNo";
            this.txtDocNo.Size = new System.Drawing.Size(136, 23);
            this.txtDocNo.TabIndex = 0;
            this.txtDocNo.Click += new System.EventHandler(this.TxtDocNo_Click);
            this.txtDocNo.TextChanged += new System.EventHandler(this.TxtDocNo_TextChanged);
            this.txtDocNo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Control_KeyUp);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(26, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 15);
            this.label1.TabIndex = 1;
            this.label1.Text = "Doc No";
            // 
            // txtMerchindiser
            // 
            this.txtMerchindiser.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMerchindiser.Location = new System.Drawing.Point(353, 113);
            this.txtMerchindiser.Name = "txtMerchindiser";
            this.txtMerchindiser.Size = new System.Drawing.Size(348, 23);
            this.txtMerchindiser.TabIndex = 8;
            this.txtMerchindiser.Click += new System.EventHandler(this.TxtMerchindiser_Click);
            this.txtMerchindiser.TextChanged += new System.EventHandler(this.TxtMerchindiser_TextChanged);
            this.txtMerchindiser.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TxtOrderUnit_KeyDown);
            // 
            // dtpDeliveryDate
            // 
            this.dtpDeliveryDate.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpDeliveryDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpDeliveryDate.Location = new System.Drawing.Point(353, 79);
            this.dtpDeliveryDate.Name = "dtpDeliveryDate";
            this.dtpDeliveryDate.Size = new System.Drawing.Size(136, 23);
            this.dtpDeliveryDate.TabIndex = 7;
            this.dtpDeliveryDate.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Control_KeyUp);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(264, 83);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(81, 15);
            this.label8.TabIndex = 15;
            this.label8.Text = "Delivery Date";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(266, 117);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(79, 15);
            this.label9.TabIndex = 17;
            this.label9.Text = "Merchindiser";
            // 
            // cmbOrderType
            // 
            this.cmbOrderType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbOrderType.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbOrderType.FormattingEnabled = true;
            this.cmbOrderType.Location = new System.Drawing.Point(781, 44);
            this.cmbOrderType.Name = "cmbOrderType";
            this.cmbOrderType.Size = new System.Drawing.Size(150, 23);
            this.cmbOrderType.TabIndex = 10;
            this.cmbOrderType.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Control_KeyUp);
            // 
            // cmbSts
            // 
            this.cmbSts.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSts.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbSts.FormattingEnabled = true;
            this.cmbSts.Location = new System.Drawing.Point(784, 14);
            this.cmbSts.Name = "cmbSts";
            this.cmbSts.Size = new System.Drawing.Size(150, 23);
            this.cmbSts.TabIndex = 9;
            this.cmbSts.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Control_KeyUp);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(708, 48);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(69, 15);
            this.label11.TabIndex = 20;
            this.label11.Text = "Order Type";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(736, 17);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(41, 15);
            this.label10.TabIndex = 19;
            this.label10.Text = "Status";
            // 
            // grFront
            // 
            this.grFront.Controls.Add(this.BtnDownloadtoExcel);
            this.grFront.Controls.Add(this.btnPrint);
            this.grFront.Controls.Add(this.btnAdd);
            this.grFront.Controls.Add(this.SfdDataGridOrderEntry);
            this.grFront.Location = new System.Drawing.Point(4, 2);
            this.grFront.Name = "grFront";
            this.grFront.Size = new System.Drawing.Size(1135, 574);
            this.grFront.TabIndex = 23;
            this.grFront.TabStop = false;
            // 
            // BtnDownloadtoExcel
            // 
            this.BtnDownloadtoExcel.Location = new System.Drawing.Point(99, 535);
            this.BtnDownloadtoExcel.Name = "BtnDownloadtoExcel";
            this.BtnDownloadtoExcel.Size = new System.Drawing.Size(109, 30);
            this.BtnDownloadtoExcel.TabIndex = 3;
            this.BtnDownloadtoExcel.Text = "Download to Excel";
            this.BtnDownloadtoExcel.UseVisualStyleBackColor = true;
            this.BtnDownloadtoExcel.Click += new System.EventHandler(this.BtnDownloadtoExcel_Click);
            // 
            // btnPrint
            // 
            this.btnPrint.Location = new System.Drawing.Point(2, 535);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(82, 30);
            this.btnPrint.TabIndex = 2;
            this.btnPrint.Text = "Print";
            this.btnPrint.UseVisualStyleBackColor = true;
            this.btnPrint.Click += new System.EventHandler(this.BtnPrint_Click);
            // 
            // SfdDataGridOrderEntry
            // 
            this.SfdDataGridOrderEntry.AccessibleName = "Table";
            this.SfdDataGridOrderEntry.AllowDraggingColumns = true;
            this.SfdDataGridOrderEntry.AllowFiltering = true;
            this.SfdDataGridOrderEntry.BackColor = System.Drawing.Color.White;
            this.SfdDataGridOrderEntry.Location = new System.Drawing.Point(4, 11);
            this.SfdDataGridOrderEntry.Name = "SfdDataGridOrderEntry";
            this.SfdDataGridOrderEntry.Size = new System.Drawing.Size(1124, 513);
            this.SfdDataGridOrderEntry.TabIndex = 0;
            this.SfdDataGridOrderEntry.Text = "sfDataGrid1";
            this.SfdDataGridOrderEntry.QueryCellStyle += new Syncfusion.WinForms.DataGrid.Events.QueryCellStyleEventHandler(this.SfdDataGridOrderEntry_QueryCellStyle);
            // 
            // FrmOrderEntry
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1144, 581);
            this.Controls.Add(this.grFront);
            this.Controls.Add(this.grBack);
            this.Name = "FrmOrderEntry";
            this.Text = "Order Entry";
            this.Load += new System.EventHandler(this.FrmOrderEntry_Load);
            this.grBack.ResumeLayout(false);
            this.grBack.PerformLayout();
            this.grSearch.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DataGridCommon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabControlOrderEntry)).EndInit();
            this.tabControlOrderEntry.ResumeLayout(false);
            this.tabStyleNo.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridStyle)).EndInit();
            this.tabAssortment.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.grImport.ResumeLayout(false);
            this.grImport.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridAssort)).EndInit();
            this.tabPageSize.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridStyleSizeCombo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridStyleSize)).EndInit();
            this.tabCombo.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridCombo)).EndInit();
            this.tabSizeMatrix.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridSizeMatrix)).EndInit();
            this.tabPlanning.ResumeLayout(false);
            this.tabPlanning.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridPlanning)).EndInit();
            this.tabPrice.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridStylePrice)).EndInit();
            this.tabLogistics.ResumeLayout(false);
            this.tabLogistics.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridDiscounts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridCharges)).EndInit();
            this.tabFabric.ResumeLayout(false);
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridFabricDet)).EndInit();
            this.grFront.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.SfdDataGridOrderEntry)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grBack;
        private Syncfusion.Windows.Forms.Tools.TabControlAdv tabControlOrderEntry;
        private Syncfusion.Windows.Forms.Tools.TabPageAdv tabStyleNo;
        private Syncfusion.Windows.Forms.Tools.TabPageAdv tabCombo;
        private Syncfusion.Windows.Forms.Tools.TabPageAdv tabPrice;
        private Syncfusion.Windows.Forms.Tools.TabPageAdv tabLogistics;
        private System.Windows.Forms.DateTimePicker dtpDocDate;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtDocNo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker dtpOrderDate;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtOrderNo;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtCustomer;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cmbYear;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox cmbSeason;
        private System.Windows.Forms.DateTimePicker dtpDeliveryDate;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtMerchindiser;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox cmbOrderType;
        private System.Windows.Forms.ComboBox cmbSts;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnStyleOk;
        private System.Windows.Forms.TextBox txtQuantity;
        private System.Windows.Forms.TextBox txtdescription;
        private System.Windows.Forms.TextBox txtStyleNo;
        private System.Windows.Forms.DataGridView DataGridStyle;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.DataGridView DataGridCombo;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ComboBox cmbPriceType;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.DataGridView DataGridStylePrice;
        private System.Windows.Forms.TextBox txtDepartment;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtAgent;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.DateTimePicker dtpRecivedDt;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtReceivedBy;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txtCrossValue;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox txtAvgRate;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox txtPayTerms;
        private System.Windows.Forms.TextBox txtPayMode;
        private System.Windows.Forms.TextBox txtShiftMode;
        private System.Windows.Forms.TextBox txtShipType;
        private System.Windows.Forms.TextBox txtCountry;
        private System.Windows.Forms.TextBox txtContact;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox txtExRate;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.TextBox txtCurrency;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.DataGridView DataGridDiscounts;
        private System.Windows.Forms.DataGridView DataGridCharges;
        private System.Windows.Forms.GroupBox grFront;
        private Syncfusion.WinForms.DataGrid.SfDataGrid SfdDataGridOrderEntry;
        private Syncfusion.Windows.Forms.Tools.SplitButton btnSave;
        private Syncfusion.Windows.Forms.Tools.SplitButton btnAdd;
        private Syncfusion.Windows.Forms.Tools.TabPageAdv tabPageSize;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.ComboBox CmbStyle;
        private System.Windows.Forms.Button btnStyleSizeOk;
        private System.Windows.Forms.TextBox txtStyleSize;
        private System.Windows.Forms.DataGridView DataGridStyleSize;
        private System.Windows.Forms.ComboBox cmbAssort;
        private System.Windows.Forms.ComboBox cmbStylePrice;
        private System.Windows.Forms.TextBox txtChargesPercetage;
        private System.Windows.Forms.TextBox txtCharges;
        private System.Windows.Forms.Button btnChargesOk;
        private System.Windows.Forms.TextBox txtChargesType;
        private System.Windows.Forms.TextBox txtChargesAmout;
        private System.Windows.Forms.Button btnDiscountOk;
        private System.Windows.Forms.TextBox txtDicType;
        private System.Windows.Forms.TextBox txtDisAmount;
        private System.Windows.Forms.TextBox txtDisPer;
        private System.Windows.Forms.TextBox txtDiscount;
        private System.Windows.Forms.Button btnComboOk;
        private System.Windows.Forms.TextBox txtCombo;
        private System.Windows.Forms.Label label33;
        private Syncfusion.Windows.Forms.Tools.TabPageAdv tabAssortment;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.ComboBox cmbStyleAssort;
        private System.Windows.Forms.DateTimePicker dtpAssortDelDt;
        private System.Windows.Forms.TextBox txtAssortQuantity;
        private System.Windows.Forms.TextBox txtAssortmentType;
        private System.Windows.Forms.TextBox txtRefNo;
        private System.Windows.Forms.DataGridView DataGridAssort;
        private System.Windows.Forms.ComboBox cmbCountry;
        private System.Windows.Forms.Button btnAssortOk;
        private System.Windows.Forms.TextBox txtFinalDistination;
        private System.Windows.Forms.TextBox txtPortofDelivery;
        private System.Windows.Forms.DateTimePicker dtpAssortShipDt;
        private System.Windows.Forms.TextBox txtComboQty;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.ComboBox cmbQtyStyle;
        private Syncfusion.Windows.Forms.Tools.TabPageAdv tabSizeMatrix;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.ComboBox cmbMatrixStyle;
        private System.Windows.Forms.DataGridView DataGridSizeMatrix;
        private System.Windows.Forms.TextBox txtMCBox;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Button btnPrint;
        private Syncfusion.Windows.Forms.Tools.TabPageAdv tabFabric;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.TextBox txtTalarance;
        private System.Windows.Forms.TextBox txtGSM;
        private System.Windows.Forms.TextBox txtStructureType;
        private System.Windows.Forms.DataGridView DataGridFabricDet;
        private System.Windows.Forms.Button btnFabric;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.ComboBox CmbPlanUom;
        private System.Windows.Forms.ComboBox cmbOrderUOm;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.DataGridView DataGridStyleSizeCombo;
        private Syncfusion.Windows.Forms.Tools.TabPageAdv tabPlanning;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.ComboBox CmbPlanningStyle;
        private System.Windows.Forms.DataGridView DataGridPlanning;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.ComboBox CmbPlanningStyleCombo;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.TextBox txtBuyerTolerance;
        private System.Windows.Forms.TextBox txtCuttingTolerance;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.Button btnCalculate;
        private System.Windows.Forms.GroupBox grImport;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label lblPath;
        private System.Windows.Forms.ComboBox CmbAssortImport;
        private System.Windows.Forms.ComboBox CmbStyleImport;
        private System.Windows.Forms.Button BtnImportHide;
        private System.Windows.Forms.Button BtnReferesh;
        private System.Windows.Forms.CheckBox ChckAll;
        private System.Windows.Forms.CheckBox ChckRefresh;
        private System.Windows.Forms.GroupBox grSearch;
        private System.Windows.Forms.Button btnSelect;
        private System.Windows.Forms.DataGridView DataGridCommon;
        private System.Windows.Forms.Button btnHide;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.TextBox txtStyleDesc;
        private System.Windows.Forms.Button BtnDownloadtoExcel;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.Label label72;
        private System.Windows.Forms.Label label71;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.Label label75;
        private System.Windows.Forms.Label label74;
        private System.Windows.Forms.Label label73;
        private System.Windows.Forms.Label label76;
        private System.Windows.Forms.Label label77;
        private System.Windows.Forms.Label label79;
        private System.Windows.Forms.Label label78;
        private System.Windows.Forms.Label label80;
        private System.Windows.Forms.Label label83;
        private System.Windows.Forms.Label label82;
        private System.Windows.Forms.Label label81;
        private System.Windows.Forms.Label label84;
        private System.Windows.Forms.Label label87;
        private System.Windows.Forms.Label label86;
        private System.Windows.Forms.Label label85;
        private System.Windows.Forms.Label label88;
        private System.Windows.Forms.Label label91;
        private System.Windows.Forms.Label label90;
        private System.Windows.Forms.Label label89;
        private System.Windows.Forms.Label label94;
        private System.Windows.Forms.Label label93;
        private System.Windows.Forms.Label label92;
        private System.Windows.Forms.Label label96;
        private System.Windows.Forms.Label label95;
        private System.Windows.Forms.Label label97;
        private System.Windows.Forms.Label LblAvgDate;
        private System.Windows.Forms.Button BtnUpdatePrice;
    }
}