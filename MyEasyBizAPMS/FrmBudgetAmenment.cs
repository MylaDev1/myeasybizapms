﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MyEasyBizAPMS
{
    public partial class FrmBudgetAmenment : Form
    {
        public FrmBudgetAmenment()
        {
            InitializeComponent();
            this.SfDataGridBudget.Style.HeaderStyle.Font.FontStyle = FontStyle.Bold;
            this.SfDataGridBudget.Style.HeaderStyle.BackColor = Color.DeepSkyBlue;
            this.tabControlAmentment.TabStyle = typeof(Syncfusion.Windows.Forms.Tools.OneNoteStyleRenderer);
        }
        int Fillid = 0; int SelectId = 0;
        private int LoadId = 0;int BudgetUid = 0;
        SqlConnection sqlconnection = new SqlConnection(GeneralParameters.ConnectionString);
        SQLDBHelper db = new SQLDBHelper();
        BindingSource bsDocNo = new BindingSource();
        BindingSource bsClass = new BindingSource();
        BindingSource bsItem = new BindingSource();
        DataSet dsBudget = new DataSet();

        private void FrmBudgetAmenment_Load(object sender, EventArgs e)
        {
            grFront.Visible = true;
            grBack.Visible = false;
            CmbProcess.Items.Clear();
            tabControlAmentment.TabPages.Remove(this.tabPageYarn);
            tabControlAmentment.TabPages.Remove(this.tabPageFabric);
            tabControlAmentment.TabPages.Remove(this.tabPageTrimsPurchase);
            tabControlAmentment.TabPages.Remove(this.tabPageTrimsProcess);
            tabControlAmentment.TabPages.Remove(this.tabPageGatmentProcess);
            tabControlAmentment.TabPages.Remove(this.tabPageCMTProcess);
            tabControlAmentment.TabPages.Remove(this.tabPageOtehrExp);
            if (this.Text == "Yarn Amendment")
            {
                CmbProcess.Items.Add("Yarn Amendment");
                tabControlAmentment.TabPages.Insert(0, this.tabPageYarn);
            }
            else if (this.Text == "Fabric Amendment")
            {
                CmbProcess.Items.Add("Fabric Amendment");
                tabControlAmentment.TabPages.Insert(0, this.tabPageFabric);
            }
            else if (this.Text == "Trims Purchase Amendment")
            {
                CmbProcess.Items.Add("Trims Purchase Amendment");
                tabControlAmentment.TabPages.Insert(0, this.tabPageTrimsPurchase);
            }
            else if (this.Text == "Trims Process Amendment")
            {
                CmbProcess.Items.Add("Trims Process Amendment");
                tabControlAmentment.TabPages.Insert(0, this.tabPageTrimsProcess);
            }
            else if (this.Text == "Garment Process Amendment")
            {
                CmbProcess.Items.Add("Garment Process Amendment");
                tabControlAmentment.TabPages.Insert(0, this.tabPageGatmentProcess);
            }
            else if (this.Text == "CMT Process Amendment")
            {
                CmbProcess.Items.Add("CMT Process Amendment");
                tabControlAmentment.TabPages.Insert(0, this.tabPageCMTProcess);
            }
            else if (this.Text == "Other Expenses Amendment")
            {
                CmbProcess.Items.Add("Other Expenses Amendment");
                tabControlAmentment.TabPages.Insert(0, this.tabPageOtehrExp);
            }
            LoadYarnGrid();
            LoadFabricProcess();
            LoadTrims();
            LoadTrimsProcess();
            LoadGarmentProcess();
            LoadCMT();
            LoadOtherExpenses();
        }

        private void TxtOrderNo_Click(object sender, EventArgs e)
        {
            try
            {
                string Query = @"select a.Uid,a.OrdermUid,b.OrderNo,c.StyleName from OrderMBudjet a 
                                inner join OrderM b on a.OrdermUid = b.Uid
                                inner join OrderMStyles c on a.OrdermUid = c.OrrderMUid
                                Where a.Sts = 'Approved' Order by b.DocNo desc";
                DataTable dataTable = db.GetDataWithoutParam(CommandType.Text, Query, sqlconnection);
                bsDocNo.DataSource = dataTable;
                FillGrid(dataTable, 1);
                Point loc = Genclass.FindLocation(txtOrderNo);
                grSearch.Location = new Point(loc.X - 10, loc.Y + 20);
                grSearch.Visible = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        private void FillGrid(DataTable dataTable, int FillId)
        {
            try
            {
                DataGridCommon.DataSource = null;
                DataGridCommon.AutoGenerateColumns = false;
                if (FillId == 1)
                {
                    Fillid = 1;
                    DataGridCommon.ColumnCount = 4;
                    DataGridCommon.Columns[0].Name = "Uid";
                    DataGridCommon.Columns[0].HeaderText = "Uid";
                    DataGridCommon.Columns[0].DataPropertyName = "OrdermUid";
                    DataGridCommon.Columns[0].Visible = false;
                    DataGridCommon.Columns[0].Width = 0;
                    DataGridCommon.Columns[1].Name = "OrderNo";
                    DataGridCommon.Columns[1].HeaderText = "OrderNo";
                    DataGridCommon.Columns[1].DataPropertyName = "OrderNo";
                    DataGridCommon.Columns[1].Width = 100;
                    DataGridCommon.Columns[2].Name = "StyleDesc";
                    DataGridCommon.Columns[2].HeaderText = "Style";
                    DataGridCommon.Columns[2].DataPropertyName = "StyleName";
                    DataGridCommon.Columns[2].Width = 210;
                    DataGridCommon.Columns[3].Name = "Uid";
                    DataGridCommon.Columns[3].HeaderText = "Uid";
                    DataGridCommon.Columns[3].DataPropertyName = "Uid";
                    DataGridCommon.Columns[3].Visible = false;
                    DataGridCommon.DataSource = bsDocNo;
                }
                if (FillId == 2)
                {
                    Fillid = 2;
                    DataGridCommon.ColumnCount = 2;
                    DataGridCommon.Columns[0].Name = "Uid";
                    DataGridCommon.Columns[0].HeaderText = "Uid";
                    DataGridCommon.Columns[0].DataPropertyName = "Guid";
                    DataGridCommon.Columns[0].Visible = false;
                    DataGridCommon.Columns[0].Width = 0;
                    DataGridCommon.Columns[1].Name = "Category";
                    DataGridCommon.Columns[1].HeaderText = "Category";
                    DataGridCommon.Columns[1].DataPropertyName = "GeneralName";
                    DataGridCommon.Columns[1].Width = 220;
                    DataGridCommon.DataSource = bsClass;
                }
                if (FillId == 3)
                {
                    Fillid = 3;
                    DataGridCommon.ColumnCount = 2;
                    DataGridCommon.Columns[0].Name = "Uid";
                    DataGridCommon.Columns[0].HeaderText = "Uid";
                    DataGridCommon.Columns[0].DataPropertyName = "UID";
                    DataGridCommon.Columns[0].Visible = false;
                    DataGridCommon.Columns[0].Width = 0;
                    DataGridCommon.Columns[1].Name = "Category";
                    DataGridCommon.Columns[1].HeaderText = "Category";
                    DataGridCommon.Columns[1].DataPropertyName = "ItemName";
                    DataGridCommon.Columns[1].Width = 320;
                    DataGridCommon.DataSource = bsItem;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void SplitAdd_Click(object sender, EventArgs e)
        {
            try
            {
                grFront.Visible = false;
                grBack.Visible = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        private void SplitAdd_DropDowItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            try
            {
                if(e.ClickedItem.Text == "Close")
                {
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        private void SplitSave_DropDowItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            try
            {
                if(e.ClickedItem.Text == "Back")
                {
                    grFront.Visible = true;
                    grBack.Visible = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        private void BtnSelect_Click(object sender, EventArgs e)
        {
            try
            {
                SelectId = 1;
                int Index = DataGridCommon.SelectedCells[0].RowIndex;
                if (Fillid == 1)
                {
                    txtOrderNo.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtOrderNo.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    BudgetUid = Convert.ToInt32(DataGridCommon.Rows[Index].Cells[3].Value.ToString());
                    SqlParameter[] sqlParameters = { new SqlParameter("@OrderMUid", txtOrderNo.Tag), new SqlParameter("@OrderMBudjetUid", BudgetUid) };
                    dsBudget = db.GetDataWithParamMultiple(CommandType.StoredProcedure, "Proc_OrderMBudjetEdit", sqlParameters, sqlconnection);
                    LoadGridsOnEdit(dsBudget);
                }
                else if (Fillid == 2)
                {
                    txtCtgy.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtCtgy.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                }
                else if (Fillid == 3)
                {
                    txtItemName.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtItemName.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    //GetAttribute(Convert.ToDecimal(txtItemName.Tag));
                }
                grSearch.Visible = false;
                SelectId = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void DataGridCommon_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                BtnSelect_Click(sender, e);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void CmbType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (this.Text == "Trims Purchase Amendment")
                {
                    if (CmbType.Text == "New Item")
                    {
                        txtCtgy.Visible = true;
                        txtItemName.Visible = true;
                        lblCtgy.Visible = true;
                        lblItemName.Visible = true;
                    }
                    else if (CmbType.Text == "Modify Item")
                    {
                        txtCtgy.Visible = false;
                        txtItemName.Visible = true;
                        lblCtgy.Visible = false;
                        lblItemName.Visible = true;
                    }
                    else if (CmbType.Text == "Alternate Item")
                    {
                        txtCtgy.Visible = false;
                        txtItemName.Visible = true;
                        lblCtgy.Visible = false;
                        lblItemName.Visible = true;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected void LoadYarnGrid()
        {
            try
            {
                DataGridYan.DataSource = null;
                DataGridYan.AutoGenerateColumns = false;
                DataGridYan.ColumnCount = 9;
                DataGridYan.Columns[0].Name = "SlNo";
                DataGridYan.Columns[0].HeaderText = "SlNo";
                DataGridYan.Columns[0].Width = 50;
                
                DataGridYan.Columns[1].Name = "YarnName";
                DataGridYan.Columns[1].HeaderText = "YarnName";
                DataGridYan.Columns[1].Width = 250;
                
                DataGridYan.Columns[2].Name = "Qty";
                DataGridYan.Columns[2].HeaderText = "Qty";
                DataGridYan.Columns[2].Width = 150;
                
                DataGridYan.Columns[3].Name = "Rate";
                DataGridYan.Columns[3].HeaderText = "Rate";
                DataGridYan.Columns[3].Width = 150;
                
                DataGridYan.Columns[4].Name = "Total";
                DataGridYan.Columns[4].HeaderText = "Total";
                DataGridYan.Columns[4].Width = 150;
                
                DataGridYan.Columns[5].Name = "YarnId";
                DataGridYan.Columns[5].HeaderText = "YarnId";
                DataGridYan.Columns[5].Visible = false;
                
                DataGridYan.Columns[6].Name = "FabricUid";
                DataGridYan.Columns[6].HeaderText = "FabricUid";
                DataGridYan.Columns[6].Visible = false;
                
                DataGridYan.Columns[7].Name = "Type";
                DataGridYan.Columns[7].HeaderText = "Type";
                DataGridYan.Columns[7].Visible = false;
                
                DataGridYan.Columns[8].Name = "BYarnUid";
                DataGridYan.Columns[8].HeaderText = "BYarnUid";
                DataGridYan.Columns[8].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected void LoadFabricProcess()
        {
            try
            {
                DataGridFabric.DataSource = null;
                DataGridFabric.AutoGenerateColumns = false;
                DataGridFabric.ColumnCount = 10;
                DataGridFabric.Columns[0].Name = "SlNo";
                DataGridFabric.Columns[0].HeaderText = "SlNo";
                DataGridFabric.Columns[0].Width = 50;
                
                DataGridFabric.Columns[1].Name = "Fabric";
                DataGridFabric.Columns[1].HeaderText = "Fabric";
                DataGridFabric.Columns[1].Width = 420;
                
                DataGridFabric.Columns[2].Name = "Colour";
                DataGridFabric.Columns[2].HeaderText = "Colour";
                DataGridFabric.Columns[2].Width = 150;
                
                DataGridFabric.Columns[3].Name = "Process";
                DataGridFabric.Columns[3].HeaderText = "Process";
                DataGridFabric.Columns[3].Width = 150;
                
                DataGridFabric.Columns[4].Name = "Qty";
                DataGridFabric.Columns[4].HeaderText = "Qty";
                DataGridFabric.Columns[4].Width = 80;
                DataGridFabric.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                
                DataGridFabric.Columns[5].Name = "Rate";
                DataGridFabric.Columns[5].HeaderText = "Rate";
                DataGridFabric.Columns[5].Width = 80;
                DataGridFabric.Columns[5].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                
                DataGridFabric.Columns[6].Name = "Total";
                DataGridFabric.Columns[6].HeaderText = "Total";
                DataGridFabric.Columns[6].Width = 80;
                DataGridFabric.Columns[6].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                
                DataGridFabric.Columns[7].Name = "FabricUid";
                DataGridFabric.Columns[7].HeaderText = "FabricUid";
                DataGridFabric.Columns[7].Visible = false;
                
                DataGridFabric.Columns[8].Name = "Type";
                DataGridFabric.Columns[8].HeaderText = "Type";
                DataGridFabric.Columns[8].Visible = false;
                
                DataGridFabric.Columns[9].Name = "FBUid";
                DataGridFabric.Columns[9].HeaderText = "FBUid";
                DataGridFabric.Columns[9].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected void LoadTrims()
        {
            try
            {
                DataGridTrims.DataSource = null;
                DataGridTrims.AutoGenerateColumns = false;
                DataGridTrims.ColumnCount = 7;
                DataGridTrims.Columns[0].Name = "SlNo";
                DataGridTrims.Columns[0].HeaderText = "SlNo";
                DataGridTrims.Columns[0].Width = 50;

                DataGridTrims.Columns[1].Name = "TrimsName";
                DataGridTrims.Columns[1].HeaderText = "TrimsName";
                DataGridTrims.Columns[1].Width = 350;

                DataGridTrims.Columns[2].Name = "Qty";
                DataGridTrims.Columns[2].HeaderText = "Qty";
                DataGridTrims.Columns[2].Width = 100;
                DataGridTrims.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

                DataGridTrims.Columns[3].Name = "Rate";
                DataGridTrims.Columns[3].HeaderText = "Rate";
                DataGridTrims.Columns[3].Width = 100;
                DataGridTrims.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

                DataGridTrims.Columns[4].Name = "Total";
                DataGridTrims.Columns[4].HeaderText = "Total";
                DataGridTrims.Columns[4].Width = 100;
                DataGridTrims.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

                DataGridTrims.Columns[5].Name = "Uid";
                DataGridTrims.Columns[5].HeaderText = "Uid";
                DataGridTrims.Columns[5].Visible = false;

                DataGridTrims.Columns[6].Name = "TUid";
                DataGridTrims.Columns[6].HeaderText = "TUid";
                DataGridTrims.Columns[6].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected void LoadTrimsProcess()
        {
            try
            {
                DataGridTrimsProcess.DataSource = null;
                DataGridTrimsProcess.AutoGenerateColumns = false;
                DataGridTrimsProcess.ColumnCount = 9;
                DataGridTrimsProcess.Columns[0].Name = "SlNo";
                DataGridTrimsProcess.Columns[0].HeaderText = "SlNo";
                DataGridTrimsProcess.Columns[0].Width = 50;

                DataGridTrimsProcess.Columns[1].Name = "ItemName";
                DataGridTrimsProcess.Columns[1].HeaderText = "ItemName";
                DataGridTrimsProcess.Columns[1].Width = 350;

                DataGridTrimsProcess.Columns[2].Name = "Process";
                DataGridTrimsProcess.Columns[2].HeaderText = "Process";
                DataGridTrimsProcess.Columns[2].Width = 150;

                DataGridTrimsProcess.Columns[3].Name = "Qty";
                DataGridTrimsProcess.Columns[3].HeaderText = "Qty";
                DataGridTrimsProcess.Columns[3].Width = 100;
                DataGridTrimsProcess.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

                DataGridTrimsProcess.Columns[4].Name = "Rate";
                DataGridTrimsProcess.Columns[4].HeaderText = "Rate";
                DataGridTrimsProcess.Columns[4].Width = 100;
                DataGridTrimsProcess.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

                DataGridTrimsProcess.Columns[5].Name = "Total";
                DataGridTrimsProcess.Columns[5].HeaderText = "Total";
                DataGridTrimsProcess.Columns[5].Width = 100;
                DataGridTrimsProcess.Columns[5].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

                DataGridTrimsProcess.Columns[6].Name = "TrimsUid";
                DataGridTrimsProcess.Columns[6].HeaderText = "Uid";
                DataGridTrimsProcess.Columns[6].Visible = false;

                DataGridTrimsProcess.Columns[7].Name = "ProcessUid";
                DataGridTrimsProcess.Columns[7].HeaderText = "ProcessUid";
                DataGridTrimsProcess.Columns[7].Visible = false;

                DataGridTrimsProcess.Columns[8].Name = "TUid";
                DataGridTrimsProcess.Columns[8].HeaderText = "TUid";
                DataGridTrimsProcess.Columns[8].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected void LoadGarmentProcess()
        {
            try
            {
                DataGridGarmentProcess.DataSource = null;
                DataGridGarmentProcess.AutoGenerateColumns = false;
                DataGridGarmentProcess.ColumnCount = 9;
                DataGridGarmentProcess.Columns[0].Name = "SlNo";
                DataGridGarmentProcess.Columns[0].HeaderText = "SlNo";
                DataGridGarmentProcess.Columns[0].Width = 50;

                DataGridGarmentProcess.Columns[1].Name = "Component";
                DataGridGarmentProcess.Columns[1].HeaderText = "Component";
                DataGridGarmentProcess.Columns[1].Width = 350;

                DataGridGarmentProcess.Columns[2].Name = "Process";
                DataGridGarmentProcess.Columns[2].HeaderText = "Process";
                DataGridGarmentProcess.Columns[2].Width = 150;

                DataGridGarmentProcess.Columns[3].Name = "Qty";
                DataGridGarmentProcess.Columns[3].HeaderText = "Qty";
                DataGridGarmentProcess.Columns[3].Width = 100;
                DataGridGarmentProcess.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

                DataGridGarmentProcess.Columns[4].Name = "Rate";
                DataGridGarmentProcess.Columns[4].HeaderText = "Rate";
                DataGridGarmentProcess.Columns[4].Width = 100;
                DataGridGarmentProcess.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

                DataGridGarmentProcess.Columns[5].Name = "Total";
                DataGridGarmentProcess.Columns[5].HeaderText = "Total";
                DataGridGarmentProcess.Columns[5].Width = 100;
                DataGridGarmentProcess.Columns[5].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

                DataGridGarmentProcess.Columns[6].Name = "ComponentUid";
                DataGridGarmentProcess.Columns[6].HeaderText = "ComponentUid";
                DataGridGarmentProcess.Columns[6].Visible = false;

                DataGridGarmentProcess.Columns[7].Name = "ProcessUid";
                DataGridGarmentProcess.Columns[7].HeaderText = "ProcessUid";
                DataGridGarmentProcess.Columns[7].Visible = false;

                DataGridGarmentProcess.Columns[8].HeaderText = "TUid";
                DataGridGarmentProcess.Columns[8].Name = "TUid";
                DataGridGarmentProcess.Columns[8].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected void LoadCMT()
        {
            try
            {
                DatGridCMTProcess.DataSource = null;
                DatGridCMTProcess.AutoGenerateColumns = false;
                DatGridCMTProcess.ColumnCount = 9;
                DatGridCMTProcess.Columns[0].Name = "SlNo";
                DatGridCMTProcess.Columns[0].HeaderText = "SlNo";
                DatGridCMTProcess.Columns[0].Width = 50;
                
                DatGridCMTProcess.Columns[1].Name = "CO-ORDINATES";
                DatGridCMTProcess.Columns[1].HeaderText = "CO-ORDINATES";
                DatGridCMTProcess.Columns[1].Width = 200;
                
                DatGridCMTProcess.Columns[2].Name = "Operations";
                DatGridCMTProcess.Columns[2].HeaderText = "Operations";
                DatGridCMTProcess.Columns[2].Width = 150;
                
                DatGridCMTProcess.Columns[3].Name = "Qty";
                DatGridCMTProcess.Columns[3].HeaderText = "Qty";
                DatGridCMTProcess.Columns[3].Width = 100;
                DatGridCMTProcess.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                
                DatGridCMTProcess.Columns[4].Name = "Rate";
                DatGridCMTProcess.Columns[4].HeaderText = "Rate";
                DatGridCMTProcess.Columns[4].Width = 100;
                DatGridCMTProcess.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                
                DatGridCMTProcess.Columns[5].Name = "Total";
                DatGridCMTProcess.Columns[5].HeaderText = "Total";
                DatGridCMTProcess.Columns[5].Width = 100;
                DatGridCMTProcess.Columns[5].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                
                DatGridCMTProcess.Columns[6].Name = "CUid";
                DatGridCMTProcess.Columns[6].HeaderText = "CUid";
                DatGridCMTProcess.Columns[6].Visible = false;
                
                DatGridCMTProcess.Columns[7].Name = "OUid";
                DatGridCMTProcess.Columns[7].HeaderText = "OUid";
                DatGridCMTProcess.Columns[7].Visible = false;
                
                DatGridCMTProcess.Columns[8].HeaderText = "TUid";
                DatGridCMTProcess.Columns[8].Name = "TUid";
                DatGridCMTProcess.Columns[8].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected void LoadOtherExpenses()
        {
            try
            {
                DatGridOtherExp.DataSource = null;
                DatGridOtherExp.AutoGenerateColumns = false;
                DatGridOtherExp.ColumnCount = 8;
                DatGridOtherExp.Columns[0].Name = "SlNo";
                DatGridOtherExp.Columns[0].HeaderText = "SlNo";
                DatGridOtherExp.Columns[0].Width = 50;
                
                DatGridOtherExp.Columns[1].Name = "Name";
                DatGridOtherExp.Columns[1].HeaderText = "Name";
                DatGridOtherExp.Columns[1].Width = 150;
                
                DatGridOtherExp.Columns[2].Name = "Type";
                DatGridOtherExp.Columns[2].HeaderText = "Type";
                DatGridOtherExp.Columns[2].Width = 100;
                
                DatGridOtherExp.Columns[3].Name = "Qty/Value";
                DatGridOtherExp.Columns[3].HeaderText = "Qty/Value";
                DatGridOtherExp.Columns[3].Width = 100;
                
                DatGridOtherExp.Columns[4].Name = "Rate/Percentage";
                DatGridOtherExp.Columns[4].HeaderText = "Rate/Percentage";
                DatGridOtherExp.Columns[4].Width = 100;
                
                DatGridOtherExp.Columns[5].Name = "Total";
                DatGridOtherExp.Columns[5].HeaderText = "Total";
                DatGridOtherExp.Columns[5].Width = 100;
                
                DatGridOtherExp.Columns[6].Name = "Guid";
                DatGridOtherExp.Columns[6].HeaderText = "Guid";
                DatGridOtherExp.Columns[6].Visible = false;
                
                DatGridOtherExp.Columns[7].Name = "Ouid";
                DatGridOtherExp.Columns[7].HeaderText = "Ouid";
                DatGridOtherExp.Columns[7].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected void LoadGridsOnEdit(DataSet dataSet)
        {
            try
            {
                //loadid = 1;
                DataTable dtYarn = dataSet.Tables[0];
                DataTable dtFabric = dataSet.Tables[1];
                DataTable dtTrims = dataSet.Tables[2];
                DataTable dtAccounts = dataSet.Tables[3];
                DataTable dtTrimsProcess = dataSet.Tables[4];
                DataTable dtGarmentProcess = dataSet.Tables[5];
                DataTable dtCMTProcess = dataSet.Tables[6];

                if (dtYarn.Rows.Count > 0)
                {
                    DataTable dtProcessYarn = dtYarn;
                    for (int i = 0; i < dtProcessYarn.Rows.Count; i++)
                    {
                        int IndexPr = DataGridYan.Rows.Add();
                        DataGridViewRow dataGrid = DataGridYan.Rows[IndexPr];
                        dataGrid.Cells[0].Value = i + 1;
                        dataGrid.Cells[1].Value = dtProcessYarn.Rows[i]["Yarnname"].ToString();
                        dataGrid.Cells[2].Value = dtProcessYarn.Rows[i]["Qty"].ToString();
                        dataGrid.Cells[3].Value = dtProcessYarn.Rows[i]["Rate"].ToString();
                        dataGrid.Cells[4].Value = dtProcessYarn.Rows[i]["Total"].ToString();
                        dataGrid.Cells[5].Value = dtProcessYarn.Rows[i]["YarnUid"].ToString();
                        dataGrid.Cells[6].Value = "0";
                        dataGrid.Cells[7].Value = dtProcessYarn.Rows[i]["YarnTag"].ToString();
                        dataGrid.Cells[8].Value = dtProcessYarn.Rows[i]["Uid"].ToString(); ;
                    }
                }
                
                if (dtFabric.Rows.Count > 0)
                {
                    DataTable dtFabricProcess = dtFabric;
                    for (int i = 0; i < dtFabricProcess.Rows.Count; i++)
                    {
                        int IndexFp = DataGridFabric.Rows.Add();
                        DataGridViewRow dataGridViewRowPr = DataGridFabric.Rows[IndexFp];
                        dataGridViewRowPr.Cells[0].Value = i + 1;
                        dataGridViewRowPr.Cells[1].Value = dtFabricProcess.Rows[i]["FabricName"].ToString();
                        dataGridViewRowPr.Cells[2].Value = dtFabricProcess.Rows[i]["Colour"].ToString();
                        dataGridViewRowPr.Cells[3].Value = dtFabricProcess.Rows[i]["Process"].ToString();
                        dataGridViewRowPr.Cells[4].Value = dtFabricProcess.Rows[i]["Qty"].ToString();
                        dataGridViewRowPr.Cells[5].Value = dtFabricProcess.Rows[i]["Rate"].ToString();
                        dataGridViewRowPr.Cells[6].Value = dtFabricProcess.Rows[i]["Total"].ToString();
                        dataGridViewRowPr.Cells[7].Value = dtFabricProcess.Rows[i]["FabricUid"].ToString();
                        dataGridViewRowPr.Cells[8].Value = dtFabricProcess.Rows[i]["FabTag"].ToString();
                        dataGridViewRowPr.Cells[9].Value = dtFabricProcess.Rows[i]["Uid"].ToString();
                    }
                }

                for (int i = 0; i < dtTrims.Rows.Count; i++)
                {
                    int IndexTr = DataGridTrims.Rows.Add();
                    DataGridViewRow dataGridViewRowT = DataGridTrims.Rows[IndexTr];
                    dataGridViewRowT.Cells[0].Value = i + 1;
                    dataGridViewRowT.Cells[1].Value = dtTrims.Rows[i]["TrimsName"].ToString();
                    dataGridViewRowT.Cells[2].Value = dtTrims.Rows[i]["Qty"].ToString();
                    dataGridViewRowT.Cells[3].Value = dtTrims.Rows[i]["Rate"].ToString();
                    dataGridViewRowT.Cells[4].Value = dtTrims.Rows[i]["Total"].ToString();
                    dataGridViewRowT.Cells[5].Value = dtTrims.Rows[i]["TrimsUid"].ToString();
                    dataGridViewRowT.Cells[6].Value = dtTrims.Rows[i]["Uid"].ToString();
                }

                for (int i = 0; i < dtAccounts.Rows.Count; i++)
                {
                    int Index = DatGridOtherExp.Rows.Add();
                    DataGridViewRow dataGridViewRow = DatGridOtherExp.Rows[Index];
                    dataGridViewRow.Cells[0].Value = i + 1;
                    dataGridViewRow.Cells[1].Value = dtAccounts.Rows[i]["Name"].ToString();
                    dataGridViewRow.Cells[2].Value = dtAccounts.Rows[i]["Exptype"].ToString();
                    dataGridViewRow.Cells[3].Value = dtAccounts.Rows[i]["Qty"].ToString();
                    dataGridViewRow.Cells[4].Value = dtAccounts.Rows[i]["rate"].ToString();
                    dataGridViewRow.Cells[5].Value = dtAccounts.Rows[i]["TotalValue"].ToString();
                    dataGridViewRow.Cells[6].Value = dtAccounts.Rows[i]["ExpenseUid"].ToString();
                    dataGridViewRow.Cells[7].Value = dtAccounts.Rows[i]["Uid"].ToString();
                }

                for (int i = 0; i < dtTrimsProcess.Rows.Count; i++)
                {
                    int Index = DataGridTrimsProcess.Rows.Add();
                    DataGridViewRow dataGridViewRow = DataGridTrimsProcess.Rows[Index];
                    dataGridViewRow.Cells[0].Value = i + 1;
                    dataGridViewRow.Cells[1].Value = dtTrimsProcess.Rows[i]["TrimsName"].ToString();
                    dataGridViewRow.Cells[2].Value = dtTrimsProcess.Rows[i]["Process"].ToString();
                    dataGridViewRow.Cells[3].Value = dtTrimsProcess.Rows[i]["Qty"].ToString();
                    dataGridViewRow.Cells[4].Value = dtTrimsProcess.Rows[i]["rate"].ToString();
                    dataGridViewRow.Cells[5].Value = dtTrimsProcess.Rows[i]["Total"].ToString();
                    dataGridViewRow.Cells[6].Value = dtTrimsProcess.Rows[i]["TrimsUid"].ToString();
                    dataGridViewRow.Cells[7].Value = dtTrimsProcess.Rows[i]["ProcessUid"].ToString();
                    dataGridViewRow.Cells[8].Value = dtTrimsProcess.Rows[i]["Uid"].ToString();
                }

                for (int i = 0; i < dtGarmentProcess.Rows.Count; i++)
                {
                    int Index = DataGridGarmentProcess.Rows.Add();
                    DataGridViewRow dataGridViewRow = DataGridGarmentProcess.Rows[Index];
                    dataGridViewRow.Cells[0].Value = i + 1;
                    dataGridViewRow.Cells[1].Value = dtGarmentProcess.Rows[i]["Component"].ToString();
                    dataGridViewRow.Cells[2].Value = dtGarmentProcess.Rows[i]["Process"].ToString();
                    dataGridViewRow.Cells[3].Value = dtGarmentProcess.Rows[i]["Qty"].ToString();
                    dataGridViewRow.Cells[4].Value = dtGarmentProcess.Rows[i]["rate"].ToString();
                    dataGridViewRow.Cells[5].Value = dtGarmentProcess.Rows[i]["Total"].ToString();
                    dataGridViewRow.Cells[6].Value = dtGarmentProcess.Rows[i]["ComponentUid"].ToString();
                    dataGridViewRow.Cells[7].Value = dtGarmentProcess.Rows[i]["ProcessUid"].ToString();
                    dataGridViewRow.Cells[8].Value = dtGarmentProcess.Rows[i]["Uid"].ToString();
                }

                for (int i = 0; i < dtCMTProcess.Rows.Count; i++)
                {
                    int Index = DatGridCMTProcess.Rows.Add();
                    DataGridViewRow dataGridViewRow = DatGridCMTProcess.Rows[Index];
                    dataGridViewRow.Cells[0].Value = i + 1;
                    dataGridViewRow.Cells[1].Value = dtCMTProcess.Rows[i]["Coordinates"].ToString();
                    dataGridViewRow.Cells[2].Value = dtCMTProcess.Rows[i]["Operation"].ToString();
                    dataGridViewRow.Cells[3].Value = dtCMTProcess.Rows[i]["Qty"].ToString();
                    dataGridViewRow.Cells[4].Value = dtCMTProcess.Rows[i]["rate"].ToString();
                    dataGridViewRow.Cells[5].Value = dtCMTProcess.Rows[i]["Total"].ToString();
                    dataGridViewRow.Cells[6].Value = dtCMTProcess.Rows[i]["COOrdinatesUid"].ToString();
                    dataGridViewRow.Cells[7].Value = dtCMTProcess.Rows[i]["OperationUid"].ToString();
                    dataGridViewRow.Cells[8].Value = dtCMTProcess.Rows[i]["Uid"].ToString();
                }
                //loadid = 0;
            }
            catch (Exception)
            {

            }
        }

        private void BtnHide_Click(object sender, EventArgs e)
        {
            grSearch.Visible = false;
        }

        public DataTable LoadGeneralM()
        {
            DataTable dt = new DataTable();
            try
            {
                dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "SP_GetGeneralsTrims", sqlconnection);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            return dt;
        }

        private void TxtCtgy_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dt = LoadGeneralM();
                DataTable dtclass = dt.Select("TypeMUid = 13").CopyToDataTable();
                bsClass.DataSource = dtclass;
                FillGrid(dtclass, 2);
                Point loc = Genclass.FindLocation(txtCtgy);
                grSearch.Location = new Point(loc.X - 10, loc.Y + 10);
                grSearch.Visible = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void TxtCtgy_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bsClass.Filter = string.Format("GeneralName LIKE '%{0}%'", txtCtgy.Text);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void TxtItemName_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtCtgy.Text != string.Empty)
                {
                    string Query = "select UID,ItemName from TrimsM Where CategoryUid  =" + txtCtgy.Tag + "";
                    DataTable dt = db.GetDataWithoutParam(CommandType.Text, Query, sqlconnection);
                    bsItem.DataSource = dt;
                    FillGrid(dt, 3);
                    Point loc = Genclass.FindLocation(txtItemName);
                    grSearch.Location = new Point(loc.X - 10, loc.Y + 20);
                    grSearch.Visible = true;
                }
                else
                {
                    MessageBox.Show("Select Catgory", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtCtgy.Focus();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtItemName_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bsItem.Filter = string.Format("ItemName LIKE '%{0}%'", txtItemName.Text);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            } 
        }

        private void DataGridTrims_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                int Index = DataGridTrims.SelectedCells[0].RowIndex;
                //txtCtgy.Text = DataGridTrims.Rows[Index].Cells[]


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
    }
}
