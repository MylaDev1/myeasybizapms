﻿using Syncfusion.Windows.Forms.Tools;
using Syncfusion.WinForms.DataGridConverter;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace MyEasyBizAPMS
{
    public partial class FrmOrderEntry : Form
    {
        public FrmOrderEntry()
        {
            InitializeComponent();
            this.tabControlOrderEntry.TabStyle = typeof(Syncfusion.Windows.Forms.Tools.OneNoteStyleRenderer);
            this.SfdDataGridOrderEntry.Style.HeaderStyle.Font.FontStyle = FontStyle.Bold;
            this.SfdDataGridOrderEntry.Style.HeaderStyle.BackColor = Color.DeepSkyBlue;
        }

        int Fillid = 0; int LoadId = 0; int clearid = 0;
        int SelectId = 0; decimal EditStyleId = 0; int StyleEdit = 0;
        int EditId = 0;
        DataTable dtOrdersGrid = new DataTable();
        SQLDBHelper db = new SQLDBHelper();
        SqlConnection conn = new SqlConnection(GeneralParameters.ConnectionString);
        BindingSource bsCustomer = new BindingSource();
        BindingSource bsMerchindiser = new BindingSource();
        BindingSource bsDept = new BindingSource();
        BindingSource bsAgent = new BindingSource();
        BindingSource bsShipType = new BindingSource();
        BindingSource bsShiftMode = new BindingSource();
        BindingSource bsPaymode = new BindingSource();
        BindingSource bsCountry = new BindingSource();
        BindingSource bsUOM = new BindingSource();
        BindingSource bsCombo = new BindingSource();
        BindingSource bsStrucType = new BindingSource();
        BindingSource bsCurrency = new BindingSource();
        BindingSource bsDocNo = new BindingSource();
        decimal OrderQty = 0;
        string OldSzeName = string.Empty;

        private void FrmOrderEntry_Load(object sender, EventArgs e)
        {
            StyleEdit = 0;
            if (this.Text == "Order Amendment")
            {
                btnAdd.Text = "Add New";
                toolstripitem toolStripItem = new toolstripitem
                {
                    Text = "Edit"
                };
                this.btnAdd.DropDownItems.Add(toolStripItem);

                toolstripitem toolStripItem1 = new toolstripitem
                {
                    Text = "Close"
                };
                this.btnAdd.DropDownItems.Add(toolStripItem1);
                btnSave.Text = "Update";

                toolstripitem toolStripSave = new toolstripitem
                {
                    Text = "Back"
                };
                this.btnSave.DropDownItems.Add(toolStripSave);
                btnPrint.Visible = false;
            }
            else
            {
                btnAdd.Text = "Add New";
                toolstripitem toolStripItem = new toolstripitem
                {
                    Text = "Edit"
                };
                this.btnAdd.DropDownItems.Add(toolStripItem);
                toolstripitem toolStripItem1 = new toolstripitem
                {
                    Text = "Close"
                };
                this.btnAdd.DropDownItems.Add(toolStripItem1);
                toolstripitem toolStripSave = new toolstripitem
                {
                    Text = "Back"
                };
                this.btnSave.DropDownItems.Add(toolStripSave);
            }
            LoadId = 1;

            grFront.Visible = true;

            cmbPriceType.SelectedIndex = 0;
            LoadOrders();
            LoaddataGridStyle();
            LoadDataGridStyleSize();
            LoadDataGridCharges();
            LoadDataGridDiscounts();
            LoadGeneralsOrderEntry();
            LoadCountry();
            LoadAssortGrid();
            LoadDataGridStyleCombo();
            LoadDataGridFabric();
            LoadUOmCombo();
            LoadDataGridPlanning();
            var LastYears = Enumerable
                   .Range(0, 4)
                   .Select(i => DateTime.Now.AddYears(i))
                   .Select(date => date.ToString("yyyy"));
            cmbYear.Items.Clear();
            cmbYear.DataSource = LastYears.ToList();
            tabControlOrderEntry.TabPages.Remove(this.tabPageSize);
            tabControlOrderEntry.TabPages.Remove(this.tabSizeMatrix);
            tabControlOrderEntry.TabPages.Remove(this.tabAssortment);
            tabControlOrderEntry.TabPages.Remove(this.tabCombo);
            tabControlOrderEntry.TabPages.Remove(this.tabPlanning);
            tabControlOrderEntry.TabPages.Remove(this.tabPrice);
            tabControlOrderEntry.TabPages.Remove(this.tabLogistics);
            tabControlOrderEntry.TabPages.Remove(this.tabFabric);
            grFront.Visible = true;
            grBack.Visible = false;
            LoadId = 0;


        }

        protected void LoadCountry()
        {
            try
            {
                DataTable dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "SP_GetGeneralsForOrderEntry", conn);
                DataTable dtCountry = dt.Select("TypeMUid = " + (int)OrderEntryGeneralM.Country + "").CopyToDataTable();
                cmbCountry.DataSource = null;
                cmbCountry.DisplayMember = "GeneralName";
                cmbCountry.ValueMember = "Guid";
                cmbCountry.DataSource = dtCountry;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void BtnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                if (btnAdd.Text == "Add New")
                {
                    decimal DocTyeId = 1;
                    txtDocNo.Tag = "0";
                    grFront.Visible = false;
                    grBack.Visible = true;
                    txtDocNo.Text = GeneralParameters.GetDocNo(DocTyeId, conn);
                    txtDocNo.Enabled = false;
                    btnSave.Text = "Save & Continue";
                }
                txtDocNo.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void LoadAssortGrid()
        {
            try
            {
                DataGridAssort.AutoGenerateColumns = false;
                DataGridAssort.ColumnCount = 11;
                DataGridAssort.Columns[0].Name = "Country";
                DataGridAssort.Columns[0].HeaderText = "Country";
                DataGridAssort.Columns[0].Width = 130;

                DataGridAssort.Columns[1].Name = "Ref No";
                DataGridAssort.Columns[1].HeaderText = "Ref No";
                DataGridAssort.Columns[1].Width = 130;

                DataGridAssort.Columns[2].Name = "AssortmentType";
                DataGridAssort.Columns[2].HeaderText = "AssortmentType";
                DataGridAssort.Columns[2].Width = 180;

                DataGridAssort.Columns[3].Name = "Quantity";
                DataGridAssort.Columns[3].HeaderText = "Quantity";

                DataGridAssort.Columns[4].Name = "Delivery Dt";
                DataGridAssort.Columns[4].HeaderText = "Delivery Dt";
                DataGridAssort.Columns[4].Width = 100;

                DataGridAssort.Columns[5].Name = "Shipment Dt";
                DataGridAssort.Columns[5].HeaderText = "Shipment Dt";
                DataGridAssort.Columns[5].Width = 100;

                DataGridAssort.Columns[6].Name = "Port of Delivery";
                DataGridAssort.Columns[6].HeaderText = "Port of Delivery";
                DataGridAssort.Columns[6].Width = 110;

                DataGridAssort.Columns[7].Name = "Final Destination";
                DataGridAssort.Columns[7].HeaderText = "Final Destination";
                DataGridAssort.Columns[7].Width = 160;

                DataGridAssort.Columns[8].Name = "CountryUid";
                DataGridAssort.Columns[8].HeaderText = "CountryUid";
                DataGridAssort.Columns[8].Visible = false;

                DataGridAssort.Columns[9].Name = "AssortUid";
                DataGridAssort.Columns[9].HeaderText = "AssortUid";
                DataGridAssort.Columns[9].Visible = false;

                DataGridAssort.Columns[10].Name = "CBox";
                DataGridAssort.Columns[10].HeaderText = "CBox";
                DataGridAssort.Columns[10].Width = 80;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void BtnSave_DropDowItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            try
            {
                if (e.ClickedItem.Text == "Back")
                {
                    grFront.Visible = true;
                    grBack.Visible = false;
                    ClearTextBoxes();
                    DataGridStyle.Rows.Clear();
                    clearid = 1;
                    tabControlOrderEntry.TabPages.Remove(this.tabPageSize);
                    tabControlOrderEntry.TabPages.Remove(this.tabSizeMatrix);
                    tabControlOrderEntry.TabPages.Remove(this.tabAssortment);
                    tabControlOrderEntry.TabPages.Remove(this.tabCombo);
                    tabControlOrderEntry.TabPages.Remove(this.tabPlanning);
                    tabControlOrderEntry.TabPages.Remove(this.tabPrice);
                    tabControlOrderEntry.TabPages.Remove(this.tabLogistics);
                    tabControlOrderEntry.TabPages.Remove(this.tabFabric);
                    clearid = 0;
                    LoadOrders();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected void LoaddataGridStyle()
        {
            try
            {
                DataGridStyle.AutoGenerateColumns = false;
                DataGridStyle.ColumnCount = 9;
                DataGridStyle.Columns[0].Name = "SlNo";
                DataGridStyle.Columns[0].HeaderText = "SlNo";
                DataGridStyle.Columns[0].Width = 50;

                DataGridStyle.Columns[1].Name = "Style No";
                DataGridStyle.Columns[1].HeaderText = "Style No";
                DataGridStyle.Columns[1].Width = 220;

                DataGridStyle.Columns[2].Name = "Style Descrisption";
                DataGridStyle.Columns[2].HeaderText = "Style Descrisption";
                DataGridStyle.Columns[2].Width = 380;

                DataGridStyle.Columns[3].Name = "Order Unit";
                DataGridStyle.Columns[3].HeaderText = "Order Unit";
                DataGridStyle.Columns[3].Width = 110;

                DataGridStyle.Columns[4].Name = "Plan Unit";
                DataGridStyle.Columns[4].HeaderText = "Plan Unit";
                DataGridStyle.Columns[4].Width = 110;

                DataGridStyle.Columns[5].Name = "Quantity";
                DataGridStyle.Columns[5].HeaderText = "Quantity";
                DataGridStyle.Columns[5].Width = 110;

                DataGridStyle.Columns[6].Name = "OrderUnitId";
                DataGridStyle.Columns[6].HeaderText = "OrderUnitId";
                DataGridStyle.Columns[6].Visible = false;

                DataGridStyle.Columns[7].Name = "Plan Unit";
                DataGridStyle.Columns[7].HeaderText = "Plan Unit";
                DataGridStyle.Columns[7].Visible = false;

                DataGridStyle.Columns[8].Name = "StyleUid";
                DataGridStyle.Columns[8].HeaderText = "StyleUid";
                DataGridStyle.Columns[8].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected void LoadDataGridStyleSize()
        {
            try
            {
                DataGridStyleSize.AutoGenerateColumns = false;
                DataGridStyleSize.ColumnCount = 2;
                DataGridStyleSize.Columns[0].Name = "Size";
                DataGridStyleSize.Columns[0].HeaderText = "Size";
                DataGridStyleSize.Columns[0].Width = 230;

                DataGridStyleSize.Columns[1].Name = "StyleUid";
                DataGridStyleSize.Columns[1].HeaderText = "StyleUid";
                DataGridStyleSize.Columns[1].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected void LoadDataGridStyleCombo()
        {
            try
            {
                DataGridStyleSizeCombo.AutoGenerateColumns = false;
                DataGridStyleSizeCombo.ColumnCount = 3;
                DataGridStyleSizeCombo.Columns[0].Name = "Combo";
                DataGridStyleSizeCombo.Columns[0].HeaderText = "Combo";
                DataGridStyleSizeCombo.Columns[0].Width = 290;

                DataGridStyleSizeCombo.Columns[1].Name = "ComboQty";
                DataGridStyleSizeCombo.Columns[1].HeaderText = "ComboQty";
                DataGridStyleSizeCombo.Columns[1].Visible = false;

                DataGridStyleSizeCombo.Columns[2].Name = "Uid";
                DataGridStyleSizeCombo.Columns[2].HeaderText = "Uid";
                DataGridStyleSizeCombo.Columns[2].Visible = false;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void CmbPriceType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                LoadId = 1;

                if (cmbPriceType.Text == "Style Wise")
                {
                    DataGridStylePrice.AutoGenerateColumns = false;
                    DataGridStylePrice.ColumnCount = 4;
                    DataGridStylePrice.Columns[0].Name = "Style No";
                    DataGridStylePrice.Columns[0].HeaderText = "Style No";
                    DataGridStylePrice.Columns[0].Width = 330;

                    DataGridStylePrice.Columns[1].Name = "Uom";
                    DataGridStylePrice.Columns[1].HeaderText = "Uom";
                    DataGridStylePrice.Columns[1].Width = 90;

                    DataGridStylePrice.Columns[2].Name = "Price";
                    DataGridStylePrice.Columns[2].HeaderText = "Price";
                    DataGridStylePrice.Columns[2].Width = 90;
                    DataGridStylePrice.Columns[2].DefaultCellStyle.Format = "N2";

                    DataGridStylePrice.Columns[3].Name = "Price";
                    DataGridStylePrice.Columns[3].HeaderText = "Price";
                    DataGridStylePrice.Columns[3].Visible = false;
                    if (txtDocNo.Tag != null)
                    {
                        DataGridStylePrice.Rows.Clear();
                        string Query1 = @"Select * from OrdermStyles a
                                        inner join GeneralM b on a.PlanUomUid = b.GUid  and TypeMUid = 7 
                                        Left join OrderMPrice c on a.OrrderMUid = c.OrderMuid and c.PriceType='Style Wise'
                                        Where a.OrrderMUid =" + txtDocNo.Tag + "";
                        DataTable dt = db.GetDataWithoutParam(CommandType.Text, Query1, conn);
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            DataGridViewRow row = (DataGridViewRow)DataGridStylePrice.Rows[0].Clone();
                            row.Cells[0].Value = dt.Rows[i]["StyleName"].ToString();
                            row.Cells[1].Value = dt.Rows[i]["GeneralName"].ToString();
                            row.Cells[2].Value = dt.Rows[i]["Price"].ToString();
                            row.Cells[3].Value = dt.Rows[i]["Uid"].ToString();
                            DataGridStylePrice.Rows.Add(row);
                        }
                    }
                }
                else if (cmbPriceType.Text == "Combo wise")
                {
                    DataGridStylePrice.AutoGenerateColumns = false;
                    DataGridStylePrice.ColumnCount = 4;
                    DataGridStylePrice.Columns[0].Name = "SlNo";
                    DataGridStylePrice.Columns[0].HeaderText = "SlNo";
                    DataGridStylePrice.Columns[0].Width = 90;

                    DataGridStylePrice.Columns[1].Name = "Combo";
                    DataGridStylePrice.Columns[1].HeaderText = "Combo";
                    DataGridStylePrice.Columns[1].Width = 330;

                    DataGridStylePrice.Columns[2].Name = "Price";
                    DataGridStylePrice.Columns[2].HeaderText = "Price";
                    DataGridStylePrice.Columns[2].Width = 90;
                    DataGridStylePrice.Columns[2].DefaultCellStyle.Format = "N2";

                    DataGridStylePrice.Columns[3].Name = "Uid";
                    DataGridStylePrice.Columns[3].HeaderText = "Uid";
                    DataGridStylePrice.Columns[3].Visible = false;

                    if (txtDocNo.Tag != null)
                    {
                        DataGridStylePrice.Rows.Clear();
                        string Query1 = @"Select * from StyleCombo a
                                        Left join OrderMPrice c on a.OrderMUid = c.OrderMuid and c.PriceType='Combo Wise' and a.Uid = c.StyleComboUid
                                        Where a.OrderMUid= " + txtDocNo.Tag + " and a.StyleUid =" + cmbStylePrice.SelectedValue + "";
                        DataTable dt1 = db.GetDataWithoutParam(CommandType.Text, Query1, conn);
                        for (int j = 0; j < dt1.Rows.Count; j++)
                        {
                            DataGridViewRow row = (DataGridViewRow)DataGridStylePrice.Rows[0].Clone();
                            row.Cells[0].Value = j + 1;
                            row.Cells[1].Value = dt1.Rows[j]["ComboUid"].ToString();
                            row.Cells[2].Value = dt1.Rows[j]["Price"].ToString();
                            row.Cells[3].Value = dt1.Rows[j]["Uid"].ToString();
                            DataGridStylePrice.Rows.Add(row);
                        }
                    }
                }
                else // Size wise
                {
                    DataGridStylePrice.Columns.Clear();
                    DataGridStylePrice.DataSource = null;
                    DataGridStylePrice.AutoGenerateColumns = false;
                    DataGridStylePrice.ColumnCount = 7;
                    DataGridStylePrice.Columns[0].Name = "SlNo";
                    DataGridStylePrice.Columns[0].HeaderText = "SlNo";
                    DataGridStylePrice.Columns[0].Width = 90;

                    DataGridStylePrice.Columns[1].Name = "StyleName";
                    DataGridStylePrice.Columns[1].HeaderText = "StyleName";
                    DataGridStylePrice.Columns[1].Width = 100;

                    DataGridStylePrice.Columns[2].Name = "Combo";
                    DataGridStylePrice.Columns[2].HeaderText = "Combo";
                    DataGridStylePrice.Columns[2].Width = 150;

                    DataGridStylePrice.Columns[3].Name = "Size";
                    DataGridStylePrice.Columns[3].HeaderText = "Size";
                    DataGridStylePrice.Columns[3].Width = 90;

                    DataGridStylePrice.Columns[4].Name = "Price";
                    DataGridStylePrice.Columns[4].HeaderText = "Price";
                    DataGridStylePrice.Columns[4].Width = 90;
                    DataGridStylePrice.Columns[4].DefaultCellStyle.Format = "N2";

                    DataGridStylePrice.Columns[5].Name = "ComboUid";
                    DataGridStylePrice.Columns[5].HeaderText = "ComboUid";
                    DataGridStylePrice.Columns[5].Visible = false;

                    DataGridStylePrice.Columns[6].Name = "SizeUid";
                    DataGridStylePrice.Columns[6].HeaderText = "SizeUid";
                    DataGridStylePrice.Columns[6].Visible = false;
                    if (txtDocNo.Tag != null)
                    {
                        DataGridStylePrice.Rows.Clear();
                        string Query = @"Select a.StyleName,d.ComboUid as Combo,e.SizeName,c.Price,e.uid,d.uid as ComboUid from OrdermStyles a
                                        inner join GeneralM b on a.PlanUomUid = b.GUid  and TypeMUid = 7 
                                        inner join StyleCombo d on a.OrrderMUid = d.OrderMUid and a.Uid = d.StyleUid
                                        inner join StyleSize e on a.OrrderMUid = e.OrderMUid
                                        Left join OrderMPrice c on a.OrrderMUid = c.OrderMuid and c.PriceType='Size Wise' 
                                        and a.Uid = c.OrderStyleUid and d.Uid = c.StyleComboUid and e.Uid = c.StyleSizeUid
                                        Where a.OrrderMUid= " + txtDocNo.Tag + " and a.uid =" + cmbStylePrice.SelectedValue + "";
                        DataTable dt1 = db.GetDataWithoutParam(CommandType.Text, Query, conn);
                        for (int j = 0; j < dt1.Rows.Count; j++)
                        {
                            DataGridViewRow row = (DataGridViewRow)DataGridStylePrice.Rows[0].Clone();
                            row.Cells[0].Value = j + 1;
                            row.Cells[1].Value = dt1.Rows[j]["StyleName"].ToString();
                            row.Cells[2].Value = dt1.Rows[j]["Combo"].ToString();
                            row.Cells[3].Value = dt1.Rows[j]["SizeName"].ToString();
                            row.Cells[4].Value = dt1.Rows[j]["Price"].ToString();
                            row.Cells[5].Value = dt1.Rows[j]["ComboUid"].ToString();
                            row.Cells[6].Value = dt1.Rows[j]["uid"].ToString();
                            DataGridStylePrice.Rows.Add(row);
                        }
                    }
                }
                LoadId = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected void LoadDataGridCharges()
        {
            try
            {
                DataGridCharges.AutoGenerateColumns = false;
                DataGridCharges.ColumnCount = 5;
                DataGridCharges.Columns[0].Name = "SlNo";
                DataGridCharges.Columns[0].Name = "SlNo";
                DataGridCharges.Columns[0].Width = 50;

                DataGridCharges.Columns[1].Name = "Description";
                DataGridCharges.Columns[1].Name = "Description";
                DataGridCharges.Columns[1].Width = 180;

                DataGridCharges.Columns[2].Name = "%";
                DataGridCharges.Columns[2].Name = "%";
                DataGridCharges.Columns[2].Width = 50;

                DataGridCharges.Columns[3].Name = "Amount";
                DataGridCharges.Columns[3].Name = "Amount";
                DataGridCharges.Columns[3].Width = 90;

                DataGridCharges.Columns[4].Name = "Type";
                DataGridCharges.Columns[4].Name = "Type";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected void LoadDataGridDiscounts()
        {
            try
            {
                DataGridDiscounts.AutoGenerateColumns = false;
                DataGridDiscounts.ColumnCount = 5;
                DataGridDiscounts.Columns[0].Name = "SlNo";
                DataGridDiscounts.Columns[0].Name = "SlNo";
                DataGridDiscounts.Columns[0].Width = 50;

                DataGridDiscounts.Columns[1].Name = "Description";
                DataGridDiscounts.Columns[1].Name = "Description";
                DataGridDiscounts.Columns[1].Width = 180;

                DataGridDiscounts.Columns[2].Name = "%";
                DataGridDiscounts.Columns[2].Name = "%";
                DataGridDiscounts.Columns[2].Width = 50;

                DataGridDiscounts.Columns[3].Name = "Amount";
                DataGridDiscounts.Columns[3].Name = "Amount";
                DataGridDiscounts.Columns[3].Width = 90;

                DataGridDiscounts.Columns[4].Name = "Type";
                DataGridDiscounts.Columns[4].Name = "Type";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected void LoadGeneralsOrderEntry()
        {
            try
            {
                DataTable dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "SP_GetGeneralsForOrderEntry", conn);
                DataTable dtSeason = dt.Select("TypeMUid = " + (int)OrderEntryGeneralM.Season + "").CopyToDataTable();
                DataTable dtOrderStatus = dt.Select("TypeMUid = " + (int)OrderEntryGeneralM.OrderStatus + "").CopyToDataTable();
                DataTable dtOrderType = dt.Select("TypeMUid = " + (int)OrderEntryGeneralM.OrderType + "").CopyToDataTable();
                DataTable dtCombo = dt.Select("TypeMUid = " + (int)OrderEntryGeneralM.Combo + "").CopyToDataTable();
                cmbSeason.DataSource = null;
                cmbSeason.DisplayMember = "GeneralName";
                cmbSeason.ValueMember = "GUid";
                cmbSeason.DataSource = dtSeason;

                cmbSts.DataSource = null;
                cmbSts.DisplayMember = "GeneralName";
                cmbSts.ValueMember = "GUid";
                cmbSts.DataSource = dtOrderStatus;

                cmbOrderType.DataSource = null;
                cmbOrderType.DisplayMember = "GeneralName";
                cmbOrderType.ValueMember = "GUid";
                cmbOrderType.DataSource = dtOrderType;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void TxtCustomer_Click(object sender, EventArgs e)
        {
            try
            {
                string Query = "Select CUid,CompanyName as Name,City from PartyM where Tag=0 order by CompanyName";
                DataTable dtCompany = db.GetDataWithoutParam(CommandType.Text, Query, conn);
                bsCustomer.DataSource = dtCompany;
                FillGrid(dtCompany, 1);
                Point loc = Genclass.FindLocation(txtCustomer);
                grSearch.Location = new Point(loc.X - 10, loc.Y + 10);
                grSearch.Visible = true;
                txtCustomer.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void FillGrid(DataTable dt, int FillId)
        {
            try
            {
                SelectId = 1;
                DataGridCommon.DataSource = null;
                DataGridCommon.AutoGenerateColumns = false;
                if (FillId == 1)
                {
                    Fillid = 1;
                    DataGridCommon.ColumnCount = 3;
                    DataGridCommon.Columns[0].Name = "CUid";
                    DataGridCommon.Columns[0].HeaderText = "CUid";
                    DataGridCommon.Columns[0].DataPropertyName = "CUid";
                    DataGridCommon.Columns[0].Visible = false;
                    DataGridCommon.Columns[0].Width = 0;
                    DataGridCommon.Columns[1].Name = "Name";
                    DataGridCommon.Columns[1].HeaderText = "Name";
                    DataGridCommon.Columns[1].DataPropertyName = "Name";
                    DataGridCommon.Columns[1].Width = 320;
                    DataGridCommon.Columns[2].Name = "City";
                    DataGridCommon.Columns[2].HeaderText = "City";
                    DataGridCommon.Columns[2].DataPropertyName = "City";
                    DataGridCommon.Columns[2].Visible = false;
                    DataGridCommon.DataSource = bsCustomer;
                }
                else if (FillId == 2)
                {
                    Fillid = 2;
                    DataGridCommon.ColumnCount = 3;
                    DataGridCommon.Columns[0].Name = "GUid";
                    DataGridCommon.Columns[0].HeaderText = "GUid";
                    DataGridCommon.Columns[0].DataPropertyName = "GUid";
                    DataGridCommon.Columns[0].Visible = false;
                    DataGridCommon.Columns[0].Width = 0;
                    DataGridCommon.Columns[1].Name = "Name";
                    DataGridCommon.Columns[1].HeaderText = "Name";
                    DataGridCommon.Columns[1].DataPropertyName = "GeneralName";
                    DataGridCommon.Columns[1].Width = 220;
                    DataGridCommon.Columns[2].Name = "ShortName";
                    DataGridCommon.Columns[2].HeaderText = "ShortName";
                    DataGridCommon.Columns[2].DataPropertyName = "ShortName";
                    DataGridCommon.DataSource = bsMerchindiser;
                }
                else if (FillId == 3)
                {
                    Fillid = 3;
                    DataGridCommon.ColumnCount = 3;
                    DataGridCommon.Columns[0].Name = "GUid";
                    DataGridCommon.Columns[0].HeaderText = "GUid";
                    DataGridCommon.Columns[0].DataPropertyName = "GUid";
                    DataGridCommon.Columns[0].Visible = false;
                    DataGridCommon.Columns[0].Width = 0;
                    DataGridCommon.Columns[1].Name = "Name";
                    DataGridCommon.Columns[1].HeaderText = "Name";
                    DataGridCommon.Columns[1].DataPropertyName = "GeneralName";
                    DataGridCommon.Columns[1].Width = 220;
                    DataGridCommon.Columns[2].Name = "ShortName";
                    DataGridCommon.Columns[2].HeaderText = "ShortName";
                    DataGridCommon.Columns[2].DataPropertyName = "ShortName";
                    DataGridCommon.DataSource = bsDept;
                }
                else if (FillId == 4)
                {
                    Fillid = 4;
                    DataGridCommon.ColumnCount = 3;
                    DataGridCommon.Columns[0].Name = "GUid";
                    DataGridCommon.Columns[0].HeaderText = "GUid";
                    DataGridCommon.Columns[0].DataPropertyName = "GUid";
                    DataGridCommon.Columns[0].Visible = false;
                    DataGridCommon.Columns[0].Width = 0;
                    DataGridCommon.Columns[1].Name = "Name";
                    DataGridCommon.Columns[1].HeaderText = "Name";
                    DataGridCommon.Columns[1].DataPropertyName = "GeneralName";
                    DataGridCommon.Columns[1].Width = 220;
                    DataGridCommon.Columns[2].Name = "ShortName";
                    DataGridCommon.Columns[2].HeaderText = "ShortName";
                    DataGridCommon.Columns[2].DataPropertyName = "ShortName";
                    DataGridCommon.DataSource = bsAgent;
                }
                else if (FillId == 5)
                {
                    Fillid = 5;
                    DataGridCommon.ColumnCount = 3;
                    DataGridCommon.Columns[0].Name = "GUid";
                    DataGridCommon.Columns[0].HeaderText = "GUid";
                    DataGridCommon.Columns[0].DataPropertyName = "GUid";
                    DataGridCommon.Columns[0].Visible = false;
                    DataGridCommon.Columns[0].Width = 0;
                    DataGridCommon.Columns[1].Name = "Name";
                    DataGridCommon.Columns[1].HeaderText = "Name";
                    DataGridCommon.Columns[1].DataPropertyName = "GeneralName";
                    DataGridCommon.Columns[1].Width = 220;
                    DataGridCommon.Columns[2].Name = "ShortName";
                    DataGridCommon.Columns[2].HeaderText = "ShortName";
                    DataGridCommon.Columns[2].DataPropertyName = "ShortName";
                    DataGridCommon.DataSource = bsShipType;
                }
                else if (FillId == 6)
                {
                    Fillid = 6;
                    DataGridCommon.ColumnCount = 3;
                    DataGridCommon.Columns[0].Name = "GUid";
                    DataGridCommon.Columns[0].HeaderText = "GUid";
                    DataGridCommon.Columns[0].DataPropertyName = "GUid";
                    DataGridCommon.Columns[0].Visible = false;
                    DataGridCommon.Columns[0].Width = 0;
                    DataGridCommon.Columns[1].Name = "Name";
                    DataGridCommon.Columns[1].HeaderText = "Name";
                    DataGridCommon.Columns[1].DataPropertyName = "GeneralName";
                    DataGridCommon.Columns[1].Width = 220;
                    DataGridCommon.Columns[2].Name = "ShortName";
                    DataGridCommon.Columns[2].HeaderText = "ShortName";
                    DataGridCommon.Columns[2].DataPropertyName = "ShortName";
                    DataGridCommon.DataSource = bsShiftMode;
                }
                else if (FillId == 7)
                {
                    Fillid = 7;
                    DataGridCommon.ColumnCount = 3;
                    DataGridCommon.Columns[0].Name = "GUid";
                    DataGridCommon.Columns[0].HeaderText = "GUid";
                    DataGridCommon.Columns[0].DataPropertyName = "GUid";
                    DataGridCommon.Columns[0].Visible = false;
                    DataGridCommon.Columns[0].Width = 0;
                    DataGridCommon.Columns[1].Name = "Name";
                    DataGridCommon.Columns[1].HeaderText = "Name";
                    DataGridCommon.Columns[1].DataPropertyName = "GeneralName";
                    DataGridCommon.Columns[1].Width = 220;
                    DataGridCommon.Columns[2].Name = "ShortName";
                    DataGridCommon.Columns[2].HeaderText = "ShortName";
                    DataGridCommon.Columns[2].DataPropertyName = "ShortName";
                    DataGridCommon.DataSource = bsPaymode;
                }
                else if (FillId == 8)
                {
                    Fillid = 8;
                    DataGridCommon.ColumnCount = 3;
                    DataGridCommon.Columns[0].Name = "GUid";
                    DataGridCommon.Columns[0].HeaderText = "GUid";
                    DataGridCommon.Columns[0].DataPropertyName = "GUid";
                    DataGridCommon.Columns[0].Visible = false;
                    DataGridCommon.Columns[0].Width = 0;
                    DataGridCommon.Columns[1].Name = "Name";
                    DataGridCommon.Columns[1].HeaderText = "Name";
                    DataGridCommon.Columns[1].DataPropertyName = "GeneralName";
                    DataGridCommon.Columns[1].Width = 220;
                    DataGridCommon.Columns[2].Name = "ShortName";
                    DataGridCommon.Columns[2].HeaderText = "ShortName";
                    DataGridCommon.Columns[2].DataPropertyName = "ShortName";
                    DataGridCommon.DataSource = bsCountry;
                }
                else if (FillId == 9)
                {
                    Fillid = 9;
                    DataGridCommon.ColumnCount = 3;
                    DataGridCommon.Columns[0].Name = "GUid";
                    DataGridCommon.Columns[0].HeaderText = "GUid";
                    DataGridCommon.Columns[0].DataPropertyName = "GUid";
                    DataGridCommon.Columns[0].Visible = false;
                    DataGridCommon.Columns[0].Width = 0;
                    DataGridCommon.Columns[1].Name = "Name";
                    DataGridCommon.Columns[1].HeaderText = "Name";
                    DataGridCommon.Columns[1].DataPropertyName = "GeneralName";
                    DataGridCommon.Columns[1].Width = 220;
                    DataGridCommon.Columns[2].Name = "ShortName";
                    DataGridCommon.Columns[2].HeaderText = "ShortName";
                    DataGridCommon.Columns[2].DataPropertyName = "ShortName";
                    DataGridCommon.DataSource = bsUOM;
                }
                else if (FillId == 10)
                {
                    Fillid = 10;
                    DataGridCommon.ColumnCount = 3;
                    DataGridCommon.Columns[0].Name = "GUid";
                    DataGridCommon.Columns[0].HeaderText = "GUid";
                    DataGridCommon.Columns[0].DataPropertyName = "GUid";
                    DataGridCommon.Columns[0].Visible = false;
                    DataGridCommon.Columns[0].Width = 0;
                    DataGridCommon.Columns[1].Name = "Name";
                    DataGridCommon.Columns[1].HeaderText = "Name";
                    DataGridCommon.Columns[1].DataPropertyName = "GeneralName";
                    DataGridCommon.Columns[1].Width = 220;
                    DataGridCommon.Columns[2].Name = "ShortName";
                    DataGridCommon.Columns[2].HeaderText = "ShortName";
                    DataGridCommon.Columns[2].DataPropertyName = "ShortName";
                    DataGridCommon.DataSource = bsUOM;
                }
                else if (FillId == 11)
                {
                    Fillid = 11;
                    DataGridCommon.ColumnCount = 3;
                    DataGridCommon.Columns[0].Name = "GUid";
                    DataGridCommon.Columns[0].HeaderText = "GUid";
                    DataGridCommon.Columns[0].DataPropertyName = "GUid";
                    DataGridCommon.Columns[0].Visible = false;
                    DataGridCommon.Columns[0].Width = 0;
                    DataGridCommon.Columns[1].Name = "Name";
                    DataGridCommon.Columns[1].HeaderText = "Name";
                    DataGridCommon.Columns[1].DataPropertyName = "GeneralName";
                    DataGridCommon.Columns[1].Width = 220;
                    DataGridCommon.Columns[2].Name = "ShortName";
                    DataGridCommon.Columns[2].HeaderText = "ShortName";
                    DataGridCommon.Columns[2].DataPropertyName = "ShortName";
                    DataGridCommon.DataSource = bsCombo;
                }
                else if (FillId == 12)
                {
                    Fillid = 12;
                    DataGridCommon.ColumnCount = 3;
                    DataGridCommon.Columns[0].Name = "GUid";
                    DataGridCommon.Columns[0].HeaderText = "GUid";
                    DataGridCommon.Columns[0].DataPropertyName = "GUid";
                    DataGridCommon.Columns[0].Visible = false;
                    DataGridCommon.Columns[0].Width = 0;
                    DataGridCommon.Columns[1].Name = "Name";
                    DataGridCommon.Columns[1].HeaderText = "Name";
                    DataGridCommon.Columns[1].DataPropertyName = "GeneralName";
                    DataGridCommon.Columns[1].Width = 220;
                    DataGridCommon.Columns[2].Name = "ShortName";
                    DataGridCommon.Columns[2].HeaderText = "ShortName";
                    DataGridCommon.Columns[2].DataPropertyName = "ShortName";
                    DataGridCommon.DataSource = bsStrucType;
                }
                else if (FillId == 13)
                {
                    Fillid = 13;
                    DataGridCommon.ColumnCount = 3;
                    DataGridCommon.Columns[0].Name = "GUid";
                    DataGridCommon.Columns[0].HeaderText = "GUid";
                    DataGridCommon.Columns[0].DataPropertyName = "GUid";
                    DataGridCommon.Columns[0].Visible = false;
                    DataGridCommon.Columns[0].Width = 0;
                    DataGridCommon.Columns[1].Name = "Name";
                    DataGridCommon.Columns[1].HeaderText = "Name";
                    DataGridCommon.Columns[1].DataPropertyName = "GeneralName";
                    DataGridCommon.Columns[1].Width = 220;
                    DataGridCommon.Columns[2].Name = "ShortName";
                    DataGridCommon.Columns[2].HeaderText = "ShortName";
                    DataGridCommon.Columns[2].DataPropertyName = "ShortName";
                    DataGridCommon.DataSource = bsCurrency;
                }
                else if (FillId == 14)
                {
                    Fillid = 14;
                    DataGridCommon.ColumnCount = 3;
                    DataGridCommon.Columns[0].Name = "Uid";
                    DataGridCommon.Columns[0].HeaderText = "Uid";
                    DataGridCommon.Columns[0].DataPropertyName = "Uid";
                    DataGridCommon.Columns[0].Visible = false;
                    DataGridCommon.Columns[0].Width = 0;
                    DataGridCommon.Columns[1].Name = "DocNo";
                    DataGridCommon.Columns[1].HeaderText = "DocNo";
                    DataGridCommon.Columns[1].DataPropertyName = "DocNo";
                    DataGridCommon.Columns[1].Width = 220;
                    DataGridCommon.Columns[2].Name = "DocDate";
                    DataGridCommon.Columns[2].HeaderText = "DocDate";
                    DataGridCommon.Columns[2].DataPropertyName = "DocDate";
                    DataGridCommon.DataSource = bsDocNo;
                }
                SelectId = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void TxtCustomer_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bsCustomer.Filter = string.Format("Name LIKE '%{0}%'", txtCustomer.Text);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void BtnSelect_Click(object sender, EventArgs e)
        {
            try
            {
                SelectId = 1;
                int Index = DataGridCommon.SelectedCells[0].RowIndex;
                if (Fillid == 1)
                {
                    txtCustomer.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtCustomer.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    cmbSeason.Focus();
                }
                else if (Fillid == 2)
                {
                    txtMerchindiser.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtMerchindiser.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    cmbSts.Focus();
                }
                else if (Fillid == 3)
                {
                    txtDepartment.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtDepartment.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    txtAgent.Focus();
                }
                else if (Fillid == 4)
                {
                    txtAgent.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtAgent.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    dtpRecivedDt.Focus();
                }
                else if (Fillid == 5)
                {
                    txtShipType.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtShipType.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    txtShiftMode.Focus();
                }
                else if (Fillid == 6)
                {
                    txtShiftMode.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtShiftMode.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    txtPayMode.Focus();
                }
                else if (Fillid == 7)
                {
                    txtPayMode.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtPayMode.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    txtPayTerms.Focus();
                }
                else if (Fillid == 8)
                {
                    txtCountry.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtCountry.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    txtCurrency.Focus();
                }
                else if (Fillid == 11)
                {
                    txtCombo.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtCombo.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    txtComboQty.Focus();
                }
                else if (Fillid == 12)
                {
                    txtStructureType.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtStructureType.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    txtComboQty.Focus();
                }
                else if (Fillid == 13)
                {
                    txtCurrency.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtCurrency.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    txtExRate.Focus();
                }
                else if (Fillid == 14)
                {
                    txtDocNo.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtDocNo.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    GetOrderMEdit(Convert.ToDecimal(txtDocNo.Tag));
                }
                grSearch.Visible = false;
                SelectId = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void BtnHide_Click(object sender, EventArgs e)
        {
            grSearch.Visible = false;
        }

        private void TxtMerchindiser_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "SP_GetGeneralsForOrderEntry", conn);
                DataTable dtMerchindiser = dt.Select("TypeMUid = " + (int)OrderEntryGeneralM.Merchindiser + "").CopyToDataTable();
                bsMerchindiser.DataSource = dtMerchindiser;
                FillGrid(dtMerchindiser, 2);
                Point loc = Genclass.FindLocation(txtMerchindiser);
                grSearch.Location = new Point(loc.X - 10, loc.Y + 10);
                grSearch.Visible = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void TxtMerchindiser_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bsMerchindiser.Filter = string.Format("GeneralName LIKE '%{0}%'", txtMerchindiser.Text);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void TxtDepartment_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "SP_GetGeneralsForOrderEntry", conn);
                DataTable dtDept = dt.Select("TypeMUid = " + (int)OrderEntryGeneralM.Dept + "").CopyToDataTable();
                bsDept.DataSource = dtDept;
                FillGrid(dtDept, 3);
                Point loc = Genclass.FindLocation(txtDepartment);
                grSearch.Location = new Point(loc.X - 10, loc.Y + 10);
                grSearch.Visible = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void TxtDepartment_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bsDept.Filter = string.Format("GeneralName LIKE '%{0}%'", txtDepartment.Text);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void TxtAgent_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "SP_GetGeneralsForOrderEntry", conn);
                DataTable dtAgent = dt.Select("TypeMUid = " + (int)OrderEntryGeneralM.Agent + "").CopyToDataTable();
                bsAgent.DataSource = dtAgent;
                FillGrid(dtAgent, 4);
                Point loc = Genclass.FindLocation(txtAgent);
                grSearch.Location = new Point(loc.X - 10, loc.Y + 10);
                grSearch.Visible = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void TxtAgent_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bsAgent.Filter = string.Format("GeneralName LIKE '%{0}%'", txtAgent.Text);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void TxtShipType_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "SP_GetGeneralsForOrderEntry", conn);
                DataTable dtShipType = dt.Select("TypeMUid = " + (int)OrderEntryGeneralM.ShipType + "").CopyToDataTable();
                bsShipType.DataSource = dtShipType;
                FillGrid(dtShipType, 5);
                Point loc = Genclass.FindLocation(txtShipType);
                grSearch.Location = new Point(loc.X - 10, loc.Y + 10);
                grSearch.Visible = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void TxtShipType_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bsShipType.Filter = string.Format("GeneralName LIKE '%{0}%'", txtShipType.Text);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void TxtShiftMode_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "SP_GetGeneralsForOrderEntry", conn);
                DataTable dtShiftMode = dt.Select("TypeMUid = " + (int)OrderEntryGeneralM.ShiftMode + "").CopyToDataTable();
                bsShiftMode.DataSource = dtShiftMode;
                FillGrid(dtShiftMode, 6);
                Point loc = Genclass.FindLocation(txtShiftMode);
                grSearch.Location = new Point(loc.X - 10, loc.Y + 10);
                grSearch.Visible = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void TxtShiftMode_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bsShiftMode.Filter = string.Format("GeneralName LIKE '%{0}%'", txtShiftMode.Text);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void TxtPayMode_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "SP_GetGeneralsForOrderEntry", conn);
                DataTable dtPyMode = dt.Select("TypeMUid = " + (int)OrderEntryGeneralM.PyMode + "").CopyToDataTable();
                bsPaymode.DataSource = dtPyMode;
                FillGrid(dtPyMode, 7);
                Point loc = Genclass.FindLocation(txtPayMode);
                grSearch.Location = new Point(loc.X - 10, loc.Y + 10);
                grSearch.Visible = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void TxtPayMode_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bsPaymode.Filter = string.Format("GeneralName LIKE '%{0}%'", txtPayMode.Text);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void TxtCountry_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "SP_GetGeneralsForOrderEntry", conn);
                DataTable dtCountry = dt.Select("TypeMUid = " + (int)OrderEntryGeneralM.Country + "").CopyToDataTable();
                bsCountry.DataSource = dtCountry;
                FillGrid(dtCountry, 8);
                Point loc = Genclass.FindLocation(txtCountry);
                grSearch.Location = new Point(loc.X - 70, loc.Y + 10);
                grSearch.Visible = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void TxtCountry_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bsCountry.Filter = string.Format("GeneralName LIKE '%{0}%'", txtCountry.Text);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void DataGridCommon_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                SelectId = 1;
                int Index = DataGridCommon.SelectedCells[0].RowIndex;
                if (Fillid == 1)
                {
                    txtCustomer.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtCustomer.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    cmbSeason.Focus();
                }
                else if (Fillid == 2)
                {
                    txtMerchindiser.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtMerchindiser.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    cmbSts.Focus();
                }
                else if (Fillid == 3)
                {
                    txtDepartment.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtDepartment.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    txtAgent.Focus();
                }
                else if (Fillid == 4)
                {
                    txtAgent.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtAgent.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    dtpRecivedDt.Focus();
                }
                else if (Fillid == 5)
                {
                    txtShipType.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtShipType.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    txtShiftMode.Focus();
                }
                else if (Fillid == 6)
                {
                    txtShiftMode.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtShiftMode.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    txtPayMode.Focus();
                }
                else if (Fillid == 7)
                {
                    txtPayMode.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtPayMode.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    txtPayTerms.Focus();
                }
                else if (Fillid == 8)
                {
                    txtCountry.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtCountry.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    txtCurrency.Focus();
                }
                else if (Fillid == 11)
                {
                    txtCombo.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtCombo.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    txtComboQty.Focus();
                }
                else if (Fillid == 12)
                {
                    txtStructureType.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtStructureType.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    txtComboQty.Focus();
                }
                else if (Fillid == 13)
                {
                    txtCurrency.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtCurrency.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    txtExRate.Focus();
                }
                else if (Fillid == 14)
                {
                    txtDocNo.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtDocNo.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    GetOrderMEdit(Convert.ToDecimal(txtDocNo.Tag));
                }
                grSearch.Visible = false;
                SelectId = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void BtnStyleOk_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtStyleNo.Text == string.Empty)
                {
                    MessageBox.Show("Style No can't Empty", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtStyleNo.Focus();
                    return;
                }
                else if (txtdescription.Text == string.Empty)
                {
                    MessageBox.Show("Style Description can't Empty", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtdescription.Focus();
                    return;
                }
                else if (cmbOrderUOm.SelectedIndex == -1)
                {
                    MessageBox.Show("Order UOM can't Empty", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    cmbOrderUOm.Focus();
                    return;
                }
                else if (CmbPlanUom.SelectedIndex == -1)
                {
                    MessageBox.Show("Plan UOM can't Empty", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    CmbPlanUom.Focus();
                    return;
                }
                else if (txtQuantity.Text == string.Empty)
                {
                    MessageBox.Show("Quantity can't Empty", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtQuantity.Focus();
                    return;
                }
                else if (txtQuantity.Text == "0")
                {
                    MessageBox.Show("Quantity can't 0", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtQuantity.Focus();
                    return;
                }
                else
                {
                    if (StyleEdit == 1)
                    {
                        SqlParameter[] parastyles = {
                        new SqlParameter("@Uid",EditStyleId),
                        new SqlParameter("@OrrderMUid",txtDocNo.Tag),
                        new SqlParameter("@StyleName",txtStyleNo.Text),
                        new SqlParameter("@Styledescription",txtdescription.Text),
                        new SqlParameter("@OrderUomUid",cmbOrderUOm.SelectedValue),
                        new SqlParameter("@PlanUomUid",CmbPlanUom.SelectedValue),
                        new SqlParameter("@StyleQty",txtQuantity.Text),
                        new SqlParameter("@StyleUid",SqlDbType.Decimal),
                                    };
                        parastyles[7].Direction = ParameterDirection.Output;
                        db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_OrderMStyles", parastyles, conn, 7);
                        EditStyleId = 0;
                    }
                    if (txtStyleNo.Text != string.Empty || txtdescription.Text != string.Empty || txtQuantity.Text != string.Empty)
                    {
                        bool checkBagNo = CheckGridValue();
                        if (checkBagNo == false)
                        {
                            if (txtQuantity.Text == string.Empty)
                            {
                                txtQuantity.Text = "0";
                            }
                            DataGridViewRow rows = (DataGridViewRow)DataGridStyle.Rows[0].Clone();
                            rows.Cells[0].Value = DataGridStyle.Rows.Count;
                            rows.Cells[1].Value = txtStyleNo.Text;
                            rows.Cells[2].Value = txtdescription.Text;
                            rows.Cells[3].Value = cmbOrderUOm.Text;
                            rows.Cells[4].Value = CmbPlanUom.Text;
                            rows.Cells[5].Value = txtQuantity.Text;
                            rows.Cells[6].Value = cmbOrderUOm.SelectedValue;
                            rows.Cells[7].Value = CmbPlanUom.SelectedValue;
                            rows.Cells[8].Value = EditStyleId;
                            DataGridStyle.Rows.Add(rows);
                            OrderQty += Convert.ToDecimal(txtQuantity.Text);
                            txtStyleNo.Text = string.Empty;
                            txtdescription.Text = string.Empty;
                            cmbOrderUOm.SelectedIndex = -1;
                            CmbPlanUom.SelectedIndex = -1;
                            txtQuantity.Text = string.Empty;
                            txtStyleNo.Focus();
                        }
                        else
                        {
                            MessageBox.Show("Style already exists", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            txtStyleNo.Focus();
                        }
                        txtStyleDesc.Text = string.Empty;
                        for (int i = 0; i < DataGridStyle.Rows.Count - 1; i++)
                        {
                            if (i == 0)
                            {
                                txtStyleDesc.Text = DataGridStyle.Rows[i].Cells[1].Value.ToString();
                            }
                            else
                            {
                                txtStyleDesc.Text = txtStyleDesc.Text + "," + DataGridStyle.Rows[i].Cells[1].Value.ToString();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected bool CheckGridValue()
        {
            bool entryFound = false;
            try
            {
                foreach (DataGridViewRow row in DataGridStyle.Rows)
                {
                    object val2 = row.Cells[1].Value;
                    if (val2 != null && val2.ToString() == txtStyleNo.Text)
                    {
                        entryFound = true;
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            return entryFound;
        }

        protected bool CheckSizeGridValue()
        {
            bool entryFound = false;
            try
            {
                foreach (DataGridViewRow row in DataGridStyleSize.Rows)
                {
                    object val2 = row.Cells[0].Value;
                    if (val2 != null && val2.ToString() == txtStyleSize.Text)
                    {
                        entryFound = true;
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            return entryFound;
        }

        private void TabControlOrderEntry_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (clearid == 0)
                {
                    if (tabControlOrderEntry.SelectedIndex == 4)
                    {
                        DataTable dt = GetorderAssort(Convert.ToDecimal(txtDocNo.Tag));
                        cmbAssort.DataSource = null;
                        LoadId = 1;
                        cmbAssort.DisplayMember = "Assort";
                        cmbAssort.ValueMember = "Uid";
                        cmbAssort.DataSource = dt;
                        LoadId = 0;
                    }
                    if (tabControlOrderEntry.SelectedIndex == 1)
                    {
                        DataTable dt = GetorderAssort(Convert.ToDecimal(txtDocNo.Tag));
                        cmbAssort.DataSource = null;
                        LoadId = 1;
                        cmbAssort.DisplayMember = "Assort";
                        cmbAssort.ValueMember = "AssortUid";
                        cmbAssort.DataSource = dt;
                        LoadId = 0;
                    }
                    if (tabControlOrderEntry.SelectedIndex == 2)
                    {
                        if (CmbStyle.SelectedIndex != -1)
                        {
                            DataGridStyleSize.Rows.Clear();
                            SqlParameter[] parameters = { new SqlParameter("@OrderStyleUid", CmbStyle.SelectedValue), new SqlParameter("@OrderMUid", txtDocNo.Tag) };
                            DataSet ds = db.GetDataWithParamMultiple(CommandType.StoredProcedure, "SP_GetStyleSizeforCombo", parameters, conn);
                            DataTable dt = ds.Tables[0];
                            if (dt.Rows.Count > 0)
                            {
                                for (int i = 0; i < dt.Rows.Count; i++)
                                {
                                    DataGridViewRow row = (DataGridViewRow)DataGridStyleSize.Rows[0].Clone();
                                    row.Cells[0].Value = dt.Rows[i]["SizeName"].ToString();
                                    row.Cells[1].Value = dt.Rows[i]["OrderMUid"].ToString();
                                    DataGridStyleSize.Rows.Add(row);
                                }
                            }
                        }
                    }
                    if (tabControlOrderEntry.SelectedIndex == 5)
                    {
                        SelectId = 1;
                        string Query = "Select Uid,ComboUid from StyleCombo Where OrderMUid =" + txtDocNo.Tag + "";
                        DataTable dtCombo = db.GetDataWithoutParam(CommandType.Text, Query, conn);
                        CmbPlanningStyleCombo.DisplayMember = "ComboUid";
                        CmbPlanningStyleCombo.ValueMember = "Uid";
                        CmbPlanningStyleCombo.DataSource = dtCombo;
                        SelectId = 0;
                        CmbPlanningStyleCombo_SelectedIndexChanged(sender, e);
                        ////BtnCalculate_Click(sender, e);
                    }
                    if (tabControlOrderEntry.SelectedIndex == 7)
                    {
                        if (DataGridStylePrice.Rows.Count > 1)
                        {
                            decimal price = 0;
                            for (int i = 0; i < DataGridStylePrice.Rows.Count - 1; i++)
                            {
                                if (cmbPriceType.Text == "Style Wise" || cmbPriceType.Text == "Combo Wise")
                                {
                                    if (string.IsNullOrEmpty(DataGridStylePrice.Rows[i].Cells[2].Value.ToString()))
                                    {
                                        price = 0;
                                    }
                                    else
                                    {
                                        price = Convert.ToDecimal(DataGridStylePrice.Rows[i].Cells[2].Value.ToString());
                                    }
                                    if (price == 0)
                                    {
                                        MessageBox.Show("Enter Price", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                        tabControlOrderEntry.TabIndex = 6;
                                        return;
                                    }
                                }
                                else
                                {
                                    if (string.IsNullOrEmpty(DataGridStylePrice.Rows[i].Cells[4].Value.ToString()))
                                    {
                                        price = 0;
                                    }
                                    else
                                    {
                                        price = Convert.ToDecimal(DataGridStylePrice.Rows[i].Cells[4].Value.ToString());
                                    }
                                    if (price == 0)
                                    {
                                        MessageBox.Show("Enter Price", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                        tabControlOrderEntry.TabIndex = 6;
                                        return;
                                    }
                                }
                            }
                        }
                        SqlParameter[] sqlParameters = { new SqlParameter("@ORDERMUID", txtDocNo.Tag), new SqlParameter("@PRICETYPE", cmbPriceType.Text) };
                        DataTable dt = db.GetDataWithParam(CommandType.StoredProcedure, "PROC_GETAVGPRICE", sqlParameters, conn);
                        if (dt.Rows.Count == 0)
                        {
                            LblAvgDate.Text = "0.000";
                        }
                        else
                        {
                            LblAvgDate.Text = Convert.ToDecimal(dt.Rows[0]["AVGCOST"].ToString()).ToString("0.000");
                        }
                        LoadId = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void BtnStyleSizeOk_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtStyleSize.Text != string.Empty)
                {
                    bool checkBagNo = CheckSizeGridValue();
                    if (checkBagNo == false)
                    {
                        DataGridViewRow rows = (DataGridViewRow)DataGridStyleSize.Rows[0].Clone();
                        rows.Cells[0].Value = txtStyleSize.Text;
                        rows.Cells[1].Value = CmbStyle.SelectedValue;
                        DataGridStyleSize.Rows.Add(rows);
                        SqlParameter[] parameters = { new SqlParameter("@Uid","0"),
                            new SqlParameter("@OrderMUid",txtDocNo.Tag),
                            new SqlParameter("@SizeName",txtStyleSize.Text),
                            new SqlParameter("@OldSizeName",txtStyleSize.Tag)
                        };
                        db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_StyleSize", parameters, conn);
                        txtStyleSize.Text = string.Empty;
                        txtStyleSize.Focus();
                    }
                    else
                    {
                        MessageBox.Show("Size already exists", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        txtStyleSize.Focus();
                    }
                }
                else
                {
                    MessageBox.Show("Size Can't Empty !", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtStyleSize.Focus();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            try
            {
                decimal OrderUid = 0;
                decimal StyleUid = 0;
                if (txtCustomer.Text != string.Empty || txtOrderNo.Text != string.Empty || txtMerchindiser.Text != string.Empty)
                {
                    if (DataGridStyle.Rows.Count > 1)
                    {
                        if (btnSave.Text == "Save & Continue")
                        {
                            if (txtMerchindiser.Text == string.Empty)
                            {
                                txtMerchindiser.Tag = "1";
                            }
                            SqlParameter[] parameters =
                            {
                                new SqlParameter("@Uid",txtDocNo.Tag),
                                new SqlParameter("@DocNo",txtDocNo.Text),
                                new SqlParameter("@DocDate",Convert.ToDateTime(dtpDocDate.Text)),
                                new SqlParameter("@OrderNo",txtOrderNo.Text),
                                new SqlParameter("@OrderDt",Convert.ToDateTime(dtpOrderDate.Text)),
                                new SqlParameter("@CustomerUid",txtCustomer.Tag),
                                new SqlParameter("@SeasonUid",cmbSeason.SelectedValue),
                                new SqlParameter("@DeliveryDt",Convert.ToDateTime(dtpDeliveryDate.Text)),
                                new SqlParameter("@MerchindiserUid",txtMerchindiser.Tag),
                                new SqlParameter("@StatusUid",cmbSts.SelectedValue),
                                new SqlParameter("@TypeUid",cmbOrderType.SelectedValue),
                                new SqlParameter("@DeptUid",DBNull.Value),
                                new SqlParameter("@AgentUid",DBNull.Value),
                                new SqlParameter("@ReceivedDt",DBNull.Value),
                                new SqlParameter("@ShipTypeUid",DBNull.Value),
                                new SqlParameter("@ShiftModeUid",DBNull.Value),
                                new SqlParameter("@PyModeUid",DBNull.Value),
                                new SqlParameter("@PaymentTerms",DBNull.Value),
                                new SqlParameter("@ContactPerson",DBNull.Value),
                                new SqlParameter("@CurrencyUid",DBNull.Value),
                                new SqlParameter("@CountryUid",DBNull.Value),
                                new SqlParameter("@ExRate","1"),
                                new SqlParameter("@ReturnUid",SqlDbType.Decimal),
                                new SqlParameter("@SeasonYear",cmbYear.Text),
                                new SqlParameter("@AvgRate","1"),
                                new SqlParameter("@GrossValue","1"),
                                new SqlParameter("@ReceivedBy",DBNull.Value),
                                new SqlParameter("@DocSts","Entry Pending"),
                                new SqlParameter("@StyleDesc",txtStyleDesc.Text)
                            };
                            parameters[22].Direction = ParameterDirection.Output;
                            OrderUid = db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_OrderM", parameters, conn, 22);
                            if (DataGridStyle.Rows.Count > 1)
                            {
                                for (int i = 0; i < DataGridStyle.Rows.Count - 1; i++)
                                {
                                    decimal styleUid = Convert.ToDecimal(DataGridStyle.Rows[i].Cells[8].Value.ToString());
                                    string StyleName = DataGridStyle.Rows[i].Cells[1].Value.ToString();
                                    string Styledescription = DataGridStyle.Rows[i].Cells[2].Value.ToString();
                                    decimal OrderUomUid = Convert.ToDecimal(DataGridStyle.Rows[i].Cells[6].Value.ToString());
                                    decimal PlanUomUid = Convert.ToDecimal(DataGridStyle.Rows[i].Cells[7].Value.ToString());
                                    decimal StyleQty = Convert.ToDecimal(DataGridStyle.Rows[i].Cells[5].Value.ToString());
                                    SqlParameter[] parastyles = {
                                        new SqlParameter("@Uid",styleUid),
                                        new SqlParameter("@OrrderMUid",OrderUid),
                                        new SqlParameter("@StyleName",StyleName),
                                        new SqlParameter("@Styledescription",Styledescription),
                                        new SqlParameter("@OrderUomUid",OrderUomUid),
                                        new SqlParameter("@PlanUomUid",PlanUomUid),
                                        new SqlParameter("@StyleQty",StyleQty),
                                        new SqlParameter("@StyleUid",SqlDbType.Decimal),
                                        new SqlParameter("@OldStyleName",txtStyleNo.Tag)
                                    };
                                    parastyles[7].Direction = ParameterDirection.Output;
                                    StyleUid = db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_OrderMStyles", parastyles, conn, 7);
                                }
                            }
                            //string doc = txtDocNo.Text;

                            //string Query = "Select LastNo from DocTypeM Where DocTypeId =1";
                            //DataTable dt2 = db.GetDataWithoutParam(CommandType.Text, Query, conn);
                            //string N = dt2.Rows[0]["LastNo"].ToString();
                            //GeneralParameters.UpdateDocNo(1, Convert.ToDecimal(N + 1), conn);
                            MessageBox.Show("Order entry Saved successfully", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            tabControlOrderEntry.TabPages.Insert(1, this.tabAssortment);
                            tabControlOrderEntry.TabPages.Insert(2, this.tabPageSize);
                            tabControlOrderEntry.TabPages.Insert(3, this.tabCombo);
                            tabControlOrderEntry.TabPages.Insert(4, this.tabSizeMatrix);
                            tabControlOrderEntry.TabPages.Insert(5, this.tabPlanning);
                            tabControlOrderEntry.TabPages.Insert(6, this.tabPrice);
                            tabControlOrderEntry.TabPages.Insert(7, this.tabLogistics);
                            tabControlOrderEntry.TabPages.Insert(8, this.tabFabric);
                            btnSave.Text = "Update";
                            DataTable dt = GetorderStyle(OrderUid);
                            LoadId = 1;
                            txtDocNo.Tag = OrderUid;
                            CmbStyle.DisplayMember = "StyleName";
                            CmbStyle.ValueMember = "Uid";
                            CmbStyle.DataSource = dt;

                            cmbStylePrice.DisplayMember = "StyleName";
                            cmbStylePrice.ValueMember = "Uid";
                            cmbStylePrice.DataSource = dt;

                            cmbQtyStyle.DisplayMember = "StyleName";
                            cmbQtyStyle.ValueMember = "Uid";
                            cmbQtyStyle.DataSource = dt;

                            cmbMatrixStyle.DisplayMember = "StyleName";
                            cmbMatrixStyle.ValueMember = "Uid";
                            cmbMatrixStyle.DataSource = dt;

                            cmbStyleAssort.DisplayMember = "StyleName";
                            cmbStyleAssort.ValueMember = "Uid";
                            cmbStyleAssort.DataSource = dt;

                            CmbPlanningStyle.DisplayMember = "StyleName";
                            CmbPlanningStyle.ValueMember = "Uid";
                            CmbPlanningStyle.DataSource = dt;

                            CmbStyleAssort_SelectedIndexChanged(sender, e);
                            CmbStyle_SelectedIndexChanged(sender, e);
                            LoadId = 0;
                        }
                        else
                        {
                            if (txtDepartment.Text == string.Empty)
                            {
                                MessageBox.Show("Department Can't Empty !", "Iformation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                txtDepartment.Focus();
                                return;
                            }
                            if (txtAgent.Text == string.Empty)
                            {
                                MessageBox.Show("Agent Can't Empty !", "Iformation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                txtAgent.Focus();
                                return;
                            }
                            if (txtShipType.Text == string.Empty)
                            {
                                MessageBox.Show("ShipType Can't Empty !", "Iformation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                txtShipType.Focus();
                                return;
                            }
                            if (txtShiftMode.Text == string.Empty)
                            {
                                MessageBox.Show("Ship Mode Can't Empty !", "Iformation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                txtShiftMode.Focus();
                                return;
                            }
                            if (txtPayMode.Text == string.Empty)
                            {
                                MessageBox.Show("Pay Mode Can't Empty !", "Iformation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                txtShiftMode.Focus();
                                return;
                            }
                            if (txtCountry.Text == string.Empty)
                            {
                                MessageBox.Show("Country Can't Empty !", "Iformation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                txtCountry.Focus();
                                return;
                            }
                            if (txtCurrency.Text == string.Empty)
                            {
                                MessageBox.Show("Currency Can't Empty !", "Iformation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                txtCurrency.Focus();
                                return;
                            }
                            if (txtAvgRate.Text == string.Empty)
                            {
                                MessageBox.Show("Avg Rate Can't Empty !", "Iformation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                txtAvgRate.Focus();
                                return;
                            }
                            if (txtCrossValue.Text == string.Empty)
                            {
                                MessageBox.Show("Cross value Can't Empty !", "Iformation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                txtCrossValue.Focus();
                                return;
                            }
                            if (txtExRate.Text == string.Empty)
                            {
                                MessageBox.Show("Exchange rate Can't Empty !", "Iformation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                txtExRate.Focus();
                                return;
                            }
                            if (txtPayTerms.Text == string.Empty)
                            {
                                MessageBox.Show("Payment Can't Empty !", "Iformation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                txtPayTerms.Focus();
                                return;
                            }
                            if (dtpRecivedDt.Text == string.Empty)
                            {
                                dtpRecivedDt.Text = DateTime.Now.Date.ToString();
                            }
                            SqlParameter[] parameters = {
                                new SqlParameter("@Uid",txtDocNo.Tag),
                                new SqlParameter("@DocNo",txtDocNo.Text),
                                new SqlParameter("@DocDate",Convert.ToDateTime(dtpDocDate.Text)),
                                new SqlParameter("@OrderNo",txtOrderNo.Text),
                                new SqlParameter("@OrderDt",Convert.ToDateTime(dtpOrderDate.Text)),
                                new SqlParameter("@CustomerUid",txtCustomer.Tag),
                                new SqlParameter("@SeasonUid",cmbSeason.SelectedValue),
                                new SqlParameter("@DeliveryDt",Convert.ToDateTime(dtpDeliveryDate.Text)),
                                new SqlParameter("@MerchindiserUid",txtMerchindiser.Tag),
                                new SqlParameter("@StatusUid",cmbSts.SelectedValue),
                                new SqlParameter("@TypeUid",cmbOrderType.SelectedValue),
                                new SqlParameter("@DeptUid",txtDepartment.Tag),
                                new SqlParameter("@AgentUid",txtAgent.Tag),
                                new SqlParameter("@ReceivedDt",Convert.ToDateTime(dtpRecivedDt.Text)),
                                new SqlParameter("@ShipTypeUid",txtShipType.Tag),
                                new SqlParameter("@ShiftModeUid",txtShiftMode.Tag),
                                new SqlParameter("@PyModeUid",txtPayMode.Tag),
                                new SqlParameter("@PaymentTerms",txtPayTerms.Text),
                                new SqlParameter("@ContactPerson",txtContact.Text),
                                new SqlParameter("@CurrencyUid",txtCurrency.Tag),
                                new SqlParameter("@CountryUid",txtCountry.Tag),
                                new SqlParameter("@ExRate",txtExRate.Text),
                                new SqlParameter("@ReturnUid",SqlDbType.Decimal),
                                new SqlParameter("@SeasonYear",cmbYear.Text),
                                new SqlParameter("@AvgRate",Convert.ToDecimal(txtAvgRate.Text).ToString("0.00")),
                                new SqlParameter("@GrossValue",Convert.ToDecimal(txtCrossValue.Text).ToString("0.00")),
                                new SqlParameter("@ReceivedBy",txtReceivedBy.Text),
                                new SqlParameter("@DocSts","Entry Completed"),
                                new SqlParameter("@StyleDesc",txtStyleDesc.Text)
                            };
                            parameters[22].Direction = ParameterDirection.Output;
                            OrderUid = db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_OrderM", parameters, conn, 22);
                            MessageBox.Show("Saved successfully", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    else
                    {
                        MessageBox.Show("Complete the Styles", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                }
                else
                {
                    MessageBox.Show("Enter the required data's", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtDocNo.Focus();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected DataTable GetorderAssort(decimal orderId)
        {
            DataTable dataTable = new DataTable();
            try
            {
                SqlParameter[] parameters = { new SqlParameter("@OrderMUid", orderId) };
                dataTable = db.GetDataWithParam(CommandType.StoredProcedure, "SP_GetAssortmentForEdit", parameters, conn);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            return dataTable;
        }

        protected DataTable GetorderMFabric(decimal orderId)
        {
            DataTable dataTable = new DataTable();
            try
            {
                SqlParameter[] parameters = { new SqlParameter("@OrderMuid", orderId) };
                dataTable = db.GetDataWithParam(CommandType.StoredProcedure, "Proc_Edit_OrderMFabricInOrderEntry", parameters, conn);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            return dataTable;
        }

        protected DataTable GetorderAssortByStyle(decimal orderId, decimal StyleUid)
        {
            DataTable dataTable = new DataTable();
            try
            {
                SqlParameter[] parameters = { new SqlParameter("@OrderMUid", orderId) };
                DataTable dt = db.GetDataWithParam(CommandType.StoredProcedure, "SP_GetAssortmentForEdit", parameters, conn);
                dataTable = dt.Clone();
                dataTable = dt.Select("OrdrStyleUid = " + StyleUid + "").CopyToDataTable();
            }
            catch (Exception)
            {
            }
            return dataTable;
        }

        protected DataTable GetorderStyle(decimal orderId)
        {
            DataTable dataTable = new DataTable();
            try
            {
                SqlParameter[] parameters = { new SqlParameter("@OrderMUid", orderId) };
                dataTable = db.GetDataWithParam(CommandType.StoredProcedure, "SP_GetOrderMStyles", parameters, conn);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            return dataTable;
        }
        protected void LoadUOmCombo()
        {
            try
            {
                DataTable dt = LoadUOm();
                DataTable dtOrder = dt.Select("GeneralName in('PCs','SET','Pack')").CopyToDataTable();
                DataTable dtPlan = dt.Select("GeneralName in('PCs','SET','Pack')").CopyToDataTable();
                cmbOrderUOm.DataSource = null;
                cmbOrderUOm.DisplayMember = "GeneralName";
                cmbOrderUOm.ValueMember = "GUid";
                cmbOrderUOm.DataSource = dtOrder;

                CmbPlanUom.DataSource = null;
                CmbPlanUom.DisplayMember = "GeneralName";
                CmbPlanUom.ValueMember = "GUid";
                CmbPlanUom.DataSource = dtPlan;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected DataTable LoadUOm()
        {
            DataTable dt = new DataTable();
            try
            {
                string Query = "Select* from GeneralM Where TypeMUid = 7";
                dt = db.GetDataWithoutParam(CommandType.Text, Query, conn);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            return dt;
        }

        private void CmbStyleCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (LoadId == 0)
                {
                    DataGridCombo.Rows.Clear();
                }


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void BtnAdd_DropDowItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            try
            {
                if (e.ClickedItem.Text == "Close")
                {
                    this.Close();
                }
                else if (e.ClickedItem.Text == "Edit")
                {
                    StyleEdit = 1;
                    EditId = 1;
                    SelectId = 1;
                    if (SfdDataGridOrderEntry.SelectedIndex == -1)
                    {
                        MessageBox.Show("Select a row", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                    var selectedItem = SfdDataGridOrderEntry.SelectedItems[0];
                    var dataRow = (selectedItem as DataRowView).Row;
                    decimal CUid = Convert.ToDecimal(dataRow["UID"].ToString());
                    GetOrderMEdit(CUid);
                    CmbStyleAssort_SelectedIndexChanged(sender, e);
                    CmbStyle_SelectedIndexChanged(sender, e);
                    CmbMatrixStyle_SelectedIndexChanged(sender, e);
                    CmbQtyStyle_SelectedIndexChanged(sender, e);
                    CmbPlanningStyleCombo_SelectedIndexChanged(sender, e);
                    CmbPriceType_SelectedIndexChanged(sender, e);
                    TxtAvgRate_TextChanged(sender, e);
                    SelectId = 0;
                }
            }
            catch (Exception)
            {
                //MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                //return;
            }
        }



        protected void GetOrderMEdit(decimal OrderMuid)
        {
            try
            {
                tabControlOrderEntry.TabPages.Remove(this.tabPageSize);
                tabControlOrderEntry.TabPages.Remove(this.tabSizeMatrix);
                tabControlOrderEntry.TabPages.Remove(this.tabAssortment);
                tabControlOrderEntry.TabPages.Remove(this.tabCombo);
                tabControlOrderEntry.TabPages.Remove(this.tabPlanning);
                tabControlOrderEntry.TabPages.Remove(this.tabPrice);
                tabControlOrderEntry.TabPages.Remove(this.tabLogistics);
                tabControlOrderEntry.TabPages.Remove(this.tabFabric);
                DataGridStyle.Rows.Clear();
                DataGridStyleSize.Rows.Clear();
                DataGridStyleSizeCombo.Rows.Clear();
                DataGridSizeMatrix.Rows.Clear();
                DataGridAssort.Rows.Clear();
                DataGridCombo.Rows.Clear();
                DataGridStylePrice.Rows.Clear();
                DataGridCharges.Rows.Clear();
                DataGridDiscounts.Rows.Clear();
                DataGridFabricDet.Rows.Clear();
                var selectedItem = SfdDataGridOrderEntry.SelectedItems[0];
                var dataRow = (selectedItem as DataRowView).Row;
                decimal CUid = Convert.ToDecimal(OrderMuid);
                SqlParameter[] parameters = { new SqlParameter("@OrderUid", CUid) };
                DataSet ds = db.GetDataWithParamMultiple(CommandType.StoredProcedure, "SP_GetOrderMEdit", parameters, conn);
                DataTable dt = ds.Tables[0];
                DataTable dtStyle = ds.Tables[1];
                DataTable dtFabric = ds.Tables[2];
                FillTextboxs(dt);
                LoaddataGridStylebyId(dtStyle);
                LoadFabricGrid(dtFabric);
                grFront.Visible = false;
                grBack.Visible = true;
                btnSave.Text = "Update";
                tabControlOrderEntry.TabPages.Insert(1, this.tabAssortment);
                tabControlOrderEntry.TabPages.Insert(2, this.tabPageSize);
                tabControlOrderEntry.TabPages.Insert(3, this.tabCombo);
                tabControlOrderEntry.TabPages.Insert(4, this.tabSizeMatrix);
                tabControlOrderEntry.TabPages.Insert(5, this.tabPlanning);
                tabControlOrderEntry.TabPages.Insert(6, this.tabPrice);
                tabControlOrderEntry.TabPages.Insert(7, this.tabLogistics);
                tabControlOrderEntry.TabPages.Insert(8, this.tabFabric);
                LoadId = 1;
                DataTable dtq = GetorderStyle(CUid);
                DataTable dtAssort = GetorderAssort(CUid);
                DataTable dataTable = GetorderMFabric(CUid);
                string Query = "Select Uid,ComboUid from StyleCombo Where OrderMUid =" + txtDocNo.Tag + "";
                DataTable dtCombo = db.GetDataWithoutParam(CommandType.Text, Query, conn);
                CmbStyle.DisplayMember = "StyleName";
                CmbStyle.ValueMember = "Uid";
                CmbStyle.DataSource = dtq;

                cmbAssort.DisplayMember = "AssortmentType";
                cmbAssort.ValueMember = "AssortUid";
                cmbAssort.DataSource = dtAssort;

                cmbStylePrice.DisplayMember = "StyleName";
                cmbStylePrice.ValueMember = "Uid";
                cmbStylePrice.DataSource = dtq;

                cmbStyleAssort.DisplayMember = "StyleName";
                cmbStyleAssort.ValueMember = "Uid";
                cmbStyleAssort.DataSource = dtq;

                cmbQtyStyle.DisplayMember = "StyleName";
                cmbQtyStyle.ValueMember = "Uid";
                cmbQtyStyle.DataSource = dtq;

                cmbMatrixStyle.DisplayMember = "StyleName";
                cmbMatrixStyle.ValueMember = "Uid";
                cmbMatrixStyle.DataSource = dtq;

                CmbPlanningStyle.DisplayMember = "StyleName";
                CmbPlanningStyle.ValueMember = "Uid";
                CmbPlanningStyle.DataSource = dtq;

                CmbPlanningStyleCombo.DisplayMember = "ComboUid";
                CmbPlanningStyleCombo.ValueMember = "Uid";
                CmbPlanningStyleCombo.DataSource = dtCombo;
                LoadOrderFabricEdit(dataTable);
                LoadId = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected void LoadFabricGrid(DataTable dataTable)
        {
            if (dataTable.Rows.Count > 0)
            {
                DataGridFabricDet.Rows.Clear();
                for (int i = 0; i < dataTable.Rows.Count; i++)
                {
                    DataGridViewRow row = (DataGridViewRow)DataGridFabricDet.Rows[0].Clone();
                    row.Cells[0].Value = i + 1;
                    row.Cells[1].Value = dataTable.Rows[i]["GeneralName"].ToString();
                    row.Cells[2].Value = dataTable.Rows[i]["GSM"].ToString();
                    row.Cells[3].Value = dataTable.Rows[i]["Talarance"].ToString();
                    row.Cells[4].Value = dataTable.Rows[i]["FabStrcuUid"].ToString();
                    row.Cells[5].Value = dataTable.Rows[i]["ID"].ToString();
                    DataGridFabricDet.Rows.Add(row);
                }
            }
        }

        private void LoadOrderFabricEdit(DataTable dataTable)
        {
            try
            {
                if (dataTable.Rows.Count > 0)
                {
                    DataGridFabricDet.Rows.Clear();
                    for (int i = 0; i < dataTable.Rows.Count; i++)
                    {
                        DataGridViewRow row = (DataGridViewRow)DataGridFabricDet.Rows[0].Clone();
                        row.Cells[0].Value = i + 1;
                        row.Cells[1].Value = dataTable.Rows[i]["StuructureType"].ToString();
                        row.Cells[2].Value = dataTable.Rows[i]["GSM"].ToString();
                        row.Cells[3].Value = dataTable.Rows[i]["Talarance"].ToString();
                        row.Cells[4].Value = dataTable.Rows[i]["FabStrcuUid"].ToString();
                        row.Cells[5].Value = dataTable.Rows[i]["ID"].ToString();
                        DataGridFabricDet.Rows.Add(row);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void LoaddataGridStylebyId(DataTable dtStyle)
        {
            try
            {
                if (dtStyle.Rows.Count > 0)
                {
                    OrderQty = 0;
                    for (int i = 0; i < dtStyle.Rows.Count; i++)
                    {
                        DataGridViewRow row = (DataGridViewRow)DataGridStyle.Rows[0].Clone();
                        row.Cells[0].Value = i + 1;
                        row.Cells[1].Value = dtStyle.Rows[i]["StyleName"].ToString();
                        row.Cells[2].Value = dtStyle.Rows[i]["Styledescription"].ToString();
                        row.Cells[3].Value = dtStyle.Rows[i]["OrderUom"].ToString();
                        row.Cells[4].Value = dtStyle.Rows[i]["PlanUom"].ToString();
                        row.Cells[5].Value = dtStyle.Rows[i]["StyleQty"].ToString();
                        row.Cells[6].Value = dtStyle.Rows[i]["OrderUomUid"].ToString();
                        row.Cells[7].Value = dtStyle.Rows[i]["PlanUomUid"].ToString();
                        row.Cells[8].Value = dtStyle.Rows[i]["Uid"].ToString();
                        DataGridStyle.Rows.Add(row);
                        OrderQty += Convert.ToDecimal(dtStyle.Rows[i]["StyleQty"].ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void FillTextboxs(DataTable dt)
        {
            try
            {
                SelectId = 1;
                txtDocNo.Text = dt.Rows[0]["DocNo"].ToString();
                txtDocNo.Tag = dt.Rows[0]["Uid"].ToString();
                dtpDocDate.Text = Convert.ToDateTime(dt.Rows[0]["DocDate"].ToString()).ToString();
                txtOrderNo.Text = dt.Rows[0]["DocNo"].ToString();
                dtpOrderDate.Text = Convert.ToDateTime(dt.Rows[0]["OrderDt"].ToString()).ToString();
                txtCustomer.Text = dt.Rows[0]["CompanyName"].ToString();
                txtCustomer.Tag = dt.Rows[0]["CustomerUid"].ToString();
                cmbSeason.SelectedValue = dt.Rows[0]["SeasonUid"].ToString();
                dtpDeliveryDate.Text = Convert.ToDateTime(dt.Rows[0]["DeliveryDt"].ToString()).ToString();
                txtMerchindiser.Text = dt.Rows[0]["Merchindiser"].ToString();
                txtMerchindiser.Tag = dt.Rows[0]["MerchindiserUid"].ToString();
                cmbSts.SelectedValue = dt.Rows[0]["StatusUid"].ToString();
                cmbOrderType.SelectedValue = dt.Rows[0]["TypeUid"].ToString();
                cmbYear.Text = dt.Rows[0]["TypeUid"].ToString();
                txtDepartment.Text = dt.Rows[0]["Dept"].ToString();
                txtDepartment.Tag = dt.Rows[0]["DeptUid"].ToString();
                txtAgent.Text = dt.Rows[0]["Agent"].ToString();
                txtAgent.Tag = dt.Rows[0]["AgentUid"].ToString();
                if (dt.Rows[0]["ReceivedDt"].ToString() != "")
                {
                    dtpRecivedDt.Text = Convert.ToDateTime(dt.Rows[0]["ReceivedDt"].ToString()).ToString();
                }
                txtShipType.Text = dt.Rows[0]["ShipType"].ToString();
                txtShipType.Tag = dt.Rows[0]["ShipTypeUid"].ToString();
                txtShiftMode.Text = dt.Rows[0]["ShiftMode"].ToString();
                txtShiftMode.Tag = dt.Rows[0]["ShiftModeUid"].ToString();
                txtPayMode.Text = dt.Rows[0]["PayMode"].ToString();
                txtPayMode.Tag = dt.Rows[0]["PyModeUid"].ToString();
                txtPayTerms.Text = dt.Rows[0]["PaymentTerms"].ToString();
                txtContact.Text = dt.Rows[0]["ContactPerson"].ToString();
                txtCurrency.Text = dt.Rows[0]["Currency"].ToString();
                txtCurrency.Tag = dt.Rows[0]["CurrencyUid"].ToString();
                txtCountry.Text = dt.Rows[0]["Country"].ToString();
                txtCountry.Tag = dt.Rows[0]["CountryUid"].ToString();
                txtExRate.Text = dt.Rows[0]["ExRate"].ToString();
                txtAvgRate.Text = dt.Rows[0]["AvgRate"].ToString();
                txtStyleDesc.Text = dt.Rows[0]["StyleDesc"].ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void CmbStyle_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (LoadId == 0)
                {
                    DataGridStyleSizeCombo.Rows.Clear();
                    if (CmbStyle.SelectedIndex != -1)
                    {
                        SqlParameter[] parameters = { new SqlParameter("@OrderStyleUid", CmbStyle.SelectedValue), new SqlParameter("@OrderMUid", txtDocNo.Tag) };
                        DataSet ds = db.GetDataWithParamMultiple(CommandType.StoredProcedure, "SP_GetStyleSizeforCombo", parameters, conn);
                        DataTable dtCombo = ds.Tables[1];
                        if (dtCombo.Rows.Count > 0)
                        {
                            for (int i = 0; i < dtCombo.Rows.Count; i++)
                            {
                                DataGridViewRow row = (DataGridViewRow)DataGridStyleSizeCombo.Rows[0].Clone();
                                row.Cells[0].Value = dtCombo.Rows[i]["Combo"].ToString();
                                row.Cells[1].Value = dtCombo.Rows[i]["ComboQty"].ToString();
                                row.Cells[2].Value = dtCombo.Rows[i]["OrderMUid"].ToString();
                                DataGridStyleSizeCombo.Rows.Add(row);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected void LoadOrders()
        {
            try
            {
                DataTable dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "SP_GetOrderM", conn);
                SfdDataGridOrderEntry.DataSource = null;
                SfdDataGridOrderEntry.DataSource = dt;
                SfdDataGridOrderEntry.Columns[1].Width = 80;
                SfdDataGridOrderEntry.Columns[2].Width = 80;
                SfdDataGridOrderEntry.Columns[3].Width = 125;
                SfdDataGridOrderEntry.Columns[4].Width = 85;
                SfdDataGridOrderEntry.Columns[5].Width = 85;
                SfdDataGridOrderEntry.Columns[6].Width = 180;
                SfdDataGridOrderEntry.Columns[9].Width = 80;
                SfdDataGridOrderEntry.Columns[10].Width = 75;
                SfdDataGridOrderEntry.Columns[8].Width = 120;
                SfdDataGridOrderEntry.Columns[12].Width = 80;
                SfdDataGridOrderEntry.Columns[14].Width = 90;
                SfdDataGridOrderEntry.Columns[16].Width = 105;
                SfdDataGridOrderEntry.Columns[0].Visible = false;
                SfdDataGridOrderEntry.Columns[7].Visible = false;
                SfdDataGridOrderEntry.Columns[9].Visible = false;
                SfdDataGridOrderEntry.Columns[11].Visible = false;
                SfdDataGridOrderEntry.Columns[13].Visible = false;
                SfdDataGridOrderEntry.Columns[15].Visible = false;
                SfdDataGridOrderEntry.Columns[14].Visible = false;
                SfdDataGridOrderEntry.Columns[17].Width = 90;
                dtOrdersGrid = dt;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        public void ClearTextBoxes(bool searchRecursively = true)
        {
            void clearTextBoxes(Control.ControlCollection controls, bool searchChildren)
            {
                SelectId = 1;
                LoadId = 1;
                foreach (Control c in controls)
                {
                    TextBox txt = c as TextBox;
                    txt?.Clear();
                    if (searchChildren && c.HasChildren)
                        clearTextBoxes(c.Controls, true);
                }
            }
            clearTextBoxes(this.Controls, searchRecursively);
            SelectId = 0;
            LoadId = 0;
        }

        protected bool CheckAssortGridValue()
        {
            bool entryFound = false;
            try
            {
                foreach (DataGridViewRow row in DataGridAssort.Rows)
                {
                    object val2 = row.Cells[1].Value;
                    if (val2 != null && val2.ToString() == txtRefNo.Text)
                    {
                        entryFound = true;
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            return entryFound;
        }

        private void BtnAssortOk_Click(object sender, EventArgs e)
        {
            try
            {
                if (cmbCountry.SelectedIndex == -1)
                {
                    MessageBox.Show("Select Country !", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    cmbCountry.Focus();
                    return;
                }
                else if (txtRefNo.Text == string.Empty)
                {
                    MessageBox.Show("RefNo can't Empty !", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtRefNo.Focus();
                    return;
                }
                else if (txtAssortmentType.Text == string.Empty)
                {
                    MessageBox.Show("Assortment Type can't Empty !", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtAssortmentType.Focus();
                    return;
                }
                else if (txtAssortQuantity.Text == string.Empty)
                {
                    MessageBox.Show("Quantity can't Empty !", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtAssortQuantity.Focus();
                    return;
                }
                else
                {
                    bool entryfound = CheckAssortGridValue();
                    if (entryfound == false)
                    {
                        if (txtMCBox.Text == string.Empty)
                        {
                            txtMCBox.Text = "0";
                        }
                        DataGridViewRow row = (DataGridViewRow)DataGridAssort.Rows[0].Clone();
                        row.Cells[0].Value = cmbCountry.Text;
                        row.Cells[1].Value = txtRefNo.Text;
                        row.Cells[2].Value = txtAssortmentType.Text;
                        row.Cells[3].Value = txtAssortQuantity.Text;
                        row.Cells[4].Value = Convert.ToDateTime(dtpAssortDelDt.Text).ToString("dd-MMM-yyyy");
                        row.Cells[5].Value = Convert.ToDateTime(dtpAssortShipDt.Text).ToString("dd-MMM-yyyy");
                        row.Cells[6].Value = txtPortofDelivery.Text;
                        row.Cells[7].Value = txtFinalDistination.Text;
                        row.Cells[8].Value = cmbCountry.SelectedValue;
                        row.Cells[9].Value = "0";
                        row.Cells[10].Value = txtMCBox.Text;
                        DataGridAssort.Rows.Add(row);
                        SqlParameter[] parameters = {
                            new SqlParameter("@AssortUid","0"),
                            new SqlParameter("@OrderMuid",txtDocNo.Tag),
                            new SqlParameter("@CountryUid",cmbCountry.SelectedValue),
                            new SqlParameter("@RefNo",txtRefNo.Text),
                            new SqlParameter("@AssortmentType",txtAssortmentType.Text),
                            new SqlParameter("@AssortQuantity",txtAssortQuantity.Text),
                            new SqlParameter("@AssortDeliveryDt",Convert.ToDateTime(dtpAssortDelDt.Text).ToString("dd-MMM-yyyy")),
                            new SqlParameter("@AssortShipmentDt",Convert.ToDateTime(dtpAssortShipDt.Text).ToString("dd-MMM-yyyy")),
                            new SqlParameter("@PartofDel",txtPortofDelivery.Text),
                            new SqlParameter("@FinalDes",txtFinalDistination.Text),
                            new SqlParameter("@OrdrStyleUid",cmbStyleAssort.SelectedValue),
                            new SqlParameter("@MCottonBox",txtMCBox.Text),
                            new SqlParameter("@RefNoOld",txtRefNo.Tag),
                            new SqlParameter("@AssortmentTypeOld",txtAssortmentType.Tag)
                        };
                        db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_OrderMAssort", parameters, conn);
                        txtRefNo.Text = string.Empty;
                        txtAssortmentType.Text = string.Empty;
                        txtAssortQuantity.Text = string.Empty;
                        txtPortofDelivery.Text = string.Empty;
                        txtMCBox.Text = string.Empty;
                        txtFinalDistination.Text = string.Empty;
                        txtRefNo.Focus();
                    }
                    else
                    {
                        MessageBox.Show("Ref No already Found", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        txtRefNo.Focus();
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void CmbStyleAssort_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (LoadId == 0 && cmbStyleAssort.SelectedIndex != -1)
                {
                    DataTable dtAssort = GetorderAssortByStyle(Convert.ToDecimal(txtDocNo.Tag), Convert.ToDecimal(cmbStyleAssort.SelectedValue));
                    DataGridAssort.Rows.Clear();
                    if (dtAssort.Rows.Count > 0)
                    {
                        for (int i = 0; i < dtAssort.Rows.Count; i++)
                        {
                            DataGridViewRow row = (DataGridViewRow)DataGridAssort.Rows[0].Clone();
                            row.Cells[0].Value = dtAssort.Rows[i]["Country"].ToString();
                            row.Cells[1].Value = dtAssort.Rows[i]["RefNo"].ToString();
                            row.Cells[2].Value = dtAssort.Rows[i]["AssortmentType"].ToString();
                            row.Cells[3].Value = dtAssort.Rows[i]["AssortQuantity"].ToString();
                            row.Cells[4].Value = Convert.ToDateTime(dtAssort.Rows[i]["AssortDeliveryDt"].ToString()).ToString("dd-MMM-yyyy");
                            row.Cells[5].Value = Convert.ToDateTime(dtAssort.Rows[i]["AssortShipmentDt"].ToString()).ToString("dd-MMM-yyyy");
                            row.Cells[6].Value = dtAssort.Rows[i]["PartofDel"].ToString();
                            row.Cells[7].Value = dtAssort.Rows[i]["FinalDes"].ToString();
                            row.Cells[8].Value = dtAssort.Rows[i]["CountryUid"].ToString();
                            row.Cells[9].Value = dtAssort.Rows[i]["AssortUid"].ToString();
                            row.Cells[10].Value = dtAssort.Rows[i]["MCottonBox"].ToString();
                            DataGridAssort.Rows.Add(row);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void BtnComboOk_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtCombo.Text != string.Empty)
                {
                    txtComboQty.Text = "0";
                    DataGridViewRow row = (DataGridViewRow)DataGridStyleSizeCombo.Rows[0].Clone();
                    row.Cells[0].Value = txtCombo.Text;
                    row.Cells[1].Value = txtComboQty.Text;
                    row.Cells[2].Value = "0";
                    DataGridStyleSizeCombo.Rows.Add(row);
                    SqlParameter[] parameters = {
                        new SqlParameter("@Uid","0"),
                        new SqlParameter("@OrderMUid",txtDocNo.Tag),
                        new SqlParameter("@ComboUid",txtCombo.Text),
                        new SqlParameter("@StyleUid",CmbStyle.SelectedValue),
                        new SqlParameter("@ComboQty",txtComboQty.Text),
                        new SqlParameter("@ComboOld",txtCombo.Tag),
                    };
                    db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_StyleCombo", parameters, conn);
                    txtCombo.Text = string.Empty;
                    txtComboQty.Text = "0";
                    txtCombo.Focus();
                }
                else
                {
                    MessageBox.Show("Combo can't Empty !", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtCombo.Focus();
                    return;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void CmbQtyStyle_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (cmbAssort.SelectedIndex != -1 && LoadId == 0)
                {
                    SqlParameter[] parameters = { new SqlParameter("@OrderStyleUid", cmbQtyStyle.SelectedValue), new SqlParameter("@OrderMUid", txtDocNo.Tag) };
                    DataTable dt = db.GetDataWithParam(CommandType.StoredProcedure, "SP_GetStyleSizeforCombo", parameters, conn);
                    if (dt.Rows.Count > 0)
                    {
                        DataGridCombo.Columns.Clear();
                        DataGridCombo.AutoGenerateColumns = false;
                        int Column1 = dt.Rows.Count + 5;
                        DataGridCombo.ColumnCount = Column1;
                        DataGridCombo.Columns[0].Name = "SlNo";
                        DataGridCombo.Columns[0].HeaderText = "SlNo";
                        DataGridCombo.Columns[0].Width = 80;

                        DataGridCombo.Columns[1].Name = "Combo";
                        DataGridCombo.Columns[1].HeaderText = "Combo";
                        DataGridCombo.Columns[1].Width = 200;
                        int j = 1;
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            j += 1;
                            DataGridCombo.Columns[j].Name = dt.Rows[i]["SizeName"].ToString();
                            DataGridCombo.Columns[j].HeaderText = dt.Rows[i]["SizeName"].ToString();
                            DataGridCombo.Columns[j].Width = 50;
                        }
                        DataGridCombo.Columns[j + 1].Name = "Total";
                        DataGridCombo.Columns[j + 1].HeaderText = "Total";
                        DataGridCombo.Columns[j + 1].Width = 100;

                        DataGridCombo.Columns[j + 2].Name = "ComboUid";
                        DataGridCombo.Columns[j + 2].HeaderText = "ComboUid";
                        DataGridCombo.Columns[j + 2].Visible = false;

                        DataGridCombo.Columns[j + 3].Name = "CottonBox";
                        DataGridCombo.Columns[j + 3].HeaderText = "CottonBox";
                    }
                    SqlParameter[] parameters1 = { new SqlParameter("@OrderStyleUid", cmbQtyStyle.SelectedValue) };
                    DataTable dt1 = db.GetDataWithParam(CommandType.StoredProcedure, "SP_StyleSizeCombo", parameters1, conn);
                    int Count = DataGridCombo.Columns.Count;
                    DataGridCombo.Rows.Clear();
                    for (int i = 0; i < dt1.Rows.Count; i++)
                    {
                        DataGridViewRow row = (DataGridViewRow)DataGridCombo.Rows[0].Clone();
                        row.Cells[0].Value = i + 1;
                        row.Cells[1].Value = dt1.Rows[i]["Combo"].ToString();
                        row.Cells[Count - 2].Value = dt1.Rows[i]["ComboUid"].ToString();
                        DataGridCombo.Rows.Add(row);
                    }
                    LoadId = 1;
                    SqlParameter[] parameters2 = { new SqlParameter("@AssortUid", cmbAssort.SelectedValue) };
                    int cnt = dt.Rows.Count + 5;
                    DataTable dt2 = db.GetDataWithParam(CommandType.StoredProcedure, "SP_GetSizeQty", parameters2, conn);
                    for (int i = 0; i < dt2.Rows.Count; i++)
                    {
                        string Name = dt2.Rows[i]["SizeName"].ToString();
                        string ComUid = dt2.Rows[i]["ComboId"].ToString();
                        for (int j = 0; j < DataGridCombo.Columns.Count; j++)
                        {
                            string ColName = DataGridCombo.Columns[j].Name.ToString();
                            if (Name == ColName)
                            {
                                for (int k = 0; k < DataGridCombo.Rows.Count - 1; k++)
                                {
                                    string comboid = DataGridCombo.Rows[k].Cells["ComboUid"].Value.ToString();
                                    if (comboid == ComUid)
                                    {
                                        DataTable dataTable1 = dt2.Select("ComboId=" + comboid + "").CopyToDataTable();
                                        DataGridCombo.Rows[k].Cells[Name].Value = dt2.Rows[i]["Qty"].ToString();
                                        DataGridCombo.Rows[k].Cells["Total"].Value = dataTable1.Compute("SUM(Qty)", string.Empty).ToString();
                                        DataGridCombo.Rows[k].Cells["CottonBox"].Value = dt2.Rows[i]["CBoxQty"].ToString();
                                    }
                                }
                            }
                        }
                    }
                    LoadId = 0;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void DataGridCombo_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (LoadId == 0)
                {
                    if (DataGridCombo.Columns[e.ColumnIndex].Name != "CottonBox")
                    {
                        if (DataGridCombo.Columns[e.ColumnIndex].Name != "SlNo" && DataGridCombo.Columns[e.ColumnIndex].Name != "Combo" && DataGridCombo.Columns[e.ColumnIndex].Name != "ComboUid")
                        {
                            decimal cbox;
                            string Size = DataGridCombo.Columns[e.ColumnIndex].Name.ToString();
                            decimal Qty = Convert.ToDecimal(DataGridCombo.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString());
                            decimal ComboUid = Convert.ToDecimal(DataGridCombo.Rows[e.RowIndex].Cells["ComboUid"].Value.ToString());
                            decimal AssortId = Convert.ToDecimal(cmbAssort.SelectedValue);
                            if (DataGridCombo.Rows[e.RowIndex].Cells["CottonBox"].Value == null || DataGridCombo.Rows[e.RowIndex].Cells["CottonBox"].Value.ToString() == "")
                            {
                                cbox = 0;
                            }
                            else
                            {
                                cbox = Convert.ToDecimal(DataGridCombo.Rows[e.RowIndex].Cells["CottonBox"].Value.ToString());
                            }
                            SqlParameter[] sqlParameters = {
                                new SqlParameter("@AssortUid",AssortId),
                                new SqlParameter("@StyleComboUid",ComboUid),
                                new SqlParameter("@OrderMUid",txtDocNo.Tag),
                                new SqlParameter("@StyleSize",Size),
                                new SqlParameter("@Qty",Qty),
                                new SqlParameter("@OrderStyleUid",cmbQtyStyle.SelectedValue),
                                new SqlParameter("@CboxQty",cbox)
                            };
                            db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_OrderMAssortQty", sqlParameters, conn);
                            CmbQtyStyle_SelectedIndexChanged(sender, e);
                        }
                    }
                    else
                    {
                        decimal cbox;
                        decimal ComboUid = Convert.ToDecimal(DataGridCombo.Rows[e.RowIndex].Cells["ComboUid"].Value.ToString());
                        decimal AssortId = Convert.ToDecimal(cmbAssort.SelectedValue);
                        if (DataGridCombo.Rows[e.RowIndex].Cells["CottonBox"].Value == null || DataGridCombo.Rows[e.RowIndex].Cells["CottonBox"].Value.ToString() == "")
                        {
                            cbox = 0;
                        }
                        else
                        {
                            cbox = Convert.ToDecimal(DataGridCombo.Rows[e.RowIndex].Cells["CottonBox"].Value.ToString());
                        }
                        SqlParameter[] sqlParameters = {
                                new SqlParameter("@AssortUid",AssortId),
                                new SqlParameter("@StyleComboUid",ComboUid),
                                new SqlParameter("@CboxQty",cbox)
                            };
                        db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_OrderMAssortCboxQty", sqlParameters, conn);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected void LoadTotaldataGridCombo()
        {
            try
            {
                if (LoadId == 0)
                {
                    for (int i = 0; i < DataGridCombo.Rows.Count; i++)
                    {

                    }
                }

            }
            catch (Exception)
            {

                throw;
            }
        }

        private void CmbMatrixStyle_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (cmbMatrixStyle.SelectedIndex != -1 && LoadId == 0)
                {
                    SqlParameter[] parameters = { new SqlParameter("@OrderStyleUid", cmbMatrixStyle.SelectedValue), new SqlParameter("@OrderMUid", txtDocNo.Tag) };
                    DataTable dt = db.GetDataWithParam(CommandType.StoredProcedure, "SP_GetStyleSizeforCombo", parameters, conn);
                    if (dt.Rows.Count > 0)
                    {
                        DataGridSizeMatrix.Columns.Clear();
                        DataGridSizeMatrix.AutoGenerateColumns = false;
                        int Column1 = dt.Rows.Count + 5;
                        DataGridSizeMatrix.ColumnCount = Column1;
                        DataGridSizeMatrix.Columns[0].Name = "SlNo";
                        DataGridSizeMatrix.Columns[0].HeaderText = "SlNo";
                        DataGridSizeMatrix.Columns[0].Width = 80;

                        DataGridSizeMatrix.Columns[1].Name = "Combo";
                        DataGridSizeMatrix.Columns[1].HeaderText = "Combo";
                        DataGridSizeMatrix.Columns[1].Width = 200;
                        int j = 1;
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            j += 1;
                            DataGridSizeMatrix.Columns[j].Name = dt.Rows[i]["SizeName"].ToString();
                            DataGridSizeMatrix.Columns[j].HeaderText = dt.Rows[i]["SizeName"].ToString();
                            DataGridSizeMatrix.Columns[j].Width = 50;
                        }
                        DataGridSizeMatrix.Columns[j + 1].Name = "Total";
                        DataGridSizeMatrix.Columns[j + 1].HeaderText = "Total";
                        DataGridSizeMatrix.Columns[j + 1].Width = 100;

                        DataGridSizeMatrix.Columns[j + 2].Name = "ComboUid";
                        DataGridSizeMatrix.Columns[j + 2].HeaderText = "ComboUid";
                        DataGridSizeMatrix.Columns[j + 2].Visible = false;

                        DataGridSizeMatrix.Columns[j + 3].Name = "CottonBox";
                        DataGridSizeMatrix.Columns[j + 3].HeaderText = "CottonBox";
                    }
                    SqlParameter[] parameters1 = { new SqlParameter("@OrderStyleUid", cmbMatrixStyle.SelectedValue) };
                    DataTable dt1 = db.GetDataWithParam(CommandType.StoredProcedure, "SP_StyleSizeCombo", parameters1, conn);
                    int Count = DataGridSizeMatrix.Columns.Count;
                    DataGridSizeMatrix.Rows.Clear();
                    for (int i = 0; i < dt1.Rows.Count; i++)
                    {
                        DataGridViewRow row = (DataGridViewRow)DataGridSizeMatrix.Rows[0].Clone();
                        row.Cells[0].Value = i + 1;
                        row.Cells[1].Value = dt1.Rows[i]["Combo"].ToString();
                        row.Cells[Count - 2].Value = dt1.Rows[i]["ComboUid"].ToString();
                        DataGridSizeMatrix.Rows.Add(row);
                    }
                    LoadId = 1;
                    SqlParameter[] parameters2 = { new SqlParameter("@AssortUid", txtDocNo.Tag), new SqlParameter("@StyleUid", cmbMatrixStyle.SelectedValue) };
                    int cnt = dt.Rows.Count + 5;
                    DataTable dt2 = db.GetDataWithParam(CommandType.StoredProcedure, "SP_GetSizeMatrixQtyNew", parameters2, conn);
                    for (int i = 0; i < dt2.Rows.Count; i++)
                    {
                        string Name = dt2.Rows[i]["SizeName"].ToString();
                        string ComUid = dt2.Rows[i]["ComboId"].ToString();
                        for (int j = 0; j < DataGridSizeMatrix.Columns.Count; j++)
                        {
                            string ColName = DataGridSizeMatrix.Columns[j].Name.ToString();
                            if (Name == ColName)
                            {
                                for (int k = 0; k < DataGridSizeMatrix.Rows.Count - 1; k++)
                                {
                                    string comboid = DataGridSizeMatrix.Rows[k].Cells["ComboUid"].Value.ToString();
                                    if (comboid == ComUid)
                                    {
                                        DataTable dataTable1 = dt2.Select("ComboId=" + comboid + "").CopyToDataTable();
                                        DataGridSizeMatrix.Rows[k].Cells[Name].Value = dt2.Rows[i]["Qty"].ToString();
                                        DataGridSizeMatrix.Rows[k].Cells["Total"].Value = dataTable1.Compute("SUM(Qty)", string.Empty).ToString();
                                        DataGridSizeMatrix.Rows[k].Cells["CottonBox"].Value = dt2.Rows[i]["CBoxQty"].ToString();
                                    }
                                }
                            }
                        }
                    }
                    LoadId = 0;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            //try
            //{
            //    try
            //    {
            //        if (cmbMatrixStyle.SelectedIndex != -1 && LoadId == 0)
            //        {
            //            SqlParameter[] parameters = { new SqlParameter("@OrderStyleUid", cmbMatrixStyle.SelectedValue), new SqlParameter("@OrderMUid", txtDocNo.Tag) };
            //            DataTable dt = db.GetDataWithParam(CommandType.StoredProcedure, "SP_GetStyleSizeforCombo", parameters, conn);
            //            if (dt.Rows.Count > 0)
            //            {
            //                DataGridSizeMatrix.Columns.Clear();
            //                DataGridSizeMatrix.AutoGenerateColumns = false;
            //                int Column1 = dt.Rows.Count + 4;
            //                DataGridSizeMatrix.ColumnCount = Column1;
            //                DataGridSizeMatrix.Columns[0].Name = "SlNo";
            //                DataGridSizeMatrix.Columns[0].HeaderText = "SlNo";
            //                DataGridSizeMatrix.Columns[0].Width = 80;

            //                DataGridSizeMatrix.Columns[1].Name = "Combo";
            //                DataGridSizeMatrix.Columns[1].HeaderText = "Combo";
            //                DataGridSizeMatrix.Columns[1].Width = 200;
            //                int j = 1;
            //                for (int i = 0; i < dt.Rows.Count; i++)
            //                {
            //                    j += 1;
            //                    DataGridSizeMatrix.Columns[j].Name = dt.Rows[i]["SizeName"].ToString();
            //                    DataGridSizeMatrix.Columns[j].HeaderText = dt.Rows[i]["SizeName"].ToString();
            //                    DataGridSizeMatrix.Columns[j].Width = 50;
            //                }
            //                DataGridSizeMatrix.Columns[j + 1].Name = "Total";
            //                DataGridSizeMatrix.Columns[j + 1].HeaderText = "Total";
            //                DataGridSizeMatrix.Columns[j + 1].Width = 100;

            //                DataGridSizeMatrix.Columns[j + 2].Name = "ComboUid";
            //                DataGridSizeMatrix.Columns[j + 2].HeaderText = "ComboUid";
            //                DataGridSizeMatrix.Columns[j + 2].Visible = false;
            //            }
            //            SqlParameter[] parameters1 = { new SqlParameter("@OrderStyleUid", cmbMatrixStyle.SelectedValue) };
            //            DataTable dt1 = db.GetDataWithParam(CommandType.StoredProcedure, "SP_StyleSizeCombo", parameters1, conn);
            //            int Count = DataGridSizeMatrix.Columns.Count;
            //            for (int i = 0; i < dt1.Rows.Count; i++)
            //            {
            //                DataGridViewRow row = (DataGridViewRow)DataGridSizeMatrix.Rows[0].Clone();
            //                row.Cells[0].Value = i + 1;
            //                row.Cells[1].Value = dt1.Rows[i]["Combo"].ToString();
            //                row.Cells[Count - 1].Value = dt1.Rows[i]["ComboUid"].ToString();
            //                DataGridSizeMatrix.Rows.Add(row);
            //            }
            //            LoadId = 1;
            //            SqlParameter[] parameters2 = { new SqlParameter("@OrderStyleuid", cmbMatrixStyle.SelectedValue) };
            //            DataTable dt2 = db.GetDataWithParam(CommandType.StoredProcedure, "SP_GetOrderMSizeMatrix", parameters2, conn);
            //            for (int i = 0; i < dt2.Rows.Count; i++)
            //            {
            //                string Name = dt2.Rows[i]["SizeName"].ToString();
            //                string ComUid = dt2.Rows[i]["ComboId"].ToString();
            //                for (int j = 0; j < DataGridSizeMatrix.Columns.Count; j++)
            //                {
            //                    string ColName = DataGridSizeMatrix.Columns[j].Name.ToString();
            //                    if (Name == ColName)
            //                    {
            //                        for (int k = 0; k < DataGridSizeMatrix.Rows.Count - 1; k++)
            //                        {
            //                            string comboid = DataGridSizeMatrix.Rows[k].Cells["ComboUid"].Value.ToString();
            //                            if (comboid == ComUid)
            //                            {
            //                                DataTable dataTable1 = dt2.Select("Comboid=" + comboid + "").CopyToDataTable();
            //                                DataGridSizeMatrix.Rows[k].Cells[Name].Value = dt2.Rows[i]["Qty"].ToString();
            //                                DataGridSizeMatrix.Rows[k].Cells["Total"].Value = dataTable1.Compute("SUM(Qty)", string.Empty).ToString();
            //                            }
            //                        }
            //                    }
            //                }
            //            }
            //            LoadId = 0;
            //        }
            //    }
            //    catch (Exception ex)
            //    {
            //        MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //        return;
            //    }
            //}
            //catch (Exception ex)
            //{
            //    MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //    return;
            //}
        }

        private void DataGridSizeMatrix_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (LoadId == 0)
                {
                    if (DataGridSizeMatrix.Columns[e.ColumnIndex].Name != "SlNo" && DataGridSizeMatrix.Columns[e.ColumnIndex].Name != "Combo" && DataGridSizeMatrix.Columns[e.ColumnIndex].Name != "ComboUid")
                    {
                        string Size = DataGridSizeMatrix.Columns[e.ColumnIndex].Name.ToString();
                        decimal Qty = Convert.ToDecimal(DataGridSizeMatrix.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString());
                        decimal ComboUid = Convert.ToDecimal(DataGridSizeMatrix.Rows[e.RowIndex].Cells["ComboUid"].Value.ToString());
                        SqlParameter[] sqlParameters = {
                            new SqlParameter("@OrderStyleUid",cmbMatrixStyle.SelectedValue),
                            new SqlParameter("@ComboUid",ComboUid),
                            new SqlParameter("@StyleSize",Size),
                            new SqlParameter("@Qty",Qty),
                            new SqlParameter("@OrderMUid",Convert.ToDecimal(txtDocNo.Tag))
                        };
                        db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_OrderMSizeMatrix", sqlParameters, conn);
                        CmbMatrixStyle_SelectedIndexChanged(sender, e);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void DataGridCombo_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                LoadCalculateTotal();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected void LoadCalculateTotal()
        {
            try
            {
                LoadId = 1;
                SqlParameter[] parameters2 = { new SqlParameter("@AssortUid", cmbAssort.SelectedValue) };
                DataTable dt2 = db.GetDataWithParam(CommandType.StoredProcedure, "SP_GetSizeQty", parameters2, conn);
                for (int i = 0; i < dt2.Rows.Count; i++)
                {
                    string Name = dt2.Rows[i]["SizeName"].ToString();
                    string ComUid = dt2.Rows[i]["ComboId"].ToString();
                    for (int j = 0; j < DataGridCombo.Columns.Count; j++)
                    {
                        string ColName = DataGridCombo.Columns[j].Name.ToString();
                        if (Name == ColName)
                        {
                            for (int k = 0; k < DataGridCombo.Rows.Count - 1; k++)
                            {
                                string comboid = DataGridCombo.Rows[k].Cells["ComboUid"].Value.ToString();
                                if (comboid == ComUid)
                                {
                                    DataGridCombo.Rows[k].Cells[Name].Value = dt2.Rows[i]["Qty"].ToString();
                                }
                            }
                        }
                    }
                }
                LoadId = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void BtnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                if (SfdDataGridOrderEntry.SelectedIndex != -1)
                {
                    var selectedItem = SfdDataGridOrderEntry.SelectedItems[0];
                    var dataRow = (selectedItem as DataRowView).Row;
                    decimal CUid = Convert.ToDecimal(dataRow["UID"].ToString());
                    GeneralParameters.ReportUid = CUid;
                    GeneralParameters.ReportName = "OrderSheet";
                    FrmReportviwer frmReportviwer = new FrmReportviwer
                    {
                        MdiParent = this.MdiParent,
                        StartPosition = FormStartPosition.Manual,
                        Location = new Point(200, 80)
                    };
                    frmReportviwer.Show();
                }
                else
                {
                    MessageBox.Show("Select a row", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void TxtStructureType_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dt = db.GetDataWithoutParam(CommandType.Text, "Select * from GeneralM Where Typemuid =11", conn);
                bsStrucType.DataSource = dt;
                FillGrid(dt, 12);
                Point loc = Genclass.FindLocation(txtStructureType);
                grSearch.Location = new Point(loc.X - 10, loc.Y + 10);
                grSearch.Visible = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void TxtStructureType_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bsStrucType.Filter = string.Format("GeneralName LIKE '%{0}%'", txtStructureType.Text);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected void LoadDataGridFabric()
        {
            try
            {
                DataGridFabricDet.DataSource = null;

                DataGridFabricDet.AutoGenerateColumns = false;
                DataGridFabricDet.ColumnCount = 6;
                DataGridFabricDet.Columns[0].Name = "SlNo";
                DataGridFabricDet.Columns[0].HeaderText = "SlNo";
                DataGridFabricDet.Columns[0].Width = 80;

                DataGridFabricDet.Columns[1].Name = "Structure Type";
                DataGridFabricDet.Columns[1].HeaderText = "Structure Type";
                DataGridFabricDet.Columns[1].Width = 200;

                DataGridFabricDet.Columns[2].Name = "GSM";
                DataGridFabricDet.Columns[2].HeaderText = "GSM";

                DataGridFabricDet.Columns[3].Name = "Talarance";
                DataGridFabricDet.Columns[3].HeaderText = "Talarance";

                DataGridFabricDet.Columns[4].Name = "StructUid";
                DataGridFabricDet.Columns[4].HeaderText = "StructUid";
                DataGridFabricDet.Columns[4].Visible = false;

                DataGridFabricDet.Columns[5].Name = "Uid";
                DataGridFabricDet.Columns[5].HeaderText = "Uid";
                DataGridFabricDet.Columns[5].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void BtnFabric_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtStructureType.Text != string.Empty || txtGSM.Text != string.Empty || txtTalarance.Text != string.Empty)
                {
                    if (txtGSM.Tag == null || txtGSM.Tag.ToString() == "")
                    {
                        txtGSM.Tag = 0;
                    }
                    DataGridViewRow row = (DataGridViewRow)DataGridFabricDet.Rows[0].Clone();
                    row.Cells[0].Value = DataGridFabricDet.Rows.Count;
                    row.Cells[1].Value = txtStructureType.Text;
                    row.Cells[2].Value = Convert.ToDecimal(txtGSM.Text);
                    row.Cells[3].Value = Convert.ToDecimal(txtTalarance.Text);
                    row.Cells[4].Value = txtStructureType.Tag;
                    row.Cells[5].Value = "0";
                    DataGridFabricDet.Rows.Add(row);
                    SqlParameter[] sqlParameters = {
                        new SqlParameter("@ID","0"),
                        new SqlParameter("@OrderMuid",txtDocNo.Tag),
                        new SqlParameter("@FabStrcuUid",txtStructureType.Tag),
                        new SqlParameter("@GSM",Convert.ToDecimal(txtGSM.Text)),
                        new SqlParameter("@Talarance",Convert.ToDecimal(txtTalarance.Text)),
                        new SqlParameter("@OldFabStrcuUid",txtGSM.Tag)
                    };
                    db.ExecuteNonQuery(CommandType.StoredProcedure, "Proc_OrderMFabricInOrderEntry", sqlParameters, conn);
                    txtStructureType.Text = string.Empty;
                    txtGSM.Text = string.Empty;
                    txtTalarance.Text = string.Empty;
                    txtGSM.Tag = string.Empty;
                    txtStructureType.Tag = string.Empty;
                }
                else
                {
                    MessageBox.Show("Fill the * Marked Fields !", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtStructureType.Focus();
                    return;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void TxtCurrency_Click(object sender, EventArgs e)
        {
            try
            {
                try
                {
                    DataTable dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "SP_GetGeneralsForOrderEntry", conn);
                    DataTable dtCurrency = dt.Select("TypeMUid = " + (int)OrderEntryGeneralM.Currency + "").CopyToDataTable();
                    bsCurrency.DataSource = dtCurrency;
                    FillGrid(dtCurrency, 13);
                    Point loc = Genclass.FindLocation(txtCurrency);
                    grSearch.Location = new Point(loc.X - 70, loc.Y + 10);
                    grSearch.Visible = true;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void TxtDocNo_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.Text != "Order Entry")
                {
                    string Query = "Select Uid,DocNo,DocDate from OrderM Where amdNumber = 0 order by DocDate desc";
                    DataTable dt = db.GetDataWithoutParam(CommandType.Text, Query, conn);
                    bsDocNo.DataSource = dt;
                    FillGrid(dt, 14);
                    Point loc = Genclass.FindLocation(txtDocNo);
                    grSearch.Location = new Point(loc.X - 10, loc.Y + 10);
                    grSearch.Visible = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void TxtDocNo_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (this.Text != "Order Entry")
                {
                    if (SelectId == 0)
                    {
                        bsDocNo.Filter = string.Format("DocNo LIKE '%{0}%'", txtDocNo.Text);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void Control_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Up)
            {
                this.SelectNextControl((Control)sender, false, true, true, true);
            }
            else if (e.KeyCode == Keys.Down)
            {
                this.SelectNextControl((Control)sender, true, true, true, true);
            }
            else if (e.KeyCode == Keys.Enter)
            {
                if (grSearch.Visible == true)
                {
                    DataGridCommon.ClearSelection();
                    DataGridCommon.Rows[0].Selected = true;
                    DataGridCommon.Select();
                }
                else
                {

                }
                this.SelectNextControl((Control)sender, true, true, true, true);
            }
        }

        private void DataGridCommon_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode.Equals(Keys.Up))
                {
                    MoveUp(DataGridCommon);
                }
                if (e.KeyCode.Equals(Keys.Down))
                {
                    MoveDown(DataGridCommon);
                }
                if (e.KeyCode.Equals(Keys.Enter))
                {
                    SelectId = 1;
                    int Index = DataGridCommon.SelectedCells[0].RowIndex;
                    if (Fillid == 1)
                    {
                        txtCustomer.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                        txtCustomer.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                        cmbSeason.Focus();
                    }
                    else if (Fillid == 2)
                    {
                        txtMerchindiser.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                        txtMerchindiser.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                        cmbSts.Focus();
                    }
                    else if (Fillid == 3)
                    {
                        txtDepartment.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                        txtDepartment.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                        txtAgent.Focus();
                    }
                    else if (Fillid == 4)
                    {
                        txtAgent.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                        txtAgent.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                        dtpRecivedDt.Focus();
                    }
                    else if (Fillid == 5)
                    {
                        txtShipType.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                        txtShipType.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                        txtShiftMode.Focus();
                    }
                    else if (Fillid == 6)
                    {
                        txtShiftMode.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                        txtShiftMode.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                        txtPayMode.Focus();
                    }
                    else if (Fillid == 7)
                    {
                        txtPayMode.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                        txtPayMode.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                        txtPayTerms.Focus();
                    }
                    else if (Fillid == 8)
                    {
                        txtCountry.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                        txtCountry.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                        txtCurrency.Focus();
                    }
                    else if (Fillid == 11)
                    {
                        txtCombo.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                        txtCombo.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                        txtComboQty.Focus();
                    }
                    else if (Fillid == 12)
                    {
                        txtStructureType.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                        txtStructureType.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                        txtComboQty.Focus();
                    }
                    else if (Fillid == 13)
                    {
                        txtCurrency.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                        txtCurrency.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                        txtExRate.Focus();
                    }
                    else if (Fillid == 14)
                    {
                        txtDocNo.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                        txtDocNo.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                        GetOrderMEdit(Convert.ToDecimal(txtDocNo.Tag));
                    }
                    grSearch.Visible = false;
                    SelectId = 0;
                }
                e.Handled = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
        private void MoveUp(DataGridView dataGridView)
        {
            if (dataGridView.RowCount > 0)
            {
                int rowCount = dataGridView.Rows.Count;
                int index = dataGridView.SelectedCells[0].OwningRow.Index;

                if (index == 0)
                {
                    return;
                }
                dataGridView.ClearSelection();
                dataGridView.Rows[index - 1].Selected = true;
            }
        }

        private void MoveDown(DataGridView dataGridView)
        {
            if (dataGridView.RowCount > 0)
            {
                int rowCount = dataGridView.Rows.Count;
                int index = dataGridView.SelectedCells[0].OwningRow.Index;
                if (index == (rowCount - 1)) // include the header row
                {
                    return;
                }
                dataGridView.ClearSelection();
                dataGridView.Rows[index + 1].Selected = true;
            }
        }

        private void DataGridStylePrice_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (LoadId != 1)
                {
                    if (cmbPriceType.Text == "Style Wise")
                    {
                        if (e.ColumnIndex == 2)
                        {
                            int Index = DataGridStylePrice.SelectedCells[0].RowIndex;
                            SqlParameter[] sqlParameters = {
                                new SqlParameter("@PriceType","Style Wise"),
                                new SqlParameter("@StyleComboUid",DBNull.Value),
                                new SqlParameter("@StyleSizeUid",DBNull.Value),
                                new SqlParameter("@OrderStyleUid",DataGridStylePrice.Rows[Index].Cells[3].Value.ToString()),
                                new SqlParameter("@Price",DataGridStylePrice.Rows[Index].Cells[2].Value.ToString()),
                                new SqlParameter("@OrderMuid",txtDocNo.Tag)
                        };
                            db.ExecuteNonQuery(CommandType.StoredProcedure, "Proc_OrderMPrice", sqlParameters, conn);
                        }
                    }
                    else if (cmbPriceType.Text == "Combo wise")
                    {
                        if (e.ColumnIndex == 2)
                        {
                            int Index = DataGridStylePrice.SelectedCells[0].RowIndex;
                            SqlParameter[] sqlParameters = {
                            new SqlParameter("@PriceType","Combo wise"),
                            new SqlParameter("@StyleComboUid",DataGridStylePrice.Rows[Index].Cells[3].Value.ToString()),
                            new SqlParameter("@StyleSizeUid",DBNull.Value),
                            new SqlParameter("@OrderStyleUid",DBNull.Value),
                            new SqlParameter("@Price",DataGridStylePrice.Rows[Index].Cells[2].Value.ToString()),
                            new SqlParameter("@OrderMuid",txtDocNo.Tag)
                        };
                            db.ExecuteNonQuery(CommandType.StoredProcedure, "Proc_OrderMPrice", sqlParameters, conn);
                        }
                    }
                    else
                    {
                        if (e.ColumnIndex == 4)
                        {
                            int Index = DataGridStylePrice.SelectedCells[0].RowIndex;
                            SqlParameter[] sqlParameters = {
                            new SqlParameter("@PriceType","Size Wise"),
                            new SqlParameter("@StyleComboUid",DataGridStylePrice.Rows[Index].Cells[5].Value.ToString()),
                            new SqlParameter("@StyleSizeUid",DataGridStylePrice.Rows[Index].Cells[6].Value.ToString()),
                            new SqlParameter("@OrderStyleUid",cmbStylePrice.SelectedValue),
                            new SqlParameter("@Price",DataGridStylePrice.Rows[Index].Cells[4].Value.ToString()),
                            new SqlParameter("@OrderMuid",txtDocNo.Tag)
                            };
                            db.ExecuteNonQuery(CommandType.StoredProcedure, "Proc_OrderMPrice", sqlParameters, conn);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void TxtOrderUnit_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    DataGridCommon.Focus();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void DataGridStyleSize_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Delete)
                {
                    DialogResult res = MessageBox.Show("Do you want delete the Size", "Information", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                    if (res == DialogResult.Yes)
                    {
                        if (DataGridStyleSize.SelectedRows.Count > 0)
                        {
                            DataGridStyleSize.Rows.RemoveAt(DataGridStyleSize.CurrentRow.Index);
                            DataGridStyleSize.ClearSelection();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void DataGridStyle_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Delete)
                {
                    DialogResult res = MessageBox.Show("Do you want delete the Style !", "Information", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                    if (res == DialogResult.Yes)
                    {
                        if (DataGridStyle.SelectedRows.Count > 0)
                        {
                            DataGridStyle.Rows.RemoveAt(DataGridStyle.CurrentRow.Index);
                            DataGridStyle.ClearSelection();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void DataGridStyleSizeCombo_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Delete)
                {
                    DialogResult res = MessageBox.Show("Do you want delete the Combo !", "Information", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                    if (res == DialogResult.Yes)
                    {
                        if (DataGridStyleSizeCombo.SelectedRows.Count > 0)
                        {
                            DataGridStyleSizeCombo.Rows.RemoveAt(DataGridStyleSizeCombo.CurrentRow.Index);
                            DataGridStyleSizeCombo.ClearSelection();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void DataGridSizeMatrix_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Delete)
                {
                    DialogResult res = MessageBox.Show("Do you want delete the Size Matrix !", "Information", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                    if (res == DialogResult.Yes)
                    {
                        if (DataGridSizeMatrix.SelectedRows.Count > 0)
                        {
                            DataGridSizeMatrix.Rows.RemoveAt(DataGridSizeMatrix.CurrentRow.Index);
                            DataGridSizeMatrix.ClearSelection();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void DataGridAssort_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Delete)
                {
                    DialogResult res = MessageBox.Show("Do you want delete the Assort!", "Information", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                    if (res == DialogResult.Yes)
                    {
                        if (DataGridAssort.SelectedRows.Count > 0)
                        {
                            DataGridAssort.Rows.RemoveAt(DataGridAssort.CurrentRow.Index);
                            DataGridAssort.ClearSelection();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void DataGridStylePrice_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Delete)
                {
                    DialogResult res = MessageBox.Show("Do you want delete the Assort!", "Information", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                    if (res == DialogResult.Yes)
                    {
                        if (DataGridStylePrice.SelectedRows.Count > 0)
                        {
                            DataGridStylePrice.Rows.RemoveAt(DataGridStylePrice.CurrentRow.Index);
                            DataGridStylePrice.ClearSelection();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void DataGridCombo_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            try
            {
                if (DataGridCombo.Columns[e.ColumnIndex].Name != "SlNo" && DataGridCombo.Columns[e.ColumnIndex].Name != "Combo" && DataGridCombo.Columns[e.ColumnIndex].Name != "Total" && DataGridCombo.Columns[e.ColumnIndex].Name != "ComboUid")
                {
                    var cell = DataGridCombo.Rows[e.RowIndex].Cells[e.ColumnIndex];
                    cell.ToolTipText = "Enter Value and Press TAB Key";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void DataGridSizeMatrix_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            try
            {
                if (DataGridSizeMatrix.Columns[e.ColumnIndex].Name != "SlNo" && DataGridSizeMatrix.Columns[e.ColumnIndex].Name != "Combo" && DataGridSizeMatrix.Columns[e.ColumnIndex].Name != "Total" && DataGridSizeMatrix.Columns[e.ColumnIndex].Name != "ComboUid")
                {
                    var cell = DataGridSizeMatrix.Rows[e.RowIndex].Cells[e.ColumnIndex];
                    cell.ToolTipText = "Enter Value and Press TAB Key";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void SfdDataGridOrderEntry_QueryCellStyle(object sender, Syncfusion.WinForms.DataGrid.Events.QueryCellStyleEventArgs e)
        {
            try
            {
                if (e.Column.MappingName == "DocSts")
                {
                    if (e.DisplayText == "Entry Pending")
                    {
                        e.Style.BackColor = Color.Coral;
                        e.Style.TextColor = Color.Black;
                    }
                    else
                    {
                        e.Style.BackColor = Color.LightGreen;
                        e.Style.TextColor = Color.DarkSlateBlue;
                    }
                }

                if (e.Column.MappingName == "BudgetSts")
                {
                    if (e.DisplayText == "Not Entered")
                    {
                        e.Style.BackColor = Color.OrangeRed;
                        e.Style.TextColor = Color.White;
                    }
                    else if (e.DisplayText == "Pending")
                    {
                        e.Style.BackColor = Color.GreenYellow;
                        e.Style.TextColor = Color.Black;
                    }
                    else
                    {
                        e.Style.BackColor = Color.LightGreen;
                        e.Style.TextColor = Color.DarkSlateBlue;
                    }
                }
                if (e.Column.MappingName == "FABRIC")
                {
                    if (e.DisplayText == "No Fabrics")
                    {
                        e.Style.BackColor = Color.OrangeRed;
                        e.Style.TextColor = Color.White;
                    }
                    else
                    {
                        e.Style.BackColor = Color.LightGreen;
                        e.Style.TextColor = Color.DarkSlateBlue;
                    }
                }
                if (e.Column.MappingName == "TRIMS")
                {
                    if (e.DisplayText == "No Trims")
                    {
                        e.Style.BackColor = Color.OrangeRed;
                        e.Style.TextColor = Color.White;
                    }
                    else
                    {
                        e.Style.BackColor = Color.LightGreen;
                        e.Style.TextColor = Color.DarkSlateBlue;
                    }
                }
                if (e.Column.MappingName == "FABRICPROCESS")
                {
                    if (e.DisplayText == "No Process")
                    {
                        e.Style.BackColor = Color.OrangeRed;
                        e.Style.TextColor = Color.White;
                    }
                    else
                    {
                        e.Style.BackColor = Color.LightGreen;
                        e.Style.TextColor = Color.DarkSlateBlue;
                    }
                }
                if (e.Column.MappingName == "TRIMSPROCESS")
                {
                    if (e.DisplayText == "No Process")
                    {
                        e.Style.BackColor = Color.OrangeRed;
                        e.Style.TextColor = Color.White;
                    }
                    else
                    {
                        e.Style.BackColor = Color.LightGreen;
                        e.Style.TextColor = Color.DarkSlateBlue;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void CmbPlanningStyleCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (LoadId == 0)
                {
                    decimal val = 0;
                    if (CmbPlanningStyleCombo.SelectedValue == null)
                    {
                        val = 0;
                    }
                    else
                    {
                        val = Convert.ToDecimal(CmbPlanningStyleCombo.SelectedValue);
                    }
                    string Query1 = "Select * from StyleCombo Where Uid =" + val + "";
                    DataTable dtTalarance = db.GetDataWithoutParam(CommandType.Text, Query1, conn);
                    txtBuyerTolerance.Text = dtTalarance.Rows[0]["BuyerTol"].ToString();
                    txtCuttingTolerance.Text = dtTalarance.Rows[0]["CuttingTol"].ToString();
                }
                if (EditId == 1 && SelectId == 0)
                {
                    SqlParameter[] sqlParameters = { new SqlParameter("@OrderMuid", txtDocNo.Tag) };
                    DataTable dtEdit = db.GetDataWithParam(CommandType.StoredProcedure, "Proc_GetOrderMPlanning", sqlParameters, conn);
                    FillDataGridPlanning(dtEdit);
                }
                else if (SelectId == 0)
                {
                    if (CmbPlanningStyleCombo.SelectedIndex != -1)
                    {
                        SqlParameter[] parameters = {
                            new SqlParameter("@StyleComboUid",CmbPlanningStyleCombo.SelectedValue),
                            new SqlParameter("@OrderMuid",txtDocNo.Tag)
                        };
                        DataTable dt = db.GetDataWithParam(CommandType.StoredProcedure, "SP_GetPlanning", parameters, conn);
                        FillDataGridPlanning(dt);
                    }
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void FillDataGridPlanning(DataTable dt)
        {
            try
            {
                DataGridPlanning.Rows.Clear();
                if (EditId == 1 && SelectId == 0)
                {
                    if (dt.Rows.Count > 0)
                    {
                        System.Data.DataRow[] dataRows = dt.Select("ComboUid =" + CmbPlanningStyleCombo.SelectedValue + "");
                        if (dataRows.Length > 0)
                        {
                            DataGridPlanning.Rows.Clear();
                            DataTable dataTable = dt.Select("ComboUid =" + CmbPlanningStyleCombo.SelectedValue + "").CopyToDataTable();
                            for (int i = 0; i < dataTable.Rows.Count; i++)
                            {
                                DataGridViewRow row = (DataGridViewRow)DataGridPlanning.Rows[0].Clone();
                                row.Cells[0].Value = dataTable.Rows[i]["SizeName"].ToString();
                                row.Cells[1].Value = dataTable.Rows[i]["Qty"].ToString();
                                row.Cells[2].Value = dataTable.Rows[i]["BuyerTol"].ToString();
                                row.Cells[3].Value = dataTable.Rows[i]["BuyerAppQty"].ToString();
                                row.Cells[4].Value = dataTable.Rows[i]["CuttingTol"].ToString();
                                row.Cells[5].Value = dataTable.Rows[i]["TotalQty"].ToString();
                                row.Cells[6].Value = dataTable.Rows[i]["SizeUid"].ToString();
                                row.Cells[7].Value = "0";
                                DataGridPlanning.Rows.Add(row);
                            }
                        }
                        else
                        {
                            SqlParameter[] parameters = {
                                new SqlParameter("@StyleComboUid",CmbPlanningStyleCombo.SelectedValue),
                                new SqlParameter("@OrderMuid",txtDocNo.Tag)
                            };
                            DataTable dtN = db.GetDataWithParam(CommandType.StoredProcedure, "SP_GetPlanning", parameters, conn);
                            for (int i = 0; i < dtN.Rows.Count; i++)
                            {
                                DataGridViewRow row = (DataGridViewRow)DataGridPlanning.Rows[0].Clone();
                                row.Cells[0].Value = dtN.Rows[i]["SizeName"].ToString();
                                row.Cells[1].Value = dtN.Rows[i]["Qty"].ToString();
                                row.Cells[2].Value = dtN.Rows[i]["BuyerTol"].ToString();
                                row.Cells[3].Value = dtN.Rows[i]["BuyerAprrovalQty"].ToString();
                                row.Cells[4].Value = dtN.Rows[i]["CuttingTol"].ToString();
                                row.Cells[5].Value = dtN.Rows[i]["TQty"].ToString();
                                row.Cells[6].Value = dtN.Rows[i]["SizeUid"].ToString();
                                row.Cells[7].Value = dtN.Rows[i]["AssortQUid"].ToString();
                                DataGridPlanning.Rows.Add(row);
                            }
                        }
                    }
                    else
                    {
                        SqlParameter[] parameters = {
                                new SqlParameter("@StyleComboUid",CmbPlanningStyleCombo.SelectedValue),
                                new SqlParameter("@OrderMuid",txtDocNo.Tag)
                            };
                        DataTable dtN = db.GetDataWithParam(CommandType.StoredProcedure, "SP_GetPlanning", parameters, conn);
                        for (int i = 0; i < dtN.Rows.Count; i++)
                        {
                            DataGridViewRow row = (DataGridViewRow)DataGridPlanning.Rows[0].Clone();
                            row.Cells[0].Value = dtN.Rows[i]["SizeName"].ToString();
                            row.Cells[1].Value = dtN.Rows[i]["Qty"].ToString();
                            row.Cells[2].Value = dtN.Rows[i]["BuyerTol"].ToString();
                            row.Cells[3].Value = dtN.Rows[i]["BuyerAprrovalQty"].ToString();
                            row.Cells[4].Value = dtN.Rows[i]["CuttingTol"].ToString();
                            row.Cells[5].Value = dtN.Rows[i]["TQty"].ToString();
                            row.Cells[6].Value = dtN.Rows[i]["SizeUid"].ToString();
                            row.Cells[7].Value = dtN.Rows[i]["AssortQUid"].ToString();
                            DataGridPlanning.Rows.Add(row);
                        }
                    }
                }
                else
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        DataGridViewRow row = (DataGridViewRow)DataGridPlanning.Rows[0].Clone();
                        row.Cells[0].Value = dt.Rows[i]["SizeName"].ToString();
                        row.Cells[1].Value = dt.Rows[i]["Qty"].ToString();
                        row.Cells[2].Value = dt.Rows[i]["BuyerTol"].ToString();
                        row.Cells[3].Value = dt.Rows[i]["BuyerAprrovalQty"].ToString();
                        row.Cells[4].Value = dt.Rows[i]["CuttingTol"].ToString();
                        row.Cells[5].Value = dt.Rows[i]["TQty"].ToString();
                        row.Cells[6].Value = dt.Rows[i]["SizeUid"].ToString();
                        row.Cells[7].Value = dt.Rows[i]["AssortQUid"].ToString();
                        DataGridPlanning.Rows.Add(row);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected void LoadDataGridPlanning()
        {
            try
            {
                LoadId = 1;
                DataGridPlanning.DataSource = null;
                DataGridPlanning.AutoGenerateColumns = false;
                DataGridPlanning.ColumnCount = 8;
                DataGridPlanning.Columns[0].Name = "SizeName";
                DataGridPlanning.Columns[0].HeaderText = "SizeName";

                DataGridPlanning.Columns[1].Name = "Qty";
                DataGridPlanning.Columns[1].HeaderText = "Qty";

                DataGridPlanning.Columns[2].Name = "BuyerTolerance";
                DataGridPlanning.Columns[2].HeaderText = "Buyer Tolerance";
                DataGridPlanning.Columns[2].Width = 150;

                DataGridPlanning.Columns[3].Name = "BuyerApprovalQty";
                DataGridPlanning.Columns[3].HeaderText = "BuyerApprovalQty";
                DataGridPlanning.Columns[3].Width = 180;

                DataGridPlanning.Columns[4].Name = "CuttingTolerance";
                DataGridPlanning.Columns[4].HeaderText = "Cutting Tolerance";
                DataGridPlanning.Columns[4].Width = 180;

                DataGridPlanning.Columns[5].Name = "TotalQty";
                DataGridPlanning.Columns[5].HeaderText = "Total Qty";

                DataGridPlanning.Columns[6].Name = "SizeUid";
                DataGridPlanning.Columns[6].HeaderText = "SizeUid";
                DataGridPlanning.Columns[6].Visible = false;

                DataGridPlanning.Columns[7].Name = "AssortQUid";
                DataGridPlanning.Columns[7].HeaderText = "AssortQUid";
                DataGridPlanning.Columns[7].Visible = false;
                LoadId = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void DataGridPlanning_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (LoadId == 0 && DataGridPlanning.Columns[e.ColumnIndex].Name != "BuyerTolerance" && DataGridPlanning.Columns[e.ColumnIndex].Name != "CuttingTolerance" && DataGridPlanning.Columns[e.ColumnIndex].Name != "SizeName" && DataGridPlanning.Columns[e.ColumnIndex].Name != "Qty" && DataGridPlanning.Columns[e.ColumnIndex].Name != "SizeUid" && DataGridPlanning.Columns[e.ColumnIndex].Name != "AssortQUid")
                {
                    decimal BuyerTol;
                    decimal BuyerAprrovalQty;
                    decimal CuttingTol;
                    decimal AssortQuid;
                    decimal StyleSizeUid;
                    decimal TQty;
                    AssortQuid = Convert.ToDecimal(DataGridPlanning.Rows[e.RowIndex].Cells[7].Value.ToString());
                    StyleSizeUid = Convert.ToDecimal(DataGridPlanning.Rows[e.RowIndex].Cells["SizeUid"].Value.ToString());
                    if (DataGridPlanning.Rows[e.RowIndex].Cells["BuyerTolerance"].Value == null || DataGridPlanning.Rows[e.RowIndex].Cells["BuyerTolerance"].Value.ToString() == "")
                    {
                        BuyerTol = 0;
                    }
                    else
                    {
                        BuyerTol = Convert.ToDecimal(DataGridPlanning.Rows[e.RowIndex].Cells["BuyerTolerance"].Value.ToString());
                    }
                    if (DataGridPlanning.Rows[e.RowIndex].Cells["BuyerApprovalQty"].Value == null || DataGridPlanning.Rows[e.RowIndex].Cells["BuyerApprovalQty"].Value.ToString() == "")
                    {
                        BuyerAprrovalQty = 0;
                    }
                    else
                    {
                        BuyerAprrovalQty = Convert.ToDecimal(DataGridPlanning.Rows[e.RowIndex].Cells["BuyerApprovalQty"].Value.ToString());
                    }
                    if (DataGridPlanning.Rows[e.RowIndex].Cells["CuttingTolerance"].Value == null || DataGridPlanning.Rows[e.RowIndex].Cells["CuttingTolerance"].Value.ToString() == "")
                    {
                        CuttingTol = 0;
                    }
                    else
                    {
                        CuttingTol = Convert.ToDecimal(DataGridPlanning.Rows[e.RowIndex].Cells["CuttingTolerance"].Value.ToString());
                    }
                    if (DataGridPlanning.Rows[e.RowIndex].Cells["TotalQty"].Value == null || DataGridPlanning.Rows[e.RowIndex].Cells["TotalQty"].Value.ToString() == "")
                    {
                        TQty = 0;
                    }
                    else
                    {
                        TQty = Convert.ToDecimal(DataGridPlanning.Rows[e.RowIndex].Cells["TotalQty"].Value.ToString());
                    }
                    TQty += BuyerAprrovalQty;
                    SqlParameter[] parameters = {
                        new SqlParameter("@AssortQuid",AssortQuid),
                        new SqlParameter("@StyleSizeUid",StyleSizeUid),
                        new SqlParameter("@BuyerTol",BuyerTol),
                        new SqlParameter("@BuyerAprrovalQty",BuyerAprrovalQty),
                        new SqlParameter("@CuttingTol",CuttingTol),
                        new SqlParameter("@TQty",TQty)
                    };
                    db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_PlanningOrderMAssortQty", parameters, conn);
                    CalcukateTotal();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void BtnCalculate_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtCuttingTolerance.Text == string.Empty)
                {
                    MessageBox.Show("Cutting Tolerance can't Empty !", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtCuttingTolerance.Focus();
                    return;
                }
                if (txtBuyerTolerance.Text == string.Empty)
                {
                    LoadId = 1;
                    txtBuyerTolerance.Text = "0";
                    LoadId = 0;
                }
                if (txtCuttingTolerance.Text == string.Empty)
                {
                    LoadId = 1;
                    txtCuttingTolerance.Text = "0";
                    LoadId = 0;
                }
                decimal val = 0;
                if (CmbPlanningStyleCombo.SelectedValue == null)
                {
                    val = 0;
                }
                else
                {
                    val = Convert.ToDecimal(CmbPlanningStyleCombo.SelectedValue);
                }
                string Query = "Update StyleCombo Set BuyerTol=" + txtBuyerTolerance.Text + ",CuttingTol=" + txtCuttingTolerance.Text + " Where Uid =" + val + "";
                db.ExecuteNonQuery(CommandType.Text, Query, conn);
                for (int i = 0; i < DataGridPlanning.Rows.Count - 1; i++)
                {
                    decimal TQty;
                    if (DataGridPlanning.Rows[i].Cells[5].Value == null || DataGridPlanning.Rows[i].Cells[5].Value.ToString() == "")
                    {
                        TQty = 0;
                    }
                    else
                    {
                        TQty = Convert.ToDecimal(DataGridPlanning.Rows[i].Cells[5].Value.ToString());
                    }
                    if (txtBuyerTolerance.Text == string.Empty)
                    {
                        txtBuyerTolerance.Text = "0";
                    }
                    decimal Qty = Convert.ToDecimal(DataGridPlanning.Rows[i].Cells[1].Value.ToString());
                    decimal BuyerTolerancePer = Convert.ToDecimal(txtBuyerTolerance.Text);
                    decimal CuttingTolerancePer = Convert.ToDecimal(txtCuttingTolerance.Text);
                    decimal BuyerTolerance = (Qty * BuyerTolerancePer) / 100;
                    decimal CuttingTolerance = (Qty * CuttingTolerancePer) / 100;
                    DataGridPlanning.Rows[i].Cells[2].Value = BuyerTolerance.ToString("0");
                    DataGridPlanning.Rows[i].Cells[4].Value = CuttingTolerance.ToString("0");
                    decimal TotalQty = Qty + Convert.ToDecimal(DataGridPlanning.Rows[i].Cells[2].Value) + Convert.ToDecimal(DataGridPlanning.Rows[i].Cells[4].Value);
                    DataGridPlanning.Rows[i].Cells[5].Value = TotalQty;
                }

                for (int j = 0; j < DataGridPlanning.Rows.Count - 1; j++)
                {
                    SqlParameter[] sqlParameters = {
                            new SqlParameter("@OrderMuid",txtDocNo.Tag),
                            new SqlParameter("@StyleUid",CmbPlanningStyle.SelectedValue),
                            new SqlParameter("@ComboUid",CmbPlanningStyleCombo.SelectedValue),
                            new SqlParameter("@SizeUid",DataGridPlanning.Rows[j].Cells[6].Value.ToString()),
                            new SqlParameter("@Qty",DataGridPlanning.Rows[j].Cells[1].Value.ToString()),
                            new SqlParameter("@BuyerTol",DataGridPlanning.Rows[j].Cells[2].Value.ToString()),
                            new SqlParameter("@BuyerAppQty",DataGridPlanning.Rows[j].Cells[3].Value.ToString()),
                            new SqlParameter("@CuttingTol",DataGridPlanning.Rows[j].Cells[4].Value.ToString()),
                            new SqlParameter("@TotalQty",DataGridPlanning.Rows[j].Cells[5].Value.ToString()),
                        };
                    db.ExecuteNonQuery(CommandType.StoredProcedure, "Proc_OrderMPlanning", sqlParameters, conn);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected void CalcukateTotal()
        {
            try
            {
                for (int i = 0; i < DataGridPlanning.Rows.Count - 1; i++)
                {
                    decimal TQty;
                    decimal BuyerAprrovalQty;
                    if (DataGridPlanning.Rows[i].Cells[5].Value == null || DataGridPlanning.Rows[i].Cells[5].Value.ToString() == "")
                    {
                        TQty = 0;
                    }
                    else
                    {
                        TQty = Convert.ToDecimal(DataGridPlanning.Rows[i].Cells[5].Value.ToString());
                    }
                    if (DataGridPlanning.Rows[i].Cells[3].Value == null || DataGridPlanning.Rows[i].Cells[3].Value.ToString() == "")
                    {
                        BuyerAprrovalQty = 0;
                    }
                    else
                    {
                        BuyerAprrovalQty = Convert.ToDecimal(DataGridPlanning.Rows[i].Cells[3].Value.ToString());
                    }
                    decimal Qty = Convert.ToDecimal(DataGridPlanning.Rows[i].Cells[1].Value.ToString());
                    decimal BuyerToleranceQty = Convert.ToDecimal(DataGridPlanning.Rows[i].Cells[2].Value.ToString()); ;
                    decimal CuttingToleranceQty = Convert.ToDecimal(DataGridPlanning.Rows[i].Cells[4].Value.ToString());
                    decimal TotalQty = Qty + BuyerAprrovalQty + BuyerToleranceQty + CuttingToleranceQty;
                    DataGridPlanning.Rows[i].Cells[5].Value = TotalQty;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void CmbPlanningStyle_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                //if (LoadId == 0)
                //{
                //    decimal val = 0;
                //    if (CmbPlanningStyle.SelectedValue == null)
                //    {
                //        val = 0;
                //    }
                //    else
                //    {
                //        val = Convert.ToDecimal(CmbPlanningStyle.SelectedValue);
                //    }
                //    string Query1 = "Select * from OrderMStyles Where Uid =" + val + "";
                //    DataTable dtTalarance = db.GetDataWithoutParam(CommandType.Text, Query1, conn);
                //    txtBuyerTolerance.Text = dtTalarance.Rows[0]["BuyerTolerance"].ToString();
                //    txtCuttingTolerance.Text = dtTalarance.Rows[0]["CuttingTolerance"].ToString();
                //}
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void DataGridAssort_MouseClick(object sender, MouseEventArgs e)
        {
            try
            {
                if (e.Button == MouseButtons.Right)
                {
                    ContextMenu m = new ContextMenu();
                    m.MenuItems.Add(new MenuItem("Import", new EventHandler(DataGridAssort_MouseClickFrmae)));

                    int currentMouseOverRow = DataGridAssort.HitTest(e.X, e.Y).RowIndex;

                    m.Show(DataGridAssort, new Point(e.X, e.Y));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void DataGridAssort_MouseClickFrmae(object sender, EventArgs e)
        {
            try
            {
                string Query = "Select* from OrderMAssort Where OrderMUid =" + txtDocNo.Tag + " Order by AssortUid";
                string Query1 = "Select* from OrderMStyles Where OrrderMUid =" + txtDocNo.Tag + " Order by Uid";
                DataTable dtAssort = db.GetDataWithoutParam(CommandType.Text, Query, conn);
                DataTable dtStyles = db.GetDataWithoutParam(CommandType.Text, Query1, conn);
                CmbAssortImport.DataSource = null;
                CmbStyleImport.DataSource = null;
                CmbAssortImport.DisplayMember = "AssortmentType";
                CmbAssortImport.ValueMember = "AssortUid";
                CmbAssortImport.DataSource = dtAssort;

                CmbStyleImport.DisplayMember = "StyleName";
                CmbStyleImport.ValueMember = "Uid";
                CmbStyleImport.DataSource = dtStyles;

                grImport.Visible = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void BtnImportHide_Click(object sender, EventArgs e)
        {
            grImport.Visible = false;
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog openKeywordsFileDialog = new OpenFileDialog
                {
                    InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments),
                    Multiselect = false,
                    ValidateNames = true,
                    DereferenceLinks = false, // Will return .lnk in shortcuts.
                    Filter = "Excel files (*.xls)|*.xls;"
                };
                var dialogResult = openKeywordsFileDialog.ShowDialog();
                if (dialogResult == DialogResult.OK)
                {
                    lblPath.Text = openKeywordsFileDialog.FileName;
                    FileInfo file = new FileInfo(openKeywordsFileDialog.FileName);
                    DataTable dataTable = GeneralParameters.ReadExcel(file.FullName, file.Extension);
                    for (int i = 0; i < dataTable.Columns.Count; i++)
                    {
                        if (i > 1)
                        {
                            SqlParameter[] sqlParameters = {
                                new SqlParameter("@OrderID",txtDocNo.Tag),
                                new SqlParameter("@StyleID",CmbStyleImport.SelectedValue),
                                new SqlParameter("@AssortId",CmbAssortImport.SelectedValue),
                                new SqlParameter("@SizeName",dataTable.Columns[i].ColumnName)
                            };
                            db.ExecuteNonQuery(CommandType.StoredProcedure, "Proc_ImportSize", sqlParameters, conn);
                        }
                    }
                    for (int i = 0; i < dataTable.Rows.Count; i++)
                    {
                        decimal sum1 = 0;
                        for (int j = 0; j < dataTable.Columns.Count; j++)
                        {
                            if (j > 1)
                            {
                                string columnName1 = dataTable.Columns[j].ColumnName.ToString();
                                var columnTotal = Convert.ToDecimal(dataTable.Rows[i][j].ToString());
                                sum1 += columnTotal;
                            }
                        }
                        SqlParameter[] sqlParameters = {
                                new SqlParameter("@OrderID",txtDocNo.Tag),
                                new SqlParameter("@StyleID",CmbStyleImport.SelectedValue),
                                new SqlParameter("@AssortId",CmbAssortImport.SelectedValue),
                                new SqlParameter("@Combo",dataTable.Rows[i][1].ToString()),
                                new SqlParameter("@ComboQty",sum1)
                            };
                        db.ExecuteNonQuery(CommandType.StoredProcedure, "Proc_ComboImport", sqlParameters, conn);
                    }
                }
                MessageBox.Show("Data's Imported Successfully", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void TxtAvgRate_TextChanged(object sender, EventArgs e)
        {
            try
            {
                txtCrossValue.Text = "0";
                if (txtExRate.Text == string.Empty)
                {
                    txtExRate.Text = "0";
                }
                if (txtAvgRate.Text != string.Empty)
                {
                    txtCrossValue.Text = string.Empty;
                    txtCrossValue.Text = ((OrderQty * Convert.ToDecimal(txtAvgRate.Text)) * Convert.ToDecimal(txtExRate.Text)).ToString("0.00");
                }
                else
                {
                    txtCrossValue.Text = "0";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void DataGridStyle_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                int Index = DataGridStyle.SelectedCells[0].RowIndex;
                txtStyleNo.Text = DataGridStyle.Rows[Index].Cells[1].Value.ToString();
                txtStyleNo.Tag = DataGridStyle.Rows[Index].Cells[1].Value.ToString();
                txtdescription.Text = DataGridStyle.Rows[Index].Cells[2].Value.ToString();
                cmbOrderUOm.Text = DataGridStyle.Rows[Index].Cells[3].Value.ToString();
                CmbPlanUom.Text = DataGridStyle.Rows[Index].Cells[4].Value.ToString();
                txtQuantity.Text = DataGridStyle.Rows[Index].Cells[5].Value.ToString();
                EditStyleId = Convert.ToDecimal(DataGridStyle.Rows[Index].Cells[8].Value.ToString());
                DataGridStyle.Rows.RemoveAt(Index);
                DataGridStyle.ClearSelection();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void DataGridStyleSize_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                int Index = DataGridStyleSize.SelectedCells[0].RowIndex;
                txtStyleSize.Tag = DataGridStyleSize.Rows[Index].Cells[0].Value.ToString();
                txtStyleSize.Text = DataGridStyleSize.Rows[Index].Cells[0].Value.ToString();
                DataGridStyleSize.Rows.RemoveAt(Index);
                DataGridStyleSize.ClearSelection();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void DataGridStyleSizeCombo_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                int Index = DataGridStyleSizeCombo.SelectedCells[0].RowIndex;
                txtCombo.Tag = DataGridStyleSizeCombo.Rows[Index].Cells[0].Value.ToString();
                txtCombo.Text = DataGridStyleSizeCombo.Rows[Index].Cells[0].Value.ToString();
                DataGridStyleSizeCombo.Rows.RemoveAt(Index);
                DataGridStyleSizeCombo.ClearSelection();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void DataGridAssort_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                int Index = DataGridAssort.SelectedCells[0].RowIndex;
                cmbCountry.Text = DataGridAssort.Rows[Index].Cells[0].Value.ToString();
                txtRefNo.Text = DataGridAssort.Rows[Index].Cells[1].Value.ToString();
                txtRefNo.Tag = DataGridAssort.Rows[Index].Cells[1].Value.ToString();
                txtAssortmentType.Text = DataGridAssort.Rows[Index].Cells[2].Value.ToString();
                txtAssortmentType.Tag = DataGridAssort.Rows[Index].Cells[2].Value.ToString();
                txtAssortQuantity.Text = DataGridAssort.Rows[Index].Cells[3].Value.ToString();
                dtpAssortDelDt.Text = DataGridAssort.Rows[Index].Cells[4].Value.ToString();
                dtpAssortShipDt.Text = DataGridAssort.Rows[Index].Cells[5].Value.ToString();
                txtPortofDelivery.Text = DataGridAssort.Rows[Index].Cells[6].Value.ToString();
                txtFinalDistination.Text = DataGridAssort.Rows[Index].Cells[7].Value.ToString();
                txtMCBox.Text = DataGridAssort.Rows[Index].Cells[10].Value.ToString();
                DataGridAssort.Rows.RemoveAt(Index);
                DataGridAssort.ClearSelection();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void GrBack_Enter(object sender, EventArgs e)
        {

        }

        private void TxtCuttingTolerance_TextChanged(object sender, EventArgs e)
        {
            try
            {
                //if (txtCuttingTolerance.Text != string.Empty)
                //{
                //    if (txtBuyerTolerance.Text == string.Empty)
                //    {
                //        LoadId = 1;
                //        txtBuyerTolerance.Text = "0";
                //        LoadId = 0;
                //    }
                //    if (txtCuttingTolerance.Text == string.Empty)
                //    {
                //        LoadId = 1;
                //        txtCuttingTolerance.Text = "0";
                //        LoadId = 0;
                //    }
                //    decimal val = 0;
                //    if (CmbPlanningStyleCombo.SelectedValue == null)
                //    {
                //        val = 0;
                //    }
                //    else
                //    {
                //        val = Convert.ToDecimal(CmbPlanningStyleCombo.SelectedValue);
                //    }
                //    string Query = "Update StyleCombo Set BuyerTol=" + txtBuyerTolerance.Text + ",CuttingTol=" + txtCuttingTolerance.Text + " Where Uid =" + val + "";
                //    db.ExecuteNonQuery(CommandType.Text, Query, conn);
                //    for (int i = 0; i < DataGridPlanning.Rows.Count - 1; i++)
                //    {
                //        decimal TQty;
                //        if (DataGridPlanning.Rows[i].Cells[5].Value == null || DataGridPlanning.Rows[i].Cells[5].Value.ToString() == "")
                //        {
                //            TQty = 0;
                //        }
                //        else
                //        {
                //            TQty = Convert.ToDecimal(DataGridPlanning.Rows[i].Cells[5].Value.ToString());
                //        }
                //        if (txtBuyerTolerance.Text == string.Empty)
                //        {
                //            txtBuyerTolerance.Text = "0";
                //        }
                //        decimal Qty = Convert.ToDecimal(DataGridPlanning.Rows[i].Cells[1].Value.ToString());
                //        decimal BuyerTolerancePer = Convert.ToDecimal(txtBuyerTolerance.Text);
                //        decimal CuttingTolerancePer = Convert.ToDecimal(txtCuttingTolerance.Text);
                //        decimal BuyerTolerance = (Qty * BuyerTolerancePer) / 100;
                //        decimal CuttingTolerance = (Qty * CuttingTolerancePer) / 100;
                //        DataGridPlanning.Rows[i].Cells[2].Value = BuyerTolerance.ToString("0");
                //        DataGridPlanning.Rows[i].Cells[4].Value = CuttingTolerance.ToString("0");
                //        decimal TotalQty = Qty + Convert.ToDecimal(DataGridPlanning.Rows[i].Cells[2].Value) + Convert.ToDecimal(DataGridPlanning.Rows[i].Cells[4].Value);
                //        DataGridPlanning.Rows[i].Cells[5].Value = TotalQty;
                //}
                //}
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void BtnReferesh_Click(object sender, EventArgs e)
        {
            try
            {
                LoadId = 1;
                DataTable dtq = GetorderStyle(Convert.ToDecimal(txtDocNo.Tag));
                CmbStyle.DataSource = null;
                CmbStyle.DisplayMember = "StyleName";
                CmbStyle.ValueMember = "Uid";
                CmbStyle.DataSource = dtq;

                cmbStylePrice.DataSource = null;
                cmbStylePrice.DisplayMember = "StyleName";
                cmbStylePrice.ValueMember = "Uid";
                cmbStylePrice.DataSource = dtq;

                cmbStyleAssort.DataSource = null;
                cmbStyleAssort.DisplayMember = "StyleName";
                cmbStyleAssort.ValueMember = "Uid";
                cmbStyleAssort.DataSource = dtq;

                cmbQtyStyle.DataSource = null;
                cmbQtyStyle.DisplayMember = "StyleName";
                cmbQtyStyle.ValueMember = "Uid";
                cmbQtyStyle.DataSource = dtq;

                cmbMatrixStyle.DataSource = null;
                cmbMatrixStyle.DisplayMember = "StyleName";
                cmbMatrixStyle.ValueMember = "Uid";
                cmbMatrixStyle.DataSource = dtq;

                CmbPlanningStyle.DataSource = null;
                CmbPlanningStyle.DisplayMember = "StyleName";
                CmbPlanningStyle.ValueMember = "Uid";
                CmbPlanningStyle.DataSource = dtq;

                CmbStyleAssort_SelectedIndexChanged(sender, e);
                CmbStyle_SelectedIndexChanged(sender, e);
                CmbMatrixStyle_SelectedIndexChanged(sender, e);
                CmbQtyStyle_SelectedIndexChanged(sender, e);
                CmbPlanningStyleCombo_SelectedIndexChanged(sender, e);
                CmbPriceType_SelectedIndexChanged(sender, e);
                TxtAvgRate_TextChanged(sender, e);
                LoadId = 0;
                MessageBox.Show("Completed", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void DataGridFabricDet_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                int Index = DataGridFabricDet.SelectedCells[0].RowIndex;
                txtStructureType.Text = DataGridFabricDet.Rows[Index].Cells[1].Value.ToString();
                txtGSM.Text = DataGridFabricDet.Rows[Index].Cells[2].Value.ToString();
                txtTalarance.Text = DataGridFabricDet.Rows[Index].Cells[3].Value.ToString();
                txtStructureType.Tag = DataGridFabricDet.Rows[Index].Cells[4].Value.ToString();
                txtGSM.Tag = DataGridFabricDet.Rows[Index].Cells[4].Value.ToString();
                DataGridFabricDet.Rows.RemoveAt(Index);
                DataGridFabricDet.ClearSelection();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void DataGridFabricDet_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Delete)
                {
                    DialogResult result = MessageBox.Show("Do you want to delete this record", "Information", MessageBoxButtons.YesNo, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                    if (result == DialogResult.Yes)
                    {
                        int Index = DataGridFabricDet.SelectedCells[0].RowIndex;
                        string Query = "delete from OrderMFabricInOrderEntry Where OrderMUid =" + txtDocNo.Tag + " and ID =" + DataGridFabricDet.Rows[Index].Cells[5].Value.ToString() + "";
                        db.ExecuteNonQuery(CommandType.Text, Query, conn);
                        DataGridFabricDet.Rows.RemoveAt(Index);
                        DataGridFabricDet.ClearSelection();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void ChckRefresh_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (ChckRefresh.Checked == true)
                {
                    DialogResult dialogResult = MessageBox.Show("do you want to delete the Planning Qty", "Information", MessageBoxButtons.YesNo, MessageBoxIcon.Information, MessageBoxDefaultButton.Button2); ;
                    if (dialogResult == DialogResult.Yes)
                    {
                        string Query = "delete from OrderMPlanning Where OrderMuid =" + txtDocNo.Tag + "";
                        db.ExecuteNonQuery(CommandType.Text, Query, conn);
                        CmbPlanningStyleCombo_SelectedIndexChanged(sender, e);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void BtnDownloadtoExcel_Click(object sender, EventArgs e)
        {
            try
            {
                Microsoft.Office.Interop.Excel._Application app = new Microsoft.Office.Interop.Excel.Application();
                Microsoft.Office.Interop.Excel._Workbook workbook = app.Workbooks.Add(Type.Missing);
                Microsoft.Office.Interop.Excel._Worksheet worksheet = null;
                app.Visible = true;
                worksheet = workbook.Sheets["Sheet1"];
                worksheet = workbook.ActiveSheet;
                worksheet.Name = "Orders";
                for (int i = 0; i < dtOrdersGrid.Columns.Count; i++)
                {
                    if (i == 0 || i == 7 || i == 9 || i == 11 || i == 13 || i == 15)
                    {
                    }
                    else
                    {
                        worksheet.Cells[1, i] = dtOrdersGrid.Columns[i].ColumnName;
                    }

                }
                for (int i = 0; i < dtOrdersGrid.Rows.Count; i++)
                {
                    for (int j = 0; j < dtOrdersGrid.Columns.Count; j++)
                    {
                        if (j == 0 || j == 7 || j == 9 || j == 11 || j == 13 || j == 15)
                        {
                        }
                        else
                        {
                            worksheet.Cells[i + 2, j] = dtOrdersGrid.Rows[i][j].ToString();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }

        private void txtCrossValue_TextChanged(object sender, EventArgs e)
        {

        }

        private void TxtQuantity_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
                {
                    e.Handled = true;
                }
                // only allow one decimal point
                if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
                {
                    e.Handled = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }

        private void BtnUpdatePrice_Click(object sender, EventArgs e)
        {
            try
            {
                if (cmbPriceType.Text == "Style Wise")
                {
                    for (int i = 0; i < DataGridStylePrice.Rows.Count -1; i++)
                    {
                        SqlParameter[] sqlParameters = {
                                new SqlParameter("@PriceType","Style Wise"),
                                new SqlParameter("@StyleComboUid",DBNull.Value),
                                new SqlParameter("@StyleSizeUid",DBNull.Value),
                                new SqlParameter("@OrderStyleUid",DataGridStylePrice.Rows[i].Cells[3].Value.ToString()),
                                new SqlParameter("@Price",DataGridStylePrice.Rows[i].Cells[2].Value.ToString()),
                                new SqlParameter("@OrderMuid",txtDocNo.Tag)
                        };
                        db.ExecuteNonQuery(CommandType.StoredProcedure, "Proc_OrderMPrice", sqlParameters, conn);
                    }
                }
                else if (cmbPriceType.Text == "Combo wise")
                {
                    for (int i = 0; i < DataGridStylePrice.Rows.Count -1; i++)
                    {
                        SqlParameter[] sqlParameters = {
                            new SqlParameter("@PriceType","Combo wise"),
                            new SqlParameter("@StyleComboUid",DataGridStylePrice.Rows[i].Cells[3].Value.ToString()),
                            new SqlParameter("@StyleSizeUid",DBNull.Value),
                            new SqlParameter("@OrderStyleUid",DBNull.Value),
                            new SqlParameter("@Price",DataGridStylePrice.Rows[i].Cells[2].Value.ToString()),
                            new SqlParameter("@OrderMuid",txtDocNo.Tag)
                        };
                        db.ExecuteNonQuery(CommandType.StoredProcedure, "Proc_OrderMPrice", sqlParameters, conn);
                    }
                }
                else
                {
                    for (int i = 0; i < DataGridStylePrice.Rows.Count -1; i++)
                    {
                        SqlParameter[] sqlParameters = {
                            new SqlParameter("@PriceType","Size Wise"),
                            new SqlParameter("@StyleComboUid",DataGridStylePrice.Rows[i].Cells[5].Value.ToString()),
                            new SqlParameter("@StyleSizeUid",DataGridStylePrice.Rows[i].Cells[6].Value.ToString()),
                            new SqlParameter("@OrderStyleUid",cmbStylePrice.SelectedValue),
                            new SqlParameter("@Price",DataGridStylePrice.Rows[i].Cells[4].Value.ToString()),
                            new SqlParameter("@OrderMuid",txtDocNo.Tag)
                            };
                        db.ExecuteNonQuery(CommandType.StoredProcedure, "Proc_OrderMPrice", sqlParameters, conn);
                    }
                    MessageBox.Show("Price Updated Successfully !", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                
            }
            catch (Exception)
            {

                throw;
            }
        }
    }

    public enum OrderEntryGeneralM
    {
        Season = 16,
        Merchindiser = 17,
        OrderStatus = 18,
        OrderType = 19,
        Dept = 20,
        Agent = 21,
        Currency = 22,
        ShipType = 23,
        ShiftMode = 24,
        PyMode = 25,
        Country = 26,
        Combo = 27
    }
}