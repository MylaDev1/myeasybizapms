﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Windows.Forms;
using Syncfusion.WinForms.Core.Enums;
using Syncfusion.WinForms.DataGrid;
using Syncfusion.WinForms.DataGrid.Events;

namespace MyEasyBizAPMS
{
    public partial class FrmYarnMaster : Form
    {
        public FrmYarnMaster()
        {
            InitializeComponent();
            this.SfDataGridYarn.Style.HeaderStyle.Font.FontStyle = FontStyle.Bold;
            this.SfDataGridYarn.Style.HeaderStyle.BackColor = Color.DeepSkyBlue;
        }

        RowAutoFitOptions autoFitOptions = new RowAutoFitOptions();
        List<string> excludeColumns = new List<string>();
        public int SelectId = 0;
        public bool EditMode = false;
        BindingSource bsYarn = new BindingSource();
        SQLDBHelper db = new SQLDBHelper();
        SqlConnection conn = new SqlConnection(GeneralParameters.ConnectionString);
        public static int TypeMUid { get; set; } = 0;

        private void FrmYarnMaster_Load(object sender, EventArgs e)
        {
            TypeMUid = 26;
            chckAc.Checked = true;
            GetData();
            Genclass.buttonstyleform(this);
            Genclass.buttonstylepanel(panadd);
            LoadButton(0);
            Uom();
            LoadTax();
            LoadMixingGrid();
        }

        protected void LoadMixingGrid()
        {
            try
            {
                DataGridMixing.AutoGenerateColumns = false;
                DataGridMixing.ColumnCount = 5;
                DataGridMixing.Columns[0].Name = "SlNo";
                DataGridMixing.Columns[0].HeaderText = "SlNo";
                DataGridMixing.Columns[0].Width = 50;

                DataGridMixing.Columns[1].Name = "Type";
                DataGridMixing.Columns[1].HeaderText = "Type";
                DataGridMixing.Columns[1].Width = 170;

                DataGridMixing.Columns[2].Name = "Mixing %";
                DataGridMixing.Columns[2].HeaderText = "Mixing %";
                DataGridMixing.Columns[2].Width = 50;

                DataGridMixing.Columns[3].Name = "typeUid";
                DataGridMixing.Columns[3].HeaderText = "typeUid";
                DataGridMixing.Columns[3].Visible = false;

                DataGridMixing.Columns[4].Name = "Uid";
                DataGridMixing.Columns[4].HeaderText = "Uid";
                DataGridMixing.Columns[4].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        public void Uom()
        {
            conn.Open();
            string qur = "select Generalname as Uom,Guid from generalm where typemuid = 7";
            SqlCommand cmd = new SqlCommand(qur, conn);
            SqlDataAdapter apt = new SqlDataAdapter(cmd);
            DataTable tab = new DataTable();
            apt.Fill(tab);
            conn.Close();
            CmbUOM.DataSource = null;
            CmbUOM.DisplayMember = "Uom";
            CmbUOM.ValueMember = "Guid";
            CmbUOM.DataSource = tab;
        }

        protected void GetData()
        {
            try
            {
                SelectId = 1;
                DataSet ds = db.GetDataWithoutParamMultiple(CommandType.StoredProcedure, "SP_GETYARNTYPEMASTER", conn);
                DataTable dtPLY = ds.Tables[0];
                DataTable dtCOUNT = ds.Tables[1];
                DataTable dtYARNTYPE = ds.Tables[2];
                DataTable dtCTNPC = ds.Tables[3];
                DataTable dtBLEND = ds.Tables[4];
                DataTable dtPurity = ds.Tables[5];


                CmbItemType.DataSource = null;
                CmbItemType.DisplayMember = "GeneralName";
                CmbItemType.ValueMember = "Guid";
                CmbItemType.DataSource = dtPLY;
                CmbItemType.SelectedIndex = -1;

                CmbCount.DataSource = null;
                CmbCount.DisplayMember = "GeneralName";
                CmbCount.ValueMember = "Guid";
                CmbCount.DataSource = dtCOUNT;
                CmbCount.SelectedIndex = -1;

                CmbYarnType.DataSource = null;
                CmbYarnType.DisplayMember = "GeneralName";
                CmbYarnType.ValueMember = "Guid";
                CmbYarnType.DataSource = dtYARNTYPE;
                CmbYarnType.SelectedIndex = -1;

                CmbCategory.DataSource = null;
                CmbCategory.DisplayMember = "GeneralName";
                CmbCategory.ValueMember = "Guid";
                CmbCategory.DataSource = dtCTNPC;
                CmbCategory.SelectedIndex = -1;

                CmbPurity.DataSource = null;
                CmbPurity.DisplayMember = "GeneralName";
                CmbPurity.ValueMember = "Guid";
                CmbPurity.DataSource = dtPurity;
                CmbPurity.SelectedIndex = -1;

                CmbBlendMixing.DataSource = null;
                CmbBlendMixing.DisplayMember = "GeneralName";
                CmbBlendMixing.ValueMember = "Guid";
                CmbBlendMixing.DataSource = dtBLEND;
                CmbBlendMixing.SelectedIndex = -1;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected void LoadButton(int id)
        {
            try
            {
                if (id == 0)
                {
                    grBack.Visible = false;
                    grFront.Visible = true;
                    btnAdd.Visible = true;
                    btnEdit.Visible = true;
                    btnExit.Visible = true;
                    btnFirst.Visible = true;
                    btnBack.Visible = true;
                    btnNxt.Visible = true;
                    btnLast.Visible = true;
                    PanelgridNos.Visible = true;
                    btnSave.Visible = false;
                    btnAddCancel.Visible = false;
                    btnSave.Text = "Save";
                    chckAc.Visible = true;
                }
                else if (id == 1)
                {
                    grBack.Visible = true;
                    grFront.Visible = false;
                    btnAdd.Visible = false;
                    btnEdit.Visible = false;
                    btnExit.Visible = false;
                    btnFirst.Visible = false;
                    btnBack.Visible = false;
                    btnNxt.Visible = false;
                    btnLast.Visible = false;
                    PanelgridNos.Visible = false;
                    btnSave.Visible = true;
                    btnAddCancel.Visible = true;
                    btnSave.Text = "Save";
                    chckAc.Visible = false;
                }

                else if (id == 2)
                {
                    grBack.Visible = true;
                    grFront.Visible = false;
                    btnAdd.Visible = false;
                    btnEdit.Visible = false;
                    btnExit.Visible = false;
                    btnFirst.Visible = false;
                    btnBack.Visible = false;
                    btnNxt.Visible = false;
                    btnLast.Visible = false;
                    PanelgridNos.Visible = false;
                    btnSave.Visible = true;
                    btnAddCancel.Visible = true;
                    btnSave.Text = "Update";
                    chckAc.Visible = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void BtnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                ClearTextBoxes();
                CmbTax.SelectedIndex = -1;
                CmbUOM.SelectedIndex = -1;
                LoadButton(1);
                chckActive.Checked = true;
                txtItemSpec.Text = string.Empty;
                txtItemSpec.Tag = "0";
                DataGridMixing.Rows.Clear();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void BtnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtItemSpec.Text != string.Empty)
                {
                    int Active;

                    if (chckActive.Checked == true)
                    {
                        Active = 1;
                    }
                    else
                    {
                        Active = 0;
                    }
                    if (btnSave.Text == "Save")
                    {
                        SqlParameter[] paracheck = { new SqlParameter("@Value", txtItemSpec.Text) };
                        DataTable dt = db.GetDataWithParam(CommandType.StoredProcedure, "SP_CheckDuplicate", paracheck, conn);

                        if (dt.Rows.Count != 0)
                        {
                            MessageBox.Show("Entry already found", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            return;
                        }
                    }
                    SqlParameter[] para = {
                                new SqlParameter("@YarnName",txtItemSpec.Text),
                                new SqlParameter("@YarnShortName",TxtShade.Text),
                                new SqlParameter("@Uom",CmbUOM.SelectedValue),
                                new SqlParameter("@HSNCode",TxtHsnCode.Text),
                                new SqlParameter("@Tax",CmbTax.SelectedValue),
                                new SqlParameter("@Active",Active),
                                new SqlParameter("@YarnItemTypeId",CmbItemType.SelectedValue),
                                new SqlParameter("@YarnCtgyId",CmbCategory.SelectedValue),
                                new SqlParameter("@YarnTypeId",CmbYarnType.SelectedValue),
                                new SqlParameter("@YarnBled","0"),
                                new SqlParameter("@YarnCount",CmbCount.SelectedValue),
                                new SqlParameter("@YarnPurity",CmbPurity.SelectedValue),
                                new SqlParameter("@YarnId",txtItemSpec.Tag),
                                new SqlParameter("@ReturnID",SqlDbType.Decimal),
                                new SqlParameter("@OpeningStk",Convert.ToDecimal(txtOpeningStk.Text))
                            };
                    para[13].Direction = ParameterDirection.Output;
                    int uid = db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_YarnMaster", para, conn, 13);
                    for (int i = 0; i < DataGridMixing.Rows.Count; i++)
                    {
                        SqlParameter[] sqlParameters = {
                            new SqlParameter("@Uid",DataGridMixing.Rows[i].Cells[4].Value.ToString()),
                            new SqlParameter("@YarnId",uid),
                            new SqlParameter("@TypeId",DataGridMixing.Rows[i].Cells[3].Value.ToString()),
                            new SqlParameter("@Mixing",DataGridMixing.Rows[i].Cells[2].Value.ToString())
                        };
                        db.ExecuteNonQuery(CommandType.StoredProcedure, "Proc_YarnBlend", sqlParameters, conn);
                    }
                    MessageBox.Show("Record has been saved successfully", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    ClearTextBoxes();
                    CmbTax.SelectedIndex = -1;
                    CmbUOM.SelectedIndex = -1;
                    LoadButton(0);
                    LoadDatatable();

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected void LoadDatatable()
        {
            try
            {
                int Active;
                if (chckAc.Checked == true)
                {
                    Active = 1;
                }
                else
                {
                    Active = 0;
                }
                SqlParameter[] para = { new SqlParameter("@Active", Active) };
                DataTable dt = db.GetDataWithParam(CommandType.StoredProcedure, "SP_GetYarnMaster", para, conn);
                SfDataGridYarn.DataSource = dt;
                lblCount.Text = "Of " + dt.Rows.Count.ToString();
                SfDataGridYarn.Columns[0].Visible = false;
                SfDataGridYarn.Columns[3].Visible = false;
                SfDataGridYarn.Columns[4].Visible = false;
                SfDataGridYarn.Columns[5].Visible = false;
                SfDataGridYarn.Columns[6].Visible = false;
                SfDataGridYarn.Columns[7].Visible = false;
                SfDataGridYarn.Columns[8].Visible = false;
                SfDataGridYarn.Columns[9].Visible = false;
                SfDataGridYarn.Columns[10].Visible = false;
                SfDataGridYarn.Columns[11].Visible = false;
                SfDataGridYarn.Columns[12].Visible = false;
                SfDataGridYarn.Columns[13].Visible = false;
                SfDataGridYarn.Columns[14].Visible = false;
                SfDataGridYarn.Columns[15].Visible = false;
                SfDataGridYarn.Columns[16].Visible = false;
                SfDataGridYarn.Columns[17].Visible = false;
                SfDataGridYarn.Columns[18].Visible = false;
                SfDataGridYarn.Columns[1].Width = 450;
                SfDataGridYarn.Columns[2].Width = 200;
                foreach (var column in this.SfDataGridYarn.Columns)
                {
                    if (!column.MappingName.Equals("YarnName") && !column.MappingName.Equals("Uom"))
                        excludeColumns.Add(column.MappingName);
                }

                (SfDataGridYarn.Columns["YarnName"] as GridTextColumn).AllowTextWrapping = true;
                SfDataGridYarn.QueryRowHeight += SfDataGrid_QueryRowHeight;
                gridRowResizingOptions.ExcludeColumns = excludeColumns;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        int height;
        RowAutoFitOptions gridRowResizingOptions = new RowAutoFitOptions();
        void SfDataGrid_QueryRowHeight(object sender, QueryRowHeightEventArgs e)
        {
            if (this.SfDataGridYarn.TableControl.IsTableSummaryIndex(e.RowIndex))
            {
                e.Height = 40;
                e.Handled = true;
            }
            else if (this.SfDataGridYarn.AutoSizeController.GetAutoRowHeight(e.RowIndex, gridRowResizingOptions, out height))
            {
                if (height > this.SfDataGridYarn.RowHeight)
                {
                    e.Height = height;
                    e.Handled = true;
                }
            }
        }

        private void BtnAddCancel_Click(object sender, EventArgs e)
        {
            GetData();
            LoadButton(0);
        }

        private void BtnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                ClearTextBoxes();
                DataGridMixing.Rows.Clear();
                var selectedItem = SfDataGridYarn.SelectedItems[0];
                var dataRow = (selectedItem as DataRowView).Row;
                EditMode = true;
                SelectId = 1;
                CmbItemType.SelectedValue = dataRow["YarnItemTypeId"].ToString();
                CmbCategory.SelectedValue = dataRow["YarnCtgyId"].ToString();
                CmbYarnType.SelectedValue = dataRow["YarnTypeId"].ToString();
                txtBlend.Tag = dataRow["YarnBled"].ToString();
                CmbCount.SelectedValue = dataRow["YarnCount"].ToString();
                CmbPurity.SelectedValue = dataRow["YarnPurity"].ToString();
                TxtShade.Text = dataRow["YarnShortName"].ToString();
                txtItemSpec.Text = dataRow["YarnName"].ToString();
                txtItemSpec.Tag = dataRow["YarnId"].ToString();
                TxtHsnCode.Text = dataRow["HSNCode"].ToString();
                CmbTax.SelectedValue = dataRow["Tax"].ToString();
                CmbUOM.SelectedValue = dataRow["Uom"].ToString();
                txtOpeningStk.Text = dataRow["OpeningStk"].ToString();
                bool Active = Convert.ToBoolean(dataRow["Active"].ToString());
                if (Active == true)
                {
                    chckActive.Checked = true;
                }
                else
                {
                    chckActive.Checked = false;
                }
                string Query = @"Select * from YarnBlend a
                                Inner join GeneralM b on a.TypeId = b.GUid and b.TypeMUid = 5
                                Where a.Yarnid =" + txtItemSpec.Tag + "";
                DataTable dataTable = db.GetDataWithoutParam(CommandType.Text, Query, conn);
                for (int i = 0; i < dataTable.Rows.Count; i++)
                {
                    int Index = DataGridMixing.Rows.Add();
                    DataGridViewRow row = (DataGridViewRow)DataGridMixing.Rows[Index];
                    row.Cells[0].Value = i + 1;
                    row.Cells[1].Value = dataTable.Rows[i]["GeneralName"].ToString();
                    row.Cells[2].Value = dataTable.Rows[i]["Mixing"].ToString();
                    row.Cells[3].Value = dataTable.Rows[i]["TypeId"].ToString();
                    row.Cells[4].Value = dataTable.Rows[i]["Uid"].ToString();
                }
                BillYarnName();
                LoadButton(2);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void ChckAc_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (chckAc.Checked == true)
                {
                    LoadDatatable();
                }
                else
                {
                    LoadDatatable();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void BtnReset_Click(object sender, EventArgs e)
        {
            CmbItemType.SelectedIndex = -1;
            CmbYarnType.SelectedIndex = -1;
            CmbCount.SelectedIndex = -1;
            CmbCategory.SelectedIndex = -1;
            CmbBlendMixing.SelectedIndex = -1;
            CmbPurity.SelectedIndex = -1;
            txtItemSpec.Text = string.Empty;
        }

        protected void LoadTax()
        {
            try
            {
                DataTable dt = GetTax();
                CmbTax.DataSource = null;
                CmbTax.DisplayMember = "GeneralName";
                CmbTax.ValueMember = "GUid";
                CmbTax.DataSource = dt;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected DataTable GetTax()
        {
            DataTable dt = new DataTable();
            try
            {
                dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "SP_GetTax", conn);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            return dt;
        }

        private void Button1_Click(object sender, EventArgs e)
        {

        }

        private void CBoxItemType_Leave(object sender, EventArgs e)
        {
            BillYarnName();
        }

        protected void BillYarnName()
        {
            try
            {
                string txt = string.Empty;
                for (int i = 0; i < DataGridMixing.Rows.Count; i++)
                {
                    if (DataGridMixing.Rows.Count == 0)
                    {
                        txt = DataGridMixing.Rows[i].Cells[1].Value.ToString() + " " + DataGridMixing.Rows[i].Cells[2].Value.ToString() + "%";
                    }
                    else
                    {
                        txt = txt + " " + DataGridMixing.Rows[i].Cells[1].Value.ToString() + " " + DataGridMixing.Rows[i].Cells[2].Value.ToString() + "%";
                    }
                }
                if (CmbYarnType.Text == "Mixed")
                {
                    txtItemSpec.Text = CmbCount.Text + " " + CmbPurity.Text + " " + CmbCategory.Text + " " + "(" + txt + " )" + TxtShade.Text;
                }
                else
                {
                    txtItemSpec.Text = CmbCount.Text + " " + CmbPurity.Text + " " + CmbCategory.Text + " " + txtBlend.Text + " " + TxtShade.Text;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void CBoxCategory_Leave(object sender, EventArgs e)
        {
            BillYarnName();
        }

        private void CBoxYarnType_Leave(object sender, EventArgs e)
        {
            BillYarnName();
            if (CmbYarnType.Text == "Mixed")
            {
                CmbBlendMixing.Visible = true;
                txtRatio.Visible = true;
                BtnMixingOk.Visible = true;
                DataGridMixing.Visible = true;
                txtBlend.Enabled = false;
            }
            else
            {
                CmbBlendMixing.Visible = false;
                txtRatio.Visible = false;
                BtnMixingOk.Visible = false;
                DataGridMixing.Visible = false;
                txtBlend.Enabled = true;
            }
        }

        private void CBoxBlend_Leave(object sender, EventArgs e)
        {
            BillYarnName();
        }

        private void CBoxCount_Leave(object sender, EventArgs e)
        {
            BillYarnName();
        }

        private void CBoxPurity_Leave(object sender, EventArgs e)
        {
            BillYarnName();
        }

        private void TxtShade_TextChanged(object sender, EventArgs e)
        {
            BillYarnName();
        }

        private void SfDataGridYarn_DrawCell(object sender, Syncfusion.WinForms.DataGrid.Events.DrawCellEventArgs e)
        {
            if (e.RowIndex == 0)
            {
                e.Style.Interior = new Syncfusion.WinForms.Core.BrushInfo(GradientStyle.Vertical, Color.FromArgb(255, 229, 201), Color.FromArgb(255, 153, 52));
            }

            else if (e.ColumnIndex == 0)
            {
                e.Style.Interior = new Syncfusion.WinForms.Core.BrushInfo(GradientStyle.Horizontal, Color.White, Color.FromArgb(102, 110, 152));
            }
        }

        public void ClearTextBoxes(bool searchRecursively = true)
        {
            void clearTextBoxes(Control.ControlCollection controls, bool searchChildren)
            {
                foreach (Control c in controls)
                {
                    TextBox txt = c as TextBox;
                    txt?.Clear();
                    if (searchChildren && c.HasChildren)
                        clearTextBoxes(c.Controls, true);
                }
            }
            clearTextBoxes(this.Controls, searchRecursively);
        }

        private void BtnMixingOk_Click(object sender, EventArgs e)
        {
            try
            {
                if (CmbBlendMixing.Text != string.Empty && txtRatio.Text != string.Empty)
                {
                    string txt = string.Empty;
                    decimal sum = 0;
                    for (int i = 0; i < DataGridMixing.Rows.Count; i++)
                    {
                        if (DataGridMixing.Rows.Count == 0)
                        {
                            txt = DataGridMixing.Rows[i].Cells[1].Value.ToString() + " " + DataGridMixing.Rows[i].Cells[2].Value.ToString() + "%";
                        }
                        else
                        {
                            txt = txt + " " + DataGridMixing.Rows[i].Cells[1].Value.ToString() + " " + DataGridMixing.Rows[i].Cells[2].Value.ToString() + "%";
                        }
                        sum += Convert.ToDecimal(DataGridMixing.Rows[i].Cells[2].Value.ToString());

                    }
                    sum += Convert.ToDecimal(txtRatio.Text);
                    if (sum <= 100)
                    {
                        txt = txt + " " + CmbBlendMixing.Text + " " + txtRatio.Text + "%";
                        int Index = DataGridMixing.Rows.Add();
                        DataGridViewRow row = (DataGridViewRow)DataGridMixing.Rows[Index];
                        row.Cells[0].Value = DataGridMixing.Rows.Count;
                        row.Cells[1].Value = CmbBlendMixing.Text;
                        row.Cells[2].Value = txtRatio.Text;
                        row.Cells[3].Value = CmbBlendMixing.SelectedValue;
                        row.Cells[4].Value = "0";
                        BillYarnName();
                    }
                    else
                    {
                        MessageBox.Show("Ration Exeed 100%", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                }
                CmbBlendMixing.SelectedIndex = -1;
                txtRatio.Text = string.Empty;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }

        private void CmbYarnType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BillYarnName();
                if (CmbYarnType.Text == "Mixed")
                {
                    CmbBlendMixing.Visible = true;
                    txtRatio.Visible = true;
                    BtnMixingOk.Visible = true;
                    DataGridMixing.Visible = true;
                    txtBlend.Enabled = false;
                }
                else
                {
                    CmbBlendMixing.Visible = false;
                    txtRatio.Visible = false;
                    BtnMixingOk.Visible = false;
                    DataGridMixing.Visible = false;
                    txtBlend.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }

        private void CmbCount_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BillYarnName();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }

        private void CmbPurity_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BillYarnName();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }
    }
}
