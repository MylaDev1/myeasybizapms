﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.OleDb;
using System.Configuration;
using System.Data.SqlClient;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Windows.Forms;



namespace MyEasyBizAPMS
{
    public partial class Crviewer : Form
    {
        //private System.Windows.Forms.TabControl tabControl1;
        //private System.Windows.Forms.TabPage DataGridView;
        //private System.Windows.Forms.TabPage CrystalReportView;
        private CrystalDecisions.Windows.Forms.CrystalReportViewer Cryview;
        //private CreatingCrystalReports.myDataset myDataset1;
        //private System.Data.OleDb.OleDbDataAdapter oleDbDataAdapter1;
        //private System.Data.OleDb.OleDbCommand oleDbSelectCommand1;
        //private System.Data.OleDb.OleDbCommand oleDbInsertCommand1;
        //private System.Data.OleDb.OleDbCommand oleDbUpdateCommand1;
        //private System.Data.OleDb.OleDbCommand oleDbDeleteCommand1;
        //private System.Data.OleDb.OleDbConnection oleDbConnection1;
        //private System.Windows.Forms.DataGrid dataGrid1;

        private System.ComponentModel.Container components = null;

        ReportDocument doc = new ReportDocument();


        public Crviewer()
        {
            InitializeComponent();
        }

        SqlConnection conn = new SqlConnection(GeneralParameters.ConnectionString);

        //SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connstr"].ConnectionString);
        SqlCommand qur = new SqlCommand();
        private void createReport()
        {



        }



        private void Cryview_Load(object sender, EventArgs e)
        {
            conn.Close();
            conn.Open();


            if (Genclass.Dtype == 7 && Genclass.address == "Yarn Purchase" && Genclass.type == 7)
            {
                Genclass.strsql = "select * from Vw_salprtexlfin   where muid=" + Genclass.Prtid + " and doctypeid=7";

                Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
                DataTable tap = new DataTable();
                aptr.Fill(tap);
                if (tap.Rows.Count > 0)
                {
                    if (tap.Rows[0]["bstate"].ToString() == "Tamilnadu")
                    {


                        SqlDataAdapter da = new SqlDataAdapter("sp_pdf", conn);
                        da.SelectCommand.CommandType = CommandType.StoredProcedure;
                        da.SelectCommand.Parameters.Add("@Muid", SqlDbType.Int).Value = Genclass.Prtid;
                        da.SelectCommand.Parameters.Add("@slno", SqlDbType.Int).Value = Genclass.slno;
                        da.SelectCommand.Parameters.Add("@companyid", SqlDbType.Int).Value = GeneralParameters.UserdId;
                        da.SelectCommand.Parameters.Add("@doctypeid", SqlDbType.Int).Value = 7;
                        DataSet ds = new DataSet();
                        da.Fill(ds, "salesinvoice");


                        doc.Load(Application.StartupPath + "\\PurchaseOrdertemp.rpt");


                        SqlDataAdapter da2 = new SqlDataAdapter("SP_SGSTTAX", conn);
                        da2.SelectCommand.CommandType = CommandType.StoredProcedure;
                        da2.SelectCommand.Parameters.Add("@uid", SqlDbType.Int).Value = Genclass.Prtid;

                        da2.SelectCommand.Parameters.Add("@doctypeid", SqlDbType.Int).Value = 7;
                        DataTable ds2 = new DataTable("SGSTTAX");
                        da2.Fill(ds2);
                        ds.Tables.Add(ds2);

                        doc.Subreports["CrySgst.rpt"].SetDataSource(ds2);
                        doc.SetDataSource(ds);


                        SqlDataAdapter da1 = new SqlDataAdapter("terms", conn);
                        da1.SelectCommand.CommandType = CommandType.StoredProcedure;
                        da1.SelectCommand.Parameters.Add("@uid", SqlDbType.Int).Value = Genclass.Prtid;
                        DataTable ds1 = new DataTable("Terms");
                        da1.Fill(ds1);
                        ds.Tables.Add(ds1);
                        doc.Subreports["Termrpt1.rpt"].SetDataSource(ds1);
                        doc.SetDataSource(ds);
                        Cryview.ReportSource = doc;

                    }

                    else
                    {
                        SqlDataAdapter da = new SqlDataAdapter("sp_pdfOTHERSTATEFabric", conn);
                        da.SelectCommand.CommandType = CommandType.StoredProcedure;
                        da.SelectCommand.Parameters.Add("@Muid", SqlDbType.Int).Value = Genclass.Prtid;
                        //da.SelectCommand.Parameters.Add("@type", SqlDbType.Int).Value = 2;
                        da.SelectCommand.Parameters.Add("@slno", SqlDbType.Int).Value = Genclass.slno;
                        da.SelectCommand.Parameters.Add("@companyid", SqlDbType.Int).Value = GeneralParameters.UserdId;
                        da.SelectCommand.Parameters.Add("@doctypeid", SqlDbType.Int).Value = 7;
                        DataSet ds = new DataSet();
                        da.Fill(ds, "salesinvoice");

                        doc.Load(Application.StartupPath + "\\PurchaseOrdertemp.rpt");


                        SqlDataAdapter da2 = new SqlDataAdapter("SP_IGSTTAX", conn);
                        da2.SelectCommand.CommandType = CommandType.StoredProcedure;
                        da2.SelectCommand.Parameters.Add("@uid", SqlDbType.Int).Value = Genclass.Prtid;
                        da2.SelectCommand.Parameters.Add("@doctypeid", SqlDbType.Int).Value = 7;
                        DataTable ds2 = new DataTable("IGSTTAX");
                        da2.Fill(ds2);
                        ds.Tables.Add(ds2);

                        doc.Subreports["Cryigst.rpt"].SetDataSource(ds2);
                        doc.SetDataSource(ds);


                        SqlDataAdapter da1 = new SqlDataAdapter("terms", conn);
                        da1.SelectCommand.CommandType = CommandType.StoredProcedure;
                        da1.SelectCommand.Parameters.Add("@uid", SqlDbType.Int).Value = Genclass.Prtid;
                        DataTable ds1 = new DataTable("Terms");
                        da1.Fill(ds1);
                        ds.Tables.Add(ds1);




                        doc.Subreports["Termrpt1.rpt"].SetDataSource(ds1);
                        doc.SetDataSource(ds);
                        Cryview.ReportSource = doc;
                    }
                }
            }
            else if (Genclass.Dtype == 7 && Genclass.address == "Trims Purchase" && Genclass.type == 7)
            {
                Genclass.strsql = "select * from Vw_salprtexlfintrims   where muid=" + Genclass.Prtid + "  and doctypeid=7";

                Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
                DataTable tap = new DataTable();
                aptr.Fill(tap);
                if (tap.Rows.Count > 0)
                {
                    if (tap.Rows[0]["bstate"].ToString() == "Tamilnadu")
                    {


                        SqlDataAdapter da = new SqlDataAdapter("sp_PurchaseTrims", conn);
                        da.SelectCommand.CommandType = CommandType.StoredProcedure;
                        da.SelectCommand.Parameters.Add("@Muid", SqlDbType.Int).Value = Genclass.Prtid;
                        da.SelectCommand.Parameters.Add("@slno", SqlDbType.Int).Value = Genclass.slno;
                        da.SelectCommand.Parameters.Add("@companyid", SqlDbType.Int).Value = GeneralParameters.UserdId;
                        da.SelectCommand.Parameters.Add("@doctypeid", SqlDbType.Int).Value = 7;
                        DataSet ds = new DataSet();
                        da.Fill(ds, "salesinvoice");


                        doc.Load(Application.StartupPath + "\\PurchaseOrdertemp.rpt");


                        SqlDataAdapter da2 = new SqlDataAdapter("SP_SGSTTAX", conn);
                        da2.SelectCommand.CommandType = CommandType.StoredProcedure;
                        da2.SelectCommand.Parameters.Add("@uid", SqlDbType.Int).Value = Genclass.Prtid;

                        da2.SelectCommand.Parameters.Add("@doctypeid", SqlDbType.Int).Value = 7;
                        DataTable ds2 = new DataTable("SGSTTAX");
                        da2.Fill(ds2);
                        ds.Tables.Add(ds2);

                        doc.Subreports["CrySgst.rpt"].SetDataSource(ds2);
                        doc.SetDataSource(ds);



                        SqlDataAdapter da1 = new SqlDataAdapter("terms", conn);
                        da1.SelectCommand.CommandType = CommandType.StoredProcedure;
                        da1.SelectCommand.Parameters.Add("@uid", SqlDbType.Int).Value = Genclass.Prtid;
                        DataTable ds1 = new DataTable("Terms");
                        da1.Fill(ds1);
                        ds.Tables.Add(ds1);





                        doc.Subreports["Termrpt1.rpt"].SetDataSource(ds1);
                        doc.SetDataSource(ds);
                        Cryview.ReportSource = doc;

                    }

                    else
                    {
                        SqlDataAdapter da = new SqlDataAdapter("sp_otherstateTrims", conn);
                        da.SelectCommand.CommandType = CommandType.StoredProcedure;
                        da.SelectCommand.Parameters.Add("@Muid", SqlDbType.Int).Value = Genclass.Prtid;
                        //da.SelectCommand.Parameters.Add("@type", SqlDbType.Int).Value = 2;
                        da.SelectCommand.Parameters.Add("@slno", SqlDbType.Int).Value = Genclass.slno;
                        da.SelectCommand.Parameters.Add("@companyid", SqlDbType.Int).Value = GeneralParameters.UserdId;
                        da.SelectCommand.Parameters.Add("@doctypeid", SqlDbType.Int).Value = 7;
                        DataSet ds = new DataSet();
                        da.Fill(ds, "salesinvoice");

                        doc.Load(Application.StartupPath + "\\PurchaseOrdertemp.rpt");


                        //Subreports.CrySgst.
                        SqlDataAdapter da2 = new SqlDataAdapter("SP_IGSTTAX", conn);
                        da2.SelectCommand.CommandType = CommandType.StoredProcedure;
                        da2.SelectCommand.Parameters.Add("@uid", SqlDbType.Int).Value = Genclass.Prtid;
                        da2.SelectCommand.Parameters.Add("@doctypeid", SqlDbType.Int).Value = 7;
                        DataTable ds2 = new DataTable("IGSTTAX");
                        da2.Fill(ds2);
                        ds.Tables.Add(ds2);
                        doc.Subreports["Cryigst.rpt"].SetDataSource(ds2);
                        doc.SetDataSource(ds);


                        SqlDataAdapter da1 = new SqlDataAdapter("terms", conn);
                        da1.SelectCommand.CommandType = CommandType.StoredProcedure;
                        da1.SelectCommand.Parameters.Add("@uid", SqlDbType.Int).Value = Genclass.Prtid;
                        DataTable ds1 = new DataTable("Terms");
                        da1.Fill(ds1);
                        ds.Tables.Add(ds1);




                        doc.Subreports["Termrpt1.rpt"].SetDataSource(ds1);
                        doc.SetDataSource(ds);
                        Cryview.ReportSource = doc;
                    }
                }
            }
            else if (Genclass.Dtype == 7 && Genclass.address == "Fabric Purchase" && Genclass.type == 7)
            {
                Genclass.strsql = "select * from Vw_salprtexlfintrims   where muid=" + Genclass.Prtid + "  and doctypeid=7";

                Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
                DataTable tap = new DataTable();
                aptr.Fill(tap);
                if (tap.Rows.Count > 0)
                {
                    if (tap.Rows[0]["bstate"].ToString() == "Tamilnadu")
                    {


                        SqlDataAdapter da = new SqlDataAdapter("sp_pdfFabric", conn);
                        da.SelectCommand.CommandType = CommandType.StoredProcedure;
                        da.SelectCommand.Parameters.Add("@Muid", SqlDbType.Int).Value = Genclass.Prtid;
                        da.SelectCommand.Parameters.Add("@slno", SqlDbType.Int).Value = Genclass.slno;
                        da.SelectCommand.Parameters.Add("@companyid", SqlDbType.Int).Value = GeneralParameters.UserdId;
                        da.SelectCommand.Parameters.Add("@doctypeid", SqlDbType.Int).Value = 7;
                        DataSet ds = new DataSet();
                        da.Fill(ds, "salesinvoice");


                        doc.Load(Application.StartupPath + "\\PurchaseOrdertemp.rpt");


                        SqlDataAdapter da2 = new SqlDataAdapter("SP_SGSTTAX", conn);
                        da2.SelectCommand.CommandType = CommandType.StoredProcedure;
                        da2.SelectCommand.Parameters.Add("@uid", SqlDbType.Int).Value = Genclass.Prtid;

                        da2.SelectCommand.Parameters.Add("@doctypeid", SqlDbType.Int).Value = 7;
                        DataTable ds2 = new DataTable("SGSTTAX");
                        da2.Fill(ds2);
                        ds.Tables.Add(ds2);

                        doc.Subreports["CrySgst.rpt"].SetDataSource(ds2);
                        doc.SetDataSource(ds);



                        SqlDataAdapter da1 = new SqlDataAdapter("terms", conn);
                        da1.SelectCommand.CommandType = CommandType.StoredProcedure;
                        da1.SelectCommand.Parameters.Add("@uid", SqlDbType.Int).Value = Genclass.Prtid;
                        DataTable ds1 = new DataTable("Terms");
                        da1.Fill(ds1);
                        ds.Tables.Add(ds1);





                        doc.Subreports["Termrpt1.rpt"].SetDataSource(ds1);
                        doc.SetDataSource(ds);
                        Cryview.ReportSource = doc;

                    }

                    else
                    {
                        SqlDataAdapter da = new SqlDataAdapter("sp_otherstateTrims", conn);
                        da.SelectCommand.CommandType = CommandType.StoredProcedure;
                        da.SelectCommand.Parameters.Add("@Muid", SqlDbType.Int).Value = Genclass.Prtid;
                        //da.SelectCommand.Parameters.Add("@type", SqlDbType.Int).Value = 2;
                        da.SelectCommand.Parameters.Add("@slno", SqlDbType.Int).Value = Genclass.slno;
                        da.SelectCommand.Parameters.Add("@companyid", SqlDbType.Int).Value = GeneralParameters.UserdId;
                        da.SelectCommand.Parameters.Add("@doctypeid", SqlDbType.Int).Value = 7;
                        DataSet ds = new DataSet();
                        da.Fill(ds, "salesinvoice");

                        doc.Load(Application.StartupPath + "\\PurchaseOrdertemp.rpt");


                        //Subreports.CrySgst.
                        SqlDataAdapter da2 = new SqlDataAdapter("SP_IGSTTAX", conn);
                        da2.SelectCommand.CommandType = CommandType.StoredProcedure;
                        da2.SelectCommand.Parameters.Add("@uid", SqlDbType.Int).Value = Genclass.Prtid;
                        da2.SelectCommand.Parameters.Add("@doctypeid", SqlDbType.Int).Value = 7;
                        DataTable ds2 = new DataTable("IGSTTAX");
                        da2.Fill(ds2);
                        ds.Tables.Add(ds2);
                        doc.Subreports["Cryigst.rpt"].SetDataSource(ds2);
                        doc.SetDataSource(ds);


                        SqlDataAdapter da1 = new SqlDataAdapter("terms", conn);
                        da1.SelectCommand.CommandType = CommandType.StoredProcedure;
                        da1.SelectCommand.Parameters.Add("@uid", SqlDbType.Int).Value = Genclass.Prtid;
                        DataTable ds1 = new DataTable("Terms");
                        da1.Fill(ds1);
                        ds.Tables.Add(ds1);




                        doc.Subreports["Termrpt1.rpt"].SetDataSource(ds1);
                        doc.SetDataSource(ds);
                        Cryview.ReportSource = doc;
                    }
                }
            }
            else if (Genclass.Dtype == 7 && Genclass.address == "Yarn Purchase" && Genclass.type == 11)
            {
                Genclass.strsql = "select * from Vw_salprtexlfin   where muid=" + Genclass.Prtid + " and doctypeid=7";

                Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
                DataTable tap = new DataTable();
                aptr.Fill(tap);
                if (tap.Rows.Count > 0)
                {
                    if (tap.Rows[0]["bstate"].ToString() == "Tamilnadu")
                    {


                        SqlDataAdapter da = new SqlDataAdapter("sp_pdf", conn);
                        da.SelectCommand.CommandType = CommandType.StoredProcedure;
                        da.SelectCommand.Parameters.Add("@Muid", SqlDbType.Int).Value = Genclass.Prtid;
                        da.SelectCommand.Parameters.Add("@slno", SqlDbType.Int).Value = Genclass.slno;
                        da.SelectCommand.Parameters.Add("@companyid", SqlDbType.Int).Value = GeneralParameters.UserdId;
                        da.SelectCommand.Parameters.Add("@doctypeid", SqlDbType.Int).Value = 7;
                        DataSet ds = new DataSet();
                        da.Fill(ds, "salesinvoice");


                        doc.Load(Application.StartupPath + "\\PurchaseOrderWithoutRat.rpt");


                        //SqlDataAdapter da2 = new SqlDataAdapter("SP_SGSTTAX", conn);
                        //da2.SelectCommand.CommandType = CommandType.StoredProcedure;
                        //da2.SelectCommand.Parameters.Add("@uid", SqlDbType.Int).Value = Genclass.Prtid;

                        //da2.SelectCommand.Parameters.Add("@doctypeid", SqlDbType.Int).Value = 7;
                        //DataTable ds2 = new DataTable("SGSTTAX");
                        //da2.Fill(ds2);
                        //ds.Tables.Add(ds2);

                        //doc.Subreports["CrySgst.rpt"].SetDataSource(ds2);
                        //doc.SetDataSource(ds);


                        SqlDataAdapter da1 = new SqlDataAdapter("terms", conn);
                        da1.SelectCommand.CommandType = CommandType.StoredProcedure;
                        da1.SelectCommand.Parameters.Add("@uid", SqlDbType.Int).Value = Genclass.Prtid;
                        DataTable ds1 = new DataTable("Terms");
                        da1.Fill(ds1);
                        ds.Tables.Add(ds1);
                        doc.Subreports["Termrpt1.rpt"].SetDataSource(ds1);
                        doc.SetDataSource(ds);
                        Cryview.ReportSource = doc;

                    }

                    else
                    {
                        SqlDataAdapter da = new SqlDataAdapter("sp_pdfOTHERSTATE", conn);
                        da.SelectCommand.CommandType = CommandType.StoredProcedure;
                        da.SelectCommand.Parameters.Add("@Muid", SqlDbType.Int).Value = Genclass.Prtid;
                        //da.SelectCommand.Parameters.Add("@type", SqlDbType.Int).Value = 2;
                        da.SelectCommand.Parameters.Add("@slno", SqlDbType.Int).Value = Genclass.slno;
                        da.SelectCommand.Parameters.Add("@companyid", SqlDbType.Int).Value = GeneralParameters.UserdId;
                        da.SelectCommand.Parameters.Add("@doctypeid", SqlDbType.Int).Value = 7;
                        DataSet ds = new DataSet();
                        da.Fill(ds, "salesinvoice");

                        doc.Load(Application.StartupPath + "\\PurchaseOrderWithoutRat.rpt");


                        //SqlDataAdapter da2 = new SqlDataAdapter("SP_IGSTTAX", conn);
                        //da2.SelectCommand.CommandType = CommandType.StoredProcedure;
                        //da2.SelectCommand.Parameters.Add("@uid", SqlDbType.Int).Value = Genclass.Prtid;
                        //da2.SelectCommand.Parameters.Add("@doctypeid", SqlDbType.Int).Value = 7;
                        //DataTable ds2 = new DataTable("IGSTTAX");
                        //da2.Fill(ds2);
                        //ds.Tables.Add(ds2);

                        //doc.Subreports["Cryigst.rpt"].SetDataSource(ds2);
                        //doc.SetDataSource(ds);


                        SqlDataAdapter da1 = new SqlDataAdapter("terms", conn);
                        da1.SelectCommand.CommandType = CommandType.StoredProcedure;
                        da1.SelectCommand.Parameters.Add("@uid", SqlDbType.Int).Value = Genclass.Prtid;
                        DataTable ds1 = new DataTable("Terms");
                        da1.Fill(ds1);
                        ds.Tables.Add(ds1);




                        doc.Subreports["Termrpt1.rpt"].SetDataSource(ds1);
                        doc.SetDataSource(ds);
                        Cryview.ReportSource = doc;
                    }
                }
            }
            else if (Genclass.Dtype == 7 && Genclass.address == "Trims Purchase" && Genclass.type == 11)
            {
                Genclass.strsql = "select * from Vw_salprtexlfintrims   where muid=" + Genclass.Prtid + "  and doctypeid=7";

                Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
                DataTable tap = new DataTable();
                aptr.Fill(tap);
                if (tap.Rows.Count > 0)
                {
                    if (tap.Rows[0]["bstate"].ToString() == "Tamilnadu")
                    {


                        SqlDataAdapter da = new SqlDataAdapter("sp_PurchaseTrims", conn);
                        da.SelectCommand.CommandType = CommandType.StoredProcedure;
                        da.SelectCommand.Parameters.Add("@Muid", SqlDbType.Int).Value = Genclass.Prtid;
                        da.SelectCommand.Parameters.Add("@slno", SqlDbType.Int).Value = Genclass.slno;
                        da.SelectCommand.Parameters.Add("@companyid", SqlDbType.Int).Value = GeneralParameters.UserdId;
                        da.SelectCommand.Parameters.Add("@doctypeid", SqlDbType.Int).Value = 7;
                        DataSet ds = new DataSet();
                        da.Fill(ds, "salesinvoice");


                        doc.Load(Application.StartupPath + "\\PurchaseOrderWithoutRat.rpt");


                        //SqlDataAdapter da2 = new SqlDataAdapter("SP_SGSTTAX", conn);
                        //da2.SelectCommand.CommandType = CommandType.StoredProcedure;
                        //da2.SelectCommand.Parameters.Add("@uid", SqlDbType.Int).Value = Genclass.Prtid;

                        //da2.SelectCommand.Parameters.Add("@doctypeid", SqlDbType.Int).Value = 7;
                        //DataTable ds2 = new DataTable("SGSTTAX");
                        //da2.Fill(ds2);
                        //ds.Tables.Add(ds2);

                        //doc.Subreports["CrySgst.rpt"].SetDataSource(ds2);
                        //doc.SetDataSource(ds);



                        SqlDataAdapter da1 = new SqlDataAdapter("terms", conn);
                        da1.SelectCommand.CommandType = CommandType.StoredProcedure;
                        da1.SelectCommand.Parameters.Add("@uid", SqlDbType.Int).Value = Genclass.Prtid;
                        DataTable ds1 = new DataTable("Terms");
                        da1.Fill(ds1);
                        ds.Tables.Add(ds1);





                        doc.Subreports["Termrpt1.rpt"].SetDataSource(ds1);
                        doc.SetDataSource(ds);
                        Cryview.ReportSource = doc;

                    }

                    else
                    {
                        SqlDataAdapter da = new SqlDataAdapter("sp_otherstateTrims", conn);
                        da.SelectCommand.CommandType = CommandType.StoredProcedure;
                        da.SelectCommand.Parameters.Add("@Muid", SqlDbType.Int).Value = Genclass.Prtid;
                        //da.SelectCommand.Parameters.Add("@type", SqlDbType.Int).Value = 2;
                        da.SelectCommand.Parameters.Add("@slno", SqlDbType.Int).Value = Genclass.slno;
                        da.SelectCommand.Parameters.Add("@companyid", SqlDbType.Int).Value = GeneralParameters.UserdId;
                        da.SelectCommand.Parameters.Add("@doctypeid", SqlDbType.Int).Value = 7;
                        DataSet ds = new DataSet();
                        da.Fill(ds, "salesinvoice");

                        doc.Load(Application.StartupPath + "\\PurchaseOrderWithoutRat.rpt");


                        //Subreports.CrySgst.
                        //SqlDataAdapter da2 = new SqlDataAdapter("SP_IGSTTAX", conn);
                        //da2.SelectCommand.CommandType = CommandType.StoredProcedure;
                        //da2.SelectCommand.Parameters.Add("@uid", SqlDbType.Int).Value = Genclass.Prtid;
                        //da2.SelectCommand.Parameters.Add("@doctypeid", SqlDbType.Int).Value = 7;
                        //DataTable ds2 = new DataTable("IGSTTAX");
                        //da2.Fill(ds2);
                        //ds.Tables.Add(ds2);
                        //doc.Subreports["Cryigst.rpt"].SetDataSource(ds2);
                        //doc.SetDataSource(ds);


                        SqlDataAdapter da1 = new SqlDataAdapter("terms", conn);
                        da1.SelectCommand.CommandType = CommandType.StoredProcedure;
                        da1.SelectCommand.Parameters.Add("@uid", SqlDbType.Int).Value = Genclass.Prtid;
                        DataTable ds1 = new DataTable("Terms");
                        da1.Fill(ds1);
                        ds.Tables.Add(ds1);




                        doc.Subreports["Termrpt1.rpt"].SetDataSource(ds1);
                        doc.SetDataSource(ds);
                        Cryview.ReportSource = doc;
                    }
                }
            }
            else if (Genclass.Dtype == 8 && Genclass.address == "Yarn Purchase" && Genclass.type != 10)
            {
                Genclass.strsql = "select * from Vw_salprtexlfin   where muid=" + Genclass.Prtid + " and doctypeid=8";

                Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
                DataTable tap = new DataTable();
                aptr.Fill(tap);
                if (tap.Rows.Count > 0)
                {
                    if (tap.Rows[0]["bstate"].ToString() == "Tamilnadu")
                    {


                        SqlDataAdapter da = new SqlDataAdapter("sp_pdf", conn);
                        da.SelectCommand.CommandType = CommandType.StoredProcedure;
                        da.SelectCommand.Parameters.Add("@Muid", SqlDbType.Int).Value = Genclass.Prtid;
                        da.SelectCommand.Parameters.Add("@slno", SqlDbType.Int).Value = Genclass.slno;
                        da.SelectCommand.Parameters.Add("@companyid", SqlDbType.Int).Value = GeneralParameters.UserdId;
                        da.SelectCommand.Parameters.Add("@doctypeid", SqlDbType.Int).Value = 8;
                        DataSet ds = new DataSet();
                        da.Fill(ds, "salesinvoice");


                        doc.Load(Application.StartupPath + "\\PurchaseOrderWithoutRat.rpt");


                        SqlDataAdapter da2 = new SqlDataAdapter("SP_SGSTTAX", conn);
                        da2.SelectCommand.CommandType = CommandType.StoredProcedure;
                        da2.SelectCommand.Parameters.Add("@uid", SqlDbType.Int).Value = Genclass.Prtid;

                        da2.SelectCommand.Parameters.Add("@doctypeid", SqlDbType.Int).Value = 8;
                        DataTable ds2 = new DataTable("SGSTTAX");
                        da2.Fill(ds2);
                        ds.Tables.Add(ds2);

                        doc.Subreports["CrySgst.rpt"].SetDataSource(ds2);
                        doc.SetDataSource(ds);


                        SqlDataAdapter da1 = new SqlDataAdapter("terms", conn);
                        da1.SelectCommand.CommandType = CommandType.StoredProcedure;
                        da1.SelectCommand.Parameters.Add("@uid", SqlDbType.Int).Value = Genclass.Prtid;
                        DataTable ds1 = new DataTable("Terms");
                        da1.Fill(ds1);
                        ds.Tables.Add(ds1);





                        doc.Subreports["Termrpt1.rpt"].SetDataSource(ds1);
                        doc.SetDataSource(ds);
                        Cryview.ReportSource = doc;

                    }

                    else
                    {
                        SqlDataAdapter da = new SqlDataAdapter("sp_pdfOTHERSTATE", conn);
                        da.SelectCommand.CommandType = CommandType.StoredProcedure;
                        da.SelectCommand.Parameters.Add("@Muid", SqlDbType.Int).Value = Genclass.Prtid;
                        //da.SelectCommand.Parameters.Add("@type", SqlDbType.Int).Value = 2;
                        da.SelectCommand.Parameters.Add("@slno", SqlDbType.Int).Value = Genclass.slno;
                        da.SelectCommand.Parameters.Add("@companyid", SqlDbType.Int).Value = GeneralParameters.UserdId;
                        da.SelectCommand.Parameters.Add("@doctypeid", SqlDbType.Int).Value = 8;
                        DataSet ds = new DataSet();
                        da.Fill(ds, "salesinvoice");

                        doc.Load(Application.StartupPath + "\\PurchaseOrderWithoutRat.rpt");


                        SqlDataAdapter da2 = new SqlDataAdapter("SP_IGSTTAX", conn);
                        da2.SelectCommand.CommandType = CommandType.StoredProcedure;
                        da2.SelectCommand.Parameters.Add("@uid", SqlDbType.Int).Value = Genclass.Prtid;
                        da2.SelectCommand.Parameters.Add("@doctypeid", SqlDbType.Int).Value = 8;
                        DataTable ds2 = new DataTable("IGSTTAX");
                        da2.Fill(ds2);
                        ds.Tables.Add(ds2);

                        doc.Subreports["Cryigst.rpt"].SetDataSource(ds2);
                        doc.SetDataSource(ds);


                        SqlDataAdapter da1 = new SqlDataAdapter("terms", conn);
                        da1.SelectCommand.CommandType = CommandType.StoredProcedure;
                        da1.SelectCommand.Parameters.Add("@uid", SqlDbType.Int).Value = Genclass.Prtid;
                        DataTable ds1 = new DataTable("Terms");
                        da1.Fill(ds1);
                        ds.Tables.Add(ds1);




                        doc.Subreports["Termrpt1.rpt"].SetDataSource(ds1);
                        doc.SetDataSource(ds);
                        Cryview.ReportSource = doc;
                    }
                }
            }
            else if (Genclass.Dtype == 8 && Genclass.address == "Trims Purchase" && Genclass.type != 10)
            {
                Genclass.strsql = "select * from Vw_salprtexlfinTrimsBillAcc   where muid=" + Genclass.Prtid + "  and doctypeid=8";

                Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
                DataTable tap = new DataTable();
                aptr.Fill(tap);
                if (tap.Rows.Count > 0)
                {
                    if (tap.Rows[0]["bstate"].ToString() == "Tamilnadu")
                    {


                        SqlDataAdapter da = new SqlDataAdapter("sp_PurchaseTrimsBillAccounting", conn);
                        da.SelectCommand.CommandType = CommandType.StoredProcedure;
                        da.SelectCommand.Parameters.Add("@Muid", SqlDbType.Int).Value = Genclass.Prtid;
                        da.SelectCommand.Parameters.Add("@slno", SqlDbType.Int).Value = Genclass.slno;
                        da.SelectCommand.Parameters.Add("@companyid", SqlDbType.Int).Value = GeneralParameters.UserdId;
                        da.SelectCommand.Parameters.Add("@doctypeid", SqlDbType.Int).Value = 8;
                        DataSet ds = new DataSet();
                        da.Fill(ds, "salesinvoice");


                        doc.Load(Application.StartupPath + "\\BillAcc.rpt");


                        SqlDataAdapter da2 = new SqlDataAdapter("SP_SGSTTAX", conn);
                        da2.SelectCommand.CommandType = CommandType.StoredProcedure;
                        da2.SelectCommand.Parameters.Add("@uid", SqlDbType.Int).Value = Genclass.Prtid;

                        da2.SelectCommand.Parameters.Add("@doctypeid", SqlDbType.Int).Value = 8;
                        DataTable ds2 = new DataTable("SGSTTAX");
                        da2.Fill(ds2);
                        ds.Tables.Add(ds2);

                        doc.Subreports["CrySgst.rpt"].SetDataSource(ds2);
                        doc.SetDataSource(ds);



                        SqlDataAdapter da1 = new SqlDataAdapter("terms", conn);
                        da1.SelectCommand.CommandType = CommandType.StoredProcedure;
                        da1.SelectCommand.Parameters.Add("@uid", SqlDbType.Int).Value = Genclass.Prtid;
                        DataTable ds1 = new DataTable("Terms");
                        da1.Fill(ds1);
                        ds.Tables.Add(ds1);





                        doc.Subreports["Termrpt1.rpt"].SetDataSource(ds1);
                        doc.SetDataSource(ds);
                        Cryview.ReportSource = doc;

                    }

                    else
                    {
                        SqlDataAdapter da = new SqlDataAdapter("sp_PurchaseTrimsBillAccountingotherstate", conn);
                        da.SelectCommand.CommandType = CommandType.StoredProcedure;
                        da.SelectCommand.Parameters.Add("@Muid", SqlDbType.Int).Value = Genclass.Prtid;
                        //da.SelectCommand.Parameters.Add("@type", SqlDbType.Int).Value = 2;
                        da.SelectCommand.Parameters.Add("@slno", SqlDbType.Int).Value = Genclass.slno;
                        da.SelectCommand.Parameters.Add("@companyid", SqlDbType.Int).Value = GeneralParameters.UserdId;
                        da.SelectCommand.Parameters.Add("@doctypeid", SqlDbType.Int).Value = 8;
                        DataSet ds = new DataSet();
                        da.Fill(ds, "salesinvoice");

                        doc.Load(Application.StartupPath + "\\BillAcc.rpt");


                        //Subreports.CrySgst.
                        SqlDataAdapter da2 = new SqlDataAdapter("SP_IGSTTAX", conn);
                        da2.SelectCommand.CommandType = CommandType.StoredProcedure;
                        da2.SelectCommand.Parameters.Add("@uid", SqlDbType.Int).Value = Genclass.Prtid;
                        da2.SelectCommand.Parameters.Add("@doctypeid", SqlDbType.Int).Value = 7;
                        DataTable ds2 = new DataTable("IGSTTAX");
                        da2.Fill(ds2);
                        ds.Tables.Add(ds2);
                        doc.Subreports["Cryigst.rpt"].SetDataSource(ds2);
                        doc.SetDataSource(ds);


                        SqlDataAdapter da1 = new SqlDataAdapter("terms1", conn);
                        da1.SelectCommand.CommandType = CommandType.StoredProcedure;
                        da1.SelectCommand.Parameters.Add("@uid", SqlDbType.Int).Value = Genclass.Prtid;
                        DataTable ds1 = new DataTable("Terms");
                        da1.Fill(ds1);
                        ds.Tables.Add(ds1);




                        doc.Subreports["Termrpt1.rpt"].SetDataSource(ds1);
                        doc.SetDataSource(ds);
                        Cryview.ReportSource = doc;
                    }
                }
            }
            else if (Genclass.Dtype == 150)

            {

                SqlDataAdapter da = new SqlDataAdapter("sp_CMTRpt", conn);
                da.SelectCommand.CommandType = CommandType.StoredProcedure;
                da.SelectCommand.Parameters.Add("@SOCNO", SqlDbType.NVarChar).Value = Genclass.Barcode;

                DataSet ds = new DataSet();
                da.Fill(ds, "CMT");

                doc.Load(Application.StartupPath + "\\CryCMT.rpt");

                doc.SetDataSource(ds);

                Cryview.ReportSource = doc;

            }
            else if (Genclass.Dtype == 540)

            {

                SqlDataAdapter da = new SqlDataAdapter("SP_GETISSURPTre", conn);
                da.SelectCommand.CommandType = CommandType.StoredProcedure;
                da.SelectCommand.Parameters.Add("@uid", SqlDbType.Int).Value = Genclass.Prtid;




                DataSet ds = new DataSet();
                da.Fill(ds, "PRINT");

                doc.Load(Application.StartupPath + "\\JobOrderIssuePrtHalfPg.rpt");


                SqlDataAdapter da1 = new SqlDataAdapter("SP_GETDCITEMSRPT", conn);
                da1.SelectCommand.CommandType = CommandType.StoredProcedure;
                da1.SelectCommand.Parameters.Add("@uid", SqlDbType.Int).Value = Genclass.Prtid;
                DataTable ds1 = new DataTable("Subrpt");
                da1.Fill(ds1);
                ds.Tables.Add(ds1);




                doc.Subreports["NewDC.rpt"].SetDataSource(ds1);


                doc.SetDataSource(ds);

                Cryview.ReportSource = doc;

            }
            else if (Genclass.Dtype == 210 || Genclass.Dtype == 580 || Genclass.Dtype == 600 || Genclass.Dtype == 620)

            {

                SqlDataAdapter da = new SqlDataAdapter("SP_GETISSURPTreIssue", conn);
                da.SelectCommand.CommandType = CommandType.StoredProcedure;
                da.SelectCommand.Parameters.Add("@uid", SqlDbType.Int).Value = Genclass.Prtid;
                da.SelectCommand.Parameters.Add("@DOCTYPEID", SqlDbType.Int).Value = Genclass.Dtype;
                da.SelectCommand.Parameters.Add("@name", SqlDbType.NVarChar).Value = Genclass.name;



                DataSet ds = new DataSet();
                da.Fill(ds, "PRINT");

                doc.Load(Application.StartupPath + "\\JobOrderIssuePrtReIssue.rpt");


                SqlDataAdapter da1 = new SqlDataAdapter("SP_GETDCITEMSRPTIISUE", conn);
                da1.SelectCommand.CommandType = CommandType.StoredProcedure;
                da1.SelectCommand.Parameters.Add("@uid", SqlDbType.Int).Value = Genclass.Prtid;
                da1.SelectCommand.Parameters.Add("@DOCTYPEID", SqlDbType.Int).Value = Genclass.Dtype;
                DataTable ds1 = new DataTable("Subrpt");
                da1.Fill(ds1);
                ds.Tables.Add(ds1);




                doc.Subreports["NewDC.rpt"].SetDataSource(ds1);


                doc.SetDataSource(ds);

                Cryview.ReportSource = doc;

            }
            else if (Genclass.Dtype == 560)

            {



                SqlDataAdapter da = new SqlDataAdapter("SP_GETISSURPTfabIssue", conn);
                da.SelectCommand.CommandType = CommandType.StoredProcedure;
                da.SelectCommand.Parameters.Add("@uid", SqlDbType.Int).Value = Genclass.Prtid;
                da.SelectCommand.Parameters.Add("@DOCTYPEID", SqlDbType.Int).Value = Genclass.Dtype;
                da.SelectCommand.Parameters.Add("@name", SqlDbType.NVarChar).Value = Genclass.name;
                DataSet ds = new DataSet();
                da.Fill(ds, "PRINT");
                doc.Load(Application.StartupPath + "\\JobOrderIssuePrtfabIssue.rpt");

                SqlDataAdapter da1 = new SqlDataAdapter("SP_GETDCITEMSRPTIISUE", conn);
                da1.SelectCommand.CommandType = CommandType.StoredProcedure;
                da1.SelectCommand.Parameters.Add("@uid", SqlDbType.Int).Value = Genclass.Prtid;
                da1.SelectCommand.Parameters.Add("@DOCTYPEID", SqlDbType.Int).Value = Genclass.Dtype;
                DataTable ds1 = new DataTable("Subrpt");
                da1.Fill(ds1);
                ds.Tables.Add(ds1);




                doc.Subreports["NewDC.rpt"].SetDataSource(ds1);

                doc.SetDataSource(ds);

                Cryview.ReportSource = doc;

            }
            else if (Genclass.Dtype == 106 && Genclass.address == "Yarn")

            {

                SqlDataAdapter da = new SqlDataAdapter("SP_GETPODETRPTYARN", conn);
                da.SelectCommand.CommandType = CommandType.StoredProcedure;
                da.SelectCommand.Parameters.Add("@FRMDT", SqlDbType.DateTime).Value = Genclass.rrtt;
                da.SelectCommand.Parameters.Add("@TODT", SqlDbType.DateTime).Value = Genclass.err;
                da.SelectCommand.Parameters.Add("@SUPPLIERUID", SqlDbType.Int).Value = Genclass.itemgrpid;

                DataSet ds = new DataSet();
                da.Fill(ds, "POREG");

                doc.Load(Application.StartupPath + "\\POREGRPT.rpt");




                doc.SetDataSource(ds);

                Cryview.ReportSource = doc;

            }
            else if (Genclass.Dtype == 107 && Genclass.address == "Yarn")

            {

                SqlDataAdapter da = new SqlDataAdapter("SP_GETPODETRPTYARNWITHOUTSUPP", conn);
                da.SelectCommand.CommandType = CommandType.StoredProcedure;
                da.SelectCommand.Parameters.Add("@FRMDT", SqlDbType.DateTime).Value = Convert.ToDateTime(Genclass.rrtt).ToString("yyyy-MM-dd");
                da.SelectCommand.Parameters.Add("@TODT", SqlDbType.DateTime).Value = Convert.ToDateTime(Genclass.err).ToString("yyyy-MM-dd");


                DataSet ds = new DataSet();
                da.Fill(ds, "POREG");

                doc.Load(Application.StartupPath + "\\POREGRPT.rpt");




                doc.SetDataSource(ds);

                Cryview.ReportSource = doc;

            }
            else if (Genclass.Dtype == 106 && Genclass.address == "Trims")

            {

                SqlDataAdapter da = new SqlDataAdapter("SP_GETPODETRPTTRIMS", conn);
                da.SelectCommand.CommandType = CommandType.StoredProcedure;
                da.SelectCommand.Parameters.Add("@FRMDT", SqlDbType.DateTime).Value = Genclass.rrtt;
                da.SelectCommand.Parameters.Add("@TODT", SqlDbType.DateTime).Value = Genclass.err;
                da.SelectCommand.Parameters.Add("@SUPPLIERUID", SqlDbType.Int).Value = Genclass.itemgrpid;

                DataSet ds = new DataSet();
                da.Fill(ds, "POREG");

                doc.Load(Application.StartupPath + "\\POREGRPT.rpt");




                doc.SetDataSource(ds);

                Cryview.ReportSource = doc;

            }
            else if (Genclass.Dtype == 107 && Genclass.address == "Trims")

            {

                SqlDataAdapter da = new SqlDataAdapter("SP_GETPODETRPTTRIMSWITHOUTSUPP", conn);
                da.SelectCommand.CommandType = CommandType.StoredProcedure;
                da.SelectCommand.Parameters.Add("@FRMDT", SqlDbType.DateTime).Value = Genclass.rrtt;
                da.SelectCommand.Parameters.Add("@TODT", SqlDbType.DateTime).Value = Genclass.err;


                DataSet ds = new DataSet();
                da.Fill(ds, "POREG");

                doc.Load(Application.StartupPath + "\\POREGRPT.rpt");




                doc.SetDataSource(ds);

                Cryview.ReportSource = doc;

            }
            else if (Genclass.Dtype == 103)

            {

                SqlDataAdapter da = new SqlDataAdapter("SP_GETITEMWISERPTRECMAINWITHOUTSUPPLIER", conn);
                da.SelectCommand.CommandType = CommandType.StoredProcedure;
                da.SelectCommand.Parameters.Add("@FRMDT", SqlDbType.DateTime).Value = Convert.ToDateTime(Genclass.rrtt).ToString("yyyy-MM-dd");
                da.SelectCommand.Parameters.Add("@TODT", SqlDbType.DateTime).Value = Convert.ToDateTime(Genclass.err).ToString("yyyy-MM-dd");
                da.SelectCommand.Parameters.Add("@yarn", SqlDbType.Int).Value = Genclass.cat;
                da.SelectCommand.Parameters.Add("@fabric", SqlDbType.Int).Value = Genclass.pp;
                da.SelectCommand.Parameters.Add("@trims", SqlDbType.Int).Value = Genclass.ty;
                DataSet ds = new DataSet();
                da.Fill(ds, "ITEMWISE");

                doc.Load(Application.StartupPath + "\\itemwiserpt.rpt");


                SqlDataAdapter da1 = new SqlDataAdapter("SP_GETITEMWISERPTRECWOSUPPLIER", conn);
                da1.SelectCommand.CommandType = CommandType.StoredProcedure;
                da1.SelectCommand.Parameters.Add("@FRMDT", SqlDbType.DateTime).Value = Convert.ToDateTime(Genclass.rrtt).ToString("yyyy-MM-dd");
                da1.SelectCommand.Parameters.Add("@TODT", SqlDbType.DateTime).Value = Convert.ToDateTime(Genclass.err).ToString("yyyy-MM-dd");
                DataTable ds1 = new DataTable("subrptrec");
                da1.Fill(ds1);
                ds.Tables.Add(ds1);




                doc.Subreports["Recitems.rpt"].SetDataSource(ds1);


                doc.SetDataSource(ds);

                Cryview.ReportSource = doc;

            }
            else if (Genclass.Dtype == 104)

            {

                SqlDataAdapter da = new SqlDataAdapter("SP_GETITEMWISERPTRECMAINWITHSUPPLIER", conn);
                da.SelectCommand.CommandType = CommandType.StoredProcedure;
                da.SelectCommand.Parameters.Add("@SUPPLIERUID", SqlDbType.Int).Value = Genclass.Prtid;
                da.SelectCommand.Parameters.Add("@FRMDT", SqlDbType.DateTime).Value = Convert.ToDateTime(Genclass.rrtt).ToString("yyyy-MM-dd");
                da.SelectCommand.Parameters.Add("@TODT", SqlDbType.DateTime).Value = Convert.ToDateTime(Genclass.err).ToString("yyyy-MM-dd");
                da.SelectCommand.Parameters.Add("@yarn", SqlDbType.Int).Value = Genclass.cat;
                da.SelectCommand.Parameters.Add("@fabric", SqlDbType.Int).Value = Genclass.pp;
                da.SelectCommand.Parameters.Add("@trims", SqlDbType.Int).Value = Genclass.ty;

                DataSet ds = new DataSet();
                da.Fill(ds, "ITEMWISE");

                doc.Load(Application.StartupPath + "\\itemwiserpt.rpt");


                SqlDataAdapter da1 = new SqlDataAdapter("SP_GETITEMWISERPTRECWITHSUPPLIER", conn);
                da1.SelectCommand.CommandType = CommandType.StoredProcedure;
                da1.SelectCommand.Parameters.Add("@SUPPLIERUID", SqlDbType.Int).Value = Genclass.Prtid;
                da1.SelectCommand.Parameters.Add("@FRMDT", SqlDbType.DateTime).Value = Convert.ToDateTime(Genclass.rrtt).ToString("yyyy-MM-dd");
                da1.SelectCommand.Parameters.Add("@TODT", SqlDbType.DateTime).Value = Convert.ToDateTime(Genclass.err).ToString("yyyy-MM-dd");
                DataTable ds1 = new DataTable("subrptrec1");
                da1.Fill(ds1);
                ds.Tables.Add(ds1);




                doc.Subreports["Recitems.rpt"].SetDataSource(ds1);


                doc.SetDataSource(ds);

                Cryview.ReportSource = doc;

            }
            else if (Genclass.Dtype == 115)
            {
                SqlDataAdapter da = new SqlDataAdapter("SP_PROCESSMATCHINGREGSINGLESOCNO", conn);
                da.SelectCommand.CommandType = CommandType.StoredProcedure;
                da.SelectCommand.Parameters.Add("@FRMDT", SqlDbType.DateTime).Value = Genclass.rrtt;
                da.SelectCommand.Parameters.Add("@TODT", SqlDbType.DateTime).Value = Genclass.err;
                da.SelectCommand.Parameters.Add("@socno", SqlDbType.NVarChar).Value = Genclass.Barcode;
                da.SelectCommand.Parameters.Add("@yarn", SqlDbType.Int).Value = Genclass.cat;
                da.SelectCommand.Parameters.Add("@fabric", SqlDbType.Int).Value = Genclass.pp;
                da.SelectCommand.Parameters.Add("@trims", SqlDbType.Int).Value = Genclass.ty;
                DataSet ds = new DataSet();
                da.Fill(ds, "ITEMWISE");

                doc.Load(Application.StartupPath + "\\itemwiserptProcess.rpt");


                SqlDataAdapter da1 = new SqlDataAdapter("SP_PROCESSMATCHINGREGRECEIPTSINGLESOCNO", conn);
                da1.SelectCommand.CommandType = CommandType.StoredProcedure;
                da1.SelectCommand.Parameters.Add("@FRMDT", SqlDbType.DateTime).Value = Genclass.rrtt;
                da1.SelectCommand.Parameters.Add("@TODT", SqlDbType.DateTime).Value = Genclass.err;
                da1.SelectCommand.Parameters.Add("@socno", SqlDbType.NVarChar).Value = Genclass.Barcode;
                DataTable ds1 = new DataTable("subrptrec");
                da1.Fill(ds1);
                ds.Tables.Add(ds1);




                doc.Subreports["Recitems.rpt"].SetDataSource(ds1);


                doc.SetDataSource(ds);

                Cryview.ReportSource = doc;

            }
            else if (Genclass.Dtype == 116)

            {

                SqlDataAdapter da = new SqlDataAdapter("SP_PROCESSMATCHINGREGALLSOCNO", conn);
                da.SelectCommand.CommandType = CommandType.StoredProcedure;
                //da.SelectCommand.Parameters.Add("@SUPPLIERUID", SqlDbType.Int).Value = Genclass.Prtid;
                da.SelectCommand.Parameters.Add("@FRMDT", SqlDbType.DateTime).Value = Genclass.rrtt;
                da.SelectCommand.Parameters.Add("@TODT", SqlDbType.DateTime).Value = Genclass.err;
                da.SelectCommand.Parameters.Add("@yarn", SqlDbType.Int).Value = Genclass.cat;
                da.SelectCommand.Parameters.Add("@fabric", SqlDbType.Int).Value = Genclass.pp;
                da.SelectCommand.Parameters.Add("@trims", SqlDbType.Int).Value = Genclass.ty;

                DataSet ds = new DataSet();
                da.Fill(ds, "ITEMWISE");

                doc.Load(Application.StartupPath + "\\itemwiserptProcess.rpt");


                SqlDataAdapter da1 = new SqlDataAdapter("SP_PROCESSMATCHINGREGRECEIPTALLSOCNO", conn);
                da1.SelectCommand.CommandType = CommandType.StoredProcedure;
                //da1.SelectCommand.Parameters.Add("@SUPPLIERUID", SqlDbType.Int).Value = Genclass.Prtid;
                da1.SelectCommand.Parameters.Add("@FRMDT", SqlDbType.DateTime).Value = Genclass.rrtt;
                da1.SelectCommand.Parameters.Add("@TODT", SqlDbType.DateTime).Value = Genclass.err;
                DataTable ds1 = new DataTable("subrptrec1");
                da1.Fill(ds1);
                ds.Tables.Add(ds1);




                doc.Subreports["Recitems.rpt"].SetDataSource(ds1);


                doc.SetDataSource(ds);

                Cryview.ReportSource = doc;

            }
            else if (Genclass.Dtype == 110)

            {

                SqlDataAdapter da = new SqlDataAdapter("SP_GETJOREGRPT", conn);
                da.SelectCommand.CommandType = CommandType.StoredProcedure;
                da.SelectCommand.Parameters.Add("@FRMDT", SqlDbType.DateTime).Value = Genclass.rrtt;
                da.SelectCommand.Parameters.Add("@TODT", SqlDbType.DateTime).Value = Genclass.err;
                da.SelectCommand.Parameters.Add("@SUPPLIERUID", SqlDbType.Int).Value = Genclass.itemgrpid;

                DataSet ds = new DataSet();
                da.Fill(ds, "JOIREG");

                doc.Load(Application.StartupPath + "\\Reports\\JOI.rpt");




                doc.SetDataSource(ds);

                Cryview.ReportSource = doc;

            }
            else if (Genclass.Dtype == 111)

            {

                SqlDataAdapter da = new SqlDataAdapter("SP_GETJOREGRPTWITHOUTSUPP", conn);
                da.SelectCommand.CommandType = CommandType.StoredProcedure;
                da.SelectCommand.Parameters.Add("@FRMDT", SqlDbType.DateTime).Value = Genclass.rrtt;
                da.SelectCommand.Parameters.Add("@TODT", SqlDbType.DateTime).Value = Genclass.err;


                DataSet ds = new DataSet();
                da.Fill(ds, "JOIREG");

                doc.Load(Application.StartupPath + "\\Reports\\JOI.rpt");




                doc.SetDataSource(ds);

                Cryview.ReportSource = doc;

            }
            else if (Genclass.Dtype == 112)

            {

                SqlDataAdapter da = new SqlDataAdapter("SP_GETJORECRPTWITHSUPP", conn);
                da.SelectCommand.CommandType = CommandType.StoredProcedure;
                da.SelectCommand.Parameters.Add("@SUPPLIERUID", SqlDbType.Int).Value = Genclass.itemgrpid;
                da.SelectCommand.Parameters.Add("@FRMDT", SqlDbType.DateTime).Value = Convert.ToDateTime(Genclass.rrtt).ToString("yyyy-MM-dd");
                da.SelectCommand.Parameters.Add("@TODT", SqlDbType.DateTime).Value = Convert.ToDateTime(Genclass.err).ToString("yyyy-MM-dd");


                DataSet ds = new DataSet();
                da.Fill(ds, "JOIREG");

                doc.Load(Application.StartupPath + "\\Reports\\JORECRPT.rpt");




                doc.SetDataSource(ds);

                Cryview.ReportSource = doc;

            }
            else if (Genclass.Dtype == 113)

            {

                SqlDataAdapter da = new SqlDataAdapter("SP_GETJORECRPTWITHOUTSUPP", conn);
                da.SelectCommand.CommandType = CommandType.StoredProcedure;
                da.SelectCommand.Parameters.Add("@FRMDT", SqlDbType.DateTime).Value = Convert.ToDateTime(Genclass.rrtt);
                da.SelectCommand.Parameters.Add("@TODT", SqlDbType.DateTime).Value = Convert.ToDateTime(Genclass.err);

                DataSet ds = new DataSet();
                da.Fill(ds, "JOIREG");

                doc.Load(Application.StartupPath + "\\Reports\\JORECRPT.rpt");




                doc.SetDataSource(ds);

                Cryview.ReportSource = doc;

            }

            else if (Genclass.type == 10)


            {
                if (Genclass.address == "Yarn Purchase")
                {
                    string strsql = "select * from Vw_salprtexlfin   where muid=" + Genclass.Prtid + "  and doctypeid=8";
                    SqlCommand cmd = new SqlCommand(strsql, conn);
                    SqlDataAdapter aptr = new SqlDataAdapter(cmd);
                    DataTable tap = new DataTable();
                    aptr.Fill(tap);
                    if (tap.Rows.Count > 0)
                    {
                        if (tap.Rows[0]["bstate"].ToString() == "Tamilnadu")
                        {


                            SqlDataAdapter da = new SqlDataAdapter("sp_getbillpassstatlocalstate", conn);
                            da.SelectCommand.CommandType = CommandType.StoredProcedure;
                            da.SelectCommand.Parameters.Add("@Muid", SqlDbType.Int).Value = Genclass.Prtid;
                            DataSet ds = new DataSet();
                            da.Fill(ds, "billstatement");
                            doc.Load(Application.StartupPath + "\\BillStatement.rpt");

                            SqlDataAdapter da2 = new SqlDataAdapter("SP_SGSTTAX", conn);
                            da2.SelectCommand.CommandType = CommandType.StoredProcedure;
                            da2.SelectCommand.Parameters.Add("@uid", SqlDbType.Int).Value = Genclass.Prtid;

                            da2.SelectCommand.Parameters.Add("@doctypeid", SqlDbType.Int).Value = 8;
                            DataTable ds2 = new DataTable("SGSTTAX");
                            da2.Fill(ds2);
                            ds.Tables.Add(ds2);

                            doc.Subreports["CrySgst.rpt"].SetDataSource(ds2);
                            doc.SetDataSource(ds);


                            SqlDataAdapter da1 = new SqlDataAdapter("sp_getbillpassstatlocalstate", conn);
                            da1.SelectCommand.CommandType = CommandType.StoredProcedure;
                            da1.SelectCommand.Parameters.Add("@muid", SqlDbType.Int).Value = Genclass.Prtid;
                            DataTable ds1 = new DataTable("billstatement");
                            da1.Fill(ds1);
                            ds.Tables.Add(ds1);


                            doc.Subreports["CryItemAna.rpt"].SetDataSource(ds1);
                            doc.SetDataSource(ds);
                            Cryview.ReportSource = doc;


                        }

                        else
                        {
                            SqlDataAdapter da = new SqlDataAdapter("sp_getbillpassstatotherstate", conn);
                            da.SelectCommand.CommandType = CommandType.StoredProcedure;
                            da.SelectCommand.Parameters.Add("@Muid", SqlDbType.Int).Value = Genclass.Prtid;

                            DataSet ds = new DataSet();
                            da.Fill(ds, "billstatement");

                            doc.Load(Application.StartupPath + "\\BillStatement.rpt");



                            SqlDataAdapter da2 = new SqlDataAdapter("SP_IGSTTAX", conn);
                            da2.SelectCommand.CommandType = CommandType.StoredProcedure;
                            da2.SelectCommand.Parameters.Add("@uid", SqlDbType.Int).Value = Genclass.Prtid;
                            da2.SelectCommand.Parameters.Add("@doctypeid", SqlDbType.Int).Value = 8;
                            DataTable ds2 = new DataTable("IGSTTAX");
                            da2.Fill(ds2);
                            ds.Tables.Add(ds2);

                            doc.Subreports["Cryigst.rpt"].SetDataSource(ds2);
                            doc.SetDataSource(ds);


                            SqlDataAdapter da1 = new SqlDataAdapter("sp_getbillpassstatlocalstate", conn);
                            da1.SelectCommand.CommandType = CommandType.StoredProcedure;
                            da1.SelectCommand.Parameters.Add("@muid", SqlDbType.Int).Value = Genclass.Prtid;
                            DataTable ds1 = new DataTable("billstatement");
                            da1.Fill(ds1);
                            ds.Tables.Add(ds1);


                            doc.Subreports["CryItemAna.rpt"].SetDataSource(ds1);
                            doc.SetDataSource(ds);
                            Cryview.ReportSource = doc;
                        }
                    }
                }
                else if (Genclass.address == "Trims Purchase")
                {

                    string strsql = "select * from Vw_salprtexlfintrims   where muid=" + Genclass.Prtid + "  and doctypeid=8";
                    SqlCommand cmd = new SqlCommand(strsql, conn);
                    SqlDataAdapter aptr = new SqlDataAdapter(cmd);
                    DataTable tap = new DataTable();
                    aptr.Fill(tap);
                    if (tap.Rows.Count > 0)
                    {
                        if (tap.Rows[0]["bstate"].ToString() == "Tamilnadu")
                        {


                            SqlDataAdapter da = new SqlDataAdapter("sp_getbillpassstatrimsstate", conn);
                            da.SelectCommand.CommandType = CommandType.StoredProcedure;
                            da.SelectCommand.Parameters.Add("@Muid", SqlDbType.Int).Value = Genclass.Prtid;
                            DataSet ds = new DataSet();
                            da.Fill(ds, "billstatement");


                            doc.Load(Application.StartupPath + "\\Reports\\BillStatement.rpt");



                            SqlDataAdapter da2 = new SqlDataAdapter("SP_SGSTTAX", conn);
                            da2.SelectCommand.CommandType = CommandType.StoredProcedure;
                            da2.SelectCommand.Parameters.Add("@uid", SqlDbType.Int).Value = Genclass.Prtid;

                            da2.SelectCommand.Parameters.Add("@doctypeid", SqlDbType.Int).Value = 8;
                            DataTable ds2 = new DataTable("SGSTTAX");
                            da2.Fill(ds2);
                            ds.Tables.Add(ds2);

                            doc.Subreports["CrySgst.rpt"].SetDataSource(ds2);
                            doc.SetDataSource(ds);

                            SqlDataAdapter da1 = new SqlDataAdapter("sp_getbillpassstatlocalstate", conn);
                            da1.SelectCommand.CommandType = CommandType.StoredProcedure;
                            da1.SelectCommand.Parameters.Add("@muid", SqlDbType.Int).Value = Genclass.Prtid;
                            DataTable ds1 = new DataTable("billstatement1");
                            da1.Fill(ds1);
                            ds.Tables.Add(ds1);


                            doc.Subreports["CryItemAna.rpt"].SetDataSource(ds1);
                            doc.SetDataSource(ds);
                            Cryview.ReportSource = doc;


                        }

                        else
                        {
                            SqlDataAdapter da = new SqlDataAdapter("sp_getbillpassstatrimsotherstate", conn);
                            da.SelectCommand.Connection = conn;
                            da.SelectCommand.CommandType = CommandType.StoredProcedure;
                            da.SelectCommand.Parameters.Add("@Muid", SqlDbType.Int).Value = Genclass.Prtid;

                            DataSet ds = new DataSet();
                            da.Fill(ds, "billstatement");

                            doc.Load(Application.StartupPath + "\\Reports\\BillStatement.rpt");


                            SqlDataAdapter da2 = new SqlDataAdapter("SP_IGSTTAX", conn);
                            da2.SelectCommand.CommandType = CommandType.StoredProcedure;
                            da2.SelectCommand.Parameters.Add("@uid", SqlDbType.Int).Value = Genclass.Prtid;
                            da2.SelectCommand.Parameters.Add("@doctypeid", SqlDbType.Int).Value = 8;
                            DataTable ds2 = new DataTable("IGSTTAX");
                            da2.Fill(ds2);
                            ds.Tables.Add(ds2);
                            doc.Subreports["Cryigst.rpt"].SetDataSource(ds2);
                            doc.SetDataSource(ds);

                            SqlDataAdapter da1 = new SqlDataAdapter("sp_getbillpassstatlocalstate", conn);
                            da1.SelectCommand.CommandType = CommandType.StoredProcedure;
                            da1.SelectCommand.Parameters.Add("@muid", SqlDbType.Int).Value = Genclass.Prtid;
                            DataTable ds1 = new DataTable("billstatement1");
                            da1.Fill(ds1);
                            ds.Tables.Add(ds1);


                            doc.Subreports["CryItemAna.rpt"].SetDataSource(ds1);
                            doc.SetDataSource(ds);
                            Cryview.ReportSource = doc;

                        }
                    }


                }


            }
           


            conn.Close();
        }


        public string Rupees(Int64 rup)
        {
            string result = "";
            Int64 res;
            if ((rup / 10000000) > 0)
            {
                res = rup / 10000000;
                rup = rup % 10000000;
                result = result + ' ' + RupeesToWords(res) + " Crore";
            }
            if ((rup / 100000) > 0)
            {
                res = rup / 100000;
                rup = rup % 100000;
                result = result + ' ' + RupeesToWords(res) + " Lakh";
            }
            if ((rup / 1000) > 0)
            {
                res = rup / 1000;
                rup = rup % 1000;
                result = result + ' ' + RupeesToWords(res) + " Thousand";
            }


            if ((rup / 100) > 0)
            {
                res = rup / 100;
                rup = rup % 100;
                if (rup > 0)
                {
                    result = result + ' ' + RupeesToWords(res) + " Hundred and";
                }
                else
                {
                    result = result + ' ' + RupeesToWords(res) + " Hundred";
                }
            }
            if ((rup % 10) >= 0)
            {
                res = rup % 100;
                result = result + " " + RupeesToWords(res);
            }
            result = "Rupees " + result + ' ' + "only";
            return result;
        }

        public string RupeesToWords(Int64 rup)
        {
            string result = "";
            if ((rup >= 1) && (rup <= 10))
            {
                if ((rup % 10) == 1) result = "One";
                if ((rup % 10) == 2) result = "Two";
                if ((rup % 10) == 3) result = "Three";
                if ((rup % 10) == 4) result = "Four";
                if ((rup % 10) == 5) result = "Five";
                if ((rup % 10) == 6) result = "Six";
                if ((rup % 10) == 7) result = "Seven";
                if ((rup % 10) == 8) result = "Eight";
                if ((rup % 10) == 9) result = "Nine";
                if ((rup % 10) == 0) result = "Ten";
            }
            if (rup > 9 && rup < 20)
            {
                if (rup == 11) result = "Eleven";
                if (rup == 12) result = "Twelve";
                if (rup == 13) result = "Thirteen";
                if (rup == 14) result = "Forteen";
                if (rup == 15) result = "Fifteen";
                if (rup == 16) result = "Sixteen";
                if (rup == 17) result = "Seventeen";
                if (rup == 18) result = "Eighteen";
                if (rup == 19) result = "Nineteen";
            }

            if (rup >= 20 && (rup / 10) == 2 && (rup % 10) == 0) result = "Twenty";
            if (rup > 20 && (rup / 10) == 3 && (rup % 10) == 0) result = "Thirty";
            if (rup > 20 && (rup / 10) == 4 && (rup % 10) == 0) result = "Forty";
            if (rup > 20 && (rup / 10) == 5 && (rup % 10) == 0) result = "Fifty";
            if (rup > 20 && (rup / 10) == 6 && (rup % 10) == 0) result = "Sixty";
            if (rup > 20 && (rup / 10) == 7 && (rup % 10) == 0) result = "Seventy";
            if (rup > 20 && (rup / 10) == 8 && (rup % 10) == 0) result = "Eighty";
            if (rup > 20 && (rup / 10) == 9 && (rup % 10) == 0) result = "Ninty";

            if (rup > 20 && (rup / 10) == 2 && (rup % 10) != 0)
            {
                if ((rup % 10) == 1) result = "Twenty One";
                if ((rup % 10) == 2) result = "Twenty Two";
                if ((rup % 10) == 3) result = "Twenty Three";
                if ((rup % 10) == 4) result = "Twenty Four";
                if ((rup % 10) == 5) result = "Twenty Five";
                if ((rup % 10) == 6) result = "Twenty Six";
                if ((rup % 10) == 7) result = "Twenty Seven";
                if ((rup % 10) == 8) result = "Twenty Eight";
                if ((rup % 10) == 9) result = "Twenty Nine";
            }
            if (rup > 20 && (rup / 10) == 3 && (rup % 10) != 0)
            {
                if ((rup % 10) == 1) result = "Thirty One";
                if ((rup % 10) == 2) result = "Thirty Two";
                if ((rup % 10) == 3) result = "Thirty Three";
                if ((rup % 10) == 4) result = "Thirty Four";
                if ((rup % 10) == 5) result = "Thirty Five";
                if ((rup % 10) == 6) result = "Thirty Six";
                if ((rup % 10) == 7) result = "Thirty Seven";
                if ((rup % 10) == 8) result = "Thirty Eight";
                if ((rup % 10) == 9) result = "Thirty Nine";
            }
            if (rup > 20 && (rup / 10) == 4 && (rup % 10) != 0)
            {
                if ((rup % 10) == 1) result = "Forty One";
                if ((rup % 10) == 2) result = "Forty Two";
                if ((rup % 10) == 3) result = "Forty Three";
                if ((rup % 10) == 4) result = "Forty Four";
                if ((rup % 10) == 5) result = "Forty Five";
                if ((rup % 10) == 6) result = "Forty Six";
                if ((rup % 10) == 7) result = "Forty Seven";
                if ((rup % 10) == 8) result = "Forty Eight";
                if ((rup % 10) == 9) result = "Forty Nine";
            }
            if (rup > 20 && (rup / 10) == 5 && (rup % 10) != 0)
            {
                if ((rup % 10) == 1) result = "Fifty One";
                if ((rup % 10) == 2) result = "Fifty Two";
                if ((rup % 10) == 3) result = "Fifty Three";
                if ((rup % 10) == 4) result = "Fifty Four";
                if ((rup % 10) == 5) result = "Fifty Five";
                if ((rup % 10) == 6) result = "Fifty Six";
                if ((rup % 10) == 7) result = "Fifty Seven";
                if ((rup % 10) == 8) result = "Fifty Eight";
                if ((rup % 10) == 9) result = "Fifty Nine";
            }
            if (rup > 20 && (rup / 10) == 6 && (rup % 10) != 0)
            {
                if ((rup % 10) == 1) result = "Sixty One";
                if ((rup % 10) == 2) result = "Sixty Two";
                if ((rup % 10) == 3) result = "Sixty Three";
                if ((rup % 10) == 4) result = "Sixty Four";
                if ((rup % 10) == 5) result = "Sixty Five";
                if ((rup % 10) == 6) result = "Sixty Six";
                if ((rup % 10) == 7) result = "Sixty Seven";
                if ((rup % 10) == 8) result = "Sixty Eight";
                if ((rup % 10) == 9) result = "Sixty Nine";
            }
            if (rup > 20 && (rup / 10) == 7 && (rup % 10) != 0)
            {
                if ((rup % 10) == 1) result = "Seventy One";
                if ((rup % 10) == 2) result = "Seventy Two";
                if ((rup % 10) == 3) result = "Seventy Three";
                if ((rup % 10) == 4) result = "Seventy Four";
                if ((rup % 10) == 5) result = "Seventy Five";
                if ((rup % 10) == 6) result = "Seventy Six";
                if ((rup % 10) == 7) result = "Seventy Seven";
                if ((rup % 10) == 8) result = "Seventy Eight";
                if ((rup % 10) == 9) result = "Seventy Nine";
            }
            if (rup > 20 && (rup / 10) == 8 && (rup % 10) != 0)
            {
                if ((rup % 10) == 1) result = "Eighty One";
                if ((rup % 10) == 2) result = "Eighty Two";
                if ((rup % 10) == 3) result = "Eighty Three";
                if ((rup % 10) == 4) result = "Eighty Four";
                if ((rup % 10) == 5) result = "Eighty Five";
                if ((rup % 10) == 6) result = "Eighty Six";
                if ((rup % 10) == 7) result = "Eighty Seven";
                if ((rup % 10) == 8) result = "Eighty Eight";
                if ((rup % 10) == 9) result = "Eighty Nine";
            }
            if (rup > 20 && (rup / 10) == 9 && (rup % 10) != 0)
            {
                if ((rup % 10) == 1) result = "Ninty One";
                if ((rup % 10) == 2) result = "Ninty Two";
                if ((rup % 10) == 3) result = "Ninty Three";
                if ((rup % 10) == 4) result = "Ninty Four";
                if ((rup % 10) == 5) result = "Ninty Five";
                if ((rup % 10) == 6) result = "Ninty Six";
                if ((rup % 10) == 7) result = "Ninty Seven";
                if ((rup % 10) == 8) result = "Ninty Eight";
                if ((rup % 10) == 9) result = "Ninty Nine";
            }
            return result;
        }
        private void btnexit_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void Crviewer_Load(object sender, EventArgs e)
        {
            Cryview_Load(sender, e);
        }
    }
}
