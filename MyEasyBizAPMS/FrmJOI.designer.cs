﻿namespace MyEasyBizAPMS
{
    partial class FrmJOI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmJOI));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.Editpnl = new System.Windows.Forms.Panel();
            this.grSearch = new System.Windows.Forms.Panel();
            this.button18 = new System.Windows.Forms.Button();
            this.btnHide = new System.Windows.Forms.Button();
            this.DataGridCommon = new System.Windows.Forms.DataGridView();
            this.button22 = new System.Windows.Forms.Button();
            this.tabC = new System.Windows.Forms.TabControl();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.panel3 = new System.Windows.Forms.Panel();
            this.txtsocno = new System.Windows.Forms.TextBox();
            this.txtwor = new System.Windows.Forms.TextBox();
            this.label36 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.cbopro1 = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.button10 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.txtscr11 = new System.Windows.Forms.TextBox();
            this.HFGP1 = new System.Windows.Forms.DataGridView();
            this.label69 = new System.Windows.Forms.Label();
            this.label67 = new System.Windows.Forms.Label();
            this.cbowo = new System.Windows.Forms.ComboBox();
            this.txtstyle = new System.Windows.Forms.TextBox();
            this.label37 = new System.Windows.Forms.Label();
            this.txtoutuom = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.lkppnl = new System.Windows.Forms.Panel();
            this.button15 = new System.Windows.Forms.Button();
            this.button16 = new System.Windows.Forms.Button();
            this.HFGP2 = new System.Windows.Forms.DataGridView();
            this.RQGR = new System.Windows.Forms.DataGridView();
            this.txtoutqty = new System.Windows.Forms.TextBox();
            this.label33 = new System.Windows.Forms.Label();
            this.txtoutrolls = new System.Windows.Forms.TextBox();
            this.label34 = new System.Windows.Forms.Label();
            this.txtoutitem = new System.Windows.Forms.TextBox();
            this.label35 = new System.Windows.Forms.Label();
            this.button17 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.txtitem = new System.Windows.Forms.TextBox();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.panel4 = new System.Windows.Forms.Panel();
            this.cboprocess = new System.Windows.Forms.ComboBox();
            this.button19 = new System.Windows.Forms.Button();
            this.button21 = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.HFG9 = new System.Windows.Forms.DataGridView();
            this.label8 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.cbotype = new System.Windows.Forms.ComboBox();
            this.cbouom = new System.Windows.Forms.ComboBox();
            this.txtrate = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.txtoutoutqty = new System.Windows.Forms.TextBox();
            this.HFIT = new System.Windows.Forms.DataGridView();
            this.button11 = new System.Windows.Forms.Button();
            this.txtdcqty = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.textBox9 = new System.Windows.Forms.TextBox();
            this.textBox10 = new System.Windows.Forms.TextBox();
            this.textBox11 = new System.Windows.Forms.TextBox();
            this.textBox12 = new System.Windows.Forms.TextBox();
            this.textBox13 = new System.Windows.Forms.TextBox();
            this.txtuom = new System.Windows.Forms.TextBox();
            this.button12 = new System.Windows.Forms.Button();
            this.dataGridView3 = new System.Windows.Forms.DataGridView();
            this.label22 = new System.Windows.Forms.Label();
            this.textBox15 = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.label23 = new System.Windows.Forms.Label();
            this.txtisstot = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.txtmode = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.textBox16 = new System.Windows.Forms.TextBox();
            this.button14 = new System.Windows.Forms.Button();
            this.label29 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.txtaddnotes = new System.Windows.Forms.TextBox();
            this.txtbeam = new System.Windows.Forms.TextBox();
            this.serialno = new System.Windows.Forms.Panel();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.label80 = new System.Windows.Forms.Label();
            this.Dataserial = new System.Windows.Forms.DataGridView();
            this.label78 = new System.Windows.Forms.Label();
            this.button20 = new System.Windows.Forms.Button();
            this.label76 = new System.Windows.Forms.Label();
            this.textBox17 = new System.Windows.Forms.TextBox();
            this.label79 = new System.Windows.Forms.Label();
            this.txtsrail = new System.Windows.Forms.TextBox();
            this.cboserial = new System.Windows.Forms.ComboBox();
            this.label84 = new System.Windows.Forms.Label();
            this.txtserialqty = new System.Windows.Forms.TextBox();
            this.label85 = new System.Windows.Forms.Label();
            this.button23 = new System.Windows.Forms.Button();
            this.label88 = new System.Windows.Forms.Label();
            this.textBox18 = new System.Windows.Forms.TextBox();
            this.textBox19 = new System.Windows.Forms.TextBox();
            this.txtsqty = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtbillqty = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtcolor = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.txtorderdes = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.reqdt = new System.Windows.Forms.DateTimePicker();
            this.label9 = new System.Windows.Forms.Label();
            this.txtqty = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.Phone = new System.Windows.Forms.Label();
            this.txtgrn = new System.Windows.Forms.TextBox();
            this.dtpgrndt = new System.Windows.Forms.DateTimePicker();
            this.txtterms = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.txtname = new System.Windows.Forms.TextBox();
            this.txtbeamid = new System.Windows.Forms.TextBox();
            this.txtoutputid = new System.Windows.Forms.TextBox();
            this.txtitemid = new System.Windows.Forms.TextBox();
            this.txtmillid = new System.Windows.Forms.TextBox();
            this.txtpuid = new System.Windows.Forms.TextBox();
            this.txtgrnid = new System.Windows.Forms.TextBox();
            this.txtnobeams = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.txtrectot = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.txtmodem = new System.Windows.Forms.TextBox();
            this.txtvehicle = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.Genpan = new System.Windows.Forms.Panel();
            this.txtscr1 = new System.Windows.Forms.TextBox();
            this.HFGP = new System.Windows.Forms.DataGridView();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.flowLayoutPanel4 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel5 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel6 = new System.Windows.Forms.FlowLayoutPanel();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.label57 = new System.Windows.Forms.Label();
            this.dtpfnt = new System.Windows.Forms.DateTimePicker();
            this.txtscr8 = new System.Windows.Forms.TextBox();
            this.txtscr7 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.txtscr6 = new System.Windows.Forms.TextBox();
            this.txtscr5 = new System.Windows.Forms.TextBox();
            this.Txtscr2 = new System.Windows.Forms.TextBox();
            this.txtscr4 = new System.Windows.Forms.TextBox();
            this.Txtscr3 = new System.Windows.Forms.TextBox();
            this.panadd = new System.Windows.Forms.Panel();
            this.button13 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.buttnext1 = new System.Windows.Forms.Button();
            this.chkact = new System.Windows.Forms.CheckBox();
            this.butcan = new System.Windows.Forms.Button();
            this.butedit = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.Chkedtact = new System.Windows.Forms.CheckBox();
            this.buttnfinbk = new System.Windows.Forms.Button();
            this.btnsave = new System.Windows.Forms.Button();
            this.Editpnl.SuspendLayout();
            this.grSearch.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridCommon)).BeginInit();
            this.tabC.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HFGP1)).BeginInit();
            this.lkppnl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HFGP2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RQGR)).BeginInit();
            this.tabPage3.SuspendLayout();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HFG9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HFIT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).BeginInit();
            this.serialno.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Dataserial)).BeginInit();
            this.Genpan.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HFGP)).BeginInit();
            this.panel2.SuspendLayout();
            this.panadd.SuspendLayout();
            this.SuspendLayout();
            // 
            // Editpnl
            // 
            this.Editpnl.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.Editpnl.Controls.Add(this.grSearch);
            this.Editpnl.Controls.Add(this.tabC);
            this.Editpnl.Controls.Add(this.label18);
            this.Editpnl.Controls.Add(this.txtorderdes);
            this.Editpnl.Controls.Add(this.label28);
            this.Editpnl.Controls.Add(this.reqdt);
            this.Editpnl.Controls.Add(this.label9);
            this.Editpnl.Controls.Add(this.txtqty);
            this.Editpnl.Controls.Add(this.label2);
            this.Editpnl.Controls.Add(this.label3);
            this.Editpnl.Controls.Add(this.Phone);
            this.Editpnl.Controls.Add(this.txtgrn);
            this.Editpnl.Controls.Add(this.dtpgrndt);
            this.Editpnl.Controls.Add(this.txtterms);
            this.Editpnl.Controls.Add(this.label20);
            this.Editpnl.Controls.Add(this.txtname);
            this.Editpnl.Controls.Add(this.txtbeamid);
            this.Editpnl.Controls.Add(this.txtoutputid);
            this.Editpnl.Controls.Add(this.txtitemid);
            this.Editpnl.Controls.Add(this.txtmillid);
            this.Editpnl.Controls.Add(this.txtpuid);
            this.Editpnl.Controls.Add(this.txtgrnid);
            this.Editpnl.Controls.Add(this.txtnobeams);
            this.Editpnl.Controls.Add(this.label17);
            this.Editpnl.Controls.Add(this.txtrectot);
            this.Editpnl.Controls.Add(this.label30);
            this.Editpnl.Controls.Add(this.txtmodem);
            this.Editpnl.Controls.Add(this.txtvehicle);
            this.Editpnl.Controls.Add(this.label1);
            this.Editpnl.Location = new System.Drawing.Point(1, 4);
            this.Editpnl.Name = "Editpnl";
            this.Editpnl.Size = new System.Drawing.Size(1145, 583);
            this.Editpnl.TabIndex = 1;
            // 
            // grSearch
            // 
            this.grSearch.BackColor = System.Drawing.Color.White;
            this.grSearch.Controls.Add(this.button18);
            this.grSearch.Controls.Add(this.btnHide);
            this.grSearch.Controls.Add(this.DataGridCommon);
            this.grSearch.Controls.Add(this.button22);
            this.grSearch.Location = new System.Drawing.Point(430, 99);
            this.grSearch.Name = "grSearch";
            this.grSearch.Size = new System.Drawing.Size(454, 339);
            this.grSearch.TabIndex = 443;
            this.grSearch.Visible = false;
            this.grSearch.Paint += new System.Windows.Forms.PaintEventHandler(this.grSearch_Paint);
            // 
            // button18
            // 
            this.button18.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button18.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button18.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button18.Location = new System.Drawing.Point(378, 309);
            this.button18.Name = "button18";
            this.button18.Size = new System.Drawing.Size(70, 28);
            this.button18.TabIndex = 394;
            this.button18.Text = "Select";
            this.button18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button18.UseVisualStyleBackColor = false;
            this.button18.Click += new System.EventHandler(this.button18_Click);
            // 
            // btnHide
            // 
            this.btnHide.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnHide.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHide.Image = ((System.Drawing.Image)(resources.GetObject("btnHide.Image")));
            this.btnHide.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnHide.Location = new System.Drawing.Point(7, 310);
            this.btnHide.Name = "btnHide";
            this.btnHide.Size = new System.Drawing.Size(70, 27);
            this.btnHide.TabIndex = 393;
            this.btnHide.Text = "Close";
            this.btnHide.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnHide.UseVisualStyleBackColor = false;
            this.btnHide.Click += new System.EventHandler(this.btnHide_Click);
            // 
            // DataGridCommon
            // 
            this.DataGridCommon.AllowUserToAddRows = false;
            this.DataGridCommon.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.DataGridCommon.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridCommon.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.DataGridCommon.Location = new System.Drawing.Point(10, 3);
            this.DataGridCommon.Name = "DataGridCommon";
            this.DataGridCommon.ReadOnly = true;
            this.DataGridCommon.RowHeadersVisible = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.DataGridCommon.RowsDefaultCellStyle = dataGridViewCellStyle1;
            this.DataGridCommon.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridCommon.Size = new System.Drawing.Size(441, 299);
            this.DataGridCommon.TabIndex = 0;
            this.DataGridCommon.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridCommon_CellContentClick);
            this.DataGridCommon.DoubleClick += new System.EventHandler(this.DataGridCommon_DoubleClick);
            this.DataGridCommon.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DataGridCommon_KeyDown);
            this.DataGridCommon.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.DataGridCommon_KeyPress);
            // 
            // button22
            // 
            this.button22.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button22.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button22.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button22.Location = new System.Drawing.Point(173, 201);
            this.button22.Name = "button22";
            this.button22.Size = new System.Drawing.Size(105, 28);
            this.button22.TabIndex = 395;
            this.button22.Text = "Add Supplier";
            this.button22.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button22.UseVisualStyleBackColor = false;
            this.button22.Click += new System.EventHandler(this.button22_Click);
            // 
            // tabC
            // 
            this.tabC.Controls.Add(this.tabPage2);
            this.tabC.Controls.Add(this.tabPage3);
            this.tabC.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabC.Location = new System.Drawing.Point(14, 117);
            this.tabC.Name = "tabC";
            this.tabC.SelectedIndex = 0;
            this.tabC.Size = new System.Drawing.Size(1134, 474);
            this.tabC.TabIndex = 443;
            this.tabC.SelectedIndexChanged += new System.EventHandler(this.tabC_SelectedIndexChanged);
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.tabPage2.Controls.Add(this.panel3);
            this.tabPage2.Controls.Add(this.txtoutuom);
            this.tabPage2.Controls.Add(this.label32);
            this.tabPage2.Controls.Add(this.lkppnl);
            this.tabPage2.Controls.Add(this.RQGR);
            this.tabPage2.Controls.Add(this.txtoutqty);
            this.tabPage2.Controls.Add(this.label33);
            this.tabPage2.Controls.Add(this.txtoutrolls);
            this.tabPage2.Controls.Add(this.label34);
            this.tabPage2.Controls.Add(this.txtoutitem);
            this.tabPage2.Controls.Add(this.label35);
            this.tabPage2.Controls.Add(this.button17);
            this.tabPage2.Controls.Add(this.button4);
            this.tabPage2.Controls.Add(this.label7);
            this.tabPage2.Controls.Add(this.txtitem);
            this.tabPage2.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabPage2.Location = new System.Drawing.Point(4, 28);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1126, 442);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "DC Items";
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.SystemColors.HighlightText;
            this.panel3.Controls.Add(this.txtsocno);
            this.panel3.Controls.Add(this.txtwor);
            this.panel3.Controls.Add(this.label36);
            this.panel3.Controls.Add(this.label24);
            this.panel3.Controls.Add(this.cbopro1);
            this.panel3.Controls.Add(this.label10);
            this.panel3.Controls.Add(this.button10);
            this.panel3.Controls.Add(this.button9);
            this.panel3.Controls.Add(this.txtscr11);
            this.panel3.Controls.Add(this.HFGP1);
            this.panel3.Controls.Add(this.label69);
            this.panel3.Controls.Add(this.label67);
            this.panel3.Controls.Add(this.cbowo);
            this.panel3.Controls.Add(this.txtstyle);
            this.panel3.Controls.Add(this.label37);
            this.panel3.Location = new System.Drawing.Point(96, 67);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(836, 364);
            this.panel3.TabIndex = 991;
            // 
            // txtsocno
            // 
            this.txtsocno.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtsocno.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtsocno.Location = new System.Drawing.Point(61, 7);
            this.txtsocno.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtsocno.Name = "txtsocno";
            this.txtsocno.Size = new System.Drawing.Size(134, 27);
            this.txtsocno.TabIndex = 5606;
            this.txtsocno.Click += new System.EventHandler(this.txtsocno_Click);
            this.txtsocno.TextChanged += new System.EventHandler(this.txtsocno_TextChanged);
            // 
            // txtwor
            // 
            this.txtwor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtwor.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtwor.Location = new System.Drawing.Point(292, 10);
            this.txtwor.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtwor.Name = "txtwor";
            this.txtwor.Size = new System.Drawing.Size(138, 27);
            this.txtwor.TabIndex = 5605;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.Location = new System.Drawing.Point(4, 10);
            this.label36.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(54, 21);
            this.label36.TabIndex = 5604;
            this.label36.Text = "SocNo";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(198, 14);
            this.label24.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(91, 21);
            this.label24.TabIndex = 5602;
            this.label24.Text = "Work Order";
            // 
            // cbopro1
            // 
            this.cbopro1.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbopro1.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbopro1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbopro1.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbopro1.FormattingEnabled = true;
            this.cbopro1.Location = new System.Drawing.Point(510, 9);
            this.cbopro1.Name = "cbopro1";
            this.cbopro1.Size = new System.Drawing.Size(187, 27);
            this.cbopro1.TabIndex = 5601;
            this.cbopro1.SelectedIndexChanged += new System.EventHandler(this.cbopro1_SelectedIndexChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(438, 15);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(63, 21);
            this.label10.TabIndex = 5600;
            this.label10.Text = "Process";
            // 
            // button10
            // 
            this.button10.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button10.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button10.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button10.Location = new System.Drawing.Point(755, 334);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(71, 27);
            this.button10.TabIndex = 396;
            this.button10.Text = "Select";
            this.button10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button10.UseVisualStyleBackColor = false;
            this.button10.Click += new System.EventHandler(this.button10_Click_1);
            // 
            // button9
            // 
            this.button9.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button9.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button9.Image = ((System.Drawing.Image)(resources.GetObject("button9.Image")));
            this.button9.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button9.Location = new System.Drawing.Point(0, 336);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(72, 27);
            this.button9.TabIndex = 395;
            this.button9.Text = "Close";
            this.button9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button9.UseVisualStyleBackColor = false;
            this.button9.Click += new System.EventHandler(this.button9_Click_1);
            // 
            // txtscr11
            // 
            this.txtscr11.AcceptsReturn = true;
            this.txtscr11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtscr11.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtscr11.Location = new System.Drawing.Point(4, 40);
            this.txtscr11.Margin = new System.Windows.Forms.Padding(4);
            this.txtscr11.Name = "txtscr11";
            this.txtscr11.Size = new System.Drawing.Size(773, 26);
            this.txtscr11.TabIndex = 98;
            this.txtscr11.TextChanged += new System.EventHandler(this.txtscr11_TextChanged_1);
            this.txtscr11.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtscr11_KeyDown_1);
            // 
            // HFGP1
            // 
            this.HFGP1.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.HFGP1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.HFGP1.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.HFGP1.Location = new System.Drawing.Point(8, 71);
            this.HFGP1.Name = "HFGP1";
            this.HFGP1.Size = new System.Drawing.Size(818, 259);
            this.HFGP1.TabIndex = 97;
            this.HFGP1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.HFGP1_CellContentClick);
            this.HFGP1.Click += new System.EventHandler(this.HFGP1_Click);
            this.HFGP1.DoubleClick += new System.EventHandler(this.HFGP1_DoubleClick_1);
            this.HFGP1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.HFGP1_KeyDown_1);
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label69.ForeColor = System.Drawing.Color.White;
            this.label69.Location = new System.Drawing.Point(64, 85);
            this.label69.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(81, 21);
            this.label69.TabIndex = 196;
            this.label69.Text = "Itemname";
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label67.ForeColor = System.Drawing.Color.White;
            this.label67.Location = new System.Drawing.Point(8, 184);
            this.label67.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(124, 21);
            this.label67.TabIndex = 198;
            this.label67.Text = "Press ESC to Exit";
            // 
            // cbowo
            // 
            this.cbowo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbowo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbowo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbowo.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbowo.FormattingEnabled = true;
            this.cbowo.Location = new System.Drawing.Point(510, 278);
            this.cbowo.Name = "cbowo";
            this.cbowo.Size = new System.Drawing.Size(128, 27);
            this.cbowo.TabIndex = 5603;
            this.cbowo.SelectedIndexChanged += new System.EventHandler(this.cbowo_SelectedIndexChanged);
            // 
            // txtstyle
            // 
            this.txtstyle.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtstyle.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtstyle.Location = new System.Drawing.Point(417, 199);
            this.txtstyle.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtstyle.Name = "txtstyle";
            this.txtstyle.Size = new System.Drawing.Size(303, 27);
            this.txtstyle.TabIndex = 5608;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.Location = new System.Drawing.Point(367, 203);
            this.label37.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(44, 21);
            this.label37.TabIndex = 5607;
            this.label37.Text = "Style";
            // 
            // txtoutuom
            // 
            this.txtoutuom.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtoutuom.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtoutuom.Location = new System.Drawing.Point(638, 34);
            this.txtoutuom.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtoutuom.Name = "txtoutuom";
            this.txtoutuom.Size = new System.Drawing.Size(84, 27);
            this.txtoutuom.TabIndex = 990;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.Location = new System.Drawing.Point(641, 10);
            this.label32.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(44, 21);
            this.label32.TabIndex = 426;
            this.label32.Text = "Uom";
            // 
            // lkppnl
            // 
            this.lkppnl.BackColor = System.Drawing.Color.White;
            this.lkppnl.Controls.Add(this.button15);
            this.lkppnl.Controls.Add(this.button16);
            this.lkppnl.Controls.Add(this.HFGP2);
            this.lkppnl.Location = new System.Drawing.Point(165, 86);
            this.lkppnl.Name = "lkppnl";
            this.lkppnl.Size = new System.Drawing.Size(442, 210);
            this.lkppnl.TabIndex = 424;
            this.lkppnl.Visible = false;
            // 
            // button15
            // 
            this.button15.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button15.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button15.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button15.Location = new System.Drawing.Point(366, 180);
            this.button15.Name = "button15";
            this.button15.Size = new System.Drawing.Size(71, 28);
            this.button15.TabIndex = 394;
            this.button15.Text = "Select";
            this.button15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button15.UseVisualStyleBackColor = false;
            this.button15.Click += new System.EventHandler(this.button15_Click);
            // 
            // button16
            // 
            this.button16.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button16.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button16.Image = ((System.Drawing.Image)(resources.GetObject("button16.Image")));
            this.button16.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button16.Location = new System.Drawing.Point(6, 181);
            this.button16.Name = "button16";
            this.button16.Size = new System.Drawing.Size(67, 27);
            this.button16.TabIndex = 393;
            this.button16.Text = "Close";
            this.button16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button16.UseVisualStyleBackColor = false;
            // 
            // HFGP2
            // 
            this.HFGP2.AllowUserToAddRows = false;
            this.HFGP2.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.HFGP2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.HFGP2.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.HFGP2.Location = new System.Drawing.Point(7, 10);
            this.HFGP2.Name = "HFGP2";
            this.HFGP2.ReadOnly = true;
            this.HFGP2.RowHeadersVisible = false;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.HFGP2.RowsDefaultCellStyle = dataGridViewCellStyle2;
            this.HFGP2.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.HFGP2.Size = new System.Drawing.Size(430, 168);
            this.HFGP2.TabIndex = 0;
            this.HFGP2.DoubleClick += new System.EventHandler(this.HFGP2_DoubleClick);
            this.HFGP2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.HFGP2_KeyDown);
            // 
            // RQGR
            // 
            this.RQGR.BackgroundColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.RQGR.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.RQGR.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.RQGR.Location = new System.Drawing.Point(97, 68);
            this.RQGR.Name = "RQGR";
            this.RQGR.Size = new System.Drawing.Size(966, 363);
            this.RQGR.TabIndex = 416;
            this.RQGR.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.RQGR_CellClick);
            this.RQGR.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.RQGR_CellContentClick);
            // 
            // txtoutqty
            // 
            this.txtoutqty.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtoutqty.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtoutqty.Location = new System.Drawing.Point(723, 34);
            this.txtoutqty.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtoutqty.Name = "txtoutqty";
            this.txtoutqty.Size = new System.Drawing.Size(71, 27);
            this.txtoutqty.TabIndex = 418;
            this.txtoutqty.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtoutqty_KeyPress);
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(809, 10);
            this.label33.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(76, 21);
            this.label33.TabIndex = 422;
            this.label33.Text = "NoofBags";
            // 
            // txtoutrolls
            // 
            this.txtoutrolls.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtoutrolls.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtoutrolls.Location = new System.Drawing.Point(795, 34);
            this.txtoutrolls.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtoutrolls.Name = "txtoutrolls";
            this.txtoutrolls.Size = new System.Drawing.Size(74, 27);
            this.txtoutrolls.TabIndex = 419;
            this.txtoutrolls.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtoutrolls_KeyDown);
            this.txtoutrolls.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtoutrolls_KeyPress);
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.Location = new System.Drawing.Point(733, 10);
            this.label34.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(35, 21);
            this.label34.TabIndex = 421;
            this.label34.Text = "Qty";
            // 
            // txtoutitem
            // 
            this.txtoutitem.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtoutitem.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtoutitem.Location = new System.Drawing.Point(95, 34);
            this.txtoutitem.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtoutitem.Name = "txtoutitem";
            this.txtoutitem.Size = new System.Drawing.Size(542, 27);
            this.txtoutitem.TabIndex = 417;
            this.txtoutitem.Click += new System.EventHandler(this.txtoutitem_Click);
            this.txtoutitem.TextChanged += new System.EventHandler(this.txtoutitem_TextChanged);
            this.txtoutitem.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtoutitem_KeyDown);
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.Location = new System.Drawing.Point(98, 10);
            this.label35.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(83, 21);
            this.label35.TabIndex = 418;
            this.label35.Text = "ItemName";
            // 
            // button17
            // 
            this.button17.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button17.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button17.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button17.Location = new System.Drawing.Point(869, 33);
            this.button17.Name = "button17";
            this.button17.Size = new System.Drawing.Size(45, 29);
            this.button17.TabIndex = 420;
            this.button17.Text = "OK";
            this.button17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button17.UseVisualStyleBackColor = false;
            this.button17.Click += new System.EventHandler(this.button17_Click);
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button4.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button4.Location = new System.Drawing.Point(747, 153);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(34, 32);
            this.button4.TabIndex = 434;
            this.button4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(621, 321);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(91, 21);
            this.label7.TabIndex = 900;
            this.label7.Text = "Work Order";
            this.label7.Visible = false;
            // 
            // txtitem
            // 
            this.txtitem.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtitem.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtitem.Location = new System.Drawing.Point(625, 347);
            this.txtitem.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtitem.Name = "txtitem";
            this.txtitem.Size = new System.Drawing.Size(111, 27);
            this.txtitem.TabIndex = 4;
            this.txtitem.Visible = false;
            this.txtitem.Click += new System.EventHandler(this.txtitem_Click);
            this.txtitem.TextChanged += new System.EventHandler(this.txtitem_TextChanged);
            this.txtitem.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtitem_KeyDown);
            this.txtitem.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtitem_KeyPress);
            // 
            // tabPage3
            // 
            this.tabPage3.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.tabPage3.Controls.Add(this.panel4);
            this.tabPage3.Controls.Add(this.label16);
            this.tabPage3.Controls.Add(this.label14);
            this.tabPage3.Controls.Add(this.label4);
            this.tabPage3.Controls.Add(this.cbotype);
            this.tabPage3.Controls.Add(this.cbouom);
            this.tabPage3.Controls.Add(this.txtrate);
            this.tabPage3.Controls.Add(this.label19);
            this.tabPage3.Controls.Add(this.txtoutoutqty);
            this.tabPage3.Controls.Add(this.HFIT);
            this.tabPage3.Controls.Add(this.button11);
            this.tabPage3.Controls.Add(this.txtdcqty);
            this.tabPage3.Controls.Add(this.label21);
            this.tabPage3.Controls.Add(this.textBox7);
            this.tabPage3.Controls.Add(this.textBox8);
            this.tabPage3.Controls.Add(this.textBox9);
            this.tabPage3.Controls.Add(this.textBox10);
            this.tabPage3.Controls.Add(this.textBox11);
            this.tabPage3.Controls.Add(this.textBox12);
            this.tabPage3.Controls.Add(this.textBox13);
            this.tabPage3.Controls.Add(this.txtuom);
            this.tabPage3.Controls.Add(this.button12);
            this.tabPage3.Controls.Add(this.dataGridView3);
            this.tabPage3.Controls.Add(this.label22);
            this.tabPage3.Controls.Add(this.textBox15);
            this.tabPage3.Controls.Add(this.button2);
            this.tabPage3.Controls.Add(this.label23);
            this.tabPage3.Controls.Add(this.txtisstot);
            this.tabPage3.Controls.Add(this.label25);
            this.tabPage3.Controls.Add(this.label26);
            this.tabPage3.Controls.Add(this.txtmode);
            this.tabPage3.Controls.Add(this.label27);
            this.tabPage3.Controls.Add(this.textBox16);
            this.tabPage3.Controls.Add(this.button14);
            this.tabPage3.Controls.Add(this.label29);
            this.tabPage3.Controls.Add(this.label31);
            this.tabPage3.Controls.Add(this.txtaddnotes);
            this.tabPage3.Controls.Add(this.txtbeam);
            this.tabPage3.Controls.Add(this.serialno);
            this.tabPage3.Controls.Add(this.label5);
            this.tabPage3.Controls.Add(this.txtbillqty);
            this.tabPage3.Controls.Add(this.label6);
            this.tabPage3.Controls.Add(this.txtcolor);
            this.tabPage3.Location = new System.Drawing.Point(4, 28);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(1126, 442);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "PlanItems";
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.SystemColors.HighlightText;
            this.panel4.Controls.Add(this.cboprocess);
            this.panel4.Controls.Add(this.button19);
            this.panel4.Controls.Add(this.button21);
            this.panel4.Controls.Add(this.textBox1);
            this.panel4.Controls.Add(this.HFG9);
            this.panel4.Controls.Add(this.label8);
            this.panel4.Controls.Add(this.label15);
            this.panel4.Controls.Add(this.label11);
            this.panel4.Location = new System.Drawing.Point(48, 57);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(876, 359);
            this.panel4.TabIndex = 5600;
            // 
            // cboprocess
            // 
            this.cboprocess.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboprocess.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboprocess.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboprocess.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboprocess.FormattingEnabled = true;
            this.cboprocess.Location = new System.Drawing.Point(95, 13);
            this.cboprocess.Name = "cboprocess";
            this.cboprocess.Size = new System.Drawing.Size(286, 27);
            this.cboprocess.TabIndex = 5599;
            this.cboprocess.Visible = false;
            this.cboprocess.SelectedIndexChanged += new System.EventHandler(this.cboprocess_SelectedIndexChanged_1);
            // 
            // button19
            // 
            this.button19.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button19.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button19.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button19.Location = new System.Drawing.Point(784, 320);
            this.button19.Name = "button19";
            this.button19.Size = new System.Drawing.Size(70, 32);
            this.button19.TabIndex = 396;
            this.button19.Text = "Select";
            this.button19.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button19.UseVisualStyleBackColor = false;
            this.button19.Click += new System.EventHandler(this.button19_Click_1);
            // 
            // button21
            // 
            this.button21.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button21.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button21.Image = ((System.Drawing.Image)(resources.GetObject("button21.Image")));
            this.button21.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button21.Location = new System.Drawing.Point(12, 322);
            this.button21.Name = "button21";
            this.button21.Size = new System.Drawing.Size(74, 30);
            this.button21.TabIndex = 395;
            this.button21.Text = "Close";
            this.button21.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button21.UseVisualStyleBackColor = false;
            this.button21.Click += new System.EventHandler(this.button21_Click_1);
            // 
            // textBox1
            // 
            this.textBox1.AcceptsReturn = true;
            this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.Location = new System.Drawing.Point(14, 48);
            this.textBox1.Margin = new System.Windows.Forms.Padding(4);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(628, 26);
            this.textBox1.TabIndex = 98;
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged_1);
            // 
            // HFG9
            // 
            this.HFG9.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.HFG9.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.HFG9.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.HFG9.Location = new System.Drawing.Point(8, 81);
            this.HFG9.Name = "HFG9";
            this.HFG9.Size = new System.Drawing.Size(846, 239);
            this.HFG9.TabIndex = 97;
            this.HFG9.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.HFG9_CellContentClick);
            this.HFG9.DoubleClick += new System.EventHandler(this.HFG9_DoubleClick_1);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.White;
            this.label8.Location = new System.Drawing.Point(64, 85);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(81, 21);
            this.label8.TabIndex = 196;
            this.label8.Text = "Itemname";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.White;
            this.label15.Location = new System.Drawing.Point(64, 184);
            this.label15.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(124, 21);
            this.label15.TabIndex = 198;
            this.label15.Text = "Press ESC to Exit";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(23, 16);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(63, 21);
            this.label11.TabIndex = 5598;
            this.label11.Text = "Process";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(643, 3);
            this.label16.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(42, 21);
            this.label16.TabIndex = 5597;
            this.label16.Text = "Type";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(554, 3);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(86, 21);
            this.label14.TabIndex = 5596;
            this.label14.Text = "BillingUom";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(932, 6);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(41, 21);
            this.label4.TabIndex = 5591;
            this.label4.Text = "Rate";
            // 
            // cbotype
            // 
            this.cbotype.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbotype.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbotype.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbotype.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbotype.FormattingEnabled = true;
            this.cbotype.Location = new System.Drawing.Point(643, 29);
            this.cbotype.Name = "cbotype";
            this.cbotype.Size = new System.Drawing.Size(125, 27);
            this.cbotype.TabIndex = 5561;
            this.cbotype.SelectedIndexChanged += new System.EventHandler(this.cbotype_SelectedIndexChanged);
            // 
            // cbouom
            // 
            this.cbouom.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbouom.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbouom.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbouom.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbouom.FormattingEnabled = true;
            this.cbouom.Location = new System.Drawing.Point(554, 29);
            this.cbouom.Name = "cbouom";
            this.cbouom.Size = new System.Drawing.Size(86, 27);
            this.cbouom.TabIndex = 5560;
            this.cbouom.SelectedIndexChanged += new System.EventHandler(this.cbouom_SelectedIndexChanged_1);
            // 
            // txtrate
            // 
            this.txtrate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtrate.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtrate.Location = new System.Drawing.Point(930, 29);
            this.txtrate.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtrate.Name = "txtrate";
            this.txtrate.Size = new System.Drawing.Size(86, 27);
            this.txtrate.TabIndex = 5564;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(503, 9);
            this.label19.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(44, 21);
            this.label19.TabIndex = 5585;
            this.label19.Text = "Uom";
            // 
            // txtoutoutqty
            // 
            this.txtoutoutqty.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtoutoutqty.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtoutoutqty.Location = new System.Drawing.Point(503, 31);
            this.txtoutoutqty.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtoutoutqty.Name = "txtoutoutqty";
            this.txtoutoutqty.Size = new System.Drawing.Size(47, 27);
            this.txtoutoutqty.TabIndex = 5569;
            // 
            // HFIT
            // 
            this.HFIT.BackgroundColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.HFIT.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.HFIT.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.HFIT.Location = new System.Drawing.Point(48, 60);
            this.HFIT.Name = "HFIT";
            this.HFIT.Size = new System.Drawing.Size(977, 373);
            this.HFIT.TabIndex = 5587;
            this.HFIT.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.HFIT_CellClick_1);
            this.HFIT.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.HFIT_CellContentClick_1);
            // 
            // button11
            // 
            this.button11.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button11.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button11.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button11.Location = new System.Drawing.Point(1023, 27);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(37, 28);
            this.button11.TabIndex = 5567;
            this.button11.Text = "OK";
            this.button11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button11.UseVisualStyleBackColor = false;
            this.button11.Click += new System.EventHandler(this.button11_Click_1);
            // 
            // txtdcqty
            // 
            this.txtdcqty.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtdcqty.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtdcqty.Location = new System.Drawing.Point(48, 30);
            this.txtdcqty.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtdcqty.Name = "txtdcqty";
            this.txtdcqty.Size = new System.Drawing.Size(452, 27);
            this.txtdcqty.TabIndex = 5558;
            this.txtdcqty.Click += new System.EventHandler(this.txtdcqty_Click_1);
            this.txtdcqty.TextChanged += new System.EventHandler(this.txtdcqty_TextChanged_1);
            this.txtdcqty.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtdcqty_KeyDown_1);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(680, 158);
            this.label21.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(95, 21);
            this.label21.TabIndex = 5568;
            this.label21.Text = "Loop Length";
            // 
            // textBox7
            // 
            this.textBox7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox7.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox7.Location = new System.Drawing.Point(687, 180);
            this.textBox7.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new System.Drawing.Size(75, 22);
            this.textBox7.TabIndex = 5571;
            // 
            // textBox8
            // 
            this.textBox8.Location = new System.Drawing.Point(710, 183);
            this.textBox8.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new System.Drawing.Size(87, 27);
            this.textBox8.TabIndex = 5580;
            // 
            // textBox9
            // 
            this.textBox9.Location = new System.Drawing.Point(298, 183);
            this.textBox9.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new System.Drawing.Size(87, 27);
            this.textBox9.TabIndex = 5579;
            // 
            // textBox10
            // 
            this.textBox10.Location = new System.Drawing.Point(831, 217);
            this.textBox10.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new System.Drawing.Size(87, 27);
            this.textBox10.TabIndex = 5578;
            // 
            // textBox11
            // 
            this.textBox11.Location = new System.Drawing.Point(447, 116);
            this.textBox11.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new System.Drawing.Size(87, 27);
            this.textBox11.TabIndex = 5577;
            // 
            // textBox12
            // 
            this.textBox12.Location = new System.Drawing.Point(432, 161);
            this.textBox12.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new System.Drawing.Size(87, 27);
            this.textBox12.TabIndex = 5576;
            // 
            // textBox13
            // 
            this.textBox13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox13.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox13.Location = new System.Drawing.Point(399, 160);
            this.textBox13.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textBox13.Name = "textBox13";
            this.textBox13.Size = new System.Drawing.Size(101, 22);
            this.textBox13.TabIndex = 5586;
            // 
            // txtuom
            // 
            this.txtuom.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtuom.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtuom.Location = new System.Drawing.Point(768, 30);
            this.txtuom.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtuom.Name = "txtuom";
            this.txtuom.Size = new System.Drawing.Size(77, 27);
            this.txtuom.TabIndex = 5562;
            this.txtuom.TextChanged += new System.EventHandler(this.txtuom_TextChanged);
            // 
            // button12
            // 
            this.button12.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button12.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button12.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button12.Location = new System.Drawing.Point(734, 98);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(34, 32);
            this.button12.TabIndex = 5566;
            this.button12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button12.UseVisualStyleBackColor = false;
            // 
            // dataGridView3
            // 
            this.dataGridView3.BackgroundColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.dataGridView3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView3.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dataGridView3.Location = new System.Drawing.Point(577, 119);
            this.dataGridView3.Name = "dataGridView3";
            this.dataGridView3.Size = new System.Drawing.Size(87, 18);
            this.dataGridView3.TabIndex = 5575;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(521, 128);
            this.label22.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(98, 21);
            this.label22.TabIndex = 5584;
            this.label22.Text = "No of Beams";
            // 
            // textBox15
            // 
            this.textBox15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox15.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox15.Location = new System.Drawing.Point(827, 98);
            this.textBox15.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textBox15.Name = "textBox15";
            this.textBox15.Size = new System.Drawing.Size(90, 22);
            this.textBox15.TabIndex = 5583;
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button2.Location = new System.Drawing.Point(951, 170);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(34, 32);
            this.button2.TabIndex = 5565;
            this.button2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button2.UseVisualStyleBackColor = false;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(731, 72);
            this.label23.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(77, 21);
            this.label23.TabIndex = 5582;
            this.label23.Text = "NoofRolls";
            // 
            // txtisstot
            // 
            this.txtisstot.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtisstot.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtisstot.Location = new System.Drawing.Point(735, 98);
            this.txtisstot.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtisstot.Name = "txtisstot";
            this.txtisstot.Size = new System.Drawing.Size(74, 22);
            this.txtisstot.TabIndex = 5573;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(775, 8);
            this.label25.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(35, 21);
            this.label25.TabIndex = 5581;
            this.label25.Text = "Qty";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(773, 158);
            this.label26.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(44, 21);
            this.label26.TabIndex = 5589;
            this.label26.Text = "GSM";
            // 
            // txtmode
            // 
            this.txtmode.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtmode.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtmode.Location = new System.Drawing.Point(764, 180);
            this.txtmode.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtmode.Name = "txtmode";
            this.txtmode.Size = new System.Drawing.Size(65, 22);
            this.txtmode.TabIndex = 5570;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(749, 122);
            this.label27.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(43, 21);
            this.label27.TabIndex = 5588;
            this.label27.Text = "Total";
            // 
            // textBox16
            // 
            this.textBox16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox16.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox16.Location = new System.Drawing.Point(753, 148);
            this.textBox16.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textBox16.Name = "textBox16";
            this.textBox16.Size = new System.Drawing.Size(76, 22);
            this.textBox16.TabIndex = 5572;
            // 
            // button14
            // 
            this.button14.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button14.Location = new System.Drawing.Point(165, 332);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(266, 33);
            this.button14.TabIndex = 5595;
            this.button14.Text = "OUTPUT ITEM MAPPING";
            this.button14.UseVisualStyleBackColor = true;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(48, 8);
            this.label29.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(76, 19);
            this.label29.TabIndex = 5590;
            this.label29.Text = "ItemName";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(648, 103);
            this.label31.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(79, 21);
            this.label31.TabIndex = 5592;
            this.label31.Text = "AddNotes";
            // 
            // txtaddnotes
            // 
            this.txtaddnotes.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtaddnotes.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtaddnotes.Location = new System.Drawing.Point(659, 242);
            this.txtaddnotes.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtaddnotes.Name = "txtaddnotes";
            this.txtaddnotes.Size = new System.Drawing.Size(163, 22);
            this.txtaddnotes.TabIndex = 5574;
            // 
            // txtbeam
            // 
            this.txtbeam.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtbeam.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtbeam.Location = new System.Drawing.Point(831, 332);
            this.txtbeam.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtbeam.Name = "txtbeam";
            this.txtbeam.Size = new System.Drawing.Size(119, 22);
            this.txtbeam.TabIndex = 5599;
            // 
            // serialno
            // 
            this.serialno.Controls.Add(this.checkBox1);
            this.serialno.Controls.Add(this.label80);
            this.serialno.Controls.Add(this.Dataserial);
            this.serialno.Controls.Add(this.label78);
            this.serialno.Controls.Add(this.button20);
            this.serialno.Controls.Add(this.label76);
            this.serialno.Controls.Add(this.textBox17);
            this.serialno.Controls.Add(this.label79);
            this.serialno.Controls.Add(this.txtsrail);
            this.serialno.Controls.Add(this.cboserial);
            this.serialno.Controls.Add(this.label84);
            this.serialno.Controls.Add(this.txtserialqty);
            this.serialno.Controls.Add(this.label85);
            this.serialno.Controls.Add(this.button23);
            this.serialno.Controls.Add(this.label88);
            this.serialno.Controls.Add(this.textBox18);
            this.serialno.Controls.Add(this.textBox19);
            this.serialno.Controls.Add(this.txtsqty);
            this.serialno.Location = new System.Drawing.Point(508, 110);
            this.serialno.Name = "serialno";
            this.serialno.Size = new System.Drawing.Size(92, 40);
            this.serialno.TabIndex = 5593;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.BackColor = System.Drawing.Color.White;
            this.checkBox1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox1.Location = new System.Drawing.Point(696, 100);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(92, 22);
            this.checkBox1.TabIndex = 448;
            this.checkBox1.Text = "From BOM";
            this.checkBox1.UseVisualStyleBackColor = false;
            // 
            // label80
            // 
            this.label80.AutoSize = true;
            this.label80.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label80.Location = new System.Drawing.Point(200, 138);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(74, 18);
            this.label80.TabIndex = 447;
            this.label80.Text = "ItemName";
            // 
            // Dataserial
            // 
            this.Dataserial.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.Dataserial.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Dataserial.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.Dataserial.Location = new System.Drawing.Point(203, 201);
            this.Dataserial.Margin = new System.Windows.Forms.Padding(4);
            this.Dataserial.Name = "Dataserial";
            this.Dataserial.ReadOnly = true;
            this.Dataserial.Size = new System.Drawing.Size(471, 285);
            this.Dataserial.TabIndex = 446;
            // 
            // label78
            // 
            this.label78.AutoSize = true;
            this.label78.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label78.ForeColor = System.Drawing.Color.Red;
            this.label78.Location = new System.Drawing.Point(400, 53);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(202, 23);
            this.label78.TabIndex = 445;
            this.label78.Text = "OUTPUT ITEM MAPPING";
            // 
            // button20
            // 
            this.button20.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button20.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button20.Image = ((System.Drawing.Image)(resources.GetObject("button20.Image")));
            this.button20.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button20.Location = new System.Drawing.Point(701, 288);
            this.button20.Margin = new System.Windows.Forms.Padding(4);
            this.button20.Name = "button20";
            this.button20.Size = new System.Drawing.Size(60, 30);
            this.button20.TabIndex = 444;
            this.button20.Text = "Back";
            this.button20.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button20.UseVisualStyleBackColor = false;
            // 
            // label76
            // 
            this.label76.AutoSize = true;
            this.label76.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label76.Location = new System.Drawing.Point(590, 138);
            this.label76.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(30, 18);
            this.label76.TabIndex = 439;
            this.label76.Text = "Qty";
            // 
            // textBox17
            // 
            this.textBox17.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox17.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox17.Location = new System.Drawing.Point(597, 161);
            this.textBox17.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.textBox17.Name = "textBox17";
            this.textBox17.Size = new System.Drawing.Size(69, 26);
            this.textBox17.TabIndex = 430;
            this.textBox17.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label79
            // 
            this.label79.AutoSize = true;
            this.label79.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label79.Location = new System.Drawing.Point(394, 234);
            this.label79.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(41, 18);
            this.label79.TabIndex = 429;
            this.label79.Text = "UOM";
            // 
            // txtsrail
            // 
            this.txtsrail.Location = new System.Drawing.Point(203, 166);
            this.txtsrail.Name = "txtsrail";
            this.txtsrail.Size = new System.Drawing.Size(382, 27);
            this.txtsrail.TabIndex = 1;
            // 
            // cboserial
            // 
            this.cboserial.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboserial.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboserial.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboserial.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboserial.FormattingEnabled = true;
            this.cboserial.Location = new System.Drawing.Point(203, 98);
            this.cboserial.Name = "cboserial";
            this.cboserial.Size = new System.Drawing.Size(471, 26);
            this.cboserial.TabIndex = 416;
            // 
            // label84
            // 
            this.label84.AutoSize = true;
            this.label84.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label84.Location = new System.Drawing.Point(346, 203);
            this.label84.Name = "label84";
            this.label84.Size = new System.Drawing.Size(30, 18);
            this.label84.TabIndex = 402;
            this.label84.Text = "Qty";
            // 
            // txtserialqty
            // 
            this.txtserialqty.Location = new System.Drawing.Point(215, 236);
            this.txtserialqty.Name = "txtserialqty";
            this.txtserialqty.Size = new System.Drawing.Size(149, 27);
            this.txtserialqty.TabIndex = 403;
            // 
            // label85
            // 
            this.label85.AutoSize = true;
            this.label85.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label85.Location = new System.Drawing.Point(204, 74);
            this.label85.Name = "label85";
            this.label85.Size = new System.Drawing.Size(37, 18);
            this.label85.TabIndex = 400;
            this.label85.Text = "Item";
            // 
            // button23
            // 
            this.button23.Location = new System.Drawing.Point(690, 160);
            this.button23.Name = "button23";
            this.button23.Size = new System.Drawing.Size(40, 28);
            this.button23.TabIndex = 2;
            this.button23.Text = "OK";
            this.button23.UseVisualStyleBackColor = true;
            // 
            // label88
            // 
            this.label88.AutoSize = true;
            this.label88.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label88.Location = new System.Drawing.Point(237, -14);
            this.label88.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label88.Name = "label88";
            this.label88.Size = new System.Drawing.Size(27, 15);
            this.label88.TabIndex = 359;
            this.label88.Text = "Qty";
            // 
            // textBox18
            // 
            this.textBox18.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox18.Enabled = false;
            this.textBox18.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox18.Location = new System.Drawing.Point(339, 239);
            this.textBox18.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.textBox18.Name = "textBox18";
            this.textBox18.Size = new System.Drawing.Size(91, 22);
            this.textBox18.TabIndex = 435;
            this.textBox18.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBox19
            // 
            this.textBox19.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox19.Enabled = false;
            this.textBox19.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox19.Location = new System.Drawing.Point(340, 267);
            this.textBox19.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.textBox19.Name = "textBox19";
            this.textBox19.Size = new System.Drawing.Size(90, 22);
            this.textBox19.TabIndex = 431;
            this.textBox19.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtsqty
            // 
            this.txtsqty.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtsqty.Enabled = false;
            this.txtsqty.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtsqty.Location = new System.Drawing.Point(444, 303);
            this.txtsqty.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtsqty.Name = "txtsqty";
            this.txtsqty.Size = new System.Drawing.Size(89, 22);
            this.txtsqty.TabIndex = 404;
            this.txtsqty.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(843, 3);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(81, 21);
            this.label5.TabIndex = 5594;
            this.label5.Text = "Billing Qty";
            // 
            // txtbillqty
            // 
            this.txtbillqty.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtbillqty.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtbillqty.Location = new System.Drawing.Point(847, 30);
            this.txtbillqty.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtbillqty.Name = "txtbillqty";
            this.txtbillqty.Size = new System.Drawing.Size(76, 27);
            this.txtbillqty.TabIndex = 5563;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(936, 219);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(47, 21);
            this.label6.TabIndex = 5598;
            this.label6.Text = "Color";
            // 
            // txtcolor
            // 
            this.txtcolor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtcolor.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtcolor.Location = new System.Drawing.Point(936, 241);
            this.txtcolor.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtcolor.Name = "txtcolor";
            this.txtcolor.Size = new System.Drawing.Size(86, 27);
            this.txtcolor.TabIndex = 5559;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(23, 59);
            this.label18.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(132, 21);
            this.label18.TabIndex = 903;
            this.label18.Text = "Order Description";
            // 
            // txtorderdes
            // 
            this.txtorderdes.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtorderdes.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtorderdes.Location = new System.Drawing.Point(18, 85);
            this.txtorderdes.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtorderdes.Name = "txtorderdes";
            this.txtorderdes.Size = new System.Drawing.Size(233, 27);
            this.txtorderdes.TabIndex = 5;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(837, 7);
            this.label28.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(102, 19);
            this.label28.TabIndex = 432;
            this.label28.Text = "Required Date";
            this.label28.Click += new System.EventHandler(this.label28_Click);
            // 
            // reqdt
            // 
            this.reqdt.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.reqdt.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.reqdt.Location = new System.Drawing.Point(837, 31);
            this.reqdt.Name = "reqdt";
            this.reqdt.Size = new System.Drawing.Size(122, 27);
            this.reqdt.TabIndex = 3;
            this.reqdt.ValueChanged += new System.EventHandler(this.reqdt_ValueChanged);
            this.reqdt.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.reqdt_KeyPress);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(268, 7);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(75, 19);
            this.label9.TabIndex = 900;
            this.label9.Text = "Reference";
            // 
            // txtqty
            // 
            this.txtqty.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtqty.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtqty.Location = new System.Drawing.Point(268, 31);
            this.txtqty.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtqty.Name = "txtqty";
            this.txtqty.Size = new System.Drawing.Size(151, 27);
            this.txtqty.TabIndex = 1;
            this.txtqty.TextChanged += new System.EventHandler(this.txtqty_TextChanged);
            this.txtqty.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtqty_KeyPress);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(135, 7);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(69, 19);
            this.label2.TabIndex = 189;
            this.label2.Text = "Doc Date";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(430, 7);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(103, 19);
            this.label3.TabIndex = 12;
            this.label3.Text = "Supplier Name";
            // 
            // Phone
            // 
            this.Phone.AutoSize = true;
            this.Phone.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Phone.Location = new System.Drawing.Point(19, 7);
            this.Phone.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Phone.Name = "Phone";
            this.Phone.Size = new System.Drawing.Size(52, 19);
            this.Phone.TabIndex = 188;
            this.Phone.Text = "DocNo";
            // 
            // txtgrn
            // 
            this.txtgrn.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtgrn.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtgrn.Location = new System.Drawing.Point(19, 31);
            this.txtgrn.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtgrn.Name = "txtgrn";
            this.txtgrn.Size = new System.Drawing.Size(110, 27);
            this.txtgrn.TabIndex = 187;
            // 
            // dtpgrndt
            // 
            this.dtpgrndt.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpgrndt.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpgrndt.Location = new System.Drawing.Point(135, 31);
            this.dtpgrndt.Name = "dtpgrndt";
            this.dtpgrndt.Size = new System.Drawing.Size(122, 27);
            this.dtpgrndt.TabIndex = 0;
            this.dtpgrndt.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.dtpgrndt_KeyPress);
            // 
            // txtterms
            // 
            this.txtterms.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtterms.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtterms.Location = new System.Drawing.Point(671, 85);
            this.txtterms.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtterms.Name = "txtterms";
            this.txtterms.Size = new System.Drawing.Size(463, 27);
            this.txtterms.TabIndex = 992;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(676, 63);
            this.label20.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(136, 19);
            this.label20.TabIndex = 993;
            this.label20.Text = "Terms && Conditions";
            // 
            // txtname
            // 
            this.txtname.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtname.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtname.Location = new System.Drawing.Point(430, 31);
            this.txtname.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtname.Name = "txtname";
            this.txtname.Size = new System.Drawing.Size(400, 27);
            this.txtname.TabIndex = 2;
            this.txtname.Click += new System.EventHandler(this.txtname_Click);
            this.txtname.TextChanged += new System.EventHandler(this.txtname_TextChanged);
            this.txtname.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtname_KeyDown);
            this.txtname.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtname_KeyPress);
            // 
            // txtbeamid
            // 
            this.txtbeamid.Location = new System.Drawing.Point(564, 265);
            this.txtbeamid.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtbeamid.Name = "txtbeamid";
            this.txtbeamid.Size = new System.Drawing.Size(87, 20);
            this.txtbeamid.TabIndex = 222;
            // 
            // txtoutputid
            // 
            this.txtoutputid.Location = new System.Drawing.Point(268, 287);
            this.txtoutputid.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtoutputid.Name = "txtoutputid";
            this.txtoutputid.Size = new System.Drawing.Size(87, 20);
            this.txtoutputid.TabIndex = 221;
            // 
            // txtitemid
            // 
            this.txtitemid.Location = new System.Drawing.Point(478, 287);
            this.txtitemid.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtitemid.Name = "txtitemid";
            this.txtitemid.Size = new System.Drawing.Size(87, 20);
            this.txtitemid.TabIndex = 220;
            // 
            // txtmillid
            // 
            this.txtmillid.Location = new System.Drawing.Point(417, 220);
            this.txtmillid.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtmillid.Name = "txtmillid";
            this.txtmillid.Size = new System.Drawing.Size(87, 20);
            this.txtmillid.TabIndex = 219;
            // 
            // txtpuid
            // 
            this.txtpuid.Location = new System.Drawing.Point(402, 265);
            this.txtpuid.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtpuid.Name = "txtpuid";
            this.txtpuid.Size = new System.Drawing.Size(87, 20);
            this.txtpuid.TabIndex = 218;
            // 
            // txtgrnid
            // 
            this.txtgrnid.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtgrnid.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtgrnid.Location = new System.Drawing.Point(369, 264);
            this.txtgrnid.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtgrnid.Name = "txtgrnid";
            this.txtgrnid.Size = new System.Drawing.Size(101, 22);
            this.txtgrnid.TabIndex = 230;
            // 
            // txtnobeams
            // 
            this.txtnobeams.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtnobeams.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtnobeams.Location = new System.Drawing.Point(588, 232);
            this.txtnobeams.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtnobeams.Name = "txtnobeams";
            this.txtnobeams.Size = new System.Drawing.Size(90, 22);
            this.txtnobeams.TabIndex = 228;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(491, 232);
            this.label17.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(98, 21);
            this.label17.TabIndex = 227;
            this.label17.Text = "No of Beams";
            // 
            // txtrectot
            // 
            this.txtrectot.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtrectot.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtrectot.Location = new System.Drawing.Point(588, 199);
            this.txtrectot.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtrectot.Name = "txtrectot";
            this.txtrectot.Size = new System.Drawing.Size(90, 22);
            this.txtrectot.TabIndex = 226;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(436, 59);
            this.label30.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(142, 21);
            this.label30.TabIndex = 436;
            this.label30.Text = "Mode Of Transport";
            // 
            // txtmodem
            // 
            this.txtmodem.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtmodem.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtmodem.Location = new System.Drawing.Point(430, 85);
            this.txtmodem.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtmodem.Name = "txtmodem";
            this.txtmodem.Size = new System.Drawing.Size(233, 27);
            this.txtmodem.TabIndex = 8;
            this.txtmodem.TextChanged += new System.EventHandler(this.txtmodem_TextChanged);
            this.txtmodem.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtmodem_KeyDown);
            this.txtmodem.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtmodem_KeyPress);
            // 
            // txtvehicle
            // 
            this.txtvehicle.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtvehicle.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvehicle.Location = new System.Drawing.Point(262, 85);
            this.txtvehicle.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtvehicle.Name = "txtvehicle";
            this.txtvehicle.Size = new System.Drawing.Size(155, 27);
            this.txtvehicle.TabIndex = 7;
            this.txtvehicle.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtvehicle_KeyPress);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(267, 59);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 21);
            this.label1.TabIndex = 1;
            this.label1.Text = "Vehicle";
            // 
            // Genpan
            // 
            this.Genpan.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.Genpan.Controls.Add(this.txtscr1);
            this.Genpan.Controls.Add(this.HFGP);
            this.Genpan.Controls.Add(this.panel2);
            this.Genpan.Controls.Add(this.button5);
            this.Genpan.Controls.Add(this.button6);
            this.Genpan.Controls.Add(this.button7);
            this.Genpan.Controls.Add(this.button8);
            this.Genpan.Controls.Add(this.label57);
            this.Genpan.Controls.Add(this.dtpfnt);
            this.Genpan.Controls.Add(this.txtscr8);
            this.Genpan.Controls.Add(this.txtscr7);
            this.Genpan.Controls.Add(this.textBox2);
            this.Genpan.Controls.Add(this.textBox3);
            this.Genpan.Controls.Add(this.txtscr6);
            this.Genpan.Controls.Add(this.txtscr5);
            this.Genpan.Controls.Add(this.Txtscr2);
            this.Genpan.Controls.Add(this.txtscr4);
            this.Genpan.Controls.Add(this.Txtscr3);
            this.Genpan.Location = new System.Drawing.Point(1, -1);
            this.Genpan.Margin = new System.Windows.Forms.Padding(4);
            this.Genpan.Name = "Genpan";
            this.Genpan.Size = new System.Drawing.Size(1145, 588);
            this.Genpan.TabIndex = 215;
            this.Genpan.Paint += new System.Windows.Forms.PaintEventHandler(this.Genpan_Paint);
            // 
            // txtscr1
            // 
            this.txtscr1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtscr1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtscr1.Location = new System.Drawing.Point(1, 33);
            this.txtscr1.Margin = new System.Windows.Forms.Padding(5);
            this.txtscr1.Name = "txtscr1";
            this.txtscr1.Size = new System.Drawing.Size(1133, 26);
            this.txtscr1.TabIndex = 1;
            this.txtscr1.TextChanged += new System.EventHandler(this.txtscr1_TextChanged);
            // 
            // HFGP
            // 
            this.HFGP.BackgroundColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.HFGP.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.HFGP.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnF2;
            this.HFGP.Location = new System.Drawing.Point(1, 68);
            this.HFGP.Margin = new System.Windows.Forms.Padding(4);
            this.HFGP.Name = "HFGP";
            this.HFGP.ReadOnly = true;
            this.HFGP.Size = new System.Drawing.Size(1133, 487);
            this.HFGP.TabIndex = 3;
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel2.Controls.Add(this.label12);
            this.panel2.Controls.Add(this.label13);
            this.panel2.Controls.Add(this.flowLayoutPanel4);
            this.panel2.Controls.Add(this.flowLayoutPanel5);
            this.panel2.Controls.Add(this.flowLayoutPanel6);
            this.panel2.Location = new System.Drawing.Point(66, 390);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(60, 30);
            this.panel2.TabIndex = 214;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(4, 5);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(15, 16);
            this.label12.TabIndex = 163;
            this.label12.Text = "1";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Black;
            this.label13.Location = new System.Drawing.Point(27, 5);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(29, 16);
            this.label13.TabIndex = 162;
            this.label13.Text = "of 1";
            // 
            // flowLayoutPanel4
            // 
            this.flowLayoutPanel4.Location = new System.Drawing.Point(61, 30);
            this.flowLayoutPanel4.Name = "flowLayoutPanel4";
            this.flowLayoutPanel4.Size = new System.Drawing.Size(200, 100);
            this.flowLayoutPanel4.TabIndex = 2;
            // 
            // flowLayoutPanel5
            // 
            this.flowLayoutPanel5.Location = new System.Drawing.Point(38, 35);
            this.flowLayoutPanel5.Name = "flowLayoutPanel5";
            this.flowLayoutPanel5.Size = new System.Drawing.Size(200, 100);
            this.flowLayoutPanel5.TabIndex = 1;
            // 
            // flowLayoutPanel6
            // 
            this.flowLayoutPanel6.Location = new System.Drawing.Point(62, -129);
            this.flowLayoutPanel6.Name = "flowLayoutPanel6";
            this.flowLayoutPanel6.Size = new System.Drawing.Size(200, 100);
            this.flowLayoutPanel6.TabIndex = 0;
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.Color.White;
            this.button5.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button5.FlatAppearance.BorderSize = 0;
            this.button5.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button5.Image = ((System.Drawing.Image)(resources.GetObject("button5.Image")));
            this.button5.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button5.Location = new System.Drawing.Point(19, 390);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(19, 31);
            this.button5.TabIndex = 213;
            this.button5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button5.UseVisualStyleBackColor = false;
            // 
            // button6
            // 
            this.button6.BackColor = System.Drawing.Color.White;
            this.button6.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button6.FlatAppearance.BorderSize = 0;
            this.button6.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button6.Image = ((System.Drawing.Image)(resources.GetObject("button6.Image")));
            this.button6.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button6.Location = new System.Drawing.Point(44, 390);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(18, 31);
            this.button6.TabIndex = 212;
            this.button6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button6.UseVisualStyleBackColor = false;
            // 
            // button7
            // 
            this.button7.BackColor = System.Drawing.Color.White;
            this.button7.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button7.FlatAppearance.BorderSize = 0;
            this.button7.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button7.Image = ((System.Drawing.Image)(resources.GetObject("button7.Image")));
            this.button7.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button7.Location = new System.Drawing.Point(156, 390);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(19, 31);
            this.button7.TabIndex = 211;
            this.button7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button7.UseVisualStyleBackColor = false;
            // 
            // button8
            // 
            this.button8.BackColor = System.Drawing.Color.White;
            this.button8.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button8.FlatAppearance.BorderSize = 0;
            this.button8.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button8.Image = ((System.Drawing.Image)(resources.GetObject("button8.Image")));
            this.button8.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button8.Location = new System.Drawing.Point(132, 390);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(18, 31);
            this.button8.TabIndex = 210;
            this.button8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button8.UseVisualStyleBackColor = false;
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label57.Location = new System.Drawing.Point(411, 167);
            this.label57.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(42, 21);
            this.label57.TabIndex = 223;
            this.label57.Text = "Date";
            // 
            // dtpfnt
            // 
            this.dtpfnt.CustomFormat = "MMM/yyyy";
            this.dtpfnt.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpfnt.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpfnt.Location = new System.Drawing.Point(458, 164);
            this.dtpfnt.Margin = new System.Windows.Forms.Padding(4);
            this.dtpfnt.Name = "dtpfnt";
            this.dtpfnt.Size = new System.Drawing.Size(104, 26);
            this.dtpfnt.TabIndex = 222;
            this.dtpfnt.Value = new System.DateTime(2017, 7, 4, 0, 0, 0, 0);
            // 
            // txtscr8
            // 
            this.txtscr8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtscr8.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtscr8.Location = new System.Drawing.Point(651, 230);
            this.txtscr8.Margin = new System.Windows.Forms.Padding(5);
            this.txtscr8.Name = "txtscr8";
            this.txtscr8.Size = new System.Drawing.Size(100, 26);
            this.txtscr8.TabIndex = 225;
            // 
            // txtscr7
            // 
            this.txtscr7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtscr7.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtscr7.Location = new System.Drawing.Point(667, 225);
            this.txtscr7.Margin = new System.Windows.Forms.Padding(5);
            this.txtscr7.Name = "txtscr7";
            this.txtscr7.Size = new System.Drawing.Size(164, 26);
            this.txtscr7.TabIndex = 224;
            // 
            // textBox2
            // 
            this.textBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox2.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox2.Location = new System.Drawing.Point(642, 162);
            this.textBox2.Margin = new System.Windows.Forms.Padding(5);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(297, 26);
            this.textBox2.TabIndex = 227;
            // 
            // textBox3
            // 
            this.textBox3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox3.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox3.Location = new System.Drawing.Point(235, 162);
            this.textBox3.Margin = new System.Windows.Forms.Padding(5);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(303, 26);
            this.textBox3.TabIndex = 226;
            // 
            // txtscr6
            // 
            this.txtscr6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtscr6.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtscr6.Location = new System.Drawing.Point(539, 162);
            this.txtscr6.Margin = new System.Windows.Forms.Padding(5);
            this.txtscr6.Name = "txtscr6";
            this.txtscr6.Size = new System.Drawing.Size(104, 26);
            this.txtscr6.TabIndex = 202;
            // 
            // txtscr5
            // 
            this.txtscr5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtscr5.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtscr5.Location = new System.Drawing.Point(288, 162);
            this.txtscr5.Margin = new System.Windows.Forms.Padding(5);
            this.txtscr5.Name = "txtscr5";
            this.txtscr5.Size = new System.Drawing.Size(250, 26);
            this.txtscr5.TabIndex = 90;
            // 
            // Txtscr2
            // 
            this.Txtscr2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Txtscr2.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txtscr2.Location = new System.Drawing.Point(-105, 162);
            this.Txtscr2.Margin = new System.Windows.Forms.Padding(5);
            this.Txtscr2.Name = "Txtscr2";
            this.Txtscr2.Size = new System.Drawing.Size(90, 26);
            this.Txtscr2.TabIndex = 87;
            // 
            // txtscr4
            // 
            this.txtscr4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtscr4.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtscr4.Location = new System.Drawing.Point(311, 169);
            this.txtscr4.Margin = new System.Windows.Forms.Padding(5);
            this.txtscr4.Name = "txtscr4";
            this.txtscr4.Size = new System.Drawing.Size(90, 22);
            this.txtscr4.TabIndex = 100;
            // 
            // Txtscr3
            // 
            this.Txtscr3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Txtscr3.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txtscr3.Location = new System.Drawing.Point(222, 169);
            this.Txtscr3.Margin = new System.Windows.Forms.Padding(5);
            this.Txtscr3.Name = "Txtscr3";
            this.Txtscr3.Size = new System.Drawing.Size(90, 22);
            this.Txtscr3.TabIndex = 88;
            // 
            // panadd
            // 
            this.panadd.BackColor = System.Drawing.Color.White;
            this.panadd.Controls.Add(this.button13);
            this.panadd.Controls.Add(this.button1);
            this.panadd.Controls.Add(this.buttnext1);
            this.panadd.Controls.Add(this.chkact);
            this.panadd.Controls.Add(this.butcan);
            this.panadd.Controls.Add(this.butedit);
            this.panadd.Controls.Add(this.button3);
            this.panadd.Location = new System.Drawing.Point(2, 584);
            this.panadd.Name = "panadd";
            this.panadd.Size = new System.Drawing.Size(902, 34);
            this.panadd.TabIndex = 236;
            // 
            // button13
            // 
            this.button13.BackColor = System.Drawing.Color.White;
            this.button13.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button13.Image = ((System.Drawing.Image)(resources.GetObject("button13.Image")));
            this.button13.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button13.Location = new System.Drawing.Point(381, 2);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(117, 30);
            this.button13.TabIndex = 225;
            this.button13.Text = "Print Preview";
            this.button13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button13.UseVisualStyleBackColor = false;
            this.button13.Click += new System.EventHandler(this.button13_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.White;
            this.button1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Image = ((System.Drawing.Image)(resources.GetObject("button1.Image")));
            this.button1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button1.Location = new System.Drawing.Point(313, 3);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(65, 30);
            this.button1.TabIndex = 216;
            this.button1.Text = "Print";
            this.button1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // buttnext1
            // 
            this.buttnext1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttnext1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttnext1.Image = ((System.Drawing.Image)(resources.GetObject("buttnext1.Image")));
            this.buttnext1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttnext1.Location = new System.Drawing.Point(504, 3);
            this.buttnext1.Name = "buttnext1";
            this.buttnext1.Size = new System.Drawing.Size(57, 30);
            this.buttnext1.TabIndex = 208;
            this.buttnext1.Text = "Exit";
            this.buttnext1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttnext1.UseVisualStyleBackColor = false;
            this.buttnext1.Click += new System.EventHandler(this.buttnext1_Click);
            // 
            // chkact
            // 
            this.chkact.AutoSize = true;
            this.chkact.BackColor = System.Drawing.Color.White;
            this.chkact.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkact.Location = new System.Drawing.Point(4, 8);
            this.chkact.Name = "chkact";
            this.chkact.Size = new System.Drawing.Size(66, 22);
            this.chkact.TabIndex = 187;
            this.chkact.Text = "Active";
            this.chkact.UseVisualStyleBackColor = false;
            // 
            // butcan
            // 
            this.butcan.BackColor = System.Drawing.Color.White;
            this.butcan.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.butcan.Image = ((System.Drawing.Image)(resources.GetObject("butcan.Image")));
            this.butcan.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.butcan.Location = new System.Drawing.Point(228, 2);
            this.butcan.Name = "butcan";
            this.butcan.Size = new System.Drawing.Size(77, 30);
            this.butcan.TabIndex = 186;
            this.butcan.Text = "Delete";
            this.butcan.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.butcan.UseVisualStyleBackColor = false;
            this.butcan.Click += new System.EventHandler(this.butcan_Click);
            // 
            // butedit
            // 
            this.butedit.BackColor = System.Drawing.Color.White;
            this.butedit.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.butedit.Image = ((System.Drawing.Image)(resources.GetObject("butedit.Image")));
            this.butedit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.butedit.Location = new System.Drawing.Point(168, 2);
            this.butedit.Name = "butedit";
            this.butedit.Size = new System.Drawing.Size(55, 30);
            this.butedit.TabIndex = 185;
            this.butedit.Text = "Edit";
            this.butedit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.butedit.UseVisualStyleBackColor = false;
            this.butedit.Click += new System.EventHandler(this.butedit_Click);
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.White;
            this.button3.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button3.FlatAppearance.BorderSize = 0;
            this.button3.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.Image = ((System.Drawing.Image)(resources.GetObject("button3.Image")));
            this.button3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button3.Location = new System.Drawing.Point(75, 2);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(89, 30);
            this.button3.TabIndex = 184;
            this.button3.Text = "Add New";
            this.button3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // Chkedtact
            // 
            this.Chkedtact.AutoSize = true;
            this.Chkedtact.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Chkedtact.Location = new System.Drawing.Point(943, 598);
            this.Chkedtact.Name = "Chkedtact";
            this.Chkedtact.Size = new System.Drawing.Size(62, 20);
            this.Chkedtact.TabIndex = 335;
            this.Chkedtact.Text = "Active";
            this.Chkedtact.UseVisualStyleBackColor = true;
            // 
            // buttnfinbk
            // 
            this.buttnfinbk.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttnfinbk.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttnfinbk.Image = ((System.Drawing.Image)(resources.GetObject("buttnfinbk.Image")));
            this.buttnfinbk.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttnfinbk.Location = new System.Drawing.Point(1086, 591);
            this.buttnfinbk.Margin = new System.Windows.Forms.Padding(4);
            this.buttnfinbk.Name = "buttnfinbk";
            this.buttnfinbk.Size = new System.Drawing.Size(60, 30);
            this.buttnfinbk.TabIndex = 333;
            this.buttnfinbk.Text = "Back";
            this.buttnfinbk.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttnfinbk.UseVisualStyleBackColor = false;
            this.buttnfinbk.Click += new System.EventHandler(this.buttnfinbk_Click);
            // 
            // btnsave
            // 
            this.btnsave.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnsave.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnsave.Image = ((System.Drawing.Image)(resources.GetObject("btnsave.Image")));
            this.btnsave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnsave.Location = new System.Drawing.Point(1009, 591);
            this.btnsave.Margin = new System.Windows.Forms.Padding(4);
            this.btnsave.Name = "btnsave";
            this.btnsave.Size = new System.Drawing.Size(69, 30);
            this.btnsave.TabIndex = 334;
            this.btnsave.Text = "Save";
            this.btnsave.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnsave.UseVisualStyleBackColor = false;
            this.btnsave.Click += new System.EventHandler(this.btnsave_Click);
            // 
            // FrmJOI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1148, 619);
            this.Controls.Add(this.panadd);
            this.Controls.Add(this.buttnfinbk);
            this.Controls.Add(this.btnsave);
            this.Controls.Add(this.Chkedtact);
            this.Controls.Add(this.Genpan);
            this.Controls.Add(this.Editpnl);
            this.Name = "FrmJOI";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Load += new System.EventHandler(this.FrmJOI_Load);
            this.Editpnl.ResumeLayout(false);
            this.Editpnl.PerformLayout();
            this.grSearch.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DataGridCommon)).EndInit();
            this.tabC.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HFGP1)).EndInit();
            this.lkppnl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.HFGP2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RQGR)).EndInit();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HFG9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HFIT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).EndInit();
            this.serialno.ResumeLayout(false);
            this.serialno.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Dataserial)).EndInit();
            this.Genpan.ResumeLayout(false);
            this.Genpan.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HFGP)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panadd.ResumeLayout(false);
            this.panadd.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel Editpnl;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtqty;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtitem;
        private System.Windows.Forms.TextBox txtvehicle;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label Phone;
        private System.Windows.Forms.TextBox txtgrn;
        private System.Windows.Forms.DateTimePicker dtpgrndt;
        private System.Windows.Forms.TextBox txtname;
        private System.Windows.Forms.TextBox txtbeamid;
        private System.Windows.Forms.TextBox txtoutputid;
        private System.Windows.Forms.TextBox txtitemid;
        private System.Windows.Forms.TextBox txtmillid;
        private System.Windows.Forms.TextBox txtpuid;
        private System.Windows.Forms.TextBox txtgrnid;
        private System.Windows.Forms.TextBox txtnobeams;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtrectot;
        private System.Windows.Forms.Panel Genpan;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox txtscr7;
        private System.Windows.Forms.TextBox txtscr6;
        private System.Windows.Forms.TextBox txtscr5;
        private System.Windows.Forms.TextBox txtscr4;
        private System.Windows.Forms.TextBox Txtscr3;
        private System.Windows.Forms.TextBox Txtscr2;
        private System.Windows.Forms.TextBox txtscr1;
        private System.Windows.Forms.DataGridView HFGP;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel4;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel5;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel6;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.DateTimePicker dtpfnt;
        private System.Windows.Forms.TextBox txtscr8;
        private System.Windows.Forms.Panel panadd;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button buttnext1;
        private System.Windows.Forms.CheckBox chkact;
        private System.Windows.Forms.Button butcan;
        private System.Windows.Forms.Button butedit;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button btnsave;
        private System.Windows.Forms.Button buttnfinbk;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.DateTimePicker reqdt;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TextBox txtmodem;
        private System.Windows.Forms.TabControl tabC;
        private System.Windows.Forms.Panel grSearch;
        private System.Windows.Forms.Button button18;
        private System.Windows.Forms.Button btnHide;
        private System.Windows.Forms.DataGridView DataGridCommon;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TextBox txtoutuom;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Panel lkppnl;
        private System.Windows.Forms.Button button15;
        private System.Windows.Forms.Button button16;
        private System.Windows.Forms.DataGridView HFGP2;
        private System.Windows.Forms.Button button17;
        private System.Windows.Forms.DataGridView RQGR;
        private System.Windows.Forms.TextBox txtoutqty;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.TextBox txtoutrolls;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.TextBox txtoutitem;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.CheckBox Chkedtact;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.TextBox txtscr11;
        private System.Windows.Forms.DataGridView HFGP1;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txtorderdes;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox txtterms;
        private System.Windows.Forms.Button button22;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Button button19;
        private System.Windows.Forms.Button button21;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.DataGridView HFG9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtcolor;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cbotype;
        private System.Windows.Forms.ComboBox cbouom;
        private System.Windows.Forms.TextBox txtrate;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txtbillqty;
        private System.Windows.Forms.TextBox txtoutoutqty;
        private System.Windows.Forms.DataGridView HFIT;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.TextBox txtdcqty;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.TextBox textBox9;
        private System.Windows.Forms.TextBox textBox10;
        private System.Windows.Forms.TextBox textBox11;
        private System.Windows.Forms.TextBox textBox12;
        private System.Windows.Forms.TextBox textBox13;
        private System.Windows.Forms.TextBox txtuom;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.DataGridView dataGridView3;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox textBox15;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox txtisstot;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox txtmode;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox textBox16;
        private System.Windows.Forms.Button button14;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.TextBox txtaddnotes;
        private System.Windows.Forms.TextBox txtbeam;
        private System.Windows.Forms.Panel serialno;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.Label label80;
        private System.Windows.Forms.DataGridView Dataserial;
        private System.Windows.Forms.Label label78;
        private System.Windows.Forms.Button button20;
        private System.Windows.Forms.Label label76;
        private System.Windows.Forms.TextBox textBox17;
        private System.Windows.Forms.Label label79;
        private System.Windows.Forms.TextBox txtsrail;
        private System.Windows.Forms.ComboBox cboserial;
        private System.Windows.Forms.Label label84;
        private System.Windows.Forms.TextBox txtserialqty;
        private System.Windows.Forms.Label label85;
        private System.Windows.Forms.Button button23;
        private System.Windows.Forms.Label label88;
        private System.Windows.Forms.TextBox textBox18;
        private System.Windows.Forms.TextBox textBox19;
        private System.Windows.Forms.TextBox txtsqty;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox cboprocess;
        private System.Windows.Forms.ComboBox cbowo;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.ComboBox cbopro1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtwor;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.TextBox txtstyle;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.TextBox txtsocno;
    }
}