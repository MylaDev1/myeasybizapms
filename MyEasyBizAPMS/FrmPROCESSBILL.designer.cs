﻿namespace MyEasyBizAPMS
{
    partial class FrmPROCESSBILL
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing && (components != null))
        //    {
        //        components.Dispose();
        //    }
        //    base.Dispose(disposing);
        //}

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmPROCESSBILL));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.Genpan = new System.Windows.Forms.Panel();
            this.label63 = new System.Windows.Forms.Label();
            this.label60 = new System.Windows.Forms.Label();
            this.dtpfnt = new System.Windows.Forms.DateTimePicker();
            this.txtscr1 = new System.Windows.Forms.TextBox();
            this.HFGP = new System.Windows.Forms.DataGridView();
            this.buttnnxtlft = new System.Windows.Forms.Button();
            this.txtscr5 = new System.Windows.Forms.TextBox();
            this.btnfinnxt = new System.Windows.Forms.Button();
            this.buttrnxt = new System.Windows.Forms.Button();
            this.txtscr8 = new System.Windows.Forms.TextBox();
            this.Txtscr3 = new System.Windows.Forms.TextBox();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblno1 = new System.Windows.Forms.Label();
            this.lblno2 = new System.Windows.Forms.Label();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.button10 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.buttnnvfst = new System.Windows.Forms.Button();
            this.txtscr9 = new System.Windows.Forms.TextBox();
            this.txtscr7 = new System.Windows.Forms.TextBox();
            this.txtscr6 = new System.Windows.Forms.TextBox();
            this.txtscr4 = new System.Windows.Forms.TextBox();
            this.Txtscr2 = new System.Windows.Forms.TextBox();
            this.Taxpan = new System.Windows.Forms.Panel();
            this.txttotaddd = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.addipan = new System.Windows.Forms.Panel();
            this.label56 = new System.Windows.Forms.Label();
            this.HFGT = new System.Windows.Forms.DataGridView();
            this.panadd = new System.Windows.Forms.Panel();
            this.button21 = new System.Windows.Forms.Button();
            this.txtchargessum = new System.Windows.Forms.TextBox();
            this.txtbasicval = new System.Windows.Forms.TextBox();
            this.txttax = new System.Windows.Forms.TextBox();
            this.txttotamt = new System.Windows.Forms.TextBox();
            this.label58 = new System.Windows.Forms.Label();
            this.button13 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.buttnext1 = new System.Windows.Forms.Button();
            this.chkact = new System.Windows.Forms.CheckBox();
            this.butedit = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.butcan = new System.Windows.Forms.Button();
            this.buttnfinbk = new System.Windows.Forms.Button();
            this.btnsave = new System.Windows.Forms.Button();
            this.label59 = new System.Windows.Forms.Label();
            this.label57 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.button7 = new System.Windows.Forms.Button();
            this.txtprice = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.txtamt = new System.Windows.Forms.TextBox();
            this.button3 = new System.Windows.Forms.Button();
            this.txtot = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.txtcharges = new System.Windows.Forms.TextBox();
            this.txttprdval = new System.Windows.Forms.TextBox();
            this.label47 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.dcdate = new System.Windows.Forms.DateTimePicker();
            this.txtgen1 = new System.Windows.Forms.TextBox();
            this.txttsgval = new System.Windows.Forms.TextBox();
            this.label48 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.txttcgstp = new System.Windows.Forms.TextBox();
            this.txttcgval = new System.Windows.Forms.TextBox();
            this.label51 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.txtsgstp = new System.Windows.Forms.TextBox();
            this.label52 = new System.Windows.Forms.Label();
            this.txtigstp = new System.Windows.Forms.TextBox();
            this.txtgen3 = new System.Windows.Forms.TextBox();
            this.txtgen2 = new System.Windows.Forms.TextBox();
            this.button12 = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.btnaddrcan = new System.Windows.Forms.Button();
            this.buttnnxt = new System.Windows.Forms.Button();
            this.txttgstp = new System.Windows.Forms.TextBox();
            this.txttgstval = new System.Windows.Forms.TextBox();
            this.txtlisid = new System.Windows.Forms.TextBox();
            this.label41 = new System.Windows.Forms.Label();
            this.txtpadd2 = new System.Windows.Forms.RichTextBox();
            this.txtplace = new System.Windows.Forms.TextBox();
            this.label37 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.txtitemcode = new System.Windows.Forms.TextBox();
            this.dtpdc = new System.Windows.Forms.DateTimePicker();
            this.txtqty = new System.Windows.Forms.TextBox();
            this.txtbval = new System.Windows.Forms.TextBox();
            this.txtnotes = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.buttcusok = new System.Windows.Forms.Button();
            this.label42 = new System.Windows.Forms.Label();
            this.txtitemname = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.button4 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.txttempadd1 = new System.Windows.Forms.TextBox();
            this.txttempadd2 = new System.Windows.Forms.TextBox();
            this.pantax = new System.Windows.Forms.Panel();
            this.txttitem = new System.Windows.Forms.RichTextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.txttqty = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.Txtrate = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.txtbasic = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.button5 = new System.Windows.Forms.Button();
            this.txttotal = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.txtigcst = new System.Windows.Forms.TextBox();
            this.cboigst = new System.Windows.Forms.ComboBox();
            this.label22 = new System.Windows.Forms.Label();
            this.txtsgst = new System.Windows.Forms.TextBox();
            this.SGST = new System.Windows.Forms.ComboBox();
            this.label21 = new System.Windows.Forms.Label();
            this.txtcgst = new System.Windows.Forms.TextBox();
            this.cbocgst = new System.Windows.Forms.ComboBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.txtper = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txttaxable = new System.Windows.Forms.TextBox();
            this.txthidqty = new System.Windows.Forms.TextBox();
            this.label38 = new System.Windows.Forms.Label();
            this.Dtppre = new System.Windows.Forms.DateTimePicker();
            this.label39 = new System.Windows.Forms.Label();
            this.Dtprem = new System.Windows.Forms.DateTimePicker();
            this.txtuom = new System.Windows.Forms.TextBox();
            this.label44 = new System.Windows.Forms.Label();
            this.txtpluid = new System.Windows.Forms.TextBox();
            this.txttitemid = new System.Windows.Forms.TextBox();
            this.txtrem = new System.Windows.Forms.RichTextBox();
            this.txtdcid = new System.Windows.Forms.TextBox();
            this.txtgrnid = new System.Windows.Forms.TextBox();
            this.txtpuid = new System.Windows.Forms.TextBox();
            this.txtgrn = new System.Windows.Forms.TextBox();
            this.Phone = new System.Windows.Forms.Label();
            this.txtname = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtdcno = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.Dtpdt = new System.Windows.Forms.DateTimePicker();
            this.DTPDOCDT = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.Txttot = new System.Windows.Forms.TextBox();
            this.txttrans = new System.Windows.Forms.TextBox();
            this.label40 = new System.Windows.Forms.Label();
            this.label55 = new System.Windows.Forms.Label();
            this.txtpadd1 = new System.Windows.Forms.RichTextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.TxtRoff = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.TxtNetAmt = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.txttdis = new System.Windows.Forms.TextBox();
            this.label43 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.txttbval = new System.Windows.Forms.TextBox();
            this.txttdisc = new System.Windows.Forms.TextBox();
            this.label46 = new System.Windows.Forms.Label();
            this.label53 = new System.Windows.Forms.Label();
            this.txtigval = new System.Windows.Forms.TextBox();
            this.txtttot = new System.Windows.Forms.TextBox();
            this.label54 = new System.Windows.Forms.Label();
            this.txtexcise = new System.Windows.Forms.TextBox();
            this.HFGST = new System.Windows.Forms.DataGridView();
            this.serialno = new System.Windows.Forms.Panel();
            this.Dataserial = new System.Windows.Forms.DataGridView();
            this.label78 = new System.Windows.Forms.Label();
            this.button20 = new System.Windows.Forms.Button();
            this.label76 = new System.Windows.Forms.Label();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.label79 = new System.Windows.Forms.Label();
            this.label80 = new System.Windows.Forms.Label();
            this.txtsrail = new System.Windows.Forms.TextBox();
            this.cboserial = new System.Windows.Forms.ComboBox();
            this.label84 = new System.Windows.Forms.Label();
            this.txtserialqty = new System.Windows.Forms.TextBox();
            this.label85 = new System.Windows.Forms.Label();
            this.button23 = new System.Windows.Forms.Button();
            this.label88 = new System.Windows.Forms.Label();
            this.textBox14 = new System.Windows.Forms.TextBox();
            this.textBox15 = new System.Windows.Forms.TextBox();
            this.textBox16 = new System.Windows.Forms.TextBox();
            this.mappnl = new System.Windows.Forms.Panel();
            this.label77 = new System.Windows.Forms.Label();
            this.button19 = new System.Windows.Forms.Button();
            this.txtqty1 = new System.Windows.Forms.TextBox();
            this.txtbags = new System.Windows.Forms.TextBox();
            this.label71 = new System.Windows.Forms.Label();
            this.cbosGReturnItem = new System.Windows.Forms.ComboBox();
            this.DataGridWeight = new System.Windows.Forms.DataGridView();
            this.label65 = new System.Windows.Forms.Label();
            this.txtWeight = new System.Windows.Forms.TextBox();
            this.label66 = new System.Windows.Forms.Label();
            this.txtBagNo = new System.Windows.Forms.TextBox();
            this.label67 = new System.Windows.Forms.Label();
            this.btnOk = new System.Windows.Forms.Button();
            this.label68 = new System.Windows.Forms.Label();
            this.txtGrossWght = new System.Windows.Forms.TextBox();
            this.label69 = new System.Windows.Forms.Label();
            this.txtTarWght = new System.Windows.Forms.TextBox();
            this.label75 = new System.Windows.Forms.Label();
            this.txtlotno2 = new System.Windows.Forms.TextBox();
            this.txtstart1 = new System.Windows.Forms.TextBox();
            this.txttarewt1 = new System.Windows.Forms.TextBox();
            this.button16 = new System.Windows.Forms.Button();
            this.txtlotno = new System.Windows.Forms.TextBox();
            this.txttarewt = new System.Windows.Forms.TextBox();
            this.txtstart = new System.Windows.Forms.TextBox();
            this.button14 = new System.Windows.Forms.Button();
            this.button15 = new System.Windows.Forms.Button();
            this.label74 = new System.Windows.Forms.Label();
            this.label73 = new System.Windows.Forms.Label();
            this.label72 = new System.Windows.Forms.Label();
            this.button17 = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.txtNoogBags = new System.Windows.Forms.TextBox();
            this.label64 = new System.Windows.Forms.Label();
            this.txtgrossgen = new System.Windows.Forms.TextBox();
            this.label70 = new System.Windows.Forms.Label();
            this.txtlotno1 = new System.Windows.Forms.TextBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.button24 = new System.Windows.Forms.Button();
            this.button22 = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label81 = new System.Windows.Forms.Label();
            this.HFGPT = new System.Windows.Forms.DataGridView();
            this.txttaxtot = new System.Windows.Forms.TextBox();
            this.label61 = new System.Windows.Forms.Label();
            this.txttype = new System.Windows.Forms.TextBox();
            this.label83 = new System.Windows.Forms.Label();
            this.cbopono = new System.Windows.Forms.ComboBox();
            this.txtgrnno = new System.Windows.Forms.TextBox();
            this.label82 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.HFIT = new System.Windows.Forms.DataGridView();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.ItemDet = new System.Windows.Forms.DataGridView();
            this.grSearch = new System.Windows.Forms.Panel();
            this.button18 = new System.Windows.Forms.Button();
            this.btnHide = new System.Windows.Forms.Button();
            this.DataGridCommon = new System.Windows.Forms.DataGridView();
            this.Editpan = new System.Windows.Forms.Panel();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.label86 = new System.Windows.Forms.Label();
            this.Genpan.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HFGP)).BeginInit();
            this.panel1.SuspendLayout();
            this.Taxpan.SuspendLayout();
            this.addipan.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HFGT)).BeginInit();
            this.panadd.SuspendLayout();
            this.pantax.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HFGST)).BeginInit();
            this.serialno.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Dataserial)).BeginInit();
            this.mappnl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridWeight)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HFGPT)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HFIT)).BeginInit();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ItemDet)).BeginInit();
            this.grSearch.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridCommon)).BeginInit();
            this.Editpan.SuspendLayout();
            this.SuspendLayout();
            // 
            // Genpan
            // 
            this.Genpan.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.Genpan.Controls.Add(this.label63);
            this.Genpan.Controls.Add(this.label60);
            this.Genpan.Controls.Add(this.dtpfnt);
            this.Genpan.Controls.Add(this.txtscr1);
            this.Genpan.Controls.Add(this.HFGP);
            this.Genpan.Controls.Add(this.buttnnxtlft);
            this.Genpan.Controls.Add(this.txtscr5);
            this.Genpan.Controls.Add(this.btnfinnxt);
            this.Genpan.Controls.Add(this.buttrnxt);
            this.Genpan.Controls.Add(this.txtscr8);
            this.Genpan.Controls.Add(this.Txtscr3);
            this.Genpan.Controls.Add(this.dateTimePicker1);
            this.Genpan.Controls.Add(this.panel1);
            this.Genpan.Controls.Add(this.button10);
            this.Genpan.Controls.Add(this.button9);
            this.Genpan.Controls.Add(this.buttnnvfst);
            this.Genpan.Controls.Add(this.txtscr9);
            this.Genpan.Controls.Add(this.txtscr7);
            this.Genpan.Controls.Add(this.txtscr6);
            this.Genpan.Controls.Add(this.txtscr4);
            this.Genpan.Controls.Add(this.Txtscr2);
            this.Genpan.Location = new System.Drawing.Point(-1, -2);
            this.Genpan.Margin = new System.Windows.Forms.Padding(4);
            this.Genpan.Name = "Genpan";
            this.Genpan.Size = new System.Drawing.Size(988, 600);
            this.Genpan.TabIndex = 187;
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.label63.Font = new System.Drawing.Font("Calibri", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label63.ForeColor = System.Drawing.Color.Red;
            this.label63.Location = new System.Drawing.Point(3, 4);
            this.label63.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(285, 36);
            this.label63.TabIndex = 341;
            this.label63.Text = "Process Bill Accounting";
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label60.Location = new System.Drawing.Point(390, 9);
            this.label60.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(58, 21);
            this.label60.TabIndex = 225;
            this.label60.Text = "Month";
            // 
            // dtpfnt
            // 
            this.dtpfnt.CustomFormat = "MMM/yyyy";
            this.dtpfnt.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpfnt.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpfnt.Location = new System.Drawing.Point(449, 8);
            this.dtpfnt.Margin = new System.Windows.Forms.Padding(4);
            this.dtpfnt.Name = "dtpfnt";
            this.dtpfnt.Size = new System.Drawing.Size(104, 26);
            this.dtpfnt.TabIndex = 224;
            this.dtpfnt.Value = new System.DateTime(2017, 7, 4, 0, 0, 0, 0);
            this.dtpfnt.ValueChanged += new System.EventHandler(this.dtpfnt_ValueChanged);
            // 
            // txtscr1
            // 
            this.txtscr1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtscr1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtscr1.Location = new System.Drawing.Point(1, 43);
            this.txtscr1.Margin = new System.Windows.Forms.Padding(5);
            this.txtscr1.Name = "txtscr1";
            this.txtscr1.Size = new System.Drawing.Size(983, 26);
            this.txtscr1.TabIndex = 1;
            this.txtscr1.TextChanged += new System.EventHandler(this.txtscr1_TextChanged);
            // 
            // HFGP
            // 
            this.HFGP.BackgroundColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.HFGP.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.HFGP.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnF2;
            this.HFGP.Location = new System.Drawing.Point(1, 74);
            this.HFGP.Margin = new System.Windows.Forms.Padding(4);
            this.HFGP.Name = "HFGP";
            this.HFGP.ReadOnly = true;
            this.HFGP.Size = new System.Drawing.Size(974, 520);
            this.HFGP.TabIndex = 3;
            this.HFGP.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.HFGP_CellContentClick);
            this.HFGP.CellMouseUp += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.HFGP_CellMouseUp);
            this.HFGP.KeyUp += new System.Windows.Forms.KeyEventHandler(this.HFGP_KeyUp);
            // 
            // buttnnxtlft
            // 
            this.buttnnxtlft.BackColor = System.Drawing.Color.White;
            this.buttnnxtlft.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.buttnnxtlft.FlatAppearance.BorderSize = 0;
            this.buttnnxtlft.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttnnxtlft.Image = ((System.Drawing.Image)(resources.GetObject("buttnnxtlft.Image")));
            this.buttnnxtlft.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttnnxtlft.Location = new System.Drawing.Point(714, 323);
            this.buttnnxtlft.Name = "buttnnxtlft";
            this.buttnnxtlft.Size = new System.Drawing.Size(18, 31);
            this.buttnnxtlft.TabIndex = 212;
            this.buttnnxtlft.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttnnxtlft.UseVisualStyleBackColor = false;
            this.buttnnxtlft.Click += new System.EventHandler(this.buttnnxtlft_Click);
            // 
            // txtscr5
            // 
            this.txtscr5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtscr5.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtscr5.Location = new System.Drawing.Point(238, 152);
            this.txtscr5.Margin = new System.Windows.Forms.Padding(5);
            this.txtscr5.Name = "txtscr5";
            this.txtscr5.Size = new System.Drawing.Size(300, 26);
            this.txtscr5.TabIndex = 90;
            this.txtscr5.TextChanged += new System.EventHandler(this.txtscr5_TextChanged);
            // 
            // btnfinnxt
            // 
            this.btnfinnxt.BackColor = System.Drawing.Color.White;
            this.btnfinnxt.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnfinnxt.FlatAppearance.BorderSize = 0;
            this.btnfinnxt.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnfinnxt.Image = ((System.Drawing.Image)(resources.GetObject("btnfinnxt.Image")));
            this.btnfinnxt.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnfinnxt.Location = new System.Drawing.Point(852, 323);
            this.btnfinnxt.Name = "btnfinnxt";
            this.btnfinnxt.Size = new System.Drawing.Size(19, 31);
            this.btnfinnxt.TabIndex = 211;
            this.btnfinnxt.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnfinnxt.UseVisualStyleBackColor = false;
            this.btnfinnxt.Click += new System.EventHandler(this.btnfinnxt_Click);
            // 
            // buttrnxt
            // 
            this.buttrnxt.BackColor = System.Drawing.Color.White;
            this.buttrnxt.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.buttrnxt.FlatAppearance.BorderSize = 0;
            this.buttrnxt.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttrnxt.Image = ((System.Drawing.Image)(resources.GetObject("buttrnxt.Image")));
            this.buttrnxt.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttrnxt.Location = new System.Drawing.Point(818, 323);
            this.buttrnxt.Name = "buttrnxt";
            this.buttrnxt.Size = new System.Drawing.Size(18, 31);
            this.buttrnxt.TabIndex = 210;
            this.buttrnxt.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttrnxt.UseVisualStyleBackColor = false;
            this.buttrnxt.Click += new System.EventHandler(this.buttrnxt_Click);
            // 
            // txtscr8
            // 
            this.txtscr8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtscr8.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtscr8.Location = new System.Drawing.Point(704, 150);
            this.txtscr8.Margin = new System.Windows.Forms.Padding(5);
            this.txtscr8.Name = "txtscr8";
            this.txtscr8.Size = new System.Drawing.Size(100, 26);
            this.txtscr8.TabIndex = 204;
            // 
            // Txtscr3
            // 
            this.Txtscr3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Txtscr3.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txtscr3.Location = new System.Drawing.Point(63, 153);
            this.Txtscr3.Margin = new System.Windows.Forms.Padding(5);
            this.Txtscr3.Name = "Txtscr3";
            this.Txtscr3.Size = new System.Drawing.Size(100, 26);
            this.Txtscr3.TabIndex = 88;
            this.Txtscr3.TextChanged += new System.EventHandler(this.Txtscr3_TextChanged);
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker1.Location = new System.Drawing.Point(127, 101);
            this.dateTimePicker1.Margin = new System.Windows.Forms.Padding(4);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(113, 23);
            this.dateTimePicker1.TabIndex = 334;
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.lblno1);
            this.panel1.Controls.Add(this.lblno2);
            this.panel1.Controls.Add(this.flowLayoutPanel3);
            this.panel1.Controls.Add(this.flowLayoutPanel2);
            this.panel1.Controls.Add(this.flowLayoutPanel1);
            this.panel1.Location = new System.Drawing.Point(738, 323);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(74, 30);
            this.panel1.TabIndex = 214;
            // 
            // lblno1
            // 
            this.lblno1.AutoSize = true;
            this.lblno1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblno1.ForeColor = System.Drawing.Color.Black;
            this.lblno1.Location = new System.Drawing.Point(4, 7);
            this.lblno1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblno1.Name = "lblno1";
            this.lblno1.Size = new System.Drawing.Size(15, 18);
            this.lblno1.TabIndex = 163;
            this.lblno1.Text = "1";
            // 
            // lblno2
            // 
            this.lblno2.AutoSize = true;
            this.lblno2.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblno2.ForeColor = System.Drawing.Color.Black;
            this.lblno2.Location = new System.Drawing.Point(-5, 7);
            this.lblno2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblno2.Name = "lblno2";
            this.lblno2.Size = new System.Drawing.Size(31, 18);
            this.lblno2.TabIndex = 162;
            this.lblno2.Text = "of 1";
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.Location = new System.Drawing.Point(61, 30);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(200, 100);
            this.flowLayoutPanel3.TabIndex = 2;
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.Location = new System.Drawing.Point(38, 35);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(200, 100);
            this.flowLayoutPanel2.TabIndex = 1;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Location = new System.Drawing.Point(62, -129);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(200, 100);
            this.flowLayoutPanel1.TabIndex = 0;
            // 
            // button10
            // 
            this.button10.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button10.Location = new System.Drawing.Point(238, 494);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(164, 27);
            this.button10.TabIndex = 398;
            this.button10.Text = "SERIAL NO MAPPING";
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Click += new System.EventHandler(this.button10_Click_3);
            // 
            // button9
            // 
            this.button9.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button9.Location = new System.Drawing.Point(131, 481);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(156, 27);
            this.button9.TabIndex = 397;
            this.button9.Text = "UOM CONVERSION";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.button9_Click_1);
            // 
            // buttnnvfst
            // 
            this.buttnnvfst.BackColor = System.Drawing.Color.White;
            this.buttnnvfst.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.buttnnvfst.FlatAppearance.BorderSize = 0;
            this.buttnnvfst.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttnnvfst.Image = ((System.Drawing.Image)(resources.GetObject("buttnnvfst.Image")));
            this.buttnnvfst.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttnnvfst.Location = new System.Drawing.Point(680, 323);
            this.buttnnvfst.Name = "buttnnvfst";
            this.buttnnvfst.Size = new System.Drawing.Size(19, 31);
            this.buttnnvfst.TabIndex = 213;
            this.buttnnvfst.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttnnvfst.UseVisualStyleBackColor = false;
            this.buttnnvfst.Click += new System.EventHandler(this.buttnnvfst_Click);
            // 
            // txtscr9
            // 
            this.txtscr9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtscr9.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtscr9.Location = new System.Drawing.Point(806, 151);
            this.txtscr9.Margin = new System.Windows.Forms.Padding(5);
            this.txtscr9.Name = "txtscr9";
            this.txtscr9.Size = new System.Drawing.Size(95, 26);
            this.txtscr9.TabIndex = 205;
            // 
            // txtscr7
            // 
            this.txtscr7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtscr7.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtscr7.Location = new System.Drawing.Point(640, 152);
            this.txtscr7.Margin = new System.Windows.Forms.Padding(5);
            this.txtscr7.Name = "txtscr7";
            this.txtscr7.Size = new System.Drawing.Size(60, 26);
            this.txtscr7.TabIndex = 203;
            // 
            // txtscr6
            // 
            this.txtscr6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtscr6.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtscr6.Location = new System.Drawing.Point(540, 151);
            this.txtscr6.Margin = new System.Windows.Forms.Padding(5);
            this.txtscr6.Name = "txtscr6";
            this.txtscr6.Size = new System.Drawing.Size(98, 26);
            this.txtscr6.TabIndex = 202;
            this.txtscr6.TextChanged += new System.EventHandler(this.txtscr6_TextChanged);
            // 
            // txtscr4
            // 
            this.txtscr4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtscr4.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtscr4.Location = new System.Drawing.Point(166, 152);
            this.txtscr4.Margin = new System.Windows.Forms.Padding(5);
            this.txtscr4.Name = "txtscr4";
            this.txtscr4.Size = new System.Drawing.Size(70, 26);
            this.txtscr4.TabIndex = 100;
            this.txtscr4.TextChanged += new System.EventHandler(this.txtscr4_TextChanged);
            // 
            // Txtscr2
            // 
            this.Txtscr2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Txtscr2.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txtscr2.Location = new System.Drawing.Point(204, 153);
            this.Txtscr2.Margin = new System.Windows.Forms.Padding(5);
            this.Txtscr2.Name = "Txtscr2";
            this.Txtscr2.Size = new System.Drawing.Size(80, 26);
            this.Txtscr2.TabIndex = 87;
            this.Txtscr2.TextChanged += new System.EventHandler(this.Txtscr2_TextChanged);
            // 
            // Taxpan
            // 
            this.Taxpan.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.Taxpan.Controls.Add(this.txttotaddd);
            this.Taxpan.Controls.Add(this.label13);
            this.Taxpan.Controls.Add(this.addipan);
            this.Taxpan.Location = new System.Drawing.Point(357, 436);
            this.Taxpan.Margin = new System.Windows.Forms.Padding(4);
            this.Taxpan.Name = "Taxpan";
            this.Taxpan.Size = new System.Drawing.Size(248, 10);
            this.Taxpan.TabIndex = 201;
            this.Taxpan.Paint += new System.Windows.Forms.PaintEventHandler(this.Taxpan_Paint);
            // 
            // txttotaddd
            // 
            this.txttotaddd.Location = new System.Drawing.Point(621, 467);
            this.txttotaddd.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txttotaddd.Name = "txttotaddd";
            this.txttotaddd.Size = new System.Drawing.Size(161, 22);
            this.txttotaddd.TabIndex = 233;
            this.txttotaddd.TextChanged += new System.EventHandler(this.txttotaddd_TextChanged);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(454, 468);
            this.label13.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(149, 18);
            this.label13.TabIndex = 232;
            this.label13.Text = "Additional Charges";
            // 
            // addipan
            // 
            this.addipan.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.addipan.Controls.Add(this.label56);
            this.addipan.Controls.Add(this.HFGT);
            this.addipan.Location = new System.Drawing.Point(48, 277);
            this.addipan.Name = "addipan";
            this.addipan.Size = new System.Drawing.Size(940, 184);
            this.addipan.TabIndex = 229;
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label56.Location = new System.Drawing.Point(319, 22);
            this.label56.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(145, 21);
            this.label56.TabIndex = 312;
            this.label56.Text = "Terms && Conditions";
            // 
            // HFGT
            // 
            this.HFGT.BackgroundColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.HFGT.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.HFGT.Location = new System.Drawing.Point(25, 53);
            this.HFGT.Name = "HFGT";
            this.HFGT.Size = new System.Drawing.Size(720, 363);
            this.HFGT.TabIndex = 311;
            // 
            // panadd
            // 
            this.panadd.BackColor = System.Drawing.Color.White;
            this.panadd.Controls.Add(this.button21);
            this.panadd.Controls.Add(this.txtchargessum);
            this.panadd.Controls.Add(this.txtbasicval);
            this.panadd.Controls.Add(this.txttax);
            this.panadd.Controls.Add(this.txttotamt);
            this.panadd.Controls.Add(this.label58);
            this.panadd.Controls.Add(this.button13);
            this.panadd.Controls.Add(this.button1);
            this.panadd.Controls.Add(this.buttnext1);
            this.panadd.Controls.Add(this.chkact);
            this.panadd.Controls.Add(this.butedit);
            this.panadd.Controls.Add(this.button6);
            this.panadd.Controls.Add(this.butcan);
            this.panadd.Location = new System.Drawing.Point(0, 596);
            this.panadd.Name = "panadd";
            this.panadd.Size = new System.Drawing.Size(987, 36);
            this.panadd.TabIndex = 235;
            this.panadd.Paint += new System.Windows.Forms.PaintEventHandler(this.panadd_Paint);
            // 
            // button21
            // 
            this.button21.BackColor = System.Drawing.Color.White;
            this.button21.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button21.Image = ((System.Drawing.Image)(resources.GetObject("button21.Image")));
            this.button21.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button21.Location = new System.Drawing.Point(416, 2);
            this.button21.Name = "button21";
            this.button21.Size = new System.Drawing.Size(68, 30);
            this.button21.TabIndex = 241;
            this.button21.Text = "Delete";
            this.button21.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button21.UseVisualStyleBackColor = false;
            this.button21.Click += new System.EventHandler(this.button21_Click);
            // 
            // txtchargessum
            // 
            this.txtchargessum.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtchargessum.Enabled = false;
            this.txtchargessum.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtchargessum.Location = new System.Drawing.Point(691, 5);
            this.txtchargessum.Margin = new System.Windows.Forms.Padding(5);
            this.txtchargessum.Name = "txtchargessum";
            this.txtchargessum.Size = new System.Drawing.Size(100, 26);
            this.txtchargessum.TabIndex = 240;
            this.txtchargessum.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtbasicval
            // 
            this.txtbasicval.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtbasicval.Enabled = false;
            this.txtbasicval.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtbasicval.Location = new System.Drawing.Point(592, 5);
            this.txtbasicval.Margin = new System.Windows.Forms.Padding(5);
            this.txtbasicval.Name = "txtbasicval";
            this.txtbasicval.Size = new System.Drawing.Size(100, 26);
            this.txtbasicval.TabIndex = 238;
            this.txtbasicval.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txttax
            // 
            this.txttax.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txttax.Enabled = false;
            this.txttax.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txttax.Location = new System.Drawing.Point(787, 5);
            this.txttax.Margin = new System.Windows.Forms.Padding(5);
            this.txttax.Name = "txttax";
            this.txttax.Size = new System.Drawing.Size(100, 26);
            this.txttax.TabIndex = 239;
            this.txttax.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txttotamt
            // 
            this.txttotamt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txttotamt.Enabled = false;
            this.txttotamt.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txttotamt.Location = new System.Drawing.Point(889, 5);
            this.txttotamt.Margin = new System.Windows.Forms.Padding(5);
            this.txttotamt.Name = "txttotamt";
            this.txttotamt.Size = new System.Drawing.Size(98, 26);
            this.txttotamt.TabIndex = 237;
            this.txttotamt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label58.Location = new System.Drawing.Point(544, 6);
            this.label58.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(43, 21);
            this.label58.TabIndex = 236;
            this.label58.Text = "Total";
            // 
            // button13
            // 
            this.button13.BackColor = System.Drawing.Color.White;
            this.button13.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button13.Image = ((System.Drawing.Image)(resources.GetObject("button13.Image")));
            this.button13.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button13.Location = new System.Drawing.Point(291, 2);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(122, 30);
            this.button13.TabIndex = 227;
            this.button13.Text = "Bill Statement";
            this.button13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button13.UseVisualStyleBackColor = false;
            this.button13.Click += new System.EventHandler(this.button13_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.White;
            this.button1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Image = ((System.Drawing.Image)(resources.GetObject("button1.Image")));
            this.button1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button1.Location = new System.Drawing.Point(224, 2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(65, 30);
            this.button1.TabIndex = 216;
            this.button1.Text = "Print";
            this.button1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // buttnext1
            // 
            this.buttnext1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttnext1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttnext1.Image = ((System.Drawing.Image)(resources.GetObject("buttnext1.Image")));
            this.buttnext1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttnext1.Location = new System.Drawing.Point(486, 2);
            this.buttnext1.Name = "buttnext1";
            this.buttnext1.Size = new System.Drawing.Size(57, 30);
            this.buttnext1.TabIndex = 208;
            this.buttnext1.Text = "Exit";
            this.buttnext1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttnext1.UseVisualStyleBackColor = false;
            this.buttnext1.Click += new System.EventHandler(this.buttnext1_Click);
            // 
            // chkact
            // 
            this.chkact.AutoSize = true;
            this.chkact.BackColor = System.Drawing.Color.White;
            this.chkact.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkact.Location = new System.Drawing.Point(8, 6);
            this.chkact.Name = "chkact";
            this.chkact.Size = new System.Drawing.Size(65, 22);
            this.chkact.TabIndex = 187;
            this.chkact.Text = "Active";
            this.chkact.UseVisualStyleBackColor = false;
            this.chkact.CheckedChanged += new System.EventHandler(this.chkact_CheckedChanged);
            // 
            // butedit
            // 
            this.butedit.BackColor = System.Drawing.Color.White;
            this.butedit.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.butedit.Image = ((System.Drawing.Image)(resources.GetObject("butedit.Image")));
            this.butedit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.butedit.Location = new System.Drawing.Point(168, 2);
            this.butedit.Name = "butedit";
            this.butedit.Size = new System.Drawing.Size(55, 30);
            this.butedit.TabIndex = 185;
            this.butedit.Text = "Edit";
            this.butedit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.butedit.UseVisualStyleBackColor = false;
            this.butedit.Click += new System.EventHandler(this.butedit_Click);
            // 
            // button6
            // 
            this.button6.BackColor = System.Drawing.Color.White;
            this.button6.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button6.FlatAppearance.BorderSize = 0;
            this.button6.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button6.Image = ((System.Drawing.Image)(resources.GetObject("button6.Image")));
            this.button6.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button6.Location = new System.Drawing.Point(79, 2);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(88, 30);
            this.button6.TabIndex = 184;
            this.button6.Text = "Add New";
            this.button6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button6.UseVisualStyleBackColor = false;
            this.button6.Click += new System.EventHandler(this.button6_Click_1);
            // 
            // butcan
            // 
            this.butcan.BackColor = System.Drawing.Color.White;
            this.butcan.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.butcan.Image = ((System.Drawing.Image)(resources.GetObject("butcan.Image")));
            this.butcan.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.butcan.Location = new System.Drawing.Point(259, -92);
            this.butcan.Name = "butcan";
            this.butcan.Size = new System.Drawing.Size(110, 30);
            this.butcan.TabIndex = 186;
            this.butcan.Text = "Invoice Cancel";
            this.butcan.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.butcan.UseVisualStyleBackColor = false;
            this.butcan.Click += new System.EventHandler(this.butcan_Click);
            // 
            // buttnfinbk
            // 
            this.buttnfinbk.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttnfinbk.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttnfinbk.Image = ((System.Drawing.Image)(resources.GetObject("buttnfinbk.Image")));
            this.buttnfinbk.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttnfinbk.Location = new System.Drawing.Point(919, 560);
            this.buttnfinbk.Margin = new System.Windows.Forms.Padding(4);
            this.buttnfinbk.Name = "buttnfinbk";
            this.buttnfinbk.Size = new System.Drawing.Size(60, 30);
            this.buttnfinbk.TabIndex = 241;
            this.buttnfinbk.Text = "Back";
            this.buttnfinbk.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttnfinbk.UseVisualStyleBackColor = false;
            this.buttnfinbk.Click += new System.EventHandler(this.buttnfinbk_Click);
            // 
            // btnsave
            // 
            this.btnsave.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnsave.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnsave.Image = ((System.Drawing.Image)(resources.GetObject("btnsave.Image")));
            this.btnsave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnsave.Location = new System.Drawing.Point(851, 561);
            this.btnsave.Margin = new System.Windows.Forms.Padding(4);
            this.btnsave.Name = "btnsave";
            this.btnsave.Size = new System.Drawing.Size(60, 30);
            this.btnsave.TabIndex = 221;
            this.btnsave.Text = "Save";
            this.btnsave.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnsave.UseVisualStyleBackColor = false;
            this.btnsave.Click += new System.EventHandler(this.btnsave_Click);
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label59.Location = new System.Drawing.Point(376, 261);
            this.label59.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(44, 21);
            this.label59.TabIndex = 324;
            this.label59.Text = "Price";
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label57.Location = new System.Drawing.Point(13, 261);
            this.label57.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(83, 21);
            this.label57.TabIndex = 325;
            this.label57.Text = "ItemName";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.Location = new System.Drawing.Point(441, 261);
            this.label36.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(35, 21);
            this.label36.TabIndex = 326;
            this.label36.Text = "Qty";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.Location = new System.Drawing.Point(510, 261);
            this.label35.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(83, 21);
            this.label35.TabIndex = 328;
            this.label35.Text = "BasicValue";
            // 
            // button7
            // 
            this.button7.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button7.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button7.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button7.Location = new System.Drawing.Point(611, 278);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(34, 32);
            this.button7.TabIndex = 330;
            this.button7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button7.UseVisualStyleBackColor = false;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // txtprice
            // 
            this.txtprice.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtprice.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtprice.Location = new System.Drawing.Point(366, 284);
            this.txtprice.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtprice.Name = "txtprice";
            this.txtprice.Size = new System.Drawing.Size(68, 26);
            this.txtprice.TabIndex = 333;
            this.txtprice.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtprice.TextChanged += new System.EventHandler(this.txtprice_TextChanged_1);
            this.txtprice.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtprice_KeyDown);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.White;
            this.button2.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button2.Location = new System.Drawing.Point(470, 301);
            this.button2.Margin = new System.Windows.Forms.Padding(4);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(113, 46);
            this.button2.TabIndex = 1;
            this.button2.Text = "Tax";
            this.button2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button2.UseVisualStyleBackColor = false;
            // 
            // txtamt
            // 
            this.txtamt.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtamt.Location = new System.Drawing.Point(101, 310);
            this.txtamt.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtamt.Name = "txtamt";
            this.txtamt.Size = new System.Drawing.Size(161, 22);
            this.txtamt.TabIndex = 226;
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button3.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.Image = ((System.Drawing.Image)(resources.GetObject("button3.Image")));
            this.button3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button3.Location = new System.Drawing.Point(338, 300);
            this.button3.Margin = new System.Windows.Forms.Padding(4);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(124, 47);
            this.button3.TabIndex = 227;
            this.button3.Text = "Back";
            this.button3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button3.UseVisualStyleBackColor = false;
            // 
            // txtot
            // 
            this.txtot.Location = new System.Drawing.Point(232, 248);
            this.txtot.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtot.Name = "txtot";
            this.txtot.Size = new System.Drawing.Size(121, 23);
            this.txtot.TabIndex = 237;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(162, 248);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(46, 18);
            this.label4.TabIndex = 238;
            this.label4.Text = "Total";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(732, 219);
            this.label12.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(69, 21);
            this.label12.TabIndex = 288;
            this.label12.Text = " Charges";
            // 
            // txtcharges
            // 
            this.txtcharges.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtcharges.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtcharges.Location = new System.Drawing.Point(800, 217);
            this.txtcharges.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtcharges.Name = "txtcharges";
            this.txtcharges.Size = new System.Drawing.Size(150, 26);
            this.txtcharges.TabIndex = 289;
            this.txtcharges.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtcharges.TextChanged += new System.EventHandler(this.txtcharges_TextChanged_1);
            // 
            // txttprdval
            // 
            this.txttprdval.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txttprdval.Enabled = false;
            this.txttprdval.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txttprdval.Location = new System.Drawing.Point(429, 272);
            this.txttprdval.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txttprdval.Name = "txttprdval";
            this.txttprdval.Size = new System.Drawing.Size(112, 22);
            this.txttprdval.TabIndex = 301;
            this.txttprdval.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label47.Location = new System.Drawing.Point(328, 274);
            this.label47.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(83, 15);
            this.label47.TabIndex = 302;
            this.label47.Text = "Product Value";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.Location = new System.Drawing.Point(575, 217);
            this.label34.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(60, 15);
            this.label34.TabIndex = 329;
            this.label34.Text = "Addnotes";
            // 
            // dcdate
            // 
            this.dcdate.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dcdate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dcdate.Location = new System.Drawing.Point(6, 127);
            this.dcdate.Margin = new System.Windows.Forms.Padding(4);
            this.dcdate.Name = "dcdate";
            this.dcdate.Size = new System.Drawing.Size(113, 23);
            this.dcdate.TabIndex = 224;
            this.dcdate.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.dcdate_KeyPress);
            // 
            // txtgen1
            // 
            this.txtgen1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtgen1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtgen1.Location = new System.Drawing.Point(5, 76);
            this.txtgen1.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtgen1.Name = "txtgen1";
            this.txtgen1.Size = new System.Drawing.Size(236, 26);
            this.txtgen1.TabIndex = 223;
            this.txtgen1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtgen1_KeyPress);
            // 
            // txttsgval
            // 
            this.txttsgval.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txttsgval.Enabled = false;
            this.txttsgval.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txttsgval.Location = new System.Drawing.Point(351, 256);
            this.txttsgval.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txttsgval.Name = "txttsgval";
            this.txttsgval.Size = new System.Drawing.Size(112, 23);
            this.txttsgval.TabIndex = 310;
            this.txttsgval.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label48.Location = new System.Drawing.Point(472, 247);
            this.label48.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(34, 15);
            this.label48.TabIndex = 303;
            this.label48.Text = "CGST";
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label49.Location = new System.Drawing.Point(585, 258);
            this.label49.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(16, 15);
            this.label49.TabIndex = 304;
            this.label49.Text = "%";
            // 
            // txttcgstp
            // 
            this.txttcgstp.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txttcgstp.Enabled = false;
            this.txttcgstp.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txttcgstp.Location = new System.Drawing.Point(517, 245);
            this.txttcgstp.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txttcgstp.Name = "txttcgstp";
            this.txttcgstp.Size = new System.Drawing.Size(36, 23);
            this.txttcgstp.TabIndex = 305;
            this.txttcgstp.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txttcgval
            // 
            this.txttcgval.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txttcgval.Enabled = false;
            this.txttcgval.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txttcgval.Location = new System.Drawing.Point(461, 263);
            this.txttcgval.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txttcgval.Name = "txttcgval";
            this.txttcgval.Size = new System.Drawing.Size(112, 23);
            this.txttcgval.TabIndex = 306;
            this.txttcgval.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label51.Location = new System.Drawing.Point(472, 281);
            this.label51.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(33, 15);
            this.label51.TabIndex = 307;
            this.label51.Text = "SGST";
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label50.Location = new System.Drawing.Point(585, 292);
            this.label50.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(16, 15);
            this.label50.TabIndex = 308;
            this.label50.Text = "%";
            // 
            // txtsgstp
            // 
            this.txtsgstp.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtsgstp.Enabled = false;
            this.txtsgstp.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtsgstp.Location = new System.Drawing.Point(517, 279);
            this.txtsgstp.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtsgstp.Name = "txtsgstp";
            this.txtsgstp.Size = new System.Drawing.Size(36, 23);
            this.txtsgstp.TabIndex = 309;
            this.txtsgstp.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label52.Location = new System.Drawing.Point(641, 328);
            this.label52.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(16, 15);
            this.label52.TabIndex = 312;
            this.label52.Text = "%";
            // 
            // txtigstp
            // 
            this.txtigstp.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtigstp.Enabled = false;
            this.txtigstp.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtigstp.Location = new System.Drawing.Point(537, 306);
            this.txtigstp.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtigstp.Name = "txtigstp";
            this.txtigstp.Size = new System.Drawing.Size(36, 23);
            this.txtigstp.TabIndex = 313;
            this.txtigstp.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtgen3
            // 
            this.txtgen3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtgen3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtgen3.Location = new System.Drawing.Point(164, 253);
            this.txtgen3.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtgen3.Name = "txtgen3";
            this.txtgen3.Size = new System.Drawing.Size(68, 22);
            this.txtgen3.TabIndex = 336;
            this.txtgen3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtgen2
            // 
            this.txtgen2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtgen2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtgen2.Location = new System.Drawing.Point(93, 288);
            this.txtgen2.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtgen2.Name = "txtgen2";
            this.txtgen2.Size = new System.Drawing.Size(68, 22);
            this.txtgen2.TabIndex = 335;
            this.txtgen2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // button12
            // 
            this.button12.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button12.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button12.Image = ((System.Drawing.Image)(resources.GetObject("button12.Image")));
            this.button12.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button12.Location = new System.Drawing.Point(403, 380);
            this.button12.Margin = new System.Windows.Forms.Padding(4);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(60, 30);
            this.button12.TabIndex = 237;
            this.button12.Text = "Back";
            this.button12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button12.UseVisualStyleBackColor = false;
            this.button12.Click += new System.EventHandler(this.button12_Click_2);
            // 
            // button11
            // 
            this.button11.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button11.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button11.Image = ((System.Drawing.Image)(resources.GetObject("button11.Image")));
            this.button11.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button11.Location = new System.Drawing.Point(502, 362);
            this.button11.Margin = new System.Windows.Forms.Padding(4);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(73, 30);
            this.button11.TabIndex = 236;
            this.button11.Text = "Next";
            this.button11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button11.UseVisualStyleBackColor = false;
            this.button11.Click += new System.EventHandler(this.button11_Click_2);
            // 
            // btnaddrcan
            // 
            this.btnaddrcan.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnaddrcan.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnaddrcan.Image = ((System.Drawing.Image)(resources.GetObject("btnaddrcan.Image")));
            this.btnaddrcan.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnaddrcan.Location = new System.Drawing.Point(335, 376);
            this.btnaddrcan.Margin = new System.Windows.Forms.Padding(4);
            this.btnaddrcan.Name = "btnaddrcan";
            this.btnaddrcan.Size = new System.Drawing.Size(60, 30);
            this.btnaddrcan.TabIndex = 1;
            this.btnaddrcan.Text = "Back";
            this.btnaddrcan.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnaddrcan.UseVisualStyleBackColor = false;
            this.btnaddrcan.Click += new System.EventHandler(this.btnaddrcan_Click_1);
            // 
            // buttnnxt
            // 
            this.buttnnxt.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttnnxt.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttnnxt.Image = ((System.Drawing.Image)(resources.GetObject("buttnnxt.Image")));
            this.buttnnxt.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttnnxt.Location = new System.Drawing.Point(475, 327);
            this.buttnnxt.Margin = new System.Windows.Forms.Padding(4);
            this.buttnnxt.Name = "buttnnxt";
            this.buttnnxt.Size = new System.Drawing.Size(73, 30);
            this.buttnnxt.TabIndex = 239;
            this.buttnnxt.Text = "Next";
            this.buttnnxt.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttnnxt.UseVisualStyleBackColor = false;
            this.buttnnxt.Click += new System.EventHandler(this.buttnnxt_Click);
            // 
            // txttgstp
            // 
            this.txttgstp.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txttgstp.Enabled = false;
            this.txttgstp.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txttgstp.Location = new System.Drawing.Point(543, 330);
            this.txttgstp.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txttgstp.Name = "txttgstp";
            this.txttgstp.Size = new System.Drawing.Size(36, 23);
            this.txttgstp.TabIndex = 318;
            this.txttgstp.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txttgstval
            // 
            this.txttgstval.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txttgstval.Enabled = false;
            this.txttgstval.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txttgstval.Location = new System.Drawing.Point(586, 330);
            this.txttgstval.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txttgstval.Name = "txttgstval";
            this.txttgstval.Size = new System.Drawing.Size(36, 23);
            this.txttgstval.TabIndex = 319;
            this.txttgstval.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtlisid
            // 
            this.txtlisid.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtlisid.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtlisid.Location = new System.Drawing.Point(178, 372);
            this.txtlisid.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtlisid.Name = "txtlisid";
            this.txtlisid.Size = new System.Drawing.Size(106, 23);
            this.txtlisid.TabIndex = 258;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label41.Location = new System.Drawing.Point(432, 357);
            this.label41.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(100, 15);
            this.label41.TabIndex = 259;
            this.label41.Text = "Document Terms";
            // 
            // txtpadd2
            // 
            this.txtpadd2.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtpadd2.Location = new System.Drawing.Point(231, 302);
            this.txtpadd2.MaxLength = 100;
            this.txtpadd2.Name = "txtpadd2";
            this.txtpadd2.Size = new System.Drawing.Size(120, 113);
            this.txtpadd2.TabIndex = 268;
            this.txtpadd2.Text = "";
            // 
            // txtplace
            // 
            this.txtplace.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtplace.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtplace.Location = new System.Drawing.Point(295, 296);
            this.txtplace.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtplace.MaxLength = 100;
            this.txtplace.Name = "txtplace";
            this.txtplace.Size = new System.Drawing.Size(144, 23);
            this.txtplace.TabIndex = 249;
            this.txtplace.TextChanged += new System.EventHandler(this.txtplace_TextChanged);
            this.txtplace.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtplace_KeyDown_1);
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.Location = new System.Drawing.Point(449, 280);
            this.label37.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(88, 15);
            this.label37.TabIndex = 250;
            this.label37.Text = "Place of Supply";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(3, 105);
            this.label11.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(95, 21);
            this.label11.TabIndex = 271;
            this.label11.Text = "Invoice Date";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(4, 51);
            this.label10.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(83, 21);
            this.label10.TabIndex = 273;
            this.label10.Text = "Invoice No";
            // 
            // txtitemcode
            // 
            this.txtitemcode.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtitemcode.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtitemcode.Location = new System.Drawing.Point(267, 349);
            this.txtitemcode.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtitemcode.Name = "txtitemcode";
            this.txtitemcode.Size = new System.Drawing.Size(119, 23);
            this.txtitemcode.TabIndex = 272;
            // 
            // dtpdc
            // 
            this.dtpdc.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpdc.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpdc.Location = new System.Drawing.Point(428, 107);
            this.dtpdc.Margin = new System.Windows.Forms.Padding(4);
            this.dtpdc.Name = "dtpdc";
            this.dtpdc.Size = new System.Drawing.Size(113, 23);
            this.dtpdc.TabIndex = 1;
            // 
            // txtqty
            // 
            this.txtqty.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtqty.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtqty.Location = new System.Drawing.Point(436, 284);
            this.txtqty.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtqty.Name = "txtqty";
            this.txtqty.Size = new System.Drawing.Size(65, 26);
            this.txtqty.TabIndex = 243;
            this.txtqty.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtqty.TextChanged += new System.EventHandler(this.txtqty_TextChanged);
            this.txtqty.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtqty_KeyDown);
            // 
            // txtbval
            // 
            this.txtbval.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtbval.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtbval.Location = new System.Drawing.Point(503, 284);
            this.txtbval.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtbval.Name = "txtbval";
            this.txtbval.Size = new System.Drawing.Size(102, 26);
            this.txtbval.TabIndex = 245;
            this.txtbval.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtnotes
            // 
            this.txtnotes.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtnotes.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtnotes.Location = new System.Drawing.Point(294, 254);
            this.txtnotes.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtnotes.Name = "txtnotes";
            this.txtnotes.Size = new System.Drawing.Size(200, 22);
            this.txtnotes.TabIndex = 260;
            this.txtnotes.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtnotes_KeyDown);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(33, 398);
            this.label14.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(34, 15);
            this.label14.TabIndex = 240;
            this.label14.Text = "Price";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(148, 399);
            this.label31.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(27, 15);
            this.label31.TabIndex = 244;
            this.label31.Text = "Qty";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.Location = new System.Drawing.Point(264, 414);
            this.label32.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(63, 15);
            this.label32.TabIndex = 246;
            this.label32.Text = "BasicValue";
            // 
            // buttcusok
            // 
            this.buttcusok.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttcusok.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttcusok.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.buttcusok.Location = new System.Drawing.Point(403, 284);
            this.buttcusok.Name = "buttcusok";
            this.buttcusok.Size = new System.Drawing.Size(34, 11);
            this.buttcusok.TabIndex = 248;
            this.buttcusok.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttcusok.UseVisualStyleBackColor = false;
            this.buttcusok.Click += new System.EventHandler(this.buttcusok_Click);
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label42.Location = new System.Drawing.Point(331, 321);
            this.label42.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(60, 15);
            this.label42.TabIndex = 261;
            this.label42.Text = "Addnotes";
            // 
            // txtitemname
            // 
            this.txtitemname.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtitemname.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtitemname.Location = new System.Drawing.Point(15, 284);
            this.txtitemname.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtitemname.Name = "txtitemname";
            this.txtitemname.Size = new System.Drawing.Size(350, 26);
            this.txtitemname.TabIndex = 241;
            this.txtitemname.Click += new System.EventHandler(this.txtitemname_Click);
            this.txtitemname.TextChanged += new System.EventHandler(this.txtitemname_TextChanged);
            this.txtitemname.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtitemname_KeyDown);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(20, 316);
            this.label15.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(66, 15);
            this.label15.TabIndex = 242;
            this.label15.Text = "ItemName";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(119, 312);
            this.label33.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(66, 15);
            this.label33.TabIndex = 242;
            this.label33.Text = "ItemName";
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button4.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.Image = ((System.Drawing.Image)(resources.GetObject("button4.Image")));
            this.button4.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button4.Location = new System.Drawing.Point(425, 363);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(91, 38);
            this.button4.TabIndex = 229;
            this.button4.Text = "Back";
            this.button4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button4.UseVisualStyleBackColor = false;
            // 
            // button8
            // 
            this.button8.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button8.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button8.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button8.Location = new System.Drawing.Point(516, 368);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(90, 38);
            this.button8.TabIndex = 236;
            this.button8.Text = "Save";
            this.button8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button8.UseVisualStyleBackColor = false;
            // 
            // txttempadd1
            // 
            this.txttempadd1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txttempadd1.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txttempadd1.Location = new System.Drawing.Point(414, 75);
            this.txttempadd1.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txttempadd1.Name = "txttempadd1";
            this.txttempadd1.Size = new System.Drawing.Size(45, 23);
            this.txttempadd1.TabIndex = 269;
            // 
            // txttempadd2
            // 
            this.txttempadd2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txttempadd2.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txttempadd2.Location = new System.Drawing.Point(467, 75);
            this.txttempadd2.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txttempadd2.Name = "txttempadd2";
            this.txttempadd2.Size = new System.Drawing.Size(45, 23);
            this.txttempadd2.TabIndex = 270;
            // 
            // pantax
            // 
            this.pantax.BackColor = System.Drawing.Color.Silver;
            this.pantax.Controls.Add(this.txttitem);
            this.pantax.Controls.Add(this.label30);
            this.pantax.Controls.Add(this.txttqty);
            this.pantax.Controls.Add(this.label29);
            this.pantax.Controls.Add(this.Txtrate);
            this.pantax.Controls.Add(this.label28);
            this.pantax.Controls.Add(this.label27);
            this.pantax.Controls.Add(this.label26);
            this.pantax.Controls.Add(this.label25);
            this.pantax.Controls.Add(this.txtbasic);
            this.pantax.Controls.Add(this.label23);
            this.pantax.Controls.Add(this.button5);
            this.pantax.Controls.Add(this.txttotal);
            this.pantax.Controls.Add(this.label24);
            this.pantax.Controls.Add(this.txtigcst);
            this.pantax.Controls.Add(this.cboigst);
            this.pantax.Controls.Add(this.label22);
            this.pantax.Controls.Add(this.txtsgst);
            this.pantax.Controls.Add(this.SGST);
            this.pantax.Controls.Add(this.label21);
            this.pantax.Controls.Add(this.txtcgst);
            this.pantax.Controls.Add(this.cbocgst);
            this.pantax.Controls.Add(this.label20);
            this.pantax.Controls.Add(this.label19);
            this.pantax.Controls.Add(this.textBox2);
            this.pantax.Controls.Add(this.label18);
            this.pantax.Controls.Add(this.txtper);
            this.pantax.Controls.Add(this.label9);
            this.pantax.Controls.Add(this.txttaxable);
            this.pantax.Controls.Add(this.txthidqty);
            this.pantax.Location = new System.Drawing.Point(519, 321);
            this.pantax.Name = "pantax";
            this.pantax.Size = new System.Drawing.Size(68, 10);
            this.pantax.TabIndex = 232;
            this.pantax.Visible = false;
            // 
            // txttitem
            // 
            this.txttitem.Enabled = false;
            this.txttitem.Location = new System.Drawing.Point(8, 36);
            this.txttitem.Name = "txttitem";
            this.txttitem.Size = new System.Drawing.Size(315, 51);
            this.txttitem.TabIndex = 247;
            this.txttitem.Text = "";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(5, 16);
            this.label30.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(40, 18);
            this.label30.TabIndex = 246;
            this.label30.Text = "Item";
            // 
            // txttqty
            // 
            this.txttqty.Location = new System.Drawing.Point(216, 102);
            this.txttqty.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txttqty.Name = "txttqty";
            this.txttqty.Size = new System.Drawing.Size(88, 23);
            this.txttqty.TabIndex = 244;
            this.txttqty.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(26, 102);
            this.label29.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(34, 18);
            this.label29.TabIndex = 243;
            this.label29.Text = "Qty";
            // 
            // Txtrate
            // 
            this.Txtrate.Enabled = false;
            this.Txtrate.Location = new System.Drawing.Point(216, 136);
            this.Txtrate.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.Txtrate.Name = "Txtrate";
            this.Txtrate.Size = new System.Drawing.Size(88, 23);
            this.Txtrate.TabIndex = 242;
            this.Txtrate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(26, 136);
            this.label28.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(43, 18);
            this.label28.TabIndex = 241;
            this.label28.Text = "Rate";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(181, 352);
            this.label27.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(22, 18);
            this.label27.TabIndex = 240;
            this.label27.Text = "%";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(181, 314);
            this.label26.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(22, 18);
            this.label26.TabIndex = 239;
            this.label26.Text = "%";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(181, 277);
            this.label25.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(22, 18);
            this.label25.TabIndex = 238;
            this.label25.Text = "%";
            // 
            // txtbasic
            // 
            this.txtbasic.Enabled = false;
            this.txtbasic.Location = new System.Drawing.Point(216, 170);
            this.txtbasic.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtbasic.Name = "txtbasic";
            this.txtbasic.Size = new System.Drawing.Size(88, 23);
            this.txtbasic.TabIndex = 237;
            this.txtbasic.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(26, 170);
            this.label23.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(96, 18);
            this.label23.TabIndex = 236;
            this.label23.Text = "Basic Value";
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button5.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button5.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button5.Location = new System.Drawing.Point(120, 425);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(83, 41);
            this.button5.TabIndex = 235;
            this.button5.Text = "Ok";
            this.button5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button5.UseVisualStyleBackColor = false;
            // 
            // txttotal
            // 
            this.txttotal.Enabled = false;
            this.txttotal.Location = new System.Drawing.Point(216, 386);
            this.txttotal.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txttotal.Name = "txttotal";
            this.txttotal.Size = new System.Drawing.Size(88, 23);
            this.txttotal.TabIndex = 228;
            this.txttotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(25, 388);
            this.label24.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(92, 18);
            this.label24.TabIndex = 227;
            this.label24.Text = "Total Value";
            // 
            // txtigcst
            // 
            this.txtigcst.Enabled = false;
            this.txtigcst.Location = new System.Drawing.Point(216, 352);
            this.txtigcst.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtigcst.Name = "txtigcst";
            this.txtigcst.Size = new System.Drawing.Size(88, 23);
            this.txtigcst.TabIndex = 225;
            this.txtigcst.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // cboigst
            // 
            this.cboigst.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboigst.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboigst.FormattingEnabled = true;
            this.cboigst.Location = new System.Drawing.Point(137, 350);
            this.cboigst.Margin = new System.Windows.Forms.Padding(4);
            this.cboigst.Name = "cboigst";
            this.cboigst.Size = new System.Drawing.Size(39, 23);
            this.cboigst.TabIndex = 224;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(26, 352);
            this.label22.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(46, 18);
            this.label22.TabIndex = 223;
            this.label22.Text = "IGST";
            // 
            // txtsgst
            // 
            this.txtsgst.Enabled = false;
            this.txtsgst.Location = new System.Drawing.Point(216, 313);
            this.txtsgst.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtsgst.Name = "txtsgst";
            this.txtsgst.Size = new System.Drawing.Size(88, 23);
            this.txtsgst.TabIndex = 222;
            this.txtsgst.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // SGST
            // 
            this.SGST.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.SGST.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.SGST.FormattingEnabled = true;
            this.SGST.Location = new System.Drawing.Point(137, 311);
            this.SGST.Margin = new System.Windows.Forms.Padding(4);
            this.SGST.Name = "SGST";
            this.SGST.Size = new System.Drawing.Size(39, 23);
            this.SGST.TabIndex = 221;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(26, 313);
            this.label21.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(53, 18);
            this.label21.TabIndex = 220;
            this.label21.Text = "SGST";
            // 
            // txtcgst
            // 
            this.txtcgst.Enabled = false;
            this.txtcgst.Location = new System.Drawing.Point(216, 276);
            this.txtcgst.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtcgst.Name = "txtcgst";
            this.txtcgst.Size = new System.Drawing.Size(88, 23);
            this.txtcgst.TabIndex = 219;
            this.txtcgst.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // cbocgst
            // 
            this.cbocgst.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbocgst.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbocgst.FormattingEnabled = true;
            this.cbocgst.Location = new System.Drawing.Point(137, 274);
            this.cbocgst.Margin = new System.Windows.Forms.Padding(4);
            this.cbocgst.Name = "cbocgst";
            this.cbocgst.Size = new System.Drawing.Size(39, 23);
            this.cbocgst.TabIndex = 218;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(26, 276);
            this.label20.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(54, 18);
            this.label20.TabIndex = 217;
            this.label20.Text = "CGST";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(25, 241);
            this.label19.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(112, 18);
            this.label19.TabIndex = 216;
            this.label19.Text = "Taxable Value";
            // 
            // textBox2
            // 
            this.textBox2.Enabled = false;
            this.textBox2.Location = new System.Drawing.Point(216, 204);
            this.textBox2.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(88, 23);
            this.textBox2.TabIndex = 215;
            this.textBox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(181, 205);
            this.label18.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(22, 18);
            this.label18.TabIndex = 214;
            this.label18.Text = "%";
            // 
            // txtper
            // 
            this.txtper.Location = new System.Drawing.Point(137, 204);
            this.txtper.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtper.Name = "txtper";
            this.txtper.Size = new System.Drawing.Size(36, 23);
            this.txtper.TabIndex = 205;
            this.txtper.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(25, 205);
            this.label9.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(54, 15);
            this.label9.TabIndex = 204;
            this.label9.Text = "Discount";
            // 
            // txttaxable
            // 
            this.txttaxable.Enabled = false;
            this.txttaxable.Location = new System.Drawing.Point(216, 240);
            this.txttaxable.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txttaxable.Name = "txttaxable";
            this.txttaxable.Size = new System.Drawing.Size(88, 23);
            this.txttaxable.TabIndex = 200;
            this.txttaxable.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txthidqty
            // 
            this.txthidqty.Location = new System.Drawing.Point(216, 101);
            this.txthidqty.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txthidqty.Name = "txthidqty";
            this.txthidqty.Size = new System.Drawing.Size(36, 23);
            this.txthidqty.TabIndex = 245;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.Location = new System.Drawing.Point(400, 391);
            this.label38.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(41, 15);
            this.label38.TabIndex = 252;
            this.label38.Text = "DC No";
            // 
            // Dtppre
            // 
            this.Dtppre.AllowDrop = true;
            this.Dtppre.CustomFormat = "dd-MMM-yyyy  hh:mm tt";
            this.Dtppre.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Dtppre.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.Dtppre.Location = new System.Drawing.Point(111, 298);
            this.Dtppre.Margin = new System.Windows.Forms.Padding(4);
            this.Dtppre.Name = "Dtppre";
            this.Dtppre.Size = new System.Drawing.Size(175, 23);
            this.Dtppre.TabIndex = 253;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.Location = new System.Drawing.Point(314, 112);
            this.label39.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(51, 15);
            this.label39.TabIndex = 254;
            this.label39.Text = "Inv.Date";
            // 
            // Dtprem
            // 
            this.Dtprem.CustomFormat = "dd-MMM-yyyy  hh:mm tt";
            this.Dtprem.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Dtprem.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.Dtprem.Location = new System.Drawing.Point(427, 62);
            this.Dtprem.Margin = new System.Windows.Forms.Padding(4);
            this.Dtprem.Name = "Dtprem";
            this.Dtprem.Size = new System.Drawing.Size(112, 23);
            this.Dtprem.TabIndex = 255;
            // 
            // txtuom
            // 
            this.txtuom.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtuom.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtuom.Location = new System.Drawing.Point(298, 312);
            this.txtuom.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtuom.Name = "txtuom";
            this.txtuom.Size = new System.Drawing.Size(59, 22);
            this.txtuom.TabIndex = 262;
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label44.Location = new System.Drawing.Point(321, 312);
            this.label44.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(33, 15);
            this.label44.TabIndex = 263;
            this.label44.Text = "UoM";
            // 
            // txtpluid
            // 
            this.txtpluid.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtpluid.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtpluid.Location = new System.Drawing.Point(485, 241);
            this.txtpluid.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtpluid.Name = "txtpluid";
            this.txtpluid.Size = new System.Drawing.Size(94, 22);
            this.txtpluid.TabIndex = 251;
            this.txtpluid.TextChanged += new System.EventHandler(this.txtpluid_TextChanged);
            // 
            // txttitemid
            // 
            this.txttitemid.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txttitemid.Location = new System.Drawing.Point(36, 253);
            this.txttitemid.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txttitemid.Name = "txttitemid";
            this.txttitemid.Size = new System.Drawing.Size(35, 22);
            this.txttitemid.TabIndex = 247;
            this.txttitemid.TextChanged += new System.EventHandler(this.txttitemid_TextChanged);
            // 
            // txtrem
            // 
            this.txtrem.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtrem.Location = new System.Drawing.Point(122, 126);
            this.txtrem.MaxLength = 100;
            this.txtrem.Name = "txtrem";
            this.txtrem.Size = new System.Drawing.Size(118, 25);
            this.txtrem.TabIndex = 225;
            this.txtrem.Text = "";
            this.txtrem.TextChanged += new System.EventHandler(this.txtrem_TextChanged);
            this.txtrem.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtrem_KeyPress);
            // 
            // txtdcid
            // 
            this.txtdcid.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtdcid.Location = new System.Drawing.Point(421, 304);
            this.txtdcid.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtdcid.Name = "txtdcid";
            this.txtdcid.Size = new System.Drawing.Size(23, 22);
            this.txtdcid.TabIndex = 228;
            this.txtdcid.TextChanged += new System.EventHandler(this.txtdcid_TextChanged);
            // 
            // txtgrnid
            // 
            this.txtgrnid.Location = new System.Drawing.Point(488, 329);
            this.txtgrnid.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtgrnid.Name = "txtgrnid";
            this.txtgrnid.Size = new System.Drawing.Size(28, 23);
            this.txtgrnid.TabIndex = 220;
            // 
            // txtpuid
            // 
            this.txtpuid.Location = new System.Drawing.Point(377, 329);
            this.txtpuid.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtpuid.Name = "txtpuid";
            this.txtpuid.Size = new System.Drawing.Size(87, 23);
            this.txtpuid.TabIndex = 217;
            this.txtpuid.TextChanged += new System.EventHandler(this.txtpuid_TextChanged);
            // 
            // txtgrn
            // 
            this.txtgrn.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtgrn.Enabled = false;
            this.txtgrn.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtgrn.Location = new System.Drawing.Point(6, 29);
            this.txtgrn.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtgrn.Name = "txtgrn";
            this.txtgrn.Size = new System.Drawing.Size(119, 26);
            this.txtgrn.TabIndex = 198;
            // 
            // Phone
            // 
            this.Phone.AutoSize = true;
            this.Phone.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Phone.Location = new System.Drawing.Point(3, 2);
            this.Phone.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.Phone.Name = "Phone";
            this.Phone.Size = new System.Drawing.Size(60, 21);
            this.Phone.TabIndex = 199;
            this.Phone.Text = "Doc.No";
            // 
            // txtname
            // 
            this.txtname.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtname.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtname.Location = new System.Drawing.Point(249, 28);
            this.txtname.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtname.MaxLength = 100;
            this.txtname.Name = "txtname";
            this.txtname.Size = new System.Drawing.Size(431, 26);
            this.txtname.TabIndex = 222;
            this.txtname.Click += new System.EventHandler(this.txtname_Click);
            this.txtname.TextChanged += new System.EventHandler(this.txtname_TextChanged);
            this.txtname.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtname_KeyDown);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(248, 2);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(92, 21);
            this.label3.TabIndex = 195;
            this.label3.Text = "Party Name";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(61, 346);
            this.label2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(68, 15);
            this.label2.TabIndex = 201;
            this.label2.Text = "Order.Date";
            // 
            // txtdcno
            // 
            this.txtdcno.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtdcno.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtdcno.Location = new System.Drawing.Point(303, 107);
            this.txtdcno.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtdcno.Name = "txtdcno";
            this.txtdcno.Size = new System.Drawing.Size(119, 23);
            this.txtdcno.TabIndex = 1;
            this.txtdcno.TextChanged += new System.EventHandler(this.txtdcno_TextChanged_1);
            this.txtdcno.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtdcno_KeyDown);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(74, 395);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(58, 15);
            this.label5.TabIndex = 203;
            this.label5.Text = "Order.No";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(127, 4);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(72, 21);
            this.label6.TabIndex = 204;
            this.label6.Text = "Doc.Date";
            // 
            // Dtpdt
            // 
            this.Dtpdt.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Dtpdt.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.Dtpdt.Location = new System.Drawing.Point(70, 333);
            this.Dtpdt.Margin = new System.Windows.Forms.Padding(4);
            this.Dtpdt.Name = "Dtpdt";
            this.Dtpdt.Size = new System.Drawing.Size(113, 23);
            this.Dtpdt.TabIndex = 205;
            // 
            // DTPDOCDT
            // 
            this.DTPDOCDT.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DTPDOCDT.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.DTPDOCDT.Location = new System.Drawing.Point(130, 29);
            this.DTPDOCDT.Margin = new System.Windows.Forms.Padding(4);
            this.DTPDOCDT.Name = "DTPDOCDT";
            this.DTPDOCDT.Size = new System.Drawing.Size(113, 26);
            this.DTPDOCDT.TabIndex = 221;
            this.DTPDOCDT.ValueChanged += new System.EventHandler(this.DTPDOCDT_ValueChanged);
            this.DTPDOCDT.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.DTPDOCDT_KeyPress);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(127, 102);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(70, 21);
            this.label1.TabIndex = 233;
            this.label1.Text = "Remarks";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // Txttot
            // 
            this.Txttot.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Txttot.Enabled = false;
            this.Txttot.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txttot.Location = new System.Drawing.Point(878, 490);
            this.Txttot.Margin = new System.Windows.Forms.Padding(5);
            this.Txttot.Name = "Txttot";
            this.Txttot.Size = new System.Drawing.Size(101, 26);
            this.Txttot.TabIndex = 202;
            this.Txttot.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.Txttot.TextChanged += new System.EventHandler(this.Txttot_TextChanged);
            // 
            // txttrans
            // 
            this.txttrans.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txttrans.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txttrans.Location = new System.Drawing.Point(303, 62);
            this.txttrans.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txttrans.Name = "txttrans";
            this.txttrans.Size = new System.Drawing.Size(118, 23);
            this.txttrans.TabIndex = 226;
            this.txttrans.Click += new System.EventHandler(this.txttrans_Click);
            this.txttrans.TextChanged += new System.EventHandler(this.txttrans_TextChanged_1);
            this.txttrans.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txttrans_KeyDown_1);
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.Location = new System.Drawing.Point(314, 63);
            this.label40.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(41, 15);
            this.label40.TabIndex = 257;
            this.label40.Text = "Inv.No";
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label55.Location = new System.Drawing.Point(789, 493);
            this.label55.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(85, 21);
            this.label55.TabIndex = 264;
            this.label55.Text = "Total Value";
            // 
            // txtpadd1
            // 
            this.txtpadd1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtpadd1.Location = new System.Drawing.Point(250, 50);
            this.txtpadd1.MaxLength = 100;
            this.txtpadd1.Name = "txtpadd1";
            this.txtpadd1.Size = new System.Drawing.Size(430, 101);
            this.txtpadd1.TabIndex = 267;
            this.txtpadd1.Text = "";
            this.txtpadd1.TextChanged += new System.EventHandler(this.txtpadd1_TextChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(701, 152);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(103, 21);
            this.label8.TabIndex = 290;
            this.label8.Text = "Taxable Value";
            // 
            // TxtRoff
            // 
            this.TxtRoff.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.TxtRoff.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TxtRoff.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtRoff.Location = new System.Drawing.Point(801, 287);
            this.TxtRoff.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.TxtRoff.Name = "TxtRoff";
            this.TxtRoff.Size = new System.Drawing.Size(149, 26);
            this.TxtRoff.TabIndex = 291;
            this.TxtRoff.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.TxtRoff.TextChanged += new System.EventHandler(this.TxtRoff_TextChanged_1);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(753, 288);
            this.label16.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(44, 21);
            this.label16.TabIndex = 292;
            this.label16.Text = "R.Off";
            // 
            // TxtNetAmt
            // 
            this.TxtNetAmt.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.TxtNetAmt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TxtNetAmt.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtNetAmt.ForeColor = System.Drawing.Color.Red;
            this.TxtNetAmt.Location = new System.Drawing.Point(799, 318);
            this.TxtNetAmt.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.TxtNetAmt.Name = "TxtNetAmt";
            this.TxtNetAmt.Size = new System.Drawing.Size(150, 26);
            this.TxtNetAmt.TabIndex = 293;
            this.TxtNetAmt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(720, 322);
            this.label17.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(77, 21);
            this.label17.TabIndex = 294;
            this.label17.Text = "Net Value";
            // 
            // txttdis
            // 
            this.txttdis.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txttdis.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txttdis.Location = new System.Drawing.Point(801, 60);
            this.txttdis.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txttdis.Name = "txttdis";
            this.txttdis.Size = new System.Drawing.Size(150, 26);
            this.txttdis.TabIndex = 295;
            this.txttdis.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txttdis.TextChanged += new System.EventHandler(this.txttdis_TextChanged_1);
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label43.Location = new System.Drawing.Point(714, 62);
            this.label43.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(87, 21);
            this.label43.TabIndex = 296;
            this.label43.Text = "Discount %";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label45.Location = new System.Drawing.Point(714, 32);
            this.label45.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(87, 21);
            this.label45.TabIndex = 297;
            this.label45.Text = "Basic Value";
            // 
            // txttbval
            // 
            this.txttbval.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.txttbval.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txttbval.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txttbval.Location = new System.Drawing.Point(801, 30);
            this.txttbval.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txttbval.Name = "txttbval";
            this.txttbval.Size = new System.Drawing.Size(149, 26);
            this.txttbval.TabIndex = 298;
            this.txttbval.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txttdisc
            // 
            this.txttdisc.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.txttdisc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txttdisc.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txttdisc.Location = new System.Drawing.Point(801, 89);
            this.txttdisc.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txttdisc.Name = "txttdisc";
            this.txttdisc.Size = new System.Drawing.Size(150, 26);
            this.txttdisc.TabIndex = 299;
            this.txttdisc.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label46.Location = new System.Drawing.Point(688, 91);
            this.label46.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(113, 21);
            this.label46.TabIndex = 300;
            this.label46.Text = "Discount Value";
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label53.Location = new System.Drawing.Point(717, 185);
            this.label53.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(79, 21);
            this.label53.TabIndex = 311;
            this.label53.Text = "GST Value";
            // 
            // txtigval
            // 
            this.txtigval.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.txtigval.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtigval.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtigval.Location = new System.Drawing.Point(801, 183);
            this.txtigval.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtigval.Name = "txtigval";
            this.txtigval.Size = new System.Drawing.Size(150, 26);
            this.txtigval.TabIndex = 314;
            this.txtigval.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtttot
            // 
            this.txtttot.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.txtttot.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtttot.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtttot.Location = new System.Drawing.Point(800, 254);
            this.txtttot.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtttot.Name = "txtttot";
            this.txtttot.Size = new System.Drawing.Size(149, 26);
            this.txtttot.TabIndex = 315;
            this.txtttot.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label54.Location = new System.Drawing.Point(713, 256);
            this.label54.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(85, 21);
            this.label54.TabIndex = 316;
            this.label54.Text = "Total Value";
            // 
            // txtexcise
            // 
            this.txtexcise.BackColor = System.Drawing.SystemColors.HighlightText;
            this.txtexcise.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtexcise.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtexcise.Location = new System.Drawing.Point(801, 151);
            this.txtexcise.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtexcise.Name = "txtexcise";
            this.txtexcise.Size = new System.Drawing.Size(150, 26);
            this.txtexcise.TabIndex = 317;
            this.txtexcise.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // HFGST
            // 
            this.HFGST.BackgroundColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.HFGST.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.HFGST.Location = new System.Drawing.Point(718, 355);
            this.HFGST.Margin = new System.Windows.Forms.Padding(4);
            this.HFGST.Name = "HFGST";
            this.HFGST.Size = new System.Drawing.Size(261, 134);
            this.HFGST.TabIndex = 337;
            // 
            // serialno
            // 
            this.serialno.Controls.Add(this.Dataserial);
            this.serialno.Controls.Add(this.label78);
            this.serialno.Controls.Add(this.button20);
            this.serialno.Controls.Add(this.label76);
            this.serialno.Controls.Add(this.textBox5);
            this.serialno.Controls.Add(this.textBox6);
            this.serialno.Controls.Add(this.label79);
            this.serialno.Controls.Add(this.label80);
            this.serialno.Controls.Add(this.txtsrail);
            this.serialno.Controls.Add(this.cboserial);
            this.serialno.Controls.Add(this.label84);
            this.serialno.Controls.Add(this.txtserialqty);
            this.serialno.Controls.Add(this.label85);
            this.serialno.Controls.Add(this.button23);
            this.serialno.Controls.Add(this.label88);
            this.serialno.Controls.Add(this.textBox14);
            this.serialno.Controls.Add(this.textBox15);
            this.serialno.Controls.Add(this.textBox16);
            this.serialno.Location = new System.Drawing.Point(1, 491);
            this.serialno.Name = "serialno";
            this.serialno.Size = new System.Drawing.Size(52, 41);
            this.serialno.TabIndex = 400;
            this.serialno.Paint += new System.Windows.Forms.PaintEventHandler(this.serialno_Paint);
            // 
            // Dataserial
            // 
            this.Dataserial.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.Dataserial.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Dataserial.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.Dataserial.Location = new System.Drawing.Point(291, 163);
            this.Dataserial.Margin = new System.Windows.Forms.Padding(4);
            this.Dataserial.Name = "Dataserial";
            this.Dataserial.ReadOnly = true;
            this.Dataserial.Size = new System.Drawing.Size(330, 300);
            this.Dataserial.TabIndex = 446;
            this.Dataserial.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.Dataserial_CellClick_1);
            this.Dataserial.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.Dataserial_CellContentClick_2);
            this.Dataserial.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.Dataserial_CellValueChanged_1);
            this.Dataserial.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Dataserial_KeyDown_1);
            // 
            // label78
            // 
            this.label78.AutoSize = true;
            this.label78.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label78.ForeColor = System.Drawing.Color.Red;
            this.label78.Location = new System.Drawing.Point(412, 2);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(177, 23);
            this.label78.TabIndex = 445;
            this.label78.Text = "SERIAL NO MAPPING";
            // 
            // button20
            // 
            this.button20.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button20.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button20.Image = ((System.Drawing.Image)(resources.GetObject("button20.Image")));
            this.button20.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button20.Location = new System.Drawing.Point(701, 279);
            this.button20.Margin = new System.Windows.Forms.Padding(4);
            this.button20.Name = "button20";
            this.button20.Size = new System.Drawing.Size(60, 30);
            this.button20.TabIndex = 444;
            this.button20.Text = "Back";
            this.button20.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button20.UseVisualStyleBackColor = false;
            this.button20.Click += new System.EventHandler(this.button20_Click);
            // 
            // label76
            // 
            this.label76.AutoSize = true;
            this.label76.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label76.Location = new System.Drawing.Point(764, 36);
            this.label76.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(30, 18);
            this.label76.TabIndex = 439;
            this.label76.Text = "Qty";
            // 
            // textBox5
            // 
            this.textBox5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox5.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox5.Location = new System.Drawing.Point(761, 57);
            this.textBox5.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(69, 26);
            this.textBox5.TabIndex = 438;
            this.textBox5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBox6
            // 
            this.textBox6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox6.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox6.Location = new System.Drawing.Point(685, 57);
            this.textBox6.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(69, 26);
            this.textBox6.TabIndex = 430;
            this.textBox6.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label79
            // 
            this.label79.AutoSize = true;
            this.label79.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label79.Location = new System.Drawing.Point(687, 39);
            this.label79.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(41, 18);
            this.label79.TabIndex = 429;
            this.label79.Text = "UOM";
            // 
            // label80
            // 
            this.label80.AutoSize = true;
            this.label80.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label80.Location = new System.Drawing.Point(296, 106);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(64, 18);
            this.label80.TabIndex = 427;
            this.label80.Text = "Serial No";
            // 
            // txtsrail
            // 
            this.txtsrail.Location = new System.Drawing.Point(291, 128);
            this.txtsrail.Name = "txtsrail";
            this.txtsrail.Size = new System.Drawing.Size(330, 23);
            this.txtsrail.TabIndex = 1;
            this.txtsrail.TextChanged += new System.EventHandler(this.txtsrail_TextChanged);
            this.txtsrail.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtsrail_KeyDown);
            // 
            // cboserial
            // 
            this.cboserial.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboserial.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboserial.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboserial.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboserial.FormattingEnabled = true;
            this.cboserial.Location = new System.Drawing.Point(291, 60);
            this.cboserial.Name = "cboserial";
            this.cboserial.Size = new System.Drawing.Size(388, 26);
            this.cboserial.TabIndex = 416;
            this.cboserial.SelectedIndexChanged += new System.EventHandler(this.cboserial_SelectedIndexChanged);
            // 
            // label84
            // 
            this.label84.AutoSize = true;
            this.label84.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label84.Location = new System.Drawing.Point(456, 171);
            this.label84.Name = "label84";
            this.label84.Size = new System.Drawing.Size(30, 18);
            this.label84.TabIndex = 402;
            this.label84.Text = "Qty";
            // 
            // txtserialqty
            // 
            this.txtserialqty.Location = new System.Drawing.Point(457, 191);
            this.txtserialqty.Name = "txtserialqty";
            this.txtserialqty.Size = new System.Drawing.Size(149, 23);
            this.txtserialqty.TabIndex = 403;
            this.txtserialqty.TextChanged += new System.EventHandler(this.textBox11_TextChanged);
            // 
            // label85
            // 
            this.label85.AutoSize = true;
            this.label85.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label85.Location = new System.Drawing.Point(292, 36);
            this.label85.Name = "label85";
            this.label85.Size = new System.Drawing.Size(37, 18);
            this.label85.TabIndex = 400;
            this.label85.Text = "Item";
            // 
            // button23
            // 
            this.button23.Location = new System.Drawing.Point(638, 122);
            this.button23.Name = "button23";
            this.button23.Size = new System.Drawing.Size(40, 28);
            this.button23.TabIndex = 2;
            this.button23.Text = "OK";
            this.button23.UseVisualStyleBackColor = true;
            this.button23.Click += new System.EventHandler(this.button23_Click);
            // 
            // label88
            // 
            this.label88.AutoSize = true;
            this.label88.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label88.Location = new System.Drawing.Point(237, -14);
            this.label88.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label88.Name = "label88";
            this.label88.Size = new System.Drawing.Size(27, 15);
            this.label88.TabIndex = 359;
            this.label88.Text = "Qty";
            // 
            // textBox14
            // 
            this.textBox14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox14.Enabled = false;
            this.textBox14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox14.Location = new System.Drawing.Point(360, 222);
            this.textBox14.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.textBox14.Name = "textBox14";
            this.textBox14.Size = new System.Drawing.Size(91, 22);
            this.textBox14.TabIndex = 435;
            this.textBox14.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.textBox14.TextChanged += new System.EventHandler(this.textBox14_TextChanged);
            // 
            // textBox15
            // 
            this.textBox15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox15.Enabled = false;
            this.textBox15.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox15.Location = new System.Drawing.Point(356, 253);
            this.textBox15.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.textBox15.Name = "textBox15";
            this.textBox15.Size = new System.Drawing.Size(90, 22);
            this.textBox15.TabIndex = 431;
            this.textBox15.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.textBox15.TextChanged += new System.EventHandler(this.textBox15_TextChanged);
            // 
            // textBox16
            // 
            this.textBox16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox16.Enabled = false;
            this.textBox16.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox16.Location = new System.Drawing.Point(360, 196);
            this.textBox16.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.textBox16.Name = "textBox16";
            this.textBox16.Size = new System.Drawing.Size(89, 22);
            this.textBox16.TabIndex = 404;
            this.textBox16.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.textBox16.TextChanged += new System.EventHandler(this.textBox16_TextChanged);
            // 
            // mappnl
            // 
            this.mappnl.Controls.Add(this.label77);
            this.mappnl.Controls.Add(this.button19);
            this.mappnl.Controls.Add(this.txtqty1);
            this.mappnl.Controls.Add(this.txtbags);
            this.mappnl.Controls.Add(this.label71);
            this.mappnl.Controls.Add(this.cbosGReturnItem);
            this.mappnl.Controls.Add(this.DataGridWeight);
            this.mappnl.Controls.Add(this.label65);
            this.mappnl.Controls.Add(this.txtWeight);
            this.mappnl.Controls.Add(this.label66);
            this.mappnl.Controls.Add(this.txtBagNo);
            this.mappnl.Controls.Add(this.label67);
            this.mappnl.Controls.Add(this.btnOk);
            this.mappnl.Controls.Add(this.label68);
            this.mappnl.Controls.Add(this.txtGrossWght);
            this.mappnl.Controls.Add(this.label69);
            this.mappnl.Controls.Add(this.txtTarWght);
            this.mappnl.Controls.Add(this.label75);
            this.mappnl.Controls.Add(this.txtlotno2);
            this.mappnl.Controls.Add(this.txtstart1);
            this.mappnl.Controls.Add(this.txttarewt1);
            this.mappnl.Controls.Add(this.button16);
            this.mappnl.Controls.Add(this.txtlotno);
            this.mappnl.Controls.Add(this.txttarewt);
            this.mappnl.Controls.Add(this.txtstart);
            this.mappnl.Controls.Add(this.button14);
            this.mappnl.Controls.Add(this.button15);
            this.mappnl.Controls.Add(this.label74);
            this.mappnl.Controls.Add(this.label73);
            this.mappnl.Controls.Add(this.label72);
            this.mappnl.Controls.Add(this.button17);
            this.mappnl.Controls.Add(this.label7);
            this.mappnl.Controls.Add(this.txtNoogBags);
            this.mappnl.Controls.Add(this.label64);
            this.mappnl.Controls.Add(this.txtgrossgen);
            this.mappnl.Controls.Add(this.label70);
            this.mappnl.Controls.Add(this.txtlotno1);
            this.mappnl.Location = new System.Drawing.Point(0, 494);
            this.mappnl.Name = "mappnl";
            this.mappnl.Size = new System.Drawing.Size(86, 31);
            this.mappnl.TabIndex = 399;
            this.mappnl.Paint += new System.Windows.Forms.PaintEventHandler(this.mappnl_Paint);
            // 
            // label77
            // 
            this.label77.AutoSize = true;
            this.label77.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label77.ForeColor = System.Drawing.Color.Red;
            this.label77.Location = new System.Drawing.Point(459, 56);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(163, 23);
            this.label77.TabIndex = 444;
            this.label77.Text = "UOM CONVERSION";
            // 
            // button19
            // 
            this.button19.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button19.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button19.Image = ((System.Drawing.Image)(resources.GetObject("button19.Image")));
            this.button19.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button19.Location = new System.Drawing.Point(878, 233);
            this.button19.Margin = new System.Windows.Forms.Padding(4);
            this.button19.Name = "button19";
            this.button19.Size = new System.Drawing.Size(60, 30);
            this.button19.TabIndex = 443;
            this.button19.Text = "Back";
            this.button19.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button19.UseVisualStyleBackColor = false;
            this.button19.Click += new System.EventHandler(this.button19_Click);
            // 
            // txtqty1
            // 
            this.txtqty1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtqty1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtqty1.Location = new System.Drawing.Point(659, 40);
            this.txtqty1.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtqty1.Name = "txtqty1";
            this.txtqty1.Size = new System.Drawing.Size(69, 26);
            this.txtqty1.TabIndex = 438;
            this.txtqty1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtbags
            // 
            this.txtbags.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtbags.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtbags.Location = new System.Drawing.Point(580, 40);
            this.txtbags.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtbags.Name = "txtbags";
            this.txtbags.Size = new System.Drawing.Size(69, 26);
            this.txtbags.TabIndex = 430;
            this.txtbags.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label71.Location = new System.Drawing.Point(581, 21);
            this.label71.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(41, 18);
            this.label71.TabIndex = 429;
            this.label71.Text = "UOM";
            // 
            // cbosGReturnItem
            // 
            this.cbosGReturnItem.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbosGReturnItem.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbosGReturnItem.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbosGReturnItem.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbosGReturnItem.FormattingEnabled = true;
            this.cbosGReturnItem.Location = new System.Drawing.Point(93, 41);
            this.cbosGReturnItem.Name = "cbosGReturnItem";
            this.cbosGReturnItem.Size = new System.Drawing.Size(478, 26);
            this.cbosGReturnItem.TabIndex = 416;
            this.cbosGReturnItem.SelectedIndexChanged += new System.EventHandler(this.cbosGReturnItem_SelectedIndexChanged);
            // 
            // DataGridWeight
            // 
            this.DataGridWeight.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.ActiveCaption;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DataGridWeight.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.DataGridWeight.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridWeight.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.DataGridWeight.EnableHeadersVisualStyles = false;
            this.DataGridWeight.Location = new System.Drawing.Point(93, 126);
            this.DataGridWeight.Name = "DataGridWeight";
            this.DataGridWeight.RowHeadersVisible = false;
            this.DataGridWeight.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridWeight.Size = new System.Drawing.Size(778, 335);
            this.DataGridWeight.TabIndex = 409;
            this.DataGridWeight.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridWeight_CellClick);
            this.DataGridWeight.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridWeight_CellContentClick);
            this.DataGridWeight.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridWeight_CellValueChanged);
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Location = new System.Drawing.Point(92, 77);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(64, 15);
            this.label65.TabIndex = 405;
            this.label65.Text = "Itemname";
            // 
            // txtWeight
            // 
            this.txtWeight.Location = new System.Drawing.Point(684, 98);
            this.txtWeight.Name = "txtWeight";
            this.txtWeight.Size = new System.Drawing.Size(93, 23);
            this.txtWeight.TabIndex = 407;
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Location = new System.Drawing.Point(363, 237);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(45, 15);
            this.label66.TabIndex = 402;
            this.label66.Text = "Bag No";
            // 
            // txtBagNo
            // 
            this.txtBagNo.Location = new System.Drawing.Point(90, 98);
            this.txtBagNo.Name = "txtBagNo";
            this.txtBagNo.Size = new System.Drawing.Size(409, 23);
            this.txtBagNo.TabIndex = 403;
            this.txtBagNo.Click += new System.EventHandler(this.txtBagNo_Click);
            this.txtBagNo.TextChanged += new System.EventHandler(this.txtBagNo_TextChanged);
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label67.Location = new System.Drawing.Point(97, 24);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(37, 18);
            this.label67.TabIndex = 400;
            this.label67.Text = "Item";
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(904, 96);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(40, 28);
            this.btnOk.TabIndex = 408;
            this.btnOk.Text = "OK";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Location = new System.Drawing.Point(138, 158);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(37, 15);
            this.label68.TabIndex = 411;
            this.label68.Text = "Gross";
            // 
            // txtGrossWght
            // 
            this.txtGrossWght.Location = new System.Drawing.Point(783, 98);
            this.txtGrossWght.Name = "txtGrossWght";
            this.txtGrossWght.Size = new System.Drawing.Size(88, 23);
            this.txtGrossWght.TabIndex = 406;
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Location = new System.Drawing.Point(440, 217);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(73, 15);
            this.label69.TabIndex = 410;
            this.label69.Text = "Tare Weight";
            // 
            // txtTarWght
            // 
            this.txtTarWght.Location = new System.Drawing.Point(432, 253);
            this.txtTarWght.Name = "txtTarWght";
            this.txtTarWght.Size = new System.Drawing.Size(86, 23);
            this.txtTarWght.TabIndex = 404;
            // 
            // label75
            // 
            this.label75.AutoSize = true;
            this.label75.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label75.Location = new System.Drawing.Point(672, 21);
            this.label75.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(30, 18);
            this.label75.TabIndex = 359;
            this.label75.Text = "Qty";
            // 
            // txtlotno2
            // 
            this.txtlotno2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtlotno2.Enabled = false;
            this.txtlotno2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtlotno2.Location = new System.Drawing.Point(98, 178);
            this.txtlotno2.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtlotno2.Name = "txtlotno2";
            this.txtlotno2.Size = new System.Drawing.Size(91, 22);
            this.txtlotno2.TabIndex = 435;
            this.txtlotno2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtstart1
            // 
            this.txtstart1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtstart1.Enabled = false;
            this.txtstart1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtstart1.Location = new System.Drawing.Point(94, 209);
            this.txtstart1.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtstart1.Name = "txtstart1";
            this.txtstart1.Size = new System.Drawing.Size(90, 22);
            this.txtstart1.TabIndex = 431;
            this.txtstart1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txttarewt1
            // 
            this.txttarewt1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txttarewt1.Enabled = false;
            this.txttarewt1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txttarewt1.Location = new System.Drawing.Point(329, 240);
            this.txttarewt1.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txttarewt1.Name = "txttarewt1";
            this.txttarewt1.Size = new System.Drawing.Size(407, 22);
            this.txttarewt1.TabIndex = 404;
            this.txttarewt1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txttarewt1.Click += new System.EventHandler(this.txttarewt1_Click);
            // 
            // button16
            // 
            this.button16.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button16.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button16.Image = ((System.Drawing.Image)(resources.GetObject("button16.Image")));
            this.button16.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button16.Location = new System.Drawing.Point(319, 336);
            this.button16.Margin = new System.Windows.Forms.Padding(4);
            this.button16.Name = "button16";
            this.button16.Size = new System.Drawing.Size(60, 30);
            this.button16.TabIndex = 442;
            this.button16.Text = "Save";
            this.button16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button16.UseVisualStyleBackColor = false;
            // 
            // txtlotno
            // 
            this.txtlotno.Location = new System.Drawing.Point(106, 218);
            this.txtlotno.Name = "txtlotno";
            this.txtlotno.Size = new System.Drawing.Size(88, 23);
            this.txtlotno.TabIndex = 441;
            // 
            // txttarewt
            // 
            this.txttarewt.Location = new System.Drawing.Point(106, 180);
            this.txttarewt.Name = "txttarewt";
            this.txttarewt.Size = new System.Drawing.Size(88, 23);
            this.txttarewt.TabIndex = 440;
            // 
            // txtstart
            // 
            this.txtstart.Location = new System.Drawing.Point(503, 98);
            this.txtstart.Name = "txtstart";
            this.txtstart.Size = new System.Drawing.Size(88, 23);
            this.txtstart.TabIndex = 439;
            // 
            // button14
            // 
            this.button14.Location = new System.Drawing.Point(117, 262);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(85, 28);
            this.button14.TabIndex = 437;
            this.button14.Text = "GENERATE ";
            this.button14.UseVisualStyleBackColor = true;
            // 
            // button15
            // 
            this.button15.Location = new System.Drawing.Point(298, 264);
            this.button15.Name = "button15";
            this.button15.Size = new System.Drawing.Size(81, 28);
            this.button15.TabIndex = 436;
            this.button15.Text = "RESET";
            this.button15.UseVisualStyleBackColor = true;
            // 
            // label74
            // 
            this.label74.AutoSize = true;
            this.label74.Location = new System.Drawing.Point(315, 220);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(44, 15);
            this.label74.TabIndex = 434;
            this.label74.Text = "Lot No";
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.label73.Location = new System.Drawing.Point(686, 79);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(41, 15);
            this.label73.TabIndex = 433;
            this.label73.Text = "Width";
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.Location = new System.Drawing.Point(591, 82);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(44, 15);
            this.label72.TabIndex = 432;
            this.label72.Text = "POQTy";
            // 
            // button17
            // 
            this.button17.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button17.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button17.Image = ((System.Drawing.Image)(resources.GetObject("button17.Image")));
            this.button17.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button17.Location = new System.Drawing.Point(118, 334);
            this.button17.Name = "button17";
            this.button17.Size = new System.Drawing.Size(57, 30);
            this.button17.TabIndex = 426;
            this.button17.Text = "Exit";
            this.button17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button17.UseVisualStyleBackColor = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(780, 79);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(45, 15);
            this.label7.TabIndex = 415;
            this.label7.Text = "Length";
            // 
            // txtNoogBags
            // 
            this.txtNoogBags.Location = new System.Drawing.Point(594, 98);
            this.txtNoogBags.Name = "txtNoogBags";
            this.txtNoogBags.Size = new System.Drawing.Size(88, 23);
            this.txtNoogBags.TabIndex = 414;
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Location = new System.Drawing.Point(503, 79);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(27, 15);
            this.label64.TabIndex = 413;
            this.label64.Text = "Qty";
            // 
            // txtgrossgen
            // 
            this.txtgrossgen.Location = new System.Drawing.Point(386, 179);
            this.txtgrossgen.Name = "txtgrossgen";
            this.txtgrossgen.Size = new System.Drawing.Size(88, 23);
            this.txtgrossgen.TabIndex = 412;
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Location = new System.Drawing.Point(281, 237);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(44, 15);
            this.label70.TabIndex = 427;
            this.label70.Text = "Lot No";
            // 
            // txtlotno1
            // 
            this.txtlotno1.Location = new System.Drawing.Point(269, 253);
            this.txtlotno1.Name = "txtlotno1";
            this.txtlotno1.Size = new System.Drawing.Size(88, 23);
            this.txtlotno1.TabIndex = 428;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Location = new System.Drawing.Point(3, 154);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(703, 438);
            this.tabControl1.TabIndex = 401;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.button24);
            this.tabPage1.Controls.Add(this.button22);
            this.tabPage1.Controls.Add(this.textBox1);
            this.tabPage1.Controls.Add(this.label81);
            this.tabPage1.Controls.Add(this.HFGPT);
            this.tabPage1.Controls.Add(this.txttaxtot);
            this.tabPage1.Controls.Add(this.label61);
            this.tabPage1.Controls.Add(this.txttype);
            this.tabPage1.Controls.Add(this.label83);
            this.tabPage1.Controls.Add(this.cbopono);
            this.tabPage1.Controls.Add(this.txtgrnno);
            this.tabPage1.Controls.Add(this.label82);
            this.tabPage1.Location = new System.Drawing.Point(4, 24);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(695, 410);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Pending Grn List";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // button24
            // 
            this.button24.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button24.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button24.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button24.Location = new System.Drawing.Point(499, 374);
            this.button24.Name = "button24";
            this.button24.Size = new System.Drawing.Size(190, 28);
            this.button24.TabIndex = 424;
            this.button24.Text = "Summarized";
            this.button24.UseVisualStyleBackColor = false;
            this.button24.Click += new System.EventHandler(this.button24_Click);
            // 
            // button22
            // 
            this.button22.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button22.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button22.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button22.Location = new System.Drawing.Point(233, 7);
            this.button22.Name = "button22";
            this.button22.Size = new System.Drawing.Size(36, 28);
            this.button22.TabIndex = 423;
            this.button22.Text = "OK";
            this.button22.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button22.UseVisualStyleBackColor = false;
            this.button22.Click += new System.EventHandler(this.button22_Click);
            // 
            // textBox1
            // 
            this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.Location = new System.Drawing.Point(79, 6);
            this.textBox1.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(146, 26);
            this.textBox1.TabIndex = 421;
            this.textBox1.Click += new System.EventHandler(this.textBox1_Click);
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged_1);
            this.textBox1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBox1_KeyDown);
            // 
            // label81
            // 
            this.label81.AutoSize = true;
            this.label81.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label81.Location = new System.Drawing.Point(12, 6);
            this.label81.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label81.Name = "label81";
            this.label81.Size = new System.Drawing.Size(51, 21);
            this.label81.TabIndex = 418;
            this.label81.Text = "Dc No";
            // 
            // HFGPT
            // 
            this.HFGPT.BackgroundColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.HFGPT.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.HFGPT.Location = new System.Drawing.Point(5, 39);
            this.HFGPT.Margin = new System.Windows.Forms.Padding(4);
            this.HFGPT.Name = "HFGPT";
            this.HFGPT.Size = new System.Drawing.Size(686, 329);
            this.HFGPT.TabIndex = 215;
            // 
            // txttaxtot
            // 
            this.txttaxtot.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txttaxtot.Enabled = false;
            this.txttaxtot.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txttaxtot.Location = new System.Drawing.Point(232, 285);
            this.txttaxtot.Margin = new System.Windows.Forms.Padding(5);
            this.txttaxtot.Name = "txttaxtot";
            this.txttaxtot.Size = new System.Drawing.Size(101, 26);
            this.txttaxtot.TabIndex = 338;
            this.txttaxtot.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label61.Location = new System.Drawing.Point(248, 146);
            this.label61.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(85, 21);
            this.label61.TabIndex = 339;
            this.label61.Text = "Total Value";
            // 
            // txttype
            // 
            this.txttype.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.txttype.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txttype.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txttype.ForeColor = System.Drawing.Color.Red;
            this.txttype.Location = new System.Drawing.Point(361, 305);
            this.txttype.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txttype.Name = "txttype";
            this.txttype.Size = new System.Drawing.Size(150, 26);
            this.txttype.TabIndex = 425;
            this.txttype.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label83
            // 
            this.label83.AutoSize = true;
            this.label83.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label83.Location = new System.Drawing.Point(441, 138);
            this.label83.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label83.Name = "label83";
            this.label83.Size = new System.Drawing.Size(85, 21);
            this.label83.TabIndex = 426;
            this.label83.Text = "Total Value";
            // 
            // cbopono
            // 
            this.cbopono.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbopono.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbopono.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbopono.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbopono.FormattingEnabled = true;
            this.cbopono.Location = new System.Drawing.Point(542, 68);
            this.cbopono.Name = "cbopono";
            this.cbopono.Size = new System.Drawing.Size(121, 26);
            this.cbopono.TabIndex = 419;
            this.cbopono.SelectedIndexChanged += new System.EventHandler(this.cbopono_SelectedIndexChanged);
            // 
            // txtgrnno
            // 
            this.txtgrnno.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtgrnno.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtgrnno.Location = new System.Drawing.Point(252, 149);
            this.txtgrnno.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtgrnno.Name = "txtgrnno";
            this.txtgrnno.Size = new System.Drawing.Size(139, 26);
            this.txtgrnno.TabIndex = 422;
            // 
            // label82
            // 
            this.label82.AutoSize = true;
            this.label82.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label82.Location = new System.Drawing.Point(199, 149);
            this.label82.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label82.Name = "label82";
            this.label82.Size = new System.Drawing.Size(56, 21);
            this.label82.TabIndex = 420;
            this.label82.Text = "GrnNo";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.HFIT);
            this.tabPage2.Location = new System.Drawing.Point(4, 24);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(695, 410);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Bill Account Details";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // HFIT
            // 
            this.HFIT.BackgroundColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.HFIT.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.HFIT.Location = new System.Drawing.Point(5, 12);
            this.HFIT.Margin = new System.Windows.Forms.Padding(4);
            this.HFIT.Name = "HFIT";
            this.HFIT.Size = new System.Drawing.Size(687, 395);
            this.HFIT.TabIndex = 214;
            this.HFIT.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.HFIT_CellClick);
            this.HFIT.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.HFIT_CellContentClick);
            this.HFIT.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.HFIT_CellValueChanged);
            this.HFIT.KeyDown += new System.Windows.Forms.KeyEventHandler(this.HFIT_KeyDown_1);
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.ItemDet);
            this.tabPage3.Location = new System.Drawing.Point(4, 24);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(695, 410);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Item Details";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // ItemDet
            // 
            this.ItemDet.BackgroundColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.ItemDet.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ItemDet.Location = new System.Drawing.Point(4, 8);
            this.ItemDet.Margin = new System.Windows.Forms.Padding(4);
            this.ItemDet.Name = "ItemDet";
            this.ItemDet.Size = new System.Drawing.Size(687, 395);
            this.ItemDet.TabIndex = 215;
            this.ItemDet.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.ItemDet_CellContentClick);
            this.ItemDet.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.ItemDet_CellValueChanged);
            // 
            // grSearch
            // 
            this.grSearch.BackColor = System.Drawing.Color.White;
            this.grSearch.Controls.Add(this.button18);
            this.grSearch.Controls.Add(this.btnHide);
            this.grSearch.Controls.Add(this.DataGridCommon);
            this.grSearch.Location = new System.Drawing.Point(249, 52);
            this.grSearch.Name = "grSearch";
            this.grSearch.Size = new System.Drawing.Size(473, 304);
            this.grSearch.TabIndex = 396;
            this.grSearch.Visible = false;
            // 
            // button18
            // 
            this.button18.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button18.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button18.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button18.Location = new System.Drawing.Point(239, 269);
            this.button18.Name = "button18";
            this.button18.Size = new System.Drawing.Size(100, 28);
            this.button18.TabIndex = 394;
            this.button18.Text = "Select (F2)";
            this.button18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button18.UseVisualStyleBackColor = false;
            this.button18.Click += new System.EventHandler(this.button18_Click);
            // 
            // btnHide
            // 
            this.btnHide.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnHide.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHide.Image = ((System.Drawing.Image)(resources.GetObject("btnHide.Image")));
            this.btnHide.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnHide.Location = new System.Drawing.Point(349, 270);
            this.btnHide.Name = "btnHide";
            this.btnHide.Size = new System.Drawing.Size(100, 27);
            this.btnHide.TabIndex = 393;
            this.btnHide.Text = "Close (F10)";
            this.btnHide.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnHide.UseVisualStyleBackColor = false;
            this.btnHide.Click += new System.EventHandler(this.btnHide_Click);
            // 
            // DataGridCommon
            // 
            this.DataGridCommon.AllowUserToAddRows = false;
            this.DataGridCommon.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.DataGridCommon.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridCommon.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.DataGridCommon.Location = new System.Drawing.Point(10, 2);
            this.DataGridCommon.Name = "DataGridCommon";
            this.DataGridCommon.ReadOnly = true;
            this.DataGridCommon.RowHeadersVisible = false;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.DataGridCommon.RowsDefaultCellStyle = dataGridViewCellStyle4;
            this.DataGridCommon.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridCommon.Size = new System.Drawing.Size(447, 267);
            this.DataGridCommon.TabIndex = 0;
            this.DataGridCommon.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridCommon_CellContentClick);
            this.DataGridCommon.DoubleClick += new System.EventHandler(this.DataGridCommon_DoubleClick);
            this.DataGridCommon.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DataGridCommon_KeyDown);
            // 
            // Editpan
            // 
            this.Editpan.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.Editpan.Controls.Add(this.textBox3);
            this.Editpan.Controls.Add(this.label86);
            this.Editpan.Controls.Add(this.grSearch);
            this.Editpan.Controls.Add(this.buttnfinbk);
            this.Editpan.Controls.Add(this.btnsave);
            this.Editpan.Controls.Add(this.tabControl1);
            this.Editpan.Controls.Add(this.mappnl);
            this.Editpan.Controls.Add(this.serialno);
            this.Editpan.Controls.Add(this.HFGST);
            this.Editpan.Controls.Add(this.txtexcise);
            this.Editpan.Controls.Add(this.label54);
            this.Editpan.Controls.Add(this.txtttot);
            this.Editpan.Controls.Add(this.txtigval);
            this.Editpan.Controls.Add(this.label53);
            this.Editpan.Controls.Add(this.label46);
            this.Editpan.Controls.Add(this.txttdisc);
            this.Editpan.Controls.Add(this.txttbval);
            this.Editpan.Controls.Add(this.label45);
            this.Editpan.Controls.Add(this.label43);
            this.Editpan.Controls.Add(this.txttdis);
            this.Editpan.Controls.Add(this.label17);
            this.Editpan.Controls.Add(this.TxtNetAmt);
            this.Editpan.Controls.Add(this.label16);
            this.Editpan.Controls.Add(this.TxtRoff);
            this.Editpan.Controls.Add(this.label8);
            this.Editpan.Controls.Add(this.txtpadd1);
            this.Editpan.Controls.Add(this.label55);
            this.Editpan.Controls.Add(this.label40);
            this.Editpan.Controls.Add(this.txttrans);
            this.Editpan.Controls.Add(this.Txttot);
            this.Editpan.Controls.Add(this.label1);
            this.Editpan.Controls.Add(this.DTPDOCDT);
            this.Editpan.Controls.Add(this.Dtpdt);
            this.Editpan.Controls.Add(this.label6);
            this.Editpan.Controls.Add(this.label5);
            this.Editpan.Controls.Add(this.txtdcno);
            this.Editpan.Controls.Add(this.label2);
            this.Editpan.Controls.Add(this.label3);
            this.Editpan.Controls.Add(this.txtname);
            this.Editpan.Controls.Add(this.Phone);
            this.Editpan.Controls.Add(this.txtgrn);
            this.Editpan.Controls.Add(this.txtpuid);
            this.Editpan.Controls.Add(this.txtgrnid);
            this.Editpan.Controls.Add(this.txtdcid);
            this.Editpan.Controls.Add(this.txtrem);
            this.Editpan.Controls.Add(this.txttitemid);
            this.Editpan.Controls.Add(this.txtpluid);
            this.Editpan.Controls.Add(this.label44);
            this.Editpan.Controls.Add(this.txtuom);
            this.Editpan.Controls.Add(this.Dtprem);
            this.Editpan.Controls.Add(this.label39);
            this.Editpan.Controls.Add(this.Dtppre);
            this.Editpan.Controls.Add(this.label38);
            this.Editpan.Controls.Add(this.pantax);
            this.Editpan.Controls.Add(this.txttempadd2);
            this.Editpan.Controls.Add(this.txttempadd1);
            this.Editpan.Controls.Add(this.button8);
            this.Editpan.Controls.Add(this.button4);
            this.Editpan.Controls.Add(this.label33);
            this.Editpan.Controls.Add(this.label15);
            this.Editpan.Controls.Add(this.txtitemname);
            this.Editpan.Controls.Add(this.label42);
            this.Editpan.Controls.Add(this.buttcusok);
            this.Editpan.Controls.Add(this.label32);
            this.Editpan.Controls.Add(this.label31);
            this.Editpan.Controls.Add(this.label14);
            this.Editpan.Controls.Add(this.txtnotes);
            this.Editpan.Controls.Add(this.txtbval);
            this.Editpan.Controls.Add(this.txtqty);
            this.Editpan.Controls.Add(this.dtpdc);
            this.Editpan.Controls.Add(this.txtitemcode);
            this.Editpan.Controls.Add(this.label10);
            this.Editpan.Controls.Add(this.label11);
            this.Editpan.Controls.Add(this.label37);
            this.Editpan.Controls.Add(this.txtplace);
            this.Editpan.Controls.Add(this.txtpadd2);
            this.Editpan.Controls.Add(this.label41);
            this.Editpan.Controls.Add(this.txtlisid);
            this.Editpan.Controls.Add(this.txttgstval);
            this.Editpan.Controls.Add(this.txttgstp);
            this.Editpan.Controls.Add(this.buttnnxt);
            this.Editpan.Controls.Add(this.btnaddrcan);
            this.Editpan.Controls.Add(this.button11);
            this.Editpan.Controls.Add(this.button12);
            this.Editpan.Controls.Add(this.txtgen2);
            this.Editpan.Controls.Add(this.txtgen3);
            this.Editpan.Controls.Add(this.txtigstp);
            this.Editpan.Controls.Add(this.label52);
            this.Editpan.Controls.Add(this.txtsgstp);
            this.Editpan.Controls.Add(this.label50);
            this.Editpan.Controls.Add(this.label51);
            this.Editpan.Controls.Add(this.txttcgval);
            this.Editpan.Controls.Add(this.txttcgstp);
            this.Editpan.Controls.Add(this.label49);
            this.Editpan.Controls.Add(this.label48);
            this.Editpan.Controls.Add(this.txttsgval);
            this.Editpan.Controls.Add(this.txtgen1);
            this.Editpan.Controls.Add(this.dcdate);
            this.Editpan.Controls.Add(this.label34);
            this.Editpan.Controls.Add(this.label47);
            this.Editpan.Controls.Add(this.txttprdval);
            this.Editpan.Controls.Add(this.txtcharges);
            this.Editpan.Controls.Add(this.label12);
            this.Editpan.Controls.Add(this.label4);
            this.Editpan.Controls.Add(this.txtot);
            this.Editpan.Controls.Add(this.button3);
            this.Editpan.Controls.Add(this.txtamt);
            this.Editpan.Controls.Add(this.button2);
            this.Editpan.Controls.Add(this.txtprice);
            this.Editpan.Controls.Add(this.button7);
            this.Editpan.Controls.Add(this.label35);
            this.Editpan.Controls.Add(this.label36);
            this.Editpan.Controls.Add(this.label57);
            this.Editpan.Controls.Add(this.label59);
            this.Editpan.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Editpan.Location = new System.Drawing.Point(-1, -2);
            this.Editpan.Margin = new System.Windows.Forms.Padding(4);
            this.Editpan.Name = "Editpan";
            this.Editpan.Size = new System.Drawing.Size(988, 594);
            this.Editpan.TabIndex = 243;
            this.Editpan.Paint += new System.Windows.Forms.PaintEventHandler(this.Editpan_Paint);
            // 
            // textBox3
            // 
            this.textBox3.BackColor = System.Drawing.SystemColors.HighlightText;
            this.textBox3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox3.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox3.Location = new System.Drawing.Point(801, 118);
            this.textBox3.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(150, 26);
            this.textBox3.TabIndex = 403;
            this.textBox3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.textBox3.TextChanged += new System.EventHandler(this.textBox3_TextChanged);
            // 
            // label86
            // 
            this.label86.AutoSize = true;
            this.label86.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label86.Location = new System.Drawing.Point(727, 118);
            this.label86.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label86.Name = "label86";
            this.label86.Size = new System.Drawing.Size(65, 21);
            this.label86.TabIndex = 402;
            this.label86.Text = "Charges";
            // 
            // FrmPROCESSBILL
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(987, 635);
            this.Controls.Add(this.panadd);
            this.Controls.Add(this.Taxpan);
            this.Controls.Add(this.Genpan);
            this.Controls.Add(this.Editpan);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Location = new System.Drawing.Point(20, 0);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "FrmPROCESSBILL";
            this.Text = "Process Bill Accounting";
            this.Load += new System.EventHandler(this.FrmPROCESSBILL_Load);
            this.Genpan.ResumeLayout(false);
            this.Genpan.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HFGP)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.Taxpan.ResumeLayout(false);
            this.Taxpan.PerformLayout();
            this.addipan.ResumeLayout(false);
            this.addipan.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HFGT)).EndInit();
            this.panadd.ResumeLayout(false);
            this.panadd.PerformLayout();
            this.pantax.ResumeLayout(false);
            this.pantax.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HFGST)).EndInit();
            this.serialno.ResumeLayout(false);
            this.serialno.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Dataserial)).EndInit();
            this.mappnl.ResumeLayout(false);
            this.mappnl.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridWeight)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HFGPT)).EndInit();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.HFIT)).EndInit();
            this.tabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ItemDet)).EndInit();
            this.grSearch.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DataGridCommon)).EndInit();
            this.Editpan.ResumeLayout(false);
            this.Editpan.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel Genpan;
        private System.Windows.Forms.DataGridView HFGP;
        private System.Windows.Forms.Panel Taxpan;
        private System.Windows.Forms.Button btnsave;
        private System.Windows.Forms.Panel addipan;
        private System.Windows.Forms.TextBox txttotaddd;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Panel panadd;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblno1;
        private System.Windows.Forms.Label lblno2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Button buttnnvfst;
        private System.Windows.Forms.Button buttnnxtlft;
        private System.Windows.Forms.Button btnfinnxt;
        private System.Windows.Forms.Button buttrnxt;
        private System.Windows.Forms.Button buttnext1;
        private System.Windows.Forms.CheckBox chkact;
        private System.Windows.Forms.Button butcan;
        private System.Windows.Forms.Button butedit;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.TextBox txtscr5;
        private System.Windows.Forms.TextBox txtscr4;
        private System.Windows.Forms.TextBox Txtscr3;
        private System.Windows.Forms.TextBox Txtscr2;
        private System.Windows.Forms.TextBox txtscr1;
        private System.Windows.Forms.TextBox txtscr6;
        private System.Windows.Forms.Button buttnfinbk;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.DataGridView HFGT;
        private System.Windows.Forms.TextBox txtscr7;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.TextBox txtscr8;
        private System.Windows.Forms.TextBox txtscr9;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.DateTimePicker dtpfnt;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.TextBox txtchargessum;
        private System.Windows.Forms.TextBox txtbasicval;
        private System.Windows.Forms.TextBox txttax;
        private System.Windows.Forms.TextBox txttotamt;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button21;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label35;
        internal System.Windows.Forms.Button button7;
        private System.Windows.Forms.TextBox txtprice;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox txtamt;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.TextBox txtot;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtcharges;
        private System.Windows.Forms.TextBox txttprdval;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.DateTimePicker dcdate;
        private System.Windows.Forms.TextBox txtgen1;
        private System.Windows.Forms.TextBox txttsgval;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.TextBox txttcgstp;
        private System.Windows.Forms.TextBox txttcgval;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.TextBox txtsgstp;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.TextBox txtigstp;
        private System.Windows.Forms.TextBox txtgen3;
        private System.Windows.Forms.TextBox txtgen2;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Button btnaddrcan;
        private System.Windows.Forms.Button buttnnxt;
        private System.Windows.Forms.TextBox txttgstp;
        private System.Windows.Forms.TextBox txttgstval;
        private System.Windows.Forms.TextBox txtlisid;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.RichTextBox txtpadd2;
        private System.Windows.Forms.TextBox txtplace;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtitemcode;
        private System.Windows.Forms.DateTimePicker dtpdc;
        private System.Windows.Forms.TextBox txtqty;
        private System.Windows.Forms.TextBox txtbval;
        private System.Windows.Forms.TextBox txtnotes;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Button buttcusok;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.TextBox txtitemname;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.TextBox txttempadd1;
        private System.Windows.Forms.TextBox txttempadd2;
        private System.Windows.Forms.Panel pantax;
        private System.Windows.Forms.RichTextBox txttitem;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TextBox txttqty;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TextBox Txtrate;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox txtbasic;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.TextBox txttotal;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox txtigcst;
        private System.Windows.Forms.ComboBox cboigst;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox txtsgst;
        private System.Windows.Forms.ComboBox SGST;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox txtcgst;
        private System.Windows.Forms.ComboBox cbocgst;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txtper;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txttaxable;
        private System.Windows.Forms.TextBox txthidqty;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.DateTimePicker Dtppre;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.DateTimePicker Dtprem;
        private System.Windows.Forms.TextBox txtuom;
        private System.Windows.Forms.Label label44;
        public System.Windows.Forms.TextBox txtpluid;
        private System.Windows.Forms.TextBox txttitemid;
        private System.Windows.Forms.RichTextBox txtrem;
        private System.Windows.Forms.TextBox txtdcid;
        private System.Windows.Forms.TextBox txtgrnid;
        private System.Windows.Forms.TextBox txtpuid;
        private System.Windows.Forms.TextBox txtgrn;
        private System.Windows.Forms.Label Phone;
        private System.Windows.Forms.TextBox txtname;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtdcno;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DateTimePicker Dtpdt;
        private System.Windows.Forms.DateTimePicker DTPDOCDT;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox Txttot;
        private System.Windows.Forms.TextBox txttrans;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.RichTextBox txtpadd1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox TxtRoff;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox TxtNetAmt;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txttdis;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.TextBox txttbval;
        private System.Windows.Forms.TextBox txttdisc;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.TextBox txtigval;
        private System.Windows.Forms.TextBox txtttot;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.TextBox txtexcise;
        private System.Windows.Forms.DataGridView HFGST;
        private System.Windows.Forms.Panel serialno;
        private System.Windows.Forms.DataGridView Dataserial;
        private System.Windows.Forms.Label label78;
        private System.Windows.Forms.Button button20;
        private System.Windows.Forms.Label label76;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.Label label79;
        private System.Windows.Forms.Label label80;
        private System.Windows.Forms.TextBox txtsrail;
        private System.Windows.Forms.ComboBox cboserial;
        private System.Windows.Forms.Label label84;
        private System.Windows.Forms.TextBox txtserialqty;
        private System.Windows.Forms.Label label85;
        private System.Windows.Forms.Button button23;
        private System.Windows.Forms.Label label88;
        private System.Windows.Forms.TextBox textBox14;
        private System.Windows.Forms.TextBox textBox15;
        private System.Windows.Forms.TextBox textBox16;
        private System.Windows.Forms.Panel mappnl;
        private System.Windows.Forms.Label label77;
        private System.Windows.Forms.Button button19;
        private System.Windows.Forms.TextBox txtqty1;
        private System.Windows.Forms.TextBox txtbags;
        private System.Windows.Forms.Label label71;
        private System.Windows.Forms.ComboBox cbosGReturnItem;
        private System.Windows.Forms.DataGridView DataGridWeight;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.TextBox txtWeight;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.TextBox txtBagNo;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.TextBox txtGrossWght;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.TextBox txtTarWght;
        private System.Windows.Forms.Label label75;
        private System.Windows.Forms.TextBox txtlotno2;
        private System.Windows.Forms.TextBox txtstart1;
        private System.Windows.Forms.TextBox txttarewt1;
        private System.Windows.Forms.Button button16;
        private System.Windows.Forms.TextBox txtlotno;
        private System.Windows.Forms.TextBox txttarewt;
        private System.Windows.Forms.TextBox txtstart;
        private System.Windows.Forms.Button button14;
        private System.Windows.Forms.Button button15;
        private System.Windows.Forms.Label label74;
        private System.Windows.Forms.Label label73;
        private System.Windows.Forms.Label label72;
        private System.Windows.Forms.Button button17;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtNoogBags;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.TextBox txtgrossgen;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.TextBox txtlotno1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Button button24;
        private System.Windows.Forms.Button button22;
        private System.Windows.Forms.TextBox txtgrnno;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label82;
        private System.Windows.Forms.ComboBox cbopono;
        private System.Windows.Forms.Label label81;
        private System.Windows.Forms.DataGridView HFGPT;
        private System.Windows.Forms.TextBox txttaxtot;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.TextBox txttype;
        private System.Windows.Forms.Label label83;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.DataGridView HFIT;
        private System.Windows.Forms.Panel grSearch;
        private System.Windows.Forms.Button button18;
        private System.Windows.Forms.Button btnHide;
        private System.Windows.Forms.DataGridView DataGridCommon;
        private System.Windows.Forms.Panel Editpan;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.DataGridView ItemDet;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Label label86;
    }
}