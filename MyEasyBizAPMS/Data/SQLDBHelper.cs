﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace MyEasyBizAPMS
{
    public class SQLDBHelper
    {
        SqlConnection conn = new SqlConnection(GeneralParameters.ConnectionString);
        public DataTable GetData2(CommandType cmdType, string ProcedureName)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = cmdType;
                cmd.CommandText = ProcedureName;
                cmd.Connection = conn;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }
        public DataTable GetData(CommandType cmdType, string ProcedureName, SqlParameter[] para)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = cmdType;
                cmd.CommandText = ProcedureName;
                cmd.Connection = conn;
                cmd.Parameters.AddRange(para);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }
        public DataTable GetDataWithParam(CommandType cmdType, string ProcedureName, SqlParameter[] para, SqlConnection DbConn)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand
                {
                    CommandType = cmdType,
                    CommandText = ProcedureName
                };
                cmd.Parameters.AddRange(para);
                cmd.Connection = DbConn;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
            }
            catch (SqlException ex)
            {
                 throw ex;
            }
            return dt;
        }
        public DataTable GetDataWithoutParam(CommandType cmdType, string ProcedureName, SqlConnection conn)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand
                {
                    CommandType = cmdType,
                    CommandText = ProcedureName,
                    Connection = conn
                };
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            return dt;
        }
        public DataSet GetMultipleData(CommandType cmdType, string ProcedureName, SqlParameter[] para)
        {
            DataSet dt = new DataSet();
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = cmdType;
                cmd.CommandText = ProcedureName;
                cmd.Connection = conn;
                cmd.Parameters.AddRange(para);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }
        public int ExecuteQuery(CommandType cmdType, string ProcedureName, SqlParameter[] para)
        {
            int i = 0;
            try
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = cmdType;
                cmd.CommandText = ProcedureName;
                cmd.Connection = conn;
                cmd.Parameters.AddRange(para);
                i = cmd.ExecuteNonQuery();
                conn.Close();
            }
            catch (Exception ex)
            {
                conn.Close();
                throw ex;
            }
            return i;
        }
        public DataTable getDataWithParameterCmdType(CommandType cmdType, string CommandText, SqlParameter[] para)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = cmdType;
                cmd.CommandText = CommandText;
                cmd.Connection = conn;
                cmd.Parameters.AddRange(para);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
            }
            catch (Exception ex)
            {
                conn.Close();
                throw ex;

            }
            return dt;
        }
        public int ExecuteQuery(CommandType cmdType, string ProcedureName, SqlParameter[] para, int OutPara)
        {
            int i = 0;
            try
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = cmdType;
                cmd.CommandText = ProcedureName;
                cmd.Connection = conn;
                cmd.Parameters.AddRange(para);
                cmd.ExecuteNonQuery();
                i = Convert.ToInt32(para[OutPara].Value.ToString());
                conn.Close();
            }
            catch (Exception ex)
            {
                conn.Close();
                throw ex;
            }
            return i;
        }
        public int ExecuteNonQuery(CommandType cmdType, string CommandText, SqlParameter[] para, SqlConnection conn, int Outpara)
        {
            int Id = 0;
            try
            {
                if (conn.State == ConnectionState.Open)
                {
                    conn.Close();
                }
                conn.Open();
                SqlCommand cmd = new SqlCommand
                {
                    CommandType = cmdType,
                    CommandText = CommandText
                };
                cmd.Parameters.AddRange(para);
                cmd.Connection = conn;
                cmd.ExecuteNonQuery();
                Id = Convert.ToInt32(para[Outpara].Value.ToString());//Solved
                conn.Close();
            }
            catch (SqlException ex)
            {
                conn.Close();
                throw ex;
            }
            return Id;
        }
        public int ExecuteNonQuery(CommandType cmdType, string CommandText, SqlParameter[] para, SqlConnection conn)
        {
            int Id = 0;
            try
            {
                if (conn.State == ConnectionState.Open)
                {
                    conn.Close();
                }
                conn.Open();
                SqlCommand cmd = new SqlCommand
                {
                    CommandType = cmdType,
                    CommandText = CommandText
                };
                cmd.Parameters.AddRange(para);
                cmd.Connection = conn;
                Id = cmd.ExecuteNonQuery();
            }
            catch (SqlException ex)
            {
                conn.Close();
                throw ex;
            }
            return Id;
        }

        public int ExecuteNonQuery(CommandType cmdType, string CommandText, SqlConnection conn)
        {
            int Id = 0;
            try
            {
                if (conn.State == ConnectionState.Open)
                {
                    conn.Close();
                }
                conn.Open();
                SqlCommand cmd = new SqlCommand
                {
                    CommandType = cmdType,
                    CommandText = CommandText
                };
                cmd.Connection = conn;
                Id = cmd.ExecuteNonQuery();
                conn.Close();
            }
            catch (SqlException ex)
            {
                conn.Close();
                throw ex;
            }
            return Id;
        }
        public DataSet GetDataWithParamMultiple(CommandType cmdType, string ProcedureName, SqlParameter[] para, SqlConnection conn)
        {
            DataSet dt = new DataSet();
            try
            {
                SqlCommand cmd = new SqlCommand
                {
                    CommandType = cmdType,
                    CommandText = ProcedureName,
                    Connection = conn
                };
                cmd.Parameters.AddRange(para);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            return dt;
        }

        public DataSet GetDataWithoutParamMultiple(CommandType cmdType, string ProcedureName, SqlConnection conn)
        {
            DataSet dt = new DataSet();
            try
            {
                SqlCommand cmd = new SqlCommand
                {
                    CommandType = cmdType,
                    CommandText = ProcedureName,
                    Connection = conn
                };
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            return dt;
        }
    }
}
