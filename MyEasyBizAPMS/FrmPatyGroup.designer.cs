﻿namespace MyEasyBizAPMS
{
    partial class FrmPatyGroup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmPatyGroup));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.Genpan = new System.Windows.Forms.Panel();
            this.HFGP = new System.Windows.Forms.DataGridView();
            this.label3 = new System.Windows.Forms.Label();
            this.txtscr1 = new System.Windows.Forms.TextBox();
            this.Txtscr4 = new System.Windows.Forms.TextBox();
            this.Txtscr3 = new System.Windows.Forms.TextBox();
            this.Txtscr2 = new System.Windows.Forms.TextBox();
            this.Editpnl = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.grSearch = new System.Windows.Forms.Panel();
            this.button18 = new System.Windows.Forms.Button();
            this.btnHide = new System.Windows.Forms.Button();
            this.DataGridCommon = new System.Windows.Forms.DataGridView();
            this.button6 = new System.Windows.Forms.Button();
            this.label30 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.button4 = new System.Windows.Forms.Button();
            this.HFGT = new System.Windows.Forms.DataGridView();
            this.button5 = new System.Windows.Forms.Button();
            this.cbostate = new System.Windows.Forms.ComboBox();
            this.label29 = new System.Windows.Forms.Label();
            this.txtacno = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.txtifsc = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.txtbranch = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.txtbank = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.txtvendor = new System.Windows.Forms.TextBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.cbocr = new System.Windows.Forms.ComboBox();
            this.cbotype = new System.Windows.Forms.ComboBox();
            this.label23 = new System.Windows.Forms.Label();
            this.txtcrdlmt = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.txtcrddays = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.txtcrange = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.txtcereg = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.txtdiv = new System.Windows.Forms.TextBox();
            this.txtecc = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.txtcoll = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.tngno = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtmail = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtpin = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.txtcity = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtadd2 = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtadd1 = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.Txtctper = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtcode = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtname = new System.Windows.Forms.TextBox();
            this.Phone = new System.Windows.Forms.Label();
            this.txtphone = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.Txtstid = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.txtcst = new System.Windows.Forms.TextBox();
            this.txtbracnid = new System.Windows.Forms.TextBox();
            this.txtstate = new System.Windows.Forms.TextBox();
            this.Chkedtact = new System.Windows.Forms.CheckBox();
            this.panadd = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblno1 = new System.Windows.Forms.Label();
            this.lblno2 = new System.Windows.Forms.Label();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.buttnnvfst = new System.Windows.Forms.Button();
            this.buttnnxtlft = new System.Windows.Forms.Button();
            this.btnfinnxt = new System.Windows.Forms.Button();
            this.buttrnxt = new System.Windows.Forms.Button();
            this.buttnext1 = new System.Windows.Forms.Button();
            this.chkact = new System.Windows.Forms.CheckBox();
            this.butexit = new System.Windows.Forms.Button();
            this.butedit = new System.Windows.Forms.Button();
            this.btnadd = new System.Windows.Forms.Button();
            this.fraitem = new System.Windows.Forms.Panel();
            this.button2 = new System.Windows.Forms.Button();
            this.HFG3 = new System.Windows.Forms.DataGridView();
            this.button1 = new System.Windows.Forms.Button();
            this.btnaddrcan = new System.Windows.Forms.Button();
            this.butnsave = new System.Windows.Forms.Button();
            this.CmdItem1 = new System.Windows.Forms.Button();
            this.buttnext2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.Genpan.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HFGP)).BeginInit();
            this.Editpnl.SuspendLayout();
            this.panel2.SuspendLayout();
            this.grSearch.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridCommon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HFGT)).BeginInit();
            this.panadd.SuspendLayout();
            this.panel1.SuspendLayout();
            this.fraitem.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HFG3)).BeginInit();
            this.SuspendLayout();
            // 
            // Genpan
            // 
            this.Genpan.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.Genpan.Controls.Add(this.HFGP);
            this.Genpan.Controls.Add(this.label3);
            this.Genpan.Controls.Add(this.txtscr1);
            this.Genpan.Controls.Add(this.Txtscr4);
            this.Genpan.Controls.Add(this.Txtscr3);
            this.Genpan.Controls.Add(this.Txtscr2);
            this.Genpan.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Genpan.Location = new System.Drawing.Point(1, 0);
            this.Genpan.Name = "Genpan";
            this.Genpan.Size = new System.Drawing.Size(807, 457);
            this.Genpan.TabIndex = 182;
            // 
            // HFGP
            // 
            this.HFGP.BackgroundColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.HFGP.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.HFGP.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnF2;
            this.HFGP.Location = new System.Drawing.Point(0, 86);
            this.HFGP.Name = "HFGP";
            this.HFGP.ReadOnly = true;
            this.HFGP.Size = new System.Drawing.Size(799, 396);
            this.HFGP.TabIndex = 3;
            this.HFGP.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.HFGP_CellClick);
            this.HFGP.CellMouseUp += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.HFGP_CellMouseUp);
            this.HFGP.KeyUp += new System.Windows.Forms.KeyEventHandler(this.HFGP_KeyUp);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(4, 9);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(61, 18);
            this.label3.TabIndex = 160;
            this.label3.Text = "Supplier";
            // 
            // txtscr1
            // 
            this.txtscr1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtscr1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtscr1.Location = new System.Drawing.Point(0, 31);
            this.txtscr1.Margin = new System.Windows.Forms.Padding(4);
            this.txtscr1.Name = "txtscr1";
            this.txtscr1.Size = new System.Drawing.Size(799, 24);
            this.txtscr1.TabIndex = 1;
            this.txtscr1.TextChanged += new System.EventHandler(this.txtscr1_TextChanged);
            // 
            // Txtscr4
            // 
            this.Txtscr4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Txtscr4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txtscr4.Location = new System.Drawing.Point(555, 86);
            this.Txtscr4.Margin = new System.Windows.Forms.Padding(4);
            this.Txtscr4.Name = "Txtscr4";
            this.Txtscr4.Size = new System.Drawing.Size(147, 24);
            this.Txtscr4.TabIndex = 150;
            this.Txtscr4.TextChanged += new System.EventHandler(this.Txtscr4_TextChanged);
            // 
            // Txtscr3
            // 
            this.Txtscr3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Txtscr3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txtscr3.Location = new System.Drawing.Point(412, 86);
            this.Txtscr3.Margin = new System.Windows.Forms.Padding(4);
            this.Txtscr3.Name = "Txtscr3";
            this.Txtscr3.Size = new System.Drawing.Size(150, 24);
            this.Txtscr3.TabIndex = 88;
            this.Txtscr3.TextChanged += new System.EventHandler(this.Txtscr3_TextChanged);
            // 
            // Txtscr2
            // 
            this.Txtscr2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Txtscr2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txtscr2.Location = new System.Drawing.Point(246, 86);
            this.Txtscr2.Margin = new System.Windows.Forms.Padding(4);
            this.Txtscr2.Name = "Txtscr2";
            this.Txtscr2.Size = new System.Drawing.Size(150, 24);
            this.Txtscr2.TabIndex = 87;
            this.Txtscr2.TextChanged += new System.EventHandler(this.Txtscr2_TextChanged);
            // 
            // Editpnl
            // 
            this.Editpnl.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.Editpnl.Controls.Add(this.panel2);
            this.Editpnl.Controls.Add(this.cbostate);
            this.Editpnl.Controls.Add(this.label29);
            this.Editpnl.Controls.Add(this.txtacno);
            this.Editpnl.Controls.Add(this.label26);
            this.Editpnl.Controls.Add(this.txtifsc);
            this.Editpnl.Controls.Add(this.label27);
            this.Editpnl.Controls.Add(this.txtbranch);
            this.Editpnl.Controls.Add(this.label28);
            this.Editpnl.Controls.Add(this.txtbank);
            this.Editpnl.Controls.Add(this.label25);
            this.Editpnl.Controls.Add(this.txtvendor);
            this.Editpnl.Controls.Add(this.checkBox1);
            this.Editpnl.Controls.Add(this.label20);
            this.Editpnl.Controls.Add(this.label16);
            this.Editpnl.Controls.Add(this.label5);
            this.Editpnl.Controls.Add(this.cbocr);
            this.Editpnl.Controls.Add(this.cbotype);
            this.Editpnl.Controls.Add(this.label23);
            this.Editpnl.Controls.Add(this.txtcrdlmt);
            this.Editpnl.Controls.Add(this.label24);
            this.Editpnl.Controls.Add(this.txtcrddays);
            this.Editpnl.Controls.Add(this.label17);
            this.Editpnl.Controls.Add(this.txtcrange);
            this.Editpnl.Controls.Add(this.label18);
            this.Editpnl.Controls.Add(this.txtcereg);
            this.Editpnl.Controls.Add(this.label19);
            this.Editpnl.Controls.Add(this.txtdiv);
            this.Editpnl.Controls.Add(this.txtecc);
            this.Editpnl.Controls.Add(this.label13);
            this.Editpnl.Controls.Add(this.label14);
            this.Editpnl.Controls.Add(this.txtcoll);
            this.Editpnl.Controls.Add(this.label22);
            this.Editpnl.Controls.Add(this.tngno);
            this.Editpnl.Controls.Add(this.label11);
            this.Editpnl.Controls.Add(this.txtmail);
            this.Editpnl.Controls.Add(this.label12);
            this.Editpnl.Controls.Add(this.txtpin);
            this.Editpnl.Controls.Add(this.label9);
            this.Editpnl.Controls.Add(this.label10);
            this.Editpnl.Controls.Add(this.txtcity);
            this.Editpnl.Controls.Add(this.label8);
            this.Editpnl.Controls.Add(this.txtadd2);
            this.Editpnl.Controls.Add(this.label7);
            this.Editpnl.Controls.Add(this.txtadd1);
            this.Editpnl.Controls.Add(this.label6);
            this.Editpnl.Controls.Add(this.Txtctper);
            this.Editpnl.Controls.Add(this.label4);
            this.Editpnl.Controls.Add(this.txtcode);
            this.Editpnl.Controls.Add(this.label1);
            this.Editpnl.Controls.Add(this.txtname);
            this.Editpnl.Controls.Add(this.Phone);
            this.Editpnl.Controls.Add(this.txtphone);
            this.Editpnl.Controls.Add(this.label2);
            this.Editpnl.Controls.Add(this.label15);
            this.Editpnl.Controls.Add(this.Txtstid);
            this.Editpnl.Controls.Add(this.label21);
            this.Editpnl.Controls.Add(this.txtcst);
            this.Editpnl.Controls.Add(this.txtbracnid);
            this.Editpnl.Controls.Add(this.txtstate);
            this.Editpnl.Location = new System.Drawing.Point(-2, 0);
            this.Editpnl.Name = "Editpnl";
            this.Editpnl.Size = new System.Drawing.Size(810, 482);
            this.Editpnl.TabIndex = 184;
            this.Editpnl.Paint += new System.Windows.Forms.PaintEventHandler(this.Editpnl_Paint);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.panel2.Controls.Add(this.grSearch);
            this.panel2.Controls.Add(this.button6);
            this.panel2.Controls.Add(this.label30);
            this.panel2.Controls.Add(this.textBox1);
            this.panel2.Controls.Add(this.button4);
            this.panel2.Controls.Add(this.HFGT);
            this.panel2.Controls.Add(this.button5);
            this.panel2.Location = new System.Drawing.Point(3, 48);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(804, 428);
            this.panel2.TabIndex = 221;
            // 
            // grSearch
            // 
            this.grSearch.BackColor = System.Drawing.Color.White;
            this.grSearch.Controls.Add(this.button18);
            this.grSearch.Controls.Add(this.btnHide);
            this.grSearch.Controls.Add(this.DataGridCommon);
            this.grSearch.Location = new System.Drawing.Point(99, 48);
            this.grSearch.Name = "grSearch";
            this.grSearch.Size = new System.Drawing.Size(496, 288);
            this.grSearch.TabIndex = 398;
            this.grSearch.Visible = false;
            // 
            // button18
            // 
            this.button18.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button18.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button18.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button18.Location = new System.Drawing.Point(393, 255);
            this.button18.Name = "button18";
            this.button18.Size = new System.Drawing.Size(100, 28);
            this.button18.TabIndex = 394;
            this.button18.Text = "Select (F2)";
            this.button18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button18.UseVisualStyleBackColor = false;
            this.button18.Click += new System.EventHandler(this.button18_Click);
            // 
            // btnHide
            // 
            this.btnHide.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnHide.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHide.Image = ((System.Drawing.Image)(resources.GetObject("btnHide.Image")));
            this.btnHide.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnHide.Location = new System.Drawing.Point(4, 256);
            this.btnHide.Name = "btnHide";
            this.btnHide.Size = new System.Drawing.Size(100, 27);
            this.btnHide.TabIndex = 393;
            this.btnHide.Text = "Close (F10)";
            this.btnHide.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnHide.UseVisualStyleBackColor = false;
            // 
            // DataGridCommon
            // 
            this.DataGridCommon.AllowUserToAddRows = false;
            this.DataGridCommon.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.DataGridCommon.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridCommon.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.DataGridCommon.Location = new System.Drawing.Point(9, 3);
            this.DataGridCommon.Name = "DataGridCommon";
            this.DataGridCommon.ReadOnly = true;
            this.DataGridCommon.RowHeadersVisible = false;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.DataGridCommon.RowsDefaultCellStyle = dataGridViewCellStyle2;
            this.DataGridCommon.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridCommon.Size = new System.Drawing.Size(484, 250);
            this.DataGridCommon.TabIndex = 0;
            this.DataGridCommon.Click += new System.EventHandler(this.DataGridCommon_Click);
            this.DataGridCommon.DoubleClick += new System.EventHandler(this.DataGridCommon_DoubleClick);
            // 
            // button6
            // 
            this.button6.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button6.Location = new System.Drawing.Point(580, 20);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(40, 23);
            this.button6.TabIndex = 212;
            this.button6.Text = "OK";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.ForeColor = System.Drawing.Color.Black;
            this.label30.Location = new System.Drawing.Point(40, 20);
            this.label30.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(51, 21);
            this.label30.TabIndex = 210;
            this.label30.Text = "Name";
            // 
            // textBox1
            // 
            this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.Location = new System.Drawing.Point(99, 15);
            this.textBox1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(463, 26);
            this.textBox1.TabIndex = 211;
            this.textBox1.Click += new System.EventHandler(this.textBox1_Click);
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button4.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.Image = ((System.Drawing.Image)(resources.GetObject("button4.Image")));
            this.button4.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button4.Location = new System.Drawing.Point(720, 440);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 30);
            this.button4.TabIndex = 209;
            this.button4.Text = "Close";
            this.button4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // HFGT
            // 
            this.HFGT.BackgroundColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.HFGT.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.HFGT.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.HFGT.Location = new System.Drawing.Point(21, 50);
            this.HFGT.Name = "HFGT";
            this.HFGT.Size = new System.Drawing.Size(778, 384);
            this.HFGT.TabIndex = 0;
            this.HFGT.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.HFGT_CellContentClick);
            this.HFGT.KeyDown += new System.Windows.Forms.KeyEventHandler(this.HFGT_KeyDown);
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.Color.White;
            this.button5.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button5.Image = ((System.Drawing.Image)(resources.GetObject("button5.Image")));
            this.button5.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button5.Location = new System.Drawing.Point(636, 439);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(60, 30);
            this.button5.TabIndex = 202;
            this.button5.Text = "Back";
            this.button5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // cbostate
            // 
            this.cbostate.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbostate.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbostate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbostate.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbostate.FormattingEnabled = true;
            this.cbostate.Items.AddRange(new object[] {
            "Dealer",
            "Distributor",
            "Manufacturer",
            "Traders"});
            this.cbostate.Location = new System.Drawing.Point(173, 379);
            this.cbostate.Name = "cbostate";
            this.cbostate.Size = new System.Drawing.Size(189, 26);
            this.cbostate.TabIndex = 220;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.ForeColor = System.Drawing.Color.Black;
            this.label29.Location = new System.Drawing.Point(581, 357);
            this.label29.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(97, 21);
            this.label29.TabIndex = 218;
            this.label29.Text = "A/C Number";
            // 
            // txtacno
            // 
            this.txtacno.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtacno.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtacno.ForeColor = System.Drawing.Color.Black;
            this.txtacno.Location = new System.Drawing.Point(584, 378);
            this.txtacno.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtacno.Name = "txtacno";
            this.txtacno.Size = new System.Drawing.Size(192, 26);
            this.txtacno.TabIndex = 217;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.Color.Black;
            this.label26.Location = new System.Drawing.Point(389, 357);
            this.label26.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(78, 21);
            this.label26.TabIndex = 216;
            this.label26.Text = "IFSC Code";
            // 
            // txtifsc
            // 
            this.txtifsc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtifsc.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtifsc.ForeColor = System.Drawing.Color.Black;
            this.txtifsc.Location = new System.Drawing.Point(392, 378);
            this.txtifsc.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtifsc.Name = "txtifsc";
            this.txtifsc.Size = new System.Drawing.Size(184, 26);
            this.txtifsc.TabIndex = 213;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.ForeColor = System.Drawing.Color.Black;
            this.label27.Location = new System.Drawing.Point(581, 301);
            this.label27.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(62, 21);
            this.label27.TabIndex = 215;
            this.label27.Text = "Branch ";
            // 
            // txtbranch
            // 
            this.txtbranch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtbranch.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtbranch.Location = new System.Drawing.Point(584, 326);
            this.txtbranch.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtbranch.Name = "txtbranch";
            this.txtbranch.Size = new System.Drawing.Size(195, 26);
            this.txtbranch.TabIndex = 212;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.Color.Black;
            this.label28.Location = new System.Drawing.Point(389, 301);
            this.label28.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(89, 21);
            this.label28.TabIndex = 214;
            this.label28.Text = "Bank Name";
            // 
            // txtbank
            // 
            this.txtbank.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtbank.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtbank.Location = new System.Drawing.Point(392, 326);
            this.txtbank.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtbank.Name = "txtbank";
            this.txtbank.Size = new System.Drawing.Size(184, 26);
            this.txtbank.TabIndex = 211;
            this.txtbank.Click += new System.EventHandler(this.txtbank_Click);
            this.txtbank.TextChanged += new System.EventHandler(this.textBox3_TextChanged);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.Black;
            this.label25.Location = new System.Drawing.Point(580, 101);
            this.label25.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(99, 21);
            this.label25.TabIndex = 210;
            this.label25.Text = "Vendor Code";
            // 
            // txtvendor
            // 
            this.txtvendor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtvendor.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvendor.Location = new System.Drawing.Point(583, 122);
            this.txtvendor.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtvendor.Name = "txtvendor";
            this.txtvendor.Size = new System.Drawing.Size(192, 26);
            this.txtvendor.TabIndex = 209;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.BackColor = System.Drawing.Color.White;
            this.checkBox1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox1.Location = new System.Drawing.Point(520, 74);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(91, 22);
            this.checkBox1.TabIndex = 208;
            this.checkBox1.Text = "100 % EOU";
            this.checkBox1.UseVisualStyleBackColor = false;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Black;
            this.label20.Location = new System.Drawing.Point(389, 148);
            this.label20.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(82, 21);
            this.label20.TabIndex = 207;
            this.label20.Text = "Phone No.";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Black;
            this.label16.Location = new System.Drawing.Point(11, 9);
            this.label16.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(91, 21);
            this.label16.TabIndex = 206;
            this.label16.Text = "Mill Master";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(23, 197);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(125, 21);
            this.label5.TabIndex = 204;
            this.label5.Text = "Contact Person2";
            // 
            // cbocr
            // 
            this.cbocr.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbocr.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbocr.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbocr.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbocr.FormattingEnabled = true;
            this.cbocr.Location = new System.Drawing.Point(584, 431);
            this.cbocr.Name = "cbocr";
            this.cbocr.Size = new System.Drawing.Size(195, 26);
            this.cbocr.TabIndex = 178;
            this.cbocr.SelectedIndexChanged += new System.EventHandler(this.cbocr_SelectedIndexChanged);
            // 
            // cbotype
            // 
            this.cbotype.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbotype.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbotype.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbotype.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbotype.FormattingEnabled = true;
            this.cbotype.Items.AddRange(new object[] {
            "Dealer",
            "Distributor",
            "Manufacturer",
            "Traders",
            "Processing Unit"});
            this.cbotype.Location = new System.Drawing.Point(392, 429);
            this.cbotype.Name = "cbotype";
            this.cbotype.Size = new System.Drawing.Size(185, 26);
            this.cbotype.TabIndex = 177;
            this.cbotype.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cbotype_KeyPress);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.Black;
            this.label23.Location = new System.Drawing.Point(581, 247);
            this.label23.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(91, 21);
            this.label23.TabIndex = 196;
            this.label23.Text = "Credit Limit";
            // 
            // txtcrdlmt
            // 
            this.txtcrdlmt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtcrdlmt.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtcrdlmt.Location = new System.Drawing.Point(584, 271);
            this.txtcrdlmt.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtcrdlmt.Name = "txtcrdlmt";
            this.txtcrdlmt.Size = new System.Drawing.Size(195, 26);
            this.txtcrdlmt.TabIndex = 176;
            this.txtcrdlmt.Text = "0";
            this.txtcrdlmt.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtcrdlmt_KeyPress);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.Black;
            this.label24.Location = new System.Drawing.Point(389, 249);
            this.label24.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(89, 21);
            this.label24.TabIndex = 194;
            this.label24.Text = "Credit Days";
            // 
            // txtcrddays
            // 
            this.txtcrddays.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtcrddays.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtcrddays.ForeColor = System.Drawing.Color.Black;
            this.txtcrddays.Location = new System.Drawing.Point(392, 271);
            this.txtcrddays.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtcrddays.Name = "txtcrddays";
            this.txtcrddays.Size = new System.Drawing.Size(184, 26);
            this.txtcrddays.TabIndex = 175;
            this.txtcrddays.Text = "0";
            this.txtcrddays.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtcrddays_KeyPress);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Black;
            this.label17.Location = new System.Drawing.Point(581, 197);
            this.label17.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(70, 21);
            this.label17.TabIndex = 192;
            this.label17.Text = "E-mail Id";
            // 
            // txtcrange
            // 
            this.txtcrange.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtcrange.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtcrange.Location = new System.Drawing.Point(584, 218);
            this.txtcrange.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtcrange.Name = "txtcrange";
            this.txtcrange.Size = new System.Drawing.Size(195, 26);
            this.txtcrange.TabIndex = 174;
            this.txtcrange.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtcrange_KeyPress);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Black;
            this.label18.Location = new System.Drawing.Point(389, 197);
            this.label18.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(82, 21);
            this.label18.TabIndex = 190;
            this.label18.Text = "Phone No.";
            // 
            // txtcereg
            // 
            this.txtcereg.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtcereg.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtcereg.Location = new System.Drawing.Point(392, 218);
            this.txtcereg.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtcereg.Name = "txtcereg";
            this.txtcereg.Size = new System.Drawing.Size(184, 26);
            this.txtcereg.TabIndex = 173;
            this.txtcereg.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtcereg_KeyPress);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.Black;
            this.label19.Location = new System.Drawing.Point(581, 148);
            this.label19.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(70, 21);
            this.label19.TabIndex = 188;
            this.label19.Text = "E-mail Id";
            // 
            // txtdiv
            // 
            this.txtdiv.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtdiv.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtdiv.Location = new System.Drawing.Point(583, 171);
            this.txtdiv.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtdiv.Name = "txtdiv";
            this.txtdiv.Size = new System.Drawing.Size(195, 26);
            this.txtdiv.TabIndex = 172;
            this.txtdiv.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtdiv_KeyPress);
            // 
            // txtecc
            // 
            this.txtecc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtecc.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtecc.Location = new System.Drawing.Point(392, 171);
            this.txtecc.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtecc.Name = "txtecc";
            this.txtecc.Size = new System.Drawing.Size(184, 26);
            this.txtecc.TabIndex = 171;
            this.txtecc.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtecc_KeyPress);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Black;
            this.label13.Location = new System.Drawing.Point(581, 407);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(42, 21);
            this.label13.TabIndex = 184;
            this.label13.Text = "Type";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Black;
            this.label14.Location = new System.Drawing.Point(389, 405);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(83, 21);
            this.label14.TabIndex = 183;
            this.label14.Text = "Party Type";
            // 
            // txtcoll
            // 
            this.txtcoll.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtcoll.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtcoll.Location = new System.Drawing.Point(26, 220);
            this.txtcoll.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtcoll.Name = "txtcoll";
            this.txtcoll.Size = new System.Drawing.Size(337, 26);
            this.txtcoll.TabIndex = 162;
            this.txtcoll.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtcoll_KeyPress);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.Black;
            this.label22.Location = new System.Drawing.Point(390, 101);
            this.label22.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(60, 21);
            this.label22.TabIndex = 179;
            this.label22.Text = "GSTTIN";
            // 
            // tngno
            // 
            this.tngno.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tngno.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tngno.Location = new System.Drawing.Point(393, 122);
            this.tngno.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tngno.Name = "tngno";
            this.tngno.Size = new System.Drawing.Size(184, 26);
            this.tngno.TabIndex = 169;
            this.tngno.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tngno_KeyPress);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(169, 405);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(70, 21);
            this.label11.TabIndex = 3;
            this.label11.Text = "E-mail Id";
            // 
            // txtmail
            // 
            this.txtmail.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtmail.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtmail.Location = new System.Drawing.Point(173, 431);
            this.txtmail.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtmail.Name = "txtmail";
            this.txtmail.Size = new System.Drawing.Size(189, 26);
            this.txtmail.TabIndex = 168;
            this.txtmail.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtmail_KeyPress);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(23, 405);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(32, 21);
            this.label12.TabIndex = 2;
            this.label12.Text = "Pin";
            // 
            // txtpin
            // 
            this.txtpin.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtpin.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtpin.Location = new System.Drawing.Point(25, 431);
            this.txtpin.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtpin.Name = "txtpin";
            this.txtpin.Size = new System.Drawing.Size(140, 26);
            this.txtpin.TabIndex = 167;
            this.txtpin.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtpin_KeyPress);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(170, 363);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(46, 21);
            this.label9.TabIndex = 3;
            this.label9.Text = "State";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(23, 354);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(37, 21);
            this.label10.TabIndex = 1;
            this.label10.Text = "City";
            // 
            // txtcity
            // 
            this.txtcity.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtcity.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtcity.Location = new System.Drawing.Point(26, 379);
            this.txtcity.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtcity.Name = "txtcity";
            this.txtcity.Size = new System.Drawing.Size(140, 26);
            this.txtcity.TabIndex = 165;
            this.txtcity.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtcity_KeyPress);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(25, 301);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(75, 21);
            this.label8.TabIndex = 8;
            this.label8.Text = "Address2";
            // 
            // txtadd2
            // 
            this.txtadd2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtadd2.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtadd2.Location = new System.Drawing.Point(26, 327);
            this.txtadd2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtadd2.Name = "txtadd2";
            this.txtadd2.Size = new System.Drawing.Size(337, 26);
            this.txtadd2.TabIndex = 164;
            this.txtadd2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtadd2_KeyPress);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(25, 249);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(75, 21);
            this.label7.TabIndex = 1;
            this.label7.Text = "Address1";
            // 
            // txtadd1
            // 
            this.txtadd1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtadd1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtadd1.Location = new System.Drawing.Point(26, 273);
            this.txtadd1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtadd1.Name = "txtadd1";
            this.txtadd1.Size = new System.Drawing.Size(337, 26);
            this.txtadd1.TabIndex = 162;
            this.txtadd1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtadd1_KeyPress);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(23, 149);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(125, 21);
            this.label6.TabIndex = 3;
            this.label6.Text = "Contact Person1";
            // 
            // Txtctper
            // 
            this.Txtctper.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Txtctper.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txtctper.Location = new System.Drawing.Point(25, 171);
            this.Txtctper.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Txtctper.Name = "Txtctper";
            this.Txtctper.Size = new System.Drawing.Size(337, 26);
            this.Txtctper.TabIndex = 161;
            this.Txtctper.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Txtctper_KeyPress);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(23, 101);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(45, 21);
            this.label4.TabIndex = 1;
            this.label4.Text = "Code";
            // 
            // txtcode
            // 
            this.txtcode.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtcode.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtcode.Location = new System.Drawing.Point(25, 122);
            this.txtcode.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtcode.Name = "txtcode";
            this.txtcode.Size = new System.Drawing.Size(140, 26);
            this.txtcode.TabIndex = 159;
            this.txtcode.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtcode_KeyPress);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(23, 48);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 21);
            this.label1.TabIndex = 0;
            this.label1.Text = "Name";
            // 
            // txtname
            // 
            this.txtname.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtname.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtname.Location = new System.Drawing.Point(24, 74);
            this.txtname.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtname.Name = "txtname";
            this.txtname.Size = new System.Drawing.Size(463, 26);
            this.txtname.TabIndex = 158;
            this.txtname.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtname_KeyPress);
            // 
            // Phone
            // 
            this.Phone.AutoSize = true;
            this.Phone.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Phone.ForeColor = System.Drawing.Color.Black;
            this.Phone.Location = new System.Drawing.Point(169, 101);
            this.Phone.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Phone.Name = "Phone";
            this.Phone.Size = new System.Drawing.Size(54, 21);
            this.Phone.TabIndex = 3;
            this.Phone.Text = "Phone";
            // 
            // txtphone
            // 
            this.txtphone.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtphone.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtphone.Location = new System.Drawing.Point(173, 122);
            this.txtphone.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtphone.Name = "txtphone";
            this.txtphone.Size = new System.Drawing.Size(189, 26);
            this.txtphone.TabIndex = 160;
            this.txtphone.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtphone_KeyPress);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(162, 31);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 21);
            this.label2.TabIndex = 157;
            this.label2.Text = "Details";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Black;
            this.label15.Location = new System.Drawing.Point(513, 28);
            this.label15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(101, 21);
            this.label15.TabIndex = 199;
            this.label15.Text = "Other Details";
            // 
            // Txtstid
            // 
            this.Txtstid.Location = new System.Drawing.Point(268, 381);
            this.Txtstid.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Txtstid.Name = "Txtstid";
            this.Txtstid.Size = new System.Drawing.Size(42, 21);
            this.Txtstid.TabIndex = 202;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Black;
            this.label21.Location = new System.Drawing.Point(400, 124);
            this.label21.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(86, 15);
            this.label21.TabIndex = 181;
            this.label21.Text = "CST No && Date";
            // 
            // txtcst
            // 
            this.txtcst.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtcst.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtcst.Location = new System.Drawing.Point(393, 122);
            this.txtcst.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtcst.Name = "txtcst";
            this.txtcst.Size = new System.Drawing.Size(85, 23);
            this.txtcst.TabIndex = 8;
            // 
            // txtbracnid
            // 
            this.txtbracnid.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtbracnid.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtbracnid.ForeColor = System.Drawing.Color.Black;
            this.txtbracnid.Location = new System.Drawing.Point(284, 74);
            this.txtbracnid.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtbracnid.Name = "txtbracnid";
            this.txtbracnid.Size = new System.Drawing.Size(56, 23);
            this.txtbracnid.TabIndex = 219;
            // 
            // txtstate
            // 
            this.txtstate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtstate.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtstate.Location = new System.Drawing.Point(298, 74);
            this.txtstate.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtstate.Name = "txtstate";
            this.txtstate.Size = new System.Drawing.Size(32, 23);
            this.txtstate.TabIndex = 7;
            this.txtstate.TextChanged += new System.EventHandler(this.txtstate_TextChanged);
            this.txtstate.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtstate_KeyDown);
            // 
            // Chkedtact
            // 
            this.Chkedtact.AutoSize = true;
            this.Chkedtact.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Chkedtact.Location = new System.Drawing.Point(12, 488);
            this.Chkedtact.Name = "Chkedtact";
            this.Chkedtact.Size = new System.Drawing.Size(65, 22);
            this.Chkedtact.TabIndex = 205;
            this.Chkedtact.Text = "Active";
            this.Chkedtact.UseVisualStyleBackColor = true;
            // 
            // panadd
            // 
            this.panadd.BackColor = System.Drawing.Color.White;
            this.panadd.Controls.Add(this.panel1);
            this.panadd.Controls.Add(this.buttnnvfst);
            this.panadd.Controls.Add(this.buttnnxtlft);
            this.panadd.Controls.Add(this.btnfinnxt);
            this.panadd.Controls.Add(this.buttrnxt);
            this.panadd.Controls.Add(this.buttnext1);
            this.panadd.Controls.Add(this.chkact);
            this.panadd.Controls.Add(this.butexit);
            this.panadd.Controls.Add(this.butedit);
            this.panadd.Controls.Add(this.btnadd);
            this.panadd.Location = new System.Drawing.Point(1, 482);
            this.panadd.Name = "panadd";
            this.panadd.Size = new System.Drawing.Size(804, 31);
            this.panadd.TabIndex = 206;
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.lblno1);
            this.panel1.Controls.Add(this.lblno2);
            this.panel1.Controls.Add(this.flowLayoutPanel3);
            this.panel1.Controls.Add(this.flowLayoutPanel2);
            this.panel1.Controls.Add(this.flowLayoutPanel1);
            this.panel1.Location = new System.Drawing.Point(64, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(74, 30);
            this.panel1.TabIndex = 214;
            // 
            // lblno1
            // 
            this.lblno1.AutoSize = true;
            this.lblno1.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblno1.ForeColor = System.Drawing.Color.Black;
            this.lblno1.Location = new System.Drawing.Point(4, 5);
            this.lblno1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblno1.Name = "lblno1";
            this.lblno1.Size = new System.Drawing.Size(14, 15);
            this.lblno1.TabIndex = 163;
            this.lblno1.Text = "1";
            // 
            // lblno2
            // 
            this.lblno2.AutoSize = true;
            this.lblno2.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblno2.ForeColor = System.Drawing.Color.Black;
            this.lblno2.Location = new System.Drawing.Point(27, 5);
            this.lblno2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblno2.Name = "lblno2";
            this.lblno2.Size = new System.Drawing.Size(28, 15);
            this.lblno2.TabIndex = 162;
            this.lblno2.Text = "of 1";
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.Location = new System.Drawing.Point(61, 30);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(200, 100);
            this.flowLayoutPanel3.TabIndex = 2;
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.Location = new System.Drawing.Point(38, 35);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(200, 100);
            this.flowLayoutPanel2.TabIndex = 1;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Location = new System.Drawing.Point(62, -129);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(200, 100);
            this.flowLayoutPanel1.TabIndex = 0;
            // 
            // buttnnvfst
            // 
            this.buttnnvfst.BackColor = System.Drawing.Color.White;
            this.buttnnvfst.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.buttnnvfst.FlatAppearance.BorderSize = 0;
            this.buttnnvfst.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttnnvfst.Image = ((System.Drawing.Image)(resources.GetObject("buttnnvfst.Image")));
            this.buttnnvfst.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttnnvfst.Location = new System.Drawing.Point(6, 0);
            this.buttnnvfst.Name = "buttnnvfst";
            this.buttnnvfst.Size = new System.Drawing.Size(19, 31);
            this.buttnnvfst.TabIndex = 213;
            this.buttnnvfst.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttnnvfst.UseVisualStyleBackColor = false;
            this.buttnnvfst.Click += new System.EventHandler(this.buttnnvfst_Click);
            // 
            // buttnnxtlft
            // 
            this.buttnnxtlft.BackColor = System.Drawing.Color.White;
            this.buttnnxtlft.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.buttnnxtlft.FlatAppearance.BorderSize = 0;
            this.buttnnxtlft.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttnnxtlft.Image = ((System.Drawing.Image)(resources.GetObject("buttnnxtlft.Image")));
            this.buttnnxtlft.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttnnxtlft.Location = new System.Drawing.Point(40, 0);
            this.buttnnxtlft.Name = "buttnnxtlft";
            this.buttnnxtlft.Size = new System.Drawing.Size(18, 31);
            this.buttnnxtlft.TabIndex = 212;
            this.buttnnxtlft.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttnnxtlft.UseVisualStyleBackColor = false;
            this.buttnnxtlft.Click += new System.EventHandler(this.buttnnxtlft_Click);
            // 
            // btnfinnxt
            // 
            this.btnfinnxt.BackColor = System.Drawing.Color.White;
            this.btnfinnxt.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnfinnxt.FlatAppearance.BorderSize = 0;
            this.btnfinnxt.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnfinnxt.Image = ((System.Drawing.Image)(resources.GetObject("btnfinnxt.Image")));
            this.btnfinnxt.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnfinnxt.Location = new System.Drawing.Point(178, 0);
            this.btnfinnxt.Name = "btnfinnxt";
            this.btnfinnxt.Size = new System.Drawing.Size(19, 31);
            this.btnfinnxt.TabIndex = 211;
            this.btnfinnxt.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnfinnxt.UseVisualStyleBackColor = false;
            this.btnfinnxt.Click += new System.EventHandler(this.btnfinnxt_Click);
            // 
            // buttrnxt
            // 
            this.buttrnxt.BackColor = System.Drawing.Color.White;
            this.buttrnxt.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.buttrnxt.FlatAppearance.BorderSize = 0;
            this.buttrnxt.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttrnxt.Image = ((System.Drawing.Image)(resources.GetObject("buttrnxt.Image")));
            this.buttrnxt.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttrnxt.Location = new System.Drawing.Point(144, 0);
            this.buttrnxt.Name = "buttrnxt";
            this.buttrnxt.Size = new System.Drawing.Size(18, 31);
            this.buttrnxt.TabIndex = 210;
            this.buttrnxt.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttrnxt.UseVisualStyleBackColor = false;
            this.buttrnxt.Click += new System.EventHandler(this.buttrnxt_Click);
            // 
            // buttnext1
            // 
            this.buttnext1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttnext1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttnext1.Image = ((System.Drawing.Image)(resources.GetObject("buttnext1.Image")));
            this.buttnext1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttnext1.Location = new System.Drawing.Point(515, 1);
            this.buttnext1.Name = "buttnext1";
            this.buttnext1.Size = new System.Drawing.Size(58, 30);
            this.buttnext1.TabIndex = 208;
            this.buttnext1.Text = "Exit";
            this.buttnext1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttnext1.UseVisualStyleBackColor = false;
            this.buttnext1.Click += new System.EventHandler(this.buttnext1_Click);
            // 
            // chkact
            // 
            this.chkact.AutoSize = true;
            this.chkact.BackColor = System.Drawing.Color.White;
            this.chkact.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkact.Location = new System.Drawing.Point(227, 7);
            this.chkact.Name = "chkact";
            this.chkact.Size = new System.Drawing.Size(65, 22);
            this.chkact.TabIndex = 187;
            this.chkact.Text = "Active";
            this.chkact.UseVisualStyleBackColor = false;
            this.chkact.CheckedChanged += new System.EventHandler(this.chkact_CheckedChanged_1);
            // 
            // butexit
            // 
            this.butexit.BackColor = System.Drawing.Color.White;
            this.butexit.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.butexit.Image = ((System.Drawing.Image)(resources.GetObject("butexit.Image")));
            this.butexit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.butexit.Location = new System.Drawing.Point(443, 1);
            this.butexit.Name = "butexit";
            this.butexit.Size = new System.Drawing.Size(71, 30);
            this.butexit.TabIndex = 186;
            this.butexit.Text = "Delete";
            this.butexit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.butexit.UseVisualStyleBackColor = false;
            this.butexit.Click += new System.EventHandler(this.butexit_Click);
            // 
            // butedit
            // 
            this.butedit.BackColor = System.Drawing.Color.White;
            this.butedit.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.butedit.Image = ((System.Drawing.Image)(resources.GetObject("butedit.Image")));
            this.butedit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.butedit.Location = new System.Drawing.Point(389, 1);
            this.butedit.Name = "butedit";
            this.butedit.Size = new System.Drawing.Size(55, 30);
            this.butedit.TabIndex = 185;
            this.butedit.Text = "Edit";
            this.butedit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.butedit.UseVisualStyleBackColor = false;
            this.butedit.Click += new System.EventHandler(this.butedit_Click);
            // 
            // btnadd
            // 
            this.btnadd.BackColor = System.Drawing.Color.White;
            this.btnadd.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnadd.FlatAppearance.BorderSize = 0;
            this.btnadd.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnadd.Image = ((System.Drawing.Image)(resources.GetObject("btnadd.Image")));
            this.btnadd.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnadd.Location = new System.Drawing.Point(298, 1);
            this.btnadd.Name = "btnadd";
            this.btnadd.Size = new System.Drawing.Size(91, 30);
            this.btnadd.TabIndex = 184;
            this.btnadd.Text = "Add New";
            this.btnadd.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnadd.UseVisualStyleBackColor = false;
            this.btnadd.Click += new System.EventHandler(this.btnadd_Click);
            // 
            // fraitem
            // 
            this.fraitem.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.fraitem.Controls.Add(this.button2);
            this.fraitem.Controls.Add(this.HFG3);
            this.fraitem.Controls.Add(this.button1);
            this.fraitem.Location = new System.Drawing.Point(1, 463);
            this.fraitem.Name = "fraitem";
            this.fraitem.Size = new System.Drawing.Size(84, 48);
            this.fraitem.TabIndex = 210;
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button2.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Image = ((System.Drawing.Image)(resources.GetObject("button2.Image")));
            this.button2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button2.Location = new System.Drawing.Point(314, 375);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 30);
            this.button2.TabIndex = 209;
            this.button2.Text = "Close";
            this.button2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // HFG3
            // 
            this.HFG3.BackgroundColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.HFG3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.HFG3.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.HFG3.Location = new System.Drawing.Point(3, 4);
            this.HFG3.Name = "HFG3";
            this.HFG3.Size = new System.Drawing.Size(796, 369);
            this.HFG3.TabIndex = 0;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.White;
            this.button1.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Image = ((System.Drawing.Image)(resources.GetObject("button1.Image")));
            this.button1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button1.Location = new System.Drawing.Point(169, 335);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(60, 30);
            this.button1.TabIndex = 202;
            this.button1.Text = "Back";
            this.button1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button1.UseVisualStyleBackColor = false;
            // 
            // btnaddrcan
            // 
            this.btnaddrcan.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnaddrcan.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnaddrcan.Image = ((System.Drawing.Image)(resources.GetObject("btnaddrcan.Image")));
            this.btnaddrcan.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnaddrcan.Location = new System.Drawing.Point(138, 484);
            this.btnaddrcan.Name = "btnaddrcan";
            this.btnaddrcan.Size = new System.Drawing.Size(60, 30);
            this.btnaddrcan.TabIndex = 201;
            this.btnaddrcan.Text = "Back";
            this.btnaddrcan.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnaddrcan.UseVisualStyleBackColor = false;
            this.btnaddrcan.Click += new System.EventHandler(this.btnaddrcan_Click);
            // 
            // butnsave
            // 
            this.butnsave.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.butnsave.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.butnsave.Image = ((System.Drawing.Image)(resources.GetObject("butnsave.Image")));
            this.butnsave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.butnsave.Location = new System.Drawing.Point(74, 483);
            this.butnsave.Name = "butnsave";
            this.butnsave.Size = new System.Drawing.Size(63, 30);
            this.butnsave.TabIndex = 179;
            this.butnsave.Text = "Save";
            this.butnsave.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.butnsave.UseVisualStyleBackColor = false;
            this.butnsave.Click += new System.EventHandler(this.butnsave_Click);
            // 
            // CmdItem1
            // 
            this.CmdItem1.BackColor = System.Drawing.Color.White;
            this.CmdItem1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CmdItem1.Image = ((System.Drawing.Image)(resources.GetObject("CmdItem1.Image")));
            this.CmdItem1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.CmdItem1.Location = new System.Drawing.Point(341, 484);
            this.CmdItem1.Name = "CmdItem1";
            this.CmdItem1.Size = new System.Drawing.Size(144, 31);
            this.CmdItem1.TabIndex = 203;
            this.CmdItem1.Text = "Mapped Item list";
            this.CmdItem1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.CmdItem1.UseVisualStyleBackColor = false;
            this.CmdItem1.Visible = false;
            this.CmdItem1.Click += new System.EventHandler(this.CmdItem1_Click);
            // 
            // buttnext2
            // 
            this.buttnext2.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttnext2.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttnext2.Image = ((System.Drawing.Image)(resources.GetObject("buttnext2.Image")));
            this.buttnext2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttnext2.Location = new System.Drawing.Point(200, 484);
            this.buttnext2.Name = "buttnext2";
            this.buttnext2.Size = new System.Drawing.Size(57, 30);
            this.buttnext2.TabIndex = 207;
            this.buttnext2.Text = "Exit";
            this.buttnext2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttnext2.UseVisualStyleBackColor = false;
            this.buttnext2.Click += new System.EventHandler(this.buttnext2_Click);
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.White;
            this.button3.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.Image = ((System.Drawing.Image)(resources.GetObject("button3.Image")));
            this.button3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button3.Location = new System.Drawing.Point(637, 483);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(171, 31);
            this.button3.TabIndex = 211;
            this.button3.Text = "Mapped Party Group";
            this.button3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // FrmPatyGroup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(808, 515);
            this.Controls.Add(this.panadd);
            this.Controls.Add(this.fraitem);
            this.Controls.Add(this.Chkedtact);
            this.Controls.Add(this.btnaddrcan);
            this.Controls.Add(this.butnsave);
            this.Controls.Add(this.CmdItem1);
            this.Controls.Add(this.buttnext2);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.Genpan);
            this.Controls.Add(this.Editpnl);
            this.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "FrmPatyGroup";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Supplier";
            this.Load += new System.EventHandler(this.FrmPatyGroup_Load);
            this.Genpan.ResumeLayout(false);
            this.Genpan.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HFGP)).EndInit();
            this.Editpnl.ResumeLayout(false);
            this.Editpnl.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.grSearch.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DataGridCommon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HFGT)).EndInit();
            this.panadd.ResumeLayout(false);
            this.panadd.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.fraitem.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.HFG3)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel Genpan;
        private System.Windows.Forms.TextBox Txtscr3;
        private System.Windows.Forms.TextBox Txtscr2;
        private System.Windows.Forms.TextBox txtscr1;
        private System.Windows.Forms.DataGridView HFGP;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel Editpnl;
        private System.Windows.Forms.CheckBox Chkedtact;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button butnsave;
        private System.Windows.Forms.Button btnaddrcan;
        private System.Windows.Forms.ComboBox cbocr;
        private System.Windows.Forms.ComboBox cbotype;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox txtcrdlmt;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox txtcrddays;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtcrange;
        protected System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txtcereg;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txtdiv;
        private System.Windows.Forms.TextBox txtecc;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtcoll;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox txtcst;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox tngno;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtmail;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtpin;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtstate;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtcity;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtadd2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtadd1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox Txtctper;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtcode;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtname;
        private System.Windows.Forms.Label Phone;
        private System.Windows.Forms.TextBox txtphone;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox Txtstid;
        private System.Windows.Forms.Button CmdItem1;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Panel panadd;
        private System.Windows.Forms.CheckBox chkact;
        private System.Windows.Forms.Button butexit;
        private System.Windows.Forms.Button butedit;
        private System.Windows.Forms.Button btnadd;
        protected System.Windows.Forms.Label label20;
        private System.Windows.Forms.Button buttnext2;
        private System.Windows.Forms.Button buttnext1;
        private System.Windows.Forms.Button btnfinnxt;
        private System.Windows.Forms.Button buttrnxt;
        private System.Windows.Forms.Button buttnnvfst;
        private System.Windows.Forms.Button buttnnxtlft;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Label lblno2;
        private System.Windows.Forms.Label lblno1;
        private System.Windows.Forms.Panel fraitem;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DataGridView HFG3;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox txtvendor;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TextBox txtacno;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox txtifsc;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox txtbranch;
        protected System.Windows.Forms.Label label28;
        private System.Windows.Forms.TextBox txtbank;
        private System.Windows.Forms.TextBox txtbracnid;
        private System.Windows.Forms.ComboBox cbostate;
        private System.Windows.Forms.TextBox Txtscr4;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.DataGridView HFGT;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Panel grSearch;
        private System.Windows.Forms.Button button18;
        private System.Windows.Forms.Button btnHide;
        private System.Windows.Forms.DataGridView DataGridCommon;
    }
}