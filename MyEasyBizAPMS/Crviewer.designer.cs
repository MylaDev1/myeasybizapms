﻿namespace MyEasyBizAPMS
{
    partial class Crviewer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        //private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.directoryEntry1 = new System.DirectoryServices.DirectoryEntry();
            this.Cryview = new CrystalDecisions.Windows.Forms.CrystalReportViewer();
            this.btnexit = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // Cryview
            // 
            this.Cryview.ActiveViewIndex = -1;
            this.Cryview.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Cryview.Cursor = System.Windows.Forms.Cursors.Default;
            this.Cryview.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Cryview.Location = new System.Drawing.Point(0, 0);
            this.Cryview.Name = "Cryview";
            this.Cryview.Size = new System.Drawing.Size(1214, 443);
            this.Cryview.TabIndex = 0;
            this.Cryview.Load += new System.EventHandler(this.Cryview_Load);
            // 
            // btnexit
            // 
            this.btnexit.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnexit.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            //this.btnexit.Image = global::MyEasyBizAPMS.Properties.Resources.eee;
            this.btnexit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnexit.Location = new System.Drawing.Point(722, 6);
            this.btnexit.Name = "btnexit";
            this.btnexit.Size = new System.Drawing.Size(68, 35);
            this.btnexit.TabIndex = 87;
            this.btnexit.Text = "Exit";
            this.btnexit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnexit.UseVisualStyleBackColor = false;
            this.btnexit.Click += new System.EventHandler(this.btnexit_Click);
            // 
            // Crviewer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1214, 443);
            this.Controls.Add(this.btnexit);
            this.Controls.Add(this.Cryview);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Crviewer";
            this.Text = "Crviewer";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Crviewer_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.DirectoryServices.DirectoryEntry directoryEntry1;
        private CrystalDecisions.Windows.Forms.CrystalReportViewer crystalReportViewer1;
        private System.Windows.Forms.Button btnexit;
    }
}