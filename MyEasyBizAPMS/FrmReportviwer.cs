﻿using CrystalDecisions.CrystalReports.Engine;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace MyEasyBizAPMS
{
    public partial class FrmReportviwer : Form
    {
        public FrmReportviwer()
        {
            InitializeComponent();
        }
        SqlConnection connection = new SqlConnection(GeneralParameters.ConnectionString);
        private void FrmReportviwer_Load(object sender, EventArgs e)
        {
            if(GeneralParameters.ReportName == "OrderSheet")
            {
                PrintOrderSheet();
            }
            else if (GeneralParameters.ReportName == "FabricBOM")
            {
                PrintFabricBOM();
            }
            else if (GeneralParameters.ReportName == "MaterialBOM")
            {
                PrintMaterialBOM();
            }
            else if (GeneralParameters.ReportName == "Budget")
            {
                PrintBudget();
            }
            else if (GeneralParameters.ReportName == "Garment PPM")
            {
                PrintGarmentPPM();
            }
            else if (GeneralParameters.ReportName == "BillPass")
            {
                PintBillPass();

            }
            else if (GeneralParameters.ReportName == "BillPass Trims")
            {
                PintBillPasstrims();

            }

            else if (GeneralParameters.ReportName == "ProcessBillPass")
            {
                PintProcessBillPass();

            }
        }
        private void PintProcessBillPass()
        {
            try
            {
                SQLDBHelper db = new SQLDBHelper();
                SqlParameter[] sqlParameters = { new SqlParameter("@UID", GeneralParameters.ReportUid) };
                DataSet dataSet = db.GetDataWithParamMultiple(CommandType.StoredProcedure, "PROC_RPTPROCESSBILLPASSDOCUMENT", sqlParameters, connection);
                DataTable dataTable = new DataTable();
                DataTable dtValue = new DataTable();
                DataTable dtVal = new DataTable();
                dataTable = dataSet.Tables[0];
                dtValue = dataSet.Tables[1];
                dtVal = dataSet.Tables[2];
                ReportDocument doc = new ReportDocument();
                doc.Load(Application.StartupPath + "\\CryPurchaseProcessBillPass.rpt");
                TextObject txtCompany = (TextObject)doc.ReportDefinition.ReportObjects["txtCompany"];
                txtCompany.Text = GeneralParameters.dtCompany.Rows[0]["CName"].ToString();
                TextObject txtAddress = (TextObject)doc.ReportDefinition.ReportObjects["txtAddress"];
                txtAddress.Text = GeneralParameters.dtCompany.Rows[0]["Adress"].ToString();

                TextObject txtBasicValue = (TextObject)doc.ReportDefinition.ReportObjects["txtBasicValue"];
                txtBasicValue.Text = Convert.ToDecimal(dtValue.Rows[0]["BASICVALUE"].ToString()).ToString("0.00");
                //TextObject txtDisValue = (TextObject)doc.ReportDefinition.ReportObjects["txtDisValue"];
                //txtDisValue.Text = Convert.ToDecimal(dtValue.Rows[0]["DISVAL"].ToString()).ToString("0.00");

                TextObject txtOtherCharges = (TextObject)doc.ReportDefinition.ReportObjects["txtOtherCharges"];
                txtOtherCharges.Text = Convert.ToDecimal(dtValue.Rows[0]["BEFORECHARGES"].ToString()).ToString("0.00");
                TextObject txtTabableValue = (TextObject)doc.ReportDefinition.ReportObjects["txtTabableValue"];
                txtTabableValue.Text = Convert.ToDecimal(dtValue.Rows[0]["TAXABLEVALUE"].ToString()).ToString("0.00");

                TextObject txtACharges = (TextObject)doc.ReportDefinition.ReportObjects["txtACharges"];
                txtACharges.Text = Convert.ToDecimal(dtValue.Rows[0]["ADDITIONALCHARGSE"].ToString()).ToString("0.00");
                TextObject txtSGST = (TextObject)doc.ReportDefinition.ReportObjects["txtSGST"];
                txtSGST.Text = Convert.ToDecimal(dtValue.Rows[0]["SGSTVAL"].ToString()).ToString("0.00");

                TextObject txtCGST = (TextObject)doc.ReportDefinition.ReportObjects["txtCGST"];
                txtCGST.Text = Convert.ToDecimal(dtValue.Rows[0]["CGSTVAL"].ToString()).ToString("0.00");
                TextObject txtRoff = (TextObject)doc.ReportDefinition.ReportObjects["txtRoff"];
                txtRoff.Text = Convert.ToDecimal(dtValue.Rows[0]["ROFF"].ToString()).ToString("0.00");

                TextObject txtNetValue = (TextObject)doc.ReportDefinition.ReportObjects["txtNetValue"];
                txtNetValue.Text = Convert.ToDecimal(dtValue.Rows[0]["NETVALUE"].ToString()).ToString("0.00");
                if (dtVal.Rows.Count > 0)
                {
                    TextObject txtnote = (TextObject)doc.ReportDefinition.ReportObjects["txtnote"];
                    txtnote.Text = dtVal.Rows[0]["Note"].ToString();
                }

                doc.SetDataSource(dataTable);
                CrystalReportViewer.ReportSource = doc;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private void PrintGarmentPPM()
        {
            try
            {
                SQLDBHelper db = new SQLDBHelper();
                SqlParameter[] parameters = { new SqlParameter("@OrderMUid", GeneralParameters.ReportUid.ToString()) };
                SqlParameter[] parameters1 = { new SqlParameter("@OrderMUid", GeneralParameters.ReportUid.ToString()) };
                SqlParameter[] parameters2 = { new SqlParameter("@OrderMUid", GeneralParameters.ReportUid.ToString()) };
                db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_RptOrderSheet", parameters, connection);
                db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_RptOrderSheerAssortforPPM", parameters1, connection);
                DataSet ds = db.GetDataWithParamMultiple(CommandType.StoredProcedure, "SP_PrintOrderSheet", parameters2, connection);
                DataTable dtOrderSheetSize = ds.Tables[1];
                SqlParameter[] sqlParameters3 = { new SqlParameter("@OrderMuid", GeneralParameters.ReportUid) };
                DataSet dsPPm = db.GetDataWithParamMultiple(CommandType.StoredProcedure, "Proc_OrderMGarmentPPM", sqlParameters3, connection);
                DataTable dtPack = dsPPm.Tables[0];
                DataTable dtQuantity = dsPPm.Tables[1];
                DataTable dtFabric = dsPPm.Tables[2];
                DataTable dtTrims = dsPPm.Tables[3];

                ReportDocument rpt = new ReportDocument();
                rpt.Load(Application.StartupPath + "\\Reports\\CryGarment.rpt");
                string Size = string.Empty;

                for (int i = 0; i < dtOrderSheetSize.Rows.Count; i++)
                {
                    if (i == 0)
                    {
                        Size = dtOrderSheetSize.Rows[i]["SizeName"].ToString();
                    }
                    else
                    {
                        Size += "," + dtOrderSheetSize.Rows[i]["SizeName"].ToString();
                    }
                }
                string[] Columns = Size.Split(',');
                for (int i = 0; i < Columns.Length; i++)
                {
                    string txtNameaSSORT = "txt" + (i + 1);
                    TextObject txtpentotal = (TextObject)rpt.Subreports["CryGarmentQty.rpt"].ReportDefinition.Sections["Section1"].ReportObjects[txtNameaSSORT];
                    txtpentotal.Text = Columns[i].ToString();
                }
                rpt.Subreports["CryGarmentPacks.rpt"].SetDataSource(dtPack);
                rpt.Subreports["CryGarmentQty.rpt"].SetDataSource(dtQuantity);
                rpt.Subreports["CryGarmentFabric.rpt"].SetDataSource(dtFabric);
                rpt.Subreports["CryGarmentTrims.rpt"].SetDataSource(dtTrims);
                TextObject txtCompany = (TextObject)rpt.ReportDefinition.ReportObjects["txtCompany"];
                txtCompany.Text = GeneralParameters.dtCompany.Rows[0]["CName"].ToString();
                TextObject txtAddress = (TextObject)rpt.ReportDefinition.ReportObjects["txtAddress"];
                txtAddress.Text = GeneralParameters.dtCompany.Rows[0]["Adress"].ToString();
                TextObject txtGST = (TextObject)rpt.ReportDefinition.ReportObjects["txtGST"];
                txtGST.Text = GeneralParameters.dtCompany.Rows[0]["GSTNumber"].ToString();
                rpt.SetDataSource(dtPack);
                CrystalReportViewer.ReportSource = rpt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void PrintBudget()
        {
            try
            {
                SQLDBHelper db = new SQLDBHelper();
                SqlParameter[] sqlParameters = { new SqlParameter("@OrderMUid", GeneralParameters.ReportUid) };
                DataTable dataTable = db.GetDataWithParam(CommandType.StoredProcedure, "Proc_OrderMBudjetPrint", sqlParameters, connection);
                SqlParameter[] sqlParameters2 = { new SqlParameter("@OrderMUid", GeneralParameters.ReportUid) };
                DataTable dataTableCost = db.GetDataWithParam(CommandType.StoredProcedure, "Proc_OrderMBudgetcost", sqlParameters2, connection);
                if (dataTableCost.Rows.Count == 1)
                {
                    MessageBox.Show(dataTableCost.Rows[0]["ResponseMessage"].ToString(), "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                SqlParameter[] sqlParameters1 = { new SqlParameter("@Uid", GeneralParameters.ReportUid) };
                DataSet dataSet  = db.GetDataWithParamMultiple(CommandType.StoredProcedure, "Proc_GetOrderInfo", sqlParameters1, connection);
                DataTable dtOrderInfo = dataSet.Tables[0];
                DataTable dtQtyInfo = dataSet.Tables[1];
                DataTable dtGSM = dataSet.Tables[2];
                DataTable dtStyle = dataSet.Tables[3];
                object TotalExpense = dataTable.Compute("Sum(Value)", string.Empty);
                decimal profitLoss = (Convert.ToDecimal(dtOrderInfo.Rows[0]["GrossValue"].ToString()) - Convert.ToDecimal(TotalExpense));
                decimal profitLossPcs = profitLoss / Convert.ToDecimal(dtQtyInfo.Rows[0]["PlanQty"].ToString());
                decimal CostPerGrment = Convert.ToDecimal(TotalExpense) / Convert.ToDecimal(dtQtyInfo.Rows[0]["PlanQty"].ToString());
                decimal profitLossPercentage = (profitLoss / Convert.ToDecimal(dtOrderInfo.Rows[0]["GrossValue"].ToString()) * 100);
                ReportDocument rpt = new ReportDocument();
                rpt.Load(Application.StartupPath + "\\Reports\\CryBudgetReport.rpt");

                TextObject txtOrderNo = (TextObject)rpt.ReportDefinition.ReportObjects["txtOrderNo"];
                txtOrderNo.Text =dtOrderInfo.Rows[0]["DocNo"].ToString();

                TextObject txtCustomer = (TextObject)rpt.ReportDefinition.ReportObjects["txtCustomer"];
                txtCustomer.Text = dtOrderInfo.Rows[0]["CompanyName"].ToString();

                TextObject txtsalesValue = (TextObject)rpt.ReportDefinition.ReportObjects["txtsalesValue"];
                txtsalesValue.Text = Convert.ToDecimal(dtOrderInfo.Rows[0]["GrossValue"].ToString()).ToString("0.00");

                TextObject txtDelDate = (TextObject)rpt.ReportDefinition.ReportObjects["txtDelDate"];
                txtDelDate.Text = Convert.ToDateTime(dtOrderInfo.Rows[0]["DeliveryDt"].ToString()).ToString("dd-MMM-yyyy");

                TextObject txtCurrency = (TextObject)rpt.ReportDefinition.ReportObjects["txtCurrency"];
                txtCurrency.Text = dtGSM.Rows[0]["GSM"].ToString();

                //TextObject txtExrate = (TextObject)rpt.ReportDefinition.ReportObjects["txtExrate"];
                //txtExrate.Text = Convert.ToDecimal(dtOrderInfo.Rows[0]["ExRate"].ToString()).ToString("0.00");

                TextObject txtOrderQty = (TextObject)rpt.ReportDefinition.ReportObjects["txtOrderQty"];
                txtOrderQty.Text = dtQtyInfo.Rows[0]["OrderPlanQty"].ToString();

                TextObject txtPlanQty = (TextObject)rpt.ReportDefinition.ReportObjects["txtPlanQty"];
                txtPlanQty.Text = Convert.ToDecimal(dtQtyInfo.Rows[0]["PlanQty"].ToString()).ToString("0");

                TextObject txtTotalIncome = (TextObject)rpt.ReportDefinition.ReportObjects["txtTotalIncome"];
                txtTotalIncome.Text = Convert.ToDecimal(dtOrderInfo.Rows[0]["GrossValue"].ToString()).ToString("0.00");

                TextObject txtTotalExpenses = (TextObject)rpt.ReportDefinition.ReportObjects["txtTotalExpenses"];
                txtTotalExpenses.Text = Convert.ToDecimal(TotalExpense).ToString("0.00");

                TextObject txtProfitLoss = (TextObject)rpt.ReportDefinition.ReportObjects["txtProfitLoss"];
                txtProfitLoss.Text = profitLoss.ToString("0.00");

                TextObject txtProfitLossPcs = (TextObject)rpt.ReportDefinition.ReportObjects["txtProfitLossPcs"];
                txtProfitLossPcs.Text = profitLossPcs.ToString("0.00");

                TextObject txtCostPerGorment = (TextObject)rpt.ReportDefinition.ReportObjects["txtCostPerGorment"];
                txtCostPerGorment.Text = CostPerGrment.ToString("0.00");

                TextObject txtProfitLossPercentage = (TextObject)rpt.ReportDefinition.ReportObjects["txtProfitLossPercentage"];
                txtProfitLossPercentage.Text = profitLossPercentage.ToString("0.00");
                TextObject txtStyle = (TextObject)rpt.ReportDefinition.ReportObjects["txtStyle"];
                txtStyle.Text = dtStyle.Rows[0]["StyleName"].ToString();
                rpt.Subreports["CryBudgetCostSub.rpt"].SetDataSource(dataTableCost);

                rpt.SetDataSource(dataTable);
                CrystalReportViewer.ReportSource = rpt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void PrintMaterialBOM()
        {
            try
            {
                SQLDBHelper db = new SQLDBHelper();
                SqlParameter[] sqlParameters = { new SqlParameter("@OrderMUid", GeneralParameters.ReportUid) };
                DataTable dataTable = db.GetDataWithParam(CommandType.StoredProcedure, "Proc_MaterialBomTrimsReport", sqlParameters, connection);
                ReportDocument rpt = new ReportDocument();
                rpt.Load(Application.StartupPath + "\\Reports\\CryTrimsRequirenment.rpt");
                rpt.SetDataSource(dataTable);
                CrystalReportViewer.ReportSource = rpt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void PrintFabricBOM()
        {
            try
            {
                SQLDBHelper db = new SQLDBHelper();
                SqlParameter[] sqlParameters = { new SqlParameter("@OrrderMuid", GeneralParameters.ReportUid) };
                DataSet dataSet = db.GetDataWithParamMultiple(CommandType.StoredProcedure, "Proc_PrintFabricBomNewRpt", sqlParameters, connection);
                //DataTable dataTable = db.GetDataWithParam(CommandType.StoredProcedure, "Proc_PrintFabricBomNewRpt", sqlParameters, connection);
                //SqlParameter[] sqlParameters1 = { new SqlParameter("@OrderMUid", GeneralParameters.ReportUid) };
                //DataTable dataTable1 = db.GetDataWithParam(CommandType.StoredProcedure, "Proc_OrderMFabricProcessReport", sqlParameters1, connection);
                //SqlParameter[] sqlParameters2 = { new SqlParameter("@OrderMUid", GeneralParameters.ReportUid) };
                ////DataTable dataTable2 = db.GetDataWithParam(CommandType.StoredProcedure, "Proc_OrderMYarnReqReport", sqlParameters2, connection);
                //DataTable dataTable2 = db.GetDataWithParam(CommandType.StoredProcedure, "Proc_OrderMYarnReqReportNEW", sqlParameters2, connection);
                //SqlParameter[] sqlParameters3 = { new SqlParameter("@OrderMuid", GeneralParameters.ReportUid) };
                //DataTable dtYarnprocess = db.GetDataWithParam(CommandType.StoredProcedure, "Proc_YarnProcessReport", sqlParameters3, connection);
                DataTable dataTable = dataSet.Tables[0];
                DataTable dataTable1 = dataSet.Tables[1];
                DataTable dataTable2 = dataSet.Tables[2];
                DataTable dtYarnprocess = dataSet.Tables[3];
         


                ReportDocument rpt = new ReportDocument();
                rpt.Load(Application.StartupPath + "\\Reports\\CryFabricBom.rpt");
            
                decimal StyleQty;
                decimal PlanQty;
                decimal Excess;
                decimal Approvel;
                //decimal RejAll;
                if (dataTable.Rows.Count == 1)
                {
                    StyleQty = Convert.ToDecimal(dataTable.Rows[0]["StyleQty"].ToString());
                    PlanQty = Convert.ToDecimal(dataTable.Rows[0]["PlanQty"].ToString());
                    Excess = Convert.ToDecimal(dataTable.Rows[0]["Excess"].ToString());
                    Approvel = Convert.ToDecimal(dataTable.Rows[0]["BuyerTolerance"].ToString());
                }
                else
                {
                    StyleQty = Convert.ToDecimal(dataTable.Rows[0]["StyleQty"].ToString());
                    Excess = Convert.ToDecimal(dataTable.Compute("Sum(Excess)", string.Empty));
                    PlanQty = Convert.ToDecimal(dataTable.Compute("Sum(PlanQty)", string.Empty));
                    Approvel = Convert.ToDecimal(dataTable.Rows[0]["BuyerTolerance"].ToString());
                }
                TextObject txtOrderNo = (TextObject)rpt.ReportDefinition.ReportObjects["txtOrderNo"];
                txtOrderNo.Text = dataTable.Rows[0]["OrderNo"].ToString();

                TextObject txtStyleRef = (TextObject)rpt.ReportDefinition.ReportObjects["txtStyleRef"];
                txtStyleRef.Text = dataTable.Rows[0]["Styledescription"].ToString();

                TextObject txtStyle = (TextObject)rpt.ReportDefinition.ReportObjects["txtStyle"];
                txtStyle.Text = dataTable.Rows[0]["StyleName"].ToString();

                TextObject txtOrderQty = (TextObject)rpt.ReportDefinition.ReportObjects["txtOrderQty"];
                txtOrderQty.Text = StyleQty.ToString("0");

                TextObject txtPlanQty = (TextObject)rpt.ReportDefinition.ReportObjects["txtPlanQty"];
                txtPlanQty.Text = PlanQty.ToString("0");

                TextObject txtExcess = (TextObject)rpt.ReportDefinition.ReportObjects["txtExcess"];
                txtExcess.Text = Excess.ToString("0");

                TextObject txtApprovel = (TextObject)rpt.ReportDefinition.ReportObjects["txtApprovel"];
                txtApprovel.Text = Approvel.ToString("0");

                TextObject txtUnit = (TextObject)rpt.ReportDefinition.ReportObjects["txtUnit"];
                txtUnit.Text = dataTable.Rows[0]["GeneralName"].ToString();

                //TextObject txtRejAll = (TextObject)rpt.ReportDefinition.ReportObjects["txtRejAll"];
                //txtRejAll.Text = Convert.ToDecimal(dataTable.Rows[0]["PlanQty"].ToString()).ToString("0");

                rpt.Subreports["CryFabricBoMYarnReq.rpt"].SetDataSource(dataTable2);
                if(dtYarnprocess.Rows.Count > 0)
                {
                    rpt.Subreports["CryYarnProcess.rpt"].SetDataSource(dtYarnprocess);
                }
                else
                {
                    rpt.ReportDefinition.Sections[3].SectionFormat.EnableSuppress = true;
                }
                rpt.Subreports["CryFabricBOMItem.rpt"].SetDataSource(dataTable1);
                rpt.SetDataSource(dataTable);
                CrystalReportViewer.ReportSource = rpt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void PrintOrderSheet()
        {
            try
            {
                SQLDBHelper db = new SQLDBHelper();
                SqlParameter[] parameters = { new SqlParameter("@OrderMUid", GeneralParameters.ReportUid.ToString()) };
                SqlParameter[] parameters1 = { new SqlParameter("@OrderMUid", GeneralParameters.ReportUid.ToString()) };
                SqlParameter[] parameters2 = { new SqlParameter("@OrderMUid", GeneralParameters.ReportUid.ToString()) };
                db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_RptOrderSheet", parameters, connection);
                db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_RptOrderSheerAssort", parameters1, connection);
                DataSet ds = db.GetDataWithParamMultiple(CommandType.StoredProcedure, "SP_PrintOrderSheet", parameters2, connection);
                SqlParameter[] sqlParameters1 = { new SqlParameter("@Uid", GeneralParameters.ReportUid) };
                DataSet dataSet = db.GetDataWithParamMultiple(CommandType.StoredProcedure, "Proc_GetOrderInfo", sqlParameters1, connection);
                DataTable dtOrderInfo = dataSet.Tables[0];
                DataTable dtQtyInfo = dataSet.Tables[1];
                DataTable dtOrderSheet = ds.Tables[0];
                DataTable dtOrderSheetSize = ds.Tables[1];
                DataTable dtOrderSheetAssort = ds.Tables[2];
                DataTable dtOrderSheetAssortSize = ds.Tables[3];
                DataTable dtFabric = ds.Tables[4];
                decimal PlanQty = Convert.ToDecimal(dtQtyInfo.Rows[0]["PlanQty"].ToString());
                ReportDocument rpt = new ReportDocument();
                rpt.Load(Application.StartupPath + "\\Reports\\CryOrderEntry.rpt");
                
                string Size = string.Empty;
                string SizeAssort = string.Empty;

                for (int i = 0; i < dtOrderSheetSize.Rows.Count; i++)
                {
                    if(i == 0)
                    {
                        Size = dtOrderSheetSize.Rows[i]["SizeName"].ToString();
                    }
                    else
                    {
                        Size += "," + dtOrderSheetSize.Rows[i]["SizeName"].ToString();
                    }
                    
                }
                for (int i = 0; i < dtOrderSheetAssortSize.Rows.Count; i++)
                {
                    if (i == 0)
                    {
                        SizeAssort = dtOrderSheetAssortSize.Rows[i]["SizeName"].ToString();
                    }
                    else
                    {
                        SizeAssort += "," + dtOrderSheetAssortSize.Rows[i]["SizeName"].ToString();
                    }
                }
                string[] Columns = Size.Split(',');
                string[] ColumnsAssort = SizeAssort.Split(',');
                for (int i = 0; i < Columns.Length; i++)
                {
                    string txtName = "txt" + (i + 1);
                    TextObject txtNoOfBales = (TextObject)rpt.ReportDefinition.ReportObjects[txtName];
                    txtNoOfBales.Text = Columns[i].ToString();
                }
                for (int i = 0; i < ColumnsAssort.Length; i++)
                {
                    string txtNameaSSORT = "txtA" + (i + 1);
                    TextObject txtpentotal = (TextObject)rpt.Subreports["CryOrderSubReport.rpt"].ReportDefinition.Sections["Section1"].ReportObjects[txtNameaSSORT];
                    txtpentotal.Text = ColumnsAssort[i].ToString();
                }
                string fabric = string.Empty;
                for (int i = 0; i < dtFabric.Rows.Count; i++)
                {
                    if(i == 0)
                    {
                        decimal gsm;
                        if (string.IsNullOrEmpty(dtFabric.Rows[i]["GSM"].ToString()))
                        {
                            gsm = 0;
                        }
                        else
                        {
                            gsm = Convert.ToDecimal(dtFabric.Rows[i]["GSM"].ToString());
                        }
                        fabric = dtFabric.Rows[i]["StuructureType"].ToString() + " - GSM " + gsm.ToString("0");
                    }
                    else
                    {
                        decimal gsm;
                        if (string.IsNullOrEmpty(dtFabric.Rows[i]["GSM"].ToString()))
                        {
                            gsm = 0;
                        }
                        else
                        {
                            gsm = Convert.ToDecimal(dtFabric.Rows[i]["GSM"].ToString());
                        }
                        fabric = fabric + "," + dtFabric.Rows[i]["StuructureType"].ToString() + " - GSM " + gsm.ToString("0");
                    }
                }
                TextObject txtFabric = (TextObject)rpt.ReportDefinition.ReportObjects["txtFabric"];
                txtFabric.Text = fabric;
                TextObject txtCompany = (TextObject)rpt.ReportDefinition.ReportObjects["txtCompany"];
                txtCompany.Text = GeneralParameters.dtCompany.Rows[0]["CName"].ToString();
                TextObject txtAddress = (TextObject)rpt.ReportDefinition.ReportObjects["txtAddress"];
                txtAddress.Text = GeneralParameters.dtCompany.Rows[0]["Adress"].ToString();
                TextObject txtGST = (TextObject)rpt.ReportDefinition.ReportObjects["txtGST"];
                txtGST.Text = GeneralParameters.dtCompany.Rows[0]["GSTNumber"].ToString();
                //TextObject txtPlanQty = (TextObject)rpt.ReportDefinition.ReportObjects["txtPlanQty"];
                //txtPlanQty.Text = PlanQty.ToString("0");
                rpt.Subreports["CryOrderSubReport.rpt"].SetDataSource(dtOrderSheetAssort);
                rpt.SetDataSource(dtOrderSheet);
                CrystalReportViewer.ReportSource = rpt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private void PintBillPass()
        {
            try
            {
                SQLDBHelper db = new SQLDBHelper();
                SqlParameter[] sqlParameters = { new SqlParameter("@UID", GeneralParameters.ReportUid) };
                DataSet dataSet = db.GetDataWithParamMultiple(CommandType.StoredProcedure, "PROC_RPTBILLPASSDOCUMENT", sqlParameters, connection);
                DataTable dataTable = new DataTable();
                DataTable dtValue = new DataTable();
                dataTable = dataSet.Tables[0];
                dtValue = dataSet.Tables[1];
                ReportDocument doc = new ReportDocument();
                doc.Load(Application.StartupPath + "\\Reports\\CryPurchaseBillPass.rpt");
                TextObject txtCompany = (TextObject)doc.ReportDefinition.ReportObjects["txtCompany"];
                txtCompany.Text = GeneralParameters.dtCompany.Rows[0]["CName"].ToString();
                TextObject txtAddress = (TextObject)doc.ReportDefinition.ReportObjects["txtAddress"];
                txtAddress.Text = GeneralParameters.dtCompany.Rows[0]["Adress"].ToString();

                TextObject txtBasicValue = (TextObject)doc.ReportDefinition.ReportObjects["txtBasicValue"];
                txtBasicValue.Text = Convert.ToDecimal(dtValue.Rows[0]["BASICVALUE"].ToString()).ToString("0.00");
                TextObject txtDisValue = (TextObject)doc.ReportDefinition.ReportObjects["txtDisValue"];
                txtDisValue.Text = Convert.ToDecimal(dtValue.Rows[0]["DISVAL"].ToString()).ToString("0.00");

                TextObject txtOtherCharges = (TextObject)doc.ReportDefinition.ReportObjects["txtOtherCharges"];
                txtOtherCharges.Text = Convert.ToDecimal(dtValue.Rows[0]["BEFORECHARGES"].ToString()).ToString("0.00");
                TextObject txtTabableValue = (TextObject)doc.ReportDefinition.ReportObjects["txtTabableValue"];
                txtTabableValue.Text = Convert.ToDecimal(dtValue.Rows[0]["TAXABLEVALUE"].ToString()).ToString("0.00");

                TextObject txtACharges = (TextObject)doc.ReportDefinition.ReportObjects["txtACharges"];
                txtACharges.Text = Convert.ToDecimal(dtValue.Rows[0]["ADDITIONALCHARGSE"].ToString()).ToString("0.00");
                TextObject txtSGST = (TextObject)doc.ReportDefinition.ReportObjects["txtSGST"];
                txtSGST.Text = Convert.ToDecimal(dtValue.Rows[0]["SGSTVAL"].ToString()).ToString("0.00");

                TextObject txtCGST = (TextObject)doc.ReportDefinition.ReportObjects["txtCGST"];
                txtCGST.Text = Convert.ToDecimal(dtValue.Rows[0]["CGSTVAL"].ToString()).ToString("0.00");
                TextObject txtRoff = (TextObject)doc.ReportDefinition.ReportObjects["txtRoff"];
                txtRoff.Text = Convert.ToDecimal(dtValue.Rows[0]["ROFF"].ToString()).ToString("0.00");

                TextObject txtNetValue = (TextObject)doc.ReportDefinition.ReportObjects["txtNetValue"];
                txtNetValue.Text = Convert.ToDecimal(dtValue.Rows[0]["NETVALUE"].ToString()).ToString("0.00");

                doc.SetDataSource(dataTable);
                CrystalReportViewer.ReportSource = doc;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private void PintBillPasstrims()
        {
            try
            {
                SQLDBHelper db = new SQLDBHelper();
                SqlParameter[] sqlParameters = { new SqlParameter("@UID", GeneralParameters.ReportUid) };
                DataSet dataSet = db.GetDataWithParamMultiple(CommandType.StoredProcedure, "PROC_RPTBILLPASSDOCUMENTTRIMS", sqlParameters, connection);
                DataTable dataTable = new DataTable();
                DataTable dtValue = new DataTable();
                dataTable = dataSet.Tables[0];
                dtValue = dataSet.Tables[1];
                ReportDocument doc = new ReportDocument();
                doc.Load(Application.StartupPath + "\\Reports\\CryPurchaseBillPass.rpt");
                TextObject txtCompany = (TextObject)doc.ReportDefinition.ReportObjects["txtCompany"];
                txtCompany.Text = GeneralParameters.dtCompany.Rows[0]["CName"].ToString();
                TextObject txtAddress = (TextObject)doc.ReportDefinition.ReportObjects["txtAddress"];
                txtAddress.Text = GeneralParameters.dtCompany.Rows[0]["Adress"].ToString();

                TextObject txtBasicValue = (TextObject)doc.ReportDefinition.ReportObjects["txtBasicValue"];
                txtBasicValue.Text = Convert.ToDecimal(dtValue.Rows[0]["BASICVALUE"].ToString()).ToString("0.00");
                TextObject txtDisValue = (TextObject)doc.ReportDefinition.ReportObjects["txtDisValue"];
                txtDisValue.Text = Convert.ToDecimal(dtValue.Rows[0]["DISVAL"].ToString()).ToString("0.00");

                TextObject txtOtherCharges = (TextObject)doc.ReportDefinition.ReportObjects["txtOtherCharges"];
                txtOtherCharges.Text = Convert.ToDecimal(dtValue.Rows[0]["BEFORECHARGES"].ToString()).ToString("0.00");
                TextObject txtTabableValue = (TextObject)doc.ReportDefinition.ReportObjects["txtTabableValue"];
                txtTabableValue.Text = Convert.ToDecimal(dtValue.Rows[0]["TAXABLEVALUE"].ToString()).ToString("0.00");

                TextObject txtACharges = (TextObject)doc.ReportDefinition.ReportObjects["txtACharges"];
                txtACharges.Text = Convert.ToDecimal(dtValue.Rows[0]["ADDITIONALCHARGSE"].ToString()).ToString("0.00");
                TextObject txtSGST = (TextObject)doc.ReportDefinition.ReportObjects["txtSGST"];
                txtSGST.Text = Convert.ToDecimal(dtValue.Rows[0]["SGSTVAL"].ToString()).ToString("0.00");

                TextObject txtCGST = (TextObject)doc.ReportDefinition.ReportObjects["txtCGST"];
                txtCGST.Text = Convert.ToDecimal(dtValue.Rows[0]["CGSTVAL"].ToString()).ToString("0.00");
                TextObject txtRoff = (TextObject)doc.ReportDefinition.ReportObjects["txtRoff"];
                txtRoff.Text = Convert.ToDecimal(dtValue.Rows[0]["ROFF"].ToString()).ToString("0.00");

                TextObject txtNetValue = (TextObject)doc.ReportDefinition.ReportObjects["txtNetValue"];
                txtNetValue.Text = Convert.ToDecimal(dtValue.Rows[0]["NETVALUE"].ToString()).ToString("0.00");

                doc.SetDataSource(dataTable);
                CrystalReportViewer.ReportSource = doc;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private void CrystalReportViewer_Load(object sender, EventArgs e)
        {

        }
    }
}
