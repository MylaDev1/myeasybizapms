﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Windows.Forms;

namespace MyEasyBizAPMS
{
    public partial class FrmYarnSubMast : Form
    {
        public FrmYarnSubMast()
        {
            InitializeComponent();
            this.sfDataGridYarn.Style.HeaderStyle.Font.FontStyle = FontStyle.Bold;
            this.sfDataGridYarn.Style.HeaderStyle.BackColor = Color.DeepSkyBlue;
        }
        int TypeMUid = 0;
        SQLDBHelper db = new SQLDBHelper();
        BindingSource bsPLy = new BindingSource();
        SqlConnection conn = new SqlConnection(GeneralParameters.ConnectionString);
        private void FrmYarnSubMast_Load(object sender, EventArgs e)
        {
            Genclass.buttonstyleform(this);
            Genclass.buttonstylepanel(panadd);
            chckAc.Checked = true;
            LoadButton(0);
            if (this.Text == "Yarn Item Type")
            {
                TypeMUid = 1;
                lbltaxPer.Visible = false;
                txtTaxPercentage.Visible = false;
                lblPer.Visible = false;
            }
            else if (this.Text == "Count")
            {
                TypeMUid = 2;
                lbltaxPer.Visible = false;
                txtTaxPercentage.Visible = false;
                lblPer.Visible = false;
            }
            else if (this.Text == "Yarn Type")
            {
                TypeMUid = 3;
                lbltaxPer.Visible = false;
                txtTaxPercentage.Visible = false;
                lblPer.Visible = false;
            }
            else if (this.Text == "Category")
            {
                TypeMUid = 4;
                lbltaxPer.Visible = false;
                txtTaxPercentage.Visible = false;
                lblPer.Visible = false;
            }
            else if (this.Text == "Blend")
            {
                TypeMUid = 5;
                lbltaxPer.Visible = false;
                txtTaxPercentage.Visible = false;
                lblPer.Visible = false;
            }
            else if (this.Text == "Purity")
            {
                TypeMUid = 14;
                lbltaxPer.Visible = false;
                txtTaxPercentage.Visible = false;
                lblPer.Visible = false;
            }
            else if (this.Text == "UOM")
            {
                TypeMUid = 7;
                lbltaxPer.Visible = false;
                txtTaxPercentage.Visible = false;
                lblPer.Visible = false;
            }
            else if (this.Text == "Tax")
            {
                TypeMUid = 8;
                lbltaxPer.Visible = true;
                txtTaxPercentage.Visible = true;
                lblPer.Visible = true;
            }
            else if (this.Text == "Season")
            {
                TypeMUid = 16;
                lbltaxPer.Visible = true;
                txtTaxPercentage.Visible = true;
                lblPer.Visible = true;
            }
            else if (this.Text == "Merchindiser")
            {
                TypeMUid = 16;
                lbltaxPer.Visible = true;
                txtTaxPercentage.Visible = true;
                lblPer.Visible = true;
            }
            else if (this.Text == "OrderStatus")
            {
                TypeMUid = 16;
                lbltaxPer.Visible = true;
                txtTaxPercentage.Visible = true;
                lblPer.Visible = true;
            }
            else if (this.Text == "OrderType")
            {
                TypeMUid = 16;
                lbltaxPer.Visible = true;
                txtTaxPercentage.Visible = true;
                lblPer.Visible = true;
            }
            LoadDatatable();
        }

        protected void LoadButton(int id)
        {
            try
            {
                if (id == 0)
                {
                    grBack.Visible = false;
                    grFront.Visible = true;
                    btnAdd.Visible = true;
                    btnEdit.Visible = true;
                    btnExit.Visible = true;
                    //btnFirst.Visible = true;
                    //btnBack.Visible = true;
                    //btnNxt.Visible = true;
                    //btnLast.Visible = true;
                    //PanelgridNos.Visible = true;
                    btnSave.Visible = false;
                    btnAddCancel.Visible = false;
                    btnSave.Text = "Save";
                    chckAc.Visible = true;
                }
                else if (id == 1)
                {
                    grBack.Visible = true;
                    grFront.Visible = false;
                    btnAdd.Visible = false;
                    btnEdit.Visible = false;
                    btnExit.Visible = false;
                    //btnFirst.Visible = false;
                    //btnBack.Visible = false;
                    //btnNxt.Visible = false;
                    //btnLast.Visible = false;
                    //PanelgridNos.Visible = false;
                    btnSave.Visible = true;
                    btnAddCancel.Visible = true;
                    btnSave.Text = "Save";
                    chckAc.Visible = false;
                }

                else if (id == 2)
                {
                    grBack.Visible = true;
                    grFront.Visible = false;
                    btnAdd.Visible = false;
                    btnEdit.Visible = false;
                    btnExit.Visible = false;
                    //btnFirst.Visible = false;
                    //btnBack.Visible = false;
                    //btnNxt.Visible = false;
                    //btnLast.Visible = false;
                    //PanelgridNos.Visible = false;
                    btnSave.Visible = true;
                    btnAddCancel.Visible = true;
                    btnSave.Text = "Update";
                    chckAc.Visible = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtName.Text != string.Empty)
                {
                    int Active;
                    if (chckActive.Checked == true)
                    {
                        Active = 1;
                    }
                    else
                    {
                        Active = 0;
                    }
                    decimal tax = 0;
                    if(TypeMUid == 8)
                    {
                        tax = Convert.ToDecimal(txtTaxPercentage.Text);
                    }
                    else
                    {
                        tax = 0;
                    }
                    if (btnSave.Text == "Save")
                    {
                        SqlParameter[] para = {
                            new SqlParameter("@TypeMUid",TypeMUid),
                            new SqlParameter("@GeneralName",txtName.Text),
                            new SqlParameter("@ShortName",txtShortName.Text),
                            new SqlParameter("@F1",tax),
                            new SqlParameter("@F2","0"),
                            new SqlParameter("@F3","0"),
                            new SqlParameter("@CreateDate",DateTime.Now),
                            new SqlParameter("@Active",Active),
                            new SqlParameter("@UserId",GeneralParameters.UserdId),
                            new SqlParameter("@GUid",txtName.Tag)
                        };
                        db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_GeneralM", para, conn);
                    }
                    else
                    {
                        SqlParameter[] para = {
                            new SqlParameter("@TypeMUid",TypeMUid),
                            new SqlParameter("@GeneralName",txtName.Text),
                            new SqlParameter("@ShortName",txtShortName.Text),
                            new SqlParameter("@F1",DBNull.Value),
                            new SqlParameter("@F2",DBNull.Value),
                            new SqlParameter("@F3",DBNull.Value),
                            new SqlParameter("@CreateDate",DateTime.Now),
                            new SqlParameter("@Active",Active),
                            new SqlParameter("@UserId",GeneralParameters.UserdId),
                            new SqlParameter("@GUid",txtName.Tag)
                        };
                        db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_GeneralM", para, conn);
                    }
                    MessageBox.Show("Record has been saved successfully", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtName.Text = string.Empty;
                    txtShortName.Text = string.Empty;
                    LoadDatatable();
                    LoadButton(0);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
        protected void LoadDatatable()
        {
            try
            {
                int Active;
                if (chckAc.Checked == true)
                {
                    Active = 1;
                }
                else
                {
                    Active = 0;
                }
                SqlParameter[] para = { new SqlParameter("@TypeMUid", TypeMUid), new SqlParameter("@Active", Active) };
                DataTable dt = db.GetDataWithParam(CommandType.StoredProcedure, "SP_GetGeneralM", para, conn);
                sfDataGridYarn.AutoGenerateColumns = true;
                DataTable dtnew = new DataTable();
                dtnew.Columns.Add("Name", typeof(string));
                dtnew.Columns.Add("ShortName", typeof(string));
                dtnew.Columns.Add("Active", typeof(bool));
                dtnew.Columns.Add("GUid", typeof(int));
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dtnew.NewRow();
                    row["Name"] = dt.Rows[i]["GeneralName"].ToString();
                    row["ShortName"] = dt.Rows[i]["ShortName"].ToString();
                    row["Active"] = Convert.ToBoolean(dt.Rows[i]["Active"].ToString());
                    row["GUid"] = dt.Rows[i]["GUid"].ToString();
                    dtnew.Rows.Add(row);
                }
                sfDataGridYarn.DataSource = dtnew;
                sfDataGridYarn.Columns[1].Width = 200;
                sfDataGridYarn.Columns[2].Visible = false;
                sfDataGridYarn.Columns[3].Visible = false;
                sfDataGridYarn.Columns[0].Width = 293;
                sfDataGridYarn.SelectionMode = Syncfusion.WinForms.DataGrid.Enums.GridSelectionMode.Extended;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void ChckAc_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (chckAc.Checked == true)
                {
                    LoadDatatable();
                }
                else
                {
                    LoadDatatable();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void BtnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BtnAdd_Click(object sender, EventArgs e)
        {
            LoadButton(1);
            txtName.Text = string.Empty;
            chckActive.Checked = true;
            txtName.Tag = 0;
        }

        private void BtnAddCancel_Click(object sender, EventArgs e)
        {
            LoadDatatable();
            LoadButton(0);
        }

        private void BtnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                var selectedItem = sfDataGridYarn.SelectedItems[0];
                var dataRow = (selectedItem as DataRowView).Row;
                var cellValue = dataRow["GUid"].ToString();
                string YarnName = dataRow["Name"].ToString();
                var Actve = dataRow["Active"].ToString();
                var ShortName = dataRow["ShortName"].ToString();

                txtName.Text = YarnName.ToString();
                txtName.Tag = cellValue.ToString();
                bool Active = Convert.ToBoolean(Actve.ToString());
                txtShortName.Text = ShortName;
                if (Active == false)
                {
                    chckActive.Checked = false;
                }
                else
                {
                    chckActive.Checked = true;
                }
                LoadButton(2);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void DataGridCommon_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                int i = DataGridCommon.CurrentCell.RowIndex;
                lblFrom.Text = (i + 1).ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void BtnFirst_Click(object sender, EventArgs e)
        {
            try
            {
                DataGridCommon.CurrentCell = DataGridCommon.Rows[0].Cells[1];
                DataGridCommon.Rows[0].Selected = true;
                int i = DataGridCommon.CurrentCell.RowIndex;
                lblFrom.Text = (i + 1).ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void BtnBack_Click(object sender, EventArgs e)
        {
            try
            {
                int Cnt = DataGridCommon.CurrentCell.RowIndex;
                if (Cnt > 0)
                {
                    int i = DataGridCommon.CurrentCell.RowIndex - 1;
                    DataGridCommon.CurrentCell = DataGridCommon.Rows[i].Cells[1];
                    DataGridCommon.Rows[i].Selected = true;
                    lblFrom.Text = (i + 1).ToString();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void BtnNxt_Click(object sender, EventArgs e)
        {
            try
            {
                int Cnt = DataGridCommon.CurrentCell.RowIndex + 1;
                if (Cnt >= 0 && Cnt != DataGridCommon.Rows.Count)
                {
                    int i = DataGridCommon.CurrentCell.RowIndex + 1;
                    DataGridCommon.CurrentCell = DataGridCommon.Rows[i].Cells[1];
                    DataGridCommon.Rows[i].Selected = true;
                    lblFrom.Text = (i + 1).ToString();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void BtnLast_Click(object sender, EventArgs e)
        {
            try
            {
                int i = DataGridCommon.Rows.Count - 1;
                DataGridCommon.CurrentCell = DataGridCommon.Rows[i].Cells[1];
                DataGridCommon.Rows[i].Selected = true;
                lblFrom.Text = (i + 1).ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
    }
}
