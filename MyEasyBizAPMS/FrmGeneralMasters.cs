﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace MyEasyBizAPMS
{
    public partial class FrmGeneralMasters : Form
    {
        public FrmGeneralMasters()
        {
            InitializeComponent();
        }
        int TypeMuid = 0;
        SQLDBHelper db = new SQLDBHelper();
        SqlConnection conn = new SqlConnection(GeneralParameters.ConnectionString);
        BindingSource bs = new BindingSource();
        decimal F1 = 0;
        decimal F2 = 0;
        decimal F3 = 0;
        int Active = 0;

        private void FrmGeneralMasters_Load(object sender, EventArgs e)
        {
            Genclass.buttonstyleform(this);
            Genclass.buttonstylepanel(panadd);
            LoadButton(0);
            if (this.Text == "Component")
            {
                lblCompulsary.Visible = false;
                lbltaxPer.Visible = false;
                txtTaxPercentage.Visible = false;
                lblPer.Visible = false;
                lblName.Visible = false;
                CmbGeneral.Visible = false;
                TypeMuid = 29;
                GetGeneralData(TypeMuid, 1);
            }
            else if (this.Text == "Ship Type")
            {
                lblCompulsary.Visible = false;
                lbltaxPer.Visible = false;
                txtTaxPercentage.Visible = false;
                lblPer.Visible = false;
                lblName.Visible = false;
                CmbGeneral.Visible = false;
                TypeMuid = 23;
                GetGeneralData(TypeMuid, 1);
            }
            else if (this.Text == "Ship Mode")
            {
                lblCompulsary.Visible = false;
                lbltaxPer.Visible = false;
                txtTaxPercentage.Visible = false;
                lblPer.Visible = false;
                lblName.Visible = false;
                CmbGeneral.Visible = false;
                TypeMuid = 24;
                GetGeneralData(TypeMuid, 1);
            }
            else if (this.Text == "Pay Mode")
            {
                lblCompulsary.Visible = false;
                lbltaxPer.Visible = false;
                txtTaxPercentage.Visible = false;
                lblPer.Visible = false;
                lblName.Visible = false;
                CmbGeneral.Visible = false;
                TypeMuid = 25;
                GetGeneralData(TypeMuid, 1);
            }
            else if (this.Text == "Currency")
            {
                lblCompulsary.Visible = false;
                lbltaxPer.Visible = false;
                txtTaxPercentage.Visible = false;
                lblPer.Visible = false;
                lblName.Visible = false;
                CmbGeneral.Visible = false;
                TypeMuid = 22;
                GetGeneralData(TypeMuid, 1);
            }
            else if (this.Text == "Country")
            {
                lblCompulsary.Visible = false;
                lbltaxPer.Visible = false;
                txtTaxPercentage.Visible = false;
                lblPer.Visible = false;
                lblName.Visible = false;
                CmbGeneral.Visible = false;
                TypeMuid = 26;
                GetGeneralData(TypeMuid, 1);
            }
            else if (this.Text == "Agent")
            {
                lblCompulsary.Visible = false;
                lbltaxPer.Visible = false;
                txtTaxPercentage.Visible = false;
                lblPer.Visible = false;
                lblName.Visible = false;
                CmbGeneral.Visible = false;
                TypeMuid = 21;
                GetGeneralData(TypeMuid, 1);
            }
            else if (this.Text == "Department")
            {
                lblCompulsary.Visible = false;
                lbltaxPer.Visible = false;
                txtTaxPercentage.Visible = false;
                lblPer.Visible = false;
                lblName.Visible = false;
                CmbGeneral.Visible = false;
                TypeMuid = 20;
                GetGeneralData(TypeMuid, 1);
            }
            else if (this.Text == "Structure")
            {
                lblCompulsary.Visible = false;
                lbltaxPer.Visible = false;
                txtTaxPercentage.Visible = false;
                lblPer.Visible = false;
                lblName.Visible = false;
                CmbGeneral.Visible = false;
                TypeMuid = 11;
                GetGeneralData(TypeMuid, 1);
            }
            else if (this.Text == "Classification")
            {
                lblCompulsary.Visible = false;
                lbltaxPer.Visible = false;
                txtTaxPercentage.Visible = false;
                lblPer.Visible = false;
                lblName.Visible = false;
                CmbGeneral.Visible = false;
                TypeMuid = 9;
                GetGeneralData(TypeMuid, 1);
            }
            else if (this.Text == "Item Category")
            {
                SqlParameter[] sqlParameters = { new SqlParameter("@TypeMUid", 9), new SqlParameter("@Active", 1) };
                DataTable dt = db.GetDataWithParam(CommandType.StoredProcedure, "SP_GetGeneralM", sqlParameters, conn);
                CmbGeneral.DataSource = null;
                CmbGeneral.DisplayMember = "GeneralName";
                CmbGeneral.ValueMember = "Guid";
                CmbGeneral.DataSource = dt;
                lbltaxPer.Visible = false;
                txtTaxPercentage.Visible = false;
                lblPer.Visible = false;
                lblName.Visible = true;
                CmbGeneral.Visible = true;
                lblCompulsary.Visible = true;
                lblName.Text = "Item Classification";
                TypeMuid = 13;
                GetGeneralData(TypeMuid, 1);
            }
            else if (this.Text == "Item Attributes")
            {
                SqlParameter[] sqlParameters = { new SqlParameter("@TypeMUid", 13), new SqlParameter("@Active", 1) };
                DataTable dt = db.GetDataWithParam(CommandType.StoredProcedure, "SP_GetGeneralM", sqlParameters, conn);
                CmbGeneral.DataSource = null;
                CmbGeneral.DisplayMember = "GeneralName";
                CmbGeneral.ValueMember = "Guid";
                CmbGeneral.DataSource = dt;
                lbltaxPer.Visible = false;
                txtTaxPercentage.Visible = false;
                lblPer.Visible = false;
                lblName.Visible = true;
                CmbGeneral.Visible = true;
                lblCompulsary.Visible = true;
                lblName.Text = "Item Category";
                TypeMuid = 28;
                GetGeneralData(TypeMuid, 1);
            }
            else if (this.Text == "Count")
            {
                TypeMuid = 2;
                lbltaxPer.Visible = false;
                txtTaxPercentage.Visible = false;
                lblName.Visible = false;
                CmbGeneral.Visible = false;
                lblPer.Visible = false;
                lblCompulsary.Visible = false;
                GetGeneralData(TypeMuid, 1);
            }
            else if (this.Text == "Yarn Item Type")
            {
                TypeMuid = 1;
                lbltaxPer.Visible = false;
                txtTaxPercentage.Visible = false;
                lblPer.Visible = false;
                lblName.Visible = false;
                CmbGeneral.Visible = false;
                lblCompulsary.Visible = false;
                GetGeneralData(TypeMuid, 1);
            }
            else if (this.Text == "Yarn Type")
            {
                TypeMuid = 3;
                lbltaxPer.Visible = false;
                txtTaxPercentage.Visible = false;
                lblPer.Visible = false;
                lblName.Visible = false;
                CmbGeneral.Visible = false;
                lblCompulsary.Visible = false;
                GetGeneralData(TypeMuid, 1);
            }
            else if (this.Text == "Category")
            {
                TypeMuid = 4;
                lbltaxPer.Visible = false;
                txtTaxPercentage.Visible = false;
                lblPer.Visible = false;
                lblName.Visible = false;
                CmbGeneral.Visible = false;
                lblCompulsary.Visible = false;
                GetGeneralData(TypeMuid, 1);
            }
            else if (this.Text == "Blend")
            {
                TypeMuid = 5;
                lbltaxPer.Visible = false;
                txtTaxPercentage.Visible = false;
                lblPer.Visible = false;
                lblName.Visible = false;
                CmbGeneral.Visible = false;
                lblCompulsary.Visible = false;
                GetGeneralData(TypeMuid, 1);
            }
            else if (this.Text == "Purity")
            {
                TypeMuid = 14;
                lbltaxPer.Visible = false;
                txtTaxPercentage.Visible = false;
                lblPer.Visible = false;
                lblName.Visible = false;
                CmbGeneral.Visible = false;
                lblCompulsary.Visible = false;
                GetGeneralData(TypeMuid, 1);
            }
            else if (this.Text == "UOM")
            {
                TypeMuid = 7;
                lbltaxPer.Visible = false;
                txtTaxPercentage.Visible = false;
                lblPer.Visible = false;
                lblName.Visible = false;
                CmbGeneral.Visible = false;
                lblCompulsary.Visible = false;
                GetGeneralData(TypeMuid, 1);
            }
            else if (this.Text == "Tax")
            {
                TypeMuid = 8;
                lbltaxPer.Visible = false;
                txtTaxPercentage.Visible = false;
                lblPer.Visible = false;
                lblName.Visible = false;
                CmbGeneral.Visible = false;
                GetGeneralData(TypeMuid, 1);
                lblCompulsary.Visible = false;
            }
            else if (this.Text == "Season")
            {
                TypeMuid = 16;
                lbltaxPer.Visible = false;
                txtTaxPercentage.Visible = false;
                lblPer.Visible = false;
                lblName.Visible = false;
                CmbGeneral.Visible = false;
                lblCompulsary.Visible = false;
                GetGeneralData(TypeMuid, 1);
            }
            else if (this.Text == "Merchindiser")
            {
                TypeMuid = 17;
                lbltaxPer.Visible = false;
                txtTaxPercentage.Visible = false;
                lblPer.Visible = false;
                lblName.Visible = false;
                CmbGeneral.Visible = false;
                lblCompulsary.Visible = false;
                GetGeneralData(TypeMuid, 1);
            }
            else if (this.Text == "OrderStatus")
            {
                TypeMuid = 18;
                lbltaxPer.Visible = false;
                txtTaxPercentage.Visible = false;
                lblPer.Visible = false;
                lblName.Visible = false;
                CmbGeneral.Visible = false;
                lblCompulsary.Visible = false;
                GetGeneralData(TypeMuid, 1);
            }
            else if (this.Text == "OrderType")
            {
                TypeMuid = 19;
                lbltaxPer.Visible = false;
                txtTaxPercentage.Visible = false;
                lblPer.Visible = false;
                lblName.Visible = false;
                CmbGeneral.Visible = false;
                lblCompulsary.Visible = false;
                GetGeneralData(TypeMuid, 1);
            }
            else if (this.Text == "PayMode")
            {
                TypeMuid = 25;
                lbltaxPer.Visible = false;
                txtTaxPercentage.Visible = false;
                lblPer.Visible = false;
                lblName.Visible = false;
                CmbGeneral.Visible = false;
                lblCompulsary.Visible = false;
                GetGeneralData(TypeMuid, 1);
            }
            else if (this.Text == "Trims")
            {
                TypeMuid = 32;
                lbltaxPer.Visible = false;
                txtTaxPercentage.Visible = false;
                lblPer.Visible = false;
                lblName.Visible = false;
                CmbGeneral.Visible = false;
                lblCompulsary.Visible = false;
                GetGeneralData(TypeMuid, 1);
            }
            else if (this.Text == "Garments")
            {
                TypeMuid = 33;
                lbltaxPer.Visible = false;
                txtTaxPercentage.Visible = false;
                lblPer.Visible = false;
                lblName.Visible = false;
                CmbGeneral.Visible = false;
                lblCompulsary.Visible = false;
                GetGeneralData(TypeMuid, 1);
            }
            else if (this.Text == "CMT-Operations")
            {
                TypeMuid = 34;
                SqlParameter[] sqlParameters = { new SqlParameter("@TypeMUid", 40), new SqlParameter("@Active", 1) };
                DataTable dt = db.GetDataWithParam(CommandType.StoredProcedure, "SP_GetGeneralM", sqlParameters, conn);
                CmbGeneral.DataSource = null;
                CmbGeneral.DisplayMember = "GeneralName";
                CmbGeneral.ValueMember = "Guid";
                CmbGeneral.DataSource = dt;
                lbltaxPer.Visible = false;
                txtTaxPercentage.Visible = false;
                lblPer.Visible = false;
                lblName.Visible = true;
                CmbGeneral.Visible = true;
                lblCompulsary.Visible = true;
                lblName.Text = "CMT - Stage";
                GetGeneralData(TypeMuid, 1);
            }
            else if (this.Text == "CO-ORDINATES")
            {
                TypeMuid = 35;
                lbltaxPer.Visible = false;
                txtTaxPercentage.Visible = false;
                lblPer.Visible = false;
                lblName.Visible = false;
                CmbGeneral.Visible = false;
                lblCompulsary.Visible = false;
                GetGeneralData(TypeMuid, 1);
            }
            else if (this.Text == "Merchand Details")
            {
                TypeMuid = 17;
                lbltaxPer.Visible = false;
                txtTaxPercentage.Visible = false;
                lblPer.Visible = false;
                lblName.Visible = false;
                CmbGeneral.Visible = false;
                lblCompulsary.Visible = false;
                GetGeneralData(TypeMuid, 1);
            }
            else if (this.Text == "Accounts Head")
            {
                TypeMuid = 31;
                lbltaxPer.Visible = false;
                txtTaxPercentage.Visible = false;
                lblPer.Visible = false;
                lblName.Visible = false;
                CmbGeneral.Visible = false;
                lblCompulsary.Visible = false;
                GetGeneralData(TypeMuid, 1);
            }
            if (TypeMuid == 28)
            {
                lblShortName.Text = "Type";
            }
            else
            {
                lblShortName.Text = "Short Name";
            }
        }

        protected void ClearControl()
        {
            try
            {
                txtName.Text = string.Empty;
                txtShortName.Text = string.Empty;
                CmbGeneral.SelectedIndex = -1;
                txtTaxPercentage.Text = "0";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.Message, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected void LoadButton(int id)
        {
            try
            {
                if (id == 0)
                {
                    GrBack.Visible = false;
                    GrFront.Visible = true;
                    btnAdd.Visible = true;
                    btnEdit.Visible = true;
                    btnExit.Visible = true;
                    btnSave.Visible = false;
                    btnAddCancel.Visible = false;
                    btnSave.Text = "Save";
                    chckAc.Visible = true;
                    BtnDelete.Visible = true;
                }
                else if (id == 1)
                {
                    GrBack.Visible = true;
                    GrFront.Visible = false;
                    btnAdd.Visible = false;
                    btnEdit.Visible = false;
                    btnExit.Visible = false;
                    btnSave.Visible = true;
                    btnAddCancel.Visible = true;
                    btnSave.Text = "Save";
                    chckAc.Visible = false;
                    BtnDelete.Visible = false;
                }

                else if (id == 2)
                {
                    GrBack.Visible = true;
                    GrFront.Visible = false;
                    btnAdd.Visible = false;
                    btnEdit.Visible = false;
                    btnExit.Visible = false;
                    btnSave.Visible = true;
                    btnAddCancel.Visible = true;
                    btnSave.Text = "Update";
                    chckAc.Visible = false;
                    BtnDelete.Visible = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void BtnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                ClearControl();
                txtName.Tag = "0";
                ChckActive.Checked = true;
                LoadButton(1);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void BtnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BtnAddCancel_Click(object sender, EventArgs e)
        {
            try
            {
                GetGeneralData(TypeMuid, 1);
                LoadButton(0);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (TypeMuid == 13 || TypeMuid == 34)
                {
                    F1 = 0;
                    F2 = Convert.ToDecimal(CmbGeneral.SelectedValue);
                    F3 = 0;
                }
                else if (TypeMuid == 28)
                {
                    F1 = 0;
                    F3 = Convert.ToDecimal(CmbGeneral.SelectedValue);
                    F2 = 0;
                }
                else
                {
                    F1 = 0;
                    F2 = 0;
                    F3 = 0;
                }
                if (ChckActive.Checked == true)
                {
                    Active = 1;
                }
                else
                {
                    Active = 0;
                }
                if (btnSave.Text == "Save")
                {
                    SqlParameter[] sqlParametersD = { new SqlParameter("@ItemName", txtName.Text), new SqlParameter("@TypeMuid", TypeMuid) };
                    DataTable dataTable = db.GetDataWithParam(CommandType.StoredProcedure, "Proc_CheckDuplicateGeneralM", sqlParametersD, conn);
                    if (dataTable.Rows.Count > 0)
                    {
                        MessageBox.Show("Item Name Already Found", "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                }
                if (txtName.Text != string.Empty && txtShortName.Text != string.Empty)
                {
                    SqlParameter[] sqlParameters = {
                        new SqlParameter("@TypeMUid",TypeMuid),
                        new SqlParameter("@GeneralName",txtName.Text),
                        new SqlParameter("@ShortName",txtShortName.Text),
                        new SqlParameter("@F1",F1),
                        new SqlParameter("@F2",F2),
                        new SqlParameter("@F3",F3),
                        new SqlParameter("@CreateDate",DateTime.Now),
                        new SqlParameter("@UserId",GeneralParameters.UserdId),
                        new SqlParameter("@GUid",txtName.Tag),
                        new SqlParameter("@Active",Active)
                    };
                    db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_GeneralM", sqlParameters, conn);
                    MessageBox.Show("Record Saved Successfully", "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    ClearControl();
                }
                else
                {
                    MessageBox.Show("Enter name", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtName.Focus();
                    return;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected void GetGeneralData(int TypemUid, int Active)
        {
            try
            {
                SqlParameter[] sqlParameters = { new SqlParameter("@TypeMUid", TypemUid), new SqlParameter("@Active", Active) };
                DataTable dt = db.GetDataWithParam(CommandType.StoredProcedure, "SP_GetGeneralM", sqlParameters, conn);
                bs.DataSource = dt;
                DataGridGeneral.DataSource = null;
                DataGridGeneral.AutoGenerateColumns = false;
                DataGridGeneral.ColumnCount = 10;
                DataGridGeneral.Columns[0].Name = "GUid";
                DataGridGeneral.Columns[0].HeaderText = "GUid";
                DataGridGeneral.Columns[0].DataPropertyName = "GUid";
                DataGridGeneral.Columns[0].Visible = false;

                DataGridGeneral.Columns[1].Name = "TypeMUid";
                DataGridGeneral.Columns[1].HeaderText = "TypeMUid";
                DataGridGeneral.Columns[1].DataPropertyName = "TypeMUid";
                DataGridGeneral.Columns[1].Visible = false;

                DataGridGeneral.Columns[2].Name = "GeneralName";
                DataGridGeneral.Columns[2].HeaderText = "Name";
                DataGridGeneral.Columns[2].DataPropertyName = "GeneralName";
                DataGridGeneral.Columns[2].Width = 390;

                DataGridGeneral.Columns[3].Name = "ShortName";
                DataGridGeneral.Columns[3].HeaderText = "ShortName";
                DataGridGeneral.Columns[3].DataPropertyName = "ShortName";
                DataGridGeneral.Columns[3].Width = 150;

                DataGridGeneral.Columns[4].Name = "F1";
                DataGridGeneral.Columns[4].HeaderText = "F1";
                DataGridGeneral.Columns[4].DataPropertyName = "F1";
                DataGridGeneral.Columns[4].Visible = false;

                DataGridGeneral.Columns[5].Name = "F2";
                DataGridGeneral.Columns[5].HeaderText = "F2";
                DataGridGeneral.Columns[5].DataPropertyName = "F2";
                DataGridGeneral.Columns[5].Visible = false;

                DataGridGeneral.Columns[6].Name = "CreateDate";
                DataGridGeneral.Columns[6].HeaderText = "CreateDate";
                DataGridGeneral.Columns[6].DataPropertyName = "CreateDate";
                DataGridGeneral.Columns[6].Visible = false;

                DataGridGeneral.Columns[7].Name = "Active";
                DataGridGeneral.Columns[7].HeaderText = "Active";
                DataGridGeneral.Columns[7].DataPropertyName = "Active";
                DataGridGeneral.Columns[7].Visible = false;

                DataGridGeneral.Columns[8].Name = "Userid";
                DataGridGeneral.Columns[8].HeaderText = "Userid";
                DataGridGeneral.Columns[8].DataPropertyName = "Userid";
                DataGridGeneral.Columns[8].Visible = false;

                DataGridGeneral.Columns[9].Name = "F3";
                DataGridGeneral.Columns[9].HeaderText = "F3";
                DataGridGeneral.Columns[9].DataPropertyName = "F3";
                DataGridGeneral.Columns[9].Visible = false;
                DataGridGeneral.DataSource = bs;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void TxtSearch_TextChanged(object sender, EventArgs e)
        {
            try
            {
                bs.Filter = string.Format("GeneralName Like '%{0}%'", TxtSearch.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void BtnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                ClearControl();
                int Index = DataGridGeneral.SelectedCells[0].RowIndex;
                txtName.Text = DataGridGeneral.Rows[Index].Cells[2].Value.ToString();
                txtShortName.Text = DataGridGeneral.Rows[Index].Cells[3].Value.ToString();
                F1 = Convert.ToDecimal(DataGridGeneral.Rows[Index].Cells[4].Value.ToString());
                string active = DataGridGeneral.Rows[Index].Cells[7].Value.ToString();
                if (TypeMuid == 13 || TypeMuid == 34)
                {
                    CmbGeneral.SelectedValue = DataGridGeneral.Rows[Index].Cells[5].Value.ToString();
                }
                else
                {
                    F2 = Convert.ToDecimal(DataGridGeneral.Rows[Index].Cells[5].Value.ToString());
                }
                if (TypeMuid == 28)
                {
                    CmbGeneral.SelectedValue = DataGridGeneral.Rows[Index].Cells[9].Value.ToString();
                }
                else
                {
                    F3 = Convert.ToDecimal(DataGridGeneral.Rows[Index].Cells[9].Value.ToString());
                }
                if (active == "True")
                {
                    Active = 1;
                    ChckActive.Checked = true;
                }
                else
                {
                    Active = 0;
                    ChckActive.Checked = false;
                }
                txtName.Tag = DataGridGeneral.Rows[Index].Cells[0].Value.ToString();
                LoadButton(2);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void panadd_Paint(object sender, PaintEventArgs e)
        {

        }

        private void BtnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                int Index = DataGridGeneral.SelectedCells[0].RowIndex;
                decimal CUid = Convert.ToDecimal(DataGridGeneral.Rows[Index].Cells["CUid"].Value.ToString());
                DialogResult dialogResult = MessageBox.Show("Do you want do delete this record", "Infromation", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2); ;
                if (dialogResult == DialogResult.Yes)
                {
                    SqlParameter[] sqlParameters = {
                        new SqlParameter("@TypeMuid",Tag),
                        new SqlParameter("@Guid",CUid),
                    };
                    db.ExecuteNonQuery(CommandType.StoredProcedure, "Proc_DeleteGenerlM", sqlParameters, conn);
                    MessageBox.Show("Record Deleted Successfully", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
    }
}
