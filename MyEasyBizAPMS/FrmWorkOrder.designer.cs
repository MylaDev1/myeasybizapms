﻿namespace MyEasyBizAPMS
{
    partial class FrmWorkOrder
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmWorkOrder));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.Genpan = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.txtscr1 = new System.Windows.Forms.TextBox();
            this.HFGP = new System.Windows.Forms.DataGridView();
            this.txtscr4 = new System.Windows.Forms.TextBox();
            this.Txtscr3 = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.txtscr6 = new System.Windows.Forms.TextBox();
            this.txtscr5 = new System.Windows.Forms.TextBox();
            this.Txtscr2 = new System.Windows.Forms.TextBox();
            this.panadd = new System.Windows.Forms.Panel();
            this.button3 = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblno1 = new System.Windows.Forms.Label();
            this.lblno2 = new System.Windows.Forms.Label();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.buttnnvfst = new System.Windows.Forms.Button();
            this.buttnnxtlft = new System.Windows.Forms.Button();
            this.btnfinnxt = new System.Windows.Forms.Button();
            this.buttrnxt = new System.Windows.Forms.Button();
            this.buttnext1 = new System.Windows.Forms.Button();
            this.chkact = new System.Windows.Forms.CheckBox();
            this.butexit = new System.Windows.Forms.Button();
            this.butedit = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.btnaddrcan = new System.Windows.Forms.Button();
            this.btnsave = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.txtrecqty = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txtpoid = new System.Windows.Forms.TextBox();
            this.txtpono = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txtjjno = new System.Windows.Forms.TextBox();
            this.dtpgrndt = new System.Windows.Forms.DateTimePicker();
            this.txtgrndt = new System.Windows.Forms.TextBox();
            this.txtgrnid = new System.Windows.Forms.TextBox();
            this.txtpuid = new System.Windows.Forms.TextBox();
            this.txtititd = new System.Windows.Forms.TextBox();
            this.txtgrn = new System.Windows.Forms.TextBox();
            this.Phone = new System.Windows.Forms.Label();
            this.txtname = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtnar = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtdcno = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.Dtpdt = new System.Windows.Forms.DateTimePicker();
            this.txtitem = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtqty = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.Reqpan = new System.Windows.Forms.Panel();
            this.txtreqqty = new System.Windows.Forms.TextBox();
            this.Reqbk = new System.Windows.Forms.Button();
            this.label14 = new System.Windows.Forms.Label();
            this.RQGR = new System.Windows.Forms.DataGridView();
            this.buttrqok = new System.Windows.Forms.Button();
            this.Dtpreq = new System.Windows.Forms.DateTimePicker();
            this.label13 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.Cboit = new System.Windows.Forms.ComboBox();
            this.txtqty1 = new System.Windows.Forms.TextBox();
            this.txtqty2 = new System.Windows.Forms.TextBox();
            this.chkgrd = new System.Windows.Forms.DataGridView();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.cboitem = new System.Windows.Forms.ComboBox();
            this.label32 = new System.Windows.Forms.Label();
            this.grSearch = new System.Windows.Forms.Panel();
            this.button18 = new System.Windows.Forms.Button();
            this.btnHide = new System.Windows.Forms.Button();
            this.DataGridCommon = new System.Windows.Forms.DataGridView();
            this.HFIT = new System.Windows.Forms.DataGridView();
            this.cbosGReturnItem = new System.Windows.Forms.ComboBox();
            this.cbotype = new System.Windows.Forms.ComboBox();
            this.btnadd = new System.Windows.Forms.Button();
            this.label19 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txtaddnotes = new System.Windows.Forms.TextBox();
            this.txtdcqty = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.txtprocessdet = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.HFIT1 = new System.Windows.Forms.DataGridView();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.Pnlin = new System.Windows.Forms.Panel();
            this.button2 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.HFITIN = new System.Windows.Forms.DataGridView();
            this.HFITIN1 = new System.Windows.Forms.DataGridView();
            this.button5 = new System.Windows.Forms.Button();
            this.TXTIN = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.TXTINQTY = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.txtbeam = new System.Windows.Forms.TextBox();
            this.txtsamqty = new System.Windows.Forms.TextBox();
            this.HFIPOUT = new System.Windows.Forms.DataGridView();
            this.type = new System.Windows.Forms.ComboBox();
            this.cboin = new System.Windows.Forms.ComboBox();
            this.label26 = new System.Windows.Forms.Label();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.label27 = new System.Windows.Forms.Label();
            this.cbotypeout = new System.Windows.Forms.ComboBox();
            this.Pnlout = new System.Windows.Forms.Panel();
            this.button6 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.HFITOUT = new System.Windows.Forms.DataGridView();
            this.HFITOUT1 = new System.Windows.Forms.DataGridView();
            this.button8 = new System.Windows.Forms.Button();
            this.TXTOUT = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.TXTOUTQTY = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.cboout = new System.Windows.Forms.ComboBox();
            this.label25 = new System.Windows.Forms.Label();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.YARNLOAD = new System.Windows.Forms.DataGridView();
            this.label30 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.button11 = new System.Windows.Forms.Button();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.txtcolor = new System.Windows.Forms.TextBox();
            this.YARNCOLORLOAD = new System.Windows.Forms.DataGridView();
            this.label28 = new System.Windows.Forms.Label();
            this.cbofabric = new System.Windows.Forms.ComboBox();
            this.grNewSearch = new System.Windows.Forms.GroupBox();
            this.button22 = new System.Windows.Forms.Button();
            this.btnSelect = new System.Windows.Forms.Button();
            this.DataGridCommonNew = new System.Windows.Forms.DataGridView();
            this.button10 = new System.Windows.Forms.Button();
            this.Editpan = new System.Windows.Forms.Panel();
            this.label31 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.Genpan.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HFGP)).BeginInit();
            this.panadd.SuspendLayout();
            this.panel1.SuspendLayout();
            this.Reqpan.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RQGR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkgrd)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.grSearch.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridCommon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HFIT)).BeginInit();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HFIT1)).BeginInit();
            this.tabPage3.SuspendLayout();
            this.Pnlin.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HFITIN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HFITIN1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HFIPOUT)).BeginInit();
            this.tabPage4.SuspendLayout();
            this.Pnlout.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HFITOUT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HFITOUT1)).BeginInit();
            this.tabPage5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.YARNLOAD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.YARNCOLORLOAD)).BeginInit();
            this.grNewSearch.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridCommonNew)).BeginInit();
            this.Editpan.SuspendLayout();
            this.SuspendLayout();
            // 
            // Genpan
            // 
            this.Genpan.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.Genpan.Controls.Add(this.label1);
            this.Genpan.Controls.Add(this.txtscr1);
            this.Genpan.Controls.Add(this.HFGP);
            this.Genpan.Controls.Add(this.txtscr4);
            this.Genpan.Controls.Add(this.Txtscr3);
            this.Genpan.Controls.Add(this.textBox1);
            this.Genpan.Controls.Add(this.txtscr6);
            this.Genpan.Controls.Add(this.txtscr5);
            this.Genpan.Controls.Add(this.Txtscr2);
            this.Genpan.Location = new System.Drawing.Point(0, -21);
            this.Genpan.Name = "Genpan";
            this.Genpan.Size = new System.Drawing.Size(1004, 525);
            this.Genpan.TabIndex = 157;
            this.Genpan.Paint += new System.Windows.Forms.PaintEventHandler(this.Genpan_Paint);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(7, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(79, 18);
            this.label1.TabIndex = 95;
            this.label1.Text = "Work Order";
            // 
            // txtscr1
            // 
            this.txtscr1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtscr1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtscr1.Location = new System.Drawing.Point(6, 42);
            this.txtscr1.Margin = new System.Windows.Forms.Padding(4);
            this.txtscr1.Name = "txtscr1";
            this.txtscr1.Size = new System.Drawing.Size(994, 26);
            this.txtscr1.TabIndex = 1;
            this.txtscr1.TextChanged += new System.EventHandler(this.txtscr1_TextChanged);
            // 
            // HFGP
            // 
            this.HFGP.AllowUserToDeleteRows = false;
            this.HFGP.AllowUserToOrderColumns = true;
            this.HFGP.BackgroundColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.HFGP.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.HFGP.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.HFGP.Location = new System.Drawing.Point(6, 71);
            this.HFGP.Name = "HFGP";
            this.HFGP.Size = new System.Drawing.Size(998, 424);
            this.HFGP.TabIndex = 3;
            // 
            // txtscr4
            // 
            this.txtscr4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtscr4.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtscr4.Location = new System.Drawing.Point(252, 177);
            this.txtscr4.Margin = new System.Windows.Forms.Padding(4);
            this.txtscr4.Name = "txtscr4";
            this.txtscr4.Size = new System.Drawing.Size(90, 26);
            this.txtscr4.TabIndex = 89;
            // 
            // Txtscr3
            // 
            this.Txtscr3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Txtscr3.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txtscr3.Location = new System.Drawing.Point(163, 177);
            this.Txtscr3.Margin = new System.Windows.Forms.Padding(4);
            this.Txtscr3.Name = "Txtscr3";
            this.Txtscr3.Size = new System.Drawing.Size(90, 26);
            this.Txtscr3.TabIndex = 88;
            // 
            // textBox1
            // 
            this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.Location = new System.Drawing.Point(98, 198);
            this.textBox1.Margin = new System.Windows.Forms.Padding(4);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(270, 26);
            this.textBox1.TabIndex = 96;
            // 
            // txtscr6
            // 
            this.txtscr6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtscr6.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtscr6.Location = new System.Drawing.Point(580, 178);
            this.txtscr6.Margin = new System.Windows.Forms.Padding(4);
            this.txtscr6.Name = "txtscr6";
            this.txtscr6.Size = new System.Drawing.Size(374, 26);
            this.txtscr6.TabIndex = 91;
            // 
            // txtscr5
            // 
            this.txtscr5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtscr5.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtscr5.Location = new System.Drawing.Point(144, 178);
            this.txtscr5.Margin = new System.Windows.Forms.Padding(4);
            this.txtscr5.Name = "txtscr5";
            this.txtscr5.Size = new System.Drawing.Size(438, 26);
            this.txtscr5.TabIndex = 90;
            // 
            // Txtscr2
            // 
            this.Txtscr2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Txtscr2.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txtscr2.Location = new System.Drawing.Point(52, 178);
            this.Txtscr2.Margin = new System.Windows.Forms.Padding(4);
            this.Txtscr2.Name = "Txtscr2";
            this.Txtscr2.Size = new System.Drawing.Size(90, 26);
            this.Txtscr2.TabIndex = 87;
            // 
            // panadd
            // 
            this.panadd.BackColor = System.Drawing.Color.White;
            this.panadd.Controls.Add(this.button3);
            this.panadd.Controls.Add(this.panel1);
            this.panadd.Controls.Add(this.buttnnvfst);
            this.panadd.Controls.Add(this.buttnnxtlft);
            this.panadd.Controls.Add(this.btnfinnxt);
            this.panadd.Controls.Add(this.buttrnxt);
            this.panadd.Controls.Add(this.buttnext1);
            this.panadd.Controls.Add(this.chkact);
            this.panadd.Controls.Add(this.butexit);
            this.panadd.Controls.Add(this.butedit);
            this.panadd.Controls.Add(this.button1);
            this.panadd.Location = new System.Drawing.Point(4, 500);
            this.panadd.Name = "panadd";
            this.panadd.Size = new System.Drawing.Size(1000, 31);
            this.panadd.TabIndex = 209;
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.White;
            this.button3.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.Image = ((System.Drawing.Image)(resources.GetObject("button3.Image")));
            this.button3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button3.Location = new System.Drawing.Point(501, 3);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(67, 30);
            this.button3.TabIndex = 215;
            this.button3.Text = "Print";
            this.button3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.lblno1);
            this.panel1.Controls.Add(this.lblno2);
            this.panel1.Controls.Add(this.flowLayoutPanel3);
            this.panel1.Controls.Add(this.flowLayoutPanel2);
            this.panel1.Controls.Add(this.flowLayoutPanel1);
            this.panel1.Location = new System.Drawing.Point(64, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(74, 30);
            this.panel1.TabIndex = 214;
            // 
            // lblno1
            // 
            this.lblno1.AutoSize = true;
            this.lblno1.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblno1.ForeColor = System.Drawing.Color.Black;
            this.lblno1.Location = new System.Drawing.Point(4, 5);
            this.lblno1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblno1.Name = "lblno1";
            this.lblno1.Size = new System.Drawing.Size(14, 15);
            this.lblno1.TabIndex = 163;
            this.lblno1.Text = "1";
            // 
            // lblno2
            // 
            this.lblno2.AutoSize = true;
            this.lblno2.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblno2.ForeColor = System.Drawing.Color.Black;
            this.lblno2.Location = new System.Drawing.Point(27, 5);
            this.lblno2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblno2.Name = "lblno2";
            this.lblno2.Size = new System.Drawing.Size(28, 15);
            this.lblno2.TabIndex = 162;
            this.lblno2.Text = "of 1";
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.Location = new System.Drawing.Point(61, 30);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(200, 100);
            this.flowLayoutPanel3.TabIndex = 2;
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.Location = new System.Drawing.Point(38, 35);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(200, 100);
            this.flowLayoutPanel2.TabIndex = 1;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Location = new System.Drawing.Point(62, -129);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(200, 100);
            this.flowLayoutPanel1.TabIndex = 0;
            // 
            // buttnnvfst
            // 
            this.buttnnvfst.BackColor = System.Drawing.Color.White;
            this.buttnnvfst.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.buttnnvfst.FlatAppearance.BorderSize = 0;
            this.buttnnvfst.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttnnvfst.Image = ((System.Drawing.Image)(resources.GetObject("buttnnvfst.Image")));
            this.buttnnvfst.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttnnvfst.Location = new System.Drawing.Point(6, 0);
            this.buttnnvfst.Name = "buttnnvfst";
            this.buttnnvfst.Size = new System.Drawing.Size(19, 31);
            this.buttnnvfst.TabIndex = 213;
            this.buttnnvfst.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttnnvfst.UseVisualStyleBackColor = false;
            // 
            // buttnnxtlft
            // 
            this.buttnnxtlft.BackColor = System.Drawing.Color.White;
            this.buttnnxtlft.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.buttnnxtlft.FlatAppearance.BorderSize = 0;
            this.buttnnxtlft.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttnnxtlft.Image = ((System.Drawing.Image)(resources.GetObject("buttnnxtlft.Image")));
            this.buttnnxtlft.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttnnxtlft.Location = new System.Drawing.Point(40, 0);
            this.buttnnxtlft.Name = "buttnnxtlft";
            this.buttnnxtlft.Size = new System.Drawing.Size(18, 31);
            this.buttnnxtlft.TabIndex = 212;
            this.buttnnxtlft.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttnnxtlft.UseVisualStyleBackColor = false;
            // 
            // btnfinnxt
            // 
            this.btnfinnxt.BackColor = System.Drawing.Color.White;
            this.btnfinnxt.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnfinnxt.FlatAppearance.BorderSize = 0;
            this.btnfinnxt.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnfinnxt.Image = ((System.Drawing.Image)(resources.GetObject("btnfinnxt.Image")));
            this.btnfinnxt.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnfinnxt.Location = new System.Drawing.Point(178, 0);
            this.btnfinnxt.Name = "btnfinnxt";
            this.btnfinnxt.Size = new System.Drawing.Size(19, 31);
            this.btnfinnxt.TabIndex = 211;
            this.btnfinnxt.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnfinnxt.UseVisualStyleBackColor = false;
            // 
            // buttrnxt
            // 
            this.buttrnxt.BackColor = System.Drawing.Color.White;
            this.buttrnxt.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.buttrnxt.FlatAppearance.BorderSize = 0;
            this.buttrnxt.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttrnxt.Image = ((System.Drawing.Image)(resources.GetObject("buttrnxt.Image")));
            this.buttrnxt.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttrnxt.Location = new System.Drawing.Point(144, 0);
            this.buttrnxt.Name = "buttrnxt";
            this.buttrnxt.Size = new System.Drawing.Size(18, 31);
            this.buttrnxt.TabIndex = 210;
            this.buttrnxt.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttrnxt.UseVisualStyleBackColor = false;
            // 
            // buttnext1
            // 
            this.buttnext1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttnext1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttnext1.Image = ((System.Drawing.Image)(resources.GetObject("buttnext1.Image")));
            this.buttnext1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttnext1.Location = new System.Drawing.Point(574, 3);
            this.buttnext1.Name = "buttnext1";
            this.buttnext1.Size = new System.Drawing.Size(57, 30);
            this.buttnext1.TabIndex = 208;
            this.buttnext1.Text = "Exit";
            this.buttnext1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttnext1.UseVisualStyleBackColor = false;
            this.buttnext1.Click += new System.EventHandler(this.buttnext1_Click);
            // 
            // chkact
            // 
            this.chkact.AutoSize = true;
            this.chkact.BackColor = System.Drawing.Color.White;
            this.chkact.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkact.Location = new System.Drawing.Point(223, 7);
            this.chkact.Name = "chkact";
            this.chkact.Size = new System.Drawing.Size(65, 22);
            this.chkact.TabIndex = 187;
            this.chkact.Text = "Active";
            this.chkact.UseVisualStyleBackColor = false;
            this.chkact.CheckedChanged += new System.EventHandler(this.chkact_CheckedChanged);
            // 
            // butexit
            // 
            this.butexit.BackColor = System.Drawing.Color.White;
            this.butexit.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.butexit.Image = ((System.Drawing.Image)(resources.GetObject("butexit.Image")));
            this.butexit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.butexit.Location = new System.Drawing.Point(432, 1);
            this.butexit.Name = "butexit";
            this.butexit.Size = new System.Drawing.Size(69, 30);
            this.butexit.TabIndex = 186;
            this.butexit.Text = "Delete";
            this.butexit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.butexit.UseVisualStyleBackColor = false;
            this.butexit.Click += new System.EventHandler(this.butexit_Click);
            // 
            // butedit
            // 
            this.butedit.BackColor = System.Drawing.Color.White;
            this.butedit.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.butedit.Image = ((System.Drawing.Image)(resources.GetObject("butedit.Image")));
            this.butedit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.butedit.Location = new System.Drawing.Point(378, 1);
            this.butedit.Name = "butedit";
            this.butedit.Size = new System.Drawing.Size(55, 30);
            this.butedit.TabIndex = 185;
            this.butedit.Text = "Edit";
            this.butedit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.butedit.UseVisualStyleBackColor = false;
            this.butedit.Click += new System.EventHandler(this.butedit_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.White;
            this.button1.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Image = ((System.Drawing.Image)(resources.GetObject("button1.Image")));
            this.button1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button1.Location = new System.Drawing.Point(290, 1);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(87, 30);
            this.button1.TabIndex = 184;
            this.button1.Text = "Add New";
            this.button1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnaddrcan
            // 
            this.btnaddrcan.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnaddrcan.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnaddrcan.Image = ((System.Drawing.Image)(resources.GetObject("btnaddrcan.Image")));
            this.btnaddrcan.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnaddrcan.Location = new System.Drawing.Point(888, 500);
            this.btnaddrcan.Name = "btnaddrcan";
            this.btnaddrcan.Size = new System.Drawing.Size(60, 29);
            this.btnaddrcan.TabIndex = 215;
            this.btnaddrcan.Text = "Back";
            this.btnaddrcan.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnaddrcan.UseVisualStyleBackColor = false;
            this.btnaddrcan.Click += new System.EventHandler(this.btnaddrcan_Click);
            // 
            // btnsave
            // 
            this.btnsave.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnsave.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnsave.Image = ((System.Drawing.Image)(resources.GetObject("btnsave.Image")));
            this.btnsave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnsave.Location = new System.Drawing.Point(827, 499);
            this.btnsave.Name = "btnsave";
            this.btnsave.Size = new System.Drawing.Size(60, 30);
            this.btnsave.TabIndex = 1;
            this.btnsave.Text = "Save";
            this.btnsave.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnsave.UseVisualStyleBackColor = false;
            this.btnsave.Click += new System.EventHandler(this.btnsave_Click);
            // 
            // button9
            // 
            this.button9.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button9.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button9.Image = ((System.Drawing.Image)(resources.GetObject("button9.Image")));
            this.button9.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button9.Location = new System.Drawing.Point(950, 500);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(54, 29);
            this.button9.TabIndex = 216;
            this.button9.Text = "Exit";
            this.button9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button9.UseVisualStyleBackColor = false;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // txtrecqty
            // 
            this.txtrecqty.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtrecqty.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtrecqty.Location = new System.Drawing.Point(505, 254);
            this.txtrecqty.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtrecqty.Name = "txtrecqty";
            this.txtrecqty.Size = new System.Drawing.Size(53, 23);
            this.txtrecqty.TabIndex = 1;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(453, 233);
            this.label16.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(44, 15);
            this.label16.TabIndex = 1;
            this.label16.Text = "Recqty";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Red;
            this.label8.Location = new System.Drawing.Point(496, 248);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(115, 18);
            this.label8.TabIndex = 2;
            this.label8.Text = "Required Date";
            this.label8.Visible = false;
            // 
            // txtpoid
            // 
            this.txtpoid.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtpoid.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtpoid.Location = new System.Drawing.Point(552, 233);
            this.txtpoid.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtpoid.Name = "txtpoid";
            this.txtpoid.Size = new System.Drawing.Size(64, 23);
            this.txtpoid.TabIndex = 1;
            // 
            // txtpono
            // 
            this.txtpono.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtpono.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtpono.Location = new System.Drawing.Point(381, 220);
            this.txtpono.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtpono.Name = "txtpono";
            this.txtpono.Size = new System.Drawing.Size(64, 23);
            this.txtpono.TabIndex = 1;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(423, 188);
            this.label15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(34, 15);
            this.label15.TabIndex = 1;
            this.label15.Text = "JJ No";
            // 
            // txtjjno
            // 
            this.txtjjno.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtjjno.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtjjno.Location = new System.Drawing.Point(426, 206);
            this.txtjjno.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtjjno.Name = "txtjjno";
            this.txtjjno.Size = new System.Drawing.Size(110, 23);
            this.txtjjno.TabIndex = 4;
            // 
            // dtpgrndt
            // 
            this.dtpgrndt.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpgrndt.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpgrndt.Location = new System.Drawing.Point(122, 37);
            this.dtpgrndt.Name = "dtpgrndt";
            this.dtpgrndt.Size = new System.Drawing.Size(122, 23);
            this.dtpgrndt.TabIndex = 10;
            this.dtpgrndt.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.dtpgrndt_KeyPress);
            // 
            // txtgrndt
            // 
            this.txtgrndt.Location = new System.Drawing.Point(159, 194);
            this.txtgrndt.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtgrndt.Name = "txtgrndt";
            this.txtgrndt.Size = new System.Drawing.Size(122, 23);
            this.txtgrndt.TabIndex = 2;
            this.txtgrndt.TabStop = false;
            // 
            // txtgrnid
            // 
            this.txtgrnid.Location = new System.Drawing.Point(29, 37);
            this.txtgrnid.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtgrnid.Name = "txtgrnid";
            this.txtgrnid.Size = new System.Drawing.Size(19, 23);
            this.txtgrnid.TabIndex = 1;
            // 
            // txtpuid
            // 
            this.txtpuid.Location = new System.Drawing.Point(199, 305);
            this.txtpuid.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtpuid.Name = "txtpuid";
            this.txtpuid.Size = new System.Drawing.Size(66, 23);
            this.txtpuid.TabIndex = 1;
            // 
            // txtititd
            // 
            this.txtititd.Location = new System.Drawing.Point(42, 194);
            this.txtititd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtititd.Name = "txtititd";
            this.txtititd.Size = new System.Drawing.Size(51, 23);
            this.txtititd.TabIndex = 1;
            // 
            // txtgrn
            // 
            this.txtgrn.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtgrn.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtgrn.Location = new System.Drawing.Point(6, 37);
            this.txtgrn.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtgrn.Name = "txtgrn";
            this.txtgrn.Size = new System.Drawing.Size(110, 23);
            this.txtgrn.TabIndex = 2;
            this.txtgrn.TabStop = false;
            this.txtgrn.TextChanged += new System.EventHandler(this.txtgrn_TextChanged);
            // 
            // Phone
            // 
            this.Phone.AutoSize = true;
            this.Phone.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Phone.Location = new System.Drawing.Point(3, 15);
            this.Phone.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Phone.Name = "Phone";
            this.Phone.Size = new System.Drawing.Size(91, 21);
            this.Phone.TabIndex = 2;
            this.Phone.Text = "Work Order";
            // 
            // txtname
            // 
            this.txtname.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtname.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtname.Location = new System.Drawing.Point(29, 231);
            this.txtname.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtname.Name = "txtname";
            this.txtname.Size = new System.Drawing.Size(459, 23);
            this.txtname.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(26, 210);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(92, 21);
            this.label3.TabIndex = 1;
            this.label3.Text = "Party Name";
            // 
            // txtnar
            // 
            this.txtnar.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtnar.Enabled = false;
            this.txtnar.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtnar.Location = new System.Drawing.Point(373, 37);
            this.txtnar.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtnar.Name = "txtnar";
            this.txtnar.Size = new System.Drawing.Size(243, 23);
            this.txtnar.TabIndex = 11;
            this.txtnar.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtnar_KeyPress);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(377, 15);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(44, 21);
            this.label4.TabIndex = 9;
            this.label4.Text = "Style";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(120, 15);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(72, 21);
            this.label2.TabIndex = 1;
            this.label2.Text = "Doc Date";
            // 
            // txtdcno
            // 
            this.txtdcno.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtdcno.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtdcno.Location = new System.Drawing.Point(624, 37);
            this.txtdcno.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtdcno.Name = "txtdcno";
            this.txtdcno.Size = new System.Drawing.Size(376, 23);
            this.txtdcno.TabIndex = 12;
            this.txtdcno.TextChanged += new System.EventHandler(this.txtdcno_TextChanged);
            this.txtdcno.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtdcno_KeyPress);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(620, 15);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(76, 21);
            this.label5.TabIndex = 2;
            this.label5.Text = "Narration";
            this.label5.Click += new System.EventHandler(this.label5_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(357, 266);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(95, 21);
            this.label6.TabIndex = 2;
            this.label6.Text = "Invoice.Date";
            // 
            // Dtpdt
            // 
            this.Dtpdt.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Dtpdt.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.Dtpdt.Location = new System.Drawing.Point(360, 287);
            this.Dtpdt.Name = "Dtpdt";
            this.Dtpdt.Size = new System.Drawing.Size(97, 23);
            this.Dtpdt.TabIndex = 3;
            // 
            // txtitem
            // 
            this.txtitem.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtitem.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtitem.Location = new System.Drawing.Point(175, 292);
            this.txtitem.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtitem.Name = "txtitem";
            this.txtitem.Size = new System.Drawing.Size(198, 23);
            this.txtitem.TabIndex = 3;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(172, 270);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(42, 21);
            this.label7.TabIndex = 5;
            this.label7.Text = "Item";
            // 
            // txtqty
            // 
            this.txtqty.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtqty.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtqty.Location = new System.Drawing.Point(38, 236);
            this.txtqty.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtqty.Name = "txtqty";
            this.txtqty.Size = new System.Drawing.Size(54, 23);
            this.txtqty.TabIndex = 1;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(38, 214);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(45, 21);
            this.label10.TabIndex = 2;
            this.label10.Text = "SLNo";
            // 
            // Reqpan
            // 
            this.Reqpan.Controls.Add(this.txtreqqty);
            this.Reqpan.Controls.Add(this.Reqbk);
            this.Reqpan.Controls.Add(this.label14);
            this.Reqpan.Controls.Add(this.RQGR);
            this.Reqpan.Controls.Add(this.buttrqok);
            this.Reqpan.Controls.Add(this.Dtpreq);
            this.Reqpan.Controls.Add(this.label13);
            this.Reqpan.Controls.Add(this.label11);
            this.Reqpan.Controls.Add(this.Cboit);
            this.Reqpan.Controls.Add(this.txtqty1);
            this.Reqpan.Controls.Add(this.txtqty2);
            this.Reqpan.Controls.Add(this.chkgrd);
            this.Reqpan.Location = new System.Drawing.Point(807, 213);
            this.Reqpan.Name = "Reqpan";
            this.Reqpan.Size = new System.Drawing.Size(44, 16);
            this.Reqpan.TabIndex = 2;
            this.Reqpan.Visible = false;
            // 
            // txtreqqty
            // 
            this.txtreqqty.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtreqqty.Location = new System.Drawing.Point(531, 34);
            this.txtreqqty.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtreqqty.Name = "txtreqqty";
            this.txtreqqty.Size = new System.Drawing.Size(51, 22);
            this.txtreqqty.TabIndex = 159;
            // 
            // Reqbk
            // 
            this.Reqbk.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Reqbk.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Reqbk.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Reqbk.Location = new System.Drawing.Point(273, 381);
            this.Reqbk.Name = "Reqbk";
            this.Reqbk.Size = new System.Drawing.Size(83, 38);
            this.Reqbk.TabIndex = 156;
            this.Reqbk.Text = "Back";
            this.Reqbk.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.Reqbk.UseVisualStyleBackColor = false;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(528, 11);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(34, 18);
            this.label14.TabIndex = 158;
            this.label14.Text = "Qty";
            // 
            // RQGR
            // 
            this.RQGR.AllowUserToDeleteRows = false;
            this.RQGR.BackgroundColor = System.Drawing.Color.White;
            this.RQGR.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.RQGR.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.RQGR.Location = new System.Drawing.Point(24, 62);
            this.RQGR.Name = "RQGR";
            this.RQGR.Size = new System.Drawing.Size(558, 313);
            this.RQGR.TabIndex = 131;
            // 
            // buttrqok
            // 
            this.buttrqok.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttrqok.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttrqok.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.buttrqok.Location = new System.Drawing.Point(590, 27);
            this.buttrqok.Name = "buttrqok";
            this.buttrqok.Size = new System.Drawing.Size(34, 32);
            this.buttrqok.TabIndex = 130;
            this.buttrqok.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttrqok.UseVisualStyleBackColor = false;
            // 
            // Dtpreq
            // 
            this.Dtpreq.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Dtpreq.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.Dtpreq.Location = new System.Drawing.Point(418, 34);
            this.Dtpreq.Name = "Dtpreq";
            this.Dtpreq.Size = new System.Drawing.Size(106, 22);
            this.Dtpreq.TabIndex = 120;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(415, 13);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(78, 18);
            this.label13.TabIndex = 119;
            this.label13.Text = "Req Date";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(21, 11);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(40, 18);
            this.label11.TabIndex = 118;
            this.label11.Text = "Item";
            // 
            // Cboit
            // 
            this.Cboit.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.Cboit.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.Cboit.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cboit.FormattingEnabled = true;
            this.Cboit.Location = new System.Drawing.Point(24, 34);
            this.Cboit.Name = "Cboit";
            this.Cboit.Size = new System.Drawing.Size(388, 24);
            this.Cboit.TabIndex = 91;
            // 
            // txtqty1
            // 
            this.txtqty1.Location = new System.Drawing.Point(418, 83);
            this.txtqty1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtqty1.Name = "txtqty1";
            this.txtqty1.Size = new System.Drawing.Size(51, 23);
            this.txtqty1.TabIndex = 160;
            // 
            // txtqty2
            // 
            this.txtqty2.Location = new System.Drawing.Point(214, 81);
            this.txtqty2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtqty2.Name = "txtqty2";
            this.txtqty2.Size = new System.Drawing.Size(51, 23);
            this.txtqty2.TabIndex = 161;
            // 
            // chkgrd
            // 
            this.chkgrd.BackgroundColor = System.Drawing.Color.White;
            this.chkgrd.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.chkgrd.Location = new System.Drawing.Point(43, 62);
            this.chkgrd.Name = "chkgrd";
            this.chkgrd.Size = new System.Drawing.Size(33, 30);
            this.chkgrd.TabIndex = 162;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Location = new System.Drawing.Point(10, 68);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(994, 436);
            this.tabControl1.TabIndex = 1;
            this.tabControl1.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.cboitem);
            this.tabPage2.Controls.Add(this.label32);
            this.tabPage2.Controls.Add(this.grSearch);
            this.tabPage2.Controls.Add(this.HFIT);
            this.tabPage2.Controls.Add(this.cbosGReturnItem);
            this.tabPage2.Controls.Add(this.cbotype);
            this.tabPage2.Controls.Add(this.btnadd);
            this.tabPage2.Controls.Add(this.label19);
            this.tabPage2.Controls.Add(this.label9);
            this.tabPage2.Controls.Add(this.txtaddnotes);
            this.tabPage2.Controls.Add(this.txtdcqty);
            this.tabPage2.Controls.Add(this.label12);
            this.tabPage2.Controls.Add(this.label18);
            this.tabPage2.Controls.Add(this.txtprocessdet);
            this.tabPage2.Controls.Add(this.label17);
            this.tabPage2.Location = new System.Drawing.Point(4, 24);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(986, 408);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Process";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // cboitem
            // 
            this.cboitem.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboitem.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboitem.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboitem.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboitem.FormattingEnabled = true;
            this.cboitem.Location = new System.Drawing.Point(404, 22);
            this.cboitem.Name = "cboitem";
            this.cboitem.Size = new System.Drawing.Size(568, 26);
            this.cboitem.TabIndex = 428;
            this.cboitem.SelectedIndexChanged += new System.EventHandler(this.cboitem_SelectedIndexChanged);
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.Location = new System.Drawing.Point(344, 23);
            this.label32.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(56, 21);
            this.label32.TabIndex = 427;
            this.label32.Text = "Fabric ";
            // 
            // grSearch
            // 
            this.grSearch.BackColor = System.Drawing.Color.White;
            this.grSearch.Controls.Add(this.button18);
            this.grSearch.Controls.Add(this.btnHide);
            this.grSearch.Controls.Add(this.DataGridCommon);
            this.grSearch.Location = new System.Drawing.Point(231, 122);
            this.grSearch.Name = "grSearch";
            this.grSearch.Size = new System.Drawing.Size(441, 217);
            this.grSearch.TabIndex = 123;
            this.grSearch.Visible = false;
            // 
            // button18
            // 
            this.button18.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button18.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button18.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button18.Location = new System.Drawing.Point(227, 188);
            this.button18.Name = "button18";
            this.button18.Size = new System.Drawing.Size(100, 28);
            this.button18.TabIndex = 394;
            this.button18.Text = "Select (F2)";
            this.button18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button18.UseVisualStyleBackColor = false;
            this.button18.Click += new System.EventHandler(this.button18_Click_1);
            // 
            // btnHide
            // 
            this.btnHide.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnHide.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHide.Image = ((System.Drawing.Image)(resources.GetObject("btnHide.Image")));
            this.btnHide.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnHide.Location = new System.Drawing.Point(333, 188);
            this.btnHide.Name = "btnHide";
            this.btnHide.Size = new System.Drawing.Size(100, 27);
            this.btnHide.TabIndex = 393;
            this.btnHide.Text = "Close (F10)";
            this.btnHide.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnHide.UseVisualStyleBackColor = false;
            this.btnHide.Click += new System.EventHandler(this.btnHide_Click_1);
            // 
            // DataGridCommon
            // 
            this.DataGridCommon.AllowUserToAddRows = false;
            this.DataGridCommon.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.DataGridCommon.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridCommon.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.DataGridCommon.Location = new System.Drawing.Point(5, 14);
            this.DataGridCommon.Name = "DataGridCommon";
            this.DataGridCommon.ReadOnly = true;
            this.DataGridCommon.RowHeadersVisible = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.DataGridCommon.RowsDefaultCellStyle = dataGridViewCellStyle1;
            this.DataGridCommon.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridCommon.Size = new System.Drawing.Size(430, 168);
            this.DataGridCommon.TabIndex = 0;
            // 
            // HFIT
            // 
            this.HFIT.BackgroundColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.HFIT.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.HFIT.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.HFIT.Location = new System.Drawing.Point(-7, 55);
            this.HFIT.Name = "HFIT";
            this.HFIT.Size = new System.Drawing.Size(979, 347);
            this.HFIT.TabIndex = 177;
            // 
            // cbosGReturnItem
            // 
            this.cbosGReturnItem.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbosGReturnItem.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbosGReturnItem.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbosGReturnItem.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbosGReturnItem.FormattingEnabled = true;
            this.cbosGReturnItem.Location = new System.Drawing.Point(634, 142);
            this.cbosGReturnItem.Name = "cbosGReturnItem";
            this.cbosGReturnItem.Size = new System.Drawing.Size(312, 26);
            this.cbosGReturnItem.TabIndex = 4;
            this.cbosGReturnItem.SelectedIndexChanged += new System.EventHandler(this.cbosGReturnItem_SelectedIndexChanged);
            this.cbosGReturnItem.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cbosGReturnItem_KeyPress);
            // 
            // cbotype
            // 
            this.cbotype.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbotype.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbotype.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbotype.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbotype.FormattingEnabled = true;
            this.cbotype.Items.AddRange(new object[] {
            "Yarn",
            "Fabric",
            "Trims"});
            this.cbotype.Location = new System.Drawing.Point(75, 21);
            this.cbotype.Name = "cbotype";
            this.cbotype.Size = new System.Drawing.Size(223, 26);
            this.cbotype.TabIndex = 122;
            this.cbotype.SelectedIndexChanged += new System.EventHandler(this.cbotype_SelectedIndexChanged);
            // 
            // btnadd
            // 
            this.btnadd.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnadd.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnadd.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnadd.Location = new System.Drawing.Point(912, 137);
            this.btnadd.Name = "btnadd";
            this.btnadd.Size = new System.Drawing.Size(34, 32);
            this.btnadd.TabIndex = 5;
            this.btnadd.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnadd.UseVisualStyleBackColor = false;
            this.btnadd.Click += new System.EventHandler(this.btnadd_Click);
            this.btnadd.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.btnadd_KeyPress);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(15, 22);
            this.label19.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(42, 21);
            this.label19.TabIndex = 12;
            this.label19.Text = "Type";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(18, 117);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(55, 21);
            this.label9.TabIndex = 173;
            this.label9.Text = "SeqNo";
            // 
            // txtaddnotes
            // 
            this.txtaddnotes.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtaddnotes.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtaddnotes.Location = new System.Drawing.Point(127, 142);
            this.txtaddnotes.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtaddnotes.Name = "txtaddnotes";
            this.txtaddnotes.Size = new System.Drawing.Size(271, 23);
            this.txtaddnotes.TabIndex = 1;
            this.txtaddnotes.Click += new System.EventHandler(this.txtaddnotes_Click);
            this.txtaddnotes.TextChanged += new System.EventHandler(this.txtaddnotes_TextChanged);
            this.txtaddnotes.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtaddnotes_KeyDown);
            this.txtaddnotes.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtaddnotes_KeyPress);
            // 
            // txtdcqty
            // 
            this.txtdcqty.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtdcqty.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.txtdcqty.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.txtdcqty.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtdcqty.FormattingEnabled = true;
            this.txtdcqty.Items.AddRange(new object[] {
            "10",
            "20",
            "30",
            "40",
            "50",
            "60",
            "70",
            "80",
            "90"});
            this.txtdcqty.Location = new System.Drawing.Point(13, 141);
            this.txtdcqty.Name = "txtdcqty";
            this.txtdcqty.Size = new System.Drawing.Size(107, 26);
            this.txtdcqty.TabIndex = 0;
            this.txtdcqty.SelectedIndexChanged += new System.EventHandler(this.txtdcqty_SelectedIndexChanged);
            this.txtdcqty.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtdcqty_KeyPress_1);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(131, 121);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(108, 21);
            this.label12.TabIndex = 175;
            this.label12.Text = "Process Name";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(630, 118);
            this.label18.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(111, 21);
            this.label18.TabIndex = 426;
            this.label18.Text = "Process Group";
            // 
            // txtprocessdet
            // 
            this.txtprocessdet.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtprocessdet.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtprocessdet.Location = new System.Drawing.Point(406, 143);
            this.txtprocessdet.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtprocessdet.Name = "txtprocessdet";
            this.txtprocessdet.Size = new System.Drawing.Size(221, 23);
            this.txtprocessdet.TabIndex = 3;
            this.txtprocessdet.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtprocessdet_KeyDown);
            this.txtprocessdet.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtprocessdet_KeyPress);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(402, 121);
            this.label17.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(114, 21);
            this.label17.TabIndex = 192;
            this.label17.Text = "Process Details";
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.HFIT1);
            this.tabPage1.Location = new System.Drawing.Point(4, 24);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(986, 408);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Process Details";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // HFIT1
            // 
            this.HFIT1.BackgroundColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.HFIT1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.HFIT1.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.HFIT1.Location = new System.Drawing.Point(5, 6);
            this.HFIT1.Name = "HFIT1";
            this.HFIT1.Size = new System.Drawing.Size(977, 396);
            this.HFIT1.TabIndex = 178;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.Pnlin);
            this.tabPage3.Controls.Add(this.HFITIN1);
            this.tabPage3.Controls.Add(this.button5);
            this.tabPage3.Controls.Add(this.TXTIN);
            this.tabPage3.Controls.Add(this.label22);
            this.tabPage3.Controls.Add(this.TXTINQTY);
            this.tabPage3.Controls.Add(this.label23);
            this.tabPage3.Controls.Add(this.label20);
            this.tabPage3.Controls.Add(this.txtbeam);
            this.tabPage3.Controls.Add(this.txtsamqty);
            this.tabPage3.Controls.Add(this.HFIPOUT);
            this.tabPage3.Controls.Add(this.type);
            this.tabPage3.Controls.Add(this.cboin);
            this.tabPage3.Controls.Add(this.label26);
            this.tabPage3.Controls.Add(this.textBox4);
            this.tabPage3.Location = new System.Drawing.Point(4, 24);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(986, 408);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Fabric Details";
            this.tabPage3.UseVisualStyleBackColor = true;
            this.tabPage3.Click += new System.EventHandler(this.tabPage3_Click);
            // 
            // Pnlin
            // 
            this.Pnlin.BackColor = System.Drawing.Color.White;
            this.Pnlin.Controls.Add(this.button2);
            this.Pnlin.Controls.Add(this.button4);
            this.Pnlin.Controls.Add(this.HFITIN);
            this.Pnlin.Location = new System.Drawing.Point(16, 120);
            this.Pnlin.Name = "Pnlin";
            this.Pnlin.Size = new System.Drawing.Size(432, 216);
            this.Pnlin.TabIndex = 440;
            this.Pnlin.Visible = false;
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button2.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button2.Location = new System.Drawing.Point(318, 185);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(100, 28);
            this.button2.TabIndex = 394;
            this.button2.Text = "Select (F2)";
            this.button2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button4.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.Image = ((System.Drawing.Image)(resources.GetObject("button4.Image")));
            this.button4.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button4.Location = new System.Drawing.Point(3, 185);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(100, 27);
            this.button4.TabIndex = 393;
            this.button4.Text = "Close (F10)";
            this.button4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // HFITIN
            // 
            this.HFITIN.AllowUserToAddRows = false;
            this.HFITIN.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.HFITIN.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.HFITIN.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.HFITIN.Location = new System.Drawing.Point(5, 14);
            this.HFITIN.Name = "HFITIN";
            this.HFITIN.ReadOnly = true;
            this.HFITIN.RowHeadersVisible = false;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.HFITIN.RowsDefaultCellStyle = dataGridViewCellStyle2;
            this.HFITIN.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.HFITIN.Size = new System.Drawing.Size(413, 168);
            this.HFITIN.TabIndex = 0;
            // 
            // HFITIN1
            // 
            this.HFITIN1.BackgroundColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.HFITIN1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.HFITIN1.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.HFITIN1.Location = new System.Drawing.Point(3, 15);
            this.HFITIN1.Name = "HFITIN1";
            this.HFITIN1.Size = new System.Drawing.Size(983, 370);
            this.HFITIN1.TabIndex = 437;
            this.HFITIN1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.HFITIN1_KeyDown);
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button5.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button5.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button5.Location = new System.Drawing.Point(485, 88);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(34, 32);
            this.button5.TabIndex = 2;
            this.button5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // TXTIN
            // 
            this.TXTIN.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TXTIN.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TXTIN.Location = new System.Drawing.Point(11, 95);
            this.TXTIN.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.TXTIN.Name = "TXTIN";
            this.TXTIN.Size = new System.Drawing.Size(412, 23);
            this.TXTIN.TabIndex = 0;
            this.TXTIN.Click += new System.EventHandler(this.TXTIN_Click);
            this.TXTIN.TextChanged += new System.EventHandler(this.TXTIN_TextChanged);
            this.TXTIN.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtaddnotes_KeyDown);
            this.TXTIN.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TXTIN_KeyPress);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(8, 74);
            this.label22.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(97, 21);
            this.label22.TabIndex = 436;
            this.label22.Text = "Fabric Name";
            // 
            // TXTINQTY
            // 
            this.TXTINQTY.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TXTINQTY.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TXTINQTY.Location = new System.Drawing.Point(424, 95);
            this.TXTINQTY.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.TXTINQTY.Name = "TXTINQTY";
            this.TXTINQTY.Size = new System.Drawing.Size(54, 23);
            this.TXTINQTY.TabIndex = 1;
            this.TXTINQTY.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TXTINQTY_KeyPress);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(420, 69);
            this.label23.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(35, 21);
            this.label23.TabIndex = 438;
            this.label23.Text = "Qty";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(555, 192);
            this.label20.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(63, 21);
            this.label20.TabIndex = 430;
            this.label20.Text = "Process";
            // 
            // txtbeam
            // 
            this.txtbeam.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtbeam.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtbeam.Location = new System.Drawing.Point(251, 342);
            this.txtbeam.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtbeam.Name = "txtbeam";
            this.txtbeam.Size = new System.Drawing.Size(85, 23);
            this.txtbeam.TabIndex = 446;
            // 
            // txtsamqty
            // 
            this.txtsamqty.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtsamqty.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtsamqty.Location = new System.Drawing.Point(585, 121);
            this.txtsamqty.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtsamqty.Name = "txtsamqty";
            this.txtsamqty.Size = new System.Drawing.Size(54, 23);
            this.txtsamqty.TabIndex = 455;
            // 
            // HFIPOUT
            // 
            this.HFIPOUT.BackgroundColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.HFIPOUT.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.HFIPOUT.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.HFIPOUT.Location = new System.Drawing.Point(537, 62);
            this.HFIPOUT.Name = "HFIPOUT";
            this.HFIPOUT.Size = new System.Drawing.Size(446, 49);
            this.HFIPOUT.TabIndex = 451;
            this.HFIPOUT.Visible = false;
            // 
            // type
            // 
            this.type.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.type.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.type.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.type.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.type.FormattingEnabled = true;
            this.type.Location = new System.Drawing.Point(628, 74);
            this.type.Name = "type";
            this.type.Size = new System.Drawing.Size(252, 26);
            this.type.TabIndex = 444;
            // 
            // cboin
            // 
            this.cboin.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboin.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboin.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboin.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboin.FormattingEnabled = true;
            this.cboin.Location = new System.Drawing.Point(628, 73);
            this.cboin.Name = "cboin";
            this.cboin.Size = new System.Drawing.Size(252, 26);
            this.cboin.TabIndex = 443;
            this.cboin.SelectedIndexChanged += new System.EventHandler(this.cboin_SelectedIndexChanged);
            this.cboin.Click += new System.EventHandler(this.cboin_Click);
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(685, 72);
            this.label26.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(42, 21);
            this.label26.TabIndex = 445;
            this.label26.Text = "Type";
            // 
            // textBox4
            // 
            this.textBox4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox4.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox4.Location = new System.Drawing.Point(735, 77);
            this.textBox4.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(85, 23);
            this.textBox4.TabIndex = 454;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.label27);
            this.tabPage4.Controls.Add(this.cbotypeout);
            this.tabPage4.Controls.Add(this.Pnlout);
            this.tabPage4.Controls.Add(this.HFITOUT1);
            this.tabPage4.Controls.Add(this.button8);
            this.tabPage4.Controls.Add(this.TXTOUT);
            this.tabPage4.Controls.Add(this.label21);
            this.tabPage4.Controls.Add(this.TXTOUTQTY);
            this.tabPage4.Controls.Add(this.label24);
            this.tabPage4.Controls.Add(this.cboout);
            this.tabPage4.Controls.Add(this.label25);
            this.tabPage4.Location = new System.Drawing.Point(4, 24);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(986, 408);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "OutPutItems";
            this.tabPage4.UseVisualStyleBackColor = true;
            this.tabPage4.Click += new System.EventHandler(this.tabPage4_Click);
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(522, 15);
            this.label27.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(42, 21);
            this.label27.TabIndex = 451;
            this.label27.Text = "Type";
            // 
            // cbotypeout
            // 
            this.cbotypeout.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbotypeout.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbotypeout.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbotypeout.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbotypeout.FormattingEnabled = true;
            this.cbotypeout.Location = new System.Drawing.Point(571, 14);
            this.cbotypeout.Name = "cbotypeout";
            this.cbotypeout.Size = new System.Drawing.Size(252, 26);
            this.cbotypeout.TabIndex = 450;
            // 
            // Pnlout
            // 
            this.Pnlout.BackColor = System.Drawing.Color.White;
            this.Pnlout.Controls.Add(this.button6);
            this.Pnlout.Controls.Add(this.button7);
            this.Pnlout.Controls.Add(this.HFITOUT);
            this.Pnlout.Location = new System.Drawing.Point(301, 117);
            this.Pnlout.Name = "Pnlout";
            this.Pnlout.Size = new System.Drawing.Size(445, 216);
            this.Pnlout.TabIndex = 449;
            this.Pnlout.Visible = false;
            // 
            // button6
            // 
            this.button6.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button6.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button6.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button6.Location = new System.Drawing.Point(227, 183);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(100, 28);
            this.button6.TabIndex = 394;
            this.button6.Text = "Select (F2)";
            this.button6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button6.UseVisualStyleBackColor = false;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // button7
            // 
            this.button7.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button7.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button7.Image = ((System.Drawing.Image)(resources.GetObject("button7.Image")));
            this.button7.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button7.Location = new System.Drawing.Point(333, 188);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(100, 27);
            this.button7.TabIndex = 393;
            this.button7.Text = "Close (F10)";
            this.button7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button7.UseVisualStyleBackColor = false;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // HFITOUT
            // 
            this.HFITOUT.AllowUserToAddRows = false;
            this.HFITOUT.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.HFITOUT.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.HFITOUT.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.HFITOUT.Location = new System.Drawing.Point(5, 14);
            this.HFITOUT.Name = "HFITOUT";
            this.HFITOUT.ReadOnly = true;
            this.HFITOUT.RowHeadersVisible = false;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.HFITOUT.RowsDefaultCellStyle = dataGridViewCellStyle3;
            this.HFITOUT.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.HFITOUT.Size = new System.Drawing.Size(430, 168);
            this.HFITOUT.TabIndex = 0;
            this.HFITOUT.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView3_CellContentClick);
            // 
            // HFITOUT1
            // 
            this.HFITOUT1.BackgroundColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.HFITOUT1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.HFITOUT1.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.HFITOUT1.Location = new System.Drawing.Point(194, 95);
            this.HFITOUT1.Name = "HFITOUT1";
            this.HFITOUT1.Size = new System.Drawing.Size(561, 303);
            this.HFITOUT1.TabIndex = 447;
            // 
            // button8
            // 
            this.button8.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button8.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button8.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button8.Location = new System.Drawing.Point(758, 60);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(34, 32);
            this.button8.TabIndex = 445;
            this.button8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button8.UseVisualStyleBackColor = false;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // TXTOUT
            // 
            this.TXTOUT.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TXTOUT.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TXTOUT.Location = new System.Drawing.Point(194, 68);
            this.TXTOUT.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.TXTOUT.Name = "TXTOUT";
            this.TXTOUT.Size = new System.Drawing.Size(464, 23);
            this.TXTOUT.TabIndex = 443;
            this.TXTOUT.Click += new System.EventHandler(this.TXTOUT_Click);
            this.TXTOUT.TextChanged += new System.EventHandler(this.TXTOUT_TextChanged);
            this.TXTOUT.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TXTOUT_KeyDown);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(198, 47);
            this.label21.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(87, 21);
            this.label21.TabIndex = 446;
            this.label21.Text = "Item Name";
            // 
            // TXTOUTQTY
            // 
            this.TXTOUTQTY.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TXTOUTQTY.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TXTOUTQTY.Location = new System.Drawing.Point(666, 69);
            this.TXTOUTQTY.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.TXTOUTQTY.Name = "TXTOUTQTY";
            this.TXTOUTQTY.Size = new System.Drawing.Size(85, 23);
            this.TXTOUTQTY.TabIndex = 444;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(662, 43);
            this.label24.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(35, 21);
            this.label24.TabIndex = 448;
            this.label24.Text = "Qty";
            // 
            // cboout
            // 
            this.cboout.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboout.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboout.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboout.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboout.FormattingEnabled = true;
            this.cboout.Location = new System.Drawing.Point(170, 13);
            this.cboout.Name = "cboout";
            this.cboout.Size = new System.Drawing.Size(342, 26);
            this.cboout.TabIndex = 442;
            this.cboout.SelectedIndexChanged += new System.EventHandler(this.cboout_SelectedIndexChanged);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(105, 14);
            this.label25.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(63, 21);
            this.label25.TabIndex = 441;
            this.label25.Text = "Process";
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.YARNLOAD);
            this.tabPage5.Controls.Add(this.label30);
            this.tabPage5.Controls.Add(this.label29);
            this.tabPage5.Controls.Add(this.button11);
            this.tabPage5.Controls.Add(this.textBox3);
            this.tabPage5.Controls.Add(this.txtcolor);
            this.tabPage5.Controls.Add(this.YARNCOLORLOAD);
            this.tabPage5.Controls.Add(this.label28);
            this.tabPage5.Controls.Add(this.cbofabric);
            this.tabPage5.Location = new System.Drawing.Point(4, 24);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Size = new System.Drawing.Size(986, 408);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "Yarn";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // YARNLOAD
            // 
            this.YARNLOAD.BackgroundColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.YARNLOAD.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.YARNLOAD.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnKeystroke;
            this.YARNLOAD.Location = new System.Drawing.Point(2, 49);
            this.YARNLOAD.Name = "YARNLOAD";
            this.YARNLOAD.Size = new System.Drawing.Size(515, 317);
            this.YARNLOAD.TabIndex = 459;
            this.YARNLOAD.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.YARNLOAD_CellClick_2);
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(705, 3);
            this.label30.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(35, 21);
            this.label30.TabIndex = 458;
            this.label30.Text = "Qty";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(529, 3);
            this.label29.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(47, 21);
            this.label29.TabIndex = 457;
            this.label29.Text = "Color";
            // 
            // button11
            // 
            this.button11.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button11.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button11.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button11.Location = new System.Drawing.Point(839, 22);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(34, 32);
            this.button11.TabIndex = 456;
            this.button11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button11.UseVisualStyleBackColor = false;
            this.button11.Click += new System.EventHandler(this.button11_Click_1);
            // 
            // textBox3
            // 
            this.textBox3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox3.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox3.Location = new System.Drawing.Point(707, 29);
            this.textBox3.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(116, 23);
            this.textBox3.TabIndex = 455;
            // 
            // txtcolor
            // 
            this.txtcolor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtcolor.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtcolor.Location = new System.Drawing.Point(533, 29);
            this.txtcolor.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtcolor.Name = "txtcolor";
            this.txtcolor.Size = new System.Drawing.Size(166, 23);
            this.txtcolor.TabIndex = 454;
            // 
            // YARNCOLORLOAD
            // 
            this.YARNCOLORLOAD.BackgroundColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.YARNCOLORLOAD.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.YARNCOLORLOAD.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.YARNCOLORLOAD.Location = new System.Drawing.Point(533, 67);
            this.YARNCOLORLOAD.Name = "YARNCOLORLOAD";
            this.YARNCOLORLOAD.Size = new System.Drawing.Size(446, 299);
            this.YARNCOLORLOAD.TabIndex = 453;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(16, 17);
            this.label28.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(56, 21);
            this.label28.TabIndex = 174;
            this.label28.Text = "Fabric ";
            // 
            // cbofabric
            // 
            this.cbofabric.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbofabric.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbofabric.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbofabric.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbofabric.FormattingEnabled = true;
            this.cbofabric.Items.AddRange(new object[] {
            "10",
            "20",
            "30",
            "40",
            "50",
            "60",
            "70",
            "80",
            "90"});
            this.cbofabric.Location = new System.Drawing.Point(79, 17);
            this.cbofabric.Name = "cbofabric";
            this.cbofabric.Size = new System.Drawing.Size(438, 26);
            this.cbofabric.TabIndex = 1;
            this.cbofabric.SelectedIndexChanged += new System.EventHandler(this.cbofabric_SelectedIndexChanged);
            // 
            // grNewSearch
            // 
            this.grNewSearch.Controls.Add(this.button22);
            this.grNewSearch.Controls.Add(this.btnSelect);
            this.grNewSearch.Controls.Add(this.DataGridCommonNew);
            this.grNewSearch.Controls.Add(this.button10);
            this.grNewSearch.Location = new System.Drawing.Point(668, 224);
            this.grNewSearch.Name = "grNewSearch";
            this.grNewSearch.Size = new System.Drawing.Size(346, 226);
            this.grNewSearch.TabIndex = 1;
            this.grNewSearch.TabStop = false;
            this.grNewSearch.Visible = false;
            // 
            // button22
            // 
            this.button22.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button22.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button22.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button22.Location = new System.Drawing.Point(136, 192);
            this.button22.Name = "button22";
            this.button22.Size = new System.Drawing.Size(109, 28);
            this.button22.TabIndex = 397;
            this.button22.Text = "Add Item";
            this.button22.UseVisualStyleBackColor = false;
            this.button22.Click += new System.EventHandler(this.button22_Click);
            // 
            // btnSelect
            // 
            this.btnSelect.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnSelect.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSelect.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnSelect.Location = new System.Drawing.Point(281, 197);
            this.btnSelect.Name = "btnSelect";
            this.btnSelect.Size = new System.Drawing.Size(61, 28);
            this.btnSelect.TabIndex = 396;
            this.btnSelect.Text = "Select";
            this.btnSelect.UseVisualStyleBackColor = false;
            this.btnSelect.Click += new System.EventHandler(this.BtnSelect_Click);
            // 
            // DataGridCommonNew
            // 
            this.DataGridCommonNew.AllowUserToAddRows = false;
            this.DataGridCommonNew.BackgroundColor = System.Drawing.Color.White;
            this.DataGridCommonNew.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridCommonNew.Location = new System.Drawing.Point(4, 12);
            this.DataGridCommonNew.MultiSelect = false;
            this.DataGridCommonNew.Name = "DataGridCommonNew";
            this.DataGridCommonNew.ReadOnly = true;
            this.DataGridCommonNew.RowHeadersVisible = false;
            this.DataGridCommonNew.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridCommonNew.Size = new System.Drawing.Size(336, 183);
            this.DataGridCommonNew.TabIndex = 0;
            this.DataGridCommonNew.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridCommonNew_CellContentClick);
            this.DataGridCommonNew.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.DataGridCommonNew_CellMouseDoubleClick);
            this.DataGridCommonNew.Click += new System.EventHandler(this.DataGridCommonNew_Click);
            this.DataGridCommonNew.DoubleClick += new System.EventHandler(this.DataGridCommonNew_DoubleClick);
            this.DataGridCommonNew.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DataGridCommonNew_KeyDown);
            // 
            // button10
            // 
            this.button10.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button10.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button10.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button10.Location = new System.Drawing.Point(6, 197);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(65, 28);
            this.button10.TabIndex = 395;
            this.button10.Text = "Close";
            this.button10.UseVisualStyleBackColor = false;
            this.button10.Click += new System.EventHandler(this.button10_Click);
            // 
            // Editpan
            // 
            this.Editpan.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.Editpan.Controls.Add(this.label31);
            this.Editpan.Controls.Add(this.textBox2);
            this.Editpan.Controls.Add(this.grNewSearch);
            this.Editpan.Controls.Add(this.tabControl1);
            this.Editpan.Controls.Add(this.Reqpan);
            this.Editpan.Controls.Add(this.label10);
            this.Editpan.Controls.Add(this.txtqty);
            this.Editpan.Controls.Add(this.label7);
            this.Editpan.Controls.Add(this.txtitem);
            this.Editpan.Controls.Add(this.Dtpdt);
            this.Editpan.Controls.Add(this.label6);
            this.Editpan.Controls.Add(this.label5);
            this.Editpan.Controls.Add(this.txtdcno);
            this.Editpan.Controls.Add(this.label2);
            this.Editpan.Controls.Add(this.label4);
            this.Editpan.Controls.Add(this.txtnar);
            this.Editpan.Controls.Add(this.label3);
            this.Editpan.Controls.Add(this.txtname);
            this.Editpan.Controls.Add(this.Phone);
            this.Editpan.Controls.Add(this.txtgrn);
            this.Editpan.Controls.Add(this.txtititd);
            this.Editpan.Controls.Add(this.txtpuid);
            this.Editpan.Controls.Add(this.txtgrnid);
            this.Editpan.Controls.Add(this.txtgrndt);
            this.Editpan.Controls.Add(this.dtpgrndt);
            this.Editpan.Controls.Add(this.txtjjno);
            this.Editpan.Controls.Add(this.label15);
            this.Editpan.Controls.Add(this.txtpono);
            this.Editpan.Controls.Add(this.txtpoid);
            this.Editpan.Controls.Add(this.label8);
            this.Editpan.Controls.Add(this.label16);
            this.Editpan.Controls.Add(this.txtrecqty);
            this.Editpan.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Editpan.Location = new System.Drawing.Point(0, -3);
            this.Editpan.Name = "Editpan";
            this.Editpan.Size = new System.Drawing.Size(1004, 500);
            this.Editpan.TabIndex = 210;
            this.Editpan.Paint += new System.Windows.Forms.PaintEventHandler(this.Editpan_Paint);
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(262, 15);
            this.label31.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(54, 21);
            this.label31.TabIndex = 13;
            this.label31.Text = "SocNo";
            this.label31.Click += new System.EventHandler(this.label31_Click);
            // 
            // textBox2
            // 
            this.textBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox2.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox2.Location = new System.Drawing.Point(257, 37);
            this.textBox2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(110, 23);
            this.textBox2.TabIndex = 14;
            this.textBox2.TabStop = false;
            this.textBox2.Click += new System.EventHandler(this.textBox2_Click);
            this.textBox2.TextChanged += new System.EventHandler(this.textBox2_TextChanged);
            this.textBox2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBox2_KeyDown);
            // 
            // FrmWorkOrder
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1005, 530);
            this.Controls.Add(this.panadd);
            this.Controls.Add(this.btnaddrcan);
            this.Controls.Add(this.btnsave);
            this.Controls.Add(this.button9);
            this.Controls.Add(this.Genpan);
            this.Controls.Add(this.Editpan);
            this.Name = "FrmWorkOrder";
            this.Text = "WorkOrder";
            this.Load += new System.EventHandler(this.FrmWorkOrder_Load);
            this.Genpan.ResumeLayout(false);
            this.Genpan.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HFGP)).EndInit();
            this.panadd.ResumeLayout(false);
            this.panadd.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.Reqpan.ResumeLayout(false);
            this.Reqpan.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RQGR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkgrd)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.grSearch.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DataGridCommon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HFIT)).EndInit();
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.HFIT1)).EndInit();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.Pnlin.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.HFITIN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HFITIN1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HFIPOUT)).EndInit();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            this.Pnlout.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.HFITOUT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HFITOUT1)).EndInit();
            this.tabPage5.ResumeLayout(false);
            this.tabPage5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.YARNLOAD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.YARNCOLORLOAD)).EndInit();
            this.grNewSearch.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DataGridCommonNew)).EndInit();
            this.Editpan.ResumeLayout(false);
            this.Editpan.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel Genpan;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtscr6;
        private System.Windows.Forms.TextBox txtscr5;
        private System.Windows.Forms.TextBox txtscr4;
        private System.Windows.Forms.TextBox Txtscr3;
        private System.Windows.Forms.TextBox Txtscr2;
        private System.Windows.Forms.TextBox txtscr1;
        private System.Windows.Forms.DataGridView HFGP;
        private System.Windows.Forms.Panel panadd;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblno1;
        private System.Windows.Forms.Label lblno2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Button buttnnvfst;
        private System.Windows.Forms.Button buttnnxtlft;
        private System.Windows.Forms.Button btnfinnxt;
        private System.Windows.Forms.Button buttrnxt;
        private System.Windows.Forms.Button buttnext1;
        private System.Windows.Forms.CheckBox chkact;
        private System.Windows.Forms.Button butexit;
        private System.Windows.Forms.Button butedit;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btnaddrcan;
        private System.Windows.Forms.Button btnsave;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox txtrecqty;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtpoid;
        private System.Windows.Forms.TextBox txtpono;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtjjno;
        private System.Windows.Forms.DateTimePicker dtpgrndt;
        private System.Windows.Forms.TextBox txtgrndt;
        private System.Windows.Forms.TextBox txtgrnid;
        private System.Windows.Forms.TextBox txtpuid;
        private System.Windows.Forms.TextBox txtititd;
        private System.Windows.Forms.TextBox txtgrn;
        private System.Windows.Forms.Label Phone;
        private System.Windows.Forms.TextBox txtname;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtnar;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtdcno;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DateTimePicker Dtpdt;
        private System.Windows.Forms.TextBox txtitem;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtqty;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Panel Reqpan;
        private System.Windows.Forms.TextBox txtreqqty;
        private System.Windows.Forms.Button Reqbk;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.DataGridView RQGR;
        private System.Windows.Forms.Button buttrqok;
        private System.Windows.Forms.DateTimePicker Dtpreq;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox Cboit;
        private System.Windows.Forms.TextBox txtqty1;
        private System.Windows.Forms.TextBox txtqty2;
        private System.Windows.Forms.DataGridView chkgrd;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Panel grSearch;
        private System.Windows.Forms.Button button18;
        private System.Windows.Forms.Button btnHide;
        private System.Windows.Forms.DataGridView DataGridCommon;
        private System.Windows.Forms.DataGridView HFIT;
        private System.Windows.Forms.ComboBox cbosGReturnItem;
        private System.Windows.Forms.ComboBox cbotype;
        private System.Windows.Forms.Button btnadd;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtaddnotes;
        private System.Windows.Forms.ComboBox txtdcqty;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txtprocessdet;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.DataGridView HFIT1;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.DataGridView HFIPOUT;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.ComboBox type;
        private System.Windows.Forms.ComboBox cboin;
        private System.Windows.Forms.Panel Pnlin;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.DataGridView HFITIN;
        private System.Windows.Forms.DataGridView HFITIN1;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.TextBox TXTIN;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox TXTINQTY;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox txtbeam;
        private System.Windows.Forms.TextBox txtsamqty;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.ComboBox cbotypeout;
        private System.Windows.Forms.Panel Pnlout;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.DataGridView HFITOUT;
        private System.Windows.Forms.DataGridView HFITOUT1;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.TextBox TXTOUT;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox TXTOUTQTY;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.ComboBox cboout;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.GroupBox grNewSearch;
        private System.Windows.Forms.Button btnSelect;
        private System.Windows.Forms.DataGridView DataGridCommonNew;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Panel Editpan;
        private System.Windows.Forms.Button button22;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.ComboBox cbofabric;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox txtcolor;
        private System.Windows.Forms.DataGridView YARNCOLORLOAD;
        private System.Windows.Forms.DataGridView YARNLOAD;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.ComboBox cboitem;
        private System.Windows.Forms.Label label32;
    }
}