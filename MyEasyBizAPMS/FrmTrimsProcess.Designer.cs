﻿namespace MyEasyBizAPMS
{
    partial class FrmTrimsProcess
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Syncfusion.Windows.Forms.Tools.SplitButtonRenderer splitButtonRenderer7 = new Syncfusion.Windows.Forms.Tools.SplitButtonRenderer();
            Syncfusion.Windows.Forms.Tools.SplitButtonRenderer splitButtonRenderer8 = new Syncfusion.Windows.Forms.Tools.SplitButtonRenderer();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            this.SplitSave = new Syncfusion.Windows.Forms.Tools.SplitButton();
            this.toolStripBack = new Syncfusion.Windows.Forms.Tools.toolstripitem();
            this.SplitAdd = new Syncfusion.Windows.Forms.Tools.SplitButton();
            this.toolstripEdit = new Syncfusion.Windows.Forms.Tools.toolstripitem();
            this.toolstripClose = new Syncfusion.Windows.Forms.Tools.toolstripitem();
            this.GrBack = new System.Windows.Forms.GroupBox();
            this.conpnl = new System.Windows.Forms.Panel();
            this.button3 = new System.Windows.Forms.Button();
            this.trimsload = new System.Windows.Forms.DataGridView();
            this.txtconitems = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.label13 = new System.Windows.Forms.Label();
            this.uom = new System.Windows.Forms.ComboBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.txtconvertedtqty = new System.Windows.Forms.TextBox();
            this.txtconqty = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.trimsitem = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.CmbUom = new System.Windows.Forms.ComboBox();
            this.grSearch = new System.Windows.Forms.GroupBox();
            this.btnSelect = new System.Windows.Forms.Button();
            this.DataGridCommon = new System.Windows.Forms.DataGridView();
            this.btnHide = new System.Windows.Forms.Button();
            this.DataGridTrimsAdd = new System.Windows.Forms.DataGridView();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.BtnOk = new System.Windows.Forms.Button();
            this.txtProcessLoss = new System.Windows.Forms.TextBox();
            this.txtRate = new System.Windows.Forms.TextBox();
            this.txtQty = new System.Windows.Forms.TextBox();
            this.txtColour = new System.Windows.Forms.TextBox();
            this.txtProcess = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.CmbItem = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.CmbStyle = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.DtpDocDate = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.DtpOrderDate = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtDocNo = new System.Windows.Forms.TextBox();
            this.txtOrderNo = new System.Windows.Forms.TextBox();
            this.txtmaterial = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.GrFront = new System.Windows.Forms.GroupBox();
            this.SfdDataGridTrims = new Syncfusion.WinForms.DataGrid.SfDataGrid();
            this.GrBack.SuspendLayout();
            this.conpnl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trimsload)).BeginInit();
            this.grSearch.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridCommon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridTrimsAdd)).BeginInit();
            this.GrFront.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SfdDataGridTrims)).BeginInit();
            this.SuspendLayout();
            // 
            // SplitSave
            // 
            this.SplitSave.BackColor = System.Drawing.SystemColors.Control;
            this.SplitSave.BeforeTouchSize = new System.Drawing.Size(98, 37);
            this.SplitSave.DropDownItems.Add(this.toolStripBack);
            this.SplitSave.DropDownPosition = Syncfusion.Windows.Forms.Tools.Position.Bottom;
            this.SplitSave.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SplitSave.ForeColor = System.Drawing.Color.Black;
            this.SplitSave.Location = new System.Drawing.Point(889, 546);
            this.SplitSave.MinimumSize = new System.Drawing.Size(75, 23);
            this.SplitSave.Name = "SplitSave";
            splitButtonRenderer7.SplitButton = this.SplitSave;
            this.SplitSave.Renderer = splitButtonRenderer7;
            this.SplitSave.ShowDropDownOnButtonClick = false;
            this.SplitSave.Size = new System.Drawing.Size(98, 37);
            this.SplitSave.TabIndex = 0;
            this.SplitSave.Text = "Save";
            this.SplitSave.DropDowItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.SplitSave_DropDowItemClicked);
            this.SplitSave.Click += new System.EventHandler(this.SplitSave_Click);
            // 
            // toolStripBack
            // 
            this.toolStripBack.Name = "toolStripBack";
            this.toolStripBack.Size = new System.Drawing.Size(23, 23);
            this.toolStripBack.Text = "Back";
            // 
            // SplitAdd
            // 
            this.SplitAdd.BackColor = System.Drawing.SystemColors.Control;
            this.SplitAdd.BeforeTouchSize = new System.Drawing.Size(87, 36);
            this.SplitAdd.DropDownItems.Add(this.toolstripEdit);
            this.SplitAdd.DropDownItems.Add(this.toolstripClose);
            this.SplitAdd.DropDownPosition = Syncfusion.Windows.Forms.Tools.Position.Bottom;
            this.SplitAdd.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SplitAdd.ForeColor = System.Drawing.Color.Black;
            this.SplitAdd.Location = new System.Drawing.Point(915, 546);
            this.SplitAdd.MinimumSize = new System.Drawing.Size(75, 23);
            this.SplitAdd.Name = "SplitAdd";
            splitButtonRenderer8.SplitButton = this.SplitAdd;
            this.SplitAdd.Renderer = splitButtonRenderer8;
            this.SplitAdd.ShowDropDownOnButtonClick = false;
            this.SplitAdd.Size = new System.Drawing.Size(87, 36);
            this.SplitAdd.TabIndex = 1;
            this.SplitAdd.Text = "Add";
            this.SplitAdd.DropDowItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.SplitAdd_DropDowItemClicked);
            this.SplitAdd.Click += new System.EventHandler(this.SplitAdd_Click);
            // 
            // toolstripEdit
            // 
            this.toolstripEdit.Name = "toolstripEdit";
            this.toolstripEdit.Size = new System.Drawing.Size(23, 23);
            this.toolstripEdit.Text = "Edit";
            // 
            // toolstripClose
            // 
            this.toolstripClose.Name = "toolstripClose";
            this.toolstripClose.Size = new System.Drawing.Size(23, 23);
            this.toolstripClose.Text = "Close";
            // 
            // GrBack
            // 
            this.GrBack.Controls.Add(this.conpnl);
            this.GrBack.Controls.Add(this.label12);
            this.GrBack.Controls.Add(this.CmbUom);
            this.GrBack.Controls.Add(this.grSearch);
            this.GrBack.Controls.Add(this.DataGridTrimsAdd);
            this.GrBack.Controls.Add(this.label11);
            this.GrBack.Controls.Add(this.label10);
            this.GrBack.Controls.Add(this.label9);
            this.GrBack.Controls.Add(this.label8);
            this.GrBack.Controls.Add(this.label7);
            this.GrBack.Controls.Add(this.BtnOk);
            this.GrBack.Controls.Add(this.txtProcessLoss);
            this.GrBack.Controls.Add(this.txtRate);
            this.GrBack.Controls.Add(this.txtQty);
            this.GrBack.Controls.Add(this.txtColour);
            this.GrBack.Controls.Add(this.txtProcess);
            this.GrBack.Controls.Add(this.label6);
            this.GrBack.Controls.Add(this.CmbItem);
            this.GrBack.Controls.Add(this.label5);
            this.GrBack.Controls.Add(this.CmbStyle);
            this.GrBack.Controls.Add(this.label4);
            this.GrBack.Controls.Add(this.DtpDocDate);
            this.GrBack.Controls.Add(this.label3);
            this.GrBack.Controls.Add(this.DtpOrderDate);
            this.GrBack.Controls.Add(this.label2);
            this.GrBack.Controls.Add(this.label1);
            this.GrBack.Controls.Add(this.txtDocNo);
            this.GrBack.Controls.Add(this.txtOrderNo);
            this.GrBack.Controls.Add(this.SplitSave);
            this.GrBack.Controls.Add(this.txtmaterial);
            this.GrBack.Controls.Add(this.button1);
            this.GrBack.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GrBack.Location = new System.Drawing.Point(8, 2);
            this.GrBack.Name = "GrBack";
            this.GrBack.Size = new System.Drawing.Size(1012, 589);
            this.GrBack.TabIndex = 0;
            this.GrBack.TabStop = false;
            // 
            // conpnl
            // 
            this.conpnl.Controls.Add(this.button3);
            this.conpnl.Controls.Add(this.trimsload);
            this.conpnl.Controls.Add(this.txtconitems);
            this.conpnl.Controls.Add(this.label17);
            this.conpnl.Controls.Add(this.button2);
            this.conpnl.Controls.Add(this.label13);
            this.conpnl.Controls.Add(this.uom);
            this.conpnl.Controls.Add(this.label14);
            this.conpnl.Controls.Add(this.label15);
            this.conpnl.Controls.Add(this.txtconvertedtqty);
            this.conpnl.Controls.Add(this.txtconqty);
            this.conpnl.Controls.Add(this.label16);
            this.conpnl.Controls.Add(this.trimsitem);
            this.conpnl.Location = new System.Drawing.Point(33, 184);
            this.conpnl.Name = "conpnl";
            this.conpnl.Size = new System.Drawing.Size(703, 332);
            this.conpnl.TabIndex = 40;
            // 
            // button3
            // 
            this.button3.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.Location = new System.Drawing.Point(635, 172);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(45, 35);
            this.button3.TabIndex = 51;
            this.button3.Text = "Exit";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // trimsload
            // 
            this.trimsload.AllowUserToAddRows = false;
            this.trimsload.BackgroundColor = System.Drawing.Color.White;
            this.trimsload.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.trimsload.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.trimsload.Location = new System.Drawing.Point(19, 89);
            this.trimsload.Name = "trimsload";
            this.trimsload.ReadOnly = true;
            this.trimsload.RowHeadersVisible = false;
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.trimsload.RowsDefaultCellStyle = dataGridViewCellStyle7;
            this.trimsload.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.trimsload.Size = new System.Drawing.Size(599, 225);
            this.trimsload.TabIndex = 50;
            // 
            // txtconitems
            // 
            this.txtconitems.Location = new System.Drawing.Point(71, 16);
            this.txtconitems.Name = "txtconitems";
            this.txtconitems.Size = new System.Drawing.Size(547, 23);
            this.txtconitems.TabIndex = 49;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(3, 19);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(65, 15);
            this.label17.TabIndex = 48;
            this.label17.Text = "Item Name";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(635, 58);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(36, 25);
            this.button2.TabIndex = 46;
            this.button2.Text = "Ok";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(385, 42);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(35, 15);
            this.label13.TabIndex = 45;
            this.label13.Text = "UOM";
            // 
            // uom
            // 
            this.uom.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.uom.FormattingEnabled = true;
            this.uom.Location = new System.Drawing.Point(385, 60);
            this.uom.Name = "uom";
            this.uom.Size = new System.Drawing.Size(92, 23);
            this.uom.TabIndex = 39;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(550, 42);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(50, 15);
            this.label14.TabIndex = 44;
            this.label14.Text = "Con.Qty";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(479, 42);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(26, 15);
            this.label15.TabIndex = 43;
            this.label15.Text = "Qty";
            // 
            // txtconvertedtqty
            // 
            this.txtconvertedtqty.Location = new System.Drawing.Point(550, 60);
            this.txtconvertedtqty.Name = "txtconvertedtqty";
            this.txtconvertedtqty.Size = new System.Drawing.Size(68, 23);
            this.txtconvertedtqty.TabIndex = 41;
            // 
            // txtconqty
            // 
            this.txtconqty.Location = new System.Drawing.Point(479, 60);
            this.txtconqty.Name = "txtconqty";
            this.txtconqty.Size = new System.Drawing.Size(70, 23);
            this.txtconqty.TabIndex = 40;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(19, 42);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(37, 15);
            this.label16.TabIndex = 42;
            this.label16.Text = "Trims";
            // 
            // trimsitem
            // 
            this.trimsitem.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.trimsitem.FormattingEnabled = true;
            this.trimsitem.Location = new System.Drawing.Point(19, 60);
            this.trimsitem.Name = "trimsitem";
            this.trimsitem.Size = new System.Drawing.Size(362, 23);
            this.trimsitem.TabIndex = 38;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(669, 99);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(35, 15);
            this.label12.TabIndex = 37;
            this.label12.Text = "UOM";
            // 
            // CmbUom
            // 
            this.CmbUom.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CmbUom.FormattingEnabled = true;
            this.CmbUom.Location = new System.Drawing.Point(669, 117);
            this.CmbUom.Name = "CmbUom";
            this.CmbUom.Size = new System.Drawing.Size(92, 23);
            this.CmbUom.TabIndex = 8;
            this.CmbUom.SelectedIndexChanged += new System.EventHandler(this.CmbUom_SelectedIndexChanged);
            // 
            // grSearch
            // 
            this.grSearch.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.grSearch.Controls.Add(this.btnSelect);
            this.grSearch.Controls.Add(this.DataGridCommon);
            this.grSearch.Controls.Add(this.btnHide);
            this.grSearch.Location = new System.Drawing.Point(100, 196);
            this.grSearch.Name = "grSearch";
            this.grSearch.Size = new System.Drawing.Size(346, 229);
            this.grSearch.TabIndex = 35;
            this.grSearch.TabStop = false;
            this.grSearch.Visible = false;
            // 
            // btnSelect
            // 
            this.btnSelect.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnSelect.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSelect.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnSelect.Location = new System.Drawing.Point(281, 197);
            this.btnSelect.Name = "btnSelect";
            this.btnSelect.Size = new System.Drawing.Size(61, 28);
            this.btnSelect.TabIndex = 396;
            this.btnSelect.Text = "Select";
            this.btnSelect.UseVisualStyleBackColor = false;
            this.btnSelect.Click += new System.EventHandler(this.BtnSelect_Click);
            // 
            // DataGridCommon
            // 
            this.DataGridCommon.AllowUserToAddRows = false;
            this.DataGridCommon.BackgroundColor = System.Drawing.Color.White;
            this.DataGridCommon.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridCommon.Location = new System.Drawing.Point(4, 12);
            this.DataGridCommon.MultiSelect = false;
            this.DataGridCommon.Name = "DataGridCommon";
            this.DataGridCommon.ReadOnly = true;
            this.DataGridCommon.RowHeadersVisible = false;
            this.DataGridCommon.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridCommon.Size = new System.Drawing.Size(336, 183);
            this.DataGridCommon.TabIndex = 0;
            this.DataGridCommon.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.DataGridCommon_CellMouseDoubleClick);
            // 
            // btnHide
            // 
            this.btnHide.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnHide.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHide.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnHide.Location = new System.Drawing.Point(6, 197);
            this.btnHide.Name = "btnHide";
            this.btnHide.Size = new System.Drawing.Size(65, 28);
            this.btnHide.TabIndex = 395;
            this.btnHide.Text = "Close";
            this.btnHide.UseVisualStyleBackColor = false;
            this.btnHide.Click += new System.EventHandler(this.BtnHide_Click);
            // 
            // DataGridTrimsAdd
            // 
            this.DataGridTrimsAdd.AllowUserToAddRows = false;
            this.DataGridTrimsAdd.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.Color.DeepSkyBlue;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DataGridTrimsAdd.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle8;
            this.DataGridTrimsAdd.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridTrimsAdd.EnableHeadersVisualStyles = false;
            this.DataGridTrimsAdd.Location = new System.Drawing.Point(6, 146);
            this.DataGridTrimsAdd.Name = "DataGridTrimsAdd";
            this.DataGridTrimsAdd.RowHeadersVisible = false;
            this.DataGridTrimsAdd.Size = new System.Drawing.Size(997, 394);
            this.DataGridTrimsAdd.TabIndex = 24;
            this.DataGridTrimsAdd.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridTrimsAdd_CellClick);
            this.DataGridTrimsAdd.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridTrimsAdd_CellContentClick);
            this.DataGridTrimsAdd.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridTrimsAdd_CellDoubleClick);
            this.DataGridTrimsAdd.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridTrimsAdd_CellValueChanged);
            this.DataGridTrimsAdd.RowPrePaint += new System.Windows.Forms.DataGridViewRowPrePaintEventHandler(this.DataGridTrimsAdd_RowPrePaint);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(903, 99);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(77, 15);
            this.label11.TabIndex = 23;
            this.label11.Text = "Process Loss";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(834, 99);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(31, 15);
            this.label10.TabIndex = 22;
            this.label10.Text = "Rate";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(763, 99);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(26, 15);
            this.label9.TabIndex = 21;
            this.label9.Text = "Qty";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(570, 99);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(44, 15);
            this.label8.TabIndex = 20;
            this.label8.Text = "Colour";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(369, 99);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(50, 15);
            this.label7.TabIndex = 19;
            this.label7.Text = "Process";
            // 
            // BtnOk
            // 
            this.BtnOk.Location = new System.Drawing.Point(967, 116);
            this.BtnOk.Name = "BtnOk";
            this.BtnOk.Size = new System.Drawing.Size(36, 25);
            this.BtnOk.TabIndex = 12;
            this.BtnOk.Text = "Ok";
            this.BtnOk.UseVisualStyleBackColor = true;
            this.BtnOk.Click += new System.EventHandler(this.BtnOk_Click);
            // 
            // txtProcessLoss
            // 
            this.txtProcessLoss.Location = new System.Drawing.Point(903, 117);
            this.txtProcessLoss.Name = "txtProcessLoss";
            this.txtProcessLoss.Size = new System.Drawing.Size(63, 23);
            this.txtProcessLoss.TabIndex = 11;
            // 
            // txtRate
            // 
            this.txtRate.Location = new System.Drawing.Point(834, 117);
            this.txtRate.Name = "txtRate";
            this.txtRate.Size = new System.Drawing.Size(68, 23);
            this.txtRate.TabIndex = 10;
            // 
            // txtQty
            // 
            this.txtQty.Location = new System.Drawing.Point(763, 117);
            this.txtQty.Name = "txtQty";
            this.txtQty.Size = new System.Drawing.Size(70, 23);
            this.txtQty.TabIndex = 9;
            // 
            // txtColour
            // 
            this.txtColour.Location = new System.Drawing.Point(570, 117);
            this.txtColour.Name = "txtColour";
            this.txtColour.Size = new System.Drawing.Size(98, 23);
            this.txtColour.TabIndex = 7;
            // 
            // txtProcess
            // 
            this.txtProcess.Location = new System.Drawing.Point(369, 117);
            this.txtProcess.Name = "txtProcess";
            this.txtProcess.Size = new System.Drawing.Size(200, 23);
            this.txtProcess.TabIndex = 6;
            this.txtProcess.Click += new System.EventHandler(this.TxtProcess_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 99);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(65, 15);
            this.label6.TabIndex = 12;
            this.label6.Text = "Item Name";
            // 
            // CmbItem
            // 
            this.CmbItem.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CmbItem.FormattingEnabled = true;
            this.CmbItem.Location = new System.Drawing.Point(6, 117);
            this.CmbItem.Name = "CmbItem";
            this.CmbItem.Size = new System.Drawing.Size(362, 23);
            this.CmbItem.TabIndex = 5;
            this.CmbItem.SelectedIndexChanged += new System.EventHandler(this.CmbItem_SelectedIndexChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(30, 58);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(33, 15);
            this.label5.TabIndex = 10;
            this.label5.Text = "Style";
            // 
            // CmbStyle
            // 
            this.CmbStyle.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CmbStyle.FormattingEnabled = true;
            this.CmbStyle.Location = new System.Drawing.Point(69, 54);
            this.CmbStyle.Name = "CmbStyle";
            this.CmbStyle.Size = new System.Drawing.Size(341, 23);
            this.CmbStyle.TabIndex = 4;
            this.CmbStyle.SelectedIndexChanged += new System.EventHandler(this.CmbStyle_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(631, 19);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(56, 15);
            this.label4.TabIndex = 8;
            this.label4.Text = "Doc Date";
            // 
            // DtpDocDate
            // 
            this.DtpDocDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.DtpDocDate.Location = new System.Drawing.Point(692, 15);
            this.DtpDocDate.Name = "DtpDocDate";
            this.DtpDocDate.Size = new System.Drawing.Size(117, 23);
            this.DtpDocDate.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(221, 19);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(67, 15);
            this.label3.TabIndex = 6;
            this.label3.Text = "Order Date";
            // 
            // DtpOrderDate
            // 
            this.DtpOrderDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.DtpOrderDate.Location = new System.Drawing.Point(293, 15);
            this.DtpOrderDate.Name = "DtpOrderDate";
            this.DtpOrderDate.Size = new System.Drawing.Size(117, 23);
            this.DtpOrderDate.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(417, 19);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(43, 15);
            this.label2.TabIndex = 4;
            this.label2.Text = "DocNo";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 15);
            this.label1.TabIndex = 3;
            this.label1.Text = "Order No";
            // 
            // txtDocNo
            // 
            this.txtDocNo.Enabled = false;
            this.txtDocNo.Location = new System.Drawing.Point(470, 15);
            this.txtDocNo.Name = "txtDocNo";
            this.txtDocNo.Size = new System.Drawing.Size(146, 23);
            this.txtDocNo.TabIndex = 2;
            // 
            // txtOrderNo
            // 
            this.txtOrderNo.Location = new System.Drawing.Point(69, 15);
            this.txtOrderNo.Name = "txtOrderNo";
            this.txtOrderNo.Size = new System.Drawing.Size(146, 23);
            this.txtOrderNo.TabIndex = 0;
            this.txtOrderNo.MouseClick += new System.Windows.Forms.MouseEventHandler(this.TxtOrderNo_MouseClick);
            this.txtOrderNo.TextChanged += new System.EventHandler(this.txtOrderNo_TextChanged);
            // 
            // txtmaterial
            // 
            this.txtmaterial.Location = new System.Drawing.Point(15, 156);
            this.txtmaterial.Name = "txtmaterial";
            this.txtmaterial.Size = new System.Drawing.Size(200, 23);
            this.txtmaterial.TabIndex = 38;
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(801, 377);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(165, 35);
            this.button1.TabIndex = 39;
            this.button1.Text = "Conversion";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // GrFront
            // 
            this.GrFront.Controls.Add(this.SplitAdd);
            this.GrFront.Controls.Add(this.SfdDataGridTrims);
            this.GrFront.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GrFront.Location = new System.Drawing.Point(8, 2);
            this.GrFront.Name = "GrFront";
            this.GrFront.Size = new System.Drawing.Size(1012, 589);
            this.GrFront.TabIndex = 36;
            this.GrFront.TabStop = false;
            // 
            // SfdDataGridTrims
            // 
            this.SfdDataGridTrims.AccessibleName = "Table";
            this.SfdDataGridTrims.AllowDraggingColumns = true;
            this.SfdDataGridTrims.AllowFiltering = true;
            this.SfdDataGridTrims.Location = new System.Drawing.Point(6, 15);
            this.SfdDataGridTrims.Name = "SfdDataGridTrims";
            this.SfdDataGridTrims.Size = new System.Drawing.Size(997, 525);
            this.SfdDataGridTrims.TabIndex = 0;
            this.SfdDataGridTrims.Text = "sfDataGrid1";
            // 
            // FrmTrimsProcess
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1026, 595);
            this.Controls.Add(this.GrFront);
            this.Controls.Add(this.GrBack);
            this.Name = "FrmTrimsProcess";
            this.Text = "FrmTrimsProcess";
            this.Load += new System.EventHandler(this.FrmTrimsProcess_Load);
            this.GrBack.ResumeLayout(false);
            this.GrBack.PerformLayout();
            this.conpnl.ResumeLayout(false);
            this.conpnl.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trimsload)).EndInit();
            this.grSearch.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DataGridCommon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridTrimsAdd)).EndInit();
            this.GrFront.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.SfdDataGridTrims)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox GrBack;
        private Syncfusion.Windows.Forms.Tools.SplitButton SplitSave;
        private Syncfusion.Windows.Forms.Tools.toolstripitem toolStripBack;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtDocNo;
        private System.Windows.Forms.TextBox txtOrderNo;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DateTimePicker DtpDocDate;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DateTimePicker DtpOrderDate;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox CmbStyle;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox CmbItem;
        private System.Windows.Forms.TextBox txtProcessLoss;
        private System.Windows.Forms.TextBox txtRate;
        private System.Windows.Forms.TextBox txtQty;
        private System.Windows.Forms.TextBox txtColour;
        private System.Windows.Forms.TextBox txtProcess;
        private System.Windows.Forms.Button BtnOk;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.DataGridView DataGridTrimsAdd;
        private System.Windows.Forms.GroupBox grSearch;
        private System.Windows.Forms.Button btnSelect;
        private System.Windows.Forms.DataGridView DataGridCommon;
        private System.Windows.Forms.Button btnHide;
        private System.Windows.Forms.GroupBox GrFront;
        private Syncfusion.Windows.Forms.Tools.SplitButton SplitAdd;
        private Syncfusion.Windows.Forms.Tools.toolstripitem toolstripEdit;
        private Syncfusion.Windows.Forms.Tools.toolstripitem toolstripClose;
        private Syncfusion.WinForms.DataGrid.SfDataGrid SfdDataGridTrims;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox CmbUom;
        private System.Windows.Forms.TextBox txtmaterial;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Panel conpnl;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ComboBox uom;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtconvertedtqty;
        private System.Windows.Forms.TextBox txtconqty;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.ComboBox trimsitem;
        private System.Windows.Forms.TextBox txtconitems;
        private System.Windows.Forms.DataGridView trimsload;
        private System.Windows.Forms.Button button3;
    }
}