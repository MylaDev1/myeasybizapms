﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Drawing.Imaging;
using System.Configuration;
using System.Data.SqlClient;
using System.Diagnostics;
using excel = Microsoft.Office.Interop.Excel;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
namespace MyEasyBizAPMS
{
    public partial class FrmProductionIssue : Form
    {
        private CrystalDecisions.Windows.Forms.CrystalReportViewer Cryview;

        ReportDocument doc = new ReportDocument();
        public FrmProductionIssue()
        {
            this.BackColor = Color.White; 
            InitializeComponent();
        }

        int mode = 0;
        string tpuid = "";
        string uid = "";
        int type = 0;
        DateTime str9;
        double sum1;
        double sum2;
        int SP;
        int Dtype = 0;
        int h = 0;
        int i = 0;
        int kk = 0;
        SqlConnection conn = new SqlConnection(GeneralParameters.ConnectionString);
        SqlCommand qur = new SqlCommand();
        private DataRow doc1;
        DataTable Docno = new DataTable();
        BindingSource bs = new BindingSource();
             DataTable Docno1 = new DataTable();
        BindingSource bsc = new BindingSource();
        BindingSource bsp = new BindingSource();
        BindingSource bsserial = new BindingSource();
        BindingSource IN = new BindingSource();
        BindingSource OUT = new BindingSource();
        BindingSource bsFabric = new BindingSource();

        SQLDBHelper db = new SQLDBHelper();
        int Fillid;
        BindingSource bsParty = new BindingSource();
        string str1key;
        public int SelectId = 0;
        private void FrmProductionIssue_Load(object sender, EventArgs e)
        {
            qur.Connection = conn;
      
            Genclass.buttonstyleform(this);
            Genclass.buttonstylepanel(panadd);
            panadd.Visible = true;
            Genpan.Visible = true;
            Editpnl.Visible = false;

            this.HFGP.DefaultCellStyle.Font = new Font("calibri", 10);
            this.HFGP.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 10, FontStyle.Bold);
            this.HFIT.DefaultCellStyle.Font = new Font("calibri", 10);
            this.HFIT.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 10, FontStyle.Bold);

            this.HFGP1.DefaultCellStyle.Font = new Font("calibri", 10);
            this.HFGP1.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 10, FontStyle.Bold);

            this.HFGP2.DefaultCellStyle.Font = new Font("calibri", 10);
            this.HFGP2.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 10, FontStyle.Bold);

            HFIT.RowHeadersVisible = false;
            HFGP.RowHeadersVisible = false;
            HFGP2.RowHeadersVisible = false;
            HFGP1.RowHeadersVisible = false;




            HFGP2.EnableHeadersVisualStyles = false;
            HFGP2.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.GradientActiveCaption;

            HFIT.EnableHeadersVisualStyles = false;
            HFIT.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.GradientActiveCaption;
            HFGP1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            HFGP1.EnableHeadersVisualStyles = false;
            HFGP1.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.GradientActiveCaption;

            HFGP.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            HFGP.EnableHeadersVisualStyles = false;
            HFGP.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.GradientActiveCaption;


           
                Genclass.Dtype = 1350;


            dtpfnt.Format = DateTimePickerFormat.Custom;
            dtpfnt.CustomFormat = "dd/MM/yyyy";
            dtpgrndt.Format = DateTimePickerFormat.Custom;
            dtpgrndt.CustomFormat = "dd/MM/yyyy";
            dtpsupp.Format = DateTimePickerFormat.Custom;
            dtpsupp.CustomFormat = "dd/MM/yyyy";
            chkact.Checked = true;


            LoadGetJobCard(1);
            Titlep();
            LoadDataGridComboWise();
            LoadDataGridSizeCombo();
            LoadDataGridSizecomponent();
        }

        protected void LoadDataGridComboWise()
        {
            try
            {
                DataGridSizeCombo.AutoGenerateColumns = false;
                DataGridSizeCombo.ColumnCount = 2;
                DataGridViewCheckBoxColumn checkBoxColumn = new DataGridViewCheckBoxColumn
                {
                    Name = "Chck",
                    HeaderText = "Chck",
                    Width = 50
                };
                DataGridSizeCombo.Columns.Insert(0, checkBoxColumn);

                DataGridSizeCombo.Columns[1].Name = "COMBONAME";
                DataGridSizeCombo.Columns[1].HeaderText = "COMBONAME";
                DataGridSizeCombo.Columns[1].Width = 150;

                DataGridSizeCombo.Columns[2].Name = "uid";
                DataGridSizeCombo.Columns[2].HeaderText = "uid";
                DataGridSizeCombo.Columns[2].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected void LoadDataGridSizeCombo()
        {
            try
            {
                DataGridSizeWise.AutoGenerateColumns = false;
                DataGridSizeWise.ColumnCount = 2;
                DataGridViewCheckBoxColumn checkBoxColumn = new DataGridViewCheckBoxColumn
                {
                    Name = "Chck",
                    HeaderText = "Chck",
                    Width = 50
                };
                DataGridSizeWise.Columns.Insert(0, checkBoxColumn);

                DataGridSizeWise.Columns[1].Name = "size";
                DataGridSizeWise.Columns[1].HeaderText = "size";
                DataGridSizeWise.Columns[1].Width = 150;

                DataGridSizeWise.Columns[2].Name = "uid";
                DataGridSizeWise.Columns[2].HeaderText = "uid";
                DataGridSizeWise.Columns[2].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
        protected void LoadDataGridSizecomponent()
        {
            try
            {
                componentgrid.AutoGenerateColumns = false;
                componentgrid.ColumnCount = 2;
                DataGridViewCheckBoxColumn checkBoxColumn = new DataGridViewCheckBoxColumn
                {
                    Name = "Chck",
                    HeaderText = "Chck",
                    Width = 50
                };
                componentgrid.Columns.Insert(0, checkBoxColumn);

                componentgrid.Columns[1].Name = "COMPONENT";
                componentgrid.Columns[1].HeaderText = "COMPONENT";
                componentgrid.Columns[1].Width = 150;

                componentgrid.Columns[2].Name = "uid";
                componentgrid.Columns[2].HeaderText = "uid";
                componentgrid.Columns[2].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
        private void FillcomboGrid()
        {
            try
            {


                string qur1 = "sp_getproductionissuesocno '" + txtqty.Text + "'";
                SqlCommand cmd1 = new SqlCommand(qur1, conn);
                SqlDataAdapter apt1 = new SqlDataAdapter(cmd1);
                DataTable tab1 = new DataTable();
                apt1.Fill(tab1);
                if (tab1.Rows.Count > 0)
                {
                    DataGridSizeCombo.Rows.Clear();
                    for (int i = 0; i < tab1.Rows.Count; i++)
                    {
                        int Index = DataGridSizeCombo.Rows.Add();
                        DataGridViewRow dataGrid = (DataGridViewRow)DataGridSizeCombo.Rows[Index];
                        dataGrid.Cells[1].Value = tab1.Rows[i]["ComboName"].ToString();
                        dataGrid.Cells[2].Value = tab1.Rows[i]["Uid"].ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        private void FillcomponentGrid()
        {
            try
            {


                string qur1 = "sp_getproductionissuecomboponent '" + txtqty.Text + "'";
                SqlCommand cmd1 = new SqlCommand(qur1, conn);
                SqlDataAdapter apt1 = new SqlDataAdapter(cmd1);
                DataTable tab1 = new DataTable();
                apt1.Fill(tab1);
                if (tab1.Rows.Count > 0)
                {
                    componentgrid.Rows.Clear();
                    for (int i = 0; i < tab1.Rows.Count; i++)
                    {
                        int Index = componentgrid.Rows.Add();
                        DataGridViewRow dataGrid = (DataGridViewRow)componentgrid.Rows[Index];
                        dataGrid.Cells[1].Value = tab1.Rows[i]["COMPONENT"].ToString();
                        dataGrid.Cells[2].Value = tab1.Rows[i]["uid"].ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void FillsizeGrid()
        {
            try
            {


                string qur1 = "sp_getproductionissuesize '" + txtqty.Text + "'";
                SqlCommand cmd1 = new SqlCommand(qur1, conn);
                SqlDataAdapter apt1 = new SqlDataAdapter(cmd1);
                DataTable tab1 = new DataTable();
                apt1.Fill(tab1);
                if (tab1.Rows.Count > 0)
                {
                    DataGridSizeWise.Rows.Clear();
                    for (int i = 0; i < tab1.Rows.Count; i++)
                    {
                        int Index = DataGridSizeWise.Rows.Add();
                        DataGridViewRow dataGrid = (DataGridViewRow)DataGridSizeWise.Rows[Index];
                        dataGrid.Cells[1].Value = tab1.Rows[i]["size"].ToString();
                        dataGrid.Cells[2].Value = tab1.Rows[i]["uid"].ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        protected DataTable LoadGetJobCard(int tag)
        {

    
            chkact.Checked = true;
            if (chkact.Checked == true)
            {
                SP = 1;

            }
            else
            {
                SP = 0;

            }


            DataTable dt = new DataTable();
            try
            {

                SqlParameter[] para = {
                    //new SqlParameter("@active",SP),
                         new SqlParameter("@doctypeid",Genclass.Dtype),


                };

                dt = db.GetData(CommandType.StoredProcedure, "SP_GETCUTTINGRECEIPTDET", para);


                LoadDataTable(dt);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        protected void LoadDataTable(DataTable dt)
        {
            try
            {
                HFGP.DataSource = null;
                HFGP.AutoGenerateColumns = false;
                HFGP.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
                HFGP.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
                HFGP.ColumnCount = 6;
                HFGP.Columns[0].Name = "Uid";
                HFGP.Columns[0].HeaderText = "Uid";
                HFGP.Columns[0].DataPropertyName = "Uid";

                HFGP.Columns[1].Name = "Docno";
                HFGP.Columns[1].HeaderText = "Docno";
                HFGP.Columns[1].DataPropertyName = "Docno";

                HFGP.Columns[2].Name = "Docdate";
                HFGP.Columns[2].HeaderText = "Docdate";
                HFGP.Columns[2].DataPropertyName = "Docdate";

                HFGP.Columns[3].Name = "ProOrderNo";
                HFGP.Columns[3].HeaderText = "ProOrderNo";
                HFGP.Columns[3].DataPropertyName = "ProOrderNo";

                HFGP.Columns[4].Name = "SocNo";
                HFGP.Columns[4].HeaderText = "SocNo";
                HFGP.Columns[4].DataPropertyName = "SocNo";

                HFGP.Columns[5].Name = "CuttingType";
                HFGP.Columns[5].HeaderText = "CuttingType";
                HFGP.Columns[5].DataPropertyName = "CuttingType";

               




                bs.DataSource = dt;

                HFGP.DataSource = bs;


                HFGP.Columns[0].Visible = false;
           
          
                HFGP.Columns[1].Width = 90;
                HFGP.Columns[2].Width = 90;
                HFGP.Columns[3].Width = 450;
    
                HFGP.Columns[4].Width = 150;
                HFGP.Columns[5].Width = 150;
            
              

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        private void button3_Click(object sender, EventArgs e)
        {
            mode = 1;
            Genpan.Visible = false;

            panadd.Visible = false;
            h = 0;
            Genclass.ClearTextBox(this, Editpnl);
            Genclass.STR = "";
            Genclass.Gendocno();
            txtgrn.Text = Genclass.ST;
            Editpnl.Visible = true;
            lkppnl.Visible = false;
            sum1 = 0;
            sum2 = 0;
            txtgrn.Tag = 0;
            HFIT.Refresh();
            HFIT.DataSource = null;
            HFIT.Rows.Clear();

            HFGP1.Refresh();
            HFGP1.DataSource = null;
            HFGP1.Rows.Clear();

            conn.Close();
            conn.Open();
            Docno.Clear();

            button13.Visible = false;
            if (Docno.Columns.Count == 0)
            {
                Docno.Columns.Add("uid");
                Docno.Columns.Add("Docno");
            }
            conn.Close();
            conn.Open();
            Chkedtact.Checked = true;




            HFIT.Refresh();
            HFIT.DataSource = null;
            HFIT.Rows.Clear();
            HFGP1.Refresh();
            HFGP1.DataSource = null;
            HFGP1.Rows.Clear();

            Titlep();


            dtpgrndt.Focus();
            LoadDataGridComboWise();
            LoadDataGridSizeCombo();
            LoadDataGridSizecomponent();
        }
        private void Titlep()
        {
            HFIT.AutoGenerateColumns = false;
            HFIT.Refresh();
            HFIT.DataSource = null;
            HFIT.Rows.Clear();
            HFIT.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            HFIT.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            HFIT.ColumnCount = 11;
            HFIT.Columns[0].Name = "uid";
            HFIT.Columns[1].Name = "ComboName";
            HFIT.Columns[2].Name = "Itemname";
      
            HFIT.Columns[3].Name = "Size";
            HFIT.Columns[4].Name = "Uom";
            HFIT.Columns[5].Name = "Qty";
            HFIT.Columns[6].Name = "Bundles";
            HFIT.Columns[7].Name = "fabricuid";
            HFIT.Columns[8].Name = "refuid";
            HFIT.Columns[9].Name = "Cutting";
            HFIT.Columns[10].Name = "Weight";
            //HFIT.Columns[13].Name = "Socno";
            //HFIT.Columns[14].Name = "Workorder";
            //HFIT.Columns[15].Name = "Style";

            //HFIT.EditMode = DataGridViewEditMode.EditOnKeystroke;

            HFIT.Columns[0].Visible = false;

            HFIT.Columns[1].Width = 250;
            HFIT.Columns[2].Width = 250;
            //HFIT.Columns[3].Width = 80;
            //HFIT.Columns[4].Width = 80;
            HFIT.Columns[3].Width = 100;
            HFIT.Columns[4].Width = 100;
            HFIT.Columns[5].Width = 100;
            HFIT.Columns[6].Width = 100;
            HFIT.Columns[7].Visible = false;
            HFIT.Columns[8].Visible = false;
            HFIT.Columns[9].Visible = false;
            HFIT.Columns[10].Width = 100;
            //HFIT.Columns[13].Visible = false;
            //HFIT.Columns[14].Visible = false;
            //HFIT.Columns[3].Visible = false;
            //HFIT.Columns[4].Visible = false;
            //HFIT.Columns[15].Width = 100;
        }
        private void txtname_Click(object sender, EventArgs e)
        {
            type = 1;
            DataTable dt = getParty();
            bsp.DataSource = dt;
            FillGrid2(dt, 1);
            Point loc = FindLocation(txtname);
            grSearch.Location = new Point(loc.X, loc.Y + 20);
            grSearch.Visible = true;
            grSearch.Text = "Name Search";
        }
        protected void FillGrid2(DataTable dt, int FillId)
        {
            try
            {
                DataGridCommon.DataSource = null;
                DataGridCommon.AutoGenerateColumns = false;


                DataGridCommon.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
                DataGridCommon.DefaultCellStyle.WrapMode = DataGridViewTriState.True;


                DataGridCommon.ColumnCount = 3;
                DataGridCommon.Columns[0].Name = "uid";
                DataGridCommon.Columns[0].HeaderText = "uid";
                DataGridCommon.Columns[0].DataPropertyName = "uid";
                DataGridCommon.Columns[1].Name = "Socno";
                DataGridCommon.Columns[1].HeaderText = "Socno";
                DataGridCommon.Columns[1].DataPropertyName = "Socno";
                DataGridCommon.Columns[1].Width = 100;
                DataGridCommon.Columns[2].Name = "Docno";
                DataGridCommon.Columns[2].HeaderText = "Docno";
                DataGridCommon.Columns[2].DataPropertyName = "Docno";
                DataGridCommon.Columns[2].Width = 100;
  

                DataGridCommon.DataSource = bsFabric;
                DataGridCommon.Columns[0].Visible = false;
             


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        private void txtname_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.F10)
                {
                    grSearch.Visible = false;
                }
                else if (e.KeyCode == Keys.F2)
                {
                    SelectId = 1;
                    int Index = DataGridCommon.SelectedCells[0].RowIndex;

                    txtname.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtpuid.Text = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    //txtmillid.Text = DataGridCommon.Rows[Index].Cells[4].Value.ToString();
                    txtmode.Focus();
                    grSearch.Visible = false;
                    SelectId = 0;
                }
                else if (e.KeyCode == Keys.Escape)
                {
                    grSearch.Visible = false;
                }
                else if (e.KeyValue == 40)
                {
                    DataGridCommon.Select();
                }
                else if (e.KeyCode == Keys.Enter)
                {
                    txtname_Click(sender, e);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtname_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bsp.Filter = string.Format("Name LIKE '%{0}%' ", txtname.Text);



                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtdcqty_Click(object sender, EventArgs e)
        {
           
            
            type = 3;
            Point loc = FindLocation(txtdcqty);
            grSearch.Location = new Point(loc.X, loc.Y + 20);
            grSearch.Visible = true;
            comboname();


        }
        private void comboname()

        {

            string qur1 = "select DISTINCT A.COMBONAME from WIP_STOCK_LEDGER A LEFT JOIN CUTTINGRECEIPTD B ON A.DETTABLE_UID=B.UID where a.doctypeid=1320 and a.socno='"+ txtqty.Text +"' GROUP BY   DETTABLE_UID ,A.COMBONAME,A.qty  HAVING A.qty-isnull(sum(b.QTY),0)>0  ";
            SqlCommand cmd1 = new SqlCommand(qur1, conn);
            SqlDataAdapter apt1 = new SqlDataAdapter(cmd1);
            DataTable tab1 = new DataTable();
            apt1.Fill(tab1);
            bsp.DataSource = tab1;
            //Point loc = FindLocation(txtdcqty);
            //grSearch.Location = new Point(loc.X, loc.Y + 20);
            grSearch.Visible = true;
            DataGridCommon.AutoGenerateColumns = false;
            DataGridCommon.Refresh();
            DataGridCommon.DataSource = null;
            DataGridCommon.Rows.Clear();
            DataGridCommon.ColumnCount = tab1.Columns.Count;
            i = 0;
            foreach (DataColumn column in tab1.Columns)
            {
                DataGridCommon.Columns[i].Name = column.ColumnName;
                DataGridCommon.Columns[i].HeaderText = column.ColumnName;
                DataGridCommon.Columns[i].DataPropertyName = column.ColumnName;
                i = i + 1;
            }

            DataGridCommon.DataSource = tab1;

           

            DataGridCommon.Columns[0].Width = 300;



        }
        private void componentname()

        {

            string qur1 = "select distinct 0 uid,a.COMPONENT  as ItemName from WIP_STOCK_LEDGER A  inner join cuttingEntryM c on a.HEADTABLE_UID=c.uid inner join cuttingComboComponentList d on a.DETTABLE_UID = d.uid LEFT JOIN CUTTINGRECEIPTD B ON A.DETTABLE_UID=B.UID   where a.SOCNO='" + txtqty.Text + "'  and doctypeid=1320   ";
            SqlCommand cmd1 = new SqlCommand(qur1, conn);
            SqlDataAdapter apt1 = new SqlDataAdapter(cmd1);
            DataTable tab1 = new DataTable();
            apt1.Fill(tab1);
            bsParty.DataSource = tab1;
            //Point loc = FindLocation(txtdcqty);
            //grSearch.Location = new Point(loc.X, loc.Y + 20);
            grSearch.Visible = true;
            DataGridCommon.AutoGenerateColumns = false;
            DataGridCommon.Refresh();
            DataGridCommon.DataSource = null;
            DataGridCommon.Rows.Clear();
            DataGridCommon.ColumnCount = tab1.Columns.Count;
            i = 0;
            foreach (DataColumn column in tab1.Columns)
            {
                DataGridCommon.Columns[i].Name = column.ColumnName;
                DataGridCommon.Columns[i].HeaderText = column.ColumnName;
                DataGridCommon.Columns[i].DataPropertyName = column.ColumnName;
                i = i + 1;
            }

            DataGridCommon.DataSource = tab1;

            DataGridCommon.Columns[1].Width = 300;

            DataGridCommon.Columns[0].Visible = false;
           
        }
        protected void FillGrid(DataTable dt, int FillId)
        {
            try
            {
                DataGridCommon.DataSource = null;
                DataGridCommon.AutoGenerateColumns = false;


                DataGridCommon.ColumnCount = 4;
                DataGridCommon.Columns[0].Name = "uid";
                DataGridCommon.Columns[0].HeaderText = "uid";
                DataGridCommon.Columns[0].DataPropertyName = "uid";
                DataGridCommon.Columns[1].Name = "Itemname";
                DataGridCommon.Columns[1].HeaderText = "Itemname";
                DataGridCommon.Columns[1].DataPropertyName = "Itemname";
                DataGridCommon.Columns[1].Width = 300;
                DataGridCommon.Columns[2].Name = "UOMID";
                DataGridCommon.Columns[2].HeaderText = "UOMID";
                DataGridCommon.Columns[2].DataPropertyName = "UOMID";

                DataGridCommon.Columns[3].Name = "GENERALNAME";
                DataGridCommon.Columns[3].HeaderText = "GENERALNAME";
                DataGridCommon.Columns[3].DataPropertyName = "GENERALNAME";



                DataGridCommon.DataSource = bsParty;
                DataGridCommon.Columns[0].Visible = false;
                DataGridCommon.Columns[2].Visible = false;
                DataGridCommon.Columns[3].Visible = false;



            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private Point FindLocation(Control ctrl)
        {
            if (ctrl.Parent is Form)
                return ctrl.Location;
            else
            {
                Point p = FindLocation(ctrl.Parent);
                p.X += ctrl.Location.X;
                p.Y += ctrl.Location.Y;
                return p;
            }
        }
        protected DataTable getParty()
        {
            DataTable dt = new DataTable();
            try
            {
                if (type == 1)
                {
                    dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "sp_getprooderno", conn);
                    bsFabric.DataSource = dt;
                }
                else if (type == 1 && Text == "Fabric Without Process")
                {
                    dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "SP_FABWOPROCESSSUPP", conn);
                    bsp.DataSource = dt;
                }
                else if (type == 2 && Text == "Knitting Without Process")
                {
                    //dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "sp_getwo", conn);
                    //bsc1.DataSource = dt;
                }
                else if (type == 2 && Text == "Fabric Without Process")
                {
                    //dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "sp_getwo", conn);
                    //bsc1.DataSource = dt;
                }



            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            return dt;
        }
        private void txtdcqty_KeyDown(object sender, KeyEventArgs e)
        {

            try
            {
                if (e.KeyCode == Keys.F10)
                {
                    grSearch.Visible = false;
                }
                else if (e.KeyCode == Keys.F2)
                {
                    SelectId = 1;
                    int Index = DataGridCommon.SelectedCells[0].RowIndex;
                    txtdcqty.Text = HFGP2.Rows[Index].Cells[0].Value.ToString();
                 

                    txtitem.Focus();
                    grSearch.Visible = false;
                    SelectId = 0;
                }
                else if (e.KeyCode == Keys.Escape)
                {
                    grSearch.Visible = false;
                }
                else if (e.KeyValue == 40)
                {
                    DataGridCommon.Select();
                }
                else if (e.KeyCode == Keys.Enter)
                {
                    txtdcqty_Click(sender, e);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtdcqty_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bsp.Filter = string.Format("comboname LIKE '%{0}%' ", txtdcqty.Text);



                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtitem_Click(object sender, EventArgs e)        {
            type = 2;
            DataTable dt = getParty();
            //bsc1.DataSource = dt;
            FillGrid3(dt, 1);
            Point loc = FindLocation(txtitem);
            grSearch.Location = new Point(loc.X, loc.Y + 20);
            grSearch.Visible = true;
            grSearch.Text = "Name Search";
        }

        private void txtitem_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.F10)
                {
                    grSearch.Visible = false;
                }
                else if (e.KeyCode == Keys.F2)
                {
                    SelectId = 1;
                    int Index = DataGridCommon.SelectedCells[0].RowIndex;

                    txtitem.Text = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    txtitem.Tag = DataGridCommon.Rows[Index].Cells[1].Value.ToString();

                    txtmode.Focus();
                    lkppnl.Visible = false;
                    SelectId = 0;
                }
                else if (e.KeyCode == Keys.Escape)
                {
                    grSearch.Visible = false;
                }
                else if (e.KeyValue == 40)
                {
                    DataGridCommon.Select();
                }
                else if (e.KeyCode == Keys.Enter)
                {
                    txtitem_Click(sender, e);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
        protected void FillGrid3(DataTable dt, int FillId)
        {
            try
            {
                DataGridCommon.DataSource = null;
                DataGridCommon.AutoGenerateColumns = false;


                DataGridCommon.ColumnCount = 4;
                DataGridCommon.Columns[0].Name = "Uid";
                DataGridCommon.Columns[0].HeaderText = "Uid";
                DataGridCommon.Columns[0].DataPropertyName = "Uid";

                DataGridCommon.Columns[1].Name = "docno";
                DataGridCommon.Columns[1].HeaderText = "Docno";
                DataGridCommon.Columns[1].DataPropertyName = "docno";
                DataGridCommon.Columns[1].Width = 150;


                DataGridCommon.Columns[2].Name = "processname";
                DataGridCommon.Columns[2].HeaderText = "processname";
                DataGridCommon.Columns[2].DataPropertyName = "processname";




                DataGridCommon.Columns[3].Name = "processid";
                DataGridCommon.Columns[3].HeaderText = "processid";
                DataGridCommon.Columns[3].DataPropertyName = "processid";






                //DataGridCommon.DataSource = bsc1;
                DataGridCommon.Columns[0].Visible = false;
                DataGridCommon.Columns[2].Visible = false;
                DataGridCommon.Columns[3].Visible = false;



            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        private void txtitem_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bsp.Filter = string.Format("docno LIKE '%{0}%' ", txtvehicle.Text);



                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void button18_Click(object sender, EventArgs e)
        {
            try
            {
                SelectId = 1;
                int Index = DataGridCommon.SelectedCells[0].RowIndex;
                if (type == 1)
                {
                    txtqty.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtqty.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    txtsocno.Text = DataGridCommon.Rows[Index].Cells[2].Value.ToString();
                    txtitem.Focus();
                    FillcomboGrid();
                    FillcomponentGrid();
                    FillsizeGrid();
                }
                else if (type == 3)
                {
                    txtdcqty.Text = DataGridCommon.Rows[Index].Cells[0].Value.ToString();

                    textBox5.Focus();
                }
                else if (type == 4)
                {
                    textBox5.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    textBox5.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();



                }
                else if (type == 5)
                {
                    textBox5.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    txtaddnotes.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtprice.Text = DataGridCommon.Rows[Index].Cells[2].Value.ToString();
                    txtuom.Text = DataGridCommon.Rows[Index].Cells[3].Value.ToString();
                    txtwok.Text = DataGridCommon.Rows[Index].Cells[3].Value.ToString();
                    txtweight.Text = DataGridCommon.Rows[Index].Cells[4].Value.ToString();
                }

                    grSearch.Visible = false;
                SelectId = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
        private void loadallitems()

        {

            if (txtname.Text == string.Empty)
            {
                MessageBox.Show("Select Supplier", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtname.Focus();
                return;
            }
            lkppnl.Visible = true;
            txtscr11.Text = "";
            txtscr11.Focus();


            if (Text == "Knitting Without Process")
            {
                Genclass.strsql = "SP_GETKNITRECWOITEMS  " + txtpuid.Text + ",'" + txtscono.Text + "' ,'" + txtwok.Text + "','" + txtwo.Text + "' ";
                Genclass.FSSQLSortStr = "Itemdes";
            }
            else

            {

                Genclass.strsql = "SP_GETFABRECWOITEMS  " + txtpuid.Text + ",'" + txtscono.Text + "' ,'" + txtwok.Text + "','" + txtwo.Text + "' ";
                Genclass.FSSQLSortStr = "Itemdes";
            }


            Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
            SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
            DataTable tap = new DataTable();
            aptr.Fill(tap);

            bsc.DataSource = tap;

            HFGP2.AutoGenerateColumns = false;
            HFGP2.Refresh();
            HFGP2.DataSource = null;
            HFGP2.Rows.Clear();
            HFGP2.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            HFGP2.DefaultCellStyle.WrapMode = DataGridViewTriState.True;


            HFGP2.ColumnCount = tap.Columns.Count;
            i = 0;

            foreach (DataColumn column in tap.Columns)
            {
                HFGP2.Columns[i].Name = column.ColumnName;
                HFGP2.Columns[i].HeaderText = column.ColumnName;
                HFGP2.Columns[i].DataPropertyName = column.ColumnName;
                i = i + 1;
            }
            HFGP2.DataSource = tap;

            HFGP2.Columns[0].Visible = false;
            HFGP2.Columns[2].Width = 100;

            HFGP2.Columns[3].Width = 100;
            HFGP2.Columns[4].Visible = false;
            HFGP2.Columns[5].Visible = false;
            HFGP2.Columns[6].Visible = false;
            HFGP2.Columns[1].Width = 250;
            HFGP2.Columns[7].Visible = false;
            HFGP2.Columns[8].Width = 100;
            HFGP2.Columns[9].Width = 200;
            HFGP2.Columns[10].Visible = false;
            conn.Close();
        }
        private void btnHide_Click(object sender, EventArgs e)
        {
            grSearch.Visible = false;
        }

        private void DataGridCommon_DoubleClick(object sender, EventArgs e)
        {
            SelectId = 1;
            int Index = DataGridCommon.SelectedCells[0].RowIndex;
            if (type == 1)
            {
                txtqty.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                txtqty.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                txtsocno.Text = DataGridCommon.Rows[Index].Cells[2].Value.ToString();
                txtitem.Focus();
                FillcomboGrid();
                FillcomponentGrid();
                FillsizeGrid();
            }
            else if (type == 3)
            {
                txtdcqty.Text = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                textBox5.Focus();
            }
            else if (type == 4)
            {
                textBox5.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                textBox5.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();



            }
                else if (type == 5)
            {
                textBox5.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                txtaddnotes.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                txtprice.Text = DataGridCommon.Rows[Index].Cells[2].Value.ToString();
                txtuom.Text = DataGridCommon.Rows[Index].Cells[3].Value.ToString();
                txtweight.Text = DataGridCommon.Rows[Index].Cells[4].Value.ToString();
                txtwok.Text = DataGridCommon.Rows[Index].Cells[3].Value.ToString();
            }

            grSearch.Visible = false;
            SelectId = 0;
        }

        private void DataGridCommon_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                int Index = DataGridCommon.SelectedCells[0].RowIndex;
                if (type == 1)
                {
                    txtqty.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtqty.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    txtsocno.Text = DataGridCommon.Rows[Index].Cells[2].Value.ToString();
                    txtitem.Focus();
                    FillcomboGrid();
                    FillcomponentGrid();
                    FillsizeGrid();
                }
                else if (type == 3)
                {
                    txtdcqty.Text = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    textBox5.Focus();
                }
                else if (type == 4)
                {
                    textBox5.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    textBox5.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();



                }
                else if (type == 5)
                {
                    textBox5.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    txtaddnotes.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtprice.Text = DataGridCommon.Rows[Index].Cells[2].Value.ToString();
                    txtuom.Text = DataGridCommon.Rows[Index].Cells[3].Value.ToString();
                    txtweight.Text = DataGridCommon.Rows[Index].Cells[4].Value.ToString();
                    txtwok.Text = DataGridCommon.Rows[Index].Cells[3].Value.ToString();
                }

                grSearch.Visible = false;
                SelectId = 0;
            }
        }

        private void button10_Click(object sender, EventArgs e)
        {

            int Index = HFGP2.SelectedCells[0].RowIndex;
            txtdcqty.Text = HFGP2.Rows[Index].Cells[1].Value.ToString();
            txtdcqty.Tag = HFGP2.Rows[Index].Cells[0].Value.ToString();
            txtuom.Text = HFGP2.Rows[Index].Cells[3].Value.ToString();
            txtoutputid.Text = HFGP2.Rows[Index].Cells[3].Value.ToString();
            txtitemid.Text = HFGP2.Rows[Index].Cells[4].Value.ToString();
            txtprice.Text = HFGP2.Rows[Index].Cells[5].Value.ToString();
            txtprice.Tag = HFGP2.Rows[Index].Cells[6].Value.ToString();
            txtisstot.Text = HFGP2.Rows[Index].Cells[7].Value.ToString();
            txtisstot.Tag = HFGP2.Rows[Index].Cells[10].Value.ToString();
            lkppnl.Visible = false;
            txtaddnotes.Focus();
        }

        private void HFGP2_DoubleClick(object sender, EventArgs e)
        {
            int Index = HFGP2.SelectedCells[0].RowIndex;

            txtdcqty.Text = HFGP2.Rows[Index].Cells[1].Value.ToString();
            txtdcqty.Tag = HFGP2.Rows[Index].Cells[0].Value.ToString();
            txtuom.Text = HFGP2.Rows[Index].Cells[3].Value.ToString();
            txtoutputid.Text = HFGP2.Rows[Index].Cells[3].Value.ToString();
            txtitemid.Text = HFGP2.Rows[Index].Cells[4].Value.ToString();
            txtprice.Text = HFGP2.Rows[Index].Cells[5].Value.ToString();
            txtprice.Tag = HFGP2.Rows[Index].Cells[6].Value.ToString();
            txtisstot.Text = HFGP2.Rows[Index].Cells[7].Value.ToString();
            txtisstot.Tag = HFGP2.Rows[Index].Cells[10].Value.ToString();
            lkppnl.Visible = false;
            txtaddnotes.Focus();
        }

        private void HFGP2_KeyDown(object sender, KeyEventArgs e)
        {

            if (e.KeyCode == Keys.Enter)
            {
                int Index = HFGP2.SelectedCells[0].RowIndex;
                txtdcqty.Text = HFGP2.Rows[Index].Cells[1].Value.ToString();
                txtdcqty.Tag = HFGP2.Rows[Index].Cells[0].Value.ToString();
                txtuom.Text = HFGP2.Rows[Index].Cells[3].Value.ToString();
                txtoutputid.Text = HFGP2.Rows[Index].Cells[3].Value.ToString();
                txtitemid.Text = HFGP2.Rows[Index].Cells[4].Value.ToString();
                txtprice.Text = HFGP2.Rows[Index].Cells[5].Value.ToString();
                txtprice.Tag = HFGP2.Rows[Index].Cells[6].Value.ToString();
                txtisstot.Text = HFGP2.Rows[Index].Cells[7].Value.ToString();
                txtisstot.Tag = HFGP2.Rows[Index].Cells[10].Value.ToString();
                lkppnl.Visible = false;
                txtaddnotes.Focus();
            }
        }

        private void buttnfinbk_Click(object sender, EventArgs e)
        {
            Editpnl.Visible = false;
            Genpan.Visible = true;
            panadd.Visible = true;
            LoadGetJobCard(1);
        }

        private void btnadd_Click(object sender, EventArgs e)
        {

          
            foreach (DataGridViewRow row1 in DataGridSizeCombo.Rows)
            {
                DataGridViewCheckBoxCell cellComponent1 = row1.Cells[0] as DataGridViewCheckBoxCell;
                if (cellComponent1.Value != null)
                {
                    if (cellComponent1.Value.ToString() == "True")
                    {
                        foreach (DataGridViewRow row2 in componentgrid.Rows)
                        {
                            DataGridViewCheckBoxCell cellComponent = row2.Cells[0] as DataGridViewCheckBoxCell;
                            if (cellComponent.Value != null)
                            {
                                if (cellComponent.Value.ToString() == "True")
                                {
                                    foreach (DataGridViewRow row3 in DataGridSizeWise.Rows)
                                    {
                                        DataGridViewCheckBoxCell cellComponent2 = row3.Cells[0] as DataGridViewCheckBoxCell;
                                        if (cellComponent2.Value != null)
                                        {
                                            if (cellComponent2.Value.ToString() == "True")
                                            {
                                                
                                                SqlParameter[] sqlParameters = { new SqlParameter("@socno", txtqty.Text) };
                                                DataTable dataTable = db.GetDataWithParam(CommandType.StoredProcedure, "sp_getproductionissueuom", sqlParameters, conn);
                                                if (!string.IsNullOrEmpty(dataTable.Rows[0]["Uom"].ToString()))
                                                {
                                                    Genclass.address = Convert.ToString(dataTable.Rows[0]["Uom"].ToString());
                                                  
                                                }

                                                int index = HFIT.Rows.Add();
                                                DataGridViewRow row = (DataGridViewRow)HFIT.Rows[index];
                                                row.Cells[0].Value = "0";
                                                row.Cells[1].Value = row1.Cells[1].Value.ToString();
                                                row.Cells[2].Value = row2.Cells[1].Value.ToString(); ;
                                                row.Cells[3].Value = row3.Cells[1].Value.ToString();
                                                row.Cells[4].Value = Genclass.address;

                                                SqlParameter[] sqlParameters1 = { new SqlParameter("@socno", txtqty.Text) , new SqlParameter("@combo", row1.Cells[1].Value.ToString()) , new SqlParameter("@comp", row2.Cells[1].Value.ToString()) , new SqlParameter("@size", row3.Cells[1].Value.ToString()) };
                                                DataTable dataTable1 = db.GetDataWithParam(CommandType.StoredProcedure, "sp_getproductionissueQtyweight", sqlParameters1, conn);
                                                if (dataTable1.Rows.Count>0)
                                                {
                                                    row.Cells[5].Value = dataTable1.Rows[0]["qty"].ToString();
                                                    row.Cells[6].Value = "0";
                                                    row.Cells[7].Value = dataTable1.Rows[0]["DETTABLE_UID"].ToString();
                                                    row.Cells[8].Value = dataTable1.Rows[0]["DETTABLE_UID"].ToString();
                                                    row.Cells[9].Value = cbocuttype.Text;
                                                    row.Cells[10].Value = dataTable1.Rows[0]["weight"].ToString();
                                                }

                                              
                                             



                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            //if (txtdcqty.Text == "")
            //{
            //    MessageBox.Show("Enter the Item");
            //    txtdcqty.Focus();
            //    return;
            //}
            //if (txtaddnotes.Text == "")
            //{
            //    MessageBox.Show("Enter the Batchno");
            //    txtaddnotes.Focus();
            //    return;
            //}

            //if (textBox1.Text == "")
            //{
            //    MessageBox.Show("Enter the rolls");
            //    textBox1.Focus();
            //    return;
            //}
            //if (txtuom.Text == "")
            //{
            //    MessageBox.Show("Enter the Qty");
            //    txtuom.Focus();
            //    return;
            //}


            //double pp = Convert.ToDouble(txtwok.Text);

            //double uu = Convert.ToDouble(txtuom.Text);
            //if (pp <uu)

            //{
            //    MessageBox.Show("Qty Exceeds");
            //    return;

            //}


            //kk = HFIT.Rows.Count - 1;
            //h = kk;



            //    h = h + 1;
            //    var index = HFIT.Rows.Add();
            //    HFIT.Rows[index].Cells[0].Value = "0";
            //    HFIT.Rows[index].Cells[1].Value = txtdcqty.Text;

            //    HFIT.Rows[index].Cells[2].Value = textBox5.Text;
            //    HFIT.Rows[index].Cells[3].Value = txtaddnotes.Text;

            //    HFIT.Rows[index].Cells[4].Value = txtprice.Text;
            //    HFIT.Rows[index].Cells[5].Value = txtuom.Text;

            //    HFIT.Rows[index].Cells[6].Value = textBox1.Text;
            //    HFIT.Rows[index].Cells[7].Value = textBox5.Tag;

            //    HFIT.Rows[index].Cells[8].Value = textBox5.Tag;
            //HFIT.Rows[index].Cells[9].Value = cbocuttype.Text;
            //HFIT.Rows[index].Cells[10].Value = txtweight.Text;

            //txtdcqty.Text = "";
            //txtstyle.Text = "";
            //txtweight.Text = "";
            //txtaddnotes.Text = "";
            //txtbags.Text = "";
            //txtuom.Text = "";
            //textBox5.Text = "";
            //txtprice.Text = "";
            //txtdcqty.Text = "";
            //textBox1.Text = "";



            //txtdcqty.Focus();
        }

        private void btnsave_Click(object sender, EventArgs e)
        {
            conn.Close();
            conn.Open();
            

    
            if (Chkedtact.Checked == true)
            {

                SP = 1;
            }
            else
            {
                SP = 0;

            }
            SqlParameter[] para ={



                    new SqlParameter("@uid",txtgrn.Tag),
                    new SqlParameter("@DOCNO",txtgrn.Text),
                    new SqlParameter("@DOCDATE",Convert.ToDateTime(dtpgrndt.Text)),
                    new SqlParameter("@PROORDERNO",txtsocno.Text),
                    new SqlParameter("@SOCNO",txtqty.Text),
                    new SqlParameter("@CUTTING",cbocuttype.Text),
                   



            };
            DataTable dataTable = db.GetDataWithParam(CommandType.StoredProcedure, "sp_CUTTINGRECEIPTM", para, conn);
            int ResponseCode = Convert.ToInt32(dataTable.Rows[0]["ResponseCode"].ToString());
            int ReturnId = Convert.ToInt32(dataTable.Rows[0]["ReturnId"].ToString());
            //db.ExecuteNonQuery(CommandType.StoredProcedure, "sp_cuttingEntryM", para, conn);
            //txtgrnid.Text= Convert.ToInt32(dataTable.Rows[0]["ReturnId"].ToString());


          

            for (int i = 0; i < HFIT.RowCount - 1; i++)
            {

                conn.Close();
                conn.Open();

                SqlParameter[] para1 ={
                   

            new SqlParameter("@uid", HFIT.Rows[i].Cells[0].Value),
                   new SqlParameter("@HEADID",ReturnId),
                    new SqlParameter("@COMBONAME",HFIT.Rows[i].Cells[1].Value),
                    new SqlParameter("@ITEMNAME", HFIT.Rows[i].Cells[2].Value),
                    new SqlParameter("@UOM", HFIT.Rows[i].Cells[3].Value),
                    new SqlParameter("@SIZE", HFIT.Rows[i].Cells[4].Value),
                              new SqlParameter("@QTY", HFIT.Rows[i].Cells[5].Value),
                                 new SqlParameter("@BUNDLES", HFIT.Rows[i].Cells[6].Value),
                    new SqlParameter("@CUTTING", HFIT.Rows[i].Cells[9].Value),
                           new SqlParameter("@FABRICID", HFIT.Rows[i].Cells[7].Value),
                                  new SqlParameter("@REFUID", HFIT.Rows[i].Cells[8].Value),
                                              new SqlParameter("@weight", HFIT.Rows[i].Cells[10].Value),

                };

                db.ExecuteNonQuery(CommandType.StoredProcedure, "CUTTINGRECEIPTDET", para1, conn);
            }



            conn.Close();
            conn.Open();
            if (mode == 1)
            {
                qur.CommandText = "update doctypem set lastno=lastno+1 where doctypeid=" + Genclass.Dtype + " and finyear='19-20'";
                qur.ExecuteNonQuery();

            }
            conn.Close();
            conn.Open();
            if (ReturnId != 0)
            {
                if (mode == 1)
                {
                    qur.CommandText = "exec SP_stocklegCUttingReceipt " + ReturnId + "," + Genclass.Dtype + "   ,1";
                    qur.ExecuteNonQuery();
                    qur.CommandText = "exec SP_PRODUCTIONISSUE_WIP_STOCK_LEDGER " + ReturnId + "  ,1," + Genclass.Dtype + " ";
                    qur.ExecuteNonQuery();

                }
                else
                {
                    qur.CommandText = "exec SP_stocklegCUttingReceipt " + ReturnId + "," + Genclass.Dtype + "   ,2";
                    qur.ExecuteNonQuery();
                    qur.CommandText = "exec SP_PRODUCTIONISSUE_WIP_STOCK_LEDGER " + ReturnId + ",2," + Genclass.Dtype + " ";
                    qur.ExecuteNonQuery();

                }

            }
            MessageBox.Show("Save Record");
            Editpnl.Visible = false;

            Genpan.Visible = true;
            panadd.Visible = true;
            LoadGetJobCard(1);



        }

        private void butedit_Click(object sender, EventArgs e)
        {
            mode = 2;
            Chkedtact.Checked = true;
            mode = 2;
            panadd.Visible = false;
            Genpan.Visible = false;
            Editpnl.Visible = true;
            lkppnl.Visible = false;
            conn.Close();
            conn.Open();
          

            //cbosGReturnItem.Items.Clear();
            int i = HFGP.SelectedCells[0].RowIndex;
            uid = HFGP.Rows[i].Cells[0].Value.ToString();
            txtgrnid.Text = HFGP.Rows[i].Cells[0].Value.ToString();
            txtgrn.Tag = Convert.ToInt32(HFGP.Rows[i].Cells[0].Value.ToString());
            txtgrn.Text = HFGP.Rows[i].Cells[1].Value.ToString();
            dtpgrndt.Text = HFGP.Rows[i].Cells[2].Value.ToString();

            txtsocno.Text = HFGP.Rows[i].Cells[3].Value.ToString();
            txtqty.Text = HFGP.Rows[i].Cells[4].Value.ToString();
            cbocuttype.Text = HFGP.Rows[i].Cells[5].Value.ToString();
         
            Chkedtact.Checked = true;
            HFIT.Refresh();
            HFIT.DataSource = null;
            HFIT.Rows.Clear();

            HFGP2.Refresh();
            HFGP2.DataSource = null;
            HFGP2.Rows.Clear();
            HFGP1.Refresh();
            HFGP1.DataSource = null;
            HFGP1.Rows.Clear();

            Titlep();
            load();

        }
        private void load()
        {
            Genclass.strsql = "SP_GETCUTTINGRECEIPD " + txtgrnid.Text + " ";
            Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
            SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
            DataTable tap1 = new DataTable();
            aptr1.Fill(tap1);

            Genclass.sum1 = 0;


            for (int k = 0; k < tap1.Rows.Count; k++)
            {
                var index = HFIT.Rows.Add();

                HFIT.Rows[index].Cells[0].Value = tap1.Rows[k]["uid"].ToString();

                HFIT.Rows[index].Cells[1].Value = tap1.Rows[k]["comboname"].ToString();


                HFIT.Rows[index].Cells[2].Value = tap1.Rows[k]["itemname"].ToString();

                HFIT.Rows[index].Cells[3].Value = tap1.Rows[k]["uom"].ToString();

                HFIT.Rows[index].Cells[4].Value = tap1.Rows[k]["size"].ToString();


                HFIT.Rows[index].Cells[5].Value = tap1.Rows[k]["qty"].ToString();
                HFIT.Rows[index].Cells[6].Value = tap1.Rows[k]["bundles"].ToString();
                HFIT.Rows[index].Cells[7].Value = tap1.Rows[k]["fabricid"].ToString();
                HFIT.Rows[index].Cells[8].Value = tap1.Rows[k]["refuid"].ToString();
                HFIT.Rows[index].Cells[9].Value = tap1.Rows[k]["cutting"].ToString();
                HFIT.Rows[index].Cells[10].Value = tap1.Rows[k]["weight"].ToString();

            }
        }

        private void dtpgrndt_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                e.Handled = true;
                SendKeys.Send("{TAB}");
            }
        }

        private void txtqty_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                e.Handled = true;
                SendKeys.Send("{TAB}");
            }
        }

        private void txtmode_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                e.Handled = true;
                SendKeys.Send("{TAB}");
            }
        }

        private void txtvehicle_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                e.Handled = true;
                SendKeys.Send("{TAB}");
            }
        }

        private void txtdcqty_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                e.Handled = true;
                SendKeys.Send("{TAB}");
            }
        }

        private void txtaddnotes_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                e.Handled = true;
                SendKeys.Send("{TAB}");
            }
        }

        private void txtisstot_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                e.Handled = true;
                SendKeys.Send("{TAB}");
            }
        }

        private void txtbags_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                e.Handled = true;
                SendKeys.Send("{TAB}");
            }
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                e.Handled = true;
                SendKeys.Send("{TAB}");
            }
        }

        private void txtprice_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                e.Handled = true;
                SendKeys.Send("{TAB}");
            }

        }

        private void txtuom_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void button11_Click(object sender, EventArgs e)
        {
            lkppnl.Visible = false;
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void buttnext1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void DataGridCommon_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dtpsupp_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                e.Handled = true;
                SendKeys.Send("{TAB}");
            }
        }

        private void txtscr11_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.F10)
                {
                    lkppnl.Visible = false;
                }
                else if (e.KeyCode == Keys.F2)
                {
                    SelectId = 1;


                    int Index = HFGP2.SelectedCells[0].RowIndex;
                    txtdcqty.Text = HFGP2.Rows[Index].Cells[1].Value.ToString();
                    txtdcqty.Tag = HFGP2.Rows[Index].Cells[0].Value.ToString();
                    txtuom.Text = HFGP2.Rows[Index].Cells[3].Value.ToString();
                    txtitemid.Text = HFGP2.Rows[Index].Cells[4].Value.ToString();
                    txtprice.Text = HFGP2.Rows[Index].Cells[5].Value.ToString();
                    txtprice.Tag = HFGP2.Rows[Index].Cells[6].Value.ToString();
                    txtisstot.Text = HFGP2.Rows[Index].Cells[7].Value.ToString();
                    lkppnl.Visible = false;
                    txtaddnotes.Focus();

                    SelectId = 0;
                }
                else if (e.KeyCode == Keys.Escape)
                {
                    lkppnl.Visible = false;
                }
                else if (e.KeyValue == 40)
                {
                    HFGP2.Select();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtscr11_TextChanged(object sender, EventArgs e)
        {
            bsc.Filter = string.Format("itemdes LIKE '%{0}%' ", txtscr11.Text);
        }

        private void txtuom_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtuom_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)

            {
                btnadd_Click(sender, e);

            }
        }

        private void button13_Click(object sender, EventArgs e)
        {

        }

        private void txtdcno_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtname.Text == string.Empty)
                {
                    MessageBox.Show("Select Supplier Name", "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtname.Focus();
                    return;
                }
                if (txtmode.Text == string.Empty)
                {
                    MessageBox.Show("Enter Supplier DC Number and Date", "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtmode.Focus();
                    return;
                }
                txtscr11.Text = "";
                type = 10;
                txtscr11.Focus();
                Genclass.strsql = "SP_GETKNITRECDCNO   " + txtpuid.Text + "";
                Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
                DataTable tap = new DataTable();
                aptr.Fill(tap);
                bsc.DataSource = tap;
                Point loc = FindLocation(txtdcno);
                grSearch.Location = new Point(loc.X, loc.Y + 20);
                grSearch.Visible = true;
                DataGridCommon.AutoGenerateColumns = false;
                DataGridCommon.Refresh();
                DataGridCommon.DataSource = null;
                DataGridCommon.Rows.Clear();
                DataGridCommon.ColumnCount = tap.Columns.Count;
                i = 0;
                foreach (DataColumn column in tap.Columns)
                {
                    DataGridCommon.Columns[i].Name = column.ColumnName;
                    DataGridCommon.Columns[i].HeaderText = column.ColumnName;
                    DataGridCommon.Columns[i].DataPropertyName = column.ColumnName;
                    i = i + 1;
                }
                DataGridCommon.DataSource = tap;
                DataGridCommon.Columns[0].Width = 100;
                DataGridCommon.Columns[1].Width = 100;
                DataGridCommon.Columns[2].Width = 100;
                DataGridCommon.Columns[3].Width = 100;
                DataGridCommon.Columns[4].Visible = false;
                DataGridCommon.Columns[5].Visible = false;
                conn.Close();
            }
            catch (Exception)
            {

                throw;
            }
        }

        private void butcan_Click(object sender, EventArgs e)
        {
            conn.Close();
            conn.Open();



            int i = HFGP.SelectedCells[0].RowIndex;

       
            qur.CommandText = "delete  from  CUTTINGRECEIPTD  where headid=" + HFGP.Rows[i].Cells[0].Value.ToString() + "";
            qur.ExecuteNonQuery();
            qur.CommandText = "delete  from  CUTTINGRECEIPTM  where uid=" + HFGP.Rows[i].Cells[0].Value.ToString() + "";
            qur.ExecuteNonQuery();
            MessageBox.Show("Deleted");
            LoadGetJobCard(1);
        }

        private void txtdcno_TextChanged(object sender, EventArgs e)
        {

        }

        private void Editpnl_Paint(object sender, PaintEventArgs e)
        {

        }

        private void chkact_CheckedChanged(object sender, EventArgs e)
        {
            LoadGetJobCard(1);
        }

        private void txtscr1_TextChanged(object sender, EventArgs e)
        {
            bs.Filter = string.Format("DOcno Like '%{0}%'  or  Docdate Like '%{1}%' or  name Like '%{1}%' or  Reference Like '%{1}%' or  WorkOrderNo Like '%{1}%'  or  ProcessName Like '%{1}%'  or  vehicleno Like '%{1}%' ", txtscr1.Text, txtscr1.Text, txtscr1.Text, txtscr1.Text, txtscr1.Text, txtscr1.Text, txtscr1.Text);

        }

        private void Genpan_Paint(object sender, PaintEventArgs e)
        {

        }

        private void HFGP2_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtscono_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtname.Text == string.Empty)
                {
                    MessageBox.Show("Select Supplier Name", "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtname.Focus();
                    return;
                }
                if (txtmode.Text == string.Empty)
                {
                    MessageBox.Show("Enter Supplier DC Number and Date", "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtmode.Focus();
                    return;
                }
                txtscr11.Text = "";
                type = 11;
                txtscr11.Focus();
                if (Text == "Knitting Without Process")
                {
                    Genclass.strsql = "SP_GETKNITWODCNO   " + txtpuid.Text + "";
                    Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
                }
                else

                {
                    Genclass.strsql = "SP_GETFABTWODCNO   " + txtpuid.Text + "";
                    Genclass.cmd = new SqlCommand(Genclass.strsql, conn);

                }
                SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
                DataTable tap = new DataTable();
                aptr.Fill(tap);
                bsc.DataSource = tap;
                Point loc = FindLocation(txtscono);
                grSearch.Location = new Point(loc.X, loc.Y + 20);
                grSearch.Visible = true;
                DataGridCommon.AutoGenerateColumns = false;
                DataGridCommon.Refresh();
                DataGridCommon.DataSource = null;
                DataGridCommon.Rows.Clear();
                DataGridCommon.ColumnCount = tap.Columns.Count;
                i = 0;
                foreach (DataColumn column in tap.Columns)
                {
                    DataGridCommon.Columns[i].Name = column.ColumnName;
                    DataGridCommon.Columns[i].HeaderText = column.ColumnName;
                    DataGridCommon.Columns[i].DataPropertyName = column.ColumnName;
                    i = i + 1;
                }
                DataGridCommon.DataSource = tap;
                DataGridCommon.Columns[0].Visible = true;
                DataGridCommon.Columns[0].Width = 100;
                DataGridCommon.Columns[1].Width = 100;
                DataGridCommon.Columns[2].Width = 100;
                DataGridCommon.Columns[3].Visible = false;
                DataGridCommon.Columns[4].Visible = false;
                DataGridCommon.Columns[5].Visible = false;
                DataGridCommon.Columns[6].Width = 100;
                conn.Close();
            }
            catch (Exception)
            {

                throw;
            }
        }

        private void txtscono_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    if (txtscono.Text != "")
                    {
                        bsc.Filter = string.Format("socno LIKE '%{0}%' ", txtscono.Text);

                    }

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtqty_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bsFabric.Filter = string.Format("docno LIKE '%{0}%' ", txtqty.Text);



                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtqty_Click(object sender, EventArgs e)
        {
            type = 1;
            DataTable dt = getParty();
            bsFabric.DataSource = dt;
            FillGrid2(dt, 1);
            Point loc = FindLocation(txtqty);
            grSearch.Location = new Point(loc.X, loc.Y + 20);
            grSearch.Visible = true;
        }

        private void textBox5_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bsParty.Filter = string.Format("itemname LIKE '%{0}%' ", textBox5.Text);



                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void textBox5_Click(object sender, EventArgs e)
        {

            type = 4;
            Point loc = FindLocation(textBox5);
            grSearch.Location = new Point(loc.X, loc.Y + 20);
            grSearch.Visible = true;
                       componentname();
        }

        private void txtaddnotes_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bs.Filter = string.Format("size LIKE '%{0}%' ", txtaddnotes.Text);



                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtaddnotes_Click(object sender, EventArgs e)
        {
            type = 5;
            Point loc = FindLocation(txtaddnotes);
            grSearch.Location = new Point(loc.X, loc.Y + 20);
            grSearch.Visible = true;
            size();
        }
        private void size()

        {

            string qur1 = "select   DETTABLE_UID,Size,uom,isnull(sum(rqty),0) -isnull(sum(qty),0)  as Qty,isnull(sum(Weight),0) as Weight from (select  DETTABLE_UID,a.Size,a.uom,a.Weight,isnull(A.qty,0) as rQty,isnull(b.QTY,0) as QTy  from WIP_STOCK_LEDGER A  inner join cuttingEntryM c on a.HEADTABLE_UID=c.uid inner join cuttingComboComponentList d on a.DETTABLE_UID = d.uid LEFT JOIN CUTTINGRECEIPTD B ON A.DETTABLE_UID=B.UID   where a.SOCNO='" + txtqty.Text + "' and a.comboname='"+ txtdcqty.Text + "'  and a.component='" + textBox5.Text + "'   and doctypeid=1320)tab group by   DETTABLE_UId,Size,uom    HAVING isnull(sum(rQTY),0)-isnull(sum(QTY),0)>0   ";
            SqlCommand cmd1 = new SqlCommand(qur1, conn);
            SqlDataAdapter apt1 = new SqlDataAdapter(cmd1);
            DataTable tab1 = new DataTable();
            apt1.Fill(tab1);
            bs.DataSource = tab1;
            //Point loc = FindLocation(txtdcqty);
            //grSearch.Location = new Point(loc.X, loc.Y + 20);
            grSearch.Visible = true;
            DataGridCommon.AutoGenerateColumns = false;
            DataGridCommon.Refresh();
            DataGridCommon.DataSource = null;
            DataGridCommon.Rows.Clear();
            DataGridCommon.ColumnCount = tab1.Columns.Count;
            i = 0;
            foreach (DataColumn column in tab1.Columns)
            {
                DataGridCommon.Columns[i].Name = column.ColumnName;
                DataGridCommon.Columns[i].HeaderText = column.ColumnName;
                DataGridCommon.Columns[i].DataPropertyName = column.ColumnName;
                i = i + 1;
            }

            DataGridCommon.DataSource = tab1;

            DataGridCommon.Columns[1].Width = 150;

            DataGridCommon.Columns[0].Visible = false;
            DataGridCommon.Columns[2].Visible = false;
            DataGridCommon.Columns[4].Visible = false;
            DataGridCommon.Columns[3].Width = 150;
            //DataGridCommon.Columns[5].Visible = false;
        }

        private void HFIT_CellClick(object sender, DataGridViewCellEventArgs e)
        {
               if (HFIT.CurrentRow.Cells[0].Value != null && HFIT.CurrentCell.ColumnIndex == 5)
            {
                if (HFIT.CurrentCell.ColumnIndex == 5)

                {
                    DataGridViewCell cell = HFIT.CurrentRow.Cells[5];
                    HFIT.CurrentCell = cell;
                    HFIT.BeginEdit(true);
                }

            }
               else if (HFIT.CurrentRow.Cells[0].Value != null && HFIT.CurrentCell.ColumnIndex == 6)
            {
                if (HFIT.CurrentCell.ColumnIndex == 6)

                {
                    DataGridViewCell cell = HFIT.CurrentRow.Cells[6];
                    HFIT.CurrentCell = cell;
                    HFIT.BeginEdit(true);
                }

            }
        }
    }
}
