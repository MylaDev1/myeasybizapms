﻿namespace MyEasyBizAPMS
{
    partial class FrmJORKnitting
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmJORKnitting));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.Genpan = new System.Windows.Forms.Panel();
            this.txtscr7 = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtscr1 = new System.Windows.Forms.TextBox();
            this.HFGP = new System.Windows.Forms.DataGridView();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.flowLayoutPanel4 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel5 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel6 = new System.Windows.Forms.FlowLayoutPanel();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.label57 = new System.Windows.Forms.Label();
            this.dtpfnt = new System.Windows.Forms.DateTimePicker();
            this.txtscr8 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.txtscr6 = new System.Windows.Forms.TextBox();
            this.txtscr5 = new System.Windows.Forms.TextBox();
            this.Txtscr2 = new System.Windows.Forms.TextBox();
            this.txtscr4 = new System.Windows.Forms.TextBox();
            this.Txtscr3 = new System.Windows.Forms.TextBox();
            this.panadd = new System.Windows.Forms.Panel();
            this.button13 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.buttnext1 = new System.Windows.Forms.Button();
            this.chkact = new System.Windows.Forms.CheckBox();
            this.butcan = new System.Windows.Forms.Button();
            this.butedit = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.txtbags = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.txtisstot = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.txtrectot = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.txtnobeams = new System.Windows.Forms.TextBox();
            this.HFGP1 = new System.Windows.Forms.DataGridView();
            this.txtuom = new System.Windows.Forms.TextBox();
            this.txtgrnid = new System.Windows.Forms.TextBox();
            this.txtpuid = new System.Windows.Forms.TextBox();
            this.txtmillid = new System.Windows.Forms.TextBox();
            this.txtitemid = new System.Windows.Forms.TextBox();
            this.txtoutputid = new System.Windows.Forms.TextBox();
            this.txtbeamid = new System.Windows.Forms.TextBox();
            this.txtname = new System.Windows.Forms.TextBox();
            this.dtpgrndt = new System.Windows.Forms.DateTimePicker();
            this.txtgrn = new System.Windows.Forms.TextBox();
            this.Phone = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtvehicle = new System.Windows.Forms.TextBox();
            this.txtitem = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtqty = new System.Windows.Forms.TextBox();
            this.txtdcqty = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.HFIT = new System.Windows.Forms.DataGridView();
            this.label4 = new System.Windows.Forms.Label();
            this.grSearch = new System.Windows.Forms.Panel();
            this.button18 = new System.Windows.Forms.Button();
            this.btnHide = new System.Windows.Forms.Button();
            this.DataGridCommon = new System.Windows.Forms.DataGridView();
            this.txtprice = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtaddnotes = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.Editpnl = new System.Windows.Forms.Panel();
            this.lkppnl = new System.Windows.Forms.Panel();
            this.label21 = new System.Windows.Forms.Label();
            this.txtstyle = new System.Windows.Forms.TextBox();
            this.txtscono = new System.Windows.Forms.TextBox();
            this.label36 = new System.Windows.Forms.Label();
            this.txtwok = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.button10 = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.txtscr11 = new System.Windows.Forms.TextBox();
            this.HFGP2 = new System.Windows.Forms.DataGridView();
            this.label69 = new System.Windows.Forms.Label();
            this.label67 = new System.Windows.Forms.Label();
            this.cbowono = new System.Windows.Forms.ComboBox();
            this.label23 = new System.Windows.Forms.Label();
            this.txtdcno = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.dtpsupp = new System.Windows.Forms.DateTimePicker();
            this.btnadd = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.txtmode = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.txtwo = new System.Windows.Forms.TextBox();
            this.Chkedtact = new System.Windows.Forms.CheckBox();
            this.buttnfinbk = new System.Windows.Forms.Button();
            this.btnsave = new System.Windows.Forms.Button();
            this.Genpan.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HFGP)).BeginInit();
            this.panel2.SuspendLayout();
            this.panadd.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HFGP1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HFIT)).BeginInit();
            this.grSearch.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridCommon)).BeginInit();
            this.Editpnl.SuspendLayout();
            this.lkppnl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HFGP2)).BeginInit();
            this.SuspendLayout();
            // 
            // Genpan
            // 
            this.Genpan.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.Genpan.Controls.Add(this.txtscr7);
            this.Genpan.Controls.Add(this.label11);
            this.Genpan.Controls.Add(this.txtscr1);
            this.Genpan.Controls.Add(this.HFGP);
            this.Genpan.Controls.Add(this.panel2);
            this.Genpan.Controls.Add(this.button5);
            this.Genpan.Controls.Add(this.button6);
            this.Genpan.Controls.Add(this.button7);
            this.Genpan.Controls.Add(this.button8);
            this.Genpan.Controls.Add(this.label57);
            this.Genpan.Controls.Add(this.dtpfnt);
            this.Genpan.Controls.Add(this.txtscr8);
            this.Genpan.Controls.Add(this.textBox2);
            this.Genpan.Controls.Add(this.textBox3);
            this.Genpan.Controls.Add(this.txtscr6);
            this.Genpan.Controls.Add(this.txtscr5);
            this.Genpan.Controls.Add(this.Txtscr2);
            this.Genpan.Controls.Add(this.txtscr4);
            this.Genpan.Controls.Add(this.Txtscr3);
            this.Genpan.Location = new System.Drawing.Point(1, -1);
            this.Genpan.Margin = new System.Windows.Forms.Padding(4);
            this.Genpan.Name = "Genpan";
            this.Genpan.Size = new System.Drawing.Size(1009, 550);
            this.Genpan.TabIndex = 215;
            this.Genpan.Paint += new System.Windows.Forms.PaintEventHandler(this.Genpan_Paint);
            // 
            // txtscr7
            // 
            this.txtscr7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtscr7.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtscr7.Location = new System.Drawing.Point(1048, 43);
            this.txtscr7.Margin = new System.Windows.Forms.Padding(5);
            this.txtscr7.Name = "txtscr7";
            this.txtscr7.Size = new System.Drawing.Size(164, 26);
            this.txtscr7.TabIndex = 224;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(2, 14);
            this.label11.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(133, 23);
            this.label11.TabIndex = 201;
            this.label11.Text = " Knitting Receipt";
            // 
            // txtscr1
            // 
            this.txtscr1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtscr1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtscr1.Location = new System.Drawing.Point(1, 43);
            this.txtscr1.Margin = new System.Windows.Forms.Padding(5);
            this.txtscr1.Name = "txtscr1";
            this.txtscr1.Size = new System.Drawing.Size(989, 26);
            this.txtscr1.TabIndex = 1;
            this.txtscr1.TextChanged += new System.EventHandler(this.txtscr1_TextChanged);
            // 
            // HFGP
            // 
            this.HFGP.BackgroundColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.HFGP.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.HFGP.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnF2;
            this.HFGP.Location = new System.Drawing.Point(1, 74);
            this.HFGP.Margin = new System.Windows.Forms.Padding(4);
            this.HFGP.Name = "HFGP";
            this.HFGP.ReadOnly = true;
            this.HFGP.Size = new System.Drawing.Size(990, 466);
            this.HFGP.TabIndex = 3;
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel2.Controls.Add(this.label12);
            this.panel2.Controls.Add(this.label13);
            this.panel2.Controls.Add(this.flowLayoutPanel4);
            this.panel2.Controls.Add(this.flowLayoutPanel5);
            this.panel2.Controls.Add(this.flowLayoutPanel6);
            this.panel2.Location = new System.Drawing.Point(66, 390);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(60, 30);
            this.panel2.TabIndex = 214;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(4, 5);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(15, 16);
            this.label12.TabIndex = 163;
            this.label12.Text = "1";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Black;
            this.label13.Location = new System.Drawing.Point(27, 5);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(29, 16);
            this.label13.TabIndex = 162;
            this.label13.Text = "of 1";
            // 
            // flowLayoutPanel4
            // 
            this.flowLayoutPanel4.Location = new System.Drawing.Point(61, 30);
            this.flowLayoutPanel4.Name = "flowLayoutPanel4";
            this.flowLayoutPanel4.Size = new System.Drawing.Size(200, 100);
            this.flowLayoutPanel4.TabIndex = 2;
            // 
            // flowLayoutPanel5
            // 
            this.flowLayoutPanel5.Location = new System.Drawing.Point(38, 35);
            this.flowLayoutPanel5.Name = "flowLayoutPanel5";
            this.flowLayoutPanel5.Size = new System.Drawing.Size(200, 100);
            this.flowLayoutPanel5.TabIndex = 1;
            // 
            // flowLayoutPanel6
            // 
            this.flowLayoutPanel6.Location = new System.Drawing.Point(62, -129);
            this.flowLayoutPanel6.Name = "flowLayoutPanel6";
            this.flowLayoutPanel6.Size = new System.Drawing.Size(200, 100);
            this.flowLayoutPanel6.TabIndex = 0;
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.Color.White;
            this.button5.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button5.FlatAppearance.BorderSize = 0;
            this.button5.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button5.Image = ((System.Drawing.Image)(resources.GetObject("button5.Image")));
            this.button5.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button5.Location = new System.Drawing.Point(19, 390);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(19, 31);
            this.button5.TabIndex = 213;
            this.button5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button5.UseVisualStyleBackColor = false;
            // 
            // button6
            // 
            this.button6.BackColor = System.Drawing.Color.White;
            this.button6.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button6.FlatAppearance.BorderSize = 0;
            this.button6.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button6.Image = ((System.Drawing.Image)(resources.GetObject("button6.Image")));
            this.button6.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button6.Location = new System.Drawing.Point(44, 390);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(18, 31);
            this.button6.TabIndex = 212;
            this.button6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button6.UseVisualStyleBackColor = false;
            // 
            // button7
            // 
            this.button7.BackColor = System.Drawing.Color.White;
            this.button7.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button7.FlatAppearance.BorderSize = 0;
            this.button7.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button7.Image = ((System.Drawing.Image)(resources.GetObject("button7.Image")));
            this.button7.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button7.Location = new System.Drawing.Point(156, 390);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(19, 31);
            this.button7.TabIndex = 211;
            this.button7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button7.UseVisualStyleBackColor = false;
            // 
            // button8
            // 
            this.button8.BackColor = System.Drawing.Color.White;
            this.button8.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button8.FlatAppearance.BorderSize = 0;
            this.button8.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button8.Image = ((System.Drawing.Image)(resources.GetObject("button8.Image")));
            this.button8.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button8.Location = new System.Drawing.Point(132, 390);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(18, 31);
            this.button8.TabIndex = 210;
            this.button8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button8.UseVisualStyleBackColor = false;
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label57.Location = new System.Drawing.Point(411, 167);
            this.label57.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(42, 21);
            this.label57.TabIndex = 223;
            this.label57.Text = "Date";
            // 
            // dtpfnt
            // 
            this.dtpfnt.CustomFormat = "MMM/yyyy";
            this.dtpfnt.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpfnt.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpfnt.Location = new System.Drawing.Point(458, 164);
            this.dtpfnt.Margin = new System.Windows.Forms.Padding(4);
            this.dtpfnt.Name = "dtpfnt";
            this.dtpfnt.Size = new System.Drawing.Size(104, 26);
            this.dtpfnt.TabIndex = 222;
            this.dtpfnt.Value = new System.DateTime(2017, 7, 4, 0, 0, 0, 0);
            // 
            // txtscr8
            // 
            this.txtscr8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtscr8.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtscr8.Location = new System.Drawing.Point(651, 230);
            this.txtscr8.Margin = new System.Windows.Forms.Padding(5);
            this.txtscr8.Name = "txtscr8";
            this.txtscr8.Size = new System.Drawing.Size(100, 26);
            this.txtscr8.TabIndex = 225;
            // 
            // textBox2
            // 
            this.textBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox2.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox2.Location = new System.Drawing.Point(694, 162);
            this.textBox2.Margin = new System.Windows.Forms.Padding(5);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(101, 26);
            this.textBox2.TabIndex = 227;
            // 
            // textBox3
            // 
            this.textBox3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox3.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox3.Location = new System.Drawing.Point(563, 162);
            this.textBox3.Margin = new System.Windows.Forms.Padding(5);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(150, 26);
            this.textBox3.TabIndex = 226;
            this.textBox3.TextChanged += new System.EventHandler(this.textBox3_TextChanged);
            // 
            // txtscr6
            // 
            this.txtscr6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtscr6.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtscr6.Location = new System.Drawing.Point(717, 162);
            this.txtscr6.Margin = new System.Windows.Forms.Padding(5);
            this.txtscr6.Name = "txtscr6";
            this.txtscr6.Size = new System.Drawing.Size(150, 26);
            this.txtscr6.TabIndex = 202;
            // 
            // txtscr5
            // 
            this.txtscr5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtscr5.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtscr5.Location = new System.Drawing.Point(108, 162);
            this.txtscr5.Margin = new System.Windows.Forms.Padding(5);
            this.txtscr5.Name = "txtscr5";
            this.txtscr5.Size = new System.Drawing.Size(456, 26);
            this.txtscr5.TabIndex = 90;
            // 
            // Txtscr2
            // 
            this.Txtscr2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Txtscr2.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txtscr2.Location = new System.Drawing.Point(19, 162);
            this.Txtscr2.Margin = new System.Windows.Forms.Padding(5);
            this.Txtscr2.Name = "Txtscr2";
            this.Txtscr2.Size = new System.Drawing.Size(90, 26);
            this.Txtscr2.TabIndex = 87;
            // 
            // txtscr4
            // 
            this.txtscr4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtscr4.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtscr4.Location = new System.Drawing.Point(321, 126);
            this.txtscr4.Margin = new System.Windows.Forms.Padding(5);
            this.txtscr4.Name = "txtscr4";
            this.txtscr4.Size = new System.Drawing.Size(90, 22);
            this.txtscr4.TabIndex = 100;
            // 
            // Txtscr3
            // 
            this.Txtscr3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Txtscr3.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txtscr3.Location = new System.Drawing.Point(232, 126);
            this.Txtscr3.Margin = new System.Windows.Forms.Padding(5);
            this.Txtscr3.Name = "Txtscr3";
            this.Txtscr3.Size = new System.Drawing.Size(90, 22);
            this.Txtscr3.TabIndex = 88;
            // 
            // panadd
            // 
            this.panadd.BackColor = System.Drawing.Color.White;
            this.panadd.Controls.Add(this.button13);
            this.panadd.Controls.Add(this.button1);
            this.panadd.Controls.Add(this.buttnext1);
            this.panadd.Controls.Add(this.chkact);
            this.panadd.Controls.Add(this.butcan);
            this.panadd.Controls.Add(this.butedit);
            this.panadd.Controls.Add(this.button3);
            this.panadd.Location = new System.Drawing.Point(2, 546);
            this.panadd.Name = "panadd";
            this.panadd.Size = new System.Drawing.Size(1004, 36);
            this.panadd.TabIndex = 236;
            // 
            // button13
            // 
            this.button13.BackColor = System.Drawing.Color.White;
            this.button13.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button13.Image = ((System.Drawing.Image)(resources.GetObject("button13.Image")));
            this.button13.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button13.Location = new System.Drawing.Point(379, 2);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(117, 30);
            this.button13.TabIndex = 225;
            this.button13.Text = "Print Preview";
            this.button13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button13.UseVisualStyleBackColor = false;
            this.button13.Click += new System.EventHandler(this.button13_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.White;
            this.button1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Image = ((System.Drawing.Image)(resources.GetObject("button1.Image")));
            this.button1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button1.Location = new System.Drawing.Point(311, 3);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(65, 30);
            this.button1.TabIndex = 216;
            this.button1.Text = "Print";
            this.button1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button1.UseVisualStyleBackColor = false;
            // 
            // buttnext1
            // 
            this.buttnext1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttnext1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttnext1.Image = ((System.Drawing.Image)(resources.GetObject("buttnext1.Image")));
            this.buttnext1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttnext1.Location = new System.Drawing.Point(502, 3);
            this.buttnext1.Name = "buttnext1";
            this.buttnext1.Size = new System.Drawing.Size(57, 30);
            this.buttnext1.TabIndex = 208;
            this.buttnext1.Text = "Exit";
            this.buttnext1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttnext1.UseVisualStyleBackColor = false;
            this.buttnext1.Click += new System.EventHandler(this.buttnext1_Click);
            // 
            // chkact
            // 
            this.chkact.AutoSize = true;
            this.chkact.BackColor = System.Drawing.Color.White;
            this.chkact.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkact.Location = new System.Drawing.Point(4, 8);
            this.chkact.Name = "chkact";
            this.chkact.Size = new System.Drawing.Size(66, 22);
            this.chkact.TabIndex = 187;
            this.chkact.Text = "Active";
            this.chkact.UseVisualStyleBackColor = false;
            this.chkact.CheckedChanged += new System.EventHandler(this.chkact_CheckedChanged);
            // 
            // butcan
            // 
            this.butcan.BackColor = System.Drawing.Color.White;
            this.butcan.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.butcan.Image = ((System.Drawing.Image)(resources.GetObject("butcan.Image")));
            this.butcan.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.butcan.Location = new System.Drawing.Point(228, 2);
            this.butcan.Name = "butcan";
            this.butcan.Size = new System.Drawing.Size(77, 30);
            this.butcan.TabIndex = 186;
            this.butcan.Text = "Delete";
            this.butcan.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.butcan.UseVisualStyleBackColor = false;
            this.butcan.Click += new System.EventHandler(this.butcan_Click);
            // 
            // butedit
            // 
            this.butedit.BackColor = System.Drawing.Color.White;
            this.butedit.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.butedit.Image = ((System.Drawing.Image)(resources.GetObject("butedit.Image")));
            this.butedit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.butedit.Location = new System.Drawing.Point(168, 2);
            this.butedit.Name = "butedit";
            this.butedit.Size = new System.Drawing.Size(55, 30);
            this.butedit.TabIndex = 185;
            this.butedit.Text = "Edit";
            this.butedit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.butedit.UseVisualStyleBackColor = false;
            this.butedit.Click += new System.EventHandler(this.butedit_Click);
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.White;
            this.button3.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button3.FlatAppearance.BorderSize = 0;
            this.button3.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.Image = ((System.Drawing.Image)(resources.GetObject("button3.Image")));
            this.button3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button3.Location = new System.Drawing.Point(75, 2);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(89, 30);
            this.button3.TabIndex = 184;
            this.button3.Text = "Add New";
            this.button3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(18, 112);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(74, 18);
            this.label6.TabIndex = 402;
            this.label6.Text = "ItemName";
            // 
            // txtbags
            // 
            this.txtbags.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtbags.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtbags.Location = new System.Drawing.Point(625, 134);
            this.txtbags.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtbags.Name = "txtbags";
            this.txtbags.Size = new System.Drawing.Size(83, 26);
            this.txtbags.TabIndex = 11;
            this.txtbags.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtbags_KeyPress);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(625, 112);
            this.label18.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(70, 18);
            this.label18.TabIndex = 350;
            this.label18.Text = "Actual Dia";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(876, 112);
            this.label15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(30, 18);
            this.label15.TabIndex = 223;
            this.label15.Text = "Qty";
            // 
            // txtisstot
            // 
            this.txtisstot.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtisstot.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtisstot.Location = new System.Drawing.Point(537, 134);
            this.txtisstot.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtisstot.Name = "txtisstot";
            this.txtisstot.Size = new System.Drawing.Size(87, 26);
            this.txtisstot.TabIndex = 10;
            this.txtisstot.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtisstot_KeyPress);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(537, 112);
            this.label16.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(58, 18);
            this.label16.TabIndex = 225;
            this.label16.Text = "Plan Dia";
            // 
            // txtrectot
            // 
            this.txtrectot.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtrectot.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtrectot.Location = new System.Drawing.Point(588, 199);
            this.txtrectot.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtrectot.Name = "txtrectot";
            this.txtrectot.Size = new System.Drawing.Size(90, 22);
            this.txtrectot.TabIndex = 226;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(491, 232);
            this.label17.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(98, 21);
            this.label17.TabIndex = 227;
            this.label17.Text = "No of Beams";
            // 
            // txtnobeams
            // 
            this.txtnobeams.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtnobeams.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtnobeams.Location = new System.Drawing.Point(588, 232);
            this.txtnobeams.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtnobeams.Name = "txtnobeams";
            this.txtnobeams.Size = new System.Drawing.Size(90, 22);
            this.txtnobeams.TabIndex = 228;
            // 
            // HFGP1
            // 
            this.HFGP1.BackgroundColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.HFGP1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.HFGP1.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.HFGP1.Location = new System.Drawing.Point(547, 223);
            this.HFGP1.Name = "HFGP1";
            this.HFGP1.Size = new System.Drawing.Size(87, 18);
            this.HFGP1.TabIndex = 205;
            // 
            // txtuom
            // 
            this.txtuom.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtuom.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtuom.Location = new System.Drawing.Point(879, 134);
            this.txtuom.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtuom.Name = "txtuom";
            this.txtuom.Size = new System.Drawing.Size(71, 26);
            this.txtuom.TabIndex = 14;
            this.txtuom.TextChanged += new System.EventHandler(this.txtuom_TextChanged);
            this.txtuom.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtuom_KeyDown);
            this.txtuom.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtuom_KeyPress);
            // 
            // txtgrnid
            // 
            this.txtgrnid.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtgrnid.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtgrnid.Location = new System.Drawing.Point(369, 264);
            this.txtgrnid.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtgrnid.Name = "txtgrnid";
            this.txtgrnid.Size = new System.Drawing.Size(101, 22);
            this.txtgrnid.TabIndex = 230;
            // 
            // txtpuid
            // 
            this.txtpuid.Location = new System.Drawing.Point(402, 265);
            this.txtpuid.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtpuid.Name = "txtpuid";
            this.txtpuid.Size = new System.Drawing.Size(87, 20);
            this.txtpuid.TabIndex = 218;
            // 
            // txtmillid
            // 
            this.txtmillid.Location = new System.Drawing.Point(475, 388);
            this.txtmillid.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtmillid.Name = "txtmillid";
            this.txtmillid.Size = new System.Drawing.Size(87, 20);
            this.txtmillid.TabIndex = 219;
            // 
            // txtitemid
            // 
            this.txtitemid.Location = new System.Drawing.Point(495, 422);
            this.txtitemid.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtitemid.Name = "txtitemid";
            this.txtitemid.Size = new System.Drawing.Size(87, 20);
            this.txtitemid.TabIndex = 220;
            // 
            // txtoutputid
            // 
            this.txtoutputid.Location = new System.Drawing.Point(268, 287);
            this.txtoutputid.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtoutputid.Name = "txtoutputid";
            this.txtoutputid.Size = new System.Drawing.Size(87, 20);
            this.txtoutputid.TabIndex = 221;
            // 
            // txtbeamid
            // 
            this.txtbeamid.Location = new System.Drawing.Point(564, 265);
            this.txtbeamid.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.txtbeamid.Name = "txtbeamid";
            this.txtbeamid.Size = new System.Drawing.Size(87, 20);
            this.txtbeamid.TabIndex = 222;
            // 
            // txtname
            // 
            this.txtname.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtname.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtname.Location = new System.Drawing.Point(430, 30);
            this.txtname.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtname.Name = "txtname";
            this.txtname.Size = new System.Drawing.Size(400, 27);
            this.txtname.TabIndex = 2;
            this.txtname.Click += new System.EventHandler(this.txtname_Click);
            this.txtname.TextChanged += new System.EventHandler(this.txtname_TextChanged);
            this.txtname.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtname_KeyDown);
            // 
            // dtpgrndt
            // 
            this.dtpgrndt.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpgrndt.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpgrndt.Location = new System.Drawing.Point(135, 30);
            this.dtpgrndt.Name = "dtpgrndt";
            this.dtpgrndt.Size = new System.Drawing.Size(122, 27);
            this.dtpgrndt.TabIndex = 0;
            this.dtpgrndt.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.dtpgrndt_KeyPress);
            // 
            // txtgrn
            // 
            this.txtgrn.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtgrn.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtgrn.Location = new System.Drawing.Point(19, 30);
            this.txtgrn.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtgrn.Name = "txtgrn";
            this.txtgrn.Size = new System.Drawing.Size(110, 27);
            this.txtgrn.TabIndex = 187;
            // 
            // Phone
            // 
            this.Phone.AutoSize = true;
            this.Phone.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Phone.Location = new System.Drawing.Point(19, 8);
            this.Phone.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Phone.Name = "Phone";
            this.Phone.Size = new System.Drawing.Size(52, 19);
            this.Phone.TabIndex = 188;
            this.Phone.Text = "DocNo";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(430, 8);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(103, 19);
            this.label3.TabIndex = 186;
            this.label3.Text = "Supplier Name";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(135, 8);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(69, 19);
            this.label2.TabIndex = 189;
            this.label2.Text = "Doc Date";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(488, 61);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(54, 21);
            this.label1.TabIndex = 86;
            this.label1.Text = "SocNo";
            // 
            // txtvehicle
            // 
            this.txtvehicle.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtvehicle.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvehicle.Location = new System.Drawing.Point(485, 85);
            this.txtvehicle.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtvehicle.Name = "txtvehicle";
            this.txtvehicle.Size = new System.Drawing.Size(151, 27);
            this.txtvehicle.TabIndex = 7;
            this.txtvehicle.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtvehicle_KeyPress);
            // 
            // txtitem
            // 
            this.txtitem.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtitem.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtitem.Location = new System.Drawing.Point(616, 296);
            this.txtitem.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtitem.Name = "txtitem";
            this.txtitem.Size = new System.Drawing.Size(111, 22);
            this.txtitem.TabIndex = 1222;
            this.txtitem.Click += new System.EventHandler(this.txtitem_Click);
            this.txtitem.TextChanged += new System.EventHandler(this.txtitem_TextChanged);
            this.txtitem.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtitem_KeyDown);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(612, 270);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(91, 21);
            this.label7.TabIndex = 66;
            this.label7.Text = "Work Order";
            // 
            // txtqty
            // 
            this.txtqty.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtqty.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtqty.Location = new System.Drawing.Point(268, 30);
            this.txtqty.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtqty.Name = "txtqty";
            this.txtqty.Size = new System.Drawing.Size(151, 27);
            this.txtqty.TabIndex = 1;
            this.txtqty.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtqty_KeyPress);
            // 
            // txtdcqty
            // 
            this.txtdcqty.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtdcqty.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtdcqty.Location = new System.Drawing.Point(18, 134);
            this.txtdcqty.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtdcqty.Name = "txtdcqty";
            this.txtdcqty.Size = new System.Drawing.Size(401, 27);
            this.txtdcqty.TabIndex = 8;
            this.txtdcqty.Click += new System.EventHandler(this.txtdcqty_Click);
            this.txtdcqty.TextChanged += new System.EventHandler(this.txtdcqty_TextChanged);
            this.txtdcqty.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtdcqty_KeyDown);
            this.txtdcqty.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtdcqty_KeyPress);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(268, 8);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(75, 19);
            this.label9.TabIndex = 90;
            this.label9.Text = "Reference";
            // 
            // HFIT
            // 
            this.HFIT.BackgroundColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.HFIT.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.HFIT.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.HFIT.Location = new System.Drawing.Point(16, 164);
            this.HFIT.Name = "HFIT";
            this.HFIT.Size = new System.Drawing.Size(986, 374);
            this.HFIT.TabIndex = 89;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(348, 363);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(63, 21);
            this.label4.TabIndex = 34;
            this.label4.Text = "Prcoess";
            // 
            // grSearch
            // 
            this.grSearch.BackColor = System.Drawing.Color.White;
            this.grSearch.Controls.Add(this.button18);
            this.grSearch.Controls.Add(this.btnHide);
            this.grSearch.Controls.Add(this.DataGridCommon);
            this.grSearch.Location = new System.Drawing.Point(563, 148);
            this.grSearch.Name = "grSearch";
            this.grSearch.Size = new System.Drawing.Size(442, 214);
            this.grSearch.TabIndex = 399;
            this.grSearch.Visible = false;
            // 
            // button18
            // 
            this.button18.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button18.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button18.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button18.Location = new System.Drawing.Point(365, 179);
            this.button18.Name = "button18";
            this.button18.Size = new System.Drawing.Size(72, 28);
            this.button18.TabIndex = 394;
            this.button18.Text = "Select";
            this.button18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button18.UseVisualStyleBackColor = false;
            this.button18.Click += new System.EventHandler(this.button18_Click);
            // 
            // btnHide
            // 
            this.btnHide.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnHide.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHide.Image = ((System.Drawing.Image)(resources.GetObject("btnHide.Image")));
            this.btnHide.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnHide.Location = new System.Drawing.Point(7, 180);
            this.btnHide.Name = "btnHide";
            this.btnHide.Size = new System.Drawing.Size(69, 27);
            this.btnHide.TabIndex = 393;
            this.btnHide.Text = "Close";
            this.btnHide.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnHide.UseVisualStyleBackColor = false;
            this.btnHide.Click += new System.EventHandler(this.btnHide_Click);
            // 
            // DataGridCommon
            // 
            this.DataGridCommon.AllowUserToAddRows = false;
            this.DataGridCommon.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.DataGridCommon.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridCommon.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.DataGridCommon.Location = new System.Drawing.Point(7, 10);
            this.DataGridCommon.Name = "DataGridCommon";
            this.DataGridCommon.ReadOnly = true;
            this.DataGridCommon.RowHeadersVisible = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.DataGridCommon.RowsDefaultCellStyle = dataGridViewCellStyle1;
            this.DataGridCommon.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridCommon.Size = new System.Drawing.Size(430, 168);
            this.DataGridCommon.TabIndex = 0;
            this.DataGridCommon.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridCommon_CellContentClick);
            this.DataGridCommon.DoubleClick += new System.EventHandler(this.DataGridCommon_DoubleClick);
            this.DataGridCommon.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DataGridCommon_KeyDown);
            // 
            // txtprice
            // 
            this.txtprice.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtprice.Enabled = false;
            this.txtprice.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtprice.Location = new System.Drawing.Point(791, 134);
            this.txtprice.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtprice.Name = "txtprice";
            this.txtprice.Size = new System.Drawing.Size(86, 26);
            this.txtprice.TabIndex = 13;
            this.txtprice.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtprice_KeyPress);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(791, 112);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(37, 18);
            this.label8.TabIndex = 404;
            this.label8.Text = "Uom";
            // 
            // txtaddnotes
            // 
            this.txtaddnotes.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtaddnotes.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtaddnotes.Location = new System.Drawing.Point(420, 134);
            this.txtaddnotes.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtaddnotes.Name = "txtaddnotes";
            this.txtaddnotes.Size = new System.Drawing.Size(115, 26);
            this.txtaddnotes.TabIndex = 9;
            this.txtaddnotes.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtaddnotes_KeyPress);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(420, 112);
            this.label19.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(88, 18);
            this.label19.TabIndex = 406;
            this.label19.Text = "Batch/Lot No";
            // 
            // textBox1
            // 
            this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox1.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.Location = new System.Drawing.Point(709, 134);
            this.textBox1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(81, 26);
            this.textBox1.TabIndex = 12;
            this.textBox1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox1_KeyPress);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(709, 112);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(69, 18);
            this.label5.TabIndex = 408;
            this.label5.Text = "NoofRolls";
            // 
            // Editpnl
            // 
            this.Editpnl.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.Editpnl.Controls.Add(this.grSearch);
            this.Editpnl.Controls.Add(this.lkppnl);
            this.Editpnl.Controls.Add(this.label23);
            this.Editpnl.Controls.Add(this.txtdcno);
            this.Editpnl.Controls.Add(this.label14);
            this.Editpnl.Controls.Add(this.dtpsupp);
            this.Editpnl.Controls.Add(this.HFIT);
            this.Editpnl.Controls.Add(this.btnadd);
            this.Editpnl.Controls.Add(this.label9);
            this.Editpnl.Controls.Add(this.txtdcqty);
            this.Editpnl.Controls.Add(this.txtqty);
            this.Editpnl.Controls.Add(this.label7);
            this.Editpnl.Controls.Add(this.txtitem);
            this.Editpnl.Controls.Add(this.txtvehicle);
            this.Editpnl.Controls.Add(this.label1);
            this.Editpnl.Controls.Add(this.label2);
            this.Editpnl.Controls.Add(this.label3);
            this.Editpnl.Controls.Add(this.Phone);
            this.Editpnl.Controls.Add(this.txtgrn);
            this.Editpnl.Controls.Add(this.dtpgrndt);
            this.Editpnl.Controls.Add(this.txtname);
            this.Editpnl.Controls.Add(this.txtbeamid);
            this.Editpnl.Controls.Add(this.txtoutputid);
            this.Editpnl.Controls.Add(this.txtitemid);
            this.Editpnl.Controls.Add(this.txtmillid);
            this.Editpnl.Controls.Add(this.txtpuid);
            this.Editpnl.Controls.Add(this.txtgrnid);
            this.Editpnl.Controls.Add(this.button2);
            this.Editpnl.Controls.Add(this.HFGP1);
            this.Editpnl.Controls.Add(this.txtnobeams);
            this.Editpnl.Controls.Add(this.label17);
            this.Editpnl.Controls.Add(this.txtrectot);
            this.Editpnl.Controls.Add(this.label10);
            this.Editpnl.Controls.Add(this.txtmode);
            this.Editpnl.Controls.Add(this.label6);
            this.Editpnl.Controls.Add(this.label5);
            this.Editpnl.Controls.Add(this.textBox1);
            this.Editpnl.Controls.Add(this.label19);
            this.Editpnl.Controls.Add(this.txtaddnotes);
            this.Editpnl.Controls.Add(this.label8);
            this.Editpnl.Controls.Add(this.txtprice);
            this.Editpnl.Controls.Add(this.txtuom);
            this.Editpnl.Controls.Add(this.label16);
            this.Editpnl.Controls.Add(this.txtisstot);
            this.Editpnl.Controls.Add(this.label15);
            this.Editpnl.Controls.Add(this.label18);
            this.Editpnl.Controls.Add(this.txtbags);
            this.Editpnl.Controls.Add(this.label4);
            this.Editpnl.Controls.Add(this.label24);
            this.Editpnl.Controls.Add(this.txtwo);
            this.Editpnl.Location = new System.Drawing.Point(1, 4);
            this.Editpnl.Name = "Editpnl";
            this.Editpnl.Size = new System.Drawing.Size(1009, 544);
            this.Editpnl.TabIndex = 1;
            this.Editpnl.Paint += new System.Windows.Forms.PaintEventHandler(this.Editpnl_Paint);
            // 
            // lkppnl
            // 
            this.lkppnl.BackColor = System.Drawing.SystemColors.HighlightText;
            this.lkppnl.Controls.Add(this.label21);
            this.lkppnl.Controls.Add(this.txtstyle);
            this.lkppnl.Controls.Add(this.txtscono);
            this.lkppnl.Controls.Add(this.label36);
            this.lkppnl.Controls.Add(this.txtwok);
            this.lkppnl.Controls.Add(this.label20);
            this.lkppnl.Controls.Add(this.button10);
            this.lkppnl.Controls.Add(this.button11);
            this.lkppnl.Controls.Add(this.txtscr11);
            this.lkppnl.Controls.Add(this.HFGP2);
            this.lkppnl.Controls.Add(this.label69);
            this.lkppnl.Controls.Add(this.label67);
            this.lkppnl.Controls.Add(this.cbowono);
            this.lkppnl.Location = new System.Drawing.Point(15, 157);
            this.lkppnl.Name = "lkppnl";
            this.lkppnl.Size = new System.Drawing.Size(796, 365);
            this.lkppnl.TabIndex = 409;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(418, 43);
            this.label21.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(40, 19);
            this.label21.TabIndex = 436;
            this.label21.Text = "Style";
            // 
            // txtstyle
            // 
            this.txtstyle.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtstyle.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtstyle.Location = new System.Drawing.Point(463, 38);
            this.txtstyle.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtstyle.Name = "txtstyle";
            this.txtstyle.Size = new System.Drawing.Size(308, 27);
            this.txtstyle.TabIndex = 435;
            // 
            // txtscono
            // 
            this.txtscono.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtscono.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtscono.Location = new System.Drawing.Point(64, 34);
            this.txtscono.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtscono.Name = "txtscono";
            this.txtscono.Size = new System.Drawing.Size(99, 27);
            this.txtscono.TabIndex = 434;
            this.txtscono.Click += new System.EventHandler(this.txtscono_Click);
            this.txtscono.TextChanged += new System.EventHandler(this.txtscono_TextChanged);
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.Location = new System.Drawing.Point(167, 42);
            this.label36.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(105, 19);
            this.label36.TabIndex = 433;
            this.label36.Text = "Work Order No";
            // 
            // txtwok
            // 
            this.txtwok.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtwok.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtwok.Location = new System.Drawing.Point(280, 37);
            this.txtwok.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtwok.Name = "txtwok";
            this.txtwok.Size = new System.Drawing.Size(127, 27);
            this.txtwok.TabIndex = 431;
            this.txtwok.TextChanged += new System.EventHandler(this.textBox4_TextChanged);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(10, 34);
            this.label20.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(49, 19);
            this.label20.TabIndex = 430;
            this.label20.Text = "SocNo";
            // 
            // button10
            // 
            this.button10.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button10.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button10.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button10.Location = new System.Drawing.Point(702, 329);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(76, 28);
            this.button10.TabIndex = 396;
            this.button10.Text = "Select ";
            this.button10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button10.UseVisualStyleBackColor = false;
            this.button10.Click += new System.EventHandler(this.button10_Click);
            // 
            // button11
            // 
            this.button11.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button11.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button11.Image = ((System.Drawing.Image)(resources.GetObject("button11.Image")));
            this.button11.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button11.Location = new System.Drawing.Point(8, 330);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(69, 27);
            this.button11.TabIndex = 395;
            this.button11.Text = "Close";
            this.button11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button11.UseVisualStyleBackColor = false;
            this.button11.Click += new System.EventHandler(this.button11_Click);
            // 
            // txtscr11
            // 
            this.txtscr11.AcceptsReturn = true;
            this.txtscr11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtscr11.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtscr11.Location = new System.Drawing.Point(4, 4);
            this.txtscr11.Margin = new System.Windows.Forms.Padding(4);
            this.txtscr11.Name = "txtscr11";
            this.txtscr11.Size = new System.Drawing.Size(767, 26);
            this.txtscr11.TabIndex = 98;
            this.txtscr11.TextChanged += new System.EventHandler(this.txtscr11_TextChanged);
            this.txtscr11.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtscr11_KeyDown);
            // 
            // HFGP2
            // 
            this.HFGP2.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.HFGP2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.HFGP2.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.HFGP2.Location = new System.Drawing.Point(8, 72);
            this.HFGP2.Name = "HFGP2";
            this.HFGP2.Size = new System.Drawing.Size(772, 252);
            this.HFGP2.TabIndex = 97;
            this.HFGP2.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.HFGP2_CellContentClick);
            this.HFGP2.DoubleClick += new System.EventHandler(this.HFGP2_DoubleClick);
            this.HFGP2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.HFGP2_KeyDown);
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label69.ForeColor = System.Drawing.Color.White;
            this.label69.Location = new System.Drawing.Point(64, 85);
            this.label69.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(81, 21);
            this.label69.TabIndex = 196;
            this.label69.Text = "Itemname";
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label67.ForeColor = System.Drawing.Color.White;
            this.label67.Location = new System.Drawing.Point(8, 184);
            this.label67.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(124, 21);
            this.label67.TabIndex = 198;
            this.label67.Text = "Press ESC to Exit";
            // 
            // cbowono
            // 
            this.cbowono.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbowono.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbowono.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbowono.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbowono.FormattingEnabled = true;
            this.cbowono.Location = new System.Drawing.Point(54, 108);
            this.cbowono.Name = "cbowono";
            this.cbowono.Size = new System.Drawing.Size(179, 27);
            this.cbowono.TabIndex = 432;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(257, 63);
            this.label23.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(78, 19);
            this.label23.TabIndex = 461;
            this.label23.Text = "Our DC No";
            // 
            // txtdcno
            // 
            this.txtdcno.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtdcno.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtdcno.Location = new System.Drawing.Point(257, 84);
            this.txtdcno.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtdcno.Name = "txtdcno";
            this.txtdcno.Size = new System.Drawing.Size(106, 27);
            this.txtdcno.TabIndex = 5;
            this.txtdcno.Click += new System.EventHandler(this.txtdcno_Click);
            this.txtdcno.TextChanged += new System.EventHandler(this.txtdcno_TextChanged);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(128, 62);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(81, 21);
            this.label14.TabIndex = 411;
            this.label14.Text = "Supp Date";
            // 
            // dtpsupp
            // 
            this.dtpsupp.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpsupp.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpsupp.Location = new System.Drawing.Point(132, 84);
            this.dtpsupp.Name = "dtpsupp";
            this.dtpsupp.Size = new System.Drawing.Size(122, 27);
            this.dtpsupp.TabIndex = 4;
            this.dtpsupp.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.dtpsupp_KeyPress);
            // 
            // btnadd
            // 
            this.btnadd.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnadd.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnadd.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnadd.Location = new System.Drawing.Point(950, 134);
            this.btnadd.Name = "btnadd";
            this.btnadd.Size = new System.Drawing.Size(42, 27);
            this.btnadd.TabIndex = 15;
            this.btnadd.Text = "OK";
            this.btnadd.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnadd.UseVisualStyleBackColor = false;
            this.btnadd.Click += new System.EventHandler(this.btnadd_Click);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button2.Location = new System.Drawing.Point(704, 165);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(34, 32);
            this.button2.TabIndex = 99;
            this.button2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button2.UseVisualStyleBackColor = false;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(12, 62);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(108, 21);
            this.label10.TabIndex = 400;
            this.label10.Text = "Supplier DcNo";
            // 
            // txtmode
            // 
            this.txtmode.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtmode.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtmode.Location = new System.Drawing.Point(15, 84);
            this.txtmode.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtmode.Name = "txtmode";
            this.txtmode.Size = new System.Drawing.Size(114, 27);
            this.txtmode.TabIndex = 3;
            this.txtmode.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtmode_KeyPress);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(371, 63);
            this.label24.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(83, 19);
            this.label24.TabIndex = 462;
            this.label24.Text = "Work Order";
            // 
            // txtwo
            // 
            this.txtwo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtwo.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtwo.Location = new System.Drawing.Point(371, 84);
            this.txtwo.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtwo.Name = "txtwo";
            this.txtwo.Size = new System.Drawing.Size(106, 27);
            this.txtwo.TabIndex = 6;
            // 
            // Chkedtact
            // 
            this.Chkedtact.AutoSize = true;
            this.Chkedtact.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Chkedtact.Location = new System.Drawing.Point(800, 559);
            this.Chkedtact.Name = "Chkedtact";
            this.Chkedtact.Size = new System.Drawing.Size(62, 20);
            this.Chkedtact.TabIndex = 335;
            this.Chkedtact.Text = "Active";
            this.Chkedtact.UseVisualStyleBackColor = true;
            // 
            // buttnfinbk
            // 
            this.buttnfinbk.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttnfinbk.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttnfinbk.Image = ((System.Drawing.Image)(resources.GetObject("buttnfinbk.Image")));
            this.buttnfinbk.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttnfinbk.Location = new System.Drawing.Point(946, 552);
            this.buttnfinbk.Margin = new System.Windows.Forms.Padding(4);
            this.buttnfinbk.Name = "buttnfinbk";
            this.buttnfinbk.Size = new System.Drawing.Size(60, 30);
            this.buttnfinbk.TabIndex = 333;
            this.buttnfinbk.Text = "Back";
            this.buttnfinbk.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttnfinbk.UseVisualStyleBackColor = false;
            this.buttnfinbk.Click += new System.EventHandler(this.buttnfinbk_Click);
            // 
            // btnsave
            // 
            this.btnsave.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnsave.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnsave.Image = ((System.Drawing.Image)(resources.GetObject("btnsave.Image")));
            this.btnsave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnsave.Location = new System.Drawing.Point(869, 552);
            this.btnsave.Margin = new System.Windows.Forms.Padding(4);
            this.btnsave.Name = "btnsave";
            this.btnsave.Size = new System.Drawing.Size(69, 30);
            this.btnsave.TabIndex = 334;
            this.btnsave.Text = "Save";
            this.btnsave.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnsave.UseVisualStyleBackColor = false;
            this.btnsave.Click += new System.EventHandler(this.btnsave_Click);
            // 
            // FrmJORKnitting
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1005, 584);
            this.Controls.Add(this.panadd);
            this.Controls.Add(this.buttnfinbk);
            this.Controls.Add(this.btnsave);
            this.Controls.Add(this.Chkedtact);
            this.Controls.Add(this.Editpnl);
            this.Controls.Add(this.Genpan);
            this.Name = "FrmJORKnitting";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Knitting Receipt";
            this.Load += new System.EventHandler(this.FrmJORKnitting_Load);
            this.Genpan.ResumeLayout(false);
            this.Genpan.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HFGP)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panadd.ResumeLayout(false);
            this.panadd.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HFGP1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HFIT)).EndInit();
            this.grSearch.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DataGridCommon)).EndInit();
            this.Editpnl.ResumeLayout(false);
            this.Editpnl.PerformLayout();
            this.lkppnl.ResumeLayout(false);
            this.lkppnl.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HFGP2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Panel Genpan;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox txtscr7;
        private System.Windows.Forms.TextBox txtscr6;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtscr5;
        private System.Windows.Forms.TextBox txtscr4;
        private System.Windows.Forms.TextBox Txtscr3;
        private System.Windows.Forms.TextBox Txtscr2;
        private System.Windows.Forms.TextBox txtscr1;
        private System.Windows.Forms.DataGridView HFGP;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel4;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel5;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel6;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.DateTimePicker dtpfnt;
        private System.Windows.Forms.TextBox txtscr8;
        private System.Windows.Forms.Panel panadd;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button buttnext1;
        private System.Windows.Forms.CheckBox chkact;
        private System.Windows.Forms.Button butcan;
        private System.Windows.Forms.Button butedit;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button btnsave;
        private System.Windows.Forms.Button buttnfinbk;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtbags;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtisstot;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtrectot;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtnobeams;
        private System.Windows.Forms.DataGridView HFGP1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox txtuom;
        private System.Windows.Forms.TextBox txtgrnid;
        private System.Windows.Forms.TextBox txtpuid;
        private System.Windows.Forms.TextBox txtmillid;
        private System.Windows.Forms.TextBox txtitemid;
        private System.Windows.Forms.TextBox txtoutputid;
        private System.Windows.Forms.TextBox txtbeamid;
        private System.Windows.Forms.TextBox txtname;
        private System.Windows.Forms.DateTimePicker dtpgrndt;
        private System.Windows.Forms.TextBox txtgrn;
        private System.Windows.Forms.Label Phone;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtvehicle;
        private System.Windows.Forms.TextBox txtitem;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtqty;
        private System.Windows.Forms.TextBox txtdcqty;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button btnadd;
        private System.Windows.Forms.DataGridView HFGP2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel grSearch;
        private System.Windows.Forms.Button button18;
        private System.Windows.Forms.Button btnHide;
        private System.Windows.Forms.DataGridView DataGridCommon;
        private System.Windows.Forms.TextBox txtprice;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtaddnotes;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Panel Editpnl;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtmode;
        private System.Windows.Forms.Panel lkppnl;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.TextBox txtscr11;
        private System.Windows.Forms.DataGridView HFIT;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.CheckBox Chkedtact;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.DateTimePicker dtpsupp;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox txtwo;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox txtdcno;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.ComboBox cbowono;
        private System.Windows.Forms.TextBox txtwok;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox txtscono;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox txtstyle;
    }
}